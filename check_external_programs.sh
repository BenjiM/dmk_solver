#!/bin/bash

# check programs 
for program in gfortran python; 
do
   if ! [ -x "$(command -v ${program})" ]; 
    then
	echo "Program:( " ${program} " ) not found"
	exit 1
    fi   
done
#echo "Programs ok"
#check external libraries
for lib in blas lapack; 
do
    test=$(ldconfig -p | grep ${lib})
    if [ -z "${test}" ]; 
    then
	echo "Library: ( "${lib} " ) not found"
	exit 1
    fi
done
#echo "External libraries ok"
# check python packages
for ppack in numpy click meshpy pyvtk; 
do
    pip show ${ppack} 1>/dev/null #pip3 for Python 3
    if [ $? != 0 ]; then
	echo "Python package: ( "${ppack}" ) not found" 
	exit 1
    fi
done
#echo "Python packages ok"
