#!/bin/bash

# store repository location
pathrepo=$(pwd)

###############################################
# Make working directory with controls and runs
###############################################
fname=$1
if [ -z "$fname" ]
then
    echo 'Give path of working folder'
    exit 1
fi
# create working folder
mkdir ${fname} 
# move 
mkdir ${fname}/runs
echo ${pathrepo}>${fname}/location_repository.txt
cp -r ${pathrepo}/script_controls/* ${fname}

###############################################
# Compile all codes
###############################################
cd code 
make clobber
make dirs
make
cd -

for f in preprocess/*_assembly/{uniform_refinement,subgrid_preprocess}/code/
do
    cd $f
    echo $f
    make clobber
    make dirs
    make
    cd -
done

# geometry mini-programs
for f in timedata2vtk data2vtk lp_norm grid2vtk
do
    cd ../geometry/$f
    echo $f
    make clobber
    make dirs
    make
    cd -
done
