from __future__ import print_function, absolute_import, division
import _mockdt
import f90wrap.runtime
import logging

class Kinddeclaration(f90wrap.runtime.FortranModule):
    """
    Module kinddeclaration
    
    
    Defined at 00modKindDeclaration.fpp lines 16-42
    
    """
    @property
    def single(self):
        """
        Element single ftype=integer pytype=int
        
        
        Defined at 00modKindDeclaration.fpp line 21
        
        """
        return _mockdt.f90wrap_kinddeclaration__get__single()
    
    @property
    def double_bn(self):
        """
        Element double_bn ftype=integer pytype=int
        
        
        Defined at 00modKindDeclaration.fpp line 23
        
        """
        return _mockdt.f90wrap_kinddeclaration__get__double_bn()
    
    @property
    def zero(self):
        """
        Element zero ftype=real(kind=double) pytype=float
        
        
        Defined at 00modKindDeclaration.fpp line 27
        
        """
        return _mockdt.f90wrap_kinddeclaration__get__zero()
    
    @property
    def one(self):
        """
        Element one ftype=real(kind=double) pytype=float
        
        
        Defined at 00modKindDeclaration.fpp line 28
        
        """
        return _mockdt.f90wrap_kinddeclaration__get__one()
    
    def __str__(self):
        ret = ['<kinddeclaration>{\n']
        ret.append('    single : ')
        ret.append(repr(self.single))
        ret.append(',\n    double_bn : ')
        ret.append(repr(self.double_bn))
        ret.append(',\n    zero : ')
        ret.append(repr(self.zero))
        ret.append(',\n    one : ')
        ret.append(repr(self.one))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

kinddeclaration = Kinddeclaration()

class Leveltwomod(f90wrap.runtime.FortranModule):
    """
    Module leveltwomod
    
    
    Defined at leveltwomod.fpp lines 5-11
    
    """
    @f90wrap.runtime.register_class("mockdt.leveltwo")
    class leveltwo(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=leveltwo)
        
        
        Defined at leveltwomod.fpp lines 9-11
        
        """
        def __init__(self, handle=None):
            """
            self = Leveltwo()
            
            
            Defined at leveltwomod.fpp lines 9-11
            
            
            Returns
            -------
            this : Leveltwo
            	Object to be constructed
            
            
            Automatically generated constructor for leveltwo
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_leveltwo_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Leveltwo
            
            
            Defined at leveltwomod.fpp lines 9-11
            
            Parameters
            ----------
            this : Leveltwo
            	Object to be destructed
            
            
            Automatically generated destructor for leveltwo
            """
            if self._alloc:
                _mockdt.f90wrap_leveltwo_finalise(this=self._handle)
        
        @property
        def rl(self):
            """
            Element rl ftype=real(8) pytype=float
            
            
            Defined at leveltwomod.fpp line 10
            
            """
            return _mockdt.f90wrap_leveltwo__get__rl(self._handle)
        
        @rl.setter
        def rl(self, rl):
            _mockdt.f90wrap_leveltwo__set__rl(self._handle, rl)
        
        def __str__(self):
            ret = ['<leveltwo>{\n']
            ret.append('    rl : ')
            ret.append(repr(self.rl))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    _dt_array_initialisers = []
    

leveltwomod = Leveltwomod()

class Define_A_Type(f90wrap.runtime.FortranModule):
    """
    Module define_a_type
    
    
    Defined at define.fpp lines 5-51
    
    Define a type which is referenced by the primary used type, to make
    sure it is also wrapped (and we can access its elements).
    """
    @f90wrap.runtime.register_class("mockdt.atype")
    class atype(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=atype)
        
        
        Defined at define.fpp lines 9-16
        
        """
        def __init__(self, handle=None):
            """
            self = Atype()
            
            
            Defined at define.fpp lines 9-16
            
            
            Returns
            -------
            this : Atype
            	Object to be constructed
            
            
            Automatically generated constructor for atype
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_atype_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Atype
            
            
            Defined at define.fpp lines 9-16
            
            Parameters
            ----------
            this : Atype
            	Object to be destructed
            
            
            Automatically generated destructor for atype
            """
            if self._alloc:
                _mockdt.f90wrap_atype_finalise(this=self._handle)
        
        @property
        def bool(self):
            """
            Element bool ftype=logical pytype=bool
            
            
            Defined at define.fpp line 10
            
            """
            return _mockdt.f90wrap_atype__get__bool(self._handle)
        
        @bool.setter
        def bool(self, bool):
            _mockdt.f90wrap_atype__set__bool(self._handle, bool)
        
        @property
        def integ(self):
            """
            Element integ ftype=integer      pytype=int
            
            
            Defined at define.fpp line 11
            
            """
            return _mockdt.f90wrap_atype__get__integ(self._handle)
        
        @integ.setter
        def integ(self, integ):
            _mockdt.f90wrap_atype__set__integ(self._handle, integ)
        
        @property
        def rl(self):
            """
            Element rl ftype=real(kind=double) pytype=float
            
            
            Defined at define.fpp line 12
            
            """
            return _mockdt.f90wrap_atype__get__rl(self._handle)
        
        @rl.setter
        def rl(self, rl):
            _mockdt.f90wrap_atype__set__rl(self._handle, rl)
        
        @property
        def vec(self):
            """
            Element vec ftype=real(8) pytype=float
            
            
            Defined at define.fpp line 13
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_atype__array__vec(self._handle)
            if array_handle in self._arrays:
                vec = self._arrays[array_handle]
            else:
                vec = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_atype__array__vec)
                self._arrays[array_handle] = vec
            return vec
        
        @vec.setter
        def vec(self, vec):
            self.vec[...] = vec
        
        @property
        def dtype(self):
            """
            Element dtype ftype=type(leveltwo) pytype=Leveltwo
            
            
            Defined at define.fpp line 14
            
            """
            dtype_handle = _mockdt.f90wrap_atype__get__dtype(self._handle)
            if tuple(dtype_handle) in self._objs:
                dtype = self._objs[tuple(dtype_handle)]
            else:
                dtype = leveltwomod.leveltwo.from_handle(dtype_handle)
                self._objs[tuple(dtype_handle)] = dtype
            return dtype
        
        @dtype.setter
        def dtype(self, dtype):
            dtype = dtype._handle
            _mockdt.f90wrap_atype__set__dtype(self._handle, dtype)
        
        @property
        def prova(self):
            """
            Element prova ftype=character(256) pytype=str
            
            
            Defined at define.fpp line 15
            
            """
            return _mockdt.f90wrap_atype__get__prova(self._handle)
        
        @prova.setter
        def prova(self, prova):
            _mockdt.f90wrap_atype__set__prova(self._handle, prova)
        
        def __str__(self):
            ret = ['<atype>{\n']
            ret.append('    bool : ')
            ret.append(repr(self.bool))
            ret.append(',\n    integ : ')
            ret.append(repr(self.integ))
            ret.append(',\n    rl : ')
            ret.append(repr(self.rl))
            ret.append(',\n    vec : ')
            ret.append(repr(self.vec))
            ret.append(',\n    dtype : ')
            ret.append(repr(self.dtype))
            ret.append(',\n    prova : ')
            ret.append(repr(self.prova))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @f90wrap.runtime.register_class("mockdt.unused_type")
    class unused_type(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=unused_type)
        
        
        Defined at define.fpp lines 22-24
        
        """
        def __init__(self, handle=None):
            """
            self = Unused_Type()
            
            
            Defined at define.fpp lines 22-24
            
            
            Returns
            -------
            this : Unused_Type
            	Object to be constructed
            
            
            Automatically generated constructor for unused_type
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_unused_type_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Unused_Type
            
            
            Defined at define.fpp lines 22-24
            
            Parameters
            ----------
            this : Unused_Type
            	Object to be destructed
            
            
            Automatically generated destructor for unused_type
            """
            if self._alloc:
                _mockdt.f90wrap_unused_type_finalise(this=self._handle)
        
        @property
        def rl(self):
            """
            Element rl ftype=real(8) pytype=float
            
            
            Defined at define.fpp line 23
            
            """
            return _mockdt.f90wrap_unused_type__get__rl(self._handle)
        
        @rl.setter
        def rl(self, rl):
            _mockdt.f90wrap_unused_type__set__rl(self._handle, rl)
        
        def __str__(self):
            ret = ['<unused_type>{\n']
            ret.append('    rl : ')
            ret.append(repr(self.rl))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @staticmethod
    def use_set_vars():
        """
        use_set_vars()
        
        
        Defined at define.fpp lines 28-33
        
        
        This type will be wrapped as it is used.
        """
        _mockdt.f90wrap_use_set_vars()
    
    @staticmethod
    def return_a_type_func():
        """
        a = return_a_type_func()
        
        
        Defined at define.fpp lines 35-41
        
        
        Returns
        -------
        a : Atype
        
        """
        a = _mockdt.f90wrap_return_a_type_func()
        a = f90wrap.runtime.lookup_class("mockdt.atype").from_handle(a)
        return a
    
    @staticmethod
    def return_a_type_sub():
        """
        a = return_a_type_sub()
        
        
        Defined at define.fpp lines 43-49
        
        
        Returns
        -------
        a : Atype
        
        """
        a = _mockdt.f90wrap_return_a_type_sub()
        a = f90wrap.runtime.lookup_class("mockdt.atype").from_handle(a)
        return a
    
    @property
    def a_set_real(self):
        """
        Element a_set_real ftype=real(8) pytype=float
        
        
        Defined at define.fpp line 18
        
        """
        return _mockdt.f90wrap_define_a_type__get__a_set_real()
    
    @a_set_real.setter
    def a_set_real(self, a_set_real):
        _mockdt.f90wrap_define_a_type__set__a_set_real(a_set_real)
    
    @property
    def a_set_bool(self):
        """
        Element a_set_bool ftype=logical pytype=bool
        
        
        Defined at define.fpp line 19
        
        This type will also be wrapped
        """
        return _mockdt.f90wrap_define_a_type__get__a_set_bool()
    
    @a_set_bool.setter
    def a_set_bool(self, a_set_bool):
        _mockdt.f90wrap_define_a_type__set__a_set_bool(a_set_bool)
    
    def __str__(self):
        ret = ['<define_a_type>{\n']
        ret.append('    a_set_real : ')
        ret.append(repr(self.a_set_real))
        ret.append(',\n    a_set_bool : ')
        ret.append(repr(self.a_set_bool))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

define_a_type = Define_A_Type()

class Horrible(f90wrap.runtime.FortranModule):
    """
    Module horrible
    
    
    Defined at define.fpp lines 61-66
    
    Another module, defining a horrible type
    """
    @f90wrap.runtime.register_class("mockdt.horrible_type")
    class horrible_type(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=horrible_type)
        
        
        Defined at define.fpp lines 64-65
        
        """
        def __init__(self, handle=None):
            """
            self = Horrible_Type()
            
            
            Defined at define.fpp lines 64-65
            
            
            Returns
            -------
            this : Horrible_Type
            	Object to be constructed
            
            
            Automatically generated constructor for horrible_type
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_horrible_type_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Horrible_Type
            
            
            Defined at define.fpp lines 64-65
            
            Parameters
            ----------
            this : Horrible_Type
            	Object to be destructed
            
            
            Automatically generated destructor for horrible_type
            """
            if self._alloc:
                _mockdt.f90wrap_horrible_type_finalise(this=self._handle)
        
        @property
        def x(self):
            """
            Element x ftype=real(8) pytype=float
            
            
            Defined at define.fpp line 65
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_horrible_type__array__x(self._handle)
            if array_handle in self._arrays:
                x = self._arrays[array_handle]
            else:
                x = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_horrible_type__array__x)
                self._arrays[array_handle] = x
            return x
        
        @x.setter
        def x(self, x):
            self.x[...] = x
        
        def __str__(self):
            ret = ['<horrible_type>{\n']
            ret.append('    x : ')
            ret.append(repr(self.x))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @property
    def a_real(self):
        """
        Element a_real ftype=real(8) pytype=float
        
        
        Defined at define.fpp line 63
        
        """
        return _mockdt.f90wrap_horrible__get__a_real()
    
    @a_real.setter
    def a_real(self, a_real):
        _mockdt.f90wrap_horrible__set__a_real(a_real)
    
    def __str__(self):
        ret = ['<horrible>{\n']
        ret.append('    a_real : ')
        ret.append(repr(self.a_real))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

horrible = Horrible()

class Dmkodedata(f90wrap.runtime.FortranModule):
    """
    Module dmkodedata
    
    
    Defined at 09modDmkOdeData.fpp lines 5-270
    
    """
    @f90wrap.runtime.register_class("mockdt.OdeData")
    class OdeData(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=odedata)
        
        
        Defined at 09modDmkOdeData.fpp lines 28-118
        
        """
        def __init__(self, handle=None):
            """
            self = Odedata()
            
            
            Defined at 09modDmkOdeData.fpp lines 28-118
            
            
            Returns
            -------
            this : Odedata
            	Object to be constructed
            
            
            Automatically generated constructor for odedata
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_odedata_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Odedata
            
            
            Defined at 09modDmkOdeData.fpp lines 28-118
            
            Parameters
            ----------
            this : Odedata
            	Object to be destructed
            
            
            Automatically generated destructor for odedata
            """
            if self._alloc:
                _mockdt.f90wrap_odedata_finalise(this=self._handle)
        
        @property
        def id_ode(self):
            """
            Element id_ode ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 33
            
            """
            return _mockdt.f90wrap_odedata__get__id_ode(self._handle)
        
        @id_ode.setter
        def id_ode(self, id_ode):
            _mockdt.f90wrap_odedata__set__id_ode(self._handle, id_ode)
        
        @property
        def ntimes(self):
            """
            Element ntimes ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 35
            
            """
            return _mockdt.f90wrap_odedata__get__ntimes(self._handle)
        
        @ntimes.setter
        def ntimes(self, ntimes):
            _mockdt.f90wrap_odedata__set__ntimes(self._handle, ntimes)
        
        @property
        def time(self):
            """
            Element time ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 37
            
            """
            return _mockdt.f90wrap_odedata__get__time(self._handle)
        
        @time.setter
        def time(self, time):
            _mockdt.f90wrap_odedata__set__time(self._handle, time)
        
        @property
        def ntdens(self):
            """
            Element ntdens ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 42
            
            """
            return _mockdt.f90wrap_odedata__get__ntdens(self._handle)
        
        @ntdens.setter
        def ntdens(self, ntdens):
            _mockdt.f90wrap_odedata__set__ntdens(self._handle, ntdens)
        
        @property
        def npot(self):
            """
            Element npot ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 44
            
            """
            return _mockdt.f90wrap_odedata__get__npot(self._handle)
        
        @npot.setter
        def npot(self, npot):
            _mockdt.f90wrap_odedata__set__npot(self._handle, npot)
        
        @property
        def dirichlet_exists(self):
            """
            Element dirichlet_exists ftype=logical pytype=bool
            
            
            Defined at 09modDmkOdeData.fpp line 47
            
            """
            return _mockdt.f90wrap_odedata__get__dirichlet_exists(self._handle)
        
        @dirichlet_exists.setter
        def dirichlet_exists(self, dirichlet_exists):
            _mockdt.f90wrap_odedata__set__dirichlet_exists(self._handle, dirichlet_exists)
        
        @property
        def ndir(self):
            """
            Element ndir ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 50
            
            """
            return _mockdt.f90wrap_odedata__get__ndir(self._handle)
        
        @ndir.setter
        def ndir(self, ndir):
            _mockdt.f90wrap_odedata__set__ndir(self._handle, ndir)
        
        @property
        def dirichlet_nodes(self):
            """
            Element dirichlet_nodes ftype=integer pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 53
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__dirichlet_nodes(self._handle)
            if array_handle in self._arrays:
                dirichlet_nodes = self._arrays[array_handle]
            else:
                dirichlet_nodes = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__dirichlet_nodes)
                self._arrays[array_handle] = dirichlet_nodes
            return dirichlet_nodes
        
        @dirichlet_nodes.setter
        def dirichlet_nodes(self, dirichlet_nodes):
            self.dirichlet_nodes[...] = dirichlet_nodes
        
        @property
        def dirichlet_values(self):
            """
            Element dirichlet_values ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 56
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__dirichlet_values(self._handle)
            if array_handle in self._arrays:
                dirichlet_values = self._arrays[array_handle]
            else:
                dirichlet_values = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__dirichlet_values)
                self._arrays[array_handle] = dirichlet_values
            return dirichlet_values
        
        @dirichlet_values.setter
        def dirichlet_values(self, dirichlet_values):
            self.dirichlet_values[...] = dirichlet_values
        
        @property
        def lambda_(self):
            """
            Element lambda_ ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 61
            
            """
            return _mockdt.f90wrap_odedata__get__lambda_(self._handle)
        
        @lambda_.setter
        def lambda_(self, lambda_):
            _mockdt.f90wrap_odedata__set__lambda_(self._handle, lambda_)
        
        @property
        def lasso(self):
            """
            Element lasso ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 65
            
            """
            return _mockdt.f90wrap_odedata__get__lasso(self._handle)
        
        @lasso.setter
        def lasso(self, lasso):
            _mockdt.f90wrap_odedata__set__lasso(self._handle, lasso)
        
        @property
        def pflux(self):
            """
            Element pflux ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 69
            
            """
            return _mockdt.f90wrap_odedata__get__pflux(self._handle)
        
        @pflux.setter
        def pflux(self, pflux):
            _mockdt.f90wrap_odedata__set__pflux(self._handle, pflux)
        
        @property
        def decay(self):
            """
            Element decay ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 72
            
            """
            return _mockdt.f90wrap_odedata__get__decay(self._handle)
        
        @decay.setter
        def decay(self, decay):
            _mockdt.f90wrap_odedata__set__decay(self._handle, decay)
        
        @property
        def pmass(self):
            """
            Element pmass ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 75
            
            """
            return _mockdt.f90wrap_odedata__get__pmass(self._handle)
        
        @pmass.setter
        def pmass(self, pmass):
            _mockdt.f90wrap_odedata__set__pmass(self._handle, pmass)
        
        @property
        def pode(self):
            """
            Element pode ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 79
            
            """
            return _mockdt.f90wrap_odedata__get__pode(self._handle)
        
        @pode.setter
        def pode(self, pode):
            _mockdt.f90wrap_odedata__set__pode(self._handle, pode)
        
        @property
        def penalty_exists(self):
            """
            Element penalty_exists ftype=logical pytype=bool
            
            
            Defined at 09modDmkOdeData.fpp line 84
            
            """
            return _mockdt.f90wrap_odedata__get__penalty_exists(self._handle)
        
        @penalty_exists.setter
        def penalty_exists(self, penalty_exists):
            _mockdt.f90wrap_odedata__set__penalty_exists(self._handle, penalty_exists)
        
        @property
        def penalty_factor(self):
            """
            Element penalty_factor ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 87
            
            """
            return _mockdt.f90wrap_odedata__get__penalty_factor(self._handle)
        
        @penalty_factor.setter
        def penalty_factor(self, penalty_factor):
            _mockdt.f90wrap_odedata__set__penalty_factor(self._handle, penalty_factor)
        
        @property
        def penalty_weight(self):
            """
            Element penalty_weight ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 91
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__penalty_weight(self._handle)
            if array_handle in self._arrays:
                penalty_weight = self._arrays[array_handle]
            else:
                penalty_weight = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__penalty_weight)
                self._arrays[array_handle] = penalty_weight
            return penalty_weight
        
        @penalty_weight.setter
        def penalty_weight(self, penalty_weight):
            self.penalty_weight[...] = penalty_weight
        
        @property
        def penalty(self):
            """
            Element penalty ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 96
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__penalty(self._handle)
            if array_handle in self._arrays:
                penalty = self._arrays[array_handle]
            else:
                penalty = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__penalty)
                self._arrays[array_handle] = penalty
            return penalty
        
        @penalty.setter
        def penalty(self, penalty):
            self.penalty[...] = penalty
        
        @property
        def kappa(self):
            """
            Element kappa ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 101
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__kappa(self._handle)
            if array_handle in self._arrays:
                kappa = self._arrays[array_handle]
            else:
                kappa = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__kappa)
                self._arrays[array_handle] = kappa
            return kappa
        
        @kappa.setter
        def kappa(self, kappa):
            self.kappa[...] = kappa
        
        @property
        def rhs_forcing(self):
            """
            Element rhs_forcing ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 107
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_odedata__array__rhs_forcing(self._handle)
            if array_handle in self._arrays:
                rhs_forcing = self._arrays[array_handle]
            else:
                rhs_forcing = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_odedata__array__rhs_forcing)
                self._arrays[array_handle] = rhs_forcing
            return rhs_forcing
        
        @rhs_forcing.setter
        def rhs_forcing(self, rhs_forcing):
            self.rhs_forcing[...] = rhs_forcing
        
        def __str__(self):
            ret = ['<odedata>{\n']
            ret.append('    id_ode : ')
            ret.append(repr(self.id_ode))
            ret.append(',\n    ntimes : ')
            ret.append(repr(self.ntimes))
            ret.append(',\n    time : ')
            ret.append(repr(self.time))
            ret.append(',\n    ntdens : ')
            ret.append(repr(self.ntdens))
            ret.append(',\n    npot : ')
            ret.append(repr(self.npot))
            ret.append(',\n    dirichlet_exists : ')
            ret.append(repr(self.dirichlet_exists))
            ret.append(',\n    ndir : ')
            ret.append(repr(self.ndir))
            ret.append(',\n    dirichlet_nodes : ')
            ret.append(repr(self.dirichlet_nodes))
            ret.append(',\n    dirichlet_values : ')
            ret.append(repr(self.dirichlet_values))
            ret.append(',\n    lambda_ : ')
            ret.append(repr(self.lambda_))
            ret.append(',\n    lasso : ')
            ret.append(repr(self.lasso))
            ret.append(',\n    pflux : ')
            ret.append(repr(self.pflux))
            ret.append(',\n    decay : ')
            ret.append(repr(self.decay))
            ret.append(',\n    pmass : ')
            ret.append(repr(self.pmass))
            ret.append(',\n    pode : ')
            ret.append(repr(self.pode))
            ret.append(',\n    penalty_exists : ')
            ret.append(repr(self.penalty_exists))
            ret.append(',\n    penalty_factor : ')
            ret.append(repr(self.penalty_factor))
            ret.append(',\n    penalty_weight : ')
            ret.append(repr(self.penalty_weight))
            ret.append(',\n    penalty : ')
            ret.append(repr(self.penalty))
            ret.append(',\n    kappa : ')
            ret.append(repr(self.kappa))
            ret.append(',\n    rhs_forcing : ')
            ret.append(repr(self.rhs_forcing))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @f90wrap.runtime.register_class("mockdt.ProblemData")
    class ProblemData(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=problemdata)
        
        
        Defined at 09modDmkOdeData.fpp lines 120-148
        
        """
        def __init__(self, handle=None):
            """
            self = Problemdata()
            
            
            Defined at 09modDmkOdeData.fpp lines 120-148
            
            
            Returns
            -------
            this : Problemdata
            	Object to be constructed
            
            
            Automatically generated constructor for problemdata
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_problemdata_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Problemdata
            
            
            Defined at 09modDmkOdeData.fpp lines 120-148
            
            Parameters
            ----------
            this : Problemdata
            	Object to be destructed
            
            
            Automatically generated destructor for problemdata
            """
            if self._alloc:
                _mockdt.f90wrap_problemdata_finalise(this=self._handle)
        
        @property
        def ntdens(self):
            """
            Element ntdens ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 125
            
            """
            return _mockdt.f90wrap_problemdata__get__ntdens(self._handle)
        
        @ntdens.setter
        def ntdens(self, ntdens):
            _mockdt.f90wrap_problemdata__set__ntdens(self._handle, ntdens)
        
        @property
        def npot(self):
            """
            Element npot ftype=integer  pytype=int
            
            
            Defined at 09modDmkOdeData.fpp line 127
            
            """
            return _mockdt.f90wrap_problemdata__get__npot(self._handle)
        
        @npot.setter
        def npot(self, npot):
            _mockdt.f90wrap_problemdata__set__npot(self._handle, npot)
        
        @property
        def opt_tdens_exists(self):
            """
            Element opt_tdens_exists ftype=logical pytype=bool
            
            
            Defined at 09modDmkOdeData.fpp line 132
            
            """
            return _mockdt.f90wrap_problemdata__get__opt_tdens_exists(self._handle)
        
        @opt_tdens_exists.setter
        def opt_tdens_exists(self, opt_tdens_exists):
            _mockdt.f90wrap_problemdata__set__opt_tdens_exists(self._handle, \
                opt_tdens_exists)
        
        @property
        def opt_tdens(self):
            """
            Element opt_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 135
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_problemdata__array__opt_tdens(self._handle)
            if array_handle in self._arrays:
                opt_tdens = self._arrays[array_handle]
            else:
                opt_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_problemdata__array__opt_tdens)
                self._arrays[array_handle] = opt_tdens
            return opt_tdens
        
        @opt_tdens.setter
        def opt_tdens(self, opt_tdens):
            self.opt_tdens[...] = opt_tdens
        
        @property
        def opt_pot_exists(self):
            """
            Element opt_pot_exists ftype=logical pytype=bool
            
            
            Defined at 09modDmkOdeData.fpp line 137
            
            """
            return _mockdt.f90wrap_problemdata__get__opt_pot_exists(self._handle)
        
        @opt_pot_exists.setter
        def opt_pot_exists(self, opt_pot_exists):
            _mockdt.f90wrap_problemdata__set__opt_pot_exists(self._handle, opt_pot_exists)
        
        @property
        def opt_pot(self):
            """
            Element opt_pot ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 140
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_problemdata__array__opt_pot(self._handle)
            if array_handle in self._arrays:
                opt_pot = self._arrays[array_handle]
            else:
                opt_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_problemdata__array__opt_pot)
                self._arrays[array_handle] = opt_pot
            return opt_pot
        
        @opt_pot.setter
        def opt_pot(self, opt_pot):
            self.opt_pot[...] = opt_pot
        
        @property
        def tdens0(self):
            """
            Element tdens0 ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 143
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_problemdata__array__tdens0(self._handle)
            if array_handle in self._arrays:
                tdens0 = self._arrays[array_handle]
            else:
                tdens0 = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_problemdata__array__tdens0)
                self._arrays[array_handle] = tdens0
            return tdens0
        
        @tdens0.setter
        def tdens0(self, tdens0):
            self.tdens0[...] = tdens0
        
        @property
        def pot0(self):
            """
            Element pot0 ftype=real(kind=double) pytype=float
            
            
            Defined at 09modDmkOdeData.fpp line 146
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_problemdata__array__pot0(self._handle)
            if array_handle in self._arrays:
                pot0 = self._arrays[array_handle]
            else:
                pot0 = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_problemdata__array__pot0)
                self._arrays[array_handle] = pot0
            return pot0
        
        @pot0.setter
        def pot0(self, pot0):
            self.pot0[...] = pot0
        
        def __str__(self):
            ret = ['<problemdata>{\n']
            ret.append('    ntdens : ')
            ret.append(repr(self.ntdens))
            ret.append(',\n    npot : ')
            ret.append(repr(self.npot))
            ret.append(',\n    opt_tdens_exists : ')
            ret.append(repr(self.opt_tdens_exists))
            ret.append(',\n    opt_tdens : ')
            ret.append(repr(self.opt_tdens))
            ret.append(',\n    opt_pot_exists : ')
            ret.append(repr(self.opt_pot_exists))
            ret.append(',\n    opt_pot : ')
            ret.append(repr(self.opt_pot))
            ret.append(',\n    tdens0 : ')
            ret.append(repr(self.tdens0))
            ret.append(',\n    pot0 : ')
            ret.append(repr(self.pot0))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    _dt_array_initialisers = []
    

dmkodedata = Dmkodedata()

class Controlparameters(f90wrap.runtime.FortranModule):
    """
    Module controlparameters
    
    
    Defined at 89modControlParameters.fpp lines 6-231
    
    """
    @f90wrap.runtime.register_class("mockdt.CtrlPrm")
    class CtrlPrm(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=ctrlprm)
        
        
        Defined at 89modControlParameters.fpp lines 10-231
        
        """
        def __init__(self, handle=None):
            """
            self = Ctrlprm()
            
            
            Defined at 89modControlParameters.fpp lines 10-231
            
            
            Returns
            -------
            this : Ctrlprm
            	Object to be constructed
            
            
            Automatically generated constructor for ctrlprm
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_ctrlprm_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Ctrlprm
            
            
            Defined at 89modControlParameters.fpp lines 10-231
            
            Parameters
            ----------
            this : Ctrlprm
            	Object to be destructed
            
            
            Automatically generated destructor for ctrlprm
            """
            if self._alloc:
                _mockdt.f90wrap_ctrlprm_finalise(this=self._handle)
        
        @property
        def debug(self):
            """
            Element debug ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 15
            
            """
            return _mockdt.f90wrap_ctrlprm__get__debug(self._handle)
        
        @debug.setter
        def debug(self, debug):
            _mockdt.f90wrap_ctrlprm__set__debug(self._handle, debug)
        
        @property
        def info_state(self):
            """
            Element info_state ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 18
            
            """
            return _mockdt.f90wrap_ctrlprm__get__info_state(self._handle)
        
        @info_state.setter
        def info_state(self, info_state):
            _mockdt.f90wrap_ctrlprm__set__info_state(self._handle, info_state)
        
        @property
        def info_inputs_update(self):
            """
            Element info_inputs_update ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 21
            
            """
            return _mockdt.f90wrap_ctrlprm__get__info_inputs_update(self._handle)
        
        @info_inputs_update.setter
        def info_inputs_update(self, info_inputs_update):
            _mockdt.f90wrap_ctrlprm__set__info_inputs_update(self._handle, \
                info_inputs_update)
        
        @property
        def info_newton(self):
            """
            Element info_newton ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 24
            
            """
            return _mockdt.f90wrap_ctrlprm__get__info_newton(self._handle)
        
        @info_newton.setter
        def info_newton(self, info_newton):
            _mockdt.f90wrap_ctrlprm__set__info_newton(self._handle, info_newton)
        
        @property
        def iformat(self):
            """
            Element iformat ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 26
            
            """
            return _mockdt.f90wrap_ctrlprm__get__iformat(self._handle)
        
        @iformat.setter
        def iformat(self, iformat):
            _mockdt.f90wrap_ctrlprm__set__iformat(self._handle, iformat)
        
        @property
        def rformat(self):
            """
            Element rformat ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 28
            
            """
            return _mockdt.f90wrap_ctrlprm__get__rformat(self._handle)
        
        @rformat.setter
        def rformat(self, rformat):
            _mockdt.f90wrap_ctrlprm__set__rformat(self._handle, rformat)
        
        @property
        def iformat_info(self):
            """
            Element iformat_info ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 30
            
            """
            return _mockdt.f90wrap_ctrlprm__get__iformat_info(self._handle)
        
        @iformat_info.setter
        def iformat_info(self, iformat_info):
            _mockdt.f90wrap_ctrlprm__set__iformat_info(self._handle, iformat_info)
        
        @property
        def rformat_info(self):
            """
            Element rformat_info ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 32
            
            """
            return _mockdt.f90wrap_ctrlprm__get__rformat_info(self._handle)
        
        @rformat_info.setter
        def rformat_info(self, rformat_info):
            _mockdt.f90wrap_ctrlprm__set__rformat_info(self._handle, rformat_info)
        
        @property
        def id_subgrid(self):
            """
            Element id_subgrid ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 37
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_subgrid(self._handle)
        
        @id_subgrid.setter
        def id_subgrid(self, id_subgrid):
            _mockdt.f90wrap_ctrlprm__set__id_subgrid(self._handle, id_subgrid)
        
        @property
        def id_ode(self):
            """
            Element id_ode ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 41
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_ode(self._handle)
        
        @id_ode.setter
        def id_ode(self, id_ode):
            _mockdt.f90wrap_ctrlprm__set__id_ode(self._handle, id_ode)
        
        @property
        def min_tdens(self):
            """
            Element min_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 43
            
            """
            return _mockdt.f90wrap_ctrlprm__get__min_tdens(self._handle)
        
        @min_tdens.setter
        def min_tdens(self, min_tdens):
            _mockdt.f90wrap_ctrlprm__set__min_tdens(self._handle, min_tdens)
        
        @property
        def threshold_tdens(self):
            """
            Element threshold_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 45
            
            """
            return _mockdt.f90wrap_ctrlprm__get__threshold_tdens(self._handle)
        
        @threshold_tdens.setter
        def threshold_tdens(self, threshold_tdens):
            _mockdt.f90wrap_ctrlprm__set__threshold_tdens(self._handle, threshold_tdens)
        
        @property
        def id_time_discr(self):
            """
            Element id_time_discr ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 54
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_time_discr(self._handle)
        
        @id_time_discr.setter
        def id_time_discr(self, id_time_discr):
            _mockdt.f90wrap_ctrlprm__set__id_time_discr(self._handle, id_time_discr)
        
        @property
        def tol_nonlinear(self):
            """
            Element tol_nonlinear ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 56
            
            """
            return _mockdt.f90wrap_ctrlprm__get__tol_nonlinear(self._handle)
        
        @tol_nonlinear.setter
        def tol_nonlinear(self, tol_nonlinear):
            _mockdt.f90wrap_ctrlprm__set__tol_nonlinear(self._handle, tol_nonlinear)
        
        @property
        def max_iteration_nonlinear(self):
            """
            Element max_iteration_nonlinear ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 58
            
            """
            return _mockdt.f90wrap_ctrlprm__get__max_iteration_nonlinear(self._handle)
        
        @max_iteration_nonlinear.setter
        def max_iteration_nonlinear(self, max_iteration_nonlinear):
            _mockdt.f90wrap_ctrlprm__set__max_iteration_nonlinear(self._handle, \
                max_iteration_nonlinear)
        
        @property
        def nrestart_max(self):
            """
            Element nrestart_max ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 60
            
            """
            return _mockdt.f90wrap_ctrlprm__get__nrestart_max(self._handle)
        
        @nrestart_max.setter
        def nrestart_max(self, nrestart_max):
            _mockdt.f90wrap_ctrlprm__set__nrestart_max(self._handle, nrestart_max)
        
        @property
        def max_nrestart_invert_jacobian(self):
            """
            Element max_nrestart_invert_jacobian ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 62
            
            """
            return _mockdt.f90wrap_ctrlprm__get__max_nrestart_invert_jacobian(self._handle)
        
        @max_nrestart_invert_jacobian.setter
        def max_nrestart_invert_jacobian(self, max_nrestart_invert_jacobian):
            _mockdt.f90wrap_ctrlprm__set__max_nrestart_invert_jacobian(self._handle, \
                max_nrestart_invert_jacobian)
        
        @property
        def tzero(self):
            """
            Element tzero ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 67
            
            """
            return _mockdt.f90wrap_ctrlprm__get__tzero(self._handle)
        
        @tzero.setter
        def tzero(self, tzero):
            _mockdt.f90wrap_ctrlprm__set__tzero(self._handle, tzero)
        
        @property
        def max_time_it(self):
            """
            Element max_time_it ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 70
            
            """
            return _mockdt.f90wrap_ctrlprm__get__max_time_it(self._handle)
        
        @max_time_it.setter
        def max_time_it(self, max_time_it):
            _mockdt.f90wrap_ctrlprm__set__max_time_it(self._handle, max_time_it)
        
        @property
        def tol_var_tdens(self):
            """
            Element tol_var_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 73
            
            """
            return _mockdt.f90wrap_ctrlprm__get__tol_var_tdens(self._handle)
        
        @tol_var_tdens.setter
        def tol_var_tdens(self, tol_var_tdens):
            _mockdt.f90wrap_ctrlprm__set__tol_var_tdens(self._handle, tol_var_tdens)
        
        @property
        def id_time_ctrl(self):
            """
            Element id_time_ctrl ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 79
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_time_ctrl(self._handle)
        
        @id_time_ctrl.setter
        def id_time_ctrl(self, id_time_ctrl):
            _mockdt.f90wrap_ctrlprm__set__id_time_ctrl(self._handle, id_time_ctrl)
        
        @property
        def deltat(self):
            """
            Element deltat ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 81
            
            """
            return _mockdt.f90wrap_ctrlprm__get__deltat(self._handle)
        
        @deltat.setter
        def deltat(self, deltat):
            _mockdt.f90wrap_ctrlprm__set__deltat(self._handle, deltat)
        
        @property
        def exp_rate(self):
            """
            Element exp_rate ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 83
            
            """
            return _mockdt.f90wrap_ctrlprm__get__exp_rate(self._handle)
        
        @exp_rate.setter
        def exp_rate(self, exp_rate):
            _mockdt.f90wrap_ctrlprm__set__exp_rate(self._handle, exp_rate)
        
        @property
        def upper_bound_deltat(self):
            """
            Element upper_bound_deltat ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 85
            
            """
            return _mockdt.f90wrap_ctrlprm__get__upper_bound_deltat(self._handle)
        
        @upper_bound_deltat.setter
        def upper_bound_deltat(self, upper_bound_deltat):
            _mockdt.f90wrap_ctrlprm__set__upper_bound_deltat(self._handle, \
                upper_bound_deltat)
        
        @property
        def lower_bound_deltat(self):
            """
            Element lower_bound_deltat ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 87
            
            """
            return _mockdt.f90wrap_ctrlprm__get__lower_bound_deltat(self._handle)
        
        @lower_bound_deltat.setter
        def lower_bound_deltat(self, lower_bound_deltat):
            _mockdt.f90wrap_ctrlprm__set__lower_bound_deltat(self._handle, \
                lower_bound_deltat)
        
        @property
        def krylov_scheme(self):
            """
            Element krylov_scheme ftype=character(len=20) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 96
            
            """
            return _mockdt.f90wrap_ctrlprm__get__krylov_scheme(self._handle)
        
        @krylov_scheme.setter
        def krylov_scheme(self, krylov_scheme):
            _mockdt.f90wrap_ctrlprm__set__krylov_scheme(self._handle, krylov_scheme)
        
        @property
        def lun_err(self):
            """
            Element lun_err ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 98
            
            """
            return _mockdt.f90wrap_ctrlprm__get__lun_err(self._handle)
        
        @lun_err.setter
        def lun_err(self, lun_err):
            _mockdt.f90wrap_ctrlprm__set__lun_err(self._handle, lun_err)
        
        @property
        def lun_out(self):
            """
            Element lun_out ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 100
            
            """
            return _mockdt.f90wrap_ctrlprm__get__lun_out(self._handle)
        
        @lun_out.setter
        def lun_out(self, lun_out):
            _mockdt.f90wrap_ctrlprm__set__lun_out(self._handle, lun_out)
        
        @property
        def iexit(self):
            """
            Element iexit ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 104
            
            """
            return _mockdt.f90wrap_ctrlprm__get__iexit(self._handle)
        
        @iexit.setter
        def iexit(self, iexit):
            _mockdt.f90wrap_ctrlprm__set__iexit(self._handle, iexit)
        
        @property
        def imax(self):
            """
            Element imax ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 106
            
            """
            return _mockdt.f90wrap_ctrlprm__get__imax(self._handle)
        
        @imax.setter
        def imax(self, imax):
            _mockdt.f90wrap_ctrlprm__set__imax(self._handle, imax)
        
        @property
        def iprt(self):
            """
            Element iprt ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 108
            
            """
            return _mockdt.f90wrap_ctrlprm__get__iprt(self._handle)
        
        @iprt.setter
        def iprt(self, iprt):
            _mockdt.f90wrap_ctrlprm__set__iprt(self._handle, iprt)
        
        @property
        def isol(self):
            """
            Element isol ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 112
            
            """
            return _mockdt.f90wrap_ctrlprm__get__isol(self._handle)
        
        @isol.setter
        def isol(self, isol):
            _mockdt.f90wrap_ctrlprm__set__isol(self._handle, isol)
        
        @property
        def tol_sol(self):
            """
            Element tol_sol ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 114
            
            """
            return _mockdt.f90wrap_ctrlprm__get__tol_sol(self._handle)
        
        @tol_sol.setter
        def tol_sol(self, tol_sol):
            _mockdt.f90wrap_ctrlprm__set__tol_sol(self._handle, tol_sol)
        
        @property
        def iort(self):
            """
            Element iort ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 117
            
            """
            return _mockdt.f90wrap_ctrlprm__get__iort(self._handle)
        
        @iort.setter
        def iort(self, iort):
            _mockdt.f90wrap_ctrlprm__set__iort(self._handle, iort)
        
        @property
        def nrestart(self):
            """
            Element nrestart ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 120
            
            """
            return _mockdt.f90wrap_ctrlprm__get__nrestart(self._handle)
        
        @nrestart.setter
        def nrestart(self, nrestart):
            _mockdt.f90wrap_ctrlprm__set__nrestart(self._handle, nrestart)
        
        @property
        def debug_solver(self):
            """
            Element debug_solver ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 124
            
            """
            return _mockdt.f90wrap_ctrlprm__get__debug_solver(self._handle)
        
        @debug_solver.setter
        def debug_solver(self, debug_solver):
            _mockdt.f90wrap_ctrlprm__set__debug_solver(self._handle, debug_solver)
        
        @property
        def solver_err(self):
            """
            Element solver_err ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 127
            
            """
            return _mockdt.f90wrap_ctrlprm__get__solver_err(self._handle)
        
        @solver_err.setter
        def solver_err(self, solver_err):
            _mockdt.f90wrap_ctrlprm__set__solver_err(self._handle, solver_err)
        
        @property
        def solver_out(self):
            """
            Element solver_out ftype=character(len=256) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 130
            
            """
            return _mockdt.f90wrap_ctrlprm__get__solver_out(self._handle)
        
        @solver_out.setter
        def solver_out(self, solver_out):
            _mockdt.f90wrap_ctrlprm__set__solver_out(self._handle, solver_out)
        
        @property
        def id_diagscale(self):
            """
            Element id_diagscale ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 132
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_diagscale(self._handle)
        
        @id_diagscale.setter
        def id_diagscale(self, id_diagscale):
            _mockdt.f90wrap_ctrlprm__set__id_diagscale(self._handle, id_diagscale)
        
        @property
        def lambda_(self):
            """
            Element lambda_ ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 134
            
            """
            return _mockdt.f90wrap_ctrlprm__get__lambda_(self._handle)
        
        @lambda_.setter
        def lambda_(self, lambda_):
            _mockdt.f90wrap_ctrlprm__set__lambda_(self._handle, lambda_)
        
        @property
        def alpha_uzawa(self):
            """
            Element alpha_uzawa ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 136
            
            """
            return _mockdt.f90wrap_ctrlprm__get__alpha_uzawa(self._handle)
        
        @alpha_uzawa.setter
        def alpha_uzawa(self, alpha_uzawa):
            _mockdt.f90wrap_ctrlprm__set__alpha_uzawa(self._handle, alpha_uzawa)
        
        @property
        def omega_uzawa(self):
            """
            Element omega_uzawa ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 138
            
            """
            return _mockdt.f90wrap_ctrlprm__get__omega_uzawa(self._handle)
        
        @omega_uzawa.setter
        def omega_uzawa(self, omega_uzawa):
            _mockdt.f90wrap_ctrlprm__set__omega_uzawa(self._handle, omega_uzawa)
        
        @property
        def prec_type(self):
            """
            Element prec_type ftype=character(len=20) pytype=str
            
            
            Defined at 89modControlParameters.fpp line 150
            
            """
            return _mockdt.f90wrap_ctrlprm__get__prec_type(self._handle)
        
        @prec_type.setter
        def prec_type(self, prec_type):
            _mockdt.f90wrap_ctrlprm__set__prec_type(self._handle, prec_type)
        
        @property
        def n_fillin(self):
            """
            Element n_fillin ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 153
            
            """
            return _mockdt.f90wrap_ctrlprm__get__n_fillin(self._handle)
        
        @n_fillin.setter
        def n_fillin(self, n_fillin):
            _mockdt.f90wrap_ctrlprm__set__n_fillin(self._handle, n_fillin)
        
        @property
        def tol_fillin(self):
            """
            Element tol_fillin ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 156
            
            """
            return _mockdt.f90wrap_ctrlprm__get__tol_fillin(self._handle)
        
        @tol_fillin.setter
        def tol_fillin(self, tol_fillin):
            _mockdt.f90wrap_ctrlprm__set__tol_fillin(self._handle, tol_fillin)
        
        @property
        def factorization_job(self):
            """
            Element factorization_job ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 161
            
            """
            return _mockdt.f90wrap_ctrlprm__get__factorization_job(self._handle)
        
        @factorization_job.setter
        def factorization_job(self, factorization_job):
            _mockdt.f90wrap_ctrlprm__set__factorization_job(self._handle, factorization_job)
        
        @property
        def max_bfgs(self):
            """
            Element max_bfgs ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 163
            
            """
            return _mockdt.f90wrap_ctrlprm__get__max_bfgs(self._handle)
        
        @max_bfgs.setter
        def max_bfgs(self, max_bfgs):
            _mockdt.f90wrap_ctrlprm__set__max_bfgs(self._handle, max_bfgs)
        
        @property
        def id_buffer_prec(self):
            """
            Element id_buffer_prec ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 172
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_buffer_prec(self._handle)
        
        @id_buffer_prec.setter
        def id_buffer_prec(self, id_buffer_prec):
            _mockdt.f90wrap_ctrlprm__set__id_buffer_prec(self._handle, id_buffer_prec)
        
        @property
        def ref_iter(self):
            """
            Element ref_iter ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 175
            
            """
            return _mockdt.f90wrap_ctrlprm__get__ref_iter(self._handle)
        
        @ref_iter.setter
        def ref_iter(self, ref_iter):
            _mockdt.f90wrap_ctrlprm__set__ref_iter(self._handle, ref_iter)
        
        @property
        def prec_growth(self):
            """
            Element prec_growth ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 177
            
            """
            return _mockdt.f90wrap_ctrlprm__get__prec_growth(self._handle)
        
        @prec_growth.setter
        def prec_growth(self, prec_growth):
            _mockdt.f90wrap_ctrlprm__set__prec_growth(self._handle, prec_growth)
        
        @property
        def relax2prec(self):
            """
            Element relax2prec ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 180
            
            """
            return _mockdt.f90wrap_ctrlprm__get__relax2prec(self._handle)
        
        @relax2prec.setter
        def relax2prec(self, relax2prec):
            _mockdt.f90wrap_ctrlprm__set__relax2prec(self._handle, relax2prec)
        
        @property
        def gamma(self):
            """
            Element gamma ftype=real(kind=double) pytype=float
            
            
            Defined at 89modControlParameters.fpp line 185
            
            """
            return _mockdt.f90wrap_ctrlprm__get__gamma(self._handle)
        
        @gamma.setter
        def gamma(self, gamma):
            _mockdt.f90wrap_ctrlprm__set__gamma(self._handle, gamma)
        
        @property
        def id_hats(self):
            """
            Element id_hats ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 187
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_hats(self._handle)
        
        @id_hats.setter
        def id_hats(self, id_hats):
            _mockdt.f90wrap_ctrlprm__set__id_hats(self._handle, id_hats)
        
        @property
        def nbroyden(self):
            """
            Element nbroyden ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 192
            
            """
            return _mockdt.f90wrap_ctrlprm__get__nbroyden(self._handle)
        
        @nbroyden.setter
        def nbroyden(self, nbroyden):
            _mockdt.f90wrap_ctrlprm__set__nbroyden(self._handle, nbroyden)
        
        @property
        def newton_method(self):
            """
            Element newton_method ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 196
            
            """
            return _mockdt.f90wrap_ctrlprm__get__newton_method(self._handle)
        
        @newton_method.setter
        def newton_method(self, newton_method):
            _mockdt.f90wrap_ctrlprm__set__newton_method(self._handle, newton_method)
        
        @property
        def reduced_jacobian(self):
            """
            Element reduced_jacobian ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 200
            
            """
            return _mockdt.f90wrap_ctrlprm__get__reduced_jacobian(self._handle)
        
        @reduced_jacobian.setter
        def reduced_jacobian(self, reduced_jacobian):
            _mockdt.f90wrap_ctrlprm__set__reduced_jacobian(self._handle, reduced_jacobian)
        
        @property
        def prec_newton(self):
            """
            Element prec_newton ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 210
            
            """
            return _mockdt.f90wrap_ctrlprm__get__prec_newton(self._handle)
        
        @prec_newton.setter
        def prec_newton(self, prec_newton):
            _mockdt.f90wrap_ctrlprm__set__prec_newton(self._handle, prec_newton)
        
        @property
        def inexact_newton(self):
            """
            Element inexact_newton ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 214
            
            """
            return _mockdt.f90wrap_ctrlprm__get__inexact_newton(self._handle)
        
        @inexact_newton.setter
        def inexact_newton(self, inexact_newton):
            _mockdt.f90wrap_ctrlprm__set__inexact_newton(self._handle, inexact_newton)
        
        @property
        def id_save_dat(self):
            """
            Element id_save_dat ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 224
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_save_dat(self._handle)
        
        @id_save_dat.setter
        def id_save_dat(self, id_save_dat):
            _mockdt.f90wrap_ctrlprm__set__id_save_dat(self._handle, id_save_dat)
        
        @property
        def freq_dat(self):
            """
            Element freq_dat ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 226
            
            """
            return _mockdt.f90wrap_ctrlprm__get__freq_dat(self._handle)
        
        @freq_dat.setter
        def freq_dat(self, freq_dat):
            _mockdt.f90wrap_ctrlprm__set__freq_dat(self._handle, freq_dat)
        
        @property
        def id_save_matrix(self):
            """
            Element id_save_matrix ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 228
            
            """
            return _mockdt.f90wrap_ctrlprm__get__id_save_matrix(self._handle)
        
        @id_save_matrix.setter
        def id_save_matrix(self, id_save_matrix):
            _mockdt.f90wrap_ctrlprm__set__id_save_matrix(self._handle, id_save_matrix)
        
        @property
        def freq_matrix(self):
            """
            Element freq_matrix ftype=integer  pytype=int
            
            
            Defined at 89modControlParameters.fpp line 230
            
            """
            return _mockdt.f90wrap_ctrlprm__get__freq_matrix(self._handle)
        
        @freq_matrix.setter
        def freq_matrix(self, freq_matrix):
            _mockdt.f90wrap_ctrlprm__set__freq_matrix(self._handle, freq_matrix)
        
        def __str__(self):
            ret = ['<ctrlprm>{\n']
            ret.append('    debug : ')
            ret.append(repr(self.debug))
            ret.append(',\n    info_state : ')
            ret.append(repr(self.info_state))
            ret.append(',\n    info_inputs_update : ')
            ret.append(repr(self.info_inputs_update))
            ret.append(',\n    info_newton : ')
            ret.append(repr(self.info_newton))
            ret.append(',\n    iformat : ')
            ret.append(repr(self.iformat))
            ret.append(',\n    rformat : ')
            ret.append(repr(self.rformat))
            ret.append(',\n    iformat_info : ')
            ret.append(repr(self.iformat_info))
            ret.append(',\n    rformat_info : ')
            ret.append(repr(self.rformat_info))
            ret.append(',\n    id_subgrid : ')
            ret.append(repr(self.id_subgrid))
            ret.append(',\n    id_ode : ')
            ret.append(repr(self.id_ode))
            ret.append(',\n    min_tdens : ')
            ret.append(repr(self.min_tdens))
            ret.append(',\n    threshold_tdens : ')
            ret.append(repr(self.threshold_tdens))
            ret.append(',\n    id_time_discr : ')
            ret.append(repr(self.id_time_discr))
            ret.append(',\n    tol_nonlinear : ')
            ret.append(repr(self.tol_nonlinear))
            ret.append(',\n    max_iteration_nonlinear : ')
            ret.append(repr(self.max_iteration_nonlinear))
            ret.append(',\n    nrestart_max : ')
            ret.append(repr(self.nrestart_max))
            ret.append(',\n    max_nrestart_invert_jacobian : ')
            ret.append(repr(self.max_nrestart_invert_jacobian))
            ret.append(',\n    tzero : ')
            ret.append(repr(self.tzero))
            ret.append(',\n    max_time_it : ')
            ret.append(repr(self.max_time_it))
            ret.append(',\n    tol_var_tdens : ')
            ret.append(repr(self.tol_var_tdens))
            ret.append(',\n    id_time_ctrl : ')
            ret.append(repr(self.id_time_ctrl))
            ret.append(',\n    deltat : ')
            ret.append(repr(self.deltat))
            ret.append(',\n    exp_rate : ')
            ret.append(repr(self.exp_rate))
            ret.append(',\n    upper_bound_deltat : ')
            ret.append(repr(self.upper_bound_deltat))
            ret.append(',\n    lower_bound_deltat : ')
            ret.append(repr(self.lower_bound_deltat))
            ret.append(',\n    krylov_scheme : ')
            ret.append(repr(self.krylov_scheme))
            ret.append(',\n    lun_err : ')
            ret.append(repr(self.lun_err))
            ret.append(',\n    lun_out : ')
            ret.append(repr(self.lun_out))
            ret.append(',\n    iexit : ')
            ret.append(repr(self.iexit))
            ret.append(',\n    imax : ')
            ret.append(repr(self.imax))
            ret.append(',\n    iprt : ')
            ret.append(repr(self.iprt))
            ret.append(',\n    isol : ')
            ret.append(repr(self.isol))
            ret.append(',\n    tol_sol : ')
            ret.append(repr(self.tol_sol))
            ret.append(',\n    iort : ')
            ret.append(repr(self.iort))
            ret.append(',\n    nrestart : ')
            ret.append(repr(self.nrestart))
            ret.append(',\n    debug_solver : ')
            ret.append(repr(self.debug_solver))
            ret.append(',\n    solver_err : ')
            ret.append(repr(self.solver_err))
            ret.append(',\n    solver_out : ')
            ret.append(repr(self.solver_out))
            ret.append(',\n    id_diagscale : ')
            ret.append(repr(self.id_diagscale))
            ret.append(',\n    lambda_ : ')
            ret.append(repr(self.lambda_))
            ret.append(',\n    alpha_uzawa : ')
            ret.append(repr(self.alpha_uzawa))
            ret.append(',\n    omega_uzawa : ')
            ret.append(repr(self.omega_uzawa))
            ret.append(',\n    prec_type : ')
            ret.append(repr(self.prec_type))
            ret.append(',\n    n_fillin : ')
            ret.append(repr(self.n_fillin))
            ret.append(',\n    tol_fillin : ')
            ret.append(repr(self.tol_fillin))
            ret.append(',\n    factorization_job : ')
            ret.append(repr(self.factorization_job))
            ret.append(',\n    max_bfgs : ')
            ret.append(repr(self.max_bfgs))
            ret.append(',\n    id_buffer_prec : ')
            ret.append(repr(self.id_buffer_prec))
            ret.append(',\n    ref_iter : ')
            ret.append(repr(self.ref_iter))
            ret.append(',\n    prec_growth : ')
            ret.append(repr(self.prec_growth))
            ret.append(',\n    relax2prec : ')
            ret.append(repr(self.relax2prec))
            ret.append(',\n    gamma : ')
            ret.append(repr(self.gamma))
            ret.append(',\n    id_hats : ')
            ret.append(repr(self.id_hats))
            ret.append(',\n    nbroyden : ')
            ret.append(repr(self.nbroyden))
            ret.append(',\n    newton_method : ')
            ret.append(repr(self.newton_method))
            ret.append(',\n    reduced_jacobian : ')
            ret.append(repr(self.reduced_jacobian))
            ret.append(',\n    prec_newton : ')
            ret.append(repr(self.prec_newton))
            ret.append(',\n    inexact_newton : ')
            ret.append(repr(self.inexact_newton))
            ret.append(',\n    id_save_dat : ')
            ret.append(repr(self.id_save_dat))
            ret.append(',\n    freq_dat : ')
            ret.append(repr(self.freq_dat))
            ret.append(',\n    id_save_matrix : ')
            ret.append(repr(self.id_save_matrix))
            ret.append(',\n    freq_matrix : ')
            ret.append(repr(self.freq_matrix))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    _dt_array_initialisers = []
    

controlparameters = Controlparameters()

class Tdenspotentialsystem(f90wrap.runtime.FortranModule):
    """
    Module tdenspotentialsystem
    
    
    Defined at 90modTdensPotentialSystem.fpp lines 5-544
    
    """
    @f90wrap.runtime.register_class("mockdt.tdpotsys")
    class tdpotsys(f90wrap.runtime.FortranDerivedType):
        """
        Type(name=tdpotsys)
        
        
        Defined at 90modTdensPotentialSystem.fpp lines 28-218
        
        """
        def __init__(self, handle=None):
            """
            self = Tdpotsys()
            
            
            Defined at 90modTdensPotentialSystem.fpp lines 28-218
            
            
            Returns
            -------
            this : Tdpotsys
            	Object to be constructed
            
            
            Automatically generated constructor for tdpotsys
            """
            f90wrap.runtime.FortranDerivedType.__init__(self)
            result = _mockdt.f90wrap_tdpotsys_initialise()
            self._handle = result[0] if isinstance(result, tuple) else result
        
        def __del__(self):
            """
            Destructor for class Tdpotsys
            
            
            Defined at 90modTdensPotentialSystem.fpp lines 28-218
            
            Parameters
            ----------
            this : Tdpotsys
            	Object to be destructed
            
            
            Automatically generated destructor for tdpotsys
            """
            if self._alloc:
                _mockdt.f90wrap_tdpotsys_finalise(this=self._handle)
        
        @property
        def ntdens(self):
            """
            Element ntdens ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 31
            
            """
            return _mockdt.f90wrap_tdpotsys__get__ntdens(self._handle)
        
        @ntdens.setter
        def ntdens(self, ntdens):
            _mockdt.f90wrap_tdpotsys__set__ntdens(self._handle, ntdens)
        
        @property
        def npot(self):
            """
            Element npot ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 33
            
            """
            return _mockdt.f90wrap_tdpotsys__get__npot(self._handle)
        
        @npot.setter
        def npot(self, npot):
            _mockdt.f90wrap_tdpotsys__set__npot(self._handle, npot)
        
        @property
        def nfull(self):
            """
            Element nfull ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 35
            
            """
            return _mockdt.f90wrap_tdpotsys__get__nfull(self._handle)
        
        @nfull.setter
        def nfull(self, nfull):
            _mockdt.f90wrap_tdpotsys__set__nfull(self._handle, nfull)
        
        @property
        def tdens(self):
            """
            Element tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 38
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__tdens(self._handle)
            if array_handle in self._arrays:
                tdens = self._arrays[array_handle]
            else:
                tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__tdens)
                self._arrays[array_handle] = tdens
            return tdens
        
        @tdens.setter
        def tdens(self, tdens):
            self.tdens[...] = tdens
        
        @property
        def tdens_old(self):
            """
            Element tdens_old ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 41
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__tdens_old(self._handle)
            if array_handle in self._arrays:
                tdens_old = self._arrays[array_handle]
            else:
                tdens_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__tdens_old)
                self._arrays[array_handle] = tdens_old
            return tdens_old
        
        @tdens_old.setter
        def tdens_old(self, tdens_old):
            self.tdens_old[...] = tdens_old
        
        @property
        def pot(self):
            """
            Element pot ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 44
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__pot(self._handle)
            if array_handle in self._arrays:
                pot = self._arrays[array_handle]
            else:
                pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__pot)
                self._arrays[array_handle] = pot
            return pot
        
        @pot.setter
        def pot(self, pot):
            self.pot[...] = pot
        
        @property
        def pot_old(self):
            """
            Element pot_old ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 47
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__pot_old(self._handle)
            if array_handle in self._arrays:
                pot_old = self._arrays[array_handle]
            else:
                pot_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__pot_old)
                self._arrays[array_handle] = pot_old
            return pot_old
        
        @pot_old.setter
        def pot_old(self, pot_old):
            self.pot_old[...] = pot_old
        
        @property
        def gfvar(self):
            """
            Element gfvar ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 50
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__gfvar(self._handle)
            if array_handle in self._arrays:
                gfvar = self._arrays[array_handle]
            else:
                gfvar = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__gfvar)
                self._arrays[array_handle] = gfvar
            return gfvar
        
        @gfvar.setter
        def gfvar(self, gfvar):
            self.gfvar[...] = gfvar
        
        @property
        def gfvar_old(self):
            """
            Element gfvar_old ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 53
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__gfvar_old(self._handle)
            if array_handle in self._arrays:
                gfvar_old = self._arrays[array_handle]
            else:
                gfvar_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__gfvar_old)
                self._arrays[array_handle] = gfvar_old
            return gfvar_old
        
        @gfvar_old.setter
        def gfvar_old(self, gfvar_old):
            self.gfvar_old[...] = gfvar_old
        
        @property
        def tdpot_syncr(self):
            """
            Element tdpot_syncr ftype=logical pytype=bool
            
            
            Defined at 90modTdensPotentialSystem.fpp line 58
            
            """
            return _mockdt.f90wrap_tdpotsys__get__tdpot_syncr(self._handle)
        
        @tdpot_syncr.setter
        def tdpot_syncr(self, tdpot_syncr):
            _mockdt.f90wrap_tdpotsys__set__tdpot_syncr(self._handle, tdpot_syncr)
        
        @property
        def all_syncr(self):
            """
            Element all_syncr ftype=logical pytype=bool
            
            
            Defined at 90modTdensPotentialSystem.fpp line 62
            
            """
            return _mockdt.f90wrap_tdpotsys__get__all_syncr(self._handle)
        
        @all_syncr.setter
        def all_syncr(self, all_syncr):
            _mockdt.f90wrap_tdpotsys__set__all_syncr(self._handle, all_syncr)
        
        @property
        def time_iteration(self):
            """
            Element time_iteration ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 67
            
            """
            return _mockdt.f90wrap_tdpotsys__get__time_iteration(self._handle)
        
        @time_iteration.setter
        def time_iteration(self, time_iteration):
            _mockdt.f90wrap_tdpotsys__set__time_iteration(self._handle, time_iteration)
        
        @property
        def current_time(self):
            """
            Element current_time ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 69
            
            """
            return _mockdt.f90wrap_tdpotsys__get__current_time(self._handle)
        
        @current_time.setter
        def current_time(self, current_time):
            _mockdt.f90wrap_tdpotsys__set__current_time(self._handle, current_time)
        
        @property
        def info_prec(self):
            """
            Element info_prec ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 73
            
            """
            return _mockdt.f90wrap_tdpotsys__get__info_prec(self._handle)
        
        @info_prec.setter
        def info_prec(self, info_prec):
            _mockdt.f90wrap_tdpotsys__set__info_prec(self._handle, info_prec)
        
        @property
        def nlinear_system(self):
            """
            Element nlinear_system ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 75
            
            """
            return _mockdt.f90wrap_tdpotsys__get__nlinear_system(self._handle)
        
        @nlinear_system.setter
        def nlinear_system(self, nlinear_system):
            _mockdt.f90wrap_tdpotsys__set__nlinear_system(self._handle, nlinear_system)
        
        @property
        def iter_media(self):
            """
            Element iter_media ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 79
            
            """
            return _mockdt.f90wrap_tdpotsys__get__iter_media(self._handle)
        
        @iter_media.setter
        def iter_media(self, iter_media):
            _mockdt.f90wrap_tdpotsys__set__iter_media(self._handle, iter_media)
        
        @property
        def total_iteration(self):
            """
            Element total_iteration ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 81
            
            """
            return _mockdt.f90wrap_tdpotsys__get__total_iteration(self._handle)
        
        @total_iteration.setter
        def total_iteration(self, total_iteration):
            _mockdt.f90wrap_tdpotsys__set__total_iteration(self._handle, total_iteration)
        
        @property
        def iter_last_prec(self):
            """
            Element iter_last_prec ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 83
            
            """
            return _mockdt.f90wrap_tdpotsys__get__iter_last_prec(self._handle)
        
        @iter_last_prec.setter
        def iter_last_prec(self, iter_last_prec):
            _mockdt.f90wrap_tdpotsys__set__iter_last_prec(self._handle, iter_last_prec)
        
        @property
        def iter_first_system(self):
            """
            Element iter_first_system ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 85
            
            """
            return _mockdt.f90wrap_tdpotsys__get__iter_first_system(self._handle)
        
        @iter_first_system.setter
        def iter_first_system(self, iter_first_system):
            _mockdt.f90wrap_tdpotsys__set__iter_first_system(self._handle, \
                iter_first_system)
        
        @property
        def total_iterations_linear_system(self):
            """
            Element total_iterations_linear_system ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 87
            
            """
            return \
                _mockdt.f90wrap_tdpotsys__get__total_iterations_linear_system(self._handle)
        
        @total_iterations_linear_system.setter
        def total_iterations_linear_system(self, total_iterations_linear_system):
            _mockdt.f90wrap_tdpotsys__set__total_iterations_linear_system(self._handle, \
                total_iterations_linear_system)
        
        @property
        def total_number_linear_system(self):
            """
            Element total_number_linear_system ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 89
            
            """
            return _mockdt.f90wrap_tdpotsys__get__total_number_linear_system(self._handle)
        
        @total_number_linear_system.setter
        def total_number_linear_system(self, total_number_linear_system):
            _mockdt.f90wrap_tdpotsys__set__total_number_linear_system(self._handle, \
                total_number_linear_system)
        
        @property
        def itemp_last_prec(self):
            """
            Element itemp_last_prec ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 91
            
            """
            return _mockdt.f90wrap_tdpotsys__get__itemp_last_prec(self._handle)
        
        @itemp_last_prec.setter
        def itemp_last_prec(self, itemp_last_prec):
            _mockdt.f90wrap_tdpotsys__set__itemp_last_prec(self._handle, itemp_last_prec)
        
        @property
        def ref_iter(self):
            """
            Element ref_iter ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 93
            
            """
            return _mockdt.f90wrap_tdpotsys__get__ref_iter(self._handle)
        
        @ref_iter.setter
        def ref_iter(self, ref_iter):
            _mockdt.f90wrap_tdpotsys__set__ref_iter(self._handle, ref_iter)
        
        @property
        def iter_newton_last_prec(self):
            """
            Element iter_newton_last_prec ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 96
            
            """
            return _mockdt.f90wrap_tdpotsys__get__iter_newton_last_prec(self._handle)
        
        @iter_newton_last_prec.setter
        def iter_newton_last_prec(self, iter_newton_last_prec):
            _mockdt.f90wrap_tdpotsys__set__iter_newton_last_prec(self._handle, \
                iter_newton_last_prec)
        
        @property
        def nrestart_newton(self):
            """
            Element nrestart_newton ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 98
            
            """
            return _mockdt.f90wrap_tdpotsys__get__nrestart_newton(self._handle)
        
        @nrestart_newton.setter
        def nrestart_newton(self, nrestart_newton):
            _mockdt.f90wrap_tdpotsys__set__nrestart_newton(self._handle, nrestart_newton)
        
        @property
        def nrestart_invert_jacobian(self):
            """
            Element nrestart_invert_jacobian ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 100
            
            """
            return _mockdt.f90wrap_tdpotsys__get__nrestart_invert_jacobian(self._handle)
        
        @nrestart_invert_jacobian.setter
        def nrestart_invert_jacobian(self, nrestart_invert_jacobian):
            _mockdt.f90wrap_tdpotsys__set__nrestart_invert_jacobian(self._handle, \
                nrestart_invert_jacobian)
        
        @property
        def res_elliptic(self):
            """
            Element res_elliptic ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 103
            
            """
            return _mockdt.f90wrap_tdpotsys__get__res_elliptic(self._handle)
        
        @res_elliptic.setter
        def res_elliptic(self, res_elliptic):
            _mockdt.f90wrap_tdpotsys__set__res_elliptic(self._handle, res_elliptic)
        
        @property
        def mass_tdens(self):
            """
            Element mass_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 109
            
            """
            return _mockdt.f90wrap_tdpotsys__get__mass_tdens(self._handle)
        
        @mass_tdens.setter
        def mass_tdens(self, mass_tdens):
            _mockdt.f90wrap_tdpotsys__set__mass_tdens(self._handle, mass_tdens)
        
        @property
        def weighted_mass_tdens(self):
            """
            Element weighted_mass_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 112
            
            """
            return _mockdt.f90wrap_tdpotsys__get__weighted_mass_tdens(self._handle)
        
        @weighted_mass_tdens.setter
        def weighted_mass_tdens(self, weighted_mass_tdens):
            _mockdt.f90wrap_tdpotsys__set__weighted_mass_tdens(self._handle, \
                weighted_mass_tdens)
        
        @property
        def energy(self):
            """
            Element energy ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 115
            
            """
            return _mockdt.f90wrap_tdpotsys__get__energy(self._handle)
        
        @energy.setter
        def energy(self, energy):
            _mockdt.f90wrap_tdpotsys__set__energy(self._handle, energy)
        
        @property
        def lyapunov(self):
            """
            Element lyapunov ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 118
            
            """
            return _mockdt.f90wrap_tdpotsys__get__lyapunov(self._handle)
        
        @lyapunov.setter
        def lyapunov(self, lyapunov):
            _mockdt.f90wrap_tdpotsys__set__lyapunov(self._handle, lyapunov)
        
        @property
        def min_tdens(self):
            """
            Element min_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 121
            
            """
            return _mockdt.f90wrap_tdpotsys__get__min_tdens(self._handle)
        
        @min_tdens.setter
        def min_tdens(self, min_tdens):
            _mockdt.f90wrap_tdpotsys__set__min_tdens(self._handle, min_tdens)
        
        @property
        def max_tdens(self):
            """
            Element max_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 124
            
            """
            return _mockdt.f90wrap_tdpotsys__get__max_tdens(self._handle)
        
        @max_tdens.setter
        def max_tdens(self, max_tdens):
            _mockdt.f90wrap_tdpotsys__set__max_tdens(self._handle, max_tdens)
        
        @property
        def max_velocity(self):
            """
            Element max_velocity ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 127
            
            """
            return _mockdt.f90wrap_tdpotsys__get__max_velocity(self._handle)
        
        @max_velocity.setter
        def max_velocity(self, max_velocity):
            _mockdt.f90wrap_tdpotsys__set__max_velocity(self._handle, max_velocity)
        
        @property
        def max_nrm_grad(self):
            """
            Element max_nrm_grad ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 130
            
            """
            return _mockdt.f90wrap_tdpotsys__get__max_nrm_grad(self._handle)
        
        @max_nrm_grad.setter
        def max_nrm_grad(self, max_nrm_grad):
            _mockdt.f90wrap_tdpotsys__set__max_nrm_grad(self._handle, max_nrm_grad)
        
        @property
        def max_nrm_grad_avg(self):
            """
            Element max_nrm_grad_avg ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 133
            
            """
            return _mockdt.f90wrap_tdpotsys__get__max_nrm_grad_avg(self._handle)
        
        @max_nrm_grad_avg.setter
        def max_nrm_grad_avg(self, max_nrm_grad_avg):
            _mockdt.f90wrap_tdpotsys__set__max_nrm_grad_avg(self._handle, max_nrm_grad_avg)
        
        @property
        def max_d3(self):
            """
            Element max_d3 ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 136
            
            """
            return _mockdt.f90wrap_tdpotsys__get__max_d3(self._handle)
        
        @max_d3.setter
        def max_d3(self, max_d3):
            _mockdt.f90wrap_tdpotsys__set__max_d3(self._handle, max_d3)
        
        @property
        def integral_flux_pvel(self):
            """
            Element integral_flux_pvel ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 140
            
            """
            return _mockdt.f90wrap_tdpotsys__get__integral_flux_pvel(self._handle)
        
        @integral_flux_pvel.setter
        def integral_flux_pvel(self, integral_flux_pvel):
            _mockdt.f90wrap_tdpotsys__set__integral_flux_pvel(self._handle, \
                integral_flux_pvel)
        
        @property
        def duality_gap(self):
            """
            Element duality_gap ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 143
            
            """
            return _mockdt.f90wrap_tdpotsys__get__duality_gap(self._handle)
        
        @duality_gap.setter
        def duality_gap(self, duality_gap):
            _mockdt.f90wrap_tdpotsys__set__duality_gap(self._handle, duality_gap)
        
        @property
        def err_tdens(self):
            """
            Element err_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 146
            
            """
            return _mockdt.f90wrap_tdpotsys__get__err_tdens(self._handle)
        
        @err_tdens.setter
        def err_tdens(self, err_tdens):
            _mockdt.f90wrap_tdpotsys__set__err_tdens(self._handle, err_tdens)
        
        @property
        def err_pot(self):
            """
            Element err_pot ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 149
            
            """
            return _mockdt.f90wrap_tdpotsys__get__err_pot(self._handle)
        
        @err_pot.setter
        def err_pot(self, err_pot):
            _mockdt.f90wrap_tdpotsys__set__err_pot(self._handle, err_pot)
        
        @property
        def err_wasserstein(self):
            """
            Element err_wasserstein ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 152
            
            """
            return _mockdt.f90wrap_tdpotsys__get__err_wasserstein(self._handle)
        
        @err_wasserstein.setter
        def err_wasserstein(self, err_wasserstein):
            _mockdt.f90wrap_tdpotsys__set__err_wasserstein(self._handle, err_wasserstein)
        
        @property
        def wasserstein_distance(self):
            """
            Element wasserstein_distance ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 155
            
            """
            return _mockdt.f90wrap_tdpotsys__get__wasserstein_distance(self._handle)
        
        @wasserstein_distance.setter
        def wasserstein_distance(self, wasserstein_distance):
            _mockdt.f90wrap_tdpotsys__set__wasserstein_distance(self._handle, \
                wasserstein_distance)
        
        @property
        def iter_nonlinear(self):
            """
            Element iter_nonlinear ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 159
            
            """
            return _mockdt.f90wrap_tdpotsys__get__iter_nonlinear(self._handle)
        
        @iter_nonlinear.setter
        def iter_nonlinear(self, iter_nonlinear):
            _mockdt.f90wrap_tdpotsys__set__iter_nonlinear(self._handle, iter_nonlinear)
        
        @property
        def loc_var_tdens(self):
            """
            Element loc_var_tdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 164
            
            """
            return _mockdt.f90wrap_tdpotsys__get__loc_var_tdens(self._handle)
        
        @loc_var_tdens.setter
        def loc_var_tdens(self, loc_var_tdens):
            _mockdt.f90wrap_tdpotsys__set__loc_var_tdens(self._handle, loc_var_tdens)
        
        @property
        def scr_ntdens(self):
            """
            Element scr_ntdens ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 170
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__scr_ntdens(self._handle)
            if array_handle in self._arrays:
                scr_ntdens = self._arrays[array_handle]
            else:
                scr_ntdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__scr_ntdens)
                self._arrays[array_handle] = scr_ntdens
            return scr_ntdens
        
        @scr_ntdens.setter
        def scr_ntdens(self, scr_ntdens):
            self.scr_ntdens[...] = scr_ntdens
        
        @property
        def scr_npot(self):
            """
            Element scr_npot ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 173
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__scr_npot(self._handle)
            if array_handle in self._arrays:
                scr_npot = self._arrays[array_handle]
            else:
                scr_npot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__scr_npot)
                self._arrays[array_handle] = scr_npot
            return scr_npot
        
        @scr_npot.setter
        def scr_npot(self, scr_npot):
            self.scr_npot[...] = scr_npot
        
        @property
        def scr_nfull(self):
            """
            Element scr_nfull ftype=real(kind=double) pytype=float
            
            
            Defined at 90modTdensPotentialSystem.fpp line 176
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__scr_nfull(self._handle)
            if array_handle in self._arrays:
                scr_nfull = self._arrays[array_handle]
            else:
                scr_nfull = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__scr_nfull)
                self._arrays[array_handle] = scr_nfull
            return scr_nfull
        
        @scr_nfull.setter
        def scr_nfull(self, scr_nfull):
            self.scr_nfull[...] = scr_nfull
        
        @property
        def ntdens_on(self):
            """
            Element ntdens_on ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 181
            
            """
            return _mockdt.f90wrap_tdpotsys__get__ntdens_on(self._handle)
        
        @ntdens_on.setter
        def ntdens_on(self, ntdens_on):
            _mockdt.f90wrap_tdpotsys__set__ntdens_on(self._handle, ntdens_on)
        
        @property
        def npot_on(self):
            """
            Element npot_on ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 183
            
            """
            return _mockdt.f90wrap_tdpotsys__get__npot_on(self._handle)
        
        @npot_on.setter
        def npot_on(self, npot_on):
            _mockdt.f90wrap_tdpotsys__set__npot_on(self._handle, npot_on)
        
        @property
        def ntdens_off(self):
            """
            Element ntdens_off ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 185
            
            """
            return _mockdt.f90wrap_tdpotsys__get__ntdens_off(self._handle)
        
        @ntdens_off.setter
        def ntdens_off(self, ntdens_off):
            _mockdt.f90wrap_tdpotsys__set__ntdens_off(self._handle, ntdens_off)
        
        @property
        def npot_off(self):
            """
            Element npot_off ftype=integer  pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 187
            
            """
            return _mockdt.f90wrap_tdpotsys__get__npot_off(self._handle)
        
        @npot_off.setter
        def npot_off(self, npot_off):
            _mockdt.f90wrap_tdpotsys__set__npot_off(self._handle, npot_off)
        
        @property
        def active_tdens(self):
            """
            Element active_tdens ftype=integer pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 190
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__active_tdens(self._handle)
            if array_handle in self._arrays:
                active_tdens = self._arrays[array_handle]
            else:
                active_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__active_tdens)
                self._arrays[array_handle] = active_tdens
            return active_tdens
        
        @active_tdens.setter
        def active_tdens(self, active_tdens):
            self.active_tdens[...] = active_tdens
        
        @property
        def active_pot(self):
            """
            Element active_pot ftype=integer pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 193
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__active_pot(self._handle)
            if array_handle in self._arrays:
                active_pot = self._arrays[array_handle]
            else:
                active_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__active_pot)
                self._arrays[array_handle] = active_pot
            return active_pot
        
        @active_pot.setter
        def active_pot(self, active_pot):
            self.active_pot[...] = active_pot
        
        @property
        def inactive_tdens(self):
            """
            Element inactive_tdens ftype=integer pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 196
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__inactive_tdens(self._handle)
            if array_handle in self._arrays:
                inactive_tdens = self._arrays[array_handle]
            else:
                inactive_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__inactive_tdens)
                self._arrays[array_handle] = inactive_tdens
            return inactive_tdens
        
        @inactive_tdens.setter
        def inactive_tdens(self, inactive_tdens):
            self.inactive_tdens[...] = inactive_tdens
        
        @property
        def inactive_pot(self):
            """
            Element inactive_pot ftype=integer pytype=int
            
            
            Defined at 90modTdensPotentialSystem.fpp line 199
            
            """
            array_ndim, array_type, array_shape, array_handle = \
                _mockdt.f90wrap_tdpotsys__array__inactive_pot(self._handle)
            if array_handle in self._arrays:
                inactive_pot = self._arrays[array_handle]
            else:
                inactive_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                        self._handle,
                                        _mockdt.f90wrap_tdpotsys__array__inactive_pot)
                self._arrays[array_handle] = inactive_pot
            return inactive_pot
        
        @inactive_pot.setter
        def inactive_pot(self, inactive_pot):
            self.inactive_pot[...] = inactive_pot
        
        def __str__(self):
            ret = ['<tdpotsys>{\n']
            ret.append('    ntdens : ')
            ret.append(repr(self.ntdens))
            ret.append(',\n    npot : ')
            ret.append(repr(self.npot))
            ret.append(',\n    nfull : ')
            ret.append(repr(self.nfull))
            ret.append(',\n    tdens : ')
            ret.append(repr(self.tdens))
            ret.append(',\n    tdens_old : ')
            ret.append(repr(self.tdens_old))
            ret.append(',\n    pot : ')
            ret.append(repr(self.pot))
            ret.append(',\n    pot_old : ')
            ret.append(repr(self.pot_old))
            ret.append(',\n    gfvar : ')
            ret.append(repr(self.gfvar))
            ret.append(',\n    gfvar_old : ')
            ret.append(repr(self.gfvar_old))
            ret.append(',\n    tdpot_syncr : ')
            ret.append(repr(self.tdpot_syncr))
            ret.append(',\n    all_syncr : ')
            ret.append(repr(self.all_syncr))
            ret.append(',\n    time_iteration : ')
            ret.append(repr(self.time_iteration))
            ret.append(',\n    current_time : ')
            ret.append(repr(self.current_time))
            ret.append(',\n    info_prec : ')
            ret.append(repr(self.info_prec))
            ret.append(',\n    nlinear_system : ')
            ret.append(repr(self.nlinear_system))
            ret.append(',\n    iter_media : ')
            ret.append(repr(self.iter_media))
            ret.append(',\n    total_iteration : ')
            ret.append(repr(self.total_iteration))
            ret.append(',\n    iter_last_prec : ')
            ret.append(repr(self.iter_last_prec))
            ret.append(',\n    iter_first_system : ')
            ret.append(repr(self.iter_first_system))
            ret.append(',\n    total_iterations_linear_system : ')
            ret.append(repr(self.total_iterations_linear_system))
            ret.append(',\n    total_number_linear_system : ')
            ret.append(repr(self.total_number_linear_system))
            ret.append(',\n    itemp_last_prec : ')
            ret.append(repr(self.itemp_last_prec))
            ret.append(',\n    ref_iter : ')
            ret.append(repr(self.ref_iter))
            ret.append(',\n    iter_newton_last_prec : ')
            ret.append(repr(self.iter_newton_last_prec))
            ret.append(',\n    nrestart_newton : ')
            ret.append(repr(self.nrestart_newton))
            ret.append(',\n    nrestart_invert_jacobian : ')
            ret.append(repr(self.nrestart_invert_jacobian))
            ret.append(',\n    res_elliptic : ')
            ret.append(repr(self.res_elliptic))
            ret.append(',\n    mass_tdens : ')
            ret.append(repr(self.mass_tdens))
            ret.append(',\n    weighted_mass_tdens : ')
            ret.append(repr(self.weighted_mass_tdens))
            ret.append(',\n    energy : ')
            ret.append(repr(self.energy))
            ret.append(',\n    lyapunov : ')
            ret.append(repr(self.lyapunov))
            ret.append(',\n    min_tdens : ')
            ret.append(repr(self.min_tdens))
            ret.append(',\n    max_tdens : ')
            ret.append(repr(self.max_tdens))
            ret.append(',\n    max_velocity : ')
            ret.append(repr(self.max_velocity))
            ret.append(',\n    max_nrm_grad : ')
            ret.append(repr(self.max_nrm_grad))
            ret.append(',\n    max_nrm_grad_avg : ')
            ret.append(repr(self.max_nrm_grad_avg))
            ret.append(',\n    max_d3 : ')
            ret.append(repr(self.max_d3))
            ret.append(',\n    integral_flux_pvel : ')
            ret.append(repr(self.integral_flux_pvel))
            ret.append(',\n    duality_gap : ')
            ret.append(repr(self.duality_gap))
            ret.append(',\n    err_tdens : ')
            ret.append(repr(self.err_tdens))
            ret.append(',\n    err_pot : ')
            ret.append(repr(self.err_pot))
            ret.append(',\n    err_wasserstein : ')
            ret.append(repr(self.err_wasserstein))
            ret.append(',\n    wasserstein_distance : ')
            ret.append(repr(self.wasserstein_distance))
            ret.append(',\n    iter_nonlinear : ')
            ret.append(repr(self.iter_nonlinear))
            ret.append(',\n    loc_var_tdens : ')
            ret.append(repr(self.loc_var_tdens))
            ret.append(',\n    scr_ntdens : ')
            ret.append(repr(self.scr_ntdens))
            ret.append(',\n    scr_npot : ')
            ret.append(repr(self.scr_npot))
            ret.append(',\n    scr_nfull : ')
            ret.append(repr(self.scr_nfull))
            ret.append(',\n    ntdens_on : ')
            ret.append(repr(self.ntdens_on))
            ret.append(',\n    npot_on : ')
            ret.append(repr(self.npot_on))
            ret.append(',\n    ntdens_off : ')
            ret.append(repr(self.ntdens_off))
            ret.append(',\n    npot_off : ')
            ret.append(repr(self.npot_off))
            ret.append(',\n    active_tdens : ')
            ret.append(repr(self.active_tdens))
            ret.append(',\n    active_pot : ')
            ret.append(repr(self.active_pot))
            ret.append(',\n    inactive_tdens : ')
            ret.append(repr(self.inactive_tdens))
            ret.append(',\n    inactive_pot : ')
            ret.append(repr(self.inactive_pot))
            ret.append('}')
            return ''.join(ret)
        
        _dt_array_initialisers = []
        
    
    @staticmethod
    def init_tdpotsys(self, lun_err, lun_out, lun_stat, ntdens, npot):
        """
        init_tdpotsys(self, lun_err, lun_out, lun_stat, ntdens, npot)
        
        
        Defined at 90modTdensPotentialSystem.fpp lines 254-333
        
        Parameters
        ----------
        this : Tdpotsys
        lun_err : int
        lun_out : int
        lun_stat : int
        ntdens : int
        npot : int
        
        """
        _mockdt.f90wrap_init_tdpotsys(this=self._handle, lun_err=lun_err, \
            lun_out=lun_out, lun_stat=lun_stat, ntdens=ntdens, npot=npot)
    
    _dt_array_initialisers = []
    

tdenspotentialsystem = Tdenspotentialsystem()

def top_level(in_):
    """
    out = top_level(in_)
    
    
    Defined at define.fpp lines 54-58
    
    Parameters
    ----------
    in_ : float
    
    Returns
    -------
    out : float
    
    Example of a top-level subroutine.
    """
    out = _mockdt.f90wrap_top_level(in_=in_)
    return out

