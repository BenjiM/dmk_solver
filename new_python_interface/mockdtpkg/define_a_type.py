"""
Module define_a_type


Defined at define.fpp lines 5-51

Define a type which is referenced by the primary used type, to make
sure it is also wrapped (and we can access its elements).
"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging
from mockdtpkg.leveltwomod import leveltwo

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("mockdtpkg.atype")
class atype(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=atype)
    
    
    Defined at define.fpp lines 9-16
    
    """
    def __init__(self, handle=None):
        """
        self = Atype()
        
        
        Defined at define.fpp lines 9-16
        
        
        Returns
        -------
        this : Atype
        	Object to be constructed
        
        
        Automatically generated constructor for atype
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_atype_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Atype
        
        
        Defined at define.fpp lines 9-16
        
        Parameters
        ----------
        this : Atype
        	Object to be destructed
        
        
        Automatically generated destructor for atype
        """
        if self._alloc:
            _mockdtpkg.f90wrap_atype_finalise(this=self._handle)
    
    @property
    def bool(self):
        """
        Element bool ftype=logical pytype=bool
        
        
        Defined at define.fpp line 10
        
        """
        return _mockdtpkg.f90wrap_atype__get__bool(self._handle)
    
    @bool.setter
    def bool(self, bool):
        _mockdtpkg.f90wrap_atype__set__bool(self._handle, bool)
    
    @property
    def integ(self):
        """
        Element integ ftype=integer      pytype=int
        
        
        Defined at define.fpp line 11
        
        """
        return _mockdtpkg.f90wrap_atype__get__integ(self._handle)
    
    @integ.setter
    def integ(self, integ):
        _mockdtpkg.f90wrap_atype__set__integ(self._handle, integ)
    
    @property
    def rl(self):
        """
        Element rl ftype=real(kind=double) pytype=float
        
        
        Defined at define.fpp line 12
        
        """
        return _mockdtpkg.f90wrap_atype__get__rl(self._handle)
    
    @rl.setter
    def rl(self, rl):
        _mockdtpkg.f90wrap_atype__set__rl(self._handle, rl)
    
    @property
    def vec(self):
        """
        Element vec ftype=real(8) pytype=float
        
        
        Defined at define.fpp line 13
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_atype__array__vec(self._handle)
        if array_handle in self._arrays:
            vec = self._arrays[array_handle]
        else:
            vec = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_atype__array__vec)
            self._arrays[array_handle] = vec
        return vec
    
    @vec.setter
    def vec(self, vec):
        self.vec[...] = vec
    
    @property
    def dtype(self):
        """
        Element dtype ftype=type(leveltwo) pytype=Leveltwo
        
        
        Defined at define.fpp line 14
        
        """
        dtype_handle = _mockdtpkg.f90wrap_atype__get__dtype(self._handle)
        if tuple(dtype_handle) in self._objs:
            dtype = self._objs[tuple(dtype_handle)]
        else:
            dtype = leveltwo.from_handle(dtype_handle)
            self._objs[tuple(dtype_handle)] = dtype
        return dtype
    
    @dtype.setter
    def dtype(self, dtype):
        dtype = dtype._handle
        _mockdtpkg.f90wrap_atype__set__dtype(self._handle, dtype)
    
    @property
    def prova(self):
        """
        Element prova ftype=character(256) pytype=str
        
        
        Defined at define.fpp line 15
        
        """
        return _mockdtpkg.f90wrap_atype__get__prova(self._handle)
    
    @prova.setter
    def prova(self, prova):
        _mockdtpkg.f90wrap_atype__set__prova(self._handle, prova)
    
    def __str__(self):
        ret = ['<atype>{\n']
        ret.append('    bool : ')
        ret.append(repr(self.bool))
        ret.append(',\n    integ : ')
        ret.append(repr(self.integ))
        ret.append(',\n    rl : ')
        ret.append(repr(self.rl))
        ret.append(',\n    vec : ')
        ret.append(repr(self.vec))
        ret.append(',\n    dtype : ')
        ret.append(repr(self.dtype))
        ret.append(',\n    prova : ')
        ret.append(repr(self.prova))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

@f90wrap.runtime.register_class("mockdtpkg.unused_type")
class unused_type(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=unused_type)
    
    
    Defined at define.fpp lines 22-24
    
    """
    def __init__(self, handle=None):
        """
        self = Unused_Type()
        
        
        Defined at define.fpp lines 22-24
        
        
        Returns
        -------
        this : Unused_Type
        	Object to be constructed
        
        
        Automatically generated constructor for unused_type
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_unused_type_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Unused_Type
        
        
        Defined at define.fpp lines 22-24
        
        Parameters
        ----------
        this : Unused_Type
        	Object to be destructed
        
        
        Automatically generated destructor for unused_type
        """
        if self._alloc:
            _mockdtpkg.f90wrap_unused_type_finalise(this=self._handle)
    
    @property
    def rl(self):
        """
        Element rl ftype=real(8) pytype=float
        
        
        Defined at define.fpp line 23
        
        """
        return _mockdtpkg.f90wrap_unused_type__get__rl(self._handle)
    
    @rl.setter
    def rl(self, rl):
        _mockdtpkg.f90wrap_unused_type__set__rl(self._handle, rl)
    
    def __str__(self):
        ret = ['<unused_type>{\n']
        ret.append('    rl : ')
        ret.append(repr(self.rl))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

def use_set_vars():
    """
    use_set_vars()
    
    
    Defined at define.fpp lines 28-33
    
    
    This type will be wrapped as it is used.
    """
    _mockdtpkg.f90wrap_use_set_vars()

def return_a_type_func():
    """
    a = return_a_type_func()
    
    
    Defined at define.fpp lines 35-41
    
    
    Returns
    -------
    a : Atype
    
    """
    a = _mockdtpkg.f90wrap_return_a_type_func()
    a = f90wrap.runtime.lookup_class("mockdtpkg.atype").from_handle(a)
    return a

def return_a_type_sub():
    """
    a = return_a_type_sub()
    
    
    Defined at define.fpp lines 43-49
    
    
    Returns
    -------
    a : Atype
    
    """
    a = _mockdtpkg.f90wrap_return_a_type_sub()
    a = f90wrap.runtime.lookup_class("mockdtpkg.atype").from_handle(a)
    return a

def get_a_set_real():
    """
    Element a_set_real ftype=real(8) pytype=float
    
    
    Defined at define.fpp line 18
    
    """
    return _mockdtpkg.f90wrap_define_a_type__get__a_set_real()

def set_a_set_real(a_set_real):
    _mockdtpkg.f90wrap_define_a_type__set__a_set_real(a_set_real)

def get_a_set_bool():
    """
    Element a_set_bool ftype=logical pytype=bool
    
    
    Defined at define.fpp line 19
    
    This type will also be wrapped
    """
    return _mockdtpkg.f90wrap_define_a_type__get__a_set_bool()

def set_a_set_bool(a_set_bool):
    _mockdtpkg.f90wrap_define_a_type__set__a_set_bool(a_set_bool)


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "define_a_type".')

for func in _dt_array_initialisers:
    func()
