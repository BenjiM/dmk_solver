from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging
import mockdtpkg.leveltwomod
import mockdtpkg.tdenspotentialsystem
import mockdtpkg.define_a_type
import mockdtpkg.kinddeclaration
import mockdtpkg.horrible
import mockdtpkg.dmkodedata
import mockdtpkg.controlparameters

def top_level(in_):
    """
    out = top_level(in_)
    
    
    Defined at define.fpp lines 54-58
    
    Parameters
    ----------
    in_ : float
    
    Returns
    -------
    out : float
    
    Example of a top-level subroutine.
    """
    out = _mockdtpkg.f90wrap_top_level(in_=in_)
    return out

