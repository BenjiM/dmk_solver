"""
Module leveltwomod


Defined at leveltwomod.fpp lines 5-11

"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("mockdtpkg.leveltwo")
class leveltwo(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=leveltwo)
    
    
    Defined at leveltwomod.fpp lines 9-11
    
    """
    def __init__(self, handle=None):
        """
        self = Leveltwo()
        
        
        Defined at leveltwomod.fpp lines 9-11
        
        
        Returns
        -------
        this : Leveltwo
        	Object to be constructed
        
        
        Automatically generated constructor for leveltwo
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_leveltwo_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Leveltwo
        
        
        Defined at leveltwomod.fpp lines 9-11
        
        Parameters
        ----------
        this : Leveltwo
        	Object to be destructed
        
        
        Automatically generated destructor for leveltwo
        """
        if self._alloc:
            _mockdtpkg.f90wrap_leveltwo_finalise(this=self._handle)
    
    @property
    def rl(self):
        """
        Element rl ftype=real(8) pytype=float
        
        
        Defined at leveltwomod.fpp line 10
        
        """
        return _mockdtpkg.f90wrap_leveltwo__get__rl(self._handle)
    
    @rl.setter
    def rl(self, rl):
        _mockdtpkg.f90wrap_leveltwo__set__rl(self._handle, rl)
    
    def __str__(self):
        ret = ['<leveltwo>{\n']
        ret.append('    rl : ')
        ret.append(repr(self.rl))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "leveltwomod".')

for func in _dt_array_initialisers:
    func()
