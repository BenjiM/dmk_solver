"""
Module horrible


Defined at define.fpp lines 61-66

Another module, defining a horrible type
"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("mockdtpkg.horrible_type")
class horrible_type(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=horrible_type)
    
    
    Defined at define.fpp lines 64-65
    
    """
    def __init__(self, handle=None):
        """
        self = Horrible_Type()
        
        
        Defined at define.fpp lines 64-65
        
        
        Returns
        -------
        this : Horrible_Type
        	Object to be constructed
        
        
        Automatically generated constructor for horrible_type
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_horrible_type_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Horrible_Type
        
        
        Defined at define.fpp lines 64-65
        
        Parameters
        ----------
        this : Horrible_Type
        	Object to be destructed
        
        
        Automatically generated destructor for horrible_type
        """
        if self._alloc:
            _mockdtpkg.f90wrap_horrible_type_finalise(this=self._handle)
    
    @property
    def x(self):
        """
        Element x ftype=real(8) pytype=float
        
        
        Defined at define.fpp line 65
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_horrible_type__array__x(self._handle)
        if array_handle in self._arrays:
            x = self._arrays[array_handle]
        else:
            x = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_horrible_type__array__x)
            self._arrays[array_handle] = x
        return x
    
    @x.setter
    def x(self, x):
        self.x[...] = x
    
    def __str__(self):
        ret = ['<horrible_type>{\n']
        ret.append('    x : ')
        ret.append(repr(self.x))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

def get_a_real():
    """
    Element a_real ftype=real(8) pytype=float
    
    
    Defined at define.fpp line 63
    
    """
    return _mockdtpkg.f90wrap_horrible__get__a_real()

def set_a_real(a_real):
    _mockdtpkg.f90wrap_horrible__set__a_real(a_real)


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module "horrible".')

for func in _dt_array_initialisers:
    func()
