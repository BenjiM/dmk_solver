#  f90wrap: F90 to Python interface generator with derived type support
#
#  Copyright James Kermode 2011-2018
#
#  This file is part of f90wrap
#  For the latest version see github.com/jameskermode/f90wrap
#
#  f90wrap is free software: you can redistribute it and/or modify
#  it under the terms of the GNU Lesser General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  f90wrap is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU Lesser General Public License for more details.
#
#  You should have received a copy of the GNU Lesser General Public License
#  along with f90wrap. If not, see <http://www.gnu.org/licenses/>.
#
#  If you would like to license the source code under different terms,
#  please contact James Kermode, james.kermode@gmail.com
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.tri as mtri

from ExampleDerivedTypes import ( Dmkcontrols,
                                  Abstractgeometry,
                                  Dmkinputsdata,
                                  example_monge_kantorovich_spectral,
                                  otpdmk,data2grids,dmkp1p0_steady_data,
                                  build_subgrid_rhs,
                                  Tdenspotentialsystem)

import sys
sys.path.append('../../globals/python_timedata')
import timedata as td
sys.path.append('../preprocess/2d_assembly/')
import example_grid as ex_grid
import example_forcing as ex_forcing
import meshtools as mt
import rectangle_grid as rectgrid

from PIL import Image


#
# open image and conver it to Image format
# 
def file2image(ImageFileName,Resolution=100):
    #open
    img_file = Image.open(ImageFileName)
    
    # compress
    if (Resolution != 100):
        print('Not done yet')
    return img_file;

#
# function to convert image into data define on triangulation
#
def image2dat(img_file):
   # get original image parameters...
   width, height = img_file.size

   format = img_file.format
   mode = img_file.mode

   # Make image Greyscale
   img_grey = img_file.convert('L')

   # Save Greyscale values
   print ('Pixels : '+str(img_grey.size[1])+' x '+ str(img_grey.size[0]))
   value = np.asarray(img_grey.getdata(), dtype=np.float)
   value[:]=value[:]/255.0
   value[value>0]=1
   print(value.shape)
   
   #value=value.reshape((img_grey.size[0], img_grey.size[1]))
   data=np.zeros(2*len(value))
   data[::2]=value[:]
   data[1::2]=value[:]
   
   return data;
     
#
# build triangulation where each pixel is divided in 2 triangles
# (nw-se diagonal) 
#
def image2grid(img_name):
    [topol,coord]=rectgrid.rectangle_grid(img_name.size[0],img_name.size[1])   
    return topol,coord;

"""
# set grid
length=1.0/16
flag_grid='rect_cnst'
extra_grid='rect_cnst'
# build grid
points, vertices, coordinates,elements,element_attributes = ex_grid.example_grid(flag_grid,length,extra_grid)
coord=np.array(coordinates)
if ( coord.shape[1] == 2 ):
    zcoord = np.zeros([coord.shape[0],1])
    coord=np.append(coord, zcoord, axis=1)
topol=np.array(elements)
try:
    print (len(element_attributes))
    flags=np.array(element_attributes)
except NameError:
    flags=np.int_(range(len(topol)))


topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1
nnode=len(coord)
ncell=len(topol)


nnode=len(coord)
ncell=len(topol)
size_cell=mt.make_size(coord,topol)
bar_cell=mt.make_bar(coord,topol)

# set forcing term
flag_source='rect_cnst'
flag_sink='rect_cnst'
extra_source=''
extra_sink=''
# build
sources=[];
sinks=[];
dirac_sources=[];
dirac_sinks=[];
source_tria=np.zeros([ncell,1])
sink_tria=np.zeros([ncell,1])
steady_source=True
steady_sink=True
ex_forcing.example_source(sources,dirac_sources,
                          str(flag_source),str(extra_source))
ex_forcing.example_sink(sinks,dirac_sinks,
                        str(flag_sink),str(extra_sink))
print(len(sources),len(sinks))
source_tria, steady_source=ex_forcing.make_source(
    source_tria,steady_source,
    sources,
    0,flags,bar_cell)
print(min(source_tria),max(source_tria))


sink_tria, steady_sink=ex_forcing.make_source(
    sink_tria,steady_sink,
    sinks,
    0,flags,bar_cell)
print(min(sink_tria),max(sink_tria))
forcing=source_tria-sink_tria

print(min(forcing),max(forcing))

td.write_steady_timedata('forcing.dat',forcing)
"""
res=25
source_img = Image.open('source.png')
sink_img = Image.open('sink.png').resize((160,300),Image.ANTIALIAS)
if (res != 100):
    print(source_img.size)
    newsize=[int(source_img.size[0]*(res/100)),int(source_img.size[1]*(res/100))]
    print (newsize)
    source_img=source_img.resize((newsize),Image.ANTIALIAS)
    sink_img = sink_img.resize((newsize),Image.ANTIALIAS)


[topol, coord]=image2grid(source_img)
source=image2dat(source_img)
sink=image2dat(sink_img)

msource = np.sum(source)
msink = np.sum(sink)
sink = (sink /msink)*  msource 


mt.write_grid(coord,topol,'grid.dat','dat')

forcing=source-sink

#td.write_steady_timedata('forcing.dat',forcing.reshape([len(forcing),1]))
#td.write_steady_timedata('source.dat',source.reshape([len(source),1]))
#td.write_steady_timedata('sink.dat',sink.reshape([len(sink),1]))


msource = np.sum(source)
msink = np.sum(sink)
sink = (sink /msink)*  msource 
forcing = source - sink

print (np.sum(forcing))

# Create triangulation.
#triang = mtri.Triangulation(coord[:,0], coord[:,1], topol)
#fig1, ax1 = plt.subplots()
#ax1.set_aspect('equal')
#tpc = ax1.tripcolor(triang, forcing, vmax=-0.001, vmin=0.001,cmap='RdBu')
#fig1.colorbar(tpc)
#ax1.set_title('Forcing Term')
#plt.show()

topolT=topol.transpose()
coordT=coord.transpose()
topolT=topolT+1
nnode=len(coord)
ncell=len(topol)


grid=Abstractgeometry.abs_simplex_mesh()
subgrid=Abstractgeometry.abs_simplex_mesh()
print(topol.shape)
mt.write_grid(coord,topol,'grid.dat','dat')

#
# init. Fortan structure for grid and subgrid
#
data2grids(6, 3, nnode, ncell, coordT, topolT, grid, subgrid)
c=subgrid.coord
cnew=c.transpose()
tt=subgrid.topol[0:3,:]
ttnew=tt.transpose()-1
print(ttnew.shape)
mt.write_grid(cnew,ttnew,'subgrid.dat','dat')

#
# init input data for dmk solver set rhs, penalty_weight, weight
#
dmkin=Dmkinputsdata.DmkInputs()
Dmkinputsdata.dmkinputs_constructor(dmkin,6,grid.ncell, subgrid.nnode,True)
# set rhs given by forcing = source-sink
build_subgrid_rhs(subgrid, dmkin.rhs, forcing)
# set pflux
dmkin.pflux=1.3
 

#
# init tdens potential datadmkp1p0_steady_data
#
tdpot=Tdenspotentialsystem.tdpotsys()
Tdenspotentialsystem.tdpotsys_constructor(tdpot,6,6, grid.ncell, subgrid.nnode)
tdpot.tdens[:]=1.0
tdpot.pot[:]=0.0

#
# init set controls
#
ctrl = Dmkcontrols.DmkCtrl()
# linear solver ctrl
ctrl.prec_type='IC'
ctrl.krylov_scheme='PCG'


# time steppin ctrl
ctrl.time_discretization_scheme=1
ctrl.max_time_iterations=200
ctrl.deltat = 0.1

# info , saving ctrl
ctrl.info_update=3
ctrl.info_state=2
ctrl.id_save_dat=3
ctrl.lun_statistics=10
ctrl.lun_out=6
ctrl.lun_tdens = 10
ctrl.fn_tdens='tdens.dat'
ctrl.lun_pot=11
ctrl.fn_pot='pot.dat'

# solve with dmk
info=0
dmkp1p0_steady_data(grid, subgrid, tdpot, dmkin, ctrl, info)

#Create triangulation.
triang = mtri.Triangulation(coord[:,0], coord[:,1], topol)
fig1, ax1 = plt.subplots()
ax1.set_aspect('equal')
tpc = ax1.tripcolor(triang, tdpot.tdens,  vmin=0.00)
fig1.colorbar(tpc)
ax1.set_title('Forcing Term')
plt.show()

"""   

info=0
otpdmk(3, nnode, ncell, topolT, coordT, pflux, forcing, tdens, pot, \
    ctrl, info)

#tdpot = Tdenspotentialsystem.tdpotsys()
#Tdenspotentialsystem.tdpotsys_construct(tdpot,6,6, 100000, 200000)
"""
