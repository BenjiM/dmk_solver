#
# List containing all the steps that one should follow to implement everything from scratch.
# A 2d-example use in (Facca-et-al SIAP 2018) will be created and executed.
#


# 0. Compile the fortran executables
# INPUT: name of the folder to be created containing all the results, e.g. ’tmp_folder’
# OUTPUT:  a folder named as the one given in input  containing all the executables and input files

./setup.sh tmp_folder

(It is better to create such folder outside the repository folder. Use, for example,
 *** ./setup.sh ../tmp_folder ***)


# 1. Prepare the input
# You need to be inside the folder created at 0-th step
# INPUT: name of the folder to be created containing the runs, e.g. ‘runs1'
# OUTPUT:  a folder named as the one given in input  containing all the runs’ results

cd tmp_folder

./dmk_folder.py  assembly runs1 inputs.ctrl

# 2. Run the code
# You need to be inside the folder created at 0-th step
# INPUT: folder name from step 1, ctrl_file
# OUTPUT:  results inside runs1/output


./dmk_folder.py run runs1 muffa