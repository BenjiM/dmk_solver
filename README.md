# README #

# (STEP 0)  : Check dependencies
# Use the script "check_external_programs.sh" 
# to check if all programs, libraries
# and python packages required are installed.
# usage:

chmod +x check_external_programs.sh
./check_external_programs.sh

# (STEP 2)  : Create work folder
# Use the script "setup.sh" to create a working folder to use the program.

chmod +x setup.sh 
./setup.sh "folder_path"

# (STEP 3) 
# Move to the work folder "folder_path" created at STEP (2) 
# to start simulations


#
The work folder "folder_path" contains
# - folder "runs" 
#   ( empty folder where store all folders containing
#     the input/output files required/produced by the muffa.out program) 
# - python script dmk_folder.py" 
#   ( script based on "click"(Command Line Interface Creation Kit)
#     to create folder, run simulation, generate vtk data etc. Usage:
#     
#     ./dmk_folder.py [option] command [arguments]
#    
#    For help use:
#

./dmk_folder.py --help :: help



#
# Other ( older ) options to create example and execute simulations
# are the bash scripts assembly_inputs.sh and folder_run.sh
#
# assembly_inputs.sh
#
# It creates a folder with the inputs file required 
# by program muffa.out. First change the parameter 
# in file "inputs.ctrl" and execute program

chmod +x assembly_inputs
./assembly_inputs.sh "folder_name"

#
# folder_run.sh 
# 
# It launches a simulation on a given folder
# Change the parameter in file "muffa.ctrl" and use
# 

chmod +x folder_run.sh
./folder_run.sh ./runs/"folder_name"


### What is this repository for? ###

* Quick summary 
This repository contains the code for the
discretization with the p1-p0 scheme of the Dynamic
Monge-Kantorovich Equations.  
* Version * 1.0

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
Repositories:
globals
geometry
linear_algebra
p1galerkin
blas 
lapack
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Enrico Facca, Mario Putti
* Other community or team contact