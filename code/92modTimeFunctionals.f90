module TimeFunctionals
  use Globals
  implicit none
  private
  !>-------------------------------------------------------------
  !> Structure containg integer and real array 
  !> with size = max_time_it or max_size_it+1
  !> with information on the system tdpot and
  !> on statistic on the numerical procedure used .
  !> The arrays collect the information at each time iteration
  !> A new functional has to be:
  !> allocated (in init)
  !> deallocated ( in kill), 
  !> defined at each iteration by the module tdpot (in set)
  !> set the printing procedure ( in info ) 
  !> set the writeing procedure ( in write )
  !> The integer array functionals_ctrl
  !> controls if the the funcional operation 
  !> 0 = not created
  !> 1 = create, compute, save to file at the end
  !> 2 = create, compute, print in output, save in file at the end
  !>-------------------------------------------------------------
  type, public :: evolfun
     !> Number of time iterations
     integer :: max_time_it
     !> Max Number of non linaera iterations 
     integer :: max_iter_nonlinear
     !>---------------------------------------------------------
     !> Integer functionals
     !>---------------------------------------------------------
     !> Number of iterations for iterative linear solver
     !> Dimension(max_iter_nonlinear,0:max_time_it)
     integer, allocatable :: iter_solver(:,:)
     !> Total number of iterations for iterative solvers
     !> Dimension(0:max_time_it)
     integer, allocatable :: iter_total(:)
     !> Average number of iterations for iterativelinear solver
     !> for each time step
     !> Dimension(0:max_time_it)
     integer, allocatable :: iter_media(:)
     !>---------------------------------------------------------
     !> Number of fix point iterations
     !> if a non linear equation need to be solved
     !> Dimension(0:max_time_it)
     integer, allocatable :: iter_nonlinear(:)
     !>---------------------------------------------------------
     !> Number of fill-in used
     !> Dimension(0:max_time_it)
     integer, allocatable :: nfillin(:)
     !>---------------------------------------------------------
     !> Flag 0-1 if Preconditioner is calculated
     !> Dimension(0:max_time_it)
     integer, allocatable :: prec_calc(:,:)
     !> Flag 0-1-2 if prec. tuning via spectral info is calculated
     !> 0=not computed
     !> 1=Computed with DACG
     !> 2=Computed with LANCZOS
     !> Dimension(0:max_time_it)
     integer, allocatable :: tuning_calc(:,:)
     !> Number of restart of newton iteration
     !> Dimension(0:max_time_it)
     integer, allocatable :: nrestart_newton(:)
     !>---------------------------------------------------------
     !> Real valued functionals
     !>---------------------------------------------------------
     !> Integral of tdens
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable ::  mass_tdens(:)
     !>---------------------------------------------------------
     !> Integral of weighted_mass of tdens
     !> $ 1/2 int_{\Domain} \frac{\Tdens^{\Pwmass}}{\Pwmass}$
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: weighted_mass_tdens(:)
     !>------------------------------------------------------------
     !> Dissipated Energy
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: energy(:)
     !>------------------------------------------------------------
     !> Lyapunov Functional=Energy + Wmass
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: lyapunov(:)
     !> Functional Energy=\int_ Forcing Pot + wmass
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: lyapunov_alternative(:)
     !>------------------------------------------------------------
     !> Min values of tdens
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: min_tdens(:)
     !>------------------------------------------------------------
     !> Max Values of tdens
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_tdens(:)
     !>------------------------------------------------------------
     !> Entropy = $ int_{\Domain}\Tdens*ln(Tdens)$ 
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: entropy_tdens(:)
     !>------------------------------------------------------------
     !> Integral flux with candidate pvel exponent
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: integral_flux_pvel(:)
     !>------------------------------------------------------------
     !> Max of flux of the flux
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_flux(:)
     !>------------------------------------------------------------
     !> Duality gap
     !> $\int_{\Omega}|q|^{pvel} - \int{\Omega} u f
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: duality_gap(:)
     !>------------------------------------------------------------
     !> Maximum value of norm of the gradient
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_nrm_grad(:)
     !>------------------------------------------------------------
     !> Maximum value of norm of the averaged gradient
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_nrm_grad_avg(:)
     !> Maximum value of D3
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_D3(:)
     !> Maximum value of D3
     !> $\max{ |\Grad \Pot|}$
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: max_D3_filtered(:)
     !>------------------------------------------------------------
     !> Nterm of precodintior / nterm matrix
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: ntermp_nterm(:)
     !>------------------------------------------------------------
     !> Constraction constant of fix point iteration 
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: cnst_nonlinear(:)
     !>------------------------------------------------------------
     !> Residual od solverolv procedure
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: res_solver(:,:)
     !> Residual of system A(tdens) \pot = rhs
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: res_elliptic(:)
     !> Relative L1-error for tdens with respect to reference solution
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: err_tdens(:)
     !> Relative Error of in Wasserstein distance
     !> | \Lyap(tdens)-\int_{\Domain} \Optdens | / |\int_{\Domain} \Optdens |
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: err_wasserstein(:)
     !> Wasserstein-1 distance compute as 
     !> \int_{\Domain} \Optdens
     real(kind=double), allocatable :: wasserstein_distance(:)
     !> Algorithm Time
     !> Time without overhead 
     !> It includes the time wasted in wrong caluclation
     real(kind=double), allocatable :: CPU_time(:)
     !> CPU wassted in update f
     real(kind=double), allocatable :: CPU_wasted(:)
     !> Relative L2-error for pot with respect to reference solution
     !> Kantorovich potential
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: err_pot_l2(:)
     !> Relative L\infty-error for pot with respect to reference solution
     !> Kantorovich potential
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: err_pot_linfty(:)
     !> Deltat
     !> Dimension(0:max_time_it)
     real(kind=double), allocatable :: deltat(:)
   contains 
     !> Static constructor
     !> (procedure public for type evolfun)
     procedure, public, pass :: init => init_evolfun
     !> Static destructor
     !> (procedure public for type evolfun)
     procedure, public, pass :: kill => kill_evolfun
     !> Info procedure at give time iteration
     !> (procedure public for type evolfun)
     procedure, public, pass :: info => info_evolfun
     !> Info procedure at give time iteration
     !> (procedure public for type evolfun)
     procedure, public, pass :: write2dat => write2dat_evolfun
     !> Procedure setting functional at
     !> given time iteration and state of tdpot
     !> (procedure public for type evolfun)
     procedure, public, pass :: set_evolfun !=> set_evolfun
  end type evolfun
contains
  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type timefun)
  !> Instantiate of variable of type timefun
  !>
  !> usage:
  !>     call 'var'%init(lun_err, max_time_it,max_iter_nonlinear)
  !>
  !> where:
  !> \param[in] stder              -> integer .I/O unit for error msg
  !> \param[in] max_time_it        -> integer. Max number of 
  !>                                   time iterations
  !> \param[in] max_iter_nonlinear -> integer. Max number of non linear
  !>                                   solver iterations 
  !<-------------------------------------------------------------  
  subroutine init_evolfun(this,lun_err,max_time_it,max_iter_nonlinear)
    implicit none
    class(evolfun), intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    integer,        intent(in   ) :: max_time_it
    integer,        intent(in   ) :: max_iter_nonlinear
    ! local var
    logical :: rc
    integer :: res

    this%max_time_it         = max_time_it
    this% max_iter_nonlinear = max_iter_nonlinear

    ! Integer arrays
    allocate(&
         this%iter_solver(max_iter_nonlinear,0:max_time_it),&
         this%iter_nonlinear(0:max_time_it),&
         this%iter_total(0:max_time_it),&
         this%iter_media(0:max_time_it),&
         this%nfillin(0:max_time_it),&
         this%prec_calc(max_iter_nonlinear,0:max_time_it),&
         this%tuning_calc(max_iter_nonlinear,0:max_time_it),&
         this%nrestart_newton(0:max_time_it),&         
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'init_evolfun', &
         ' type timefun integer fucntionals',res)
    this%iter_solver    = 0 
    this%iter_total     = 0 
    this%iter_media     = 0 
    this%iter_nonlinear = 0
    this%nfillin        = 0
    this%prec_calc      = 0
    this%tuning_calc    = 0
    this%nrestart_newton= 0
    !
    ! Real valued functional 
    !
    allocate(&
         this%mass_tdens(0:max_time_it),&
         this%weighted_mass_tdens(0:max_time_it),&
         this%energy(0:max_time_it),&
         this%lyapunov(0:max_time_it),&
         this%lyapunov_alternative(0:max_time_it),&
         this%min_tdens(0:max_time_it),&
         this%max_tdens(0:max_time_it),&
         this%entropy_tdens(0:max_time_it),&
         this%integral_flux_pvel(0:max_time_it),&
         this%max_flux(0:max_time_it),&
         this%duality_gap(0:max_time_it),&
         this%max_nrm_grad(0:max_time_it),&
         this%max_nrm_grad_avg(0:max_time_it),&
         this%ntermp_nterm(0:max_time_it),&
         this%cnst_nonlinear(0:max_time_it),&
         this%res_solver(max_iter_nonlinear,0:max_time_it),&
         this%res_elliptic(0:max_time_it),&
         this%err_tdens(0:max_time_it),&
         this%err_wasserstein(0:max_time_it),&
         this%wasserstein_distance(0:max_time_it),&
         this%CPU_time(0:max_time_it),&
         this%err_pot_l2(0:max_time_it),&
         this%err_pot_linfty(0:max_time_it),&
         this%deltat(0:max_time_it),&
         this%max_D3(0:max_time_it),&
         this%max_D3_filtered(0:max_time_it),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'init_evolfun', &
         ' type timefun real functionals',res)

    this%mass_tdens=zero
    this%weighted_mass_tdens=zero
    this%energy=zero
    this%lyapunov=zero
    this%lyapunov_alternative=zero
    this%min_tdens=zero
    this%max_tdens=zero
    this%entropy_tdens=zero
    this%integral_flux_pvel=zero
    this%max_flux=zero
    this%duality_gap=zero
    this%max_nrm_grad=zero
    this%max_nrm_grad_avg=zero
    this%ntermp_nterm=zero
    this%cnst_nonlinear=zero
    this%res_solver=zero
    this%res_elliptic=zero
    this%err_tdens=zero
    this%err_wasserstein=zero
    this%wasserstein_distance = zero
    this%CPU_time = zero
    this%err_pot_l2=zero
    this%deltat=zero
    this%err_pot_linfty=zero
    this%max_D3=zero
    this%max_D3_filtered=zero

  end subroutine init_evolfun

  subroutine kill_evolfun(this,lun_err)
    implicit none
    class(evolfun), intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    !local
    logical :: rc
    integer :: res

    ! Integer arrays
    deallocate(&
         this%iter_solver,&
         this%iter_total,&
         this%iter_media,&
         this%iter_nonlinear,&
         this%nfillin,&
         this%prec_calc,&
         this%tuning_calc,&
         this%nrestart_newton,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'init_evolfun', &
            ' type timefun integer fucntionals',res)
   
    ! Real valued functional 
    deallocate(&
         this%mass_tdens,&
         this%weighted_mass_tdens,&
         this%energy,&
         this%lyapunov,&
         this%lyapunov_alternative,&
         this%min_tdens,&
         this%max_tdens,&
         this%entropy_tdens,&
         this%integral_flux_pvel,&
         this%max_flux,&
         this%duality_gap,&
         this%max_nrm_grad,&
         this%max_nrm_grad_avg,&
         this%ntermp_nterm,&
         this%cnst_nonlinear,&
         this%res_solver,&
         this%res_elliptic,&
         this%err_tdens,&
         this%err_wasserstein,&
         this%wasserstein_distance,&
         this%CPU_time,&
         this%deltat,&
         this%err_pot_l2,&
         this%err_pot_linfty,&
         this%max_D3,&
         this%max_D3_filtered,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'init_evolfun', &
         ' type timefun real functionals',res)

  
  end subroutine kill_evolfun

  subroutine info_evolfun(this,lun,&
       option,&
       ctrl,&
       itemp)
    use Globals
    use ControlParameters
    implicit none
    class(evolfun),   intent(in) :: this
    integer,          intent(in) :: lun
    integer,          intent(in) :: option
    type(CtrlPrm),    intent(in) :: ctrl
    integer,          intent(in) :: itemp
    !local
    character(len=256) :: out_format
    character(len=3)   :: sep
    

    if (option .eq. 1) then
       sep=' | '
       out_format=ctrl%formatting('aaaraaaraaar')

       write(lun,out_format) &
            'tdens : ',&
            'min  ',' = ', this%min_tdens(itemp), sep,&
            'max  ',' = ', this%max_tdens(itemp),sep,&
            'wmass',' = ', this%weighted_mass_tdens(itemp)


       out_format=ctrl%formatting('aaaraaaraaar')

       write(lun,out_format) &
            'funct : ',&
            'ene  ',' = ', this%energy(itemp), sep,&
            'lyap ',' = ', this%lyapunov(itemp), sep, &
            'dgap ',' = ', this%duality_gap(itemp)
       
       if ( this%err_tdens(itemp) .gt. small)  then
          out_format=ctrl%formatting('aaaraaaraaar')
          write(lun,out_format) &
               'err   : ',&
               'td   ',' = ', this%err_tdens(itemp), sep,&
               'wass ',' = ', this%err_wasserstein(itemp), sep,&
               '1wass',' = ', this%wasserstein_distance(itemp)
          out_format=ctrl%formatting('aaaraaaraaar')
          write(lun,out_format) &
               'grad  : ',&
               'abs  ',' = ', this%max_nrm_grad(itemp), sep,&
               'avg  ',' = ', this%max_nrm_grad_avg(itemp), sep,&
               'D3   ',' = ', this%max_D3(itemp)
          
       end if
       
       if ( this%err_pot_l2(itemp) .gt. small)  then
          out_format=ctrl%formatting('aaraar')
          write(lun,out_format) &
               'errpot: ',&
               'l2 err = ', this%err_pot_l2(itemp), sep,&
               'linfty err = ', this%err_pot_linfty(itemp)

       end if
       
       out_format=ctrl%formatting('aar')
          write(lun,out_format) &
               'linsys: ',&
               'resell= ', this%res_elliptic(itemp)

    end if
  end subroutine info_evolfun


  subroutine set_evolfun(this,itemp,tdpot,p1p0,ode_inputs,ctrl,CPU)
    use Globals
    use TdensPotentialSystem
    use ControlParameters
    use MuffeTiming
    use DmkOdeData
    implicit none
    class(evolfun), intent(inout) :: this
    integer,        intent(in   ) :: itemp
    type(tdpotsys), intent(in   ) :: tdpot
    type(p1p0_space_discretization), intent(in   ) :: p1p0
    type(odedata),     intent(in   ) :: ode_inputs
    type(CtrlPrm),  intent(in   ) :: ctrl
    type(CodeTim), intent(in) :: CPU
   
    !local
    real(kind=double) :: power,w1dist

    !>----------------------------------------------------------------
    !> Integer Valued Fucntional
    !>----------------------------------------------------------------
    this%iter_solver(:,itemp) = &
         tdpot%sequence_info_solver(:)%iter
    this%iter_nonlinear(itemp) = tdpot%iter_nonlinear
    this%iter_total(itemp)=sum(this%iter_solver(1:tdpot%iter_nonlinear,itemp))
    this%iter_media(itemp)=int(this%iter_total(itemp)*one/tdpot%iter_nonlinear)
    this%nfillin(itemp) = tdpot%nfillin_used
    this%prec_calc(:,itemp) = &
         tdpot%sequence_build_prec(:)
    if (ctrl%build_tuning .eq. 1) &
         this%tuning_calc(:,itemp) = &
         tdpot%sequence_build_tuning(:)
    this%nrestart_newton(itemp) = tdpot%nrestart_newton
    !----------------------------------------------------------------
    ! Real Valued Fucntional
    !---------------------------------------------------------------
        
    this%mass_tdens(itemp)= tdpot%mass_tdens
    this%weighted_mass_tdens(itemp)= tdpot%weighted_mass_tdens
    this%energy(itemp) = tdpot%energy
    this%lyapunov(itemp)  =  tdpot%lyapunov
    this%min_tdens(itemp)= tdpot%min_tdens
    this%max_tdens(itemp)= tdpot%max_tdens
    this%max_flux(itemp) = tdpot%max_velocity
    this%max_nrm_grad(itemp)     = tdpot%max_nrm_grad
    this%max_nrm_grad_avg(itemp) = tdpot%max_nrm_grad_avg 
    this%max_D3(itemp) = tdpot%max_D3 

    power = p1p0 %pvel_exponent(ode_inputs)
    this%integral_flux_pvel(itemp) = tdpot%integral_flux_pvel
    this%duality_gap(itemp) = tdpot%duality_gap
    this%res_solver(:,itemp) = &
         tdpot%sequence_info_solver(:)%resreal

    this%res_elliptic(itemp) = tdpot%res_elliptic
    if ( ode_inputs%opt_tdens_exists) then
       w1dist=p1p0%grid_tdens%normp_cell(one, ode_inputs%opt_tdens(:,1))
       this%err_tdens(itemp) = tdpot%err_tdens
       this%err_wasserstein(itemp) = abs( tdpot%lyapunov - w1dist) / w1dist 
    end if
    this%wasserstein_distance(itemp) = tdpot%lyapunov
    this%CPU_time(itemp) = CPU%ALGORITHM%CUMusr

    this%deltat(itemp) = ctrl%deltat 
    
  end subroutine set_evolfun

  subroutine write2dat_evolfun(this,&
       lun_err,lun,folder,&
       it_stop,&
       time,iformat,rformat)
    use Globals
    implicit none
    class(evolfun),     intent(in) :: this
    integer,            intent(in) :: lun_err
    integer,            intent(in) :: lun
    character (len=*) , intent(in) :: folder
    integer,            intent(in) :: it_stop
    real(kind=double),  intent(in) :: time(0:this%max_time_it)
    character (len=*) , intent(in) :: rformat
    character (len=*),  intent(in) :: iformat
    !local
    logical :: rc
    integer :: res
    character (len=256) :: fname
    integer :: max_time_it,max_nonlin
    integer, allocatable :: ones(:),max_nnlin(:)

    allocate(ones(0:this%max_time_it),max_nnlin(0:this%max_time_it),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'write2dat_evolfun', &
         ' wotk array ones',res)

    ones=1
    max_nnlin=this%max_iter_nonlinear


    max_time_it = this%max_time_it
    max_nonlin  = this%max_iter_nonlinear

    ! integer arrays
    fname=etb(etb(folder)//'iter_solver.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_it,&
         max_nnlin,0,it_stop,&
         time, this%iter_solver,&
         iformat,rformat)
    

    fname=etb(etb(folder)//'iter_total.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%iter_total,&
         iformat,rformat)

    fname=etb(etb(folder)//'iter_media.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%iter_media,&
         iformat,rformat)
    
    fname=etb(etb(folder)//'iter_nonlinear.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%iter_nonlinear,&
         iformat,rformat)

    fname=etb(etb(folder)//'fillin.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%nfillin,&
         iformat,rformat)

    fname=etb(etb(folder)//'prec_calc.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_it,&
         max_nnlin,0,it_stop,&
         time, this%prec_calc,&
         iformat,rformat)

    fname=etb(etb(folder)//'tuning_calc.dat')
    call write_integer(lun_err,lun,fname,&
         max_nonlin,max_time_it,&
         this%iter_nonlinear,0,it_stop,&
         time, this%tuning_calc,&
         iformat,rformat)

    fname=etb(etb(folder)//'nrestart_newton.dat')
    call write_integer(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%nrestart_newton,&
         iformat,rformat)


    fname=etb(etb(folder)//'mass_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%mass_tdens,&
         rformat)

    fname=etb(etb(folder)//'weighted_mass_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_it,&
         ones, 0,it_stop,&
         time, this%weighted_mass_tdens,&
         rformat)

    fname=etb(etb(folder)//'energy.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_it,&
         ones, 0,it_stop,&
         time, this%energy,&
         rformat)
    fname=etb(etb(folder)//'lyapunov.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_it,&
         ones,0,it_stop,&
         time, this%lyapunov,&
         rformat)

    fname=etb(etb(folder)//'lyapunov_alternative.dat')
    call write_real(lun_err,lun,fname,&
         1, max_time_it,&
         ones,0,it_stop,&
         time, this%lyapunov_alternative,&
         rformat)


    fname=etb(etb(folder)//'min_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%min_tdens,&
         rformat)

    fname=etb(etb(folder)//'max_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%max_tdens,&
         rformat)

    fname=etb(etb(folder)//'entropy.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%entropy_tdens,&
         rformat)
    
    fname=etb(etb(folder)//'integral_flux_pvel.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%integral_flux_pvel,&
         rformat)

    fname=etb(etb(folder)//'max_flux.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%integral_flux_pvel,&
         rformat)

    fname=etb(etb(folder)//'duality_gap.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%duality_gap,&
         rformat)

    fname=etb(etb(folder)//'max_nrm_grad.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%max_nrm_grad,&
         rformat)

    fname=etb(etb(folder)//'max_nrm_grad_avg.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%max_nrm_grad_avg,&
         rformat)


    fname=etb(etb(folder)//'ntermp_nterm.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%ntermp_nterm,&
         rformat)

    fname=etb(etb(folder)//'cnst_nonlinear.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%cnst_nonlinear,&
         rformat)

    fname=etb(etb(folder)//'res_solver.dat')
    call write_real(lun_err,lun,fname,&
         max_nonlin,max_time_it,&
         max_nnlin,0,it_stop,&
         time, this%res_solver,&
         rformat)

    fname=etb(etb(folder)//'res_elliptic.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%res_elliptic,&
         rformat)

    fname=etb(etb(folder)//'err_tdens.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%err_tdens,&
         rformat)

    fname=etb(etb(folder)//'err_wasserstein.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%err_wasserstein,&
         rformat)

    fname=etb(etb(folder)//'wasserstein_distance.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%wasserstein_distance,&
         rformat)

    fname=etb(etb(folder)//'CPU_time.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%CPU_time,&
         rformat)

    fname=etb(etb(folder)//'err_pot_l2.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%err_pot_l2,&
         rformat)

    fname=etb(etb(folder)//'deltat.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%deltat,&
         rformat)

    fname=etb(etb(folder)//'err_pot_linfty.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%err_pot_linfty,&
         rformat)

    fname=etb(etb(folder)//'max_D3.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%max_D3,&
         rformat)

    fname=etb(etb(folder)//'max_D3_filtered.dat')
    call write_real(lun_err,lun,fname,&
         1,max_time_it,&
         ones,0,it_stop,&
         time, this%max_D3_filtered,&
         rformat)



    deallocate(ones,max_nnlin,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'write2dat_evolfun', &
         ' wotk array ones',res)


  contains
    subroutine write_integer(&
         lun_err,&
         lun_file,&
         fname,&
         ndata,&
         max_time_it,&
         nonzeros,&
         it_start,it_stop,&
         time, iarray,&
         iformat,&
         rformat)
      use Globals
      implicit none
      integer,            intent(in   ) :: lun_err
      integer,            intent(in   ) :: lun_file
      character(len=*),   intent(inout) :: fname
      integer,            intent(in   ) :: ndata
      integer,            intent(in   ) :: max_time_it
      integer,            intent(in   ) :: nonzeros(0:max_time_it)
      integer,            intent(in   ) :: it_start,it_stop
      real(kind=double),  intent(in   ) :: time(0:max_time_it)
      integer,            intent(in   ) :: iarray(ndata,0:max_time_it)
      character(len=*),   intent(in   ) :: iformat
      character(len=*),   intent(in   ) :: rformat
      !local 
      integer :: res,i,j 
      logical :: rc
      character(len=256)  :: out_format,dataformat
      
      open(lun_file,file=fname, iostat = res)
      if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
           'write2dat_evol_timefunc', &
           'err open file '//etb(fname), res)
      do i = it_start, it_stop
         out_format =etb('(')!//etb(rformat)//',1x,')
         write(dataformat,'(I3,a,a,a)') &
              max(1,nonzeros(i)),'(1x,',etb(iformat),')'      
         dataformat=etb(dataformat)
         out_format=etb(etb(out_format)//etb(dataformat)//')')         
         write(lun,out_format) &!time(i), &
              (iarray(j,i),j=1,max(nonzeros(i),1))
      end  do
      close(lun_file)


    end subroutine write_integer

    subroutine write_real(&
         lun_err,&
         lun_file,&
         fname,&
         ndata,&
         max_time_it,&
         nonzeros,&
         it_start,it_stop,&
         time, rarray,&
         rformat)
      use Globals
      implicit none
      integer,            intent(in   ) :: lun_err
      integer,            intent(in   ) :: lun_file
      character(len=*),   intent(inout) :: fname
      integer,            intent(in   ) :: ndata
      integer,            intent(in   ) :: max_time_it
      integer,            intent(in   ) :: nonzeros(0:max_time_it)
      integer,            intent(in   ) :: it_start,it_stop
      real(kind=double),  intent(in   ) :: time(0:max_time_it)
      real(kind=double),  intent(in   ) :: rarray(ndata,0:max_time_it)
      character(len=*),   intent(in   ) :: rformat
      !local 
      integer :: res,i,j
      logical :: rc
      character(len=256)  :: out_format,dataformat

      open(lun_file,file=fname, iostat = res)
      if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
           'write2dat_evol_timefunc', &
           'err open file '//etb(fname), res)
      do i = it_start, it_stop
         out_format =etb('(')!//etb(rformat)//',1x,')
         write(dataformat,'(I2,a,a,a)') &
              max(1,nonzeros(i)),'(1x,',etb(rformat),')'      
         dataformat=etb(dataformat)
         out_format=etb(etb(out_format)//etb(dataformat)//')')

         write(lun,out_format) &!time(i),  
              (rarray(j,i),j=1,max(1,nonzeros(i)))
      end  do
      close(lun_file)


    end subroutine write_real
  end subroutine write2dat_evolfun


end module Timefunctionals
