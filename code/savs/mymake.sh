#!/bin/bash

order=$1
echo $order
if [ -z $order ]
then
    cp ../../globals/code/*f90 .
    cp ../../linear_algebra/code/*f90 .
    cp ../../geometry/code/*f90 .
    cp ../../p1galerkin/code/*f90 .
    make
fi

if [ "$order" == 'clean' ]
then 
    rm [0-6][0-9]mod*f90
    make clean
fi

if [ "$order" == 'debug' ]
then
    cp ../../globals/code/*f90 .
    cp ../../linear_algebra/code/*f90 .
    cp ../../geometry/code/*f90 .
    cp ../../p1galerkin/code/*f90 .
    make debug
fi

 
