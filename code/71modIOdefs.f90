!> Defines I/O units, directories, filenames
module IOdefs
  !use hdf5, only: hid_t
  use Globals
  implicit none  
  private
  type, public :: IOfdescr
     !> max unit number used in the code.
     !> Dynamic var used to avoid conflicts in opening new units.
     integer :: maxUsedLun
     !> terminal (generally stdout)
     type(file) :: term
     !> obvious defs
     type(file) :: stdin,stdout,stderr
     !>-------------------------------------------------------
     !> Input Files and Directories (mandatory)
     !>-------------------------------------------------------
     !> Genereal input folder
     type(file) :: input_folder
     !>-------------------------------------------------------
     !> Simulations controls     
     !> input file for control parameters
     type(file) :: control
     !>-------------------------------------------------------
     !> Topology files 
     !> input file for mesh data
     type(file) :: grid
     !> input file with subgrid for p1 discretization
     type(file) :: subgrid  
     !> output directory for linear system data
     type(file) :: parents
     !>-------------------------------------------------------
     !> Input file of ODE
     !> input file for real left power
     type(file) :: pflux
     !> input file for real right power
     type(file) :: pmass
     !> input file for time- varing decay
     type(file) :: decay
     !> input file for initial tdens
     type(file) :: tdens0
     !> input file for spatial decay
     type(file) :: kappa
     !> input file for integrated rhs
     !> given by 
     !> $ \int_{\Domain} \Focring\Psi 
     !>   + \int_{\partial \Domain}\OutFlux \cdot n $
     type(file) :: rhs_integrated
     !>-------------------------------------------------------
     !> Output Files and Directories
     !> general output directory 
     type(file) :: output_folder
     !> output file summarizing all data of simulations
     type(file) :: statistic
     !> output directory where to put spatial varying datas 
     !> tdens, pot etc
     type(file) :: result
     !> Tdens time evolution
     type(file) :: tdens_out
     !> Pot time evolution
     type(file) :: pot_out
     !> Pot time evolution
     type(file) :: nrmgraddyn_out
     !> output directory where to put time functional data
     type(file) :: timefun
     !> output directory where to put vtk files
     type(file) :: linear_sys
     !> generic debug file
     type(file) :: debug
     !>-------------------------------------------------------
     !> input file for forcing term ode
     type(file) :: rhs_grid_integrated
     !> input file to compare with tdens (optional)
     type(file) :: optdens
     !> Input file for penalty 
     type(file) :: penalty
     !> Input file for penalty weight
     type(file) :: penalty_weight
     !> Input file for multiplicative factor for penalty weight
     type(file) :: penalty_factor
     !> input file to compare with tdens (optional)
     type(file) :: dirichlet_bc
     !> Input file paremeter lambda to ensure coercivity
     type(file) :: lambda
   contains
     !> static constructor
     !> (public for type IOfdescr)
     procedure, public, pass :: init => IO_initialize
     !> output the content of the variable
     !> (public for type IOfdescr)
     procedure, public, pass :: info => IO_print_info 
     !> static destructor
     !> (public for type IOfdescr)
     procedure, public, pass :: kill => IO_close_all_units
     !> Find the logical unit for 
     !> stderr, stdout, statistic and debug
     !> (public for type IOfdescr)
     procedure, public, pass :: find_lun
  end type IOfdescr

contains
  !>-------------------------------------------------------------
  !> Static constructor. 
  !> (public procedure for type IOfdescr)
  !> Instantiate and initialize a variable of type IOfdescr
  !>
  !> usage:
  !>     call 'var'\%init()
  !>
  !> - defines stdin,stderr,stdout
  !> - reads from "muffa.fnames" working directory and filenames
  !> - open all file units checking for consistency
  !<-------------------------------------------------------------
  subroutine IO_initialize(this)
    use Globals
    use, intrinsic :: iso_fortran_env, only : stdin=>input_unit, &
         stdout=>output_unit, &
         stderr=>error_unit
    class(IOfdescr) :: this

    ! local vars
    integer :: opstat,res
    logical :: file_exist=.false.,dir_exist
    character (len=256) :: fname='muffa.fnames', dir,input,str

    integer :: nlun
    integer :: lun !  da tirare via ********
    lun = stderr   !  da tirare via ********
    
    this%stderr%exist = .true.
    this%stderr%lun=stderr
    this%stderr%fn='stderr'
    
    this%stdin%exist = .true.
    this%stdin%lun=stdin
    this%stdin%fn='stdin'
    
    this%stdout%exist = .true.
    this%stdout%lun=stdout
    this%stdout%fn='stdout'
    
    this%term%exist = .true.
    this%term%lun=this%stdout%lun
    this%term%fn='terminal'


    ! read in from standard file (or input) the IO control file
    ! that overrides above default values
    do while (.not. file_exist)
       inquire(file=fname,exist=file_exist)
       if (file_exist) then
          !
          ! read folder position (usaully .)
          !
          open(1,file=fname)
          read(1,'(a)') dir
          dir=etb(erase_comment(dir))

          ! Want to avoid default lun 0, 5 and 6
          ! as they usually are stderr,stdin,stdout

          nlun=6
          !
          ! 1-read files/fodlers listed in muffa.fnames
          ! 2-clean str from comments
          ! 3-append dir if no absolute path
          ! 4-open/check file
          !
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%input_folder%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.True.)
          
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%control%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%grid%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%subgrid%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%parents%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pflux%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pmass%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%decay%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          write(*,*) etb(str)
          call dir2str(dir,str)
          call this%tdens0%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%kappa%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%rhs_integrated%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          !
          ! output files
          !
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%output_folder%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%statistic%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%result%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%tdens_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pot_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%nrmgraddyn_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%timefun%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%linear_sys%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%debug%init(stderr,str,nlun,'out',&
               folder=.False.)


          !
          ! optional files
          !
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%optdens%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%rhs_grid_integrated%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty_weight%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          
         
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             write(*,*) etb(str)
             call this%dirichlet_bc%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if
          
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty_factor%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%lambda%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.)
          end if

          this%maxUsedLun=nlun

          close(1)

          !inquire(file=dir,exist=dir_exist)
          !if(.not.dir_exist) then
          !   file_exist = IOerr(stderr, wrn_IO,'IOinitialize',etb(dir))
          !   cycle
          !end if
       else
          write(lun,*) ' Control file ',etb(fname),&
               ' does not exist in the current directory!!'
          if (lun.eq.this%stdin%lun) then
             write(lun,*) ' input another file name or END/QUIT to stop'
             read(*,'(a)') fname
             fname=to_lower(fname)
             if ( &
                  fname.eq.'end' .or. &
                  fname.eq.'quit' .or. &
                  fname.eq.'q' .or. &
                  fname.eq.'e' &
                  ) then
                stop ' simulation ended'
             end if
             ! everything has failed, restart loop
          else 
             stop ' simulation ended'
          end if
       end if
    end do
  contains
    subroutine prepend_dir(file_name,dir)
      use Globals
      implicit none
      type(file),       intent(inout) :: file_name
      character(len=*), intent(in   ) :: dir
      
      if( file_name%fn(1:1) .ne. '/') then
         file_name%fn=etb(dir)//'/'//etb(erase_comment(file_name%fn))
      else
         file_name%fn=etb(erase_comment(file_name%fn))
      end if
    end subroutine prepend_dir

    subroutine dir2str(dir,str)
      use Globals
      implicit none
      character(len=*), intent(in   ) :: dir
      character(len=*), intent(inout) :: str
      
      if( str(1:1) .ne. '/') then
         str=etb(dir)//'/'//etb(str)
      end if
    end subroutine dir2str

    
    subroutine check_open(file2read,lun,&
         file_folder_flag,&
         mandatory_optional_flag,&
         input_output_flag)
      use Globals
      implicit none
      type(file), intent(inout) :: file2read
      integer,    intent(in   ) :: lun
      integer,    intent(in   ) :: file_folder_flag
      integer,    intent(in   ) :: mandatory_optional_flag
      integer,    intent(in   ) :: input_output_flag

      ! rhs_integrated
      ! local
      logical :: file_exist
      
      if   ( file_folder_flag .eq. 1 ) then
         if ( input_output_flag .eq. 1 ) then
            inquire(file=file2read%fn,exist=file_exist)
            if ( file_exist ) then
               file2read%exist = .true.
               open(file2read%lun,file=file2read%fn,iostat=opstat)
               if(opstat.ne.0) then
                  write(lun,*) ' failed to open unit ',file2read%lun,&
                       ' to be linked to file ',etb(file2read%fn)
                  stop ' simulation ended'
               end if
            else 
               if ( mandatory_optional_flag .eq. 2 ) then
                  write(lun,*) 'File ', etb(file2read%fn), ' not found'
                  write(lun,*) 'Optional,  simulation continues'
               else
                  write(lun,*) 'Mandatory File ', &
                       etb(file2read%fn), 'not found'
                  stop ' simulation ended'
               end if
            end if
         else if ( input_output_flag .eq. 2 )  then
            if ( mandatory_optional_flag .eq. 1 ) then 
               open(file2read%lun,file=file2read%fn,iostat=opstat)
               if(opstat.ne.0) then
                  write(lun,*) ' failed to open unit ',file2read%lun,&
                       ' to be linked to file ',etb(file2read%fn)
                  stop ' simulation ended'
               else
                  file2read%exist = .true.
               end if
            end if
         end if
      else if ( file_folder_flag .eq. 2 ) then        
         inquire(file=etb(file2read%fn),exist=file_exist)
         if(.not.file_exist) then
            call execute_command_line ('mkdir -p ' // etb(file2read%fn))
         end if
         file2read%exist= .true.
         open(file2read%lun,file=etb(file2read%fn)//'deleteme',&
              iostat=opstat)
         if (opstat.ne.0) then
            write(lun,*) ' problems creating or accessing folder directory ', &
                 etb(file2read%fn)
            stop ' simulation ended'
         end if
         close(file2read%lun)
         call execute_command_line ('rm ' // etb(file2read%fn)//'deleteme')  
      end if

    end subroutine check_open
    
  end subroutine IO_initialize

  !-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type IOfdescr)
  !> Prints content of a variable of type IOfdescr
  !>
  !> usage:
  !>    call 'var'%info(lun)
  !>
  !> where:
  !> \param[in] lun: output unit
  !-------------------------------------------------------------
  subroutine IO_print_info(this, lun)
    class(IOfdescr) :: this
    integer :: lun

    write(lun,*) ' IO Units used in the code'
    
    write(lun,*) ' '
    write(lun,*) ' Standard units'
    call this%term%info(lun)
    call this%stderr%info(lun)
    call this%stdin%info(lun)
    call this%stdout%info(lun)

    write(lun,*) ' '
    write(lun,*) ' Simulation units'
    write(lun,*) ' Input units'
    call this%input_folder%info(lun)
    call this%control%info(lun)
    write(lun,*) ' Spatial discretiazion  units '
    call this%grid%info(lun)
    call this%subgrid%info(lun)
    call this%parents%info(lun)
    call this%pflux%info(lun)
    call this%pmass%info(lun)
    call this%decay%info(lun)
    call this%tdens0%info(lun)
    call this%kappa%info(lun)
    call this%rhs_integrated%info(lun)
    write(lun,*) ' Output units'
    call this%output_folder%info(lun)
    call this%statistic%info(lun)
    call this%result%info(lun)
    call this%tdens_out%info(lun)
    call this%pot_out%info(lun)
    call this%nrmgraddyn_out%info(lun)
    call this%timefun%info(lun)    
    call this%linear_sys%info(lun)    
    call this%debug%info(lun)
    write(lun,*) ' Optional files units' 
    call this%optdens%info(lun)
    call this%rhs_grid_integrated%info(lun)
    call this%penalty%info(lun)
    call this%penalty_weight%info(lun)
    call this%dirichlet_bc%info(lun)
    call this%penalty_factor%info(lun)
    call this%lambda%info(lun)


    
  end subroutine IO_print_info
  
  !-------------------------------------------------------------
  !> Static destructor.
  !> (public procedure for type IOfdescr)
  !> Deallocate a variable of type IOfdescr and close I/O units
  !>
  !> usage:
  !>    call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !-------------------------------------------------------------
  subroutine IO_close_all_units(this,lun_err)
    class(IOfdescr),intent(inout) :: this
    integer, intent(in)           :: lun_err
    ! input units
    close(1)
    call this%input_folder%kill(lun_err)
    call this%control%kill(lun_err)
    call this%grid%kill(lun_err)
    call this%subgrid%kill(lun_err)
    call this%pflux%kill(lun_err)
    call this%pmass%kill(lun_err)
    call this%decay%kill(lun_err)
    call this%tdens0%kill(lun_err)
    call this%rhs_integrated%kill(lun_err)
    call this%kappa%kill(lun_err)
    ! output units 
    call this%output_folder%kill(lun_err)
    call this%statistic%kill(lun_err)
    call this%result%kill(lun_err)
    call this%tdens_out%kill(lun_err)
    call this%pot_out%kill(lun_err)
    call this%nrmgraddyn_out%kill(lun_err)        
    call this%timefun%kill(lun_err)
    call this%linear_sys%kill(lun_err)
    call this%debug%kill(lun_err)
    ! optional 
    call this%optdens%kill(lun_err)
    call this%rhs_grid_integrated%kill(lun_err)
    call this%penalty%kill(lun_err)
    call this%penalty_weight%kill(lun_err)
    call this%dirichlet_bc%kill(lun_err)
    call this%penalty_factor%kill(lun_err)
    call this%lambda%kill(lun_err)


  end subroutine IO_close_all_units  
  
  !>-------------------------------------------
  !> Short-cut fucntion to find the logical unit
  !> associate to a certain file
  !>-------------------------------------------
  function find_lun(this,string) result (lun)
    use Globals
    implicit none
    class(IOfDescr),    intent(in   ) :: this
    character(len=256), intent(inout) :: string
    integer :: lun

    select case ( etb(erase_comment(string)) )
    case ('stderr')
       lun=this%stderr%lun
    case ('stdout')
       lun=this%stdout%lun
    case ('debug')
       lun=this%debug%lun
    case ('statistic')
       lun=this%statistic%lun
    case default
       lun=-1000
    end select
  end function find_lun
    
end module IOdefs
