module TdensPotentialSystem
  use Globals
  use MuffeTiming
  use AbstractGeometry
  use P1Galerkin
  use OdeInputs
  use LinearOperator
  use Matrix
  use SimpleMatrix
  use SparseMatrix
  use CombinedSparseMatrix
  use InexactConstraintPreconditioner
  use PreconditionerTuning
  use FullMatrix
  use FullPreconditioner
  use RankOneUpdate
  use BlockMatrix
  use StdSparsePrec
  use Eigenv
  use ControlParameters
  use Scratch
  use LinearSolver
  use DmkOdeData
  implicit none
  private 
  type, extends(abs_linop),public :: spkernel
     integer :: nkernel=0
     real(kind=double), allocatable :: base_kernel(:)
     integer, allocatable :: indeces(:)
   contains
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: init => spkernel_init
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: kill => spkernel_kill
     !> Procedure to initialized
     !> (public procedure for type spmat)
     procedure, public, pass :: set => spkernel_set
     !> Procedure to compute 
     !>         y = M * x 
     !> (public procedure for type spmat)
     procedure, public, pass :: Mxv => spkernel_Mxv
  end type spkernel
  
  !>-------------------------------------------------------------------
  !> Structure Variable containg member for the discretization
  !> of Physarum Polycephalum ODE equation 
  !> or the gradient flow equation in tdens variable
  !> or the gradient flow equation in gfvar varible
  !> with PO-P1 scheme, P0 for $\Tdens$, P1 for $\Pot$, with 
  !> P1 that can be defined of the same grid of $\Tdens$ or the
  !> conformal refinement.
  !> It contains all linear algebra quantities for the 
  !> computation of the time evolution.
  !>-------------------------------------------------------------------
   type, public :: p1p0_space_discretization
     !> Number of degrees of freedom of ntdens
     integer :: ntdens
     !> Number of degrees of freedom of pot
     integer :: npot
     !> Number of degrees of freedom of system tdens + pot
     integer :: nfull
     !> Number of degrees of gradient of gradient
     integer :: ngrad
     !> Number of degrees of pot
     integer :: ambient_dimension
     !>-----------------------------------------------------------------
     !> Geometrical info
     !>-----------------------------------------------------------------
     !> Flag for two-level grid
     integer :: id_subgrid
     !> Mesh for tdens
     type(abs_simplex_mesh), pointer  :: grid_tdens
     !> Mesh for pot
     type(abs_simplex_mesh), pointer  :: grid_pot
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: nrm_grad_dyn(:)
     !>-----------------------------------------------------------------
     !> Finite Elements Scheme 
     !> P1-Galerkin variables (built on grid_pot)
     type(p1gal)    :: p1
     !>-----------------------------------------------------------------
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spmat) :: stiff
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spkernel) :: near_kernel
     !> Kernel for pot-tdens system
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spkernel) :: near_kernel_full
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spmat) :: stiff_laplacian
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(inverse) :: inverse_stiff
     !>-----------------------------------------------------------
     !> Block matrix type containng Jacobian
     !> ( A            BT )
     !> ( -deltat D1 C  D2 ) 
     type(blockmat)  :: jacobian_full
     !> Block matrix type containng augmented Jacobian
     !>  ( A_gamma       B_gamma^T )
     !>  ( -deltat D1 C  D2        ) 
     type(augmented_prec)  :: triangular_augmented_prec
     !>  P = ( A_gamma      B^T )
     !>      ( 0            W        ) 
     type(blockmat)  :: augmented_jacobian
     !> A_gamma=Stiff(\Tdens)+gamma *B
     !>  ( A_gamma       B_gamma^T )
     !>  ( -deltat D1 C  D2        ) 
     type(combspmat)  :: stiff_gamma
     !> A_gamma=Stiff(\Tdens)+gamma *B
     type(spmat)  :: stiff_gamma_assembled
     !> A_gamma=Stiff(\Tdens)+gamma *B
     !>  ( A_gamma       B_gamma^T )
     !>  ( -deltat D1 C  D2        ) 
     type(spmat)  :: BT_gamma
     !> Combined matrix A+deltat BT D2^{-1} D1 C
     !> for reduced jacobian
     type(combspmat) :: jacobian_reduced
     !> Block matrix type containng Jacobian
     !> ( A  BT )
     !> ( B  G  ) 
     type(blockmat) :: sym_jacobian_full 
     !> Block matrix type containng Jacobian
     !> ( A            BT   diag(\Gfvar)  )
     !> ( diag(\Gfvar) B    1/deltat - D2 ) 
     type(blockmat)  :: gf_jacobian_full     
     ! kernel of full jacobian
     real(kind=double), allocatable  :: kernel_full(:,:)
     !>---------------------------------------------------------
     !> Jacobian component
     !>---------------------------------------------------------
     !> Diagonal matrix use to define G action
     !> Compenent of symmmetic jacobian
     type(diagmat)  :: G_matrix
     !> Diagonal matrix use to define D2 action
     type(diagmat) :: D_matrix
     !> Diagonal matrix use to define D2 action
     type(diagmat) :: inv_D_matrix
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: D1(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: D2(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: D3(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: invD2(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: invD2_D1(:)     
     !> Dimension (ntdens)
     !> Tdens-part of Newton function 
     real(kind=double), allocatable :: fnewton_tdens(:)
     !> Dimension (ntdens)
     !> Gfvar-part of Newton function 
     real(kind=double), allocatable :: fnewton_gfvar(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: fnewton_pot(:)
     !> Dimension (ntdens)
     !> Tdens-part of Newton function 
     real(kind=double), allocatable :: fnewton_tdens_old(:)
     !> Dimension (ntdens)
     !> Gfvar-part of Newton function 
     real(kind=double), allocatable :: fnewton_gfvar_old(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: fnewton_pot_old(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: rhs_reduced(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: diagonal_weight(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: inv_diagonal_weight(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: diagonal_scale(:)
      !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: inv_diagonal_scale(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: hatS(:)
     real(kind=double), allocatable :: MP_coeff(:)
     !>-----------------------------------------------------------------
     !> Logical Flag to mark equality between
     !> Matric B and C
     logical ::  BequalC
     !> Dimension(ntdens,npot)
     !> Matrix B for ei_newton for two-level grids
     !> B_{k,i}=\int_{T_k} \Grad \Pot \Psi_i
     type(spmat) :: B_matrix
     !> Dimension(ntdens,npot)
     !> Matrix BT for ei_newton for two-level grids
     !> B_{i,k}=\int_{T_k} \Grad \Pot \Psi_i
     type(spmat) :: BT_matrix
     !> Dimension(ntdens,npot)
     !> Matrix B for ei_newton for two-level grids
     !> B_{k,i}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
     type(spmat) :: DB_matrix
     !> Dimension(ntdens,npot)
     !> Matrix BT for ei_newton for two-level grids
     !> B_{i,k}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
     type(spmat) :: BTD_matrix
     !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
     !> Trija pointer for assembly of B_matrix
     integer, allocatable :: assembler_Bmatrix_subgrid(:,:)
     !> Dimension(grid_tdens%nnodeincell, grid_tdens%ncell ) 
     !> Trija pointer for assembly of B_matrix 
     !> with no subgrid
     integer, allocatable :: assembler_Bmatrix_grid(:,:)
     !> Dimension(B_matrix%nterm) 
     !> Integer pointer to redirect non-zero term of B_matrix
     !> into non-zero term of BT_matrix = (B_matrix)^T
     integer, allocatable :: transposer(:)
     !>-----------------------------------------------------------------
     !> Dimension(ntdens,npot)
     !> Matrix C for ei_newton for two-level grids
     !> $C_{k,i}=\int_{T_k} \Grad \Pot \Psi_i * weight_{k}$
     !> where  $weight_{k}$ depends on the pflux exponent
     type(spmat) :: C_matrix
     !> Dimension(npot,npot)
     !> Transpose Matrix C 
     type(spmat) :: CT_matrix
     !> Dimension(ntdens,npot)
     !> Transpose Matrix M
     !>   M = deltat D1 * C_matrix
     !> It is the block 1,2 of the jacobian
     type(spmat) :: deltatD1C_matrix
     !> Dimension(npot,npot)
     !> Matrix conating the ia ja pattern for matrix
     !> BTB or BTC for ei_newton 
     type(spmat) :: BTDC_matrix
     !> Dimension(ntdens,ntdens)
     !> Matrix coanting the ia ja pattern for matrix
     !> B diagonal BT 
     type(spmat) :: BDBT
     !> Dimension(stiff%nterm) 
     !> Redirector of stiff matrix into BTDC
     integer, allocatable :: stiff2BTDC(:)
     !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
     !> Trija pointer for assembly of B_matrix
     !> Scratch array for PCG and BICGSTAB  Procedure
     type(scrt) :: aux_bicgstab
     !> Scratch array for implicit_euler_newton
     type(scrt) :: aux_newton
     !> Work array form rhs of linear system
     real(kind=double), allocatable :: rhs(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: rhs_ode(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: inc_ode(:)
     !> Dimension (ncellpot)
     !> Work array form update procedure
     real(kind=double), allocatable :: tdens_prj(:)
     !>---------------------------------------------------------------
     !> Preconditioner for sparse linear systems
     !>---------------------------------------------------------------
     !> Back up Sparse preconditioner for PCG procedure
     type(stdprec) :: standard_prec_saved
     type(fullmat) :: der_pot_tdens_jacobian
     type(fullmat) :: tdens_jacobian
          
     !> BFGS update
     type(bfgs) :: bfgs_prec
     !> 
     type(fullmat) :: matrix_V
     type(fullmat) :: matrix_AV
     type(fullmat) :: matrix_VTAV
     type(precfull) :: matrix_PI
     !> Sparse preconditioner stifnees of laplacian
     type(stdprec) :: prec_laplacian
     !> Back up Sparse preconditioner for PCG procedure
     type(inverse) :: approx_inverse
     !> Back up of sparse preconditioner of Schur complement BTDT+G
     type(stdprec) :: prec_Schur
     !> Constrained preconditioner for solving full jacobian 
     !> in Newton method for Implcit Euler 
     type(constrained_prec) :: icprec_full
     !> Mixed Constrained preconditioner for solving full jacobian 
     !> in Newton method for Implcit Euler 
     type(mixed_constrained_prec) :: mix_icprec_full
     !> Triangular preconditioner for solving full jacobian 
     !> in Newton method for Implcit Euler 
     type(triangular_constrained_prec)  :: triang_prec
     ! Array involded in symmetrization of jacobian
     ! Dimension (ntdens)
     real(kind=double), allocatable :: symmetrizer(:)
     !> Diagonal precondtioner with diag(A)^{-1}
     type(diagmat)  :: inverse_diag_stiff
     !> Block diagonal precondittioner
     !> P ~= diag(A,D2)^{-1}
     type(block_linop) :: prec_full
     !> Diagonal precondittioner
     !> P = D2^{-1}
     type(diagmat) :: inverse_D2
     !>-----------------------------------------------------------
     !> Broyden_update stragey for preconditioning 
     !> Linear system from Newton iterattions
     !>-----------------------------------------------------------
     !> Number of maximum broyden_updates
     integer :: nbroyden_update
     !> Actual broyden updates
     type(broyden), allocatable  :: broyden_updates(:)
     !> Pointer to final preconditioenr to be used
     type(array_linop) , allocatable :: broyden_sequence(:)
     !> Pointer to final preconditioenr to be used
     type(shmr) , allocatable :: rankone_update(:)
     !> Quasi newton rank-k updates
     !type(rankk_mat) :: quasi_newton_updates
     !>-----------------------------------------------------------
     !> Scratch arrays for general porpuse
     !>-----------------------------------------------------------
     !> Dimension (ngrad)
     !> Work array 
     real(kind=double), allocatable :: scr_ngrad(:)
     !> Dimension (ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     integer, allocatable :: scr_integer(:)
     !> Dimension (npot)
     !> Diagonal of the laplacian 
     real(kind=double), allocatable :: diagonal_laplacian(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: norm_rows_stiff(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: norm_rows_BT(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_nfull(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: rhs_full(:)
     !> Dimension (npot)
     !> Work array to store D(A)^{-1/2} in diagonal scaling procedure
     real(kind=double), allocatable :: sqrt_diag(:)
     !> Scratch diagonal matrix
     type(diagmat) :: scr_diagmat_ntdens
     !>-----------------------------------------------------------
     !> Eigen. conteiner for DACG procedure
     type(eigen) :: spectral_info
   contains
     !> Static constructor 
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: init => init_p1p0
     !> Static destructor
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: kill => kill_p1p0
     !> Static destructor
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: build_grad_vars
     !> Static destructor
     !> (procedure public for type p1p0_space_discretization)
     !procedure, public , pass :: tdens2pot
     !> Procedure for building
     !> $\int _{T_r} |\nrm_grad|^\Pode $ / |T_r|
     !> with \Pode=\Pflux for PP dynamic
     !>      \Pode=2      for GF dynamic
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: build_nrm_grad_dyn
     !>-------------------------------------------------------------
     !> Procedure for assembly the  matrix 
     !> $\Matr[\Itd,\Ipot]{B} =
     !> \int_{\Domain} |\Grad \Pot|^{\Pode-2} \testx_{\Itd} \Grad \Pot \Grad \testp _{\Ipot}$
     !> (procedure public for type p1p0_space_discretization)
     procedure, private , pass :: assembly_BC_matrix
     !> Procedure $\Gfvar$-part of Newton function
     !> (procedure public for type p1p0_space_discretization)
     procedure, private , pass :: assembly_fnewton_gfvar
     !> Procedure $\Tdens$-part of Newton function
     !> (procedure public for type p1p0_space_discretization)
     procedure, private , pass :: assembly_fnewton_tdens
     !> Procedure $\Pot$-part of Newton function
     !> (procedure public for type p1p0_space_discretization)
     procedure, private , pass :: assembly_fnewton_pot     
     !>-------------------------------------------------------------------------
     !> Procedure for assembly the stiffness matrix 
     !> A_{i,j} = \int \Tdens \Grad_i \Grad j
     !> array tdens
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: assembly_stiffness_matrix
     !> Subroutine for assembling right-hand side of 
     !> Tdens (or Gfvar) ODE
     procedure, public , pass :: assembly_rhs_ode
     !> Subroutine to get the tdens increment from the rhs_ode
     !> Tdens (or Gfvar) ODE
     procedure, public , pass :: get_increment_ode
     !> Subroutine for assembling all varaibles 
     !> involved in the implicit euler time-stepping
     !> via Newton method
     procedure, private , pass :: assembly_newton => assembly_newton_p1p0
     !> Subroutine for writing to file all varaibles 
     !> involved in the implicit euler time-stepping
     !> via Newton method
     procedure, private , pass :: write_newton => write_newton_p1p0
     !> Subroutine to solve linear system
     !> J inc = -F 
     !> in newton iteration
     procedure, private , pass :: invert_jacobian
     !> Subroutine to solve linear system
     !> J inc = -F 
     !> in newton iteration
     procedure, private , pass :: invert_jacobian_general
     !>----------------------------------------------------------------
     !> Scalar and integer fucntional descibing important
     !> caracteristic of the state of tdens-pot system
     !>---------------------------------------------------------------
     !> Subroutine for computation of scalar and integer
     !> functionals like energy, lyapunov, etc
     procedure, public , pass :: evaluate_functionals
     !>--------------------------------------------------------------
     !> Scalar functions
     !>--------------------------------------------------------------
     !> Function eval $\int \tdens $
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: mass
     !> Scalar functions
     !> Function eval $\int \tdens^{\WPower{\Pflux,\Pmass}$
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: w_mass
     !> Function eval $\int \tdens |\nabla u |^2 $
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: energy
     !> Function eval $\Ene(\tdens) + \Wmass(\tdens)$
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: lyap
     !> Function eval $\int \tdens log( \tdens ) $
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: ent_tdens
     !> Function eval 
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: eval_var_tdens
     !> Function evaluating candidate branch exponet 
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: pvel_exponent
     !> Function evaluating candidate branch exponet 
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: exponent_wmass
     !> Function evaluating
     !> $\int_{\Omega}|q|^{power}$
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: integral_flux
     !> Function evaluating
     !> $\int_{\Omega} u f $
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: integral_potforcing
     !> Function evaluating
     !> $\int_{\Omega}|q|^{power}- uf $
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: duality_gap
     !> Procedure to tell active and frezeed regions
     !> (procedure public for type p1p0_space_discretization)
     procedure, public , pass :: set_active_regions
  end type p1p0_space_discretization

   !>-------------------------------------------------------------------
  !> Structure Variable containg member for the discretization
  !> of Physarum Polycephalum ODE equation 
  !> or the gradient flow equation in tdens variable
  !> or the gradient flow equation in gfvar varible
  !> with PO-P1 scheme, P0 for $\Tdens$, P1 for $\Pot$, with 
  !> P1 that can be defined of the same grid of $\Tdens$ or the
  !> conformal refinement.
  !> It contains all linear algebra quantities for the 
  !> computation of the time evolution.
  !>-------------------------------------------------------------------
   type, public :: p1p1_space_discretization
     !> Number of degrees of freedom of ntdens
     integer :: ntdens
     !> Number of degrees of freedom of pot
     integer :: npot
     !> Number of degrees of freedom of system tdens + pot
     integer :: nfull
     !> Number of degrees of gradient of gradient
     integer :: ngrad
     !> Number of degrees of pot
     integer :: ambient_dimension
     !>-----------------------------------------------------------------
     !> Geometrical info
     !>-----------------------------------------------------------------
     !> Flag for two-level grid
     integer :: id_subgrid
     !> Mesh for tdens
     type(abs_simplex_mesh), pointer  :: grid_tdens
     !> Mesh for tdens
     type(abs_simplex_mesh), pointer  :: grid_pot
     !> Mesh for tdens
     type(p1gal),pointer :: p1_tdens
     !> Mesh for tdens
     type(p1gal) :: p1_pot
     !> Dimension(ncell_tdens)
     !> $\frac{\int_{T_r} |\nabla u|}{|T_r|}
     !> with $T_r$ triangle on grid_tdens
     real(kind=double), allocatable :: nrm_grad_dyn(:)
     !>-----------------------------------------------------------------
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spmat) :: stiff
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spmat) :: mass_mat
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     type(spkernel) :: near_kernel
     !> Stiff matrix from 
     !> $ -\Div ( \Tdens \Grad \Pot ) = \Forcing  $
     !>---------------------------------------------------------
     !> Jacobian component
     !>---------------------------------------------------------
     !> Dimension (ntdens)
     !> Tdens-part of Newton function 
     real(kind=double), allocatable :: fnewton_tdens(:)
     !> Dimension (ntdens)
     !> Gfvar-part of Newton function 
     real(kind=double), allocatable :: fnewton_gfvar(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: fnewton_pot(:)
     !> Dimension (ntdens)
     !> Tdens-part of Newton function 
     real(kind=double), allocatable :: fnewton_tdens_old(:)
     !> Dimension (ntdens)
     !> Gfvar-part of Newton function 
     real(kind=double), allocatable :: fnewton_gfvar_old(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: fnewton_pot_old(:)
     !> Dimension (npot)
     !> Pot-part of Newton function 
     real(kind=double), allocatable :: rhs_reduced(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: diagonal_weight(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: inv_diagonal_weight(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: diagonal_scale(:)
      !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: inv_diagonal_scale(:)
     !> Dimension (ntdens)
     !> Weight use in the augmented jacobian
     real(kind=double), allocatable :: hatS(:)
     real(kind=double), allocatable :: MP_coeff(:)
     !>-----------------------------------------------------------------
     !> Logical Flag to mark equality between
     !> Matric B and C
     logical ::  BequalC
     !> Dimension(ntdens,npot)
     !> Matrix B for ei_newton for two-level grids
     !> B_{k,i}=\int_{T_k} \Grad \Pot \Psi_i
     type(spmat) :: B_matrix
     !> Dimension(ntdens,npot)
     !> Matrix BT for ei_newton for two-level grids
     !> B_{i,k}=\int_{T_k} \Grad \Pot \Psi_i
     type(spmat) :: BT_matrix
     !> Dimension(ntdens,npot)
     !> Matrix B for ei_newton for two-level grids
     !> B_{k,i}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
     type(spmat) :: DB_matrix
     !> Dimension(ntdens,npot)
     !> Matrix BT for ei_newton for two-level grids
     !> B_{i,k}=\int_{T_k} gf_{k}\Grad \Pot \Psi_i
     type(spmat) :: BTD_matrix
     !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
     !> Trija pointer for assembly of B_matrix
     integer, allocatable :: assembler_Bmatrix_subgrid(:,:)
     !> Dimension(grid_tdens%nnodeincell, grid_tdens%ncell ) 
     !> Trija pointer for assembly of B_matrix 
     !> with no subgrid
     integer, allocatable :: assembler_Bmatrix_grid(:,:)
     !> Dimension(B_matrix%nterm) 
     !> Integer pointer to redirect non-zero term of B_matrix
     !> into non-zero term of BT_matrix = (B_matrix)^T
     integer, allocatable :: transposer(:)
     !>-----------------------------------------------------------------
     !> Dimension(ntdens,npot)
     !> Matrix C for ei_newton for two-level grids
     !> $C_{k,i}=\int_{T_k} \Grad \Pot \Psi_i * weight_{k}$
     !> where  $weight_{k}$ depends on the pflux exponent
     type(spmat) :: C_matrix
     !> Dimension(npot,npot)
     !> Transpose Matrix C 
     type(spmat) :: CT_matrix
     !> Dimension(ntdens,npot)
     !> Transpose Matrix M
     !>   M = deltat D1 * C_matrix
     !> It is the block 1,2 of the jacobian
     type(spmat) :: deltatD1C_matrix
     !> Dimension(npot,npot)
     !> Matrix conating the ia ja pattern for matrix
     !> BTB or BTC for ei_newton 
     type(spmat) :: BTDC_matrix
     !> Dimension(ntdens,ntdens)
     !> Matrix coanting the ia ja pattern for matrix
     !> B diagonal BT 
     type(spmat) :: BDBT
     !> Dimension(stiff%nterm) 
     !> Redirector of stiff matrix into BTDC
     integer, allocatable :: stiff2BTDC(:)
     !> Dimension(grid_pot%nnodeincell, grid_pot%ncell) 
     !> Trija pointer for assembly of B_matrix
     !> Scratch array for PCG and BICGSTAB  Procedure
     !> Dimension(ntdens,ntdens)
     !> Matrix coanting the ia ja pattern for matrix
     !> B diagonal BT 
     type(diagmat) :: D_matrix
     type(diagmat) :: inv_D_matrix
     type(scrt) :: aux_bicgstab
     !> Scratch array for implicit_euler_newton
     type(scrt) :: aux_newton
     !> Work array form rhs of linear system
     real(kind=double), allocatable :: rhs(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: rhs_ode(:)
     !> Dimension (ntdens)
     !> Work array form update procedure
     real(kind=double), allocatable :: inc_ode(:)
     !> Dimension (ncellpot)
     !> Work array form update procedure
     real(kind=double), allocatable :: tdens_prj(:)
     !>---------------------------------------------------------------
     !> Preconditioner for sparse linear systems
     !>---------------------------------------------------------------
     !> Back up Sparse preconditioner for PCG procedure
     type(stdprec) :: standard_prec_saved
     !>-----------------------------------------------------------
     !> Scratch arrays for general porpuse
     !>-----------------------------------------------------------
     !> Dimension (ngrad)
     !> Work array 
     real(kind=double), allocatable :: scr_ngrad(:)
     !> Dimension (ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     integer, allocatable :: scr_integer(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_nfull(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: rhs_full(:)
   contains
     !> Static constructor 
     !> (procedure public for type p1p1_space_discretization)
     procedure, public , pass :: init => init_p1p1
     !> Static destructor
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: kill => kill_p1p1
     !> Static destructor
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: build_grad_vars
     !> Static destructor
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: tdens2pot
     !> Procedure for building
     !> $\int _{T_r} |\nrm_grad|^\Pode $ / |T_r|
     !> with \Pode=\Pflux for PP dynamic
     !>      \Pode=2      for GF dynamic
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: build_nrm_grad_dyn
     !>-------------------------------------------------------------
     !> Procedure for assembly the  matrix 
     !> $\Matr[\Itd,\Ipot]{B} =
     !> \int_{\Domain} |\Grad \Pot|^{\Pode-2} \testx_{\Itd} \Grad \Pot \Grad \testp _{\Ipot}$
     !> (procedure public for type p1p1_space_discretization)
     !procedure, private , pass :: assembly_BC_matrix
     !> Procedure $\Gfvar$-part of Newton function
     !> (procedure public for type p1p1_space_discretization)
     !procedure, private , pass :: assembly_fnewton_gfvar
     !> Procedure $\Tdens$-part of Newton function
     !> (procedure public for type p1p1_space_discretization)
     !procedure, private , pass :: assembly_fnewton_tdens
     !> Procedure $\Pot$-part of Newton function
     !> (procedure public for type p1p1_space_discretization)
     !procedure, private , pass :: assembly_fnewton_pot     
     !>-------------------------------------------------------------------------
     !> Procedure for assembly the stiffness matrix 
     !> A_{i,j} = \int \Tdens \Grad_i \Grad j
     !> array tdens
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: assembly_stiffness_matrix
     !> Subroutine for assembling right-hand side of 
     !> Tdens (or Gfvar) ODE
     !procedure, public , pass :: assembly_rhs_ode
     !> Subroutine to get the tdens increment from the rhs_ode
     !> Tdens (or Gfvar) ODE
     !procedure, public , pass :: get_increment_ode
     !> Subroutine for assembling all varaibles 
     !> involved in the implicit euler time-stepping
     !> via Newton method
     !procedure, private , pass :: assembly_newton => assembly_newton_p1p1
     !>----------------------------------------------------------------
     !> Scalar and integer fucntional descibing important
     !> caracteristic of the state of tdens-pot system
     !>---------------------------------------------------------------
     !> Subroutine for computation of scalar and integer
     !> functionals like energy, lyapunov, etc
     !procedure, public , pass :: evaluate_functionals
     !>--------------------------------------------------------------
     !> Scalar functions
     !>--------------------------------------------------------------
     !> Procedure to tell active and frezeed regions
     !> (procedure public for type p1p1_space_discretization)
     !procedure, public , pass :: set_active_regions
  end type p1p1_space_discretization

  

 

  !>-------------------------------------------------------------------
  !> Structure Variable containg member for the discretization
  !> of PP ODE equation
  !> \begin{align}
  !>   &-\Div( \Tdens \nabla \Pot) = f \\
  !>   & \Dt{\Tdens}=(\Tdens |\nabla \Pot|)^{\Pflux}-decay kappa \Tdens^Pmass
  !> \end {align}
  !> or the gradient flow equation
  !> \begin{align}
  !>  &-\Div( \Tdens \nabla \Pot) = f \\
  !>  &\Tdens'=(\Tdens )^{\Pflux}|\nabla\Pot|^2-decay kappa \Tdens^Pmass
  !> \end {align}
  !> with PO-P1 scheme, P0 for $\Tdens$, P1 for $\Pot$, with 
  !> P1 that can be defined of the same grid of $\Tdens$ or the
  !> conformal refinement.
  !> It contains the structure variable odein with the inputs of the ODE
  !>-------------------------------------------------------------------
  type, public :: tdpotsys
     !> Array length of tdens and pot
     !> Length of tdens array
     integer :: ntdens
     !> Length of pot array
     integer :: npot
     !> Length of full system tdens + pot 
     integer :: nfull
     !>-----------------------------------------------------------------
     !> Dimension(ntdens)
     !> Piecewise constant conductivity
     real(kind=double), allocatable :: tdens(:)
     !> Piecewise constant cond. at previuos time step
     !> Used in evol_time
     real(kind=double), allocatable :: tdens_old(:)
     !> Dimension(npot)
     !> Coefficient of P1-galerkin Discretization
     real(kind=double), allocatable :: pot(:)
     !> Dimension(npot)
     !> Coefficient of P1-galerkin Discretization
     real(kind=double), allocatable :: pot_old(:)
     !> Dimension(ntdens)
     !> Gradient flow variable
     real(kind=double), allocatable :: gfvar(:)
     !> Dimension(ntdens)
     !> Gradient flow variable
     real(kind=double), allocatable :: gfvar_old(:)
     !>----------------------------------------------------------------
     !> Derived variables
     !> (vars that can be computed once tdens and pot are known)
     !>----------------------------------------------------------------
     !> Array length for work and auxiliary array
     !> Nmb of triangles of grid_pot
     integer :: ngrad
     !> Dimension(3,ngrad)
     !> x,y,z component of the gradient on grid_pot cells
     real(kind=double), allocatable :: gradpot(:,:)
     !> Dimension(3,ntdens)
     !> x,y,z component of the gradient on grid_tdens cells
     real(kind=double), allocatable :: gradpot_avg(:,:)
     !> Dimension(ngrad)
     !> Norm the gradient over the triangles
     real(kind=double), allocatable :: nrm_grad(:)
     !> Dimension(ntdens)
     !> Norm the gradient over the triangles of grid_tdens
     !> It the average of norm_grad in the two-grid scheme
     real(kind=double), allocatable :: nrm_grad_avg(:)
     !>-----------------------------------------------------------------
     !> Linear system variables
     !>-----------------------------------------------------------------
     !> Number of fix point iteration
     integer :: time_iteration=0
     !> Info/outputs for linear solver 
     type(output_solver) :: info_solver
     !> Flag for error in preconditioner building
     !>   info_prec=0 => no errors
     !> other flags>0 are descibed in precondioner modules
     integer :: info_prec
     !> Flag for error in tuning building
     !>   info_prec=0 => no errors
     !> other flags>0 some errors occurs
     integer :: info_tuning
     !> Logical unit for statistic file
     integer :: lun_stat
     !> Total number of linear system
     integer :: nlinear_system=0
     !>----------------------------------------------------------
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: tdpot_syncr=.false.
     !> Error flag if the variable 
     !> Tdens Pot ODEin and Grad variable
     !> at the same time 
     logical :: all_syncr=.false.
     !>--------------------------------------------------
     !> Scratch varibles for storing 
     !> Averaged cgs iterations for time step
     integer :: iter_media
     !> Total cgs iterations 
     integer :: total_iteration
     !> Scratch varible to control the calulation of prec.
     integer :: iter_last_prec
     !> Scratch varible to store number of iteration
     integer :: iter_first_system
     !> Scratch varible to store number of iteration
     integer :: total_iterations_linear_system=0
     !> Scratch varible to store number of iteration
     integer :: total_number_linear_system=0
     !> Scratch varible to control the calulation of prec.
     integer :: itemp_last_prec
     !> Scratch varible to control the calulation of prec.
     integer :: ref_iter
     !> Scratch varible to store newton iteration when 
     !> Preconditioner was computed
     integer :: iter_newton_last_prec=0
     !> Scratch varible to count newton restart
     integer :: nrestart_newton=0
     !> Scratch varible to count newton restart
     integer :: nrestart_invert_jacobian=0
     !> Scratch varible to store the resisual of linear system
     !> | Stiff(\Tdens) \Pot - rhs | / | rhs |
     real(kind=double) :: res_elliptic
     !>---------------------------------------------------------
     !> Real valued quantities
     !>---------------------------------------------------------
     !> Real scalar containing 
     !> $\int \Tdens \dx$
     real(kind=double) :: mass_tdens
     !> Real scalar containing 
     !> $1/2 \int \Tdens^{P(\Pflux)} \dx / P(\Pflux)$
     real(kind=double) :: weighted_mass_tdens
     !> Real scalar containing 
     !> $1/2 \int \Tdens |\Grad \Pot|^2 \dx$
     real(kind=double) :: energy
     !> Real scalar containing 
     !> $\Lyap(\Tdens)+\Wmass(Tdens)$
     real(kind=double) :: lyapunov
     !> Real scalar containing 
     !> $\min(\Tdens)$
     real(kind=double) :: min_tdens
     !> Real scalar containing 
     !> $\max(\Tdens)$
     real(kind=double) :: max_tdens
     !> Real scalar containing 
     !> $\max(|\Vel|)$
     real(kind=double) :: max_velocity
     !> Real scalar containing 
     !> $\max(|\Grad \Pot|)$
     real(kind=double) :: max_nrm_grad
     !> Real scalar containing 
     !> $\max(|\Grad \Pot|)$
     real(kind=double) :: max_nrm_grad_avg
     !> Real scalar containing 
     !> $\max(D3)=max(\Jac_{2, 2})$
     real(kind=double) :: max_D3
     !> Real scalar containing 
     !> Real scalar containing 
     !> $\int |\Vel|^{\Pvel} \dx$
     real(kind=double) :: integral_flux_pvel
     !> Real scalar containing 
     !> $\int |\Vel|^{\Pvel} - \Pot \Forcing\dx$
     real(kind=double) :: duality_gap
     !> Real scalar containing 
     !> Errors w.r.t. Tdens solution
     real(kind=double) :: err_tdens
     !> Real scalar containing 
     !> Errors w.r.t. Pot solution
     real(kind=double) :: err_pot
     !> Real scalar containing 
     !> Errors w.r.t. Pot solution
     real(kind=double) :: err_wasserstein
     !> Real scalar containing 
     !> estimated Wasserstein 1 distance
     real(kind=double) :: wasserstein_distance
     !>--------------------------------------------------------
     !> Info for non-linear solver
     !> Number of fix point iteration
     integer :: iter_nonlinear
     !> Linear system solver info
     type(output_solver), allocatable :: sequence_info_solver(:)
     !> Controls of preconditioner construction
     real(kind=double), allocatable :: sequence_inc_norm(:)
     !> Controls of preconditioner construction
     integer, allocatable :: sequence_build_prec(:)
     !> Controls of preconditioner construction
     integer, allocatable :: sequence_build_tuning(:)
     !> Number of fix point iteration
     integer :: nfillin_used
     !> Constant of contraction of fix point iteration
     real(kind=double) :: cnst_nonlinear
     !>--------------------------------------------------------
     !> Scratch arrays
     !>--------------------------------------------------------
     !> Scratch var_tdens
     real(kind=double) :: loc_var_tdens=1e30
     !>-----------------------------------------------------------
     !> Scratch arrays for general porpuse
     !>----------------------------------------------------------
     !> Dimension (ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_nfull(:)
     !> Dimension (ncellpot)
     !> Work array 
     real(kind=double), allocatable :: scr_ngrad(:)
     !>---------------------------------------------------------
     !> Selcection procedure
     !>---------------------------------------------------------
     !> Number of active tdens
     integer :: ntdens_on
     !> Number of active nodes
     integer :: npot_on
     !> Number of active tdens
     integer :: ntdens_off
     !> Number of active nodes
     integer :: npot_off
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: active_tdens(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: active_pot(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: inactive_tdens(:)
     !> Dimension (tdens)
     !> List of active tdens
     integer, allocatable :: inactive_pot(:)
     !> Dimension (tdens)
     !> List of active tdens
     logical, allocatable :: onoff_tdens(:)
     !> Dimension (tdens)
     !> List of active tdens
     logical, allocatable :: onoff_pot(:)
   contains
     !> Static constructor 
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: init => init_tdpotsys
     !> Static destructor
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: kill => kill_tdpotsys
     !> Saving procedure for data
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: write2dat => write2dat0_tdpotsys
     !> Saving procedure for data
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: write2dat3 => write2dat3_tdpotsys
     !> Saving procedure for data
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: write_opt
     !-------------------------------------------------------
     ! Subroutine for solving the elliptic equation 
     ! and related quanties
     !>-------------------------------------------------------
     !> Procedure for interfacing with linear solver subrotine
     !> with : 1 - diagonal scaling 
     !>        2 - preconditioner build and back-up
     !> (procedure public for type tdpotsys)
     !procedure, public , nopass :: my_linear_solver
     !> Procedure to obtain the solution pot that solves
     !>   $-\Div ( \tdens \nabla u) = f $
     !> once tdens is given
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: tdens2pot
     !> Procedure for building derivide varaibles
     !> - gradpot
     !> - grad_avg
     !> - nrm_grad
     !> - nrm_grad_avg
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: build_nrm_grad_vars
     !> Procedure to transform tdens into gfvar
     !> (procedure public for type tdpotsys)
     procedure, public , nopass :: tdens2gfvar
     !> Procedure to transform tdens into gfvar
     !> (procedure public for type tdpotsys)
     procedure, public , nopass :: gfvar2tdens
     !---------------------------------------------------
     procedure, public , pass :: syncronize_at_tzero
     !> Procedure for upate the wholw system
     !> to the next time step
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: update
     !> Function eval $\sup \tdens * |nrm_grad| $
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: set_threshold
     !>-----------------------------------------------------------
     !> NEWTON PRODEDURE
     !>-----------------------------------------------------------
!!$     !> Procedure for upate the whole system
!!$     !> to the next time step via backward timestepping
!!$     !> solved via newton method
!!$     !> (procedure public for type tdpotsys)
!!$     procedure, public , pass :: ei_newton  
     !> Procedure for upate the whole system
     !> to the next time step via backward timestepping
     !> solved via newton method
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: implicit_euler_newton  
     !> Procedure for upate the whole system
     !> to the next time step via backward timestepping
     !> solved via newton method
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: implicit_euler_newton_gfvar  
     !> Subroutine computing the pointer ia, ja, triaja 
     !> for assemblying the B_matrix, C_matrix used in subroutine
     !> ei_newton for two grid algorithm
     procedure, private , nopass :: set_B_matrix_general
     !> Subroutine computing the pointer ia, ja, triaja 
     !> for assemblying the B_matrix, C_matrix used in subroutine
     !> ei_newton for one grid algorithm
     procedure, private , nopass :: set_B_matrix_grid
     !> Subroutine assembly preconditioner from sparse matrix
     !> according to prec. controls
     !> 
     procedure, private , nopass :: assembly_stdprec
     !> Subroutine to assembly tuning for 
     !> preconditioner from sparse matrix
     !> according to tuning controls
     procedure, private , nopass :: assembly_tuning
     !> Subroutine adjust linear system controls
     !> in newton iterations
     !procedure, private , pass :: set_linear_solver_tolerance
     !> Procedure for control deltat in time evolution
     !> (procedure public for type tdpotsys)
     procedure, public , nopass :: control_deltat
     !> Function eval $\sup \tdens * |nrm_grad| $
     !> (procedure public for type tdpotsys)
     procedure, public , pass :: supflux
  end type tdpotsys 

      

     

     
 

contains
  !>--------------------------------------------------------------------
  !> Static Constructor, given ctrl and two meshes
  !> (public procedure for type tdpotsys)
  !> 
  !> usage: call var%init(&
  !>                IOfiles,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in ] ncell_tdens  -> integer. Nmb. of triangles of grid_tdens
  !> \param[in ] IOfiles      -> type(IOfdescr). I/O info
  !> \param[in ], ctrl        -> type(CtrlPrm). Controls variables
  !> \param[in ] grid_tdens   -> tyep(mesh). Mesh for tdens
  !> \param[in ] grid_pot     -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_tdpotsys(this,&
       lun_err,lun_out,lun_stat,&
       p1p0,&
       ctrl)

    use Globals
    use ControlParameters
    implicit none
    class(tdpotsys), target,      intent(inout) :: this
    integer,                      intent(in   ) :: lun_err
    integer,                      intent(in   ) :: lun_out
    integer,                      intent(in   ) :: lun_stat
    type(p1p0_space_discretization), intent(in) :: p1p0
    type(CtrlPrm),                intent(in   ) :: ctrl

    !local
    logical :: rc
    integer :: res
    integer :: stderr,stdout
    integer :: ntdens, ngrad, npot,nterm
    integer :: i,j,start,end,icell_sub
    integer,allocatable :: ntemp(:), temp(:,:)
    type(file) :: fmat
    ! BTB varaibles
    integer , allocatable :: mask(:),ptrc(:),count(:),rowc(:)
    integer , allocatable :: iacsc(:),jacsc(:)
    real(kind=double) , allocatable:: coeff(:) 
    real(kind=double) ::dnrm2
    integer :: flag,nzc

    stderr= lun_err
    stdout= lun_out
    this%lun_stat = lun_stat
    
    !
    ! dimension assignment
    !
    this%ntdens    = p1p0%ntdens
    this%npot      = p1p0%npot
    this%ngrad     = p1p0%grid_pot%ncell
    this%nfull     = this%ntdens + this%npot

    ! local copy for this procedure
    ntdens    = this%ntdens
    npot      = this%npot
    ngrad     = p1p0%grid_pot%ncell


    write(lun_stat,'(a)') 'allocations'
    ! system real variables
    allocate (&
         this%tdens(ntdens),&
         this%tdens_old(ntdens),&
         this%pot(npot),&
         this%pot_old(npot),&
         this%gfvar(ntdens),&
         this%gfvar_old(ntdens),&
         this%gradpot(p1p0%grid_pot%logical_dimension,ngrad),&
         this%gradpot_avg(p1p0%grid_pot%logical_dimension,ntdens),&         
         this%nrm_grad(ngrad),&
         this%nrm_grad_avg(ntdens),&
         stat=res)  

    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'system_tdenspot', &
         ' member tdens, tdens_old, pot_old' // &
         ' gradpot' // &
         ' nrm_grad, nrm_grad_avg, nrm_grad_dyn',res)

    this%tdpot_syncr = .false.
    this%all_syncr = .false.

    !
    ! linear solver info
    !
    allocate(&
         this%sequence_info_solver(ctrl%max_iter_nonlinear),&
         this%sequence_inc_norm(ctrl%max_iter_nonlinear),&
         this%sequence_build_prec(ctrl%max_iter_nonlinear),&
         this%sequence_build_tuning(ctrl%max_iter_nonlinear),&
         stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_tdpotsys', &
         ' type tdpotsys member sequence_info_solver')


    ! 
    ! work array
    ! 
    allocate(&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         this%scr_ngrad(ngrad),&
         this%scr_nfull(npot+ntdens),&
         this%active_tdens(ntdens),&
         this%active_pot(npot),&
         this%inactive_tdens(ntdens),&
         this%inactive_pot(npot),&
         this%onoff_tdens(ntdens),&
         this%onoff_pot(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_tdpotsys', &
         ' type tdpotsys member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

    this%ntdens_on = ntdens
    do i=1,ntdens
       this%active_tdens(i) = i
    end do
    this%ntdens_off = 0
    this%inactive_tdens(:)=0
    
    this%npot_on = npot
    do i=1,npot
       this%active_pot(i) = i
    end do
    this%npot_off = 0
    this%inactive_pot(:)=0
       

    
  end subroutine init_tdpotsys


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type tdpotsys)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<-----------------------------------------------------------    
  subroutine kill_tdpotsys(this,lun_err)     
    use Globals
    implicit none
    class(tdpotsys), intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    !local
    integer :: res
    logical :: rc


    
    !
    ! free array tdens pot
    !
    deallocate(&        
         this%tdens,&
         this%tdens_old,&
         this%pot,&
         this%pot_old,&
         this%gfvar,&
         this%gfvar_old,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_tdpotsys', &
         ' dealloc failed for type tdpotsys meber'//&
         ' member tdens, tdens_old, pot, pot_old',res)

    !free arrays derived variables
    deallocate(&
         this%gradpot,&
         this%gradpot_avg,&
         this%nrm_grad,&
         this%nrm_grad_avg,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         'kill_tdpotsys', &
         ' type tdpotsys members'//&
         ' grad, grad_avg, nrm_grad, nrm_grad_avg',res)

    !
    ! free work arrays
    !
    deallocate(&
         this%scr_npot,&
         this%scr_ntdens,&
         this%scr_nfull,&
         this%scr_ngrad,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         'kill_tdpotsys', &
         ' type tdpotsys meber'//&
         ' member rhs, rhs_ode, tdens_prj,'//&
         ' scr_npot, scr_ngrad, scr_ntdens'&
         ,res)
    
  end subroutine kill_tdpotsys

  subroutine write2dat0_tdpotsys(this,&
       out_format,&
       itemp,&
       time,&
       IOfiles)
    use Globals
    use IOdefs
    implicit none
    class(tdpotsys),    intent(in) :: this
    character(len=*),   intent(in) :: out_format
    integer,            intent(in) :: itemp
    real(kind=double),  intent(in) :: time
    type(Iofdescr),     intent(in) :: IOfiles

    select case  (out_format) 
    case ('timedata')
       call this%write2dat3(&
            itemp,&
            time,&
            IOfiles)

!!$    case ('hdf5')
!!$       call this%write2hdf5(&
!!$            itemp,&
!!$            time,&
!!$            0.0d0,&
!!$            IOfiles)
    end select
  end subroutine write2dat0_tdpotsys


  subroutine write2dat3_tdpotsys(this,&
       itemp,&
       time,&
       IOfiles)
    use Globals
    use IOdefs
    implicit none
    class(tdpotsys),    intent(in) :: this
    integer,            intent(in) :: itemp
    real(kind=double),  intent(in) :: time
    type(Iofdescr),     intent(in) :: IOfiles
    !local
    integer ::  stderr
    character (len=*), parameter :: rformat= '(1pe16.6,a)'
    character (len=*), parameter :: iformat= '(I16,a)'

    stderr = IOfiles%stderr%lun

    ! tdens
    call write_data_time(stderr,&
         IOfiles%tdens_out,'tdens',&
         this%ntdens,itemp,time,this%tdens)
    ! pot
    call write_data_time(stderr,&
         IOfiles%pot_out,'pot',&
         this%npot,itemp,time,this%pot)

    ! nrmgradyn
    call write_data_time(stderr,&
         IOfiles%nrmgraddyn_out,'nrm_grad_avg',&
         this%ntdens,itemp,time,this%nrm_grad_avg)

  contains
    subroutine write_data_time(stderr,&
         file2write,data_name,&
         ndata,&
         itemp,time,data)
      use Globals
      use IOdefs
      implicit none
      integer,            intent(in) :: stderr
      type(file),         intent(in) :: file2write
      character (len=*),  intent(in) :: data_name
      integer,            intent(in) :: ndata
      integer,            intent(in) :: itemp
      real(kind=double),  intent(in) :: time
      real(kind=double),  intent(in) :: data(ndata)

      !local
      logical :: rc
      integer :: res, lun, i
      character(len=256) :: fname,str
      character(len=15 ) :: rdwr
      ! character (len=*), parameter :: rformat= '(1pe16.6,a)'
      ! character (len=*), parameter :: iformat= '(I16,a)'

      lun   = file2write%lun
      fname = file2write%fn

      ! write head with dimensions
      if (itemp .eq. 0) then 
         write(lun,*, iostat=res) 1, ndata, ' !  dimensions'
         if(res .ne. 0) then
            rc = IOerr(stderr, err_out , 'write2dat3', &
                 trim(fname)// &
                 ' time',res)
         end if
      end if

      ! write head time
      write(lun,*, iostat=res) 'time  ', time
      if(res .ne. 0) then
         rc = IOerr(stderr, err_out , 'write2dat3', &
              trim(fname)// &
              ' time',res)
      end if

      ! write dimension
      write(lun,*, iostat=res) ndata
      if(res .ne. 0) then
         rc = IOerr(stderr, err_inp , 'write2dat3', &
              trim(fname)//&
              'writing ndata',res)
      end if

      ! write dimension
      do i = 1, ndata
         write(lun,*, iostat=res) i,data(i) 
         if(res .ne. 0) THEN
            write(rdwr,'(i5)') i
            str=trim(adjustl(rdwr))//'/'
            rc = IOerr(stderr, err_inp , 'write2dat3', &
                 trim(fname) // &
                 ' type tdpot member '//etb(data_name)//&
                 ' at line '//trim(str),res)
         end if
      end do

    end subroutine write_data_time


  end subroutine write2dat3_tdpotsys

  subroutine write_opt(this,&
       lun_err,&
       ctrl,&
       folder)
    use Globals
    implicit none
    class(tdpotsys), intent(in   ) :: this
    integer,         intent(in   ) :: lun_err
    type(CtrlPrm),   intent(in   ) :: ctrl
    type(file),      intent(in   ) :: folder
    logical :: rc
    integer :: res,lun,i
    character(len=256) :: foldername,fname,out_format
    
    foldername=folder%fn
    lun=folder%lun
    
    ! optdens
    fname=etb(etb(foldername)//'/opt_tdens.dat')
    out_format = ctrl%formatting('ir',2)
    open(lun,file=fname,iostat = res)
    if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
         'write_opt', &
         'err open file '//etb(fname),res)
    write(lun,*) 1, this%ntdens
    write(lun,*) ' time 0.0 ' 
    write(lun,*) this%ntdens
    do i = 1, this%ntdens
       write(lun,out_format) i, this%tdens(i)
    end  do
    write(lun,*) ' time 1.0e30 '
    close(lun)
   
    !
    ! optpot
    !
    fname=etb(etb(foldername)//'/opt_pot.dat')
    out_format = ctrl%formatting('ir',2)
    open(lun,file=fname,iostat = res)
    if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
         'write_opt', &
         'err open file '//etb(fname),res)
    write(lun,*) 1, this%npot
    write(lun,*) ' time 0.0 ' 
    write(lun,*) this%npot
    do i = 1, this%npot
       write(lun,out_format) i, this%pot(i)
    end  do
    write(lun,*) ' time 1.0e30 '
    close(lun)

    !
    ! avgnrmgrad
    !
    fname=etb(etb(foldername)//'/opt_nrm_grad_avg.dat')
    out_format = ctrl%formatting('ir',2)
    open(lun,file=fname,iostat = res)
    if(res .ne. 0) rc = IOerr(lun_err, err_IO,&
         'write_opt', &
         'err open file '//etb(fname),res)
    write(lun,*) 1, this%ntdens
    write(lun,*) ' time 0.0 ' 
    write(lun,*) this%ntdens
    do i = 1, this%ntdens
       write(lun,out_format) i, this%nrm_grad_avg(i)
    end  do
    write(lun,*) ' time 1.0e30 '
    close(lun)

  end subroutine write_opt




!!$    subroutine write2hdf5_tdpotsys(this,&
!!$         itemp,&
!!$         current_time,&
!!$         current_var,&
!!$         IOfiles)
!!$      use Globals
!!$      use IOdefs
!!$      use hdf5
!!$
!!$      implicit none
!!$      class(tdpotsys),    intent(in) :: this
!!$      integer,            intent(in) :: itemp
!!$      real(kind=double),  intent(in) :: current_time
!!$      real(kind=double),  intent(in) :: current_var
!!$      type(IOfdescr),     intent(in) :: IOfiles
!!$      
!!$
!!$      ! local
!!$      character(len=256) :: tdens_name
!!$      character(len=256) :: pot_name
!!$      character(len=256) :: nrm_grad_dyn_name
!!$      character(len=256) :: current_name
!!$      integer :: lun_err
!!$      integer(hid_t) :: tdens_group_id        
!!$      integer(hid_t) :: pot_group_id          
!!$      integer(hid_t) :: nrm_grad_dyn_group_id
!!$      integer(hid_t) :: data_type
!!$      integer, parameter :: tdens_rank=2
!!$      integer(hsize_t) :: tdens_dim(tdens_rank)
!!$      integer, parameter :: pot_rank=2
!!$      integer(hsize_t) :: pot_dim(pot_rank)
!!$      integer, parameter :: nrm_grad_dyn_rank=2
!!$      integer(hsize_t) :: nrm_grad_dyn_dim(nrm_grad_dyn_rank)
!!$      
!!$      tdens_dim(1)=1
!!$      tdens_dim(2)=this%ntdens
!!$
!!$      pot_dim(1)=1
!!$      pot_dim(2)=this%grid_pot%nnode
!!$
!!$      nrm_grad_dyn_dim(1)=1
!!$      nrm_grad_dyn_dim(2)=this%ntdens
!!$      
!!$
!!$      lun_err=IOfiles%stderr%lun
!!$
!!$      tdens_group_id        = IOfiles%tdens_out%hdf5_id
!!$      pot_group_id          = IOfiles%pot_out%hdf5_id
!!$      nrm_grad_dyn_group_id = IOfiles%nrmgraddyn_out%hdf5_id
!!$
!!$      write(current_name,'(I8)') itemp
!!$
!!$      tdens_name=etb(current_name)
!!$      call write2hdf_ddata(lun_err,&
!!$           tdens_group_id, &
!!$           tdens_name, tdens_rank, tdens_dim, &
!!$           this%tdens,&
!!$           current_time, current_var) 
!!$
!!$      pot_name=etb(current_name)
!!$      call write2hdf_ddata(lun_err,&
!!$           pot_group_id, &
!!$           pot_name, pot_rank, pot_dim, &
!!$           this%pot,&
!!$           current_time, current_var) 
!!$
!!$      nrm_grad_dyn_name=etb(current_name)      
!!$      call write2hdf_ddata(lun_err,&
!!$           nrm_grad_dyn_group_id, &
!!$           nrm_grad_dyn_name, nrm_grad_dyn_rank, nrm_grad_dyn_dim,&
!!$           this%nrm_grad_dyn,&
!!$           current_time, current_var) 
!!$
!!$    contains 
!!$      subroutine write2hdf_ddata(lun_err,&
!!$           group_id, &
!!$           data_name, data_rank, data_dim, data ,&
!!$           current_time, current_var_tdens)
!!$        use hdf5
!!$        implicit none
!!$        integer,           intent(in) :: lun_err
!!$        integer(hid_t),    intent(in) :: group_id
!!$        character(len=*),  intent(in) :: data_name 
!!$        ! integer(hid_t),    intent(in) :: data_type
!!$        integer,           intent(in) :: data_rank
!!$        integer(hsize_t),  intent(in) :: data_dim(data_rank)
!!$        real(kind=double), intent(in) :: data(..) ! assumed ranked array
!!$        real(kind=double), intent(in) :: current_time
!!$        real(kind=double), intent(in) :: current_var_tdens
!!$
!!$        ! local
!!$        integer :: error
!!$        integer(hid_t) :: data_dataspace_id
!!$        integer(hid_t) :: data_dataset_id
!!$        integer, parameter :: time_rank = 1
!!$        integer(hsize_t), dimension(time_rank) :: time_dim = (/1/)
!!$        character(len=4) :: time_name='time' 
!!$        integer(hid_t) :: time_dataspace_id
!!$        integer(hid_t) :: time_dataset_id
!!$
!!$        integer, parameter :: var_rank = 1
!!$        integer(hsize_t), dimension(var_rank) :: var_dim = (/1/)
!!$        character(len=8) :: var_name='var_tdens' 
!!$        integer(hid_t)  :: var_dataspace_id
!!$        integer(hid_t)  :: var_dataset_id
!!$
!!$        character(len=9) :: iter_name='iteration' 
!!$
!!$        
!!$
!!$        ! create data_contanier in group
!!$        CALL h5screate_simple_f (data_rank,&
!!$             data_dim, data_dataspace_id, error)
!!$        CALL h5dcreate_f(group_id,data_name,H5T_NATIVE_DOUBLE,&
!!$             data_dataspace_id, &
!!$             data_dataset_id, error)
!!$
!!$        ! write data in the container
!!$        CALL h5dwrite_f (data_dataset_id,&
!!$             H5T_NATIVE_DOUBLE, data, data_dim, &
!!$             error)
!!$
!!$        ! set up attribut "time"
!!$        CALL h5screate_simple_f(time_rank, time_dim, time_dataspace_id, error)
!!$        CALL h5acreate_f(data_dataset_id, time_name, H5T_NATIVE_DOUBLE, &
!!$             time_dataspace_id, time_dataset_id, error)
!!$        CALL h5awrite_f(time_dataset_id, H5T_NATIVE_DOUBLE,current_time,&
!!$             time_dim, error)
!!$        CALL h5aclose_f(time_dataset_id, error)
!!$        CALL h5sclose_f(time_dataspace_id, error)
!!$
!!$        ! set up attribut "var"
!!$        CALL h5screate_simple_f(var_rank, var_dim, var_dataspace_id, error)
!!$        CALL h5acreate_f(data_dataset_id, var_name, H5T_NATIVE_DOUBLE, &
!!$             var_dataspace_id, var_dataset_id, error)
!!$        CALL h5awrite_f(var_dataset_id, H5T_NATIVE_DOUBLE,current_var_tdens,&
!!$             var_dim, error)
!!$        CALL h5aclose_f(var_dataset_id, error)
!!$        CALL h5sclose_f(var_dataspace_id, error)
!!$
!!$        ! close container
!!$        CALL h5dclose_f(data_dataset_id, error)
!!$        CALL h5sclose_f(data_dataspace_id, error)
!!$
!!$      end subroutine write2hdf_ddata
!!$
!!$    end subroutine write2hdf5_tdpotsys
!!$      






  !>----------------------------------------------------
  !> Real function evaluates the energy 
  !> Joule dissipeted energy $\int \tdens |\nabla u|^2$
  !> (procedure public for type p10p0)
  !> 
  !> usage:    call var%energy(tdpot)
  !>    
  !> where:
  !> \param  [in ] tdpot  -> type(tdpotsys). Tdens/potential system
  !> \result [out] energy -> real. Lyap. functional
  !<----------------------------------------------------
  function energy(this,tdpot) result(sum)
    use Globals
    implicit none
    class(p1p0_space_discretization), intent(in) :: this
    class(tdpotsys),                  intent(in) :: tdpot
    real(kind=double) :: sum
    !local 
    integer :: itria


    if (this%id_subgrid .eq. 1) then
       sum = zero
       do itria = 1, this%grid_pot%ncell
          sum = sum + &
               tdpot%tdens( this%grid_pot%cell_parent(itria) ) * &
               ( tdpot%nrm_grad(itria) )**2 * &
               this%grid_pot%size_cell(itria)
       end do
       sum = onehalf * sum
    else
       sum = zero
       do itria = 1, this%grid_pot%ncell
          sum = sum + &
               tdpot%tdens( itria ) * &
               ( tdpot%nrm_grad(itria) )**2 * &
               this%grid_pot%size_cell(itria)
       end do
       sum = onehalf * sum
    end if

  end function energy


  !>----------------------------------------------------
  !> Real function evaluates the tdens mass 
  !> $\int \tdens \dx $ 
  !> (procedure public for type p10p0)
  !> 
  !> usage:    call var%mass(tdpot)
  !>    
  !> where:
  !> \param  [in ] tdpot -> type(tdpotsys). Tdens/potential system
  !> \result [out] mass  -> real. Tdens integeral
  !<----------------------------------------------------
  function mass(this,tdpot) result(sum)
    use Globals
    implicit none
    class(p1p0_space_discretization), intent(in) :: this
    class(tdpotsys),                  intent(in) :: tdpot
    real(kind=double):: sum
    !real(kind=double), intent(out) :: mass

    !local 
    real(kind=double) :: ddot

    sum = ddot( this%ntdens, &
         tdpot%tdens,1, &
         this%grid_tdens%size_cell,1)

  end function mass

  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> $\int 1/2 \Tdens |\Grad \Pot|^2+1/2\int \Tdens^{P(\Pflux)}/^{P(\Pflux)}$
  !> (procedure public for type fun)
  !> 
  !> usage: var%s_lyap()
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(odedata)  Ode's inputs
  !> \result [out] lyap       -> real. Lyapunov functional
  !<------------------------------------------------------
  function lyap(this,tdpot,ode_inputs) result(res)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    type(odedata),                    intent(in   ) :: ode_inputs
    ! out variable
    real(kind=double) :: res

    res = this%energy(tdpot) + this%w_mass(tdpot,ode_inputs)

  end function lyap


  !>---------------------------------------------------------
  !> It evaluates the lyap. fun. 
  !> $1/2 \int \Tdens^{P(\Pflux)}/^{P(\Pflux)}$
  !> (procedure public for type fun)
  !> 
  !> usage: var%w_mass(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(odedata)  Ode's inputs
  !> \result [out] lyap       -> real. Lyapunov functional
  !<------------------------------------------------------
  function w_mass(this,tdpot,ode_inputs)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    type(odedata),                    intent(in   ) :: ode_inputs
    ! results
    real(kind=double) :: w_mass
    !local 
    integer           :: itria
    real(kind=double) :: power


    if (ode_inputs%id_ode .eq. 1 ) then
       power = this%exponent_wmass(ode_inputs)

       if ( abs(power) .gt. 1.0d-10 ) then
          w_mass = zero
          do itria=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(itria,1)**2) *&
                  tdpot%tdens(itria) ** power * &
                  this%grid_tdens%size_cell(itria)
          end do
          w_mass = onehalf*w_mass/(power) 
       else
          w_mass = zero
          do itria=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(itria,1)**2) * &
                  log(tdpot%tdens(itria))/log(exp(1.0d0))* &
                  this%grid_tdens%size_cell(itria)
          end do
          w_mass = onehalf * w_mass 
       end if
    end if

    if (ode_inputs%id_ode .eq. 2 ) then
       power = this%exponent_wmass(ode_inputs)

       if ( abs(power) .gt. 1.0d-9 ) then
          w_mass = zero
          do itria=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(itria,2)**2) * &
                  tdpot%tdens(itria) ** power            * &
                  this%grid_tdens%size_cell(itria)
          end do
          w_mass =  onehalf*w_mass/(power) 
       else
          w_mass = zero
          do itria=1,this%ntdens
             w_mass = w_mass + &
                  (ode_inputs%kappa(itria,2)**2) * &
                  log(tdpot%tdens(itria))/log(exp(1.0d0))* &
                  this%grid_tdens%size_cell(itria)
          end do
          w_mass = onehalf * w_mass 
       end if
    end if


  end function w_mass

  !>---------------------------------------------------------
  !> It evaluates $\int_{\Omega}|\Vel|^{power}$ for
  !> a giben power
  !> (procedure public for type fun)
  !> 
  !> usage: var%w_mass(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot      -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs -> type(odedata)  Ode's inputs
  !> \result [out] integral   -> real. Weighted flux integrated
  !<------------------------------------------------------
  function integral_flux(this,tdpot,power) result(integral)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    real(kind=double),                intent(in   ) :: power
    ! result
    real(kind=double) :: integral
    !local 
    integer :: itria

    integral=zero
    do itria=1,this%ntdens
       integral = integral + &
            ( tdpot%tdens(itria) * tdpot%nrm_grad_avg(itria) )** power * & 
            this%grid_tdens%size_cell(itria)
    end do
    integral = integral/power

  end function integral_flux


  !>---------------------------------------------------------
  !> It evaluates $\int_{\Omega}\Pot \Forcing $ for
  !> a giben power
  !> (procedure public for type fun)
  !> 
  !> usage: var%integral_potforcing(tdpot,ode_inputs)
  !>    
  !> where:
  !> \param  [in ] tdpot               -> type(tdpotsys) Tdens-Potential System
  !> \param  [in ] ode_inputs          -> type(odedata)  Ode's inputs
  !> \result [out] integral_potforcing -> real. Weighted flux integrated
  !<------------------------------------------------------
  function integral_potforcing(this,tdpot,ode_inputs) result(integral)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    type(odedata),                    intent(in   ) :: ode_inputs
    ! result
    real(kind=double) :: integral
    !local 
    real(kind=double) :: ddot

    integral=ddot(tdpot%npot,&
         tdpot%pot,1,&
         ode_inputs%rhs_integrated(:,1),1)

  end function integral_potforcing


  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function pvel_exponent(this,ode_inputs) result(exponent)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(odedata),                    intent(in   ) :: ode_inputs
    real(kind=double) :: exponent
    !local 
    real(kind=double) :: pflux
    real(kind=double) :: pmass

    pflux = ode_inputs%pflux(1)
    pmass = ode_inputs%pmass(1)

    if ( ode_inputs%id_ode .eq. 1) then
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pmass             ) > 1.0d-10 ) ) then
          exponent = (2 * pmass - pflux ) / pmass
       end if
    else
       exponent = zero
    end if

    if ( ode_inputs%id_ode .eq. 2) then
       if ( ( abs(1 - pflux + pmass ) > 1.0d-10 ) .and. &
            ( abs(2 - pflux + pmass ) > 1.0d-10 ) ) then
          exponent = 2 * (1 - pflux + pmass ) / (2 - pflux + pmass )
       else
          exponent = zero
       end if
    end if

  end function pvel_exponent

  !>------------------------------------------------------------
  !> Procedure evaluating $pvel$ exponent wihich is the candidate
  !> branch exponent for give pflux and pmass
  !>-------------------------------------------------------------
  function exponent_wmass(this,ode_inputs) result(exponent)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(odedata),                    intent(in   ) :: ode_inputs
    real(kind=double) :: exponent
    !local 
    real(kind=double) :: pflux
    real(kind=double) :: pmass

    pflux = ode_inputs%pflux(1)
    pmass = ode_inputs%pmass(1)

    exponent = zero

    if ( ode_inputs%id_ode .eq. 1) then
       if ( ( abs(2 * pmass - pflux ) > 1.0d-10 ) .and. &
            ( abs(pflux             ) > 1.0d-10 ) ) then
          exponent = (2 * pmass - pflux ) / pflux
       else
          exponent = zero
       end if
    end if

    if ( ode_inputs%id_ode .eq. 2) then
       if (  abs(one - pflux + pmass ) > 1.0d-10 ) then 
          exponent = 1.0d0 + pmass - pflux 
       else
          exponent = zero
       end if
    end if

  end function exponent_wmass



  !>------------------------------------------------------------
  !> Procedure evaluating
  !> $\int_{\Omega}|q|^{pvel} - \int{\Omega} u f $
  !>-------------------------------------------------------------
  function duality_gap(this,tdpot,ode_inputs) result(gap)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    type(odedata),                    intent(in   ) :: ode_inputs
    ! result
    real(kind=double) :: gap
    !local 
    real(kind=double) :: power

    power = this%pvel_exponent(ode_inputs)
    gap   = abs(this%integral_flux( tdpot, power) - &
         this%integral_potforcing(tdpot,ode_inputs))

  end function duality_gap

  !> ent_tdens= \int (\tdens/mass(tdens) log( \tdens / (mass(\tdens) ) 
  function ent_tdens(this,tdpot) result(sum)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    real(kind=double) :: sum
    !local 
    integer :: itria
    real(kind=double) :: total_mass

    sum        = zero
    total_mass = this%mass(tdpot) 

    do itria = 1, tdpot%ngrad
       sum = sum + &
            ( tdpot%tdens( this%grid_pot%cell_parent(itria) ) / &
            total_mass )* &
            log( tdpot%tdens( this%grid_pot%cell_parent(itria) ) / &
            total_mass) * &
            this%grid_pot%size_cell(itria)
    end do

  end function ent_tdens

  function supflux(this) result(res)
    implicit none
    class(tdpotsys), intent(in) :: this
    real(kind=double) :: res

    res = maxval( this%tdens * this%nrm_grad_avg ) 

  end function supflux


  !>----------------------------------------------------------------
  !> Function to eval.
  !> $ var(\TdensH^k):=\frac{
  !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
  !>                   }{ 
  !>                     \| \tdens^{k+1} \|_{L2}}  
  !>                   } $
  !> (procedure public for type tdpotsys) 
  !> 
  !> usage: call var%err_tdesn()
  !>    
  !> where:
  !> \param  [in ] var       -> type(tdpotsys) 
  !> \result [out] var_tdens -> real. Weighted var. of tdens
  !<----------------------------------------------------------------
  function eval_var_tdens(this,tdpot,deltat,power)
    use Globals
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(in   ) :: tdpot
    real(kind=double),                intent(in   ) :: deltat
    real(kind=double), optional,      intent(in   ) :: power
    real(kind=double) :: eval_var_tdens
    !local
    real(kind=double) :: norm_old, norm_var,exponent

    ! $ var_tdens = 
    !              frac{
    !                   \|\tdens^{n+1}-\tdens^n\|_{L^2}
    !                  }{
    !                   \deltat \|\tdens^{n}\|_{L^2}
    !                  }$
    if (present(power)) then
       exponent = power
    else
       exponent = 2.0d0
    end if
    norm_old = this%grid_tdens%normp_cell(exponent,tdpot%tdens_old)
    norm_var = this%grid_tdens%normp_cell(exponent,tdpot%tdens-tdpot%tdens_old)

    eval_var_tdens =  norm_var / ( deltat * norm_old )
  end function eval_var_tdens

  !>-------------------------------------------------
  !> Precedure for grads variables and grad-depending variables
  !> including nrm_grad_dyn defiend as
  !> nrm_grad_dyn(r) = \int _{T_r} |\nabla u| /{T_r}
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call var%build_nrm_grad_vars(ctrl)
  !>
  !<-------------------------------------------------
  subroutine build_nrm_grad_vars(this,p1p0,stderr)
    use Globals
    use ControlParameters
    implicit none
    class(tdpotsys),   intent(inout) :: this
    class(p1p0_space_discretization), intent(inout) :: p1p0
    integer,           intent(in   ) :: stderr
    !local
    integer :: itria,icell_sub,i,j
    real(kind=double) :: ddot,pflux

    if ( .not. this%tdpot_syncr )  then
       write(stderr,*) 'Tdens and Pot are not syncronized'
       stop
    end if


    ! evaluation of gradx, grady ,gradz
    call p1p0%p1%eval_grad(this%pot,this%gradpot)

    ! evaluation nrm_grad   
    call p1p0%p1%eval_nrm_grad(this%pot,this%nrm_grad)

    ! average on subgrid or copy 
    if (p1p0%id_subgrid .eq. 1 ) then
       call p1p0%grid_pot%avg_cell_subgrid(&
            p1p0%grid_tdens,&
            this%nrm_grad,this%nrm_grad_avg)
       do i =1,p1p0%grid_pot%logical_dimension
          this%scr_ngrad(:) = this%gradpot(i,:)
          call p1p0%grid_pot%avg_cell_subgrid(&
               p1p0%grid_tdens,&
               this%scr_ngrad,this%scr_ntdens)
          do j=1,this%ntdens
             this%gradpot_avg(i,j) = this%scr_ntdens(j)
          end do
       end do

    else if (p1p0%id_subgrid .eq. 0 ) then
       this%nrm_grad_avg = this%nrm_grad
       this%gradpot_avg  = this%gradpot
    end if

    this%all_syncr = .true.

  end subroutine build_nrm_grad_vars

  !>-------------------------------------------------
  !> Precedure for grads variables and grad-depending variables
  !> including nrm_grad_dyn defiend as
  !> nrm_grad_dyn(r) = \int _{T_r} |\nabla u| /{T_r}
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call var%build_nrm_grad_vars(ctrl)
  !>
  !<-------------------------------------------------
  subroutine build_nrm_grad_dyn(this,tdpot,pode)
    use Globals
    use ControlParameters
    implicit none
    
    class(p1p0_space_discretization), intent(inout) :: this
    type(tdpotsys),    intent(inout) :: tdpot
    real(kind=double), intent(in   ) :: pode
    !local
    logical rc

    ! compute norm of the gradient at power
    ! pode = pflux for PP dynamic
    ! pode = 2.0   for GF dynamic 
    
    ! eval nrm_grad   
    call this%p1%eval_nrm_grad(tdpot%pot,this%scr_ngrad)
    
    
    
    ! eval nrm_grad**pode
    this%scr_ngrad = this%scr_ngrad**pode

    ! eval nrm_grad_dyn and nrm_grad_power
    if (this%id_subgrid .eq. 1) then  
       ! average nrm_grad^pode
       call this%grid_pot%avg_cell_subgrid(&
            this%grid_tdens,&
            this%scr_ngrad, &
            this%nrm_grad_dyn)

    else
       ! copy values
       this%nrm_grad_dyn = this%scr_ngrad
    end if
    
    this%nrm_grad_dyn=abs(this%nrm_grad_dyn)
    
  end subroutine build_nrm_grad_dyn

  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine tdens2gfvar(ntdens,id_ode,pflux,tdens,gfvar)
    use Globals
    use ControlParameters
    implicit none
    integer, intent(in) :: ntdens
    integer, intent(in) :: id_ode
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: tdens(ntdens)
    real(kind=double), intent(out) :: gfvar(ntdens)
    !local
    logical :: rc
    real(kind=double) :: power
    
    !
    ! gfvar= tdens^{\frac{pflux}/{2-beta}}
    !
    if ( id_ode .eq. 2) then
       if ( pflux < 2.0d0) then
          power = (2.0d0 - pflux) / 2.0d0
          gfvar = tdens**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          gfvar = log(tdens)
       end if
    else
       rc = IOerr(6, err_inp, 'proc. tdens2gfvar', &
            ' trnaformation not defined for id_ode = ',id_ode)
    end if
    
  end subroutine tdens2gfvar

  !>-------------------------------------------------
  !> Precedure to compute gfvar variable for GF dynamics
  !> ( public procedure for type tdpotsys)
  !> 
  !> usage: call 
  !>
  !<-------------------------------------------------
  subroutine gfvar2tdens(ntdens,id_ode,pflux,gfvar,tdens)
    use Globals
    use ControlParameters
    implicit none
    integer, intent(in) :: ntdens
    integer, intent(in) :: id_ode
    real(kind=double), intent(in) :: pflux
    real(kind=double), intent(in) :: gfvar(ntdens)
    real(kind=double), intent(out) :: tdens(ntdens)    
    !local
    logical :: rc
    real(kind=double) :: power
    
    if ( id_ode .eq. 2) then
       if ( pflux < 2.0d0) then
          power = 2.0d0 / (2.0d0 - pflux)
          tdens = gfvar**power
       else if ( abs( pflux - 2.0d0) < 1.0d-12 ) then 
          tdens = log(gfvar)
       end if
    else
       rc = IOerr(6, err_inp, 'proc. tdens2gfvar', &
            ' transformation not defined for id_ode = ',id_ode)
    end if
    
  end subroutine gfvar2tdens
 
  !>------------------------------------------------------
  !> Prodeduce for the assembly of the siffness
  !> matrix for given Tdens
  !>------------------------------------------------------
  subroutine assembly_stiffness_matrix(p1p0,lun_err,tdens)
    use Globals
    use Timing
    implicit none
    class(p1p0_space_discretization), intent(inout) :: p1p0
    integer,           intent(in   ) :: lun_err
    real(kind=double), intent(in   ) :: tdens(p1p0%ntdens)
    
    if ( p1p0%id_subgrid .eq. 0 ) then
       ! assembly
       call p1p0%p1%build_stiff(lun_err, 'csr', tdens, p1p0%stiff)
       
    else
       ! projection and assembly
       call p1p0%grid_pot%proj_subgrid(tdens,p1p0%tdens_prj)
       call p1p0%p1%build_stiff(lun_err,&
            'csr', p1p0%tdens_prj, p1p0%stiff)
    end if


  end subroutine assembly_stiffness_matrix

  
  
  subroutine tdens2pot(this,&
       p1p0,&
       ctrl,&
       lun_err,&
       info,&
       CPU,&
       ode_inputs,&
       tdens,rhs,pot)
    use Globals    
    use Timing
    implicit none
    class(tdpotsys),   intent(inout) :: this
    type(p1p0_space_discretization), intent(inout) :: p1p0
    type(CtrlPrm),     intent(in   ) :: ctrl
    integer,           intent(in   ) :: lun_err
    integer,           intent(inout) :: info
    type(codeTim),     intent(inout) :: CPU
    type(odedata),     intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens(this%ntdens)
    real(kind=double), intent(inout) :: rhs(this%npot)
    real(kind=double), intent(inout) :: pot(this%npot)

    ! local
    integer :: npot,i,j,imax
    real(kind=double) :: wct_ass,dnrm2
    character(len=256) :: outformat
    type(input_prec) :: ctrl_prec
    type(input_solver) :: ctrl_solver
    logical ::     use_inverse_prec
    type(spmat) :: copy_stiff

    npot=this%npot

    !
    ! build stiffness matrix
    !
    wct_ass=CPU%ASSEMBLY%wct
    call CPU%ASSEMBLY%set('start')
    
    this%scr_ntdens=tdens+ode_inputs%lambda(1)
    call p1p0%assembly_stiffness_matrix(lun_err,tdens)
    
   

    call CPU%ASSEMBLY%set('stop')
    !outformat=ctrl%formatting('ar')
    !write(ctrl%ctrl_solver%lun_out,etb(outformat)) &
    !     'stiffness matrix assembly /wcl time =', CPU%ASSEMBLY%wct-wct_ass

    
    !
    ! handle singularity / Dirichlet 
    !
    if ( ode_inputs%ndir > 0 ) then
       call p1p0%p1%dirichlet_bc(lun_err,&
            p1p0%stiff,rhs, pot,&
            ode_inputs%ndir,&
            ode_inputs%dirichlet_nodes,&
            ode_inputs%dirichlet_values)
    else
       call ortogonalize(p1p0%stiff%ncol,&
            1,&
            p1p0%kernel_full(1:npot,1),pot)
    end if

    copy_stiff = p1p0%stiff  
    if ( this%ntdens_on .ne. this%ntdens ) then
       this%scr_npot=zero
       ! freeze potential imposing dirichlet 
       do i=1,this%npot_off
          
          call copy_stiff%set_rowcol(this%inactive_pot(i),zero)
          copy_stiff%coeff(copy_stiff%idiag(this%inactive_pot(i)))=one
       end do
            
      
       write(*,*) ' near kernel dimension',this%npot_off
       call p1p0%near_kernel%set(this%npot_off, this%inactive_pot)

    end if

    
    !
    ! Solve the Linear system
    !  
    call CPU%LINEAR_SOLVER%set('start')
    this%nlinear_system=this%nlinear_system+1
    use_inverse_prec = .False.
    if ( .not. use_inverse_prec) then 
       imax=1
       do i=1,npot
          j=copy_stiff%idiag(i)
          copy_stiff%coeff(j)=copy_stiff%coeff(j)+ctrl%relax2prec!*p1p0%diagonal_laplacian(j)
          if ( rhs(i) > rhs(imax) ) imax = i 
       end do
       call  assembly_stdprec(lun_err,&
            copy_stiff,&
            !p1p0%stiff,&
            ctrl%build_prec,&
            ctrl%ctrl_prec,&
            this%info_prec,&
            p1p0%standard_prec_saved)

       this%scr_nfull(1:npot)=zero
       call linear_solver(p1p0%stiff,&
            rhs,pot,&
            this%info_solver, &
            ctrl%ctrl_solver,&
            prec_left= p1p0%standard_prec_saved,&
            aux=p1p0%aux_bicgstab, ortogonalization_matrix=p1p0%near_kernel)



!!$       call my_linear_solver(&
!!$            p1p0%stiff,rhs,pot,&
!!$            ctrl%ctrl_solver,&
!!$            this%info_solver,&
!!$            ctrl%id_diagscale,&
!!$            ctrl%build_prec,&
!!$            ctrl%ctrl_prec,&
!!$            this%info_prec,&
!!$            p1p0%standard_prec_saved,&
!!$            ctrl%build_tuning,&
!!$            ctrl%ctrl_tuning,&
!!$            this%info_tuning,&
!!$            p1p0%spectral_info,&
!!$            infomod=ctrl%debug,& 
!!$            aux=p1p0%aux_bicgstab)
    else
       !
       ! set approx inverse preconditioner
       !
       ctrl_prec%prec_type   = ctrl%ctrl_prec%prec_type
       ctrl_prec%n_fillin    = ctrl%ctrl_prec%n_fillin
       ctrl_prec%tol_fillin  = ctrl%ctrl_prec%tol_fillin
       call p1p0%standard_prec_saved%init(lun_err, info,&
            ctrl_prec, p1p0%stiff%nrow, p1p0%stiff)

       !
       ! set linear solver controls
       !
       ctrl_solver%scheme  = 'PCG'
       ctrl_solver%tol_sol = 1.0d-2
       ctrl_solver%imax    = 1000
       ctrl_solver%isol    = 0
       ctrl_solver%iprt    = 0
       ctrl_solver%debug   = 0
       ctrl_solver%iort    = 5

       call p1p0%approx_inverse%set(p1p0%stiff,ctrl_solver,&
            p1p0%standard_prec_saved)

       

       !
       ! call linear solver
       !
       call linear_solver(p1p0%stiff,rhs,pot,&
               this%info_solver, &
               ctrl%ctrl_solver,&
               prec_left=p1p0%approx_inverse,&
               aux=p1p0%aux_bicgstab)
       write(ctrl%ctrl_solver%lun_out,*)' inverse as prec' 
    end if
    

    info = this%info_solver%ierr
    
    !
    ! store linear system information
    !
    if (info .eq. 0) then
       if ( ctrl%build_prec .eq. 1) &
            this%iter_last_prec = this%info_solver%iter
       this%total_number_linear_system = &
            this%total_number_linear_system +1
       this%total_iterations_linear_system =  &
            this%total_iterations_linear_system + &
            this%info_solver%iter
       this%res_elliptic = this%info_solver%resreal
    end if
    call CPU%LINEAR_SOLVER%set('stop')

    ! tdens and pot are syncronized
    this%tdpot_syncr=.true.

    call copy_stiff%kill(6)

  end subroutine tdens2pot

  subroutine syncronize_at_tzero( this,&
       info,&
       lun_err,lun_out,lun_stat,&
       p1p0,&
       ode_inputs,&
       ctrl,&
       CPU)
    implicit none
    class(tdpotsys), intent(inout) :: this
    integer,         intent(inout) :: info
    integer,         intent(in   ) :: lun_err,lun_out,lun_stat
    type(p1p0_space_discretization), intent(inout) :: p1p0
    type(OdeData),   intent(in   ) :: ode_inputs
    type(CtrlPrm),   intent(in   ) :: ctrl
    type(codeTim),   intent(inout) :: CPU


    ! local
    logical :: endfile
    integer :: icell
    integer :: itemp=0 
    integer :: int_before_dat,  int_before_matrix

    call CPU%ALGORITHM%set('start')
    !
    ! 2 - Compute $\Pot$ at time $tzero$
    !

    ! 2.1 - Assign data 
    this%tdens   = ode_inputs%tdens0
    if ( abs( ode_inputs%pode(1)-2.0d0 ) < small ) then
       call tdens2gfvar(this%ntdens,2,ode_inputs%pflux(1),&
            this%tdens,this%gfvar)
       
    end if
    this%pot     = zero
    
    
    ! 2.2 - solve $-\Div(\Tdens+lambda \Pot) = \Forcing$    
    this%scr_npot   = ode_inputs%rhs_integrated(:,1) ! copy rhs
    this%scr_ntdens = this%tdens + ode_inputs%lambda(1) ! use tdens + lambda
    call ctrl%ctrl_prec%info(6)
    call this%tdens2pot(p1p0,ctrl,lun_err,info,CPU,&
         ode_inputs,&
         this%scr_ntdens ,this%scr_npot, this%pot)
    call this%info_solver%info(lun_out)
    write(lun_out,*) etb(this%info_solver%time2str())


    call CPU%ALGORITHM%set('stop')

    !
    ! store linear solver info 
    !
    this%sequence_info_solver(1)=this%info_solver
    this%iter_nonlinear = 1
    this%sequence_build_prec(1)=ctrl%build_prec
    this%sequence_build_tuning(1)=ctrl%build_tuning
    this%iter_first_system = this%info_solver%iter


    !
    ! 3 - Compute tdpot extra component
    ! $\Grad \Pot$, $|\Grad \Pot|$, 
    ! $\Grad \Pot_{avg}$, $|\Grad \Pot|_{|dyn}$ 
    ! at time $tzero$
    !
    call CPU%EXTRACOMP%set('start')
    call this%build_nrm_grad_vars(p1p0,lun_err)
    call p1p0%build_nrm_grad_dyn(this,ode_inputs%pode(1))
    call CPU%EXTRACOMP%set('stop')


  end subroutine syncronize_at_tzero


  !>------------------------------------------------
  !> Procedure for computation of next state of system
  !> ( all variables ) given the preovius one
  !> ( private procedure for type tdpotsys, used in update)
  !> 
  !> usage: call var%update(stderr,itemp)
  !> 
  !> where:
  !> \param[in ] stderr -> Integer. I/O err. unit
  !> \param[in ] itemp   -> Integer. Time iteration
  !<---------------------------------------------------
  subroutine update(this,&
       lun_err,lun_out,lun_stat,&
       ctrl,&
       deltat,&
       current_time_iteration,&
       current_time,&
       CPU,&
       info,&
       p1p0,&
       ode_inputs)
    use Globals
    use IOdefs
    use ControlParameters
    use Timing
    use DataSequence

    implicit none
    class(tdpotsys),            intent(inout) :: this
    integer,                    intent(in   ) :: lun_err,lun_out,lun_stat
    type(CtrlPrm),              intent(inout) :: ctrl
    real(kind=double),          intent(inout) :: deltat
    integer,                    intent(in   ) :: current_time_iteration
    real(kind=double),          intent(in   ) :: current_time
    type(codeTim),              intent(inout) :: CPU
    integer,                    intent(inout) :: info
    type(p1p0_space_discretization), target,intent(inout) :: p1p0
    type(odedata),              intent(in   ) :: ode_inputs
    ! local
    real(kind=double) :: decay,pflux,pmass,pode,time,tnext
    integer :: newton_initial
    integer :: ntdens, npot
    integer :: info_inter,passed_reduced_jacobian
    type(tim) :: wasted_temp
    character(len=256) :: str,msg
    integer :: slot
    
    ntdens = this%ntdens
    npot   = this%npot
        
    this%time_iteration=current_time_iteration
    !
    ! copy before update
    !
    call dcopy(this%ntdens,&
         this%tdens,1,&
         this%tdens_old,1)
    call dcopy(this%npot,&
         this%pot,1,&
         this%pot_old,1)

    if ( ctrl%id_time_discr .eq. 3 ) p1p0%D1 = this%gfvar_old
    call dcopy(this%ntdens,&
         this%gfvar,1,&
         this%gfvar_old,1)

    write(lun_out,*) 'Time step = ',deltat

    ! update all spatial variables at itemp+1
    select case (  ctrl%id_time_discr )
    case (1)
       if ( ctrl%info_inputs_update .gt. 0) then
          write(lun_out,*) ' IN : build_prec =', ctrl%build_prec
       end if
       write(str,'(a)') ' EXPLICIT EULER NEWTON begin '
       write(msg,'(a)') ctrl%sep(str) 
       write(lun_out,*) etb(msg)
       write(lun_stat,*) etb(msg)
       call explicit_euler(this,&
            lun_err,info,&
            ctrl,&
            deltat,&
            current_time,&
            CPU,&
            p1p0,&
            ode_inputs)
       if (info .ne. 0) then
          write(str,'(a)') ' EXPLICIT EULER NEWTON succesed '
          write(msg,'(a)') ctrl%sep(str) 
          
       else
          write(str,'(a)') ' EXPLICIT EULER NEWTON failed '
          write(msg,'(a)') ctrl%sep(str) 
       END if
       write(lun_out,*) etb(msg)
       write(lun_stat,*) etb(msg)
          
    case (2) 
       !
       ! Newton legend
       !
       write(str,'(a)') ' IMPLICIT EULER NEWTON begin '
       write(msg,'(a)') ctrl%sep(str) 
       write(lun_out,*) etb(msg)
       write(lun_stat,*) etb(msg)       
       call this%implicit_euler_newton(&
            lun_err,lun_out,lun_stat, &
            ctrl, &
            current_time_iteration,current_time,deltat, &
            info,&
            CPU,&
            p1p0,&
            ode_inputs,&
            this%tdens_old, this%pot_old)
       if ( info .eq. 0) then
          write(str,'(a)') ' IMPLICIT EULER NEWTON end '
          write(lun_out,'(a)') ctrl%sep(str) 
          write(lun_stat,'(a)') ctrl%sep(str) 
          
             
       else
          write(str,'(a)') ' IMPLICIT EULER NEWTON failed '
          write(lun_out,'(a)') ctrl%sep(str)
          write(lun_stat,'(a)') ctrl%sep(str) 
       end if
          
       
    case (3)
       if ( ctrl%debug .eq. 1 ) &
            write( lun_out, *) ' Accelerated Explicit Euler Gfvar'
       call explicit_euler_accelerated_gfvar(this,&
            lun_err,info,&
            ctrl,&
            current_time_iteration,&
            deltat,&
            current_time,&
            CPU,&
            p1p0,&
            ode_inputs) 
    case (4)
      !
       ! Newton legend
       !
       write(str,'(a)') ' IMPLICIT EULER GRADIENT FLOW NEWTON begin '
       write(msg,'(a)') ctrl%sep(str) 
       write(lun_out,*) etb(msg)
       write(lun_stat,*) etb(msg)
       call this%implicit_euler_newton_gfvar(&
            lun_err,lun_out,lun_stat, &
            ctrl, &
            current_time_iteration,current_time,deltat, &
            info,&
            CPU,&
            p1p0,&
            ode_inputs,&
            this%gfvar_old, this%pot_old)
       if ( info .eq. 0) then
          write(str,'(a)') ' IMPLICIT EULER GRADIENT FLOW end '
          write(lun_out,'(a)') ctrl%sep(str) 
          write(lun_stat,'(a)') ctrl%sep(str) 
          
             
       else
          write(str,'(a)') ' IMPLICIT EULER GRADIENT FLOW failed '
          write(lun_out,'(a)') ctrl%sep(str)
          write(lun_stat,'(a)') ctrl%sep(str) 
       end if

      
    end select


  contains
    !>----------------------------------------------------
    !> Procedure for update the system with Explicit Euler
    !> IMPORTANT all the variables tdens pot 
    !> odein have to be syncronized at time time(itemp)
    !>
    !> usage: call var%explicit_euler(stderr,itemp)
    !> 
    !> where:
    !> \param[in ] stderr -> Integer. I/O err. unit
    !> \param[in ] itemp   -> Integer. Time iteration
    !<---------------------------------------------------    
    subroutine explicit_euler(this,&
         lun_err, info,&
         ctrl,&
         deltat,&
         current_time,&
         CPU,&
         p1p0,&
         ode_inputs) 
      use Globals 
      use IOdefs
      use ControlParameters
      use Timing

      implicit none
      class(tdpotsys),   intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      integer,           intent(inout) :: info
      type(CtrlPrm),     intent(in   ) :: ctrl
      real(kind=double), intent(in   ) :: deltat
      real(kind=double), intent(in   ) :: current_time
      type(codeTim),     intent(inout) :: CPU
      type(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),     intent(in   ) :: ode_inputs
          

      !local
      integer :: itria, id_ode
      integer :: ntdens,npot
      real(kind=double) :: pode,pflux
      real(kind=double) :: delta,dnrm2
      integer :: icell, isubcell, ifather, inode, i,j,iloc
      real(kind=double) :: grad_pot(3), grad_base(3),grad_der_pot(3),der_pot(3)
      real(kind=double) :: ddot
      integer :: jcell, jsubcell, jfather
      real(kind=double), allocatable :: grad_w(:,:)
      character(len=256) :: fname, number

      
      ntdens = this%ntdens
      npot   = this%npot

      if ( .not. this%all_syncr)  then
         write(lun_err,*) 'Not all varibles are syncronized'
         stop
      end if

      if ( ctrl%threshold_tdens>1e-20 ) then
         call p1p0%set_active_regions(this,ctrl)
         write(*,*) ' tdens on =', this%ntdens_on, 'of', this%ntdens
         write(*,*) ' pot  yes =', this%npot_on,' pot not =', this%npot_off, 'of', this%npot
         call p1p0%near_kernel%set(this%npot_off, this%inactive_pot)
      end if


      !
      ! Compute rhs ode 
      !
      
      !
      ! PP ODE: ctrl%id_ode .eq. 1 
      ! $ (\Tdens|\Grad Pot|)^Pflux-\Kappa*Annealing*\Tdens^\Pmass $
      !             or      
      ! Gradient Flow : ctrl%id_ode .eq. 2
      ! $ (\Tdens)^Pflux |\Grad \Pot|^2 - \kappa*Annealing*\Tdens^\Pmass $
      !
      !call this%build_nrm_grad_dyn(pode)
      call p1p0%assembly_rhs_ode(&
         1,&
         ode_inputs,&
         this,&
         p1p0%rhs_ode)

      ! scale by the mass matrix con the Finite Elements used for tdens
      call p1p0%get_increment_ode(p1p0%rhs_ode,p1p0%inc_ode)

      !
      ! Update $\Tdens$ and $Gfvar$ 
      !
      id_ode = ode_inputs%id_ode
      pflux  = ode_inputs%pflux(2)
      select case ( id_ode ) 
      case (1)
         !
         ! PP dynamic - tdens update 
         !
         if ( this%ntdens_on .ne. ntdens ) then
            call daxpy(ntdens, deltat, p1p0%inc_ode,1,this%tdens,1)
            do icell = 1, this%ntdens
               this%tdens(icell) = max(this%tdens(icell),ctrl%min_tdens)
            end do
         else
            do i = 1, this%ntdens_on
               icell  = this%active_tdens(i)
               this%tdens(icell) = &
                    this%tdens(icell) + deltat * p1p0%inc_ode(icell)
               this%tdens(icell) = max(this%tdens(icell),ctrl%min_tdens)
            end do
         end if
      case (2)
         !
         ! GF dynamic - tdens update 
         !
         call daxpy(ntdens, deltat, p1p0%inc_ode,1,this%tdens,1)
         do itria = 1, this%ntdens
            this%tdens(itria) = max(this%tdens(itria),ctrl%min_tdens)
         end do
         call tdens2gfvar(ntdens,id_ode,pflux,&
              this%tdens,this%gfvar)
      case (3)
         !
         ! GF dynamic - tdens update 
         !
         call daxpy(ntdens, deltat, p1p0%inc_ode,1,this%gfvar,1)
         call gfvar2tdens(ntdens,id_ode,ode_inputs%pflux(2),&
              this%gfvar, this%tdens)
         do itria = 1, this%ntdens
            this%tdens(itria) = max(this%tdens(itria),ctrl%min_tdens)
         end do
         call tdens2gfvar(ntdens,id_ode,ode_inputs%pflux(2),&
              this%tdens,this%gfvar)
      end select         
      this%all_syncr = .false.

      ! Store var_tdens
      this%loc_var_tdens = p1p0%eval_var_tdens(this,deltat)

 
      !
      ! copy rhs into scr for  
      !
      p1p0%scr_npot = ode_inputs%rhs_integrated(:,2)

      !
      ! update $\Pot$ input varible at time $t^{\tstep+1}$
      !
      this%scr_ntdens = this%tdens + ode_inputs%lambda(1)
      call this%tdens2pot(p1p0,ctrl,lun_err,info,CPU,ode_inputs,this%scr_ntdens,p1p0%scr_npot,this%pot)
      call this%info_solver%info(lun_out)
      info = this%info_solver%ierr 

      



      !
      ! store information for liear solver
      !
      this%sequence_info_solver(1)=this%info_solver
      this%iter_nonlinear = 1
      this%sequence_build_prec(1)=ctrl%build_prec
      this%sequence_build_tuning(1)=ctrl%build_tuning

      !-------------------------------------------------------------
      ! Update $\Grad \Pot$, $|\Grad \Pot|$, 
      ! $\Grad \Pot_{avg}$, $|\Grad \Pot|_{|dyn}$ 
      ! at time $t^{\tstep+1}$
      call CPU%EXTRACOMP%set('start')
      call this%build_nrm_grad_vars(p1p0,lun_err)
      pode=ode_inputs%pode(1)
      call p1p0%build_nrm_grad_dyn(this,pode)
      call CPU%EXTRACOMP%set('stop')
      !-------------------------------------------------------------

      !if ( this%time_iteration .eq. 20 ) call eval_der_pot(this,p1p0)

    end subroutine explicit_euler
      
    subroutine eval_der_pot(this,p1p0)
      use Geometry2d  
      implicit none
      
      class(tdpotsys),   intent(inout) :: this
        type(p1p0_space_discretization), intent(inout) :: p1p0
        ! local
        integer :: lun_err
        integer :: icell, isubcell, ifather, inode, i,j,iloc
        real(kind=double) :: grad_pot(3), grad_base(3),grad_der_pot(3),der_pot(3)
        real(kind=double) :: ddot
        integer :: jcell, jsubcell, jfather
        real(kind=double), allocatable :: grad_w(:,:),work(:)
        character(len=256) :: fname, number
        integer :: nnz,m
        type(spmat) :: adj_temp
        integer, allocatable :: perm(:),iperm(:)
        class(abs_simplex_mesh), pointer :: grid 

        ntdens = this%ntdens
        npot   = this%npot


        call p1p0%assembly_stiffness_matrix(lun_err, this%tdens)
        call  assembly_stdprec(&
             lun_err,&
             p1p0%stiff,&
             1,&
             ctrl%ctrl_prec,&
             this%info_prec,&
             p1p0%standard_prec_saved)

        

  

        p1p0%der_pot_tdens_jacobian%coeff = zero
        do icell = 1, ntdens
           p1p0%scr_npot = zero
           !
           do j=1,4
              isubcell = (icell - 1 ) * 4 + j
              ! gradient of isubcell
              grad_pot=this%gradpot(:,isubcell)
              !write(*,*) icell, isubcell, p1p0%grid_pot%cell_parent(isubcell)
              do iloc=1,p1p0%grid_pot%nnodeincell
                 ! gradient of base fu
                 inode = p1p0%grid_pot%topol(iloc,isubcell) 
                 call p1p0%p1%get_gradbase(iloc, isubcell,grad_base) 
                 ! compute \Grad pot \Grad base
                 p1p0%scr_npot(inode) = p1p0%scr_npot(inode) + &
                      ddot(3,grad_pot, 1, grad_base, 1) * &
                      p1p0%grid_pot%size_cell(isubcell)
              end do
           end  do
           
           if ( this%tdens(i)>ctrl%min_tdens) then
              call linear_solver(p1p0%stiff,&
                   p1p0%scr_npot,p1p0%der_pot_tdens_jacobian%coeff(:,icell),&
                   this%info_solver, &
                   ctrl%ctrl_solver,&
                   prec_left=p1p0%standard_prec_saved,&
                   aux=p1p0%aux_bicgstab)
              call this%info_solver%info(6)
              write(*,*)  this%info_solver%bnorm
           else
              p1p0%der_pot_tdens_jacobian%coeff(:,icell)=zero
           end if
           

           write(number,'(I0.3)') icell
           write(*,*) etb(number)
           write(fname,'(a,a,a)') 'w_',etb(number),'.dat'
           open( 1236,file=fname)
           write(1236,*) 1, npot
           write(1236,*) 'time 0.0'
           write(1236,*) npot
           do i=1,npot
              write(1236,*) i, p1p0%der_pot_tdens_jacobian%coeff(i,icell)
           end do
           write(1236,*) 'time 1.0e30'
           close(1236)

           write(number,'(I0.3)') icell
           write(fname,'(a,a,a)') 'rhs_',etb(number),'.dat'
           open( 1236,file=fname)
           write(1236,*) 1, npot
           write(1236,*) 'time 0.0'
           write(1236,*) npot
           do i=1,npot
              write(1236,*) i, p1p0%scr_npot(i)
           end do
           write(1236,*) 'time 1.0e30'
           close(1236)

        end do


        !call p1p0%der_pot_tdens_jacobian%write(1234)
        
        p1p0%tdens_jacobian%coeff = zero
        allocate (grad_w(3,p1p0%grid_pot%ncell),work(p1p0%grid_pot%ncell))

        do icell = 1,ntdens  ! fix E
           
           call p1p0%p1%eval_grad( p1p0%der_pot_tdens_jacobian%coeff(:,icell),grad_w) 
           work=zero
           ! calcolo \grad u \grad W(E) su tutte le sotto celle
           do isubcell = 1, p1p0%grid_pot%ncell 
              work(isubcell) =work(isubcell) + &
                   ddot(p1p0%grid_pot%logical_dimension,this%gradpot(:,isubcell), 1, grad_w(:,isubcell), 1) 
           end do

           
           p1p0%tdens_jacobian%coeff(:,icell) = zero
           
           do isubcell = 1, p1p0%grid_pot%ncell 
              ifather = p1p0%grid_pot%cell_parent(isubcell)
              p1p0%tdens_jacobian%coeff(ifather,icell) = &
                   p1p0%tdens_jacobian%coeff(ifather,icell) + &
                   work(isubcell) * &
                   p1p0%grid_pot%size_cell(isubcell)
           end do
           write(*,*) 'icell=',icell, maxval(p1p0%tdens_jacobian%coeff(:,icell))
              
              

        end  do



!!$        do isubcell = 1, p1p0%grid_pot%ncell 
!!$           !
!!$           ifather = p1p0%grid_pot%cell_parent(isubcell) ! E
!!$           call p1p0%p1%eval_grad( p1p0%der_pot_tdens_jacobian%coeff(:,ifather),grad_w) 
!!$
!!$           
!!$           do jsubcell = 1, p1p0%grid_pot%ncell ! L
!!$              jfather  = p1p0%grid_pot%cell_parent(jsubcell) 
!!$
!!$              p1p0%tdens_jacobian%coeff(jfather,ifather) = &
!!$                   p1p0%tdens_jacobian%coeff(jfather,ifather) + &
!!$                   !this%tdens(jfather) *  &
!!$                   ddot(3,this%gradpot(:,jsubcell), 1, grad_w(:,isubcell), 1) * &         
!!$                   p1p0%grid_pot%size_cell(jsubcell)
!!$           end do
!!$           p1p0%tdens_jacobian%coeff(:,ifather) = zero
!!$
!!$        end  do

!!$        do icell=1,ntdens
!!$           p1p0%tdens_jacobian%coeff(:,icell) = &
!!$                p1p0%tdens_jacobian%coeff(:,icell)
!!$        end do
!!$        call p1p0%tdens_jacobian%write(5678)
        open(1239,file='tdens_jacobian.dat')
        do jcell = 1,ntdens
           do icell = 1, ntdens
              write(1239,*) p1p0%tdens_jacobian%coeff(icell,jcell)
           end do
        end do
        close(1239)
        open(1239,file='size_cell.dat')
        do icell = 1, ntdens
           write(1239,*) p1p0%grid_tdens%size_cell(icell)
        end do
        close(1239)
        deallocate(grad_w)
        
      end subroutine eval_der_pot

    

    
   
   
    !>--------------------------------------------------------------
    !> Procedure to control the delta_t of the time-stepping
    !> (private procedure used in upadte)
    !<---------------------------------------------------------------
    subroutine  control_deltat_ee(lun_out,ntdens,tdens,rhs_ode,deltat,&
         ctrl,grid_tdens)
      use Globals
      use ControlParameters
      implicit none

      integer,           intent(in   ) :: lun_out
      integer,           intent(in   ) :: ntdens
      real(kind=double), intent(in   ) :: tdens(ntdens)
      real(kind=double), intent(in   ) :: rhs_ode(ntdens)
      real(kind=double), intent(inout) :: deltat
      type(CtrlPrm),     intent(in   ) :: ctrl
      type(abs_simplex_mesh),        intent(in   ) :: grid_tdens

      !local
      real(kind=double) :: sup_rhs
      real(kind=double) :: n2,n2tdens
      character(len=256):: out_format



      if (ctrl%id_time_ctrl == 1) then
         deltat=ctrl%deltat 
         out_format=ctrl%formatting('ar')
      end if
      if (ctrl%id_time_ctrl == 2) then
         if( deltat * ctrl%exp_rate < ctrl%upper_bound_deltat ) then
            deltat =deltat * ctrl%exp_rate
         else
            deltat=ctrl%upper_bound_deltat
         end if
         out_format=ctrl%formatting('ar')
      end if
      if (ctrl%id_time_ctrl == 3) then
         sup_rhs= maxval(rhs_ode)
         if ( sup_rhs .lt. zero ) then
            deltat = ctrl%upper_bound_deltat
         else
            deltat=1.0d0/sup_rhs
            deltat=min(deltat,ctrl%upper_bound_deltat)
            deltat=max(deltat,ctrl%lower_bound_deltat)
         end if
         out_format=ctrl%formatting('araar')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' linfty norm rhs_ode = ', sup_rhs 
      end if

      if (ctrl%id_time_ctrl == 4) then
         n2 = grid_tdens%normp_cell(2.0d0, rhs_ode)  
         deltat=1.0d0/n2
         deltat=min(deltat,ctrl%upper_bound_deltat)
         deltat=max(deltat,ctrl%lower_bound_deltat)

         out_format=ctrl%formatting('araar')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' l2 norm rhs_ode     = ', n2 

      end if


      if (ctrl%id_time_ctrl == 5) then
         n2tdens = grid_tdens%normp_cell(0.0d0, tdens)  
         n2      = grid_tdens%normp_cell(0.0d0, rhs_ode)  
         deltat=1.0d0/(n2/n2tdens)
         deltat=min(deltat,ctrl%upper_bound_deltat)
         deltat=max(deltat,ctrl%lower_bound_deltat)
         write(out_format,*) etb('(a,'//etb(ctrl%rformat_info)&
              //',a,a,'//etb(ctrl%rformat_info)//')')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' l2 norm rhs_ode / n2tdens = ', n2 
      end if

      write(lun_out,out_format) 'time step = ',deltat

    end subroutine control_deltat_ee

    !>----------------------------------------------------
    !> Procedure for update the system with Explicit Euler
    !> IMPORTANT all the variables tdens pot 
    !> odein have to be syncronized at time time(itemp)
    !>
    !> usage: call var%explicit_euler(stderr,itemp)
    !> 
    !> where:
    !> \param[in ] stderr -> Integer. I/O err. unit
    !> \param[in ] itemp   -> Integer. Time iteration
    !<---------------------------------------------------    
    subroutine explicit_euler_accelerated_gfvar(this,&
         lun_err, info,&
         ctrl,&
         time_iteration,&
         deltat,&
         current_time,&
         CPU,&
         p1p0,&
         ode_inputs) 
      use Globals 
      use IOdefs
      use ControlParameters
      use Timing

      implicit none
      class(tdpotsys),   intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      integer,           intent(inout) :: info
      type(CtrlPrm),     intent(in   ) :: ctrl
      integer,           intent(in   ) :: time_iteration
      real(kind=double), intent(in   ) :: deltat
      real(kind=double), intent(in   ) :: current_time
      type(codeTim),     intent(inout) :: CPU
      type(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),     intent(in   ) :: ode_inputs
          

      !local
      integer :: itria, id_ode
      integer :: ntdens,npot
      real(kind=double) :: pode,pflux
      real(kind=double) :: delta,dnrm2
      
      ntdens = this%ntdens
      npot   = this%npot

      if ( .not. this%all_syncr)  then
         write(lun_err,*) 'Not all varibles are syncronized'
         stop
      end if

     
      ! Gradient Flow : ctrl%id_ode .eq. 2
      ! $ \Gfvar|\Grad \Pot|^2 - \Gfvar $
      !
      if ( time_iteration .eq. 1 ) then
         !
         ! at first iteration use standard explicit Euler
         !
         
         !
         ! compute increment
         !
         call p1p0%build_nrm_grad_dyn(this,2.0d0)
         write(*,*) minval(this%gfvar), maxval(this%gfvar)
         this%gfvar = this%gfvar  +  deltat * ( this%gfvar * p1p0%nrm_grad_dyn - this%gfvar)
         call gfvar2tdens(ntdens,2,one,&
              this%gfvar, this%tdens)
         !
         ! ensure positivity
         !
         do itria = 1, this%ntdens
            this%tdens(itria) = max(this%tdens(itria),ctrl%min_tdens)
         end do
         call tdens2gfvar(ntdens,2,one,&
              this%tdens,this%gfvar)
         !
         ! compute potential
         !
         p1p0%scr_npot = ode_inputs%rhs_integrated(:,1)
         this%scr_ntdens = this%tdens + ode_inputs%lambda(1)
         call this%tdens2pot(p1p0,&
              ctrl,lun_err,info,CPU,ode_inputs,&
              this%scr_ntdens,&
              p1p0%scr_npot,&
              this%pot)
         call this%info_solver%info(lun_out)
         info = this%info_solver%ierr
         write(*,*)' gfvar', time_iteration, minval(this%gfvar), maxval(this%gfvar)

      else
         !
         ! yk=x{k}-(k-1)/(k+2)*(x_{k}-x_{k-1})
         !
         write(*,*)'deltat',deltat
         write(*,*) ' gfvar', time_iteration, minval(this%gfvar), maxval(this%gfvar)
         this%scr_ntdens = this%gfvar + &
              ( time_iteration - 1 ) *  &
              one/ ( time_iteration + 10 + 2 ) * &
              ( this%gfvar - p1p0%D1 )
         write(*,*) ' xk-1', time_iteration, minval(p1p0%D1), maxval(p1p0%D1)
         write(*,*) ' yk  ', time_iteration, minval(this%scr_ntdens), maxval(this%scr_ntdens)
         !
         ! update tdens to compute potential drift
         !
         call gfvar2tdens(ntdens,2,one,&
              this%scr_ntdens,&
              p1p0%scr_ntdens)
         write(*,*) ' scr_ntdens', time_iteration, minval(p1p0%scr_ntdens), maxval(p1p0%scr_ntdens)

         p1p0%scr_npot = ode_inputs%rhs_integrated(:,1)
         !
         ! compute \Grad \lyap ( yk )
         !
         ! compute potential
         this%scr_ntdens = this%scr_ntdens + ode_inputs%lambda(1)
         call this%tdens2pot(p1p0,&
              ctrl,lun_err,info,CPU,ode_inputs,&
              p1p0%scr_ntdens,&
              p1p0%scr_npot,&
              this%pot)
         call this%info_solver%info(lun_out)
         info = this%info_solver%ierr

         call p1p0%build_nrm_grad_dyn(this,2.0d0)
         p1p0%rhs_ode = ( this%scr_ntdens * p1p0%nrm_grad_dyn - this%scr_ntdens) 
         write(*,*) ' nrm', time_iteration, minval(p1p0%nrm_grad_dyn), maxval(p1p0%nrm_grad_dyn)
                  

         !
         ! x_k+1=yk - deltat \Grad \Lyap (yk) 
         !
         this%gfvar = p1p0%scr_ntdens + deltat * p1p0%rhs_ode
         
         call gfvar2tdens(ntdens,2,ode_inputs%pflux(2),&
              this%gfvar, this%tdens)
         write(*,*) ' tdens', time_iteration, minval(this%tdens), maxval(this%tdens)

      end if

       !
      ! store information for liear solver
      !
      this%sequence_info_solver(1)=this%info_solver
      this%iter_nonlinear = 1
      this%sequence_build_prec(1)=ctrl%build_prec
      this%sequence_build_tuning(1)=ctrl%build_tuning

      !-------------------------------------------------------------
      ! Update $\Grad \Pot$, $|\Grad \Pot|$, 
      ! $\Grad \Pot_{avg}$, $|\Grad \Pot|_{|dyn}$ 
      ! at time $t^{\tstep+1}$
      call CPU%EXTRACOMP%set('start')
      call this%build_nrm_grad_vars(p1p0,lun_err)
      pode=ode_inputs%pode(1)
      call p1p0%build_nrm_grad_dyn(this,pode)
      call CPU%EXTRACOMP%set('stop')
      !-------------------------------------------------------------


    end subroutine explicit_euler_accelerated_gfvar



  end subroutine update


  !>---------------------------------------------------------
  !> Producedure to update tdens-pot and derived varibles to
  !> via backward Euler time-stepping i.e.
  !> (\Tdens^{k+1},\Pot^{k+1}) = solution of
  !>
  !> \Stiff[\Tdens] \Pot = \RHS_FORCING
  !> \Tdens - \Tdens^{k} = -\Deltat *( RHS_ODE(\Tdens,\Pot) 
  !>
  !> The non-linear equation is solved via Newton-Rapshon method.
  !> Iteration will start from ( tdens_start, pot_start) 
  !>---------------------------------------------------------
  subroutine implicit_euler_newton(this, &
       lun_err,lun_out,lun_stat, &
       ctrl, &
       current_time_iteration,current_time,deltat, &
       info,&
       CPU,&
       p1p0,&
       ode_inputs,&
       tdens_start,&
       pot_start)
    use Globals
    use IOdefs
    use TimeInputs, only : write_steady
    use CombinedSparseMatrix
    use Timing
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use InexactConstraintPreconditioner
    implicit none
    class(tdpotsys), target,  intent(inout) :: this
    integer,           intent(in   ) :: lun_err,lun_out,lun_stat
    type(CtrlPrm),     intent(inout) :: ctrl
    integer,           intent(in   ) :: current_time_iteration
    real(kind=double), intent(in   ) :: current_time
    real(kind=double), intent(inout) :: deltat
    integer,           intent(inout) :: info
    type(codeTim),     intent(inout) :: CPU
    type(p1p0_space_discretization), target, intent(inout) :: p1p0
    type(odedata),     intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: tdens_start(this%ntdens)
    real(kind=double), intent(in   ) :: pot_start(this%npot)
    

    ! local
    logical :: rc,endfile,test_exit,reduced=.False.
    character(len=256) :: outformat
    integer :: res,method
    integer :: ntdens, npot,nfull
    integer :: i,j,iter_newton,info_prec,info_broyden,ind
    real(kind=double) :: res_tdens, res_pot
        
    integer :: ibegin, iend
    real(kind=double), pointer :: D1(:)
    real(kind=double), pointer :: D2(:)
    real(kind=double), pointer :: invD1(:)
    real(kind=double), pointer :: invD2(:)
    real(kind=double), pointer :: invD2_D1(:),invD1_D2(:)
    real(kind=double), pointer :: fnewton_pot(:)
    real(kind=double), pointer :: fnewton_tdens(:)
    real(kind=double), pointer :: fnewton_pot_old(:)
    real(kind=double), pointer :: fnewton_tdens_old(:)
    ! increments
    real(kind=double), pointer :: inc_pot(:)
    real(kind=double), pointer :: inc_tdens(:)
    real(kind=double), pointer :: inc_full(:)
    ! local work arrays
    real(kind=double), pointer :: work(:)
    real(kind=double), pointer :: work2(:)
    ! intial choice 
    real(kind=double), pointer :: tdens_initial(:)
    real(kind=double), pointer :: pot_initial(:)
        ! rhs linear system
    real(kind=double), pointer :: rhs_reduced(:),rhs_full(:)
    real(kind=double), pointer :: kernel_full(:,:) 
    real(kind=double) :: rmax


    real(kind=double) :: rhs_norm,prev_rhs_norm,prev_res ,shift
    real(kind=double) ::  inc_norm, prev_inc_norm
    ! jacobian variables reduced 
    type(combspmat) :: jacobian_reduced
    type(eye),   target :: identity_npot
    class(abs_linop), pointer :: prec_E
    type(spmat), pointer :: matrix2prec

    !type(stdprec), target :: spprec_Schur
    class(abs_linop), pointer :: prec_Schur
        
    ! prec vars
    class(abs_linop), pointer :: prec_final 
    
    ! prec for full jacobian
    type(stdprec),  target :: prec_stiff
    type(eye),   target :: identity_ntdens
    
    type(file) :: fmat
    

    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(eye),  target  :: identity
    real(kind=double), pointer :: diagonal_full(:) 

    logical old, matsave
    character(len=256) :: fname,directory,tail
    

    ! functions 
    real(kind=double) :: dnrm2,ddot,pode,old_tol
    character(len=70) :: str
    character(len=256) :: msg,msg1,msg2

    integer :: slot

    call CPU%OVH%set('start')
    
   
    !
    ! array dimension
    !
    npot   = this%npot
    ntdens = this%ntdens
    nfull  = npot + ntdens

    do i=1,ctrl%nbroyden
       call p1p0%broyden_updates(i)%init(lun_err, ntdens)
       call p1p0%rankone_update(i)%init(lun_err, ntdens)
    end do
    !
    ! assing memory
    !
    iend=0
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    D1 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    D2 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    invD2 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    invD1 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    invD2_D1 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    invD1 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    invD1_D2 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(npot,ibegin,iend)
    fnewton_pot => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    fnewton_tdens=> p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    fnewton_tdens_old => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(npot,ibegin,iend)
    fnewton_pot_old => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(npot,ibegin,iend)
    inc_pot => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    inc_tdens => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    work => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    work2 => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(ntdens,ibegin,iend)
    tdens_initial => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(npot,ibegin,iend)
    pot_initial => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(npot,ibegin,iend)
    rhs_reduced => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(nfull,ibegin,iend)
    rhs_full => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(nfull,ibegin,iend)
    kernel_full(1:nfull,1:1) => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(nfull,ibegin,iend)
    diagonal_full => p1p0%aux_newton%raux(ibegin:iend)
    call p1p0%aux_newton%range(nfull,ibegin,iend)
    inc_full => p1p0%aux_newton%raux(ibegin:iend)

    ! set zero increments
    !
    inc_pot=zero
    inc_tdens=zero
    inc_full=zero
    
    do i=1,ctrl%max_iter_nonlinear
       call this%sequence_info_solver(i)%kill()
       this%sequence_build_prec(i)   = 0
       this%sequence_build_tuning(i) = 0   
    end do


    !
    ! init flags
    !
    
    
    info = 0
    iter_newton = 0
    test_exit=.false.  
    
    

    !write(msg,'(a)')&
    !    'it| INPUT  : NEWTON SYSTEM INFO'
    !write(lun_out,*) etb(msg)
    !write(lun_stat,*) etb(msg)

    !write(msg,'(a)')&
    !     'it| INPUT  : CONTROLS '
    !write(lun_out,*) etb(msg)
    !write(lun_stat,*) etb(msg)

    write(msg,'(a)')&
         'it| OUT   :  nrm_inc nrm_rhs | inc_td  inc_pot | '//&
         'info it resini  resnorm '//&
         'resreal   method      ' 
    write(lun_out,*) etb(msg)
    write(lun_stat,*) etb(msg)

    !write(str,'(a)') ''
    !write(msg,'(a)') ctrl%sep(str) 
    !write(lun_out,*) etb(msg)
    !write(lun_stat,*) etb(msg)

    call CPU%OVH%set('stop')
    


    do while ( .not. test_exit )
       if ( ctrl%info_newton .ge. 0 ) then
          msg=('------------------------'//&
               '----------------------------------------------')
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if

       call CPU%ASSEMBLY%set('start')
       this%nlinear_system=this%nlinear_system+1
       !------------------------------------------------------
       ! Assembly matrices
       !   stiff, B, C, D1, D2^{-1}, D2^{-1}D1
       ! that compose the jacobian matrix 
       !      J_F=( Stiff(tdens) B^T )
       !          ( -dt D1 C     D2  ) 
       !-------------------------------------------------------
       call p1p0%assembly_newton(&
            this,&
            lun_err,lun_out,ctrl%debug,&
            iter_newton,&
            current_time_iteration,current_time,deltat,&
            ode_inputs)       
       !
       ! assembly rhs
       !
       rhs_full(1:npot)             = -p1p0%fnewton_pot(:)
       rhs_full(npot+1:npot+ntdens) = -p1p0%fnewton_tdens(:)

       if ( ctrl%threshold_tdens>1e-20 ) then
          call p1p0%set_active_regions(this,ctrl)
          write(*,*) ' tdens on =', this%ntdens_on,  ' pot  on =', this%npot_on
       end if


       !
       ! evalute norm of rhs and residua for inexact newton
       !
       ! res=rhs - J * inc  = (rhs for iter_newton=0)
       !     
       if ( iter_newton .ge. 1) then
          prev_inc_norm  = inc_norm
          prev_rhs_norm  = rhs_norm
       end if

       rhs_norm   = dnrm2(npot+ntdens,rhs_full,1)
       call p1p0%jacobian_full%Mxv(inc_full, this%scr_nfull)
       this%scr_nfull =  this%scr_nfull + rhs_full
       prev_res = dnrm2(nfull, this%scr_nfull ,1 )
       call CPU%ASSEMBLY%set('stop')

       !
       ! save all matrices and rhs in NEwton system
       !
       matsave=(ctrl%id_save_matrix > 0) 
       if ( matsave) then
          call CPU%SAVE%set('start')
          write(tail,'(I0.4,a,I0.4,a)') &
               this%time_iteration,'_',iter_newton, '.dat' 
          directory=etb('./output/linsys')
          if (ctrl%id_save_matrix .ge. 2) &
               call p1p0%write_newton(lun_err,10000,directory,tail,deltat)
          
          fname=etb(etb(directory)//'/deltat_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          write(fmat%lun,*) deltat 
          call fmat%kill(lun_err)

          
          fname=etb(etb(directory)//'/tdens_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call write_steady(lun_err, 10000, ntdens, this%tdens)
          call fmat%kill(lun_err)
          
          fname=etb(etb(directory)//'/pot_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call write_steady(lun_err, 10000, npot, this%pot)
          call fmat%kill(lun_err)

          fname=etb(etb(directory)//'/stiff_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call p1p0%stiff%write(10000)
          call fmat%kill(lun_err)


          fname=etb(etb(directory)//'/B_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call p1p0%B_matrix%write(10000)
          call fmat%kill(lun_err)


          call CPU%SAVE%set('stop')
       end if

       !
       ! info state before linear system solution
       !
       if ( ctrl%info_newton .eq. 2 ) then
          !
          ! Jacobian info
          !
          write(msg1,'(I2,a,1pe9.1,a,1pe9.1)' ) &
               iter_newton+1,&
               ' | IN    : JACOBIAN(2,2): min =',minval(p1p0%D2),&
               ' max= ',maxval(p1p0%D2)
          write(lun_out,*)  etb(msg1)
          write(lun_stat,*) etb(msg1)
       end if
       
       !
       ! set controls for inversion of jacobian 
       ! of the newton iteration like:
       ! 1 - tolerance of solution
       !
       call set_linear_solver_tolerance(ctrl,&
            iter_newton,rhs_norm,rhs_norm,prev_res)
  
       !
       ! test and update
       !
       if ( rhs_norm .lt. ctrl%tol_nonlinear ) then
          inc_full=zero
          write(msg,'(I2,a,a,2(1pe8.1),a,a,2(1pe8.1)a,a,2(1pe8.1))') &
               iter_newton,&
               ' | OUT   : ',&
               'inc,rhs=',zero,rhs_norm,&
               ' | ',&
               'td : inc,rhs=', dnrm2(ntdens,inc_full(npot+1:nfull),1),dnrm2(ntdens,p1p0%fnewton_tdens,1),&
               ' | ',&
               'pot: inc,rhs=', dnrm2(ntdens,inc_full(1:npot),1),dnrm2(npot,p1p0%fnewton_pot,1)
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
          info=0
          return
       end if
       
       

       !
       ! select base don tdens
       !          
       if ( ctrl%threshold_tdens>1e-20 ) then
          call p1p0%set_active_regions(this,ctrl)

          if ( this%ntdens_on .ne. this%ntdens ) then
             write(msg1,'(I2,a,a,1(1f6.2),a,1(1f6.2))' ) &
                  iter_newton+1,&
                  ' | IN    : ',&
                  ' %tdens on =', this%ntdens_on*100.d0/this%ntdens, &
                  ' %pot   on =', this%npot_on*100.d0/this%npot
             write(lun_out,*)  etb(msg1)
             write(lun_stat,*) etb(msg1)
             call p1p0%near_kernel%set(this%npot_off, this%inactive_pot)
             
             !
             ! set to one D matrix to ensure inversion
             ! this term will be kill in B diag(gf) D diag(gf) BT 
             !
             do i=1,this%ntdens_off
                j = this%inactive_tdens(i)
                p1p0%D_matrix%diagonal(j) = 1.0d0
             end do
             !
             ! kill terms on Au-b that corresponding to "zero" tdens
             !
             rmax=zero
             do i=1,this%npot_off
                j = this%inactive_pot(i)
                rmax=max(rmax,abs(rhs_full(j)))
                rhs_full(j) = zero
                call p1p0%stiff%set_rowcol(j,zero)
             end do
             write(*,*) 'max pot rhs ',rmax 
             !
             ! kill terms on ode increment that corresponding to "zero" tdens
             !
             rmax=zero
             do i=1,this%ntdens_off
                j = this%inactive_tdens(i)
                rmax=max(rmax,abs(rhs_full(npot+j)))
                rhs_full(npot+j)=zero
             end do
             write(*,*) 'max tdens rhs ',rmax              
          end if
       end if

       !
       ! try to solve linear system J_F(k) inc(k) = -F(k)
       !
       old = .true. 
       if ( old ) then
          p1p0%deltatD1C_matrix%coeff =  - p1p0%deltatD1C_matrix%coeff 
          !rhs_full(npot+1:nfull) =    -    rhs_full(npot+1:nfull) 


          call p1p0%invert_jacobian( &
               iter_newton,&
               this,&       
               lun_err,lun_out,lun_stat, &
               ctrl, &
               current_time_iteration,current_time,deltat, &
               info,&
               CPU,&
               ode_inputs,&
               rhs_full, inc_full)
          
          call ortogonalize(p1p0%stiff%ncol,&
               1,&
               p1p0%kernel_full(1:npot,1),inc_full(1:npot))


       else
          
          p1p0%D_matrix%diagonal = p1p0%D2 
          !p1p0%deltatD1C_matrix%coeff = p1p0%deltatD1C_matrix%coeff
          rhs_full(npot+1:nfull) =    -    rhs_full(npot+1:nfull) 

          !p1p0%deltatD1C_matrix%coeff = p1p0%C_matrix%coeff
          p1p0%deltatD1C_matrix%coeff = p1p0%B_matrix%coeff
          p1p0%scr_ntdens = deltat*p1p0%D1 
          call p1p0%deltatD1C_matrix%DxM(lun_err, p1p0%scr_ntdens )

          !call p1p0%BT_matrix%info(6)
          !call        p1p0%BT_matrix%kill(0)
          !p1p0%BT_matrix = p1p0%B_matrix
          !call p1p0%BT_matrix%info(6)
          !call p1p0%BT_matrix%transpose(lun_err)
          !call p1p0%BT_matrix%info(6)
          !call p1p0%BT_matrix%transpose(lun_err,p1p0%transposer)
          do i=1,p1p0%BT_matrix%nterm
             p1p0%BT_matrix%coeff(p1p0%transposer(i)) = p1p0%B_matrix%coeff(i)
          end do


          inc_full=zero
          call p1p0%invert_jacobian_general( &
               p1p0%stiff,& ! A
               p1p0%B_matrix,&    ! B
               p1p0%BT_matrix,&   ! BT
               p1p0%deltatD1C_matrix,&      ! C
               p1p0%D_matrix, & ! D
               .false.,&         ! B=C
               rhs_full(1:npot),& ! f 
               rhs_full(npot+1:npot+ntdens), & ! g
               inc_full(1:npot),& ! vec_x
               inc_full(npot+1:npot+ntdens),& ! vec_y
               iter_newton,&
               this,ode_inputs,&       
               lun_err,lun_out,lun_stat, &
               ctrl, &
               current_time_iteration,current_time,deltat, &
               info,&
               CPU)
       end if

       if ( this%ntdens .ne. this%ntdens_on) then
          do i=1,this%npot_off
             j=this%inactive_pot(i)
             inc_full(j)=zero
          end do
          do i=1,this%ntdens_off
             j=this%inactive_tdens(i)
             inc_full(npot+j)=zero
          end do
       end if

       

       
       !
       ! if increment was not computed properly exit
       !
       if ( info .ne. 0) return 
       

       !
       ! residum \| s \| and norm of rhs \| F \|
       !
       
       
       inc_norm = dnrm2(nfull,inc_full,1)     
       rhs_norm   = dnrm2(nfull,rhs_full,1)

       
       this%scr_npot=one
       call ortogonalize(npot,&
            1,& 
            this%scr_npot,inc_full(1:npot))
       
       !
       ! update iteration number
       !
       iter_newton = iter_newton + 1
       if ( ctrl%info_newton .gt. 0) then
          write(msg,'(I2,a,a,2(1pe8.1),a,a,2(1pe8.1)a,a,2(1pe8.1))') &
               iter_newton,&
               ' | OUT   : ',&
               'inc,rhs=',inc_norm,rhs_norm,&
               ' | ',&
               'td : inc,rhs=', dnrm2(ntdens,inc_full(npot+1:nfull),1),dnrm2(ntdens,p1p0%fnewton_tdens,1),&
               ' | ',&
               'pot: inc,rhs=', dnrm2(ntdens,inc_full(1:npot),1),dnrm2(npot,p1p0%fnewton_pot,1)
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if
       !
       ! check if newton is converging
       !
       if ( iter_newton .gt. 1) then
          if ( ( rhs_norm  .gt. prev_rhs_norm * 1.0d2  )  &
               ! .or. (  inc_norm  .gt. prev_inc_norm * 1.0d2 ) ) &
               ) &
              then
             write(msg,*)'prev rhs_norm', prev_rhs_norm , &
                  'current rhs norm',  rhs_norm  
             write(lun_out,*) etb(msg)
             write(lun_stat,*) etb(msg)
!!$             write(msg,*)'prev inc_norm', prev_inc_norm , &
!!$                  'current inc norm',  inc_norm  
!!$             write(lun_out,*) etb(msg)
!!$             write(lun_stat,*) etb(msg)
             info=-4
             rc = IOerr(lun_err, wrn_val,&
                  ' implicit_euler_newton', &
                  ' norm of residua of rhs was not decreasing ' ) 
             return
          end if
       end if
       
       !
       ! store info linear solver 
       !
       this%sequence_info_solver(iter_newton)  = this%info_solver
       this%sequence_build_prec(iter_newton)   = ctrl%build_prec
       this%sequence_build_tuning(iter_newton) = ctrl%build_tuning
       this%sequence_inc_norm(iter_newton)     = min(inc_norm,rhs_norm)
       
       !
       ! test and update
       !
       test_exit = ( rhs_norm .lt. ctrl%tol_nonlinear )

       !
       ! update
       !
       if ( .not. test_exit ) then
          this%pot   = this%pot   + inc_full(1:npot)
          this%tdens = this%tdens + inc_full(npot+1:npot+ntdens)
          do i=1,ntdens
             this%tdens(i) = max(this%tdens(i),ctrl%min_tdens)
          end do
       end if  

       
       if (( ctrl%max_bfgs > 0) .and. ( info .eq. 0) )then
          slot = mod(this%total_number_linear_system, ctrl%max_bfgs )+1
          write(*,*) 'total lin.sys=',this%total_number_linear_system, 'slot=', slot, 'ctrl%max_bfgs',ctrl%max_bfgs
          p1p0%matrix_V%coeff(1:npot,slot) = inc_full(1:npot)
       end if

       !
       ! check for restarting newton
       !            
       if ( iter_newton .eq. ctrl%max_iter_nonlinear ) then
          test_exit = .true.
          info = -2
       end if
      

       !---------------------------------------------------
       ! Store info before jacobian is updated 
       !---------------------------------------------------
       prev_res      = this%info_solver%resreal

       !
       ! store F newton for broyden
       !
       p1p0%fnewton_pot_old   = p1p0%fnewton_pot
       p1p0%fnewton_tdens_old = p1p0%fnewton_tdens         
       
    end do
    this%iter_nonlinear=iter_newton
    
    this%scr_npot=one
    call ortogonalize(npot,&
         1,& 
         this%scr_npot,this%pot)
    call this%build_nrm_grad_vars(p1p0,lun_err)
    pode = ode_inputs%pode(2)
    call p1p0%build_nrm_grad_dyn(this,pode)


    this%res_elliptic = &
         dnrm2(npot,p1p0%fnewton_pot,1) / &
         dnrm2(npot,ode_inputs%rhs_integrated(:,2),1) 
   
    !
    ! free memory
    ! 
    do i=1,ctrl%nbroyden
       call p1p0%broyden_updates(i)%kill(lun_err)
       call p1p0%rankone_update(i)%kill(lun_err)

    end do

  contains

        subroutine assembly_rhs_reduced(B_matrix,&
         invD2,fnewton_tdens,fnewton_pot,work,rhs_reduced)
      use Globals
      use SparseMatrix
      implicit none
      type(spmat),       intent(inout) :: B_matrix
      real(kind=double), intent(in   ) :: invD2(B_matrix%nrow)
      real(kind=double), intent(in   ) :: fnewton_pot(B_matrix%nrow)
      real(kind=double), intent(in   ) :: fnewton_tdens(B_matrix%ncol)
      real(kind=double), intent(inout) :: work(B_matrix%nrow)
      real(kind=double), intent(inout) :: rhs_reduced(B_matrix%ncol)
      !local

      do i=1,B_matrix%nrow
         work(i)=invD2(i)*fnewton_tdens(i)
      end do

      call B_matrix%MTxv(work,rhs_reduced)
      call daxpy(B_matrix%ncol,-one,fnewton_pot,1,rhs_reduced,1)

    end subroutine assembly_rhs_reduced


    subroutine get_inc_tdens(BC_matrix,deltat,D1,invD2,inc_pot,fnewton_tdens,work,inc_tdens)
      use Globals
      use SparseMatrix
      implicit none
      type(spmat),       intent(inout) :: BC_matrix
      real(kind=double), intent(in   ) :: deltat
      real(kind=double), intent(in   ) :: D1(BC_matrix%nrow)
      real(kind=double), intent(in   ) :: invD2(BC_matrix%nrow)
      real(kind=double), intent(in   ) :: inc_pot(BC_matrix%ncol)
      real(kind=double), intent(in   ) :: fnewton_tdens(BC_matrix%nrow)
      real(kind=double), intent(inout) :: work(BC_matrix%nrow)
      real(kind=double), intent(inout) :: inc_tdens(BC_matrix%nrow)
      !local

      call BC_matrix%Mxv(inc_pot,work)

      do i = 1,BC_matrix%nrow
         inc_tdens(i) = invD2(i) * ( &
              deltat * D1(i) * work(i) - fnewton_tdens(i) &
              )
      end do

    end subroutine get_inc_tdens

  end subroutine implicit_euler_newton
    
  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (itria) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine set_B_matrix_general(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)


    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
                                ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'set_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'set_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine set_B_matrix_general


  



  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine set_B_matrix_grid(lun_err,grid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: grid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(3,grid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, inode, j,k,ind,icol
    integer :: nnode, ncell, nnodeincell
    integer :: start, finish,nodes(3)
    integer , allocatable :: nnode_in_cell(:),ia(:),ja(:)


    ncell        = grid%ncell
    nnode        = grid%nnode
    nnodeincell  = grid%nnodeincell

    ! allocate work array 
    allocate(&
         nnode_in_cell(ncell),&
         ia(ncell+1),&
         ja(ncell*3),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'subnode_topol', &
         'alloc fail for temp array nnode_in_cell ia ja')


    call B_matrix%init(lun_err, &
         ncell, nnode, ncell*nnodeincell,&
         storage_system='csr',&
         is_symmetric =.false.)
    
    nnode_in_cell=0
    ia=0
    ja=0

    do icell=1,ncell+1
       ia(icell)=1+(icell-1)*nnodeincell
    end do

    do icell = 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       ja(start:finish) = grid%topol(1:nnodeincell,icell)
       call isort(nnodeincell,ja(start:finish))
    end do

    !
    ! assign ia,ja
    !      
    B_matrix%ia=ia
    B_matrix%ja=ja

    !
    ! assembly of trija
    !
    do icell= 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       nodes=grid%topol(1:nnodeincell,icell)
       do ind=start,finish
          icol=ja(ind)
          k=1
          do while ( icol .ne. nodes(k) )
             k=k+1
          end do
          trija(k,icell) = ind
       end do
    end do



    deallocate(nnode_in_cell,ia,ja,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'subnode_topol', &
         'dealloc fail for temp array nnode_in_cell ia ja')

  end subroutine set_B_matrix_grid



!!$  subroutine my_linear_solver(&
!!$       matrix,rhs,sol,&
!!$       ctrl_solver,&
!!$       info_solver,&
!!$       diagonal_scale,&
!!$       build_prec,&
!!$       ctrl_prec,&
!!$       info_prec,&
!!$       prec_saved,&
!!$       build_tuning,&
!!$       ctrl_tuning,&
!!$       info_tuning,&
!!$       spectral_saved,&
!!$       aux,&
!!$       infomod)
!!$    use Globals
!!$    use Timing
!!$    use ControlParameters
!!$    !use Matrix
!!$    use ScalableMatrix
!!$    use SparseMatrix
!!$    use CombinedSparseMatrix
!!$    !use PCG
!!$    use PreconditionerTuning
!!$    use Scratch
!!$    use LinearSolver
!!$    use ScalableMatrix
!!$
!!$    implicit none
!!$    class(scalable_mat),      target,     intent(inout) :: matrix
!!$    real(kind=double),                 intent(inout) :: rhs(matrix%nrow)
!!$    real(kind=double),                 intent(inout) :: sol(matrix%ncol)
!!$    type(output_solver),               intent(inout) :: info_solver
!!$    type(input_solver),                intent(in   ) :: ctrl_solver    
!!$    integer,                           intent(in   ) :: diagonal_scale
!!$    ! input/output vars for standard preconditioner
!!$    integer,        optional,          intent(in   ) :: build_prec
!!$    type(input_prec), optional,     intent(in   ) :: ctrl_prec
!!$    integer,        optional,          intent(inout) :: info_prec
!!$    type(stdprec),  optional, target,  intent(inout) :: prec_saved
!!$    ! input/output vars for tuning
!!$    integer,                           intent(in   ) :: build_tuning
!!$    type(input_tuning),optional,       intent(in   ) :: ctrl_tuning       
!!$    integer,       optional,           intent(inout) :: info_tuning
!!$    type(eigen),   optional, target,   intent(inout) :: spectral_saved
!!$    ! auxiliary scratch vars
!!$    type(scrt),    optional,           intent(inout) :: aux
!!$    integer, optional, intent(in) :: infomod
!!$    !local  
!!$    logical :: rc  
!!$    integer :: res,i
!!$    integer :: lun_err, lun_out,nequ
!!$    integer :: info
!!$    real(kind=double), allocatable :: inv_sqrt_diagonal(:)
!!$    real(kind=double) :: dnrm2
!!$    type(spmat),   pointer :: matrix2prec
!!$    type(stdprec), pointer :: prec_zero
!!$    type(stdprec), target  :: prec_local
!!$    type(pcgcoeff) :: abpres
!!$    class(abs_linop), pointer :: prec1,prec2
!!$    class(stdprec), pointer :: prec_sparse
!!$    type(stdprec),target :: prec_loc
!!$    class(abs_linop), pointer :: final_prec
!!$    type(tunedprec), target :: combined_prec
!!$    type(eye) ,  target :: identity
!!$    !type(tunedprec) :: combined_prec
!!$    character(len=256):: msg
!!$
!!$    lun_err  = ctrl_solver%lun_err
!!$    lun_out  = ctrl_solver%lun_out
!!$
!!$    nequ=matrix%ncol
!!$    msg=''
!!$
!!$    if ( diagonal_scale .eq. 1 ) then
!!$       ! allocation working array
!!$       allocate(inv_sqrt_diagonal(nequ),stat=res)
!!$       if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'pcg_solver', &
!!$            'work array diagonal',res)
!!$
!!$       ! D=Diag(M) temporary stored in inv_sqrt_diagonal
!!$       call matrix%get_diagonal(inv_sqrt_diagonal)
!!$
!!$       ! D=Diag(M)^{-1/2}
!!$       inv_sqrt_diagonal=one/sqrt(inv_sqrt_diagonal)
!!$
!!$
!!$
!!$       call scale_system(lun_err, matrix, rhs, sol, inv_sqrt_diagonal)
!!$       msg=etb(etb(msg)//'System scaled by diag |' )
!!$    end if
!!$    if( present(infomod)) then
!!$       if ( infomod .eq. 2 ) write(lun_out,*) 'Diagonal scale'
!!$    end if
!!$    !
!!$    ! Procedures to assembly preconditioner
!!$    !
!!$
!!$    !
!!$    ! Select matrix to use for build the preconditioner
!!$    !
!!$    select type(matrix)
!!$    type is (spmat)
!!$       matrix2prec => matrix
!!$    type is (combspmat)
!!$       matrix2prec => matrix%A_matrix
!!$    end select
!!$
!!$    !
!!$    ! Assign standard preconditioner to pointer prec_zero
!!$    !
!!$    info = 0 
!!$    if ( build_prec .eq. 1 ) then
!!$       msg = etb(msg)//' prec. build |'
!!$    else
!!$       msg = etb(msg)//' used saved prec. |'
!!$    end if
!!$
!!$    if (.not. present(prec_saved) ) then
!!$       prec_sparse => prec_loc
!!$    else
!!$       prec_sparse => prec_saved
!!$    end if
!!$    call  assembly_stdprec(lun_err,&
!!$         matrix2prec,&
!!$         build_prec,&
!!$         ctrl_prec,&
!!$         info,&
!!$         prec_sparse)
!!$    if ( present(info_prec) ) info_prec = info
!!$    if( present(infomod)) then
!!$       if ( infomod .eq. 2 ) write(lun_out,*) 'Assembly Preconditioner'
!!$    end if
!!$    !
!!$    ! Build spectral_saved
!!$    ! if build_runing == 1 then
!!$    !    ctrl_tunig%ieig == 1 Use Dacg and compute/update Eigen
!!$    !                         spectral_saved of matrix
!!$    !    ctrl_tunig%ieig == 2 Prepare abpres varibles to store data from PCG
!!$    !                         The eigen. will be compute after 
!!$    !                         the linear system will be solved
!!$    !                         spectral_tuning points to previously computed
!!$    !                         eigen.
!!$    !
!!$    info = 0 
!!$    if (ctrl_tuning%ieig .ne. 0) then
!!$       if ( build_tuning .eq. 1 ) then
!!$          msg = etb(msg)//' tuning build |'
!!$       else
!!$          msg = etb(msg)//' used saved tuning|'
!!$       end if
!!$    end if
!!$
!!$    call  assembly_tuning(lun_err,lun_out,&
!!$         matrix,&
!!$         prec_sparse,&
!!$         build_tuning,&
!!$         ctrl_tuning,&
!!$         info,&
!!$         spectral_saved,&
!!$         abpres)   
!!$    if ( info .ne. 0) rc = IOerr(lun_err, wrn_inp, &
!!$         'my_linear_solver', &
!!$         ' error in tuned preconditioner construction',info )
!!$    if ( present(info_tuning) ) info_tuning = info
!!$    if( present(infomod)) then
!!$       if ( infomod .eq. 2 ) write(lun_out,*) 'Assembly Tuning'
!!$    end if
!!$
!!$       
!!$
!!$    ! assembly combined preconditioner
!!$    if ( ( ctrl_tuning%ieig .gt. 0         ) .and. &
!!$         ( spectral_saved%is_initialized  ) .and. &
!!$         ( spectral_saved%computed .gt. 0 ) .and. &
!!$         ( info .eq. 0 ) ) then
!!$       call combined_prec%init(lun_err,&
!!$            prec_sparse,&
!!$            ctrl_tuning,&
!!$            spectral_saved)
!!$       prec1 => combined_prec
!!$    else
!!$       prec1 => prec_sparse
!!$    end if
!!$
!!$    
!!$    call identity%init(nequ)
!!$    prec2 => identity
!!$
!!$    
!!$     
!!$
!!$    !
!!$    ! Preconditioner assembled 
!!$    !
!!$    !-------------------------------------------------------
!!$
!!$    !write(ctrl_solver%lun_out,'(a)') etb(msg)
!!$    ! SOLVE LINEAR SYSTEM
!!$    info_solver%ierr = 0
!!$    if ( abpres%is_initialized ) then
!!$       ! collect info if required
!!$       call linear_solver(matrix, rhs, sol,&
!!$            info_solver, &
!!$            ctrl_solver,&
!!$            prec_left=prec1,&
!!$            prec_right=prec2,&
!!$            aux=aux,&
!!$            abpres=abpres)
!!$    else
!!$       call linear_solver(matrix, rhs, sol,&
!!$            info_solver, &
!!$            ctrl_solver,&
!!$            prec_left=prec1,&
!!$            prec_right=prec2,&
!!$            aux=aux)
!!$    end if
!!$
!!$    if (info_solver%ierr .ne. 0) then
!!$       rc = IOerr(lun_err, wrn_out, 'my_linear_solver', &
!!$            'convergence not achieved info%ierr=', &
!!$            info_solver%ierr)
!!$    end if
!!$    if ( ( build_tuning        .eq. 1 ) .and. &
!!$         ( ctrl_tuning%ieig    .eq. 2 ) .and. &
!!$         ( abpres%is_initialized      ) .and. &
!!$         ( abpres%npres_stored .gt. 0 ) ) then
!!$       !
!!$       ! compute eigenvalues
!!$       !
!!$       call spectral_saved%lanczos_eigprec(&
!!$            ctrl_tuning%ctrl_dacg%lun_err,&
!!$            ctrl_tuning%ctrl_dacg%lun_out,&
!!$            abpres,&
!!$            matrix,&
!!$            prec_sparse)  
!!$       call abpres%kill(lun_err)
!!$    end if
!!$
!!$
!!$    if ( diagonal_scale .eq. 1 ) then
!!$       !
!!$       !call scale_back(nequ, sol, inv_sqrt_diagonal)
!!$
!!$       inv_sqrt_diagonal = one / inv_sqrt_diagonal
!!$       call scale_system(lun_err, matrix, rhs, sol, inv_sqrt_diagonal)
!!$       !call matrix%Mxv(sol,inv_sqrt_diagonal)
!!$       !inv_sqrt_diagonal=inv_sqrt_diagonal-rhs
!!$       ! write(6,*) 'real error', dnrm2(nequ,inv_sqrt_diagonal,1)
!!$
!!$
!!$       deallocate(inv_sqrt_diagonal,stat=res)
!!$       if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'pcg_solver', &
!!$            'work array diagonal',res)
!!$    end if
!!$
!!$    ! free memory
!!$    
!!$    call combined_prec%kill(lun_err)
!!$    
!!$    ! 
!!$    call identity%kill()
!!$    prec1 => null()
!!$    prec2 => null()
!!$        
!!$  end subroutine my_linear_solver
!!$   
  !>----------------------------------------------------------
  !> Subroutine to assign the pointer prec_zero to a
  !> stdprec. Two option are available
  !> prec_zero => prec_saved
  !> prec_zero => prec_local
  !>--------------------------------------------------------
  subroutine assembly_stdprec(lun_err,&
         matrix2prec,&
         build_prec,&
         ctrl_prec,&
         info_prec,&
         prec_saved)!,prec_local, prec_zero)
      use Globals
      implicit none
      integer,                intent(in   ) :: lun_err
      type(spmat),            intent(inout) :: matrix2prec
      integer,                intent(in   ) :: build_prec
      type(input_prec),    intent(in   ) :: ctrl_prec
      integer,                intent(inout) :: info_prec
      type(stdprec),          intent(inout) :: prec_saved
      !type(stdprec), target,  intent(inout) :: prec_local
      !type(stdprec), pointer, intent(inout) :: prec_zero
      ! local
      logical :: rc
      type(stdprec)  :: prec_local
      
      if ( build_prec .eq. 1 ) then
         info_prec=0
         ! assembly prec on local work prec
         call prec_local%init(lun_err, info_prec,&
              ctrl_prec, matrix2prec%nrow, matrix2prec)
         if ( info_prec .eq. 0 ) then            
            prec_saved = prec_local
         else
            !
            ! write warning 
            !
            ! prec_saved is updated with new preconditioner
            !
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 'using saved prec. assembly failed info_prec=', info_prec)
         end if
         call prec_local%kill(lun_err)


      else
         info_prec = 0
         ! use saved prec.
         if ( .not. prec_saved%is_built) then
            ! check if prec_saved contains something
            rc = IOerr(lun_err, wrn_val, 'assembly_stdprec', &
                 ' prec. saved not built')
            info_prec = -1
         end if
         
      end if

    end subroutine assembly_stdprec


    !>--------------------------------------------------------
    !> Subroutine to assign the pointer spectral_tuning 
    !> to eigen. 
    !> spectral_tuning => spectral_saved
    !> spectral_tuning => spectral_local
    !>--------------------------------------------------------
    subroutine assembly_tuning(lun_err,lun_out,&
         matrix,&
         prec_zero,&
         build_tuning,&
         ctrl_tuning,&
         info_tuning,&
         spectral_saved,&
         abpres)
      use Globals
      use Eigenv
      use Matrix
      use PreconditionerTuning
      implicit none
      integer,               intent(in   ) :: lun_err

      integer,               intent(in   ) :: lun_out
      class(abs_matrix),     intent(inout) :: matrix
      class(abs_linop),       intent(inout) :: prec_zero
      integer,               intent(in   ) :: build_tuning
      type(input_tuning),    intent(in   ) :: ctrl_tuning
      integer,               intent(inout) :: info_tuning
      type(eigen),           intent(inout) :: spectral_saved
      type(pcgcoeff),        intent(inout) :: abpres
      ! local
      logical :: rc
      integer :: nequ
      integer :: ierr_dacg=0
      type(output_dacg) :: info_dacg
      type(eigen) :: spectral_local

      nequ        = matrix%nrow
      info_tuning = 0
      if ( ctrl_tuning%ieig .eq. 0 ) then
         ! set null pointer
         !spectral_tuning => null()
         return 
      else
         ! if some tuning is used
         ! ( build_tuning .eq. 1 ) update
         ! ( build_tuning .eq. 0 ) use stored eigenvectors
         if ( build_tuning .eq. 1 ) then
            select case ( ctrl_tuning%ieig )
               ! select tuning procedure
            case (1)
               !
               ! DACG
               ! compute and store eigenvalues and eigenvectors
               !
               ierr_dacg=0
               call spectral_local%init(lun_err,&
                    ctrl_tuning%nev,&
                    nequ)
               call info_dacg%init(lun_err,ctrl_tuning%nev)
               call spectral_local%dacg_algorithm(&
                    ctrl_tuning%ctrl_dacg,&
                    info_dacg,&
                    matrix,&
                    prec_zero)
               call info_dacg%info(lun_out)
               info_tuning = info_dacg%ierr 
               if ( info_tuning .ne. 0) then
                  ! redirect spectral_tuning to passed eigen.
                  rc = IOerr(lun_err, wrn_inp, 'assembly_tuning', &
                       ' eigen. assembly failed- no tuning used')
                  call info_dacg%info(lun_err)
               else
                  ! make a copy
                  spectral_saved = spectral_local
               end if
               call info_dacg%kill(lun_err)
               call spectral_local%kill(lun_err)
            case (2)
               !
               ! LANCZOS
               !
               ! init var to collect variables from solver procedure
               call abpres%init(lun_err,nequ,ctrl_tuning%npres)
            end select
         end if
      end if

    end subroutine assembly_tuning

    !>--------------------------------------------------------------
    !> Subroutine to assign to set controls for newton update
    !> To be used after assembly ( all varibles have to be calculated) 
    !> before linear system solution
    !>--------------------------------------------------------------
    subroutine set_linear_solver_tolerance(ctrl,&
         iter_newton,nrm_rhs,prev_nrm_rhs,prev_res)
      use Globals
      implicit none
      type(CtrlPrm),         intent(inout) :: ctrl
      integer,               intent(in   ) :: iter_newton
      real(kind=double),     intent(in   ) :: nrm_rhs
      real(kind=double),     intent(in   ) :: prev_nrm_rhs
      real(kind=double),     intent(in   ) :: prev_res
      !local
      real(kind=double) ::  nu
      integer :: threshold


      !
      ! inexact newton tolerance
      !
      select case (ctrl%inexact_newton) 
      case (0)
         ctrl%ctrl_solver%tol_sol = ctrl%ctrl_solver_original%tol_sol
         ctrl%ctrl_solver%iexit   = 0
         ctrl%ctrl_solver%iexit   = 1
      case (1)
         ctrl%ctrl_solver%iexit    = 0
         ctrl%ctrl_solver%isol     = 1
         if (ctrl%ctrl_solver%scheme == 'MINRES') then
            nu =1.0d-5
         else
            nu = 1.0d-5
         end if
         ctrl%ctrl_solver%tol_sol = nu * nrm_rhs
         ctrl%ctrl_solver%tol_sol = &
              max(&
              ctrl%ctrl_solver%tol_sol,&
              ctrl%ctrl_solver_original%tol_sol)
      case (2)
         ctrl%ctrl_solver%tol_sol = ctrl%ctrl_solver_original%tol_sol
         ctrl%ctrl_solver%iexit    = 1
         ctrl%ctrl_solver%isol     = 1
      end select



     
         
    end subroutine set_linear_solver_tolerance


    !>--------------------------------------------------------------
    !> Procedure to control the delta_t of the time-stepping
    !> (private procedure used in upadte)
    !<---------------------------------------------------------------
    subroutine  control_deltat(lun_out,ntdens,&
         tdens,increment,previous_var,&
         ctrl,grid_tdens,deltat)
      use Globals
      use ControlParameters
      implicit none

      integer,           intent(in   ) :: lun_out
      integer,           intent(in   ) :: ntdens
      real(kind=double), intent(in   ) :: tdens(ntdens)
      real(kind=double), intent(in   ) :: increment(ntdens)
      real(kind=double), intent(in   ) :: previous_var
      type(CtrlPrm),     intent(in   ) :: ctrl
      type(abs_simplex_mesh),        intent(in   ) :: grid_tdens
      real(kind=double), intent(inout) :: deltat

      !local
      real(kind=double) :: sup_rhs, cnst=1e-8
      real(kind=double) :: n2,n2tdens
      character(len=256):: out_format



      if (ctrl%id_time_ctrl == 1) then
         deltat=ctrl%deltat 
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if
      if (ctrl%id_time_ctrl == 2) then
         deltat=min(deltat * ctrl%exp_rate,ctrl%upper_bound_deltat)
         out_format=ctrl%formatting('ar')
         write(lun_out,out_format) 'time step = ',deltat
      end if

      if (ctrl%id_time_ctrl == 3) then
         sup_rhs= maxval(increment)
         if ( sup_rhs < zero ) then
            deltat = min(deltat * ctrl%exp_rate,ctrl%upper_bound_deltat)
         else
            deltat=(1.0d0-ctrl%lower_bound_deltat)/sup_rhs
         end if
         deltat=min(deltat,ctrl%upper_bound_deltat)
         
         out_format=ctrl%formatting('araar')
         write(lun_out,out_format) 'time step = ',deltat,&
              ' | ', ' linfty norm increment = ', sup_rhs
      end if

      if (ctrl%id_time_ctrl == 4) then
         if (previous_var .ne. zero ) then
            deltat=2.0d-1/previous_var
            deltat=min(deltat,ctrl%upper_bound_deltat)
            deltat=max(deltat,ctrl%lower_bound_deltat)
         end if
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            deltat=max(deltat,ctrl%lower_bound_deltat)
         end if
      end if
      
      if (ctrl%id_time_ctrl == 5) then
         sup_rhs =  maxval(increment)         
         if ( 1-deltat*maxval(increment) < zero) then
            deltat = (1.0d0-cnst)/maxval(increment)
            write(lun_out,*) 'time step = ',deltat            
         else
            if ( sup_rhs .gt. zero ) deltat = one / maxval(increment)
            if ( sup_rhs .lt. zero ) deltat = one / maxval(abs(increment))
         end if
         deltat=min(deltat,ctrl%upper_bound_deltat)
         deltat=max(deltat,ctrl%lower_bound_deltat)
      end if


    end subroutine control_deltat
    
    !>---------------------------------------------------------
    !> Procedure to write into file the variables involved in the
    !> newotn procedure.
    !>---------------------------------------------------------
    subroutine write_newton_p1p0(p1p0,&
         lun_err,lun_work,&
         directory,tail,&
         deltat)
      use Globals
      implicit none
      class(p1p0_space_discretization), intent(in   ) :: p1p0
      integer,                          intent(in   ) :: lun_err
      integer,                          intent(in   ) :: lun_work
      character(len=*),                 intent(in   ) :: directory
      character(len=*),                 intent(in   ) :: tail
      real(kind=double),                intent(in   ) :: deltat
      ! local 
      integer :: i
      character(len=256) :: fname
      type(file) :: fmat
      integer :: ntdens, npot
      
      ntdens = p1p0%ntdens
      npot   = p1p0%npot


      fname=etb(etb(directory)//'/stiff_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%stiff%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/b_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%B_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/bt_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%BT_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/c_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%C_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/deltatd1c_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      call p1p0%deltatD1C_matrix%write(fmat%lun,'matlab')
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/d1_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) p1p0%D1(i)
      end do
      call fmat%kill(lun_err)

      fname=etb(etb(directory)//'/d2_matrix_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) p1p0%D2(i)
      end do
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/deltat_'//etb(tail))
      call fmat%init(lun_err,fname,10000,'out')
      write(fmat%lun,*) deltat 
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/rhs_pot_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,npot
         write(fmat%lun,*) -p1p0%fnewton_pot(i)
      end do
      call fmat%kill(lun_err)


      fname=etb(etb(directory)//'/rhs_tdens_'//etb(tail))
      call fmat%init(lun_err,fname,lun_work,'out')
      do i=1,ntdens
         write(fmat%lun,*) -p1p0%fnewton_tdens(i)
      end do
      call fmat%kill(lun_err) 


    end subroutine write_newton_p1p0
  


    subroutine assembly_newton_p1p0(p1p0,&
         tdpot,&
         lun_err,lun_out,debug,&
         current_newton_iteration,&
         current_time_iteration,current_time,deltat,&
         ode_inputs)
         !fnewton_pot,fnewton_tdens)
      use Globals
      use IOdefs
      use OdeInPuts
    implicit none
    class(p1p0_space_discretization), target, intent(inout) :: p1p0
    class(tdpotsys),   intent(inout) :: tdpot
    integer,           intent(in   ) :: lun_err, lun_out, debug
    type(odedata),     intent(in   ) :: ode_inputs
    integer,           intent(in   ) :: current_newton_iteration
    integer,           intent(in   ) :: current_time_iteration
    real(kind=double), intent(in   ) :: current_time
    real(kind=double), intent(inout) :: deltat

    
    ! local
    logical :: rc,endfile,exit_test
    integer :: res
    integer :: i,j,i1,i2
    integer :: ntdens, npot
    real(kind=double) :: decay,pflux,pmass,pode,time,sav_deltat,dnrm2,maximum
    
    !
    ! copy dimensions
    !
    ntdens = p1p0%ntdens
    npot   = p1p0%npot    
    
    !
    ! 2 - assembly D1 D2 invD2 invD2_D1
    !     possible to changse deltat on
    !
    call p1p0%build_grad_vars(lun_err,tdpot)
    pode = ode_inputs%pode(2) 
    call p1p0%build_nrm_grad_dyn(tdpot,pode)
    

    if ( debug .eq. 1 ) write( lun_out, *) ' Build D1 D2 at time + deltat' 
    call assembly_D1D2D3(p1p0,ode_inputs, tdpot,deltat)
    p1p0%invD2    = one / p1p0%D2
    p1p0%invD2_D1 = p1p0%invD2 * p1p0%D1
    
    !
    ! Update D:matrix with new D2 (block 2,2) 
    ! 
    call p1p0%D_matrix%set(lun_err,p1p0%D2)
    
    !
    ! 1 - assembly stiffness
    !
    if ( debug .eq. 1 ) write( lun_out, *) &
         ' Assembly Stiffness'
    tdpot%scr_ntdens = tdpot%tdens + ode_inputs%lambda(2)
    call p1p0%assembly_stiffness_matrix(lun_err, tdpot%scr_ntdens)

    

 
    !
    ! 2 - assembly rect matrix B
    !
    if ( debug .eq. 1 ) write( lun_out, *) &
         ' Assembly B_matrix'
    
    ! evulation of gradx, grady, gradz 
    call p1p0%p1%eval_grad(tdpot%pot,tdpot%gradpot)
    ! build matrix B
    call p1p0%assembly_BC_matrix(tdpot, 2.0d0, p1p0%B_matrix)
    ! transpose coefficeint into BT
    do i=1,p1p0%BT_matrix%nterm
       p1p0%BT_matrix%coeff(p1p0%transposer(i)) = p1p0%B_matrix%coeff(i)
    end do
    
    
    
    !
    ! build matrix C
    !
    if ( ode_inputs%id_ode .eq. 1) then
       ! assemby rect matrix C for PP ode
       if ( debug .eq. 1 ) write( lun_out, *) &
            ' Assembly C_matrix'
       call p1p0%assembly_BC_matrix(tdpot, pode, p1p0%C_matrix)
       p1p0%BequalC = .False.
    else       
       p1p0%C_matrix%coeff=p1p0%B_matrix%coeff
       p1p0%BequalC = .True.
    end if
    !
    ! build matrix C^T
    !
    do i=1,p1p0%CT_matrix%nterm
       p1p0%CT_matrix%coeff(p1p0%transposer(i)) = p1p0%C_matrix%coeff(i)
    end do

    !
    ! build matrix delta D1 C
    !
    p1p0%deltatD1C_matrix%coeff = p1p0%C_matrix%coeff
    p1p0%scr_ntdens = deltat*p1p0%D1 
    call p1p0%deltatD1C_matrix%DxM(lun_err, p1p0%scr_ntdens ) 
    

    !
    ! if dirichlet point are present 
    ! change stiff, rhs to have zero increment
    ! 
    !
    !
    if ( ode_inputs%ndir > 0 ) then
       p1p0%scr_npot=zero
       call p1p0%p1%dirichlet_bc(lun_err,&
            p1p0%stiff,p1p0%scr_npot, tdpot%pot,&
            ode_inputs%ndir,&
            ode_inputs%dirichlet_nodes(:,2),&
            ode_inputs%dirichlet_values(:,2))
       do i=1,ode_inputs%ndir
          j=ode_inputs%dirichlet_nodes(i,2)
       end do
    end if


    p1p0%scr_npot = ode_inputs%rhs_integrated(:,2)
    if ( ode_inputs%ndir > 0 ) then
       do i=1,ode_inputs%ndir
          j=ode_inputs%dirichlet_nodes(i,2)
          p1p0%scr_npot(j) = zero
       end do
    end if  
    
    !
    ! assembly function $\Fnewton$
    !
    call assembly_fnewton_pot(p1p0,ode_inputs,tdpot)
    call assembly_fnewton_tdens(p1p0,ode_inputs,tdpot,deltat)

    j=0
    do i=1,tdpot%npot
       i1 = p1p0%stiff%ia(i)
       i2 = p1p0%stiff%ia(i+1)-1
       p1p0%norm_rows_stiff(i)=dnrm2(i2-i1+1,p1p0%stiff%coeff(i1:i2),1)
    end do
    
    j=0
    maximum=maxval(p1p0%norm_rows_stiff)
    do i=1,tdpot%npot
       if (abs(p1p0%norm_rows_stiff(i)) .lt. 1e-10 * maximum) j=j+1
    end do
    !write(*,*) ' nnz rows ', j,' nodes',tdpot%npot


!!$    do i=1,tdpot%npot
!!$       i1 = p1p0%BT_matrix%ia(i)
!!$       i2 = p1p0%BT_matrix%ia(i+1)-1
!!$       p1p0%norm_rows_BT(i)=maxval(tdpot%tdens(p1p0%BT_matrix%ja(i1:i2)))
!!$    end do

!!$    j=0
!!$    do i=1,tdpot%npot
!!$       if (abs(p1p0%norm_rows_BT(i)) .lt. 1e-10) then
!!$          j=j+1
!!$          !
!!$          ! set to zero entries of idir-column and row-idir   
!!$          !
!!$          call p1p0%stiff%set_rowcol(i,zero)
!!$          p1p0%stiff%coeff(p1p0%stiff%idiag(i)) = one
!!$       end if
!!$    end do
    !write(*,*) ' nnz rows ', j,' nodes',tdpot%npot
    
    
    !
    ! reassing jacobian matrix
    !
    p1p0%jacobian_full%mats(1)%mat => p1p0%stiff 
    p1p0%jacobian_full%mats(2)%mat => p1p0%BT_matrix
    p1p0%jacobian_full%mats(3)%mat => p1p0%deltatD1C_matrix
    p1p0%jacobian_full%mats(4)%mat => p1p0%D_matrix

    

    !
    ! reassing sym jacobian matrix
    !
    p1p0%sym_jacobian_full%mats(1)%mat => p1p0%stiff 
    p1p0%sym_jacobian_full%mats(2)%mat => p1p0%BT_matrix
    p1p0%sym_jacobian_full%mats(3)%mat => p1p0%B_matrix
    p1p0%sym_jacobian_full%mats(4)%mat => p1p0%G_matrix
      
    
          


  contains
    !
    ! local subroutines
    !
    

    
    
    subroutine assembly_D1D2D3(p1p0,ode_inputs,tdpot,deltat)
      implicit none 
      type(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),                   intent(in   ) :: ode_inputs
      type(tdpotsys),                  intent(in   ) :: tdpot
      real(kind=double),               intent(in   ) :: deltat

      !local
      integer :: icell
      real(kind=double) :: min,max
      real(kind=double) ::  pflux, pmass, decay, pode

      pflux = ode_inputs%pflux(2)
      pmass = ode_inputs%pmass(2)
      decay = ode_inputs%decay(2)
      pode  = ode_inputs%pode(2)

      do icell = 1,p1p0%ntdens
         !
         ! cycle that can be used to estimate D2 and adapt deltat
         !
        ! write(*,*) icell, p1p0%nrm_grad_dyn(icell)
         p1p0%D3(icell) = &
              pflux * tdpot%tdens(icell)**(pflux-one) * &
              p1p0%nrm_grad_dyn(icell) &
              - &
              decay*ode_inputs%kappa(icell,2) * &
              pmass * tdpot%tdens(icell)**(pmass-one) 
      end do


      do icell = 1,p1p0%ntdens
         !
         ! asembly D2
         !
         p1p0%D2(icell) = one - deltat * p1p0%D3(icell)
      end do

      do icell = 1,p1p0%ntdens
         !
         ! asembly D1 D2
         !
         p1p0%D1(icell) = pode * ( tdpot%tdens(icell)**pflux ) &
              / p1p0%grid_tdens%size_cell(icell)
      end do
      

    end subroutine assembly_D1D2D3

!!$    subroutine assembly_C_matrix(p1,pot,pflux,trija,C_matrix)
!!$      use Globals
!!$      use SparseMatrix
!!$      implicit none
!!$      type(p1gal),       intent(in   ) :: p1
!!$      real(kind=double), intent(in   ) :: pot(p1%grid%nnode)
!!$      real(kind=double), intent(in   ) :: pflux
!!$      integer,           intent(in   ) :: trija(p1%grid%nnodeincell,p1%grid%ncell)
!!$      type(spmat),       intent(inout) :: C_matrix
!!$      !local
!!$      integer :: icell_sub,itria_parent,iloc,ind
!!$      integer :: nnodeincell,ndim
!!$      real(kind=double) :: pot_cell(4),gradpot_cell(3),grad_base(3), area_subgrid
!!$      real(kind=double) :: weight_grad_norm      
!!$      real(kind=double) :: ddot,dnrm2
!!$
!!$      nnodeincell = p1%grid%nnodeincell
!!$      ndim        = p1%grid%ambient_dimension
!!$
!!$      C_matrix%coeff  = zero
!!$      C_matrix%kernel = one
!!$
!!$      do icell_sub=1,p1%grid%ncell
!!$         ! copy index of parent triangle
!!$         !itria_parent = p1%grid%cell_parent(icell_sub)      
!!$
!!$         ! copy values of pot and compute grad on the cell
!!$         pot_cell(1:nnodeincell) = pot(p1%grid%topol(1:nnodeincell,icell_sub))     
!!$         call p1%eval_gradcell(icell_sub,pot_cell,gradpot_cell)
!!$
!!$         ! copy area of triangle in subgrid            
!!$         area_subgrid = p1%grid%size_cell(icell_sub)
!!$
!!$         !  
!!$         weight_grad_norm = dnrm2(ndim, gradpot_cell,1)**(pflux-2.0d0)
!!$
!!$         ! get index of triangle of coaser grid     
!!$         do iloc = 1,nnodeincell
!!$            ! get gradient of local base function
!!$            call p1%get_gradbase(iloc,icell_sub, grad_base)
!!$            
!!$            ! add contribution
!!$            ind=trija(iloc,icell_sub)
!!$            C_matrix%coeff(ind) = C_matrix%coeff(ind) + &
!!$                 ddot(ndim, gradpot_cell(1:ndim),1, grad_base(1:ndim), 1 ) * &
!!$                 weight_grad_norm * area_subgrid
!!$         end do
!!$      end do
!!$
!!$    end subroutine assembly_C_matrix
!!$
!!$    subroutine assembly_B_matrix(p1,pot,trija,B_matrix)
!!$      use Globals
!!$      use SparseMatrix
!!$      implicit none
!!$      type(p1gal),       intent(in   ) :: p1
!!$      real(kind=double), intent(in   ) :: pot(p1%grid%nnode)
!!$      integer,           intent(in   ) :: trija(p1%grid%nnodeincell,p1%grid%ncell)
!!$      type(spmat),       intent(inout) :: B_matrix
!!$      !local
!!$      integer :: icell_sub,icell_parent,iloc,ind
!!$      integer :: nnodeincell,ndim
!!$      real(kind=double) :: pot_cell(4),grad_cell(3),grad_base(3), area_subgrid
!!$      real(kind=double) :: ddot
!!$
!!$      nnodeincell = p1%grid%nnodeincell
!!$      ndim        = p1%grid%ambient_dimension
!!$
!!$      B_matrix%kernel = one
!!$      B_matrix%coeff  = zero
!!$
!!$      do icell_sub=1,p1%grid%ncell
!!$         ! copy index of parent triangle
!!$         !icell_parent = p1%grid%cell_parent(icell_sub)      
!!$
!!$         ! copy values of pot and compute grad on the cell
!!$         pot_cell(1:nnodeincell) = pot(p1%grid%topol(1:nnodeincell,icell_sub))  
!!$         call p1%eval_gradcell(icell_sub,pot_cell(1:nnodeincell),grad_cell)
!!$
!!$         
!!$         ! copy area of triangle in subgrid            
!!$         area_subgrid = p1%grid%size_cell(icell_sub)
!!$
!!$         ! add the contribution 
!!$         ! \Grad \Pot \dot \Grad \Psi_j for j=1,..,nnodeincell
!!$         do iloc=1,nnodeincell   
!!$            ! get gradient of local base function
!!$            call p1%get_gradbase(iloc,icell_sub, grad_base)
!!$
!!$            ! add contribution
!!$            ind=trija(iloc,icell_sub)
!!$            B_matrix%coeff(ind) = B_matrix%coeff(ind) + &
!!$                 ddot( ndim,  &
!!$                 grad_cell(1:ndim),1,&
!!$                 grad_base(1:ndim),1 ) * &
!!$                 area_subgrid
!!$         end do
!!$      end do
!!$
!!$    end subroutine assembly_B_matrix



  end subroutine assembly_newton_p1p0

  
  !>--------------------------------------------------------------------
  !> Static Constructor 
  !> (public procedure for type p1p0_space_discretization)
  !> 
  !> usage: call var%init(&
  !>                lun_err,lun_out,lun_stat,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in] lun_err    -> integer. I/O unit for error message
  !> \param[in] lun_out    -> integer. I/O unit for output message
  !> \param[in] lun_stat   -> integer. I/O unit for statistic
  !> \param[in] ctrl       -> type(CtrlPrm). Controls variables
  !> \param[in] grid_tdens -> tyep(mesh). Mesh for tdens
  !> \param[in] grid_pot   -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_p1p0(this,&
       lun_err,lun_out,lun_stat,&
       ctrl,&
       id_ode,&
       id_subgrid, grid_tdens, grid_pot)
    implicit none
    class(p1p0_space_discretization),target, intent(inout) :: this
    integer,                          intent(in   ) :: lun_err
    integer,                          intent(in   ) :: lun_out
    integer,                          intent(in   ) :: lun_stat
    type(CtrlPrm),                    intent(in   ) :: ctrl
    integer,                          intent(in   ) :: id_ode
    integer,                          intent(in   ) :: id_subgrid
    type(abs_simplex_mesh), target,               intent(in   ) :: grid_tdens
    type(abs_simplex_mesh), target,               intent(in   ) :: grid_pot
    ! local
    logical :: rc
    integer :: res
    integer :: ntdens, npot, ngrad,i,iloc,jloc
    ! jacobian 
    type(array_mat) :: jacobian_list(4)
    integer :: block_structure(3,4)
    character(len=1) :: jacobian_directions(4)
    real(kind=double), allocatable :: kernel_full(:,:) 
    ! preconditioners
    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(input_prec) :: ctrl_prec
    type(eye),   target :: identity_npot
    integer ::info,icell_sub
    type(input_prec) :: ctrl_inverse
    real(kind=double) :: dnrm2
    real(kind=double) :: grad_base(3)
    
    this%id_subgrid = id_subgrid
    this%grid_tdens => grid_tdens
    this%grid_pot   => grid_pot
    
    !
    ! dimension assignment + local copy
    !
    this%ntdens    = grid_tdens%ncell
    this%npot      = grid_pot%nnode
    this%ngrad     = grid_pot%ncell
    this%ambient_dimension = grid_tdens%ambient_dimension
    this%nfull     =  this%ntdens +  this%npot

    

    ntdens   = this%ntdens    
    npot     = this%npot      
    ngrad = this%ngrad  
    

    !
    ! construction of p1 element space
    !
    call this%p1%init(lun_err,this%grid_pot)
    write(lun_stat,'(a)') 'p1 Galerkin initialized'
    
    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Stiffness Matrix Initiliazation'
    call this%stiff%init(lun_err,&
         npot, npot,  this%p1%nterm_csr,&
         storage_system='csr',&
         is_symmetric=.true.)
    this%stiff%ia=this%p1%ia_csr
    this%stiff%ja=this%p1%ja_csr
    
    
    


    write(lun_stat,'(a)') 'stiff initiliazed'
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Stiffness Matrix initiliazed'
    call this%near_kernel%init(lun_err,npot)
    call this%near_kernel_full%init(lun_err,npot+ntdens)
    this%near_kernel_full%base_kernel(1:npot) = one
    this%near_kernel_full%base_kernel(1+npot: npot+ntdens) = zero
    this%near_kernel_full%base_kernel(:) = &
         this%near_kernel_full%base_kernel(:)/dnrm2(npot+ntdens,this%near_kernel_full%base_kernel(:),1)
    

    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    allocate(&
         this%assembler_Bmatrix_subgrid(&
         this%grid_pot%nnodeincell,this%grid_pot%ncell),&
         this%assembler_Bmatrix_grid(&
         this%grid_tdens%nnodeincell,this%grid_tdens%ncell),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member assembler_Bmatrix_subgrid')
    
    if (ctrl%id_subgrid .eq. 1) then
       call assembly_B_matrix_general(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_subgrid) 
    else
       call assembly_B_matrix_grid(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_grid)
    end if
    

    !
    ! copy and transpose matrix_B
    !
    this%BT_matrix = this%B_matrix
    allocate (this%transposer(this%B_matrix%nterm),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member transposer')
    call this%BT_matrix%transpose(lun_err,this%transposer)


    !
    ! copies of B matrix to copy its structure
    !
    this%C_matrix=this%B_matrix
    this%deltatD1C_matrix = this%C_matrix
    !
    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! gf matrices
    !
    this%DB_matrix = this%B_matrix
    this%BTD_matrix = this%BT_matrix

    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! set dimensions for compunent od block matrix
    !  
    call this%D_matrix%init(lun_err, ntdens)
    call this%inv_D_matrix%init(lun_err, ntdens)


    ! set Kernel
    allocate(this%kernel_full(npot+ntdens,1),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' temporary member')
    this%kernel_full(1:npot,1)             = one
    this%kernel_full(npot+1:npot+ntdens,1) = zero

    ! set jacobian block structure (only dimensions)
    !
    ! (A          B^T )
    ! (-dt D1 C   D2  )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff 
    jacobian_directions(1) = 'N'

    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix
    jacobian_directions(2) = 'N'

    block_structure(:,3) = (/3,2,1/) ! -dt D1 B matrix in 2,1
    jacobian_list(3)%mat => this%deltatD1C_matrix
    jacobian_directions(3) = 'N'

    block_structure(:,4) = (/4,2,2/) ! D2       matrix in 2,2
    jacobian_list(4)%mat => this%D_matrix
    jacobian_directions(4) = 'N'

    call this%jacobian_full%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure,jacobian_directions, .false. ) 


    ! EFAMLB
    ! set symmetric jacobian block structure
    !
    if ( ctrl%debug .eq. 1 ) &
         write( lun_out, *) ' Init symmetric  jacobian '
    
    allocate(this%symmetrizer(ntdens),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         'init_p1p0', &
         'symmetrizer' ) 
   
    call this%G_matrix%init(lun_err, ntdens)
    
       


   
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure  
    call this%aux_bicgstab%init(lun_err, 0, 9*(npot+ntdens))

    ! Auxiliary var for PCG procedure  
    call this%aux_newton%init(lun_err,&
         0, 13*ntdens + 5 * npot + 4* (ntdens + npot))

    ! rhs scratch
    allocate(&
         this%rhs(npot),&
         this%rhs_ode(ntdens),&
         this%inc_ode(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member rhs')

    ! 
    ! work array
    ! 
    allocate(&
         this%tdens_prj(ngrad),&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         this%scr_ngrad(ngrad),&
         this%scr_nfull(npot+ntdens),&
         this%rhs_full(npot+ntdens),&
         this%sqrt_diag(npot),&
         this%scr_integer(npot+ntdens),&
         this%diagonal_laplacian(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', & 
        ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

    ! 
    ! work array
    ! 
    allocate(&
         this%D1(ntdens),&
         this%D2(ntdens),&
         this%D3(ntdens),&
         this%invD2(ntdens),&
         this%invD2_D1(ntdens),&
         this%norm_rows_stiff(npot),&
         this%nrm_grad_dyn(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member'//&
         ' D1, D2, invD2, invD2_D1')
   
    this%scr_ntdens=one
    call this%assembly_stiffness_matrix(lun_err,this%scr_ntdens)
    call this%stiff%get_diagonal(this%diagonal_laplacian )

    ! 
    ! newton function
    ! 
    allocate(&
         this%fnewton_tdens(ntdens),&
         this%fnewton_gfvar(ntdens),&
         this%fnewton_pot(npot),&
         this%fnewton_tdens_old(ntdens),&
         this%fnewton_gfvar_old(ntdens),&
         this%fnewton_pot_old(npot),&
         this%rhs_reduced(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' type p1p0 member'//&
         ' fnewton_pot,  fnewton_tdens'//&
         ' fnewton_pot_old,  fnewton_tdens_old'//&
         ' rhs_reduced')


     !
    ! reduced jacobian A +deltat BT D2^{-1} D1 C 
    !
    !
    !  assembly system matrix 
    !     M = A + deltat + B^T invD2 D1 C ( PP ode)  
    !        or 
    !     M = A + deltat + B^T invD2 D1 B ( GF ode)
    select case(id_ode) 
    case (1)
       ! for PP ode
       ! M = A+deltat B^T invD2 D1 C
       call this%jacobian_reduced%init(&
            lun_err,&
            .false.,&
            this%stiff,&
            this%B_matrix,&
            this%C_matrix,&
            this%invD2_D1)

       ! for PP ode
       ! M = A+deltat B^T invD2 D1 C
       call this%stiff_gamma%init(&
            lun_err,&
            .false.,&
            this%stiff,&
            this%B_matrix,&
            this%C_matrix,&
            this%scr_ntdens)

      

    case (2)
       ! for GF ode
       ! M = A + 2*deltat B^T invD2 D1 B
       call this%jacobian_reduced%init(&
            lun_err,&
            .true.,&
            this%stiff,&
            this%B_matrix,&
            this%B_matrix,&
            this%invD2_D1)

        ! for PP ode
       ! M = A+deltat B^T invD2 D1 C
       call this%stiff_gamma%init(&
            lun_err,&
            .True.,&
            this%stiff,&
            this%B_matrix,&
            this%B_matrix,&
            this%scr_ntdens)
       
       !
       ! build sparsity pattern of matrix BTDC
       !
       call this%stiff_gamma_assembled%mult_MDN(lun_err,&
            this%BT_matrix,this%C_matrix,&
            100,100*this%npot,&
            this%invD2_D1)

       this%BT_gamma = this%BT_matrix 
    end select

    
    !
    ! augemented lagrangian approach
    !
    ! set jacobian block structure (only dimensions)
    !
    ! (A_gamma    B_gamma^T )
    ! (-dt D1 C   D2        )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff !_gamma_assembled 
    jacobian_directions(1) = 'N'

    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix !gamma
    jacobian_directions(2) = 'N'

    block_structure(:,3) = (/3,2,1/) ! -dt D1 B matrix in 2,1
    jacobian_list(3)%mat => this%deltatD1C_matrix
    jacobian_directions(3) = 'N'

    block_structure(:,4) = (/4,2,2/) ! D2       matrix in 2,2
    jacobian_list(4)%mat => this%D_matrix
    jacobian_directions(4) = 'N'

    call this%augmented_jacobian%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure,jacobian_directions, .false. ) 

    call this%triangular_augmented_prec%init(&
         lun_err,&
         this%stiff_gamma_assembled,&
         this%B_matrix)

     allocate (&
          this%diagonal_weight(ntdens),&
          this%inv_diagonal_weight(ntdens),&
          this%diagonal_scale(ntdens),&
          this%inv_diagonal_scale(ntdens),&
          this%hatS(ntdens),&
          this%MP_coeff(ntdens),&
          stat=res)
     if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
          ' member diagonal weight ')


    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'init scratch'

    !
    ! build sparsity pattern of matrix BTDC
    !
    call this%BTDC_matrix%mult_MDN(lun_err,&
         this%BT_matrix,this%C_matrix,&
         100,100*this%npot,&
         this%invD2_D1)

    !
    ! assembly redirector of stiffness  matrix into BTDC 
    !
    allocate(this%stiff2BTDC(this%p1%nterm_csr),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p0', &
         ' member stiff2BTDC ')
    call this%stiff%assembly_redirector(lun_err,&
         this%BTDC_matrix,this%stiff2BTDC)


        ! (A          B^T            )
    ! (B   -D2 (deltat D1)^-1 =G )
    !
    block_structure(:,1) = (/1,1,1/) ! A        matrix in 1,1
    jacobian_list(1)%mat => this%stiff 
    jacobian_directions(1) = 'N'
    
    block_structure(:,2) = (/2,1,2/) ! B^T      matrix in 1,2 
    jacobian_list(2)%mat => this%BT_matrix
    jacobian_directions(2) = 'N'
    
    block_structure(:,3) = (/3,2,1/) ! B matrix in 2,1
    jacobian_list(3)%mat => this%B_matrix
    jacobian_directions(3) = 'N'
    
    block_structure(:,4) = (/4,2,2/) ! D2 / ( -dt D1 )  matrix in 2,2
    jacobian_list(4)%mat => this%G_matrix
    jacobian_directions(4) = 'N'
    
    call this%sym_jacobian_full%init(&
         lun_err,&
         4, jacobian_list,&
         2 , 2, 4, block_structure, jacobian_directions, .True.)




    ! Predonditioner and back_up
    call this%spectral_info%init(lun_err,&
         ctrl%ctrl_tuning%nev,this%npot)


    call this%icprec_full%init(lun_err, this%B_matrix, &
         this%npot,&
         this%ntdens&
         )

    call this%mix_icprec_full%init(lun_err, this%B_matrix, &
         this%npot,&
         this%ntdens&
         )
    
    call this%triang_prec%init(lun_err, this%B_matrix, &
         this%npot,&
         this%ntdens&
         )
    
    !call this%der_pot_tdens_jacobian%init(lun_err, npot, ntdens)
    !call this%tdens_jacobian%init(lun_err, ntdens,ntdens)

    !
    ! bfgs 
    !
    call this%bfgs_prec%init(lun_err,npot)
    call this%matrix_V%init(lun_err, npot, ctrl%max_bfgs,&
         is_symmetric= .False.)
    call this%matrix_AV%init(lun_err, npot, ctrl%max_bfgs,&
        is_symmetric= .False.)
    call this%matrix_VTAV%init(lun_err, ctrl%max_bfgs, ctrl%max_bfgs,&
         is_symmetric= .False.)
    !call this%matrix_VTAV%info(6)
    ctrl_inverse%prec_type = 'ICHOL'
    call this%matrix_PI%init(lun_err,this%matrix_VTAV,ctrl_inverse)
    
    !
    ! component (2,2)
    !
    call this%inverse_D2%init(lun_err,ntdens)
       
    call this%inverse_diag_stiff%init(lun_err,npot)

    prec_block_structure(:,1) = (/1,1,1/)
    prec_list(1)%linop => this%inverse_diag_stiff

    prec_block_structure(:,2) = (/2,2,2/)
    prec_list(2)%linop=> this%inverse_D2

    
    call this%prec_full%init(lun_err, &
         2, prec_list,&
         2, 2, &
         2, prec_block_structure,.True.)
        
    
    !
    ! broyden update variables
    !
    this%nbroyden_update = ctrl%nbroyden
    allocate(this%broyden_updates(this%nbroyden_update),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         ' init_p1p0', &
         ' member updates' ) 
    ! nupdate+1 list of preconditioner
    allocate(this%broyden_sequence(0:this%nbroyden_update),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         ' init_p1p0', &
         ' member  broyden_sequence' )

    allocate(this%rankone_update(this%nbroyden_update),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, &
         ' init_p1p0', &
         ' member updates' ) 

!!$    call this%quasi_newton_update%init(lun_err, npot, npot, &
!!$         ctrl%max_iter_nonlinear)
   
    call this%scr_diagmat_ntdens%init(lun_err,ntdens)
    
    call this%approx_inverse%init(lun_err,npot)

    call this%inverse_stiff%init(lun_err,npot)

    
    ! projection and assembly
    this%stiff_laplacian = this%stiff
    this%tdens_prj  = one
    call this%p1%build_stiff(lun_err,&
         'csr', this%tdens_prj, this%stiff_laplacian)
    

    
    call this%assembly_stiffness_matrix(lun_err,this%scr_ntdens)
    
    !
    ! set approx inverse preconditioner
    !
    ctrl_prec%prec_type   = 'IC'
    ctrl_prec%n_fillin    = 50
    ctrl_prec%tol_fillin  = 1.0d-8
    call this%prec_laplacian%init(lun_err, info,&
         ctrl_prec, this%stiff_laplacian%nrow, this%stiff_laplacian)

  contains

      !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (itria) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_general(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)
    
    

    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
         ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'assembly_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       


       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do


    

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    
    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'assembly_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine assembly_B_matrix_general

  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_grid(lun_err,grid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: grid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(3,grid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, inode, j,k,ind,icol
    integer :: nnode, ncell, nnodeincell
    integer :: start, finish,nodes(3)
    integer , allocatable :: nnode_in_cell(:),ia(:),ja(:)


    ncell        = grid%ncell
    nnode        = grid%nnode
    nnodeincell  = grid%nnodeincell

    ! allocate work array 
    allocate(&
         nnode_in_cell(ncell),&
         ia(ncell+1),&
         ja(ncell*3),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'subnode_topol', &
         'alloc fail for temp array nnode_in_cell ia ja')


    call B_matrix%init(lun_err, &
         ncell, nnode, ncell*nnodeincell,&
         storage_system='csr',&
         is_symmetric =.false.)
    
    nnode_in_cell=0
    ia=0
    ja=0

    do icell=1,ncell+1
       ia(icell)=1+(icell-1)*nnodeincell
    end do

    do icell = 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       ja(start:finish) = grid%topol(1:nnodeincell,icell)
       call isort(nnodeincell,ja(start:finish))
    end do

    !
    ! assign ia,ja
    !      
    B_matrix%ia=ia
    B_matrix%ja=ja

    !
    ! assembly of trija
    !
    do icell= 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       nodes=grid%topol(1:nnodeincell,icell)
       do ind=start,finish
          icol=ja(ind)
          k=1
          do while ( icol .ne. nodes(k) )
             k=k+1
          end do
          trija(k,icell) = ind
       end do
    end do



    deallocate(nnode_in_cell,ia,ja,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'subnode_topol', &
         'dealloc fail for temp array nnode_in_cell ia ja')

  end subroutine assembly_B_matrix_grid


    
  end subroutine init_p1p0


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type p1p0_space_discretization)
  !>
  !> usage:
  !>     call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !<-----------------------------------------------------------    
  subroutine kill_p1p0(this,lun_err)     
    use Globals
    implicit none
    class(p1p0_space_discretization), intent(inout) :: this
    integer,                          intent(in   ) :: lun_err
    !local
    integer :: res
    logical :: rc

    this%grid_tdens => null()
    this%grid_pot   => null()
    
    !
    ! dimension reset
    !
    this%ntdens    = 0
    this%npot      = 0
    this%ngrad  = 0
    this%ambient_dimension = 0

    

    !
    ! construction of p1 element space
    !
    call this%p1%kill(lun_err)

    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    call this%stiff%kill(lun_err)
    call this%near_kernel%kill(lun_err)


    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    deallocate(&
         this%assembler_Bmatrix_subgrid,&
         this%assembler_Bmatrix_grid,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member assembler_Bmatrix_subgrid')

    !
    ! kill matrix_B and its transpose 
    !
    call this%B_matrix%kill(lun_err)
    call this%BT_matrix%kill(lun_err)
    deallocate (this%transposer,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member transposer')


    !
    ! kill matrix_C and its transpose 
    !
    call this%C_matrix%kill(lun_err)
    call this%CT_matrix%kill(lun_err)
    call this%deltatD1C_matrix%kill(lun_err)
    
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure  
    call this%aux_bicgstab%kill(lun_err)
    ! Auxiliary var for PCG procedure  
    call this%aux_newton%kill(lun_err)
    ! rhs scratch
    deallocate(&
         this%rhs,&
         this%rhs_ode,&
         this%inc_ode,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member rhs')

    ! 
    ! work array
    ! 
    deallocate(&
         this%tdens_prj,&
         this%scr_npot,&
         this%norm_rows_stiff,&
         this%norm_rows_BT,&
         this%scr_ntdens,&
         this%scr_ngrad,&
         this%scr_nfull,&
         this%sqrt_diag,&
         this%nrm_grad_dyn,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

    

    ! 
    ! work array
    ! 
    deallocate(&
         this%D1,&
         this%D2,&
         this%D3,&
         this%invD2,&
         this%invD2_D1,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member'//&
         ' D1, D2, invD2, invD2_D1')

    ! 
    ! newton function
    ! 
    deallocate(&
         this%fnewton_tdens,&
         this%fnewton_gfvar,&         
         this%fnewton_pot,&
         this%fnewton_tdens_old,&
         this%fnewton_gfvar_old,&         
         this%fnewton_pot_old,&
         this%rhs_reduced,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' type p1p0 member'//&
         ' fnewton_pot,  fnewton_tdens')


    !
    ! kill matrix BTDC
    !
    call this%BTDC_matrix%kill(lun_err)

    !
    ! assembly redirector of stiffness  matrix into BTDC 
    !
    deallocate(this%stiff2BTDC,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_p1p0', &
         ' member stiff2BTDC ')

    !
    ! broyden update variables
    !
    this%nbroyden_update = 0
    deallocate(this%broyden_updates,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         ' kill_p1p0', &
         ' work type broyden_updates' ) 
    deallocate(this%rankone_update,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         ' kill_p1p0', &
         ' work type broyden_updates' ) 
    deallocate(this%broyden_sequence,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, &
         ' kill_p1p0', &
         ' work type broyden_sequence' ) 

    !call this%quasi_newton_update%kill(lun_err)

    call this%scr_diagmat_ntdens%kill(lun_err)

    call this%approx_inverse%kill(lun_err)


  end subroutine kill_p1p0


  



    subroutine evaluate_functionals(this,tdpot,ode_inputs)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    type(tdpotsys),                   intent(inout) :: tdpot
    type(odedata),                    intent(in   ) :: ode_inputs
    !local
    real(kind=double) :: power,w1dist

    !----------------------------------------------------------------
    ! Real Valued Fucntional
    !---------------------------------------------------------------
    tdpot%mass_tdens          = this%mass(tdpot)
    tdpot%weighted_mass_tdens = this%w_mass(tdpot,ode_inputs)
    tdpot%energy              = this%energy(tdpot)
    tdpot%lyapunov            = this%lyap(tdpot,ode_inputs)
    tdpot%min_tdens           = minval(tdpot%tdens)
    tdpot%max_tdens           = maxval(tdpot%tdens)
    tdpot%max_velocity        = maxval(tdpot%tdens * tdpot%nrm_grad_avg)
    tdpot%max_nrm_grad         = maxval( tdpot%nrm_grad)
    tdpot%max_nrm_grad_avg    = maxval( tdpot%nrm_grad_avg )
    tdpot%max_D3= abs(maxval( this%D3 ))

    power = this%pvel_exponent(ode_inputs)

    tdpot%integral_flux_pvel  = this%integral_flux(tdpot,power) 
    tdpot%duality_gap         = this%duality_gap(tdpot,ode_inputs)
    
    if ( ode_inputs%opt_tdens_exists) then
       w1dist=this%grid_tdens%normp_cell(1.0d0,ode_inputs%opt_tdens(:,1))
       tdpot%err_tdens = &
            this%grid_tdens%normp_cell(1.0d0,tdpot%tdens-ode_inputs%opt_tdens(:,1))/&
            w1dist
       tdpot%err_wasserstein = &
            abs(tdpot%lyapunov - this%grid_tdens%normp_cell(1.0d0,ode_inputs%opt_tdens(:,1)))/ &
            w1dist
    end if
    tdpot%wasserstein_distance = this%grid_tdens%normp_cell(1.0d0,tdpot%tdens)

  end subroutine evaluate_functionals



  !>----------------------------------------------------------
  !> Precedure to compute 
  !> grads variables and grad-depending variables, given
  !> the spatial discretization and variable tdens and pot.
  !> ( public procedure for type p1p0)
  !> 
  !> usage: call var%build_nrm_grad_vars(tdpot,lun_err)
  !>
  !<------------------------------------------------------
  subroutine build_grad_vars(this,lun_err,tdpot)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    integer,                          intent(in   ) :: lun_err
    type(tdpotsys),                   intent(inout) :: tdpot
    !local
    integer :: i,j
    
    if ( .not. tdpot%tdpot_syncr )  then
       write(lun_err,*) 'Tdens and Pot are not syncronized'
       stop
    end if


    ! evulation of gradx, grady, gradz 
    call this%p1%eval_grad(tdpot%pot,tdpot%gradpot)

    ! eval nrm_grad   
    call this%p1%eval_nrm_grad(tdpot%pot,tdpot%nrm_grad)

    ! average on subgrid or copy 
    if (this%id_subgrid .eq. 1 ) then
       call this%grid_pot%avg_cell_subgrid(&
            this%grid_tdens,&
            tdpot%nrm_grad,&
            tdpot%nrm_grad_avg)
       do i=1,this%grid_pot%logical_dimension
          tdpot%scr_ngrad(:) = tdpot%gradpot(i,:)
          call this%grid_pot%avg_cell_subgrid(&
               this%grid_tdens,&
               tdpot%scr_ngrad,&
               tdpot%scr_ntdens)
          do j=1,tdpot%ntdens
             tdpot%gradpot_avg(i,j) = tdpot%scr_ntdens(j)
          end do
       end do

    else if (this%id_subgrid .eq. 0 ) then
       tdpot%nrm_grad_avg = tdpot%nrm_grad
       tdpot%gradpot_avg  = tdpot%gradpot
    end if

    tdpot%all_syncr = .true.

  end subroutine build_grad_vars


  !>------------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !<-------------------------------------------------------------
  subroutine assembly_rhs_ode(this,&
       ntime,&
       ode_inputs,&
       tdpot,&
       rhs_ode)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    integer,           intent(in   ) :: ntime
    type(OdeData),     intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 
    !IMPORTANT rhs_ode is already scaled by the mass matrix

    !local
    integer :: icell,j
    integer :: ntdens
    real(kind=double) :: min,max, pflux, pmass,decay
    real(kind=double) :: ptrans, gf_pflux, gf_pmass
    real(kind=double) :: penalty_factor

    ntdens = this%ntdens


    rhs_ode = zero

    pflux = ode_inputs%pflux(ntime)
    decay = ode_inputs%decay(ntime)
    pmass = ode_inputs%pmass(ntime)

    if ( ( ode_inputs%id_ode .eq. 1 ) .or.  &
         ( ode_inputs%id_ode .eq. 2 ) ) then

       do icell = 1,ntdens
          rhs_ode(icell) = rhs_ode(icell) + &
               tdpot%tdens(icell) ** pflux * this%nrm_grad_dyn(icell) 
       end do

       do icell = 1,ntdens     
          rhs_ode(icell) = rhs_ode(icell) - &
               ode_inputs%kappa(icell,ntime) * decay * &
               tdpot%tdens(icell) ** pmass 
       end do

       !
       ! penalization
       !
       penalty_factor = ode_inputs%penalty_factor(ntime)
       if ( abs( penalty_factor ) .gt. small) then
          do icell = 1,ntdens
             rhs_ode(icell) = rhs_ode(icell) - &
                  penalty_factor * &
                  ode_inputs%penalty_weight(icell,ntime) * &
                  ( tdpot%tdens(icell) - &
                  ode_inputs%penalty(icell,ntime) * &
                  tdpot%tdens(icell)**pflux ) 
          end do
       end if
    else
       ptrans = 2/ (2-pflux) 
       gf_pflux = pflux / (2 - pflux) 
       gf_pmass = pmass * ptrans - ptrans + 1  
       
       if( abs(pflux-one) > 1e-12) then
          write(*,*) 'only pflux=1, in assembly rhs_ode'
          stop
       end if
       
       !
       ! increasing part \Gfvar ^{p1} |\Grad \Pot|^2
       !
       do icell = 1,this%ntdens
          rhs_ode(icell) = rhs_ode(icell) + &
                gf_pflux * tdpot%gfvar(icell) ** gf_pflux * this%nrm_grad_dyn(icell) 
       end do

       !
       ! decay part \Gfvar ^{p2} |\Grad \Pot|^2
       !
       do icell = 1,this%ntdens
          rhs_ode(icell) = rhs_ode(icell) - &
               ode_inputs%kappa(icell,ntime) * decay *&
               gf_pflux* tdpot%gfvar(icell) ** gf_pmass 
       end do

       !
       ! penalization 
       !
       penalty_factor = ode_inputs%penalty_factor(ntime)
       if ( abs( penalty_factor) .gt. small) then
          do icell = 1,this%ntdens
             rhs_ode(icell) = &
                  rhs_ode(icell) - &
                  penalty_factor*ode_inputs%penalty_weight(icell,ntime) * &
                  ( tdpot%gfvar(icell) - ode_inputs%penalty(icell,ntime) ) 
          end do
       end if
    end if


  end subroutine assembly_rhs_ode

  !>------------------------------------------------------------
  !> Assembly the rhs of the tdens ode of gfvar ode
  !<-------------------------------------------------------------
  subroutine get_increment_ode(this, rhs_ode,inc_ode)
    implicit none
    class(p1p0_space_discretization), intent(in   ) :: this
    real(kind=double),                intent(in   ) :: rhs_ode(this%ntdens)
    real(kind=double),                intent(inout) :: inc_ode(this%ntdens)

    inc_ode = rhs_ode

  end subroutine get_increment_ode
  

  



  
  subroutine invert_jacobian(p1p0, &
       iter_newton,&
       tdpot,&       
       lun_err,lun_out,lun_stat, &
       ctrl, &
       current_time_iteration,current_time,deltat, &
       info,&
       CPU,&
       ode_inputs,&
       rhs_full, inc_full)
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use RankOneUpdate
    use ScalableMatrix

    implicit none
    class(p1p0_space_discretization), target, intent(inout) :: p1p0
    integer,           intent(in   ) :: iter_newton
    type(tdpotsys),    target,  intent(inout) :: tdpot
    integer,           intent(in   ) :: lun_err,lun_out,lun_stat
    type(CtrlPrm),     intent(inout) :: ctrl
    integer,           intent(in   ) :: current_time_iteration
    real(kind=double), intent(in   ) :: current_time
    real(kind=double), intent(inout) :: deltat
    integer,           intent(inout) :: info
    type(codeTim),     intent(inout) :: CPU    
    type(odedata),     intent(in   ) :: ode_inputs
    real(kind=double), intent(inout) :: rhs_full(p1p0%nfull)
    real(kind=double), intent(inout) :: inc_full(p1p0%nfull)
    ! local
    logical :: rc
    integer :: res
    integer :: i,j,ind
    integer :: info_prec
    integer :: ntdens, npot, nfull
    
    ! inversion cycle 
    logical :: try2invert

    type(input_solver) :: ctrl_uzawa

    ! broyden updates quantities
    integer :: iter_broyden
    integer :: info_broyden
  
    type(file) :: fmat
    character(len=256) :: fname,directory,tail
    type(spmat),   pointer :: matrix2prec
    ! prec vars
    class(abs_linop), pointer :: prec_E
    class(abs_linop), pointer :: prec_Schur
    class(abs_linop), pointer :: prec_final 
    !
    real(kind=double) :: dnrm2,esnorm
    real(kind=double) :: max,rmax
    integer :: imax

    character(len=70) :: str
    character(len=256) :: msg,msg1,msg2,msg3,msg4

    !
    ! set no error flag
    !
    info = 0
    tdpot%nrestart_invert_jacobian = 0
    
    
    !
    ! copy dimensions
    !
    ntdens = tdpot%ntdens
    npot   = tdpot%npot
    nfull  = tdpot%nfull

    !
    ! solve linear system J_F(k) = -F(k)
    ! We use a cycle to try diffrent approaches if required.
    ! Use subroutine handle_failure_invert_jacobian
    ! switch to .True. . Otherwise is set .False.
    !
    try2invert = .True.
    !
    do while ( try2invert  .and. &
         (tdpot%nrestart_invert_jacobian .le. &
         ctrl%max_nrestart_invert_jacobian ) )
       try2invert = .False.
       !
       ! select linear solver strategy 
       ! - reduced or full
       ! - preconditioner to be used
       !
       ! Used only at first attempt, use 
       ! set controls in subroutine 
       ! handle_failure_invert_jacobian
       !
       if ( tdpot%nrestart_invert_jacobian .eq. 0) then
          !
          ! selcet method and preconditioner
          !
          call select_inversion_approach(lun_err,&
               p1p0,&
               ctrl,info)
          if (info .ne. 0) return
          !
          ! set flag ctrl%build_prec
          !
          call select_preconditioner_building(&
               tdpot,&
               iter_newton,&
               ctrl)
       end if


       ! info state before linear system solution
       !
       if ( ctrl%info_newton .eq. 2 ) then
          !
          ! Print information on linear solver strategy
          !
          if ( tdpot%nrestart_invert_jacobian .gt. 0) write(lun_out,*) ctrl%sep('RESTARTED')
          select case (ctrl%reduced_jacobian)
             case (1)
                write(msg3,*) 'REDUCED'
             case (0)
                write(msg3,*) 'FULL '
             end select

             select case (ctrl%prec_newton)
             case (1)
                write(msg4,*) 'ICHOL A'
             case (2)
                write(msg4,*) 'ICHOL (A\D2) '
             case (3)
                write(msg4,*) 'MIXED '
             case (4)
                write(msg4,*) 'TRIANG '
             end select
                
             write(msg1,'(I2,a,a,1x,I2,1x,a,1x,a,1x,a,1x,a,1x,a,1x,I2,a,1x,I2,a,1x,e8.2)') &
                  iter_newton+1,' | CTRL  : ','build prec=',ctrl%build_prec,&
                  'LIN. SYS. :',etb(msg3),&
                  'PREC =',etb(msg4),&
                  'diag. scale =',ctrl%id_diagscale,&
                  'nbfgs =',ctrl%max_bfgs,&
                  'tol=',ctrl%ctrl_solver%tol_sol
             write(lun_out,*) etb(msg1)
             write(lun_stat,*) etb(msg1)
       end if

       !
       ! used the selected method to solve 
       !     J_F inc = - F = rhs
       !
       
       select case ( ctrl%reduced_jacobian ) 
          !
          ! solve reduce 
          !
       case (1) ! solve_reduced_jacobian
          !
          ! Prepare linear system
          !  assembly system matrix 
          !     M = A + deltat * B^T D2^{-1} D1 C ( PP ode)  
          !        or 
          !     M = A + deltat * B^T D2^{-1} D1 B ( GF ode)
          select case(ode_inputs%id_ode) 
          case (1)
             ! for PP ode
             ! M = A+deltat B^T invD2 D1 C
             call p1p0%jacobian_reduced%set(&
                  lun_err,&
                  .false.,&
                  one,&
                  deltat,&
                  p1p0%stiff,&
                  p1p0%B_matrix,&
                  p1p0%C_matrix,&
                  p1p0%invD2_D1)
          case (2)
             ! for GF ode
             ! M = A + 2*deltat B^T invD2 D1 B
             call p1p0%jacobian_reduced%set(&
                  lun_err,&
                  .true.,&
                  one,&
                  deltat,&
                  p1p0%stiff,&
                  p1p0%B_matrix,&
                  p1p0%B_matrix,&
                  p1p0%invD2_D1)  
          end select

          !
          ! assembly rhs of the reduced system 
          !
          call assembly_rhs_reduced(&
               p1p0%B_matrix,p1p0%invD2,&
               p1p0%fnewton_tdens,p1p0%fnewton_pot,p1p0%scr_npot,&
               p1p0%rhs_reduced)


          !
          ! solve linear system
          !

          !
          ! 1-cope singularity of matrix M
          !
          if ( ode_inputs%ndir .eq. 0 ) then
             call ortogonalize(&
                  p1p0%jacobian_reduced%ncol,&
                  1,&
                  p1p0%kernel_full(1:npot,1),inc_full(1:npot))
          end if


          !
          ! assembly matrix A+deltat*BTDC
          ! passagge required only for building preconditioner
          !
          if ( (ctrl%build_prec  .ne. 0) .and. &
               (ctrl%prec_newton .eq. 2) ) then 
             call CPU%PREC%set('start')

             !
             ! build A + deltat BT D1 D2^{-1} C              
             ! 
             tdpot%scr_ntdens=deltat * p1p0%invD2_D1
             call p1p0%BTDC_matrix%mult_MDN(lun_err,&
                  p1p0%BT_matrix,p1p0%C_matrix,&
                  100,100*tdpot%npot,&
                  tdpot%scr_ntdens,p1p0%CT_matrix)

             p1p0%BTDC_matrix%is_symmetric = p1p0%BequalC

             !
             ! build A + deltat BT D1 D2^{-1} C              
             ! 
             do i=1,p1p0%stiff%nrow
                do j=p1p0%stiff%ia(i),p1p0%stiff%ia(i+1)-1
                   p1p0%BTDC_matrix%coeff(p1p0%stiff2BTDC(j)) = &
                        p1p0%BTDC_matrix%coeff(p1p0%stiff2BTDC(j)) +&
                        p1p0%stiff%coeff(j)
                end do
             end do

             if ( tdpot%ntdens_on .ne. tdpot%ntdens ) then
                ! freeze potential
                do i=1,tdpot%npot_off
                   call p1p0%BTDC_matrix%set_rowcol(tdpot%inactive_pot(i),zero)
                   p1p0%BTDC_matrix%coeff(p1p0%BTDC_matrix%idiag(tdpot%inactive_pot(i)))=one
                end do
             end if


             call CPU%PREC%set('stop')
             if ( ctrl%id_save_matrix > 1 ) then
                write(tail,'(I0.4,a,I0.4,a)') &
                     tdpot%time_iteration,'_',iter_newton, '.dat' 
                directory=etb('./output/linsys')
                fname=etb(etb(directory)//'/apbtdc_matrix_'//etb(tail))
                call fmat%init(lun_err,fname,10000,'out')
                call p1p0%BTDC_matrix%write(fmat%lun,'matlab')
                call fmat%kill(lun_err) 
             end if

          end if

          !
          ! star solving linear system J s = rhs
          !
          
          !
          !
          ! set to zero initial increments
          !
          inc_full=zero

          !
          ! scale linear system
          !
          if ( ctrl%id_diagscale .eq. 1 ) then
             ! D=Diag(M) temporary stored in inv_sqrt_diagonal
             call p1p0%jacobian_reduced%get_diagonal(p1p0%sqrt_diag)

             ! D=Diag(M)^{-1/2}
             p1p0%sqrt_diag=one/sqrt(p1p0%sqrt_diag)

             !
             ! scale by D system M x = b
             !
             call scale_system(lun_err, &
                  p1p0%jacobian_reduced ,p1p0%rhs_reduced,inc_full(1:npot),&
                  p1p0%sqrt_diag)

             !
             ! scale matrix use to build prec
             !
             if ( ( ctrl%build_prec  .ge. 1 )  .and. &
                  ( ctrl%prec_newton .eq. 2 ) ) then  
                call CPU%PREC%set('start')
                call p1p0%BTDC_matrix%diagonal_scale(lun_err,  &
                     p1p0%sqrt_diag)
                call CPU%PREC%set('stop')
             end if
          end if

          select case (ctrl%prec_newton)
          case ( 1 )
             matrix2prec => p1p0%stiff
          case ( 2 )
             matrix2prec => p1p0%BTDC_matrix
          case default
             rc=IOerr(lun_err, err_val, 'invert_jacobian', &
                  ' control not defined for reduced jacobian'//&
                  ' ctrl%prec_newton = ', ctrl%prec_newton)
          end select


          call CPU%PREC%set('start')
          call  assembly_stdprec(&
               lun_err,&
               matrix2prec,&
               ctrl%build_prec,&
               ctrl%ctrl_prec,&
               tdpot%info_prec,&
               p1p0%standard_prec_saved)
          call CPU%PREC%set('stop')

          if  ( tdpot%info_prec .ne. 0 ) then
             info=-2000
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' prec. construstion failed ', tdpot%info_prec)
          end if
          matrix2prec => null()

          prec_final => p1p0%standard_prec_saved
          if ( ctrl%max_bfgs > 0) then
             if ( tdpot%total_number_linear_system > ctrl%max_bfgs) then
                write(*,*) 'total lin. sys. =,',tdpot%total_number_linear_system
                !
                ! only at initial iteration compute stiff * V
                !
                if ( iter_newton .eq. 0 ) then
                   do i=1,ctrl%max_bfgs 
                      call p1p0%stiff%Mxv(p1p0%matrix_V%coeff(:,i), p1p0%matrix_AV%coeff(:,i))
                   end do
                end if
                call p1p0%bfgs_prec%set(&
                     p1p0%matrix_V,&
                     p1p0%matrix_PI,&
                     p1p0%matrix_AV,&
                     p1p0%standard_prec_saved)
                prec_final => p1p0%bfgs_prec
             else
                prec_final => p1p0%standard_prec_saved
             end if
          end if
          

          !
          ! ortogonalized increment
          !
          call CPU%LINEAR_SOLVER%set('start')
          call linear_solver(p1p0%jacobian_reduced,&
               p1p0%rhs_reduced,inc_full(1:npot),&
               tdpot%info_solver, &
               ctrl%ctrl_solver,&
               prec_left=prec_final,&
               aux=p1p0%aux_bicgstab,&
               ortogonalization_matrix=p1p0%near_kernel)
          call CPU%LINEAR_SOLVER%set('stop')


          if ( tdpot%info_solver%ierr .ne. 0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_err)
             rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_stat)
             info=-1
          end if

           

          !
          ! scale back 
          !
          if ( ctrl%id_diagscale .eq. 1  ) then
             !
             p1p0%sqrt_diag = one / p1p0%sqrt_diag

             call scale_system(lun_err, &
                  p1p0%jacobian_reduced ,p1p0%rhs_reduced,inc_full(1:npot),&
                  p1p0%sqrt_diag)
          end if

          !
          ! 3 - get inc_tdens from inc_pot
          !
          select case(ode_inputs%id_ode) 
          case (1)
             call get_inc_tdens(p1p0%C_matrix,deltat,&
                  p1p0%D1,p1p0%invD2,inc_full(1:npot),&
                  p1p0%fnewton_tdens,p1p0%scr_ntdens,inc_full(npot+1:nfull))
          case (2)
             call get_inc_tdens(p1p0%B_matrix,deltat,&
                  p1p0%D1,p1p0%invD2, inc_full(1:npot),&
                  p1p0%fnewton_tdens,p1p0%scr_ntdens,inc_full(npot+1:nfull))
          end select

       case  (0) ! solve_full_jacobian
          if ( ctrl%debug .eq. 1 ) &
               write( lun_out, *) ' Full jacobian'

          if ( ctrl%prec_newton .eq. 1) then
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) &
                  ' Full jacobian + Block prec diag(A^-1, D2^1)'

             !
             ! Build_preconditioner
             !
             call CPU%PREC%set('start')
             if ( p1p0%nbroyden_update .eq. 0) then
                !
                ! No rank-one-update
                !

                !
                ! update component (1,1) of block prec.
                !
                if ( ctrl%debug .eq. 1 ) & 
                     write(lun_out,*) ' Assembly prec stiff',ctrl%build_prec
                call assembly_stdprec(lun_err,&
                     p1p0%stiff,&
                     ctrl%build_prec,&
                     ctrl%ctrl_prec,&
                     info_prec,&
                     p1p0%standard_prec_saved)
                if ( info_prec .ne. 0) then
                   rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                        'prec. stiff construstion failed'//&
                        'info prec. = ',info_prec)
                   info = -2000
                end if

                !
                ! update component (2,2) of block prec.
                !
                if ( ctrl%debug .eq. 1 ) & 
                     write(lun_out,*) ' Assembly prec inv_D2'
                p1p0%scr_ntdens = one/p1p0%D2
                call p1p0%inverse_D2%set(lun_err,p1p0%scr_ntdens)

                !
                ! re-assign prec_full component
                !
                p1p0%prec_full%linops(1)%linop => p1p0%standard_prec_saved
                p1p0%prec_full%linops(2)%linop => p1p0%inverse_D2


                !
                ! redirect preconditoner used
                !
                prec_final => p1p0%prec_full
             else
                !
                ! Broyden updates techinque
                !
                !
                if ( iter_newton .eq. 0) then
                   !
                   ! assembly P0
                   !
                   !
                   ! update component (1,1) of block prec.
                   !
                   !if ( ctrl%debug .eq. 1 ) & 
                   write(lun_out,*) &
                        ' Assembly prec stiff for prec_broyden(0)' 
                   call  assembly_stdprec(lun_err,&
                        p1p0%stiff,&
                        ctrl%build_prec,&
                        ctrl%ctrl_prec,&
                        info_prec,&
                        p1p0%standard_prec_saved)
                   if ( info .ne. 0) then
                      info=-2000
                      rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                           ' prec. stiff construstion failed'//&
                           'info prec. = ',info_prec)
                   end if


                   !
                   ! update component (2,2) of block prec.
                   !
                   if ( ctrl%debug .eq. 1 ) & 
                        write(lun_out,*) &
                        ' Assembly prec inv_D2 for prec_broyden(0)'
                    p1p0%scr_ntdens = one
                    call p1p0%inverse_D2%set(lun_err,p1p0%scr_ntdens)!/p1p0%D2)


                   !
                   ! assign final prec.
                   !
                   p1p0%broyden_sequence(0)%linop => p1p0%prec_full
                   prec_final => p1p0%broyden_sequence(0)%linop
                end if
                !
                ! P_{iter} = P_{iter-1} + u_k w_k ^t
                !
                if ( (iter_newton .ge. 1) .and. &
                     (iter_newton .le. p1p0%nbroyden_update) ) then
                   !
                   ! store y_k &
                   ! ( can be removed we can pass y_k init update%set 
                   !
                   p1p0%scr_nfull(:) = &
                        (/p1p0%fnewton_pot,    p1p0%fnewton_tdens    /) - &
                        (/p1p0%fnewton_pot_old,p1p0%fnewton_tdens_old/)
                   !
                   !
                   !
                   if ( ctrl%debug .eq. 1 ) & 
                        write(lun_out,*) &
                        ' Assembly prec_broyden(',iter_newton,')'
                   call p1p0%broyden_updates(iter_newton)%set(lun_err,&
                        p1p0%broyden_sequence(iter_broyden)%linop,&! prec to update
                        inc_full,p1p0%scr_nfull,info_broyden)
                   if ( info .eq. 0) then
                      ! broyden update is formed properly
                      iter_broyden = iter_broyden + 1 
                      p1p0%broyden_sequence(iter_broyden)%linop => &
                           p1p0%broyden_updates(iter_newton)
                   else
                      rc = IOerr(lun_err, wrn_out, 'invert_jacobian', &
                           ' construstion broyden update failed'//&
                           ' info prec. = ',info_broyden)
                   end if
                   !
                   ! assign final prec. as the greater broyden update
                   !
                   prec_final => p1p0%broyden_sequence(iter_broyden)%linop
                end if

                if ( iter_newton .gt. p1p0%nbroyden_update) then
                   !
                   ! P_{k} = last rank-one-update
                   !
                   prec_final => p1p0%broyden_sequence(iter_broyden)%linop
                end if
             end if
             call CPU%PREC%set('stop')

             !
             ! reassing jacobian matrix
             !
             call p1p0%D_matrix%set(lun_err,p1p0%D2)
             p1p0%jacobian_full%mats(1)%mat => p1p0%stiff 
             p1p0%jacobian_full%mats(2)%mat => p1p0%BT_matrix
             p1p0%jacobian_full%mats(3)%mat => p1p0%deltatD1C_matrix !is minus
             p1p0%jacobian_full%mats(4)%mat => p1p0%D_matrix

             
             !
             ! 2-solve linear system M inc_pot = rhs_reduced
             !
             call CPU%LINEAR_SOLVER%set('start')
             call linear_solver(&
                  p1p0%jacobian_full,rhs_full,inc_full,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,& 
                  prec_final,&
                  aux=p1p0%aux_bicgstab,&
                  ortogonalization_matrix=p1p0%near_kernel_full)
             call CPU%LINEAR_SOLVER%set('stop')

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if

          if ( ctrl%prec_newton .eq. 2) then
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' Full jacobian symmetrized'
             ! 
             ! solving (A B^T) = (I                    ) ( A            BT )
             !         (B G  )   (   (-deltat D1)^{-1} ) ( -deltatD1B   D2 )


             !
             ! Update G_matrix with new - D2/( deltat D1)
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' G_matrix'
             p1p0%symmetrizer=-deltat*p1p0%D1
             call p1p0%G_matrix%set(lun_err, p1p0%D2/p1p0%symmetrizer )

             !
             ! apply symmetrizer^{-1} to rhs(npot+1:nfull)
             !
             rhs_full(npot+1:nfull) = rhs_full(npot+1:nfull) &
                  / p1p0%symmetrizer(:) 


             ! EFAMLB
             ! build prec_E
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_E'
             !
             ! update component (1,1) of block prec.
             !
             if ( ctrl%debug .eq. 1 ) & 
                  write(lun_out,*) ' Assembly prec stiff'
             call assembly_stdprec(lun_err,&
                  p1p0%stiff,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
             prec_E => p1p0%standard_prec_saved

             if ( info_prec .ne. 0) then
                info = -2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     '  prec. stiff  failed '//&
                     'info prec. = ',info_prec)
             end if


             !
             ! build shur complement matrix B E^-1 BT + G
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_Schur | assembler BDBT'
             !
             ! assembly matrix B diag(A)^{-1} BT
             !
             call p1p0%stiff%get_diagonal(tdpot%scr_npot) 
             tdpot%scr_npot= one / tdpot%scr_npot
             call p1p0%BDBT%MULT_MDN(lun_err, p1p0%B_matrix , p1p0%BT_matrix, &
                  ntdens, 100*ntdens ,tdpot%scr_npot, p1p0%B_matrix )


             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_Schur | assembler BDBT+G'
             do i = 1, tdpot%ntdens
                ind=p1p0%BDBT%idiag(i) 
                !if ( G_matrix%diagonal(i) < one ) 
                p1p0%BDBT%coeff(ind) = p1p0%BDBT%coeff(ind) - p1p0%G_matrix%diagonal(i)
             end do

             !
             ! build_prec_shur
             !
             str=ctrl%ctrl_prec%prec_type
             ctrl%ctrl_prec%prec_type = 'IC'
             call tdpot%assembly_stdprec(lun_err,&
                  p1p0%BDBT,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%prec_schur)  
             ctrl%ctrl_prec%prec_type=str
             if ( info_prec .ne. 0) then
                info=-2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     ' prec. Schur  failed'//&
                     'info prec. = ',info_prec)
             end if

             prec_Schur =>  p1p0%prec_schur

             !
             ! set preconditioner
             !
             call p1p0%icprec_full%set(lun_err, p1p0%B_matrix, prec_E, prec_Schur)
             prec_final => p1p0%icprec_full

             !
             ! solve linear system sym_J s = rhs_sym
             !       

             !
             ! set to zero initial increments
             !
             inc_full=zero


             !
             ! 2-solve linear system M inc_pot = rhs_reduced
             !
             call CPU%LINEAR_SOLVER%set('start')
             call linear_solver(&
                  p1p0%sym_jacobian_full,rhs_full,inc_full,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,&
                  prec_final,&
                  aux=p1p0%aux_bicgstab,&
                  ortogonalization_matrix=p1p0%near_kernel_full)
             call CPU%LINEAR_SOLVER%set('stop')

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if

          if ( ( ctrl%prec_newton .eq. 3) .or. &
               ( ctrl%prec_newton .eq. 4) )then 
             !
             ! solve mixed_constranied_prec or triangular
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' Full jacobian mixed constrained preconditioner'
             ! 
             ! solving (A               B^T)
             !         (-deltat D1 B    D2 ) 
             !                     =
             !                     C
             call p1p0%D_matrix%set(lun_err,p1p0%D2)
             
             !
             ! reassing jacobian matrix
             !
             p1p0%jacobian_full%mats(1)%mat => p1p0%stiff 
             p1p0%jacobian_full%mats(2)%mat => p1p0%BT_matrix
             p1p0%jacobian_full%mats(3)%mat => p1p0%deltatD1C_matrix
             p1p0%jacobian_full%mats(4)%mat => p1p0%D_matrix



             ! EFAMLB
             ! build prec_E
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_E'

             !
             ! update component (1,1) of block prec.
             !
             if ( ctrl%debug .eq. 1 ) & 
                  write(lun_out,*) ' Assembly prec stiff'

             p1p0%stiff_laplacian = p1p0%stiff
!!$             rmax=0.0d0
!!$             imax=1
!!$             do i=1,npot
!!$                j=p1p0%stiff_laplacian%idiag(i)
!!$                if ( rmax < ode_inputs%rhs_integrated(i,1) ) then
!!$                   rmax = ode_inputs%rhs_integrated(i,1) 
!!$                   imax = i
!!$                end if
!!$             end do
!!$             j=p1p0%stiff_laplacian%idiag(imax)
!!$             call p1p0%stiff_laplacian%set_rowcol(imax,zero)
!!$             p1p0%stiff_laplacian%coeff(j)=huge

!!$             rmax=0.0d0
!!$             imax=1
!!$             do i=1,npot
!!$                j=p1p0%stiff_laplacian%idiag(i)
!!$                if ( p1p0%stiff_laplacian%coeff(j)> rmax ) then
!!$                   rmax = p1p0%stiff_laplacian%coeff(j)
!!$                   imax = i
!!$                end if
!!$             end do
!!$             write(*,*) 'lift for prec ',imax, p1p0%grid_pot%coord(:,imax), rmax
!!$             do i=1,npot
!!$                j=p1p0%stiff_laplacian%idiag(i)
!!$                p1p0%stiff_laplacian%coeff(j) = p1p0%stiff_laplacian%coeff(j)  + ctrl%relax2prec * rmax
!!$             end do


             call assembly_stdprec(lun_err,&
                  p1p0%stiff_laplacian,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
             prec_E => p1p0%standard_prec_saved

             if ( info_prec .ne. 0) then
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     '  prec. stiff  failed '//&
                     'info prec. = ',info_prec)
                info=-2000
             end if


             !
             ! build shur complement matrix  delat D1 C diag(A)^{-1} BT + D2
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) & 
                  'prec_Schur | assembler D2 + deltat D1Cdiag(A)^{-1} BT'

             !
             ! assembly matrix C diag(A)^{-1} BT
             !
             call p1p0%stiff%get_diagonal(tdpot%scr_npot) 
             tdpot%scr_npot= one / tdpot%scr_npot
             call p1p0%BDBT%MULT_MDN(lun_err, p1p0%B_matrix , p1p0%BT_matrix, &
                  ntdens, 100*ntdens ,tdpot%scr_npot, p1p0%B_matrix )


             !
             ! assembly matrix deltat D1 C diag(A)^{-1} BT + D2
             !
             tdpot%scr_ntdens=deltat*p1p0%D1
             call p1p0%BDBT%DxM( lun_err, tdpot%scr_ntdens)

             do i = 1, tdpot%ntdens
                ind=p1p0%BDBT%idiag(i) 
                p1p0%BDBT%coeff(ind) = p1p0%BDBT%coeff(ind) + p1p0%D2(i)
             end do

             if ( ctrl%id_save_matrix > 1 ) then
                write(tail,'(I0.4,a,I0.4,a)') &
                     tdpot%time_iteration,'_',iter_newton, '.dat' 
                directory=etb('./output/linsys')
                fname=etb(etb(directory)//'/bdbt_matrix_'//etb(tail))
                call fmat%init(lun_err,fname,10000,'out')
                call p1p0%BDBT%write(fmat%lun,'matlab')
                call fmat%kill(lun_err) 
             end if


             !
             ! build_prec_shur
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_Schur | assembler deltaD1 * B P(A)^-1 BT+D2 '

             str=ctrl%ctrl_prec%prec_type
             ctrl%ctrl_prec%prec_type = 'ILU'
             call tdpot%assembly_stdprec(lun_err,&
                  p1p0%BDBT,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%prec_schur)  
             ctrl%ctrl_prec%prec_type=str
             !call p1p0%prec_schur%info(6)

             if ( info_prec .ne. 0) then
                info=-2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     ' prec. Schur  failed'//&
                     'info prec. = ',info_prec)
             end if

             prec_Schur =>  p1p0%prec_schur

             !
             ! set preconditioner
             !
             tdpot%scr_ntdens = deltat*p1p0%D1
             select case (ctrl%prec_newton)
             case (4)
                call p1p0%triang_prec%set(lun_err,&
                     p1p0%B_matrix, prec_E, prec_Schur, tdpot%scr_ntdens)
                prec_final => p1p0%triang_prec
             case (3)                   
                call p1p0%mix_icprec_full%set(lun_err,&
                     p1p0%B_matrix, prec_E, prec_Schur, tdpot%scr_ntdens)
                prec_final => p1p0%mix_icprec_full
             case default
                rc=IOerr(lun_err, err_val, 'invert_jacobian', &
                     ' control not defined for full jacobian'//&
                     ' ctrl%prec_newton = ', ctrl%prec_newton)
             end select

             
             !write(*,*) associated(prec_Schur,p1p0%prec_schur)
             !write(*,*) associated(prec_Schur,p1p0%mix_icprec_full%prec_Schur)


             !----------------------------------------------------------
             !
             ! 2-solve linear system J inc_full = -F = rhs
             !
             !inc_full  = zero
             !inc_pot   = zero
             !inc_tdens = zero

             !
             ! only bicgstab this configuration
             !
             !ctrl%ctrl_solver%scheme = 'BICGSTAB'
             call CPU%LINEAR_SOLVER%set('start')
             call linear_solver(&
                  p1p0%jacobian_full,rhs_full,inc_full,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,&
                  prec_left=prec_final,&
                  aux=p1p0%aux_bicgstab,&
                  ortogonalization_matrix=p1p0%near_kernel_full)
             call CPU%LINEAR_SOLVER%set('stop')

             !ctrl%ctrl_solver%scheme = ctrl%ctrl_solver_original%scheme

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if
       case (3) ! solve_inexact_jacobian
          call assembly_stdprec(lun_err,&
                  p1p0%stiff,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
          call CPU%LINEAR_SOLVER%set('start')
          
          p1p0%scr_npot = ode_inputs%rhs_integrated(:,2) ! copy rhs 
          inc_full(1:npot) = tdpot%pot     
          call linear_solver(&
               p1p0%stiff,p1p0%scr_npot,tdpot%pot,&
               tdpot%info_solver,&
               ctrl%ctrl_solver,&
               prec_left=p1p0%standard_prec_saved,&
               aux=p1p0%aux_bicgstab)          
          inc_full(1:npot) =  tdpot%pot - inc_full(1:npot)

          call tdpot%build_nrm_grad_vars(p1p0,lun_err)
          call p1p0%build_nrm_grad_dyn(tdpot,2.0d0)
          
 
          call CPU%LINEAR_SOLVER%set('stop')
          if ( ctrl%nbroyden > 0) then
             if (iter_newton .eq. 0) then
                call p1p0%scr_diagmat_ntdens%set( &
                     lun_err,one / one + deltat*( p1p0%nrm_grad_dyn + one ) )
                call p1p0%scr_diagmat_ntdens%Mxv(&
                     - p1p0%fnewton_tdens , inc_full(1+npot:nfull) )
             else
                p1p0%scr_ntdens = p1p0%fnewton_tdens - p1p0%fnewton_tdens_old
                esnorm=one/dnrm2(ntdens,inc_full(1+npot:nfull),1)**2
                if ( iter_newton .eq. 1 ) then
                   call p1p0%rankone_update(iter_newton)%set(lun_err,&
                        p1p0%scr_diagmat_ntdens,&! prec to update
                        p1p0%fnewton_tdens, &
                        inc_full(1+npot:nfull),.False., &
                        info_broyden,esnorm)
                else
                   call p1p0%rankone_update(iter_newton)%set(lun_err,&
                        p1p0%rankone_update(iter_newton-1),&! prec to update
                        p1p0%fnewton_tdens, &
                        inc_full(1+npot:nfull),.False.,  info_broyden,&
                        esnorm)
                end if

                call p1p0%rankone_update(iter_newton)%Mxv(&
                     - p1p0%fnewton_tdens , inc_full(1+npot:nfull) )
             end if
          else
             do i=1,ntdens
                p1p0%scr_ntdens(i) =  &
                     one / &
                     (one/p1p0%grid_tdens%size_cell(i)  &
                     - deltat * ( p1p0%nrm_grad_dyn(i) + one)   &
                     + 2*deltat * tdpot%tdens(i)/p1p0%grid_tdens%size_cell(i) * p1p0%nrm_grad_dyn(i))
             end do
             do i=1,ntdens
                inc_full(i+npot:nfull) = &
                     -p1p0%fnewton_tdens(i) * p1p0%scr_ntdens(i)
             end do
          end if

       case (4) ! solve_augemented_jacobian
          if (ode_inputs%id_ode.eq. 1)  then
             write(*,*) 'only B=C case' 
             stop
          end if
          
          call p1p0%D_matrix%set(lun_err,p1p0%D2)

          !
          ! multiply second line lin sys by -mass_tdens/deltat
          !  
          p1p0%diagonal_scale = - deltat !/ p1p0%grid_tdens%size_cell 
          p1p0%inv_diagonal_scale = one / p1p0%diagonal_scale
        
          ! jac (2,1)
          call p1p0%deltatD1C_matrix%DxM(lun_err,&
               p1p0%inv_diagonal_scale)
          !jac (2,1)= deltatD1C_matrix = D1 B
          ! jac 2,2
          p1p0%D_matrix%diagonal = p1p0%D_matrix%diagonal * &
               p1p0%inv_diagonal_scale
          ! rhs
          rhs_full(npot+1:nfull) = rhs_full(npot+1:nfull) &
               * p1p0%inv_diagonal_scale

          
          !
          ! set relaxaction parameter
          !          
          ctrl%gamma =  deltat !* onehalf
          if (ctrl%debug .gt. 1) write(*,*) 'gamma' , ctrl%gamma
          

          ! W = Mass + gamma C
          p1p0%MP_coeff =  one ! p1p0%grid_tdens%size_cell
          p1p0%diagonal_weight = p1p0%MP_coeff &
               - & ! this minus because D = -C
               ctrl%gamma * p1p0%D_matrix%diagonal 
          p1p0%inv_diagonal_weight = one/ p1p0%diagonal_weight
           if (ctrl%debug .gt. 1) write(*,*) 'weight' , &
               ' min= ',minval(p1p0%diagonal_weight) ,&
               ' max= ',maxval(p1p0%diagonal_weight) 

          ! S = -one/gamma W
          !p1p0%hatS = - ( one / ctrl%gamma ) *  p1p0%diagonal_weight
          ! hatS = -one/gamma M_p-C
          p1p0%hatS = - ( one / ctrl%gamma ) *  p1p0%MP_coeff + &
               p1p0%D_matrix%diagonal
          
           if (ctrl%debug .ge. 1) write(*,*) 'hatS' , &
               ' min= ',minval(p1p0%hatS) ,&
               ' max= ',maxval(p1p0%hatS) 
 
          !
          ! compute new rhs_pot = rhs_old+gamma*BT W^{-1} rhs_tdens
          !                     
          do i=1,ntdens
             p1p0%scr_ntdens(i) = &
                  p1p0%inv_diagonal_weight(i) * rhs_full(npot+i)
          end do
          call p1p0%BT_matrix%Mxv( p1p0%scr_ntdens, p1p0%scr_npot)
          rhs_full(1:npot) = &
               rhs_full(1:npot) +  &
               ctrl%gamma *p1p0%scr_npot

          
          ! BT_gamma = BT - gamma BT W^{-1} C
          !          = BT + gamma BT W^{-1} D
          p1p0%BT_gamma%coeff = p1p0%BT_matrix%coeff
          do i=1,ntdens
             p1p0%scr_ntdens(i) = &
                  one + & ! here plus because D=-C
                  ctrl%gamma * &
                  p1p0%inv_diagonal_weight(i)* &
                  p1p0%D_matrix%diagonal(i)
          end do
          call p1p0%BT_gamma%MxD(lun_err,p1p0%scr_ntdens)

          write(*,*) 'gammaBT_gamma' 
          
          !
          ! build stiff_gamma = stiff +  gamma BT W ^{-1} D1 B 
          !                                                ==
          !                                 p1p0%deltatD1C_matrix 
          ! 
          tdpot%scr_ntdens= ctrl%gamma * p1p0%inv_diagonal_weight
          call p1p0%stiff_gamma_assembled%mult_MDN(lun_err,&
               p1p0%BT_matrix,p1p0%deltatD1C_matrix,&
               100,100*tdpot%npot,&
               tdpot%scr_ntdens)
          do i=1,p1p0%stiff%nrow
             do j=p1p0%stiff%ia(i),p1p0%stiff%ia(i+1)-1
                p1p0%stiff_gamma_assembled%coeff(p1p0%stiff2BTDC(j)) = &
                     p1p0%stiff_gamma_assembled%coeff(p1p0%stiff2BTDC(j)) +&
                     p1p0%stiff%coeff(j)
             end do
          end do 
          p1p0%stiff_gamma_assembled%is_symmetric = .true.
         
          
!!$          max=0.0
!!$          imax = 1
!!$          do i=1,p1p0%stiff_gamma_assembled%nrow
!!$             if (p1p0%stiff_gamma_assembled%coeff(p1p0%stiff_gamma_assembled%idiag(i))> max ) then
!!$                max = p1p0%stiff_gamma_assembled%coeff(p1p0%stiff_gamma_assembled%idiag(i))
!!$                imax = i
!!$             end if
!!$          end do
!!$          call p1p0%p1%dirichlet_bc(lun_err,&
!!$               p1p0%stiff_gamma_assembled,p1p0%scr_npot, tdpot%pot,&
!!$               1,&
!!$               (/imax/),&
!!$               (/one/))
          
          !write(*,*) 'A + gamma BTW^{-1}D1B' 
          !
          ! reassing jacobian matrix
          !
          p1p0%augmented_jacobian%mats(1)%mat => p1p0%stiff_gamma_assembled 
          p1p0%augmented_jacobian%mats(2)%mat => p1p0%BT_gamma
          p1p0%augmented_jacobian%mats(3)%mat => p1p0%deltatD1C_matrix
          p1p0%augmented_jacobian%mats(4)%mat => p1p0%D_matrix

          !
          ! set augmented lagrangian preconditoner
          !
!!$          call p1p0%triangular_augmented_prec%set(lun_err,&
!!$               p1p0%stiff_gamma_assembled , &
!!$               p1p0%deltatD1C_matrix,p1p0%hatS,info)
          info=0
          if (info.ne.0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' triangular augumented prec failure ')
              info=-1
           end if

          
          !
          ! update component (2,2) of block prec.
          !
!!$          ctrl%build_prec =1
!!$          p1p0%scr_ntdens = one/p1p0%D_matrix%diagonal
!!$          call p1p0%inverse_D2%set(lun_err,p1p0%scr_ntdens)
!!$          call assembly_stdprec(lun_err,&
!!$               p1p0%stiff_gamma_assembled ,&
!!$               ctrl%build_prec,&
!!$               ctrl%ctrl_prec,&
!!$               info_prec,&
!!$               p1p0%standard_prec_saved)


          !
          ! re-assign prec_full component
          !
          !p1p0%prec_full%precs(1)%linop => p1p0%triangular_augmented_prec%prec_A
!!$          p1p0%prec_full%linops(1)%linop => p1p0%standard_prec_saved
!!$          p1p0%prec_full%linops(2)%linop => p1p0%inverse_D2

          


          call CPU%LINEAR_SOLVER%set('start') 
          call ctrl%ctrl_solver%info(6)
          call linear_solver(&
               p1p0%augmented_jacobian,rhs_full,inc_full,&
               tdpot%info_solver,&
               ctrl%ctrl_solver,&
               prec_left=p1p0%triangular_augmented_prec,&
               !prec_left = p1p0%prec_full,&
               aux=p1p0%aux_bicgstab)      
                       
          call CPU%LINEAR_SOLVER%set('stop')

!!$          inc_full(npot+1:nfull) = inc_full(npot+1:nfull) * &
!!$               p1p0%diagonal_scale(:)
          if ( tdpot%info_solver%ierr .ne. 0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_err)
             rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_stat)
             info=-1
          end if
          
       case ( 5 ) ! uzawa method
          !
          ! first scale by -deltat 
          !
          !
          ! multiply second line lin sys by -mass_tdens/deltat
          !  
          p1p0%diagonal_scale = - deltat !/ p1p0%D1
          p1p0%inv_diagonal_scale = one / p1p0%diagonal_scale

          ! jac (2,1)
          p1p0%deltatD1C_matrix%coeff = p1p0%B_matrix%coeff
          tdpot%scr_ntdens =sqrt(tdpot%tdens)*p1p0%grid_tdens%size_cell
          call p1p0%deltatD1C_matrix%DxM(lun_err,&
               tdpot%scr_ntdens)
          tdpot%scr_ntdens =sqrt(tdpot%tdens)
          call p1p0%B_matrix%MxD(lun_err,&
               tdpot%scr_ntdens)

          call p1p0%deltatD1C_matrix%DxM(lun_err,&
               p1p0%inv_diagonal_scale)
          !jac (2,1)= deltatD1C_matrix = D1 B
          ! jac 2,2
          p1p0%D_matrix%diagonal = p1p0%D2
          p1p0%D_matrix%diagonal = -p1p0%D_matrix%diagonal * &
               p1p0%inv_diagonal_scale
          ! rhs
          call p1p0%assembly_fnewton_gfvar(ode_inputs,tdpot,deltat)
          rhs_full(npot+1:nfull) = p1p0%fnewton_gfvar &
               * p1p0%inv_diagonal_scale

          !
          ! set inverse of stiffness matrix
          !
          call assembly_stdprec(lun_err,&
               p1p0%stiff,&
               ctrl%build_prec,&
               ctrl%ctrl_prec,&
               info_prec,&
               p1p0%standard_prec_saved)
          call p1p0%inverse_stiff%set(p1p0%stiff,ctrl%ctrl_solver,&
               p1p0%standard_prec_saved)
          

          ctrl_uzawa = ctrl%ctrl_solver
          ctrl_uzawa%imax=10000
          ctrl_uzawa%iprt=1
          inc_full=zero
          call  uzawa_arrow_hurwics(&
               p1p0%stiff,&
               p1p0%BT_matrix,&
               p1p0%deltatD1C_matrix,&
               p1p0%D_matrix,&
               rhs_full(1:npot),rhs_full(npot+1:nfull),&
               inc_full(1:npot),inc_full(npot+1:nfull),&
               1.0d0, 1.0d-7,&
               ctrl_uzawa, tdpot%info_solver,&
               p1p0%inverse_stiff,aux=p1p0%aux_bicgstab)        
          call tdpot%info_solver%info(lun_out)
          info = tdpot%info_solver%ierr
          
          inc_full(npot+1:nfull) = inc_full(npot+1:nfull)**2  
          

       end select
       if ( ctrl%info_newton .gt. 0) then
          call tdpot%info_solver%info2str(str) 
          write(msg,'(I2,a,a)') iter_newton+1,' | LINSOL: ', etb(str)
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if

       !
       ! try to handle inversion failure with different
       ! linear solver procedure. 
       !
       if ( info .ne. 0) then
          call handle_failure_invert_jacobian(info,&
               iter_newton,&
               p1p0, &
               tdpot,&       
               ctrl, &
               try2invert)
          write(*,*) 'info try2invert', info,try2invert
          if ( try2invert ) then
             tdpot%nrestart_invert_jacobian = &
                  tdpot%nrestart_invert_jacobian  + 1 
             info = 0
          else
             return
          end if
       else
          !
          ! everything is OK, store linear system informations
          !
          if ( ctrl%build_prec .eq. 1) then
             tdpot%iter_last_prec = tdpot%info_solver%iter
             tdpot%iter_newton_last_prec = iter_newton
          end if
          
          tdpot%total_number_linear_system = &
               tdpot%total_number_linear_system +1
          tdpot%total_iterations_linear_system =  &
               tdpot%total_iterations_linear_system + &
               tdpot%info_solver%iter
          tdpot%res_elliptic = tdpot%info_solver%resreal
          
       end if
       
    end do

    call ortogonalize(npot,&
         1,&
         p1p0%kernel_full(1:npot,1),inc_full(1:npot))

    !
    ! restore linear solver controls
    !
    ctrl%ctrl_solver = ctrl%ctrl_solver_original

  contains
    !>------------------------------------------------
    !> Series of local subroutine for reduced approach
    !<--------------------------------------------------
    !>------------------------------------------------
    !> Local procedure to assembly rhs of lin. sys. for 
    !> reduced apprroach
    !>---------------------------------------------------
     subroutine assembly_rhs_reduced(B_matrix,&
         invD2,fnewton_tdens,fnewton_pot,work,rhs_reduced)
      use Globals
      use SparseMatrix
      implicit none
      type(spmat),       intent(inout) :: B_matrix
      real(kind=double), intent(in   ) :: invD2(B_matrix%nrow)
      real(kind=double), intent(in   ) :: fnewton_pot(B_matrix%nrow)
      real(kind=double), intent(in   ) :: fnewton_tdens(B_matrix%ncol)
      real(kind=double), intent(inout) :: work(B_matrix%nrow)
      real(kind=double), intent(inout) :: rhs_reduced(B_matrix%ncol)
      !local

      do i=1,B_matrix%nrow
         work(i)=invD2(i)*fnewton_tdens(i)
      end do

      call B_matrix%MTxv(work,rhs_reduced)
      call daxpy(B_matrix%ncol,-one,fnewton_pot,1,rhs_reduced,1)

    end subroutine assembly_rhs_reduced

    !>------------------------------------------------
    !> Local procedure to get increment of tdens variable 
    !> in reduced apprroach
    !>---------------------------------------------------
    subroutine get_inc_tdens(BC_matrix,&
         deltat,D1,invD2,inc_pot,fnewton_tdens,work,inc_tdens)
      use Globals
      use SparseMatrix
      implicit none
      type(spmat),       intent(inout) :: BC_matrix
      real(kind=double), intent(in   ) :: deltat
      real(kind=double), intent(in   ) :: D1(BC_matrix%nrow)
      real(kind=double), intent(in   ) :: invD2(BC_matrix%nrow)
      real(kind=double), intent(in   ) :: inc_pot(BC_matrix%ncol)
      real(kind=double), intent(in   ) :: fnewton_tdens(BC_matrix%nrow)
      real(kind=double), intent(inout) :: work(BC_matrix%nrow)
      real(kind=double), intent(inout) :: inc_tdens(BC_matrix%nrow)
      !local

      call BC_matrix%Mxv(inc_pot,work)

      do i = 1,BC_matrix%nrow
         inc_tdens(i) = invD2(i) * ( &
              deltat * D1(i) * work(i) - fnewton_tdens(i) &
              )
      end do

    end subroutine get_inc_tdens


    !>------------------------------------------------
    !> Procedure to select inversion approach
    !> Reduced or full
    !<--------------------------------------------------
    subroutine select_inversion_approach(lun_err,&
         p1p0,&
         ctrl,info)
      use Globals
      implicit none
      !vars
      integer,                         intent(in   )  :: lun_err
      type(p1p0_space_discretization), intent(in   )  :: p1p0
      type(CtrlPrm),                   intent(inout)  :: ctrl
      integer,                         intent(inout)  :: info
      ! local
      logical :: rc
      integer :: res
      

      !
      ! reduced approach
      !
      if ( ctrl%newton_method/10 .eq. 1 ) then       
         if ( minval(p1p0%D2) > zero)  then
            ctrl%reduced_jacobian = 1
         else
            if  ( ctrl%ctrl_solver%scheme .eq. 'PCG') then
               rc=IOerr(lun_err, wrn_val, 'select_inversion_strategy', &
                     ' PCG + negative value in D2 ')
               info = -1000
               return
            else
               !
               ! try to solve it with MINRES OR BIGSTAB
               !
               ctrl%reduced_jacobian = 1
               info = -1000
            end if
         end if
         ctrl%prec_newton = mod(ctrl%newton_method,10)
      end if


      !
      ! full approach
      !
      if ( ctrl%newton_method/10 .eq. 0 ) then       
         ctrl%reduced_jacobian = 0
         ctrl%prec_newton = mod(ctrl%newton_method,10) 
      end if

      !
      ! full approach
      !
      if ( ctrl%newton_method/10 .eq. 3 ) then       
         ctrl%reduced_jacobian = 3
         ctrl%prec_newton = 1
      end if

      !
      ! ibrid approach
      !
      if ( ctrl%newton_method/10 .eq. 2 ) then       
         if ( minval(p1p0%D2) > zero)  then
            !
            ! D2^{-1} exist end is positive 
            !
            ctrl%reduced_jacobian = 1
            select case ( mod(ctrl%newton_method,10))
            case (1) 
               ctrl%prec_newton = 1
            case (2) 
               ctrl%prec_newton = 2
            case (3) 
               ctrl%prec_newton = 1
            case (4) 
               ctrl%prec_newton = 2
            end  select
         else
            !
            ! if PCG is used, switch to solution of full system
            !
!!$            if  ( ctrl%ctrl_solver%scheme .ne. 'PCG') then
!!$               ctrl%reduced_jacobian = 1
!!$               select case ( mod(ctrl%newton_method,10))
!!$               case (1) 
!!$                  ctrl%prec_newton = 1 ! reduced + P~=A^{-1}
!!$               case (2) 
!!$                  ctrl%prec_newton = 2 ! reduced + P~=M^{-1}
!!$               case (3) 
!!$                  ctrl%prec_newton = 1 ! reduced + P~=A^{-1}
!!$               case (4) 
!!$                  ctrl%prec_newton = 2 ! reduced + P~=M^{-1}
!!$               end  select
!!$            else
               ctrl%reduced_jacobian = 0
               select case ( mod(ctrl%newton_method,10))
               case (1) 
                  ctrl%prec_newton = 3
               case (2) 
                  ctrl%prec_newton = 3
               case (3) 
                  ctrl%prec_newton = 4
               case (4) 
                  ctrl%prec_newton = 4
               end  select
               ctrl%ctrl_solver%scheme = 'BICGSTAB'
           ! end if
         end if
      end if

    end subroutine select_inversion_approach

    !
    ! Subroutine that, according to the inversion strategy
    ! select which preconditoner must be used.
    ! It sets flag
    ! build_prec  = 0/1
    ! build_tunig = 0/1
    !
    subroutine select_preconditioner_building(&
         this,&
         iter_newton,&
         ctrl)

      type(tdpotsys), intent(in   )  :: this
      integer,        intent(in   )  :: iter_newton
      type(CtrlPrm),  intent(inout)  :: ctrl

      ! local
      logical :: rc
      integer :: res
             
      !
      ! globals approach 0,1,2,3,4 
      !
      call this%set_threshold(ctrl)

      !
      ! newton specific appoach approach
      !
      select case (ctrl%id_buffer_prec)
      case (5)
         !
         ! build only a first newton iteration
         !
         ctrl%build_prec = 0
         if (iter_newton .eq. 0) ctrl%build_prec = 1
      case (6)
         !
         ! build at first newton iteration 
         ! and set as reference the iteration required
         !
         ctrl%build_prec = 0
         if (iter_newton .eq. 0)  then
            ctrl%build_prec = 1
         else if ( iter_newton .eq. 1) then
            ctrl%build_prec = 0
         else   
            ctrl%threshold = this%sequence_info_solver(1)%iter
            if ( this%sequence_info_solver(iter_newton-1)%iter > & 
                 int(ctrl%prec_growth  * ctrl%threshold ) ) then
               ctrl%build_prec=1
               ctrl%build_tuning=1
            else
               ctrl%build_prec   = 0
               ctrl%build_tuning = 0     
            end if
         end if
      end select

    end subroutine select_preconditioner_building

    !> -----------------------------------------------------
    !> Subroutine used to reset inversion controls
    !> (strategy, preconditioner,etc,)
    !> in case of linear solver failure
    !>------------------------------------------------------
    subroutine handle_failure_invert_jacobian(info,&
         iter_newton,&
         p1p0, &
         tdpot,&       
         ctrl, &
         try2invert)
      implicit none
      integer,                         intent(in   ) :: info
      integer,                         intent(in   ) :: iter_newton
      type(p1p0_space_discretization), intent(inout) :: p1p0
      type(tdpotsys),                  intent(inout) :: tdpot
      type(CtrlPrm),                   intent(inout) :: ctrl
      logical,                         intent(inout) :: try2invert

      try2invert = .False.
      if ( ( info .eq. -1) .and. &
           ( ctrl%newton_method/10 .eq. 2 ) .and. &
           ( ctrl%ctrl_solver%scheme .ne. 'BICGSTAB' ) ) then
         ctrl%ctrl_solver%scheme='BICGSTAB'
         ctrl%reduced_jacobian = 0
         select case ( mod(ctrl%newton_method,10))
         case (1) 
            ctrl%prec_newton = 3
         case (2) 
            ctrl%prec_newton = 3
         case (3) 
            ctrl%prec_newton = 4
         case (4) 
            ctrl%prec_newton = 4
         end  select
         try2invert = .True.
      end if
      
      !
      ! if we ctrl%build_prec
      !
      if ( ctrl%build_prec .eq. 0) then
         if ( tdpot%info_solver%iter > &
              ctrl%ctrl_solver_original%imax ) then
            ctrl%build_prec = 1
            try2invert = .True.
         end if
      end if

    end subroutine handle_failure_invert_jacobian


  end subroutine invert_jacobian

 subroutine set_threshold(tdpot,ctrl)
    implicit none
    class(tdpotsys), intent(in  ) :: tdpot
    type(CtrlPrm),  intent(inout) :: ctrl
    
    ! threshold is the minimum between the 
    ! number of iterations 
    ! required just after preconditioning and
    ! reference thereshold
    
    select case (ctrl%id_buffer_prec)
    case(1)
       ! cosntant threshold given in controls file
       ctrl%threshold  = ctrl%ref_iter
    case (2)
       !
       ! threshold is average of the linear solver iterations
       ! 
       ctrl%threshold  = &
            int( tdpot%total_iterations_linear_system  / &
            tdpot%total_number_linear_system ) 
    case (3)
       ! threshold is the number of iterations 
       ! required using the optimal preconditioner
       ctrl%threshold =  tdpot%iter_last_prec
    case (4)
       ! threshold is the minimum between the 
       ! number of iterations 
       ! required just after preconditioning and
       ! reference thereshold
       ctrl%threshold =  min(ctrl%ref_iter, tdpot%iter_last_prec)
    case (5)
       ! iteration for solution of first elliptic 
       ctrl%threshold = tdpot%iter_first_system
    end select

    if ( ctrl%id_buffer_prec .ne. 0) then
       if ( tdpot%info_solver%iter > & 
            int(ctrl%prec_growth  * ctrl%threshold ) ) then
          ctrl%build_prec=1
          ctrl%build_tuning=1
       else
          ctrl%build_prec   = 0
          ctrl%build_tuning = 0     
       end if
    else
       ctrl%build_prec=1
       ctrl%build_tuning=1
    end if
  end subroutine set_threshold

  !>--------------------------------------------------------
  !> Procedure to assembly sparse matrix
  !> 
  !> $\Matr[\Itd, IPot]{BC} = 
  !>   \int_{\Domain} \testx_{\Itd} |\Grad \Pot|^{\Pgrad-2}< \Grad \Pot, \Grad \testp_{\Ipot}>$ 
  !<--------------------------------------------------------
  subroutine assembly_BC_matrix(p1p0,tdpot, pgrad ,BC_matrix)
      use Globals
      use SparseMatrix
      implicit none
      
      class(p1p0_space_discretization), intent(inout) :: p1p0
      type(tdpotsys),                   intent(inout) :: tdpot
      real(kind=double),                intent(in   ) :: pgrad
      type(spmat),                      intent(inout) :: BC_matrix
      !local
      integer :: icell_sub,itria_parent,iloc,ind
      integer :: nnodeincell,ndim
      real(kind=double) :: pot_cell(4),gradpot_cell(3),grad_base(3), area_subgrid
      real(kind=double) :: weight_grad_norm      
      real(kind=double) :: ddot,dnrm2

      nnodeincell = p1p0%p1%grid%nnodeincell
      ndim        = p1p0%grid_pot%logical_dimension

      BC_matrix%coeff  = zero

      do icell_sub=1,p1p0%p1%grid%ncell
         ! copy area of triangle in subgrid            
         area_subgrid = p1p0%p1%grid%size_cell(icell_sub)
         weight_grad_norm = dnrm2(ndim, tdpot%gradpot(1:ndim,icell_sub),1)**(pgrad-2.0d0) 

         ! get index of triangle of coaser grid     
         do iloc = 1,nnodeincell
            ! get gradient of local base function
            call p1p0%p1%get_gradbase(iloc,icell_sub, grad_base)
            
            
            ! add contribution
            ind=p1p0%assembler_Bmatrix_subgrid(iloc,icell_sub)
            BC_matrix%coeff(ind) = BC_matrix%coeff(ind) + &
                 ddot(ndim, tdpot%gradpot(1:ndim,icell_sub),1, grad_base(1:ndim), 1 ) * &
                 weight_grad_norm * area_subgrid
         end do
      end do

      BC_matrix%is_symmetric = ( abs(pgrad-2.0d0)<small )

    end subroutine assembly_BC_matrix

    !>--------------------------------------------------------
    !> Procedure $\Pot$-part of Newton function
    !> 
    !> $\Fnewton_1 = 
    !> \Stiff\Of{\Vect{\Tdens}} \cdot \Vect{\Pot}-\Vect{\Forcing}$ 
    !<--------------------------------------------------------
    subroutine assembly_fnewton_pot(p1p0,ode_inputs,tdpot)
      use Globals
      use SparseMatrix
      implicit none
      class(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),                   intent(in   ) :: ode_inputs
      type(tdpotsys),                  intent(in   ) :: tdpot

      !
      ! compute \Stiff * \Pot
      !
      call p1p0%stiff%Mxv(tdpot%pot,p1p0%fnewton_pot)
      !
      ! compute \Fnewton =  \Stiff * \Pot - \Rhs
      !
      p1p0%fnewton_pot = p1p0%fnewton_pot - &
           ode_inputs%rhs_integrated(1:p1p0%npot,2)
      
    end subroutine assembly_fnewton_pot

    !>--------------------------------------------------------
    !> Procedure $\Pot$-part of Newton function
    !> 
    !> $\Fnewton_2 =
    !> \Vect{\Tdens} - \Vect{\Tdens}^{k} -deltat * RHS_ODE$ 
    !<--------------------------------------------------------   
    subroutine assembly_fnewton_tdens(p1p0,ode_inputs,tdpot,deltat)
      use Globals
      implicit none
      class(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),                   intent(in   ) :: ode_inputs
      type(tdpotsys),                  intent(in   ) :: tdpot
      real(kind=double),               intent(in   ) :: deltat
      ! local
      integer :: icell

      call p1p0%assembly_rhs_ode(&
         2,&
         ode_inputs,&
         tdpot,&
         p1p0%rhs_ode)

      call p1p0%get_increment_ode(p1p0%rhs_ode,p1p0%scr_ntdens)
      
      p1p0%fnewton_tdens = tdpot%tdens - tdpot%tdens_old  &
           - deltat * p1p0%scr_ntdens
      
    end subroutine assembly_fnewton_tdens

    !>--------------------------------------------------------
    !> Procedure $\Pot$-part of Newton function
    !> 
    !> $\Fnewton_2 =
    !> \Vect{\Gfvar} - \Vect{\Gfvar}^{k} -deltat * RHS_ODE$ 
    !<--------------------------------------------------------   
    subroutine assembly_fnewton_gfvar(p1p0,ode_inputs,tdpot,deltat)
      use Globals
      implicit none
      class(p1p0_space_discretization), intent(inout) :: p1p0
      type(odedata),                   intent(in   ) :: ode_inputs
      type(tdpotsys),                  intent(in   ) :: tdpot
      real(kind=double),               intent(in   ) :: deltat
      ! local
      integer :: icell

      call p1p0%assembly_rhs_ode(&
         2,&
         ode_inputs,&
         tdpot,&
         p1p0%rhs_ode)

      call p1p0%get_increment_ode(p1p0%rhs_ode,p1p0%scr_ntdens)
      
      p1p0%fnewton_gfvar = one/deltat * (tdpot%gfvar - tdpot%gfvar_old)  &
           + p1p0%scr_ntdens
      
    end subroutine assembly_fnewton_gfvar

    subroutine set_active_regions(p1p0,tdpot,ctrl)
      use Globals
      implicit none
      class(p1p0_space_discretization), intent(inout) :: p1p0
      type(tdpotsys),                   intent(inout) :: tdpot
      type(CtrlPrm),  intent(in   ) :: ctrl
      ! local 
      integer :: icell,inode,iloc,ifather
      logical :: on
 
      
      tdpot%ntdens_on    = 0
      tdpot%ntdens_off   = 0
      tdpot%active_tdens = 0
      tdpot%onoff_tdens  = .False.
      do icell = 1,p1p0%grid_tdens%ncell 
         on = ( tdpot%tdens(icell) .ge. ctrl%threshold_tdens)
         tdpot%onoff_tdens(icell) = on
         if ( on ) then
            tdpot%ntdens_on = tdpot%ntdens_on + 1
            tdpot%active_tdens(tdpot%ntdens_on) = icell
         else
            tdpot%ntdens_off = tdpot%ntdens_off + 1 
            tdpot%inactive_tdens(tdpot%ntdens_off) = icell
         end if
      end do

      !
      ! in order to switch off a node all tdens 
      ! surrunding it must be inactive
      !      
      tdpot%onoff_pot(:)  = .False.
      do icell=1,p1p0%grid_pot%ncell
         ifather = p1p0%grid_pot%cell_parent(icell) 
         do iloc = 1, p1p0%grid_pot%nnodeincell
            inode =  p1p0%grid_pot%topol(iloc,icell)
            tdpot%onoff_pot(inode) = tdpot%onoff_pot(inode) .or.  tdpot%onoff_tdens(ifather) 
         end do
      end do

      tdpot%npot_on  =0
      tdpot%npot_off = 0
      do inode = 1,p1p0%grid_pot%nnode
        if ( tdpot%onoff_pot(inode) ) then
            tdpot%npot_on = tdpot%npot_on + 1
            tdpot%active_pot(tdpot%npot_on) = inode
         else
            tdpot%npot_off = tdpot%npot_off + 1 
            tdpot%inactive_pot(tdpot%npot_off) = inode
         end if
      end do

    end subroutine set_active_regions


    !>---------------------------------------------------------
  !> Producedure to update tdens-pot and derived varibles to
  !> via backward Euler time-stepping i.e.
  !> (\Tdens^{k+1},\Pot^{k+1}) = solution of
  !>
  !> \Stiff[\Tdens] \Pot = \RHS_FORCING
  !> \Tdens - \Tdens^{k} = -\Deltat *( RHS_ODE(\Tdens,\Pot) 
  !>
  !> The non-linear equation is solved via Newton-Rapshon method.
  !> Iteration will start from ( tdens_start, pot_start) 
  !>---------------------------------------------------------
  subroutine implicit_euler_newton_gfvar(this, &
       lun_err,lun_out,lun_stat, &
       ctrl, &
       current_time_iteration,current_time,deltat, &
       info,&
       CPU,&
       p1p0,&
       ode_inputs,&
       gfvar_start,&
       pot_start)
    use Globals
    use IOdefs
    use TimeInputs, only : write_steady
    use CombinedSparseMatrix
    use Timing
    use SimpleMatrix
    use Matrix
    use BlockMatrix
    use InexactConstraintPreconditioner
    implicit none
    class(tdpotsys), target,  intent(inout) :: this
    integer,           intent(in   ) :: lun_err,lun_out,lun_stat
    type(CtrlPrm),     intent(inout) :: ctrl
    integer,           intent(in   ) :: current_time_iteration
    real(kind=double), intent(in   ) :: current_time
    real(kind=double), intent(inout) :: deltat
    integer,           intent(inout) :: info
    type(codeTim),     intent(inout) :: CPU
    type(p1p0_space_discretization), target, intent(inout) :: p1p0
    type(odedata),     intent(in   ) :: ode_inputs
    real(kind=double), intent(in   ) :: gfvar_start(this%ntdens)
    real(kind=double), intent(in   ) :: pot_start(this%npot)
    

    ! local
    logical :: rc,endfile,test_exit,reduced=.False.
    character(len=256) :: outformat
    integer :: res,method
    integer :: ntdens, npot,nfull
    integer :: i,j,iter_newton,info_prec,info_broyden,ind
    real(kind=double) :: res_tdens, res_pot
        
    integer :: ibegin, iend
    real(kind=double), pointer :: D1(:)
    real(kind=double), pointer :: D2(:)
    real(kind=double), pointer :: invD1(:)
    real(kind=double), pointer :: invD2(:)
    real(kind=double), pointer :: invD2_D1(:),invD1_D2(:)
    real(kind=double), pointer :: fnewton_pot(:)
    real(kind=double), pointer :: fnewton_tdens(:)
    real(kind=double), pointer :: fnewton_pot_old(:)
    real(kind=double), pointer :: fnewton_tdens_old(:)
    ! increments
    real(kind=double), pointer :: inc_pot(:)
    real(kind=double), pointer :: inc_tdens(:)
    real(kind=double), allocatable :: inc_full(:)
    ! local work arrays
    real(kind=double), pointer :: work(:)
    real(kind=double), pointer :: work2(:)
    ! intial choice 
    real(kind=double), pointer :: tdens_initial(:)
    real(kind=double), pointer :: pot_initial(:)
        ! rhs linear system
    real(kind=double), allocatable :: rhs_full(:)
    real(kind=double), pointer :: kernel_full(:,:) 
    


    real(kind=double) :: rhs_norm,prev_rhs_norm,prev_res ,shift
    real(kind=double) ::  inc_norm, prev_inc_norm
    ! jacobian variables reduced 
    type(combspmat) :: jacobian_reduced
    type(eye),   target :: identity_npot
    class(abs_linop), pointer :: prec_E
    type(spmat), pointer :: matrix2prec

    !type(stdprec), target :: spprec_Schur
    class(abs_linop), pointer :: prec_Schur
        
    ! prec vars
    class(abs_linop), pointer :: prec_final 
    
    ! prec for full jacobian
    type(stdprec),  target :: prec_stiff
    type(eye),   target :: identity_ntdens
    
    type(file) :: fmat
    

    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(eye),  target  :: identity
    real(kind=double), pointer :: diagonal_full(:) 

    logical old, matsave
    character(len=256) :: fname,directory,tail
    

    ! functions 
    real(kind=double) :: dnrm2,ddot,pode,old_tol,max_on_support,rmax
    character(len=70) :: str
    character(len=256) :: msg,msg1,msg2

    call CPU%OVH%set('start')
    

    !
    ! array dimension
    !
    npot   = this%npot
    ntdens = this%ntdens
    nfull  = npot + ntdens
    
    allocate(rhs_full(nfull), inc_full(nfull))


    do i=1,ctrl%nbroyden
       call p1p0%broyden_updates(i)%init(lun_err, ntdens)
       call p1p0%rankone_update(i)%init(lun_err, ntdens)
    end do

    
    do i=1,ctrl%max_iter_nonlinear
       call this%sequence_info_solver(i)%kill()
       this%sequence_build_prec(i)   = 0
       this%sequence_build_tuning(i) = 0
    end do


    !
    ! init flags
    !
    
    
    info = 0
    iter_newton = 0
    test_exit=.false.  
    
    !
    ! Newton legend
    !
    write(str,'(a)') ' NEWTON GFVAR begin '
    write(msg,'(a)') ctrl%sep(str) 
    write(lun_out,*) etb(msg)
    write(lun_stat,*) etb(msg)



    write(msg,'(a)')&
         'it| OUT   :  nrm_inc nrm_rhs | inc_td  inc_pot | '//&
         'info it resini  resnorm '//&
         'resreal   method      ' 
    write(lun_out,*) etb(msg)
    write(lun_stat,*) etb(msg)

    call CPU%OVH%set('stop')
    


    do while ( .not. test_exit )
       if ( ctrl%info_newton .ge. 0 ) then
          msg=('------------------------'//&
               '----------------------------------------------')
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if

       call CPU%ASSEMBLY%set('start')
       this%nlinear_system=this%nlinear_system+1
       !------------------------------------------------------
       ! Assembly matrices
       !   stiff, B, C, D1, D2^{-1}, D2^{-1}D1
       ! that compose the jacobian matrix 
       !      J_F=( Stiff(tdens) B^T DG )
       !          ( DG  C        D2  ) 
       !-------------------------------------------------------
       call p1p0%assembly_newton(&
            this,&
            lun_err,lun_out,ctrl%debug,&
            iter_newton,&
            current_time_iteration,current_time,deltat,&
            ode_inputs) 
       !
       ! 2 - assembly D1 D2 invD2 invD2_D1
       !     possible to changse deltat on
       !
       call p1p0%build_grad_vars(lun_err,this)
       call p1p0%build_nrm_grad_dyn(this,2.0d0)
       if ( ctrl%gfvar_approach .eq. 1) then
          !
          ! 2 diag(gfvar) B
          ! 
          p1p0%DB_matrix%coeff = p1p0%B_matrix%coeff 
          this%scr_ntdens= two*this%gfvar
          call p1p0%DB_matrix%DxM(lun_err,this%scr_ntdens)

          !
          ! 2 BT diag(gfvar)
          ! 
          p1p0%BTD_matrix%coeff = p1p0%BT_matrix%coeff 
          call p1p0%BTD_matrix%MxD(lun_err, this%scr_ntdens)
       else
          !
          ! 2 diag(gfvar^2) B
          ! 
          p1p0%DB_matrix%coeff = p1p0%B_matrix%coeff 
          this%scr_ntdens= two*this%gfvar**2
          call p1p0%DB_matrix%DxM(lun_err,this%scr_ntdens)

          !
          ! 2 BT diag(gfvar)
          ! 
          p1p0%BTD_matrix%coeff = p1p0%BT_matrix%coeff 
          this%scr_ntdens= two*this%gfvar
          call p1p0%BTD_matrix%MxD(lun_err, this%scr_ntdens)
       end if

       !
       ! fnewton = A(mu)-b
       !
       call p1p0%stiff%Mxv(this%pot,p1p0%scr_npot)
       p1p0%fnewton_pot = p1p0%scr_npot - ode_inputs%rhs_integrated(:,2)
       !call assembly_fnewton_pot(p1p0,ode_inputs,this) ! same as tdens
       
       if ( ctrl%gfvar_approach .eq. 1) then
          !
          ! -1/dt + gf * ( |\grad \Pot|^2-1)
          !
          do i=1,ntdens
             p1p0%fnewton_gfvar(i) = - one/deltat * (this%gfvar(i) - this%gfvar_old(i))  &
                  + this%gfvar(i) *( p1p0%nrm_grad_dyn(i) - one )
          end do
       else
          !
          ! -(gfvar-gfvar_old)^2/dt + gf^2 * ( |\grad \Pot|^2-1)
          !
          do i=1,ntdens
             p1p0%fnewton_gfvar(i) = - ( this%gfvar(i)-this%gfvar_old(i) )**2 /deltat + &
                   (this%gfvar(i)**2)*( p1p0%nrm_grad_dyn(i) - one )
          end do
       end if
       !
       ! matric D with minus
       !
       if ( ctrl%gfvar_approach .eq. 1) then
          do i=1,ntdens
             this%scr_ntdens(i)= ( - one* (-one/deltat + ( p1p0%nrm_grad_dyn(i) - one ) ) ) * &
                  p1p0%grid_tdens%size_cell(i)
          end do
       else
          !
          ! -2*(gfvar-gfvar_old)/dt + 2*gf * ( |\grad \Pot|^2-1)
          !
          do i=1,ntdens
             this%scr_ntdens(i)= ( - one* (&
                  -2*( this%gfvar(i)-this%gfvar_old(i) )/deltat + &
                  2*this%gfvar(i) * ( p1p0%nrm_grad_dyn(i) - one ) ) ) * &
                  p1p0%grid_tdens%size_cell(i)
          end do
       end if
       call p1p0%D_matrix%set(lun_err, this%scr_ntdens)

      
      

       

       
       p1p0%sym_jacobian_full%mats(1)%mat => p1p0%stiff
       p1p0%sym_jacobian_full%mats(2)%mat => p1p0%BTD_matrix
       p1p0%sym_jacobian_full%mats(3)%mat => p1p0%DB_matrix
       p1p0%sym_jacobian_full%mats(4)%mat => p1p0%D_matrix
    
       !
       ! assembly rhs
       !
       rhs_full(1:npot)             = - p1p0%fnewton_pot(:)
       rhs_full(npot+1:npot+ntdens) = - p1p0%fnewton_gfvar(:)

       !
       ! evalute norm of rhs and residua for inexact newton
       !
       ! res=rhs - J * inc  = (rhs for iter_newton=0)
       !     
       rhs_norm   = dnrm2(npot+ntdens,rhs_full,1)
       call p1p0%sym_jacobian_full%Mxv(inc_full, this%scr_nfull)
       this%scr_nfull =  this%scr_nfull + rhs_full
       prev_res = dnrm2(nfull, this%scr_nfull ,1 )
       call CPU%ASSEMBLY%set('stop')

       !
       ! save all matrices and rhs in NEwton system
       !
       !
       ! save all matrices and rhs in NEwton system
       !
       matsave=(ctrl%id_save_matrix > 0) 
       if ( matsave) then
          call CPU%SAVE%set('start')
          write(tail,'(I0.4,a,I0.4,a)') &
               this%time_iteration,'_',iter_newton, '.dat' 
          directory=etb('./output/linsys')
          if (ctrl%id_save_matrix .ge. 2) &
               call p1p0%write_newton(lun_err,10000,directory,tail,deltat)
          
          fname=etb(etb(directory)//'/deltat_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          write(fmat%lun,*) deltat 
          call fmat%kill(lun_err)

          
          fname=etb(etb(directory)//'/tdens_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call write_steady(lun_err, 10000, ntdens, this%tdens)
          call fmat%kill(lun_err)
          
          fname=etb(etb(directory)//'/pot_'//etb(tail))
          call fmat%init(lun_err,fname,10000,'out')
          call write_steady(lun_err, 10000, npot, this%pot)
          call fmat%kill(lun_err)


          call CPU%SAVE%set('stop')
       end if

       !
       ! info state before linear system solution
       !
       if ( ctrl%info_newton .eq. 2 ) then
          !
          ! Jacobian info
          !
          this%scr_ntdens(:)=p1p0%nrm_grad_dyn(:) - one
          max_on_support = zero
          do i=1,ntdens
             if ( this%tdens(i) > 1.0d-12) then
                max_on_support = max(max_on_support,this%scr_ntdens(i))
             end if
          end do
                
          write(msg1,'(I2,a,2(1pe9.1),a,1(1pe9.1),a,1(1pe9.1))' ) &
               iter_newton+1,&
               ' | IN    : JACOBIAN(2,2): min max D =',&
               minval( p1p0%D_matrix%diagonal),maxval( p1p0%D_matrix%diagonal)
          write(lun_out,*)  etb(msg1)
          write(lun_stat,*) etb(msg1)
          write(msg1,'(I2,a,a,1(1pe9.1),a,1(1pe9.1),a,1(1pe9.1))' ) &
               iter_newton+1,&
               ' | IN    : ',&
               ' MAX(|\grad u|^2-1) = ', maxval(this%scr_ntdens),&
               '| MAX(|\grad u|^2-1)=', max_on_support,&
               ' on mu>', 1.0d-12
          write(lun_out,*)  etb(msg1)
          write(lun_stat,*) etb(msg1)
          max_on_support = zero
          do i=1,ntdens
             max_on_support = max(max_on_support,this%gfvar(i)*this%scr_ntdens(i))
          end do
          rmax = zero
          do i=1,ntdens
             rmax = max(rmax,this%tdens(i)*this%scr_ntdens(i))
          end do
          write(msg1,'(I2,a,a,1(1pe9.1),a,1(1pe9.1))' ) &
               iter_newton+1,&
               ' | IN    : ',&
               ' MAX(\mu(|\grad u|^2-1)) = ', rmax,&
               '| MAX(\gf(|\grad u|^2-1))=', max_on_support
          write(lun_out,*)  etb(msg1)
          write(lun_stat,*) etb(msg1)
          
       end if
       
       !
       ! set controls for inversion of jacobian 
       ! of the newton iteration like:
       ! 1 - tolerance of solution
       !
       call set_linear_solver_tolerance(ctrl,&
            iter_newton,rhs_norm,rhs_norm,prev_res)
  
       !
       ! try to solve linear system J_F(k) inc(k) = -F(k)
       !
       rhs_full(npot+1:npot+ntdens) = rhs_full(npot+1:npot+ntdens) * p1p0%grid_tdens%size_cell(:)
       inc_full=zero


       !
       ! select base don tdens
       !          
       if ( ctrl%threshold_tdens>1e-20 ) then
          call p1p0%set_active_regions(this,ctrl)

          if ( this%ntdens_on .ne. this%ntdens ) then
             write(msg1,'(I2,a,a,1(1f6.2),a,1(1f6.2))' ) &
                  iter_newton+1,&
                  ' | IN    : ',&
                  ' %tdens on =', this%ntdens_on*100.d0/this%ntdens, &
                  ' %pot   on =', this%npot_on*100.d0/this%npot
             write(lun_out,*)  etb(msg1)
             write(lun_stat,*) etb(msg1)
             call p1p0%near_kernel%set(this%npot_off, this%inactive_pot)

!!$
!!$             write(tail,'(I0.4,a,I0.4,a)') &
!!$                  this%time_iteration,'_',iter_newton, '.dat' 
!!$             directory=etb('./output/linsys')
!!$             fname=etb(etb(directory)//'/active_tdens_'//etb(tail))
!!$             p1p0%scr_ntdens=zero
!!$             do i=1,this%ntdens_on
!!$                j = this%active_tdens(i)
!!$                p1p0%scr_ntdens(j) = 1.0d0
!!$             end do
!!$             call fmat%init(lun_err,fname,10000,'out')
!!$             call write_steady(lun_err, 10000, ntdens, p1p0%scr_ntdens)
!!$             call fmat%kill(lun_err)
!!$
!!$             fname=etb(etb(directory)//'/inactive_tdens_'//etb(tail))
!!$             p1p0%scr_ntdens=zero
!!$             do i=1,this%ntdens_off
!!$                j = this%inactive_tdens(i)
!!$                p1p0%scr_ntdens(j) = 2.0d0
!!$             end do
!!$             call fmat%init(lun_err,fname,10000,'out')
!!$             call write_steady(lun_err, 10000, ntdens, p1p0%scr_ntdens)
!!$             call fmat%kill(lun_err)
!!$
!!$
!!$
!!$             write(tail,'(I0.4,a,I0.4,a)') &
!!$                  this%time_iteration,'_',iter_newton, '.dat' 
!!$             directory=etb('./output/linsys')
!!$             fname=etb(etb(directory)//'/active_pot_'//etb(tail))
!!$             p1p0%scr_npot=zero
!!$             do i=1,this%npot_on
!!$                j = this%active_pot(i)
!!$                p1p0%scr_npot(j) = 1.0d0
!!$             end do
!!$             call fmat%init(lun_err,fname,10000,'out')
!!$             call write_steady(lun_err, 10000, npot, p1p0%scr_npot)
!!$             call fmat%kill(lun_err)
!!$
!!$             write(tail,'(I0.4,a,I0.4,a)') &
!!$                  this%time_iteration,'_',iter_newton, '.dat' 
!!$             directory=etb('./output/linsys')
!!$             fname=etb(etb(directory)//'/inactive_pot_'//etb(tail))
!!$             p1p0%scr_npot=zero
!!$             do i=1,this%npot_off
!!$                j = this%inactive_pot(i)
!!$                p1p0%scr_npot(j) = 2.0d0
!!$             end do
!!$             call fmat%init(lun_err,fname,10000,'out')
!!$             call write_steady(lun_err, 10000, npot, p1p0%scr_npot)
!!$             call fmat%kill(lun_err)
             
             
             !
             ! set to one D matrix to ensure inversion
             ! this term will be kill in B diag(gf) D diag(gf) BT 
             !
             do i=1,this%ntdens_off
                j = this%inactive_tdens(i)
                p1p0%D_matrix%diagonal(j) = 1.0d0
             end do
             !
             ! kill terms on Au-b that corresponding to "zero" tdens
             !
             rmax=zero
             do i=1,this%npot_off
                j = this%inactive_pot(i)
                rmax=max(rmax,abs(rhs_full(j)))
                rhs_full(j) = zero
                call p1p0%stiff%set_rowcol(j,zero)
             end do
             write(*,*) 'max pot rhs ',rmax 
             !
             ! kill terms on ode increment that corresponding to "zero" tdens
             !
             rmax=zero
             do i=1,this%ntdens_off
                j = this%inactive_tdens(i)
                rmax=max(rmax,abs(rhs_full(npot+j)))
                rhs_full(npot+j)=zero
             end do
             write(*,*) 'max tdens rhs ',rmax 

!!$             p1p0%scr_ntdens=one
!!$             do i=1,this%ntdens_off
!!$                j = this%inactive_tdens(i)
!!$                p1p0%scr_ntdens(j)=zero
!!$             end do
!!$             call p1p0%DB_matrix%DxM(lun_err,p1p0%scr_ntdens)
!!$             call p1p0%BTD_matrix%MxD(lun_err,p1p0%scr_ntdens)
             
          end if
       end if
       call p1p0%invert_jacobian_general( &
            p1p0%stiff,& ! A
            p1p0%DB_matrix,&    ! B
            p1p0%BTD_matrix,&   ! BT
            p1p0%DB_matrix,&      ! C
            p1p0%D_matrix, & ! D
            .true.,&         ! B=C
            rhs_full(1:npot),& ! f 
            rhs_full(npot+1:npot+ntdens), & ! g
            inc_full(1:npot),& ! vec_x
            inc_full(npot+1:npot+ntdens),& ! vec_y
            iter_newton,&
            this,ode_inputs,&       
            lun_err,lun_out,lun_stat, &
            ctrl, &
            current_time_iteration,current_time,deltat, &
            info,&
            CPU)
       rhs_full(npot+1:npot+ntdens) = rhs_full(npot+1:npot+ntdens) / p1p0%grid_tdens%size_cell(:)
       !
       ! if increment was not computed properly exit
       !
       if ( info .ne. 0) return 

       if ( this%ntdens .ne. this%ntdens_on) then
          do i=1,this%npot_off
             j=this%inactive_pot(i)
             inc_full(j)=zero
          end do
          do i=1,this%ntdens_off
             j=this%inactive_tdens(i)
             inc_full(npot+j)=zero
          end do
       end if
       

       !
       ! residum \| s \| and norm of rhs \| F \|
       !
       prev_inc_norm = inc_norm
       prev_rhs_norm  = rhs_norm


       inc_norm = dnrm2(nfull,inc_full,1)     
       rhs_norm   = dnrm2(nfull,rhs_full,1)

       


       !
       ! update iteration number
       !
       iter_newton = iter_newton + 1
       if ( ctrl%info_newton .gt. 0) then
          write(msg,'(I2,a,a,2(1pe8.1),a,a,2(1pe8.1)a,a,2(1pe8.1))') &
               iter_newton,&
               ' | OUT   : ',&
               'inc,rhs=',inc_norm,rhs_norm,&
               ' | ',&
               'gf : inc,rhs=', dnrm2(ntdens,inc_full(npot+1:nfull),1),dnrm2(ntdens,p1p0%fnewton_gfvar,1),&
               ' | ',&
               'pot: inc,rhs=', dnrm2(ntdens,inc_full(1:npot),1),dnrm2(ntdens,p1p0%fnewton_pot,1)
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if
       
       
       !
       ! store info linear solver 
       !
       this%sequence_info_solver(iter_newton)  = this%info_solver
       this%sequence_build_prec(iter_newton)   = ctrl%build_prec
       this%sequence_build_tuning(iter_newton) = ctrl%build_tuning
       this%sequence_inc_norm(iter_newton)     = min(inc_norm,rhs_norm)

       
       !
       ! test and update
       !
       test_exit = ( min(inc_norm,rhs_norm) .lt. ctrl%tol_nonlinear )


       !
       ! check if newton is converging
       !
       if ( ( iter_newton .gt. 1) .and. (.not. test_exit) )  then
          if ( ( rhs_norm  .gt. prev_rhs_norm * 1.0d4  )  .or. &
              (  inc_norm  .gt. prev_inc_norm * 1.0d4 ) ) then
             write(msg,*)'prev rhs_norm', prev_rhs_norm , &
                  'current rhs norm',  rhs_norm  
             write(lun_out,*) etb(msg)
             write(lun_stat,*) etb(msg)
             write(msg,*)'prev inc_norm', prev_inc_norm , &
                  'current inc norm',  inc_norm  
             write(lun_out,*) etb(msg)
             write(lun_stat,*) etb(msg)
             info=-4
             rc = IOerr(lun_err, wrn_val,&
                  ' implicit_euler_newton', &
                  ' norm of residua of rhs was not decreasing ' ) 
             return
          end if
       end if



       !
       ! update
       !
       if (res.ne.0) rc=IOerr(lun_err,err_dealloc, 'spkernel kill', &
                     ' type spkernl member indeces ')

      


       if ( .not. test_exit ) then
          this%pot   = this%pot   + inc_full(1:npot)
          this%gfvar = this%gfvar + inc_full(npot+1:npot+ntdens)
          call gfvar2tdens(ntdens,2,one,&
               this%gfvar, this%tdens)
          do i=1,ntdens
             this%tdens(i) = max(this%tdens(i),ctrl%min_tdens)
          end do
       end if       

       !
       ! check for restarting newton
       !            
       if ( iter_newton .eq. ctrl%max_iter_nonlinear ) then
          test_exit = .true.
          info = -2
       end if
      

       !---------------------------------------------------
       ! Store info before jacobian is updated 
       !---------------------------------------------------
       prev_res      = this%info_solver%resreal

       !
       ! store F newton for broyden
       !
       p1p0%fnewton_pot_old   = p1p0%fnewton_pot
       p1p0%fnewton_gfvar_old = p1p0%fnewton_gfvar
       
    end do
    this%iter_nonlinear=iter_newton
    
    this%scr_npot=one
    call ortogonalize(npot,&
         1,& 
         this%scr_npot,this%pot)
    call this%build_nrm_grad_vars(p1p0,lun_err)
    pode = ode_inputs%pode(2)
    call p1p0%build_nrm_grad_dyn(this,pode)


    this%res_elliptic = &
         dnrm2(npot,p1p0%fnewton_pot,1) / &
         dnrm2(npot,ode_inputs%rhs_integrated(:,2),1) 
   
    !
    ! free memory
    ! 
    do i=1,ctrl%nbroyden
       call p1p0%broyden_updates(i)%kill(lun_err)
       call p1p0%rankone_update(i)%kill(lun_err)

    end do
    deallocate(rhs_full, inc_full)

  end subroutine implicit_euler_newton_gfvar
      
    subroutine spkernel_init(this,lun_err,ncol)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,         intent(in   ) :: lun_err
       integer,         intent(in   ) :: ncol
       ! local
       logical :: rc
       integer :: res
       
       this%nrow = ncol
       this%ncol = ncol
       this%is_symmetric =.True.
       allocate(this%indeces(ncol),this%base_kernel(ncol),stat=res)
       if (res.ne.0) rc=IOerr(lun_err,err_dealloc, 'spkernel kill', &
                     ' type spkernl member indeces ')
       this%nkernel=0
       
     end subroutine spkernel_init

     subroutine spkernel_kill(this,lun_err)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,         intent(in ) :: lun_err
       ! local
       logical :: rc
       integer :: res

       
       this%nrow = 0
       this%nkernel=0
       this%ncol = 0
       this%is_symmetric =.false.
       deallocate(this%indeces,stat=res)
       if (res.ne.0) rc=IOerr(lun_err,err_dealloc, 'spkernel kill', &
                     ' type spkernl member indeces ')
       
     end subroutine spkernel_kill

     subroutine spkernel_set(this,nkernel, indeces)
       use Globals
       implicit none
       class(spkernel), intent(inout) :: this
       integer,  intent(in ) :: nkernel
       integer,  intent(in ) :: indeces(nkernel)
       ! local
       integer :: i
       
       this%nrow = nkernel
       this%indeces(1:nkernel) = indeces(1:nkernel)
       this%nkernel = nkernel
       this%base_kernel = one
       do i=1,nkernel
          this%base_kernel(i) = zero
       end do
       this%base_kernel = this%base_kernel / sqrt(one*(this%ncol-nkernel))
       
     end subroutine spkernel_set

       
    subroutine spkernel_Mxv(this,vec_in,vec_out,info,lun_err)
       implicit none
       class(spkernel), intent(inout) :: this
       real(kind=double), intent(in   ) :: vec_in(this%ncol)
       real(kind=double), intent(inout) :: vec_out(this%nrow)
       integer, optional, intent(inout) :: info
       integer, optional, intent(in   ) :: lun_err
       ! 
       integer :: i,j
       real(kind=double) :: rort,ddot

       if(present(info)) info=0
       vec_out=zero
       do i=1,this%nkernel
          j=this%indeces(i)
          vec_out(j)=vec_in(j)
       end do
       rort=ddot(this%ncol,this%base_kernel,1, vec_in,1)
       vec_out=vec_out + rort*this%base_kernel

     end subroutine spkernel_Mxv
            
            

     subroutine invert_jacobian_general(p1p0, &
          A_matrix, B_matrix, BT_matrix, C_matrix, D_matrix,&
          BequalC,&
          f_vec, g_vec, & ! rhs
          x_vec, y_vec, & ! solution and initial guess
          iter_newton,&
          tdpot,ode_inputs,&       
          lun_err,lun_out,lun_stat, &
          ctrl, &
          current_time_iteration,current_time,deltat, &
          info,&
          CPU)
       use SimpleMatrix
       use Matrix
       use BlockMatrix
       use ScalableMatrix
       use RankOneUpdate
       use SparseMatrix
       use StdSparsePrec
       use ScalableMatrix
       implicit none
       class(p1p0_space_discretization), target, intent(inout) :: p1p0
       type(spmat),target,       intent(inout) :: A_matrix
       type(spmat),target,       intent(inout) :: B_matrix
       type(spmat),target,       intent(inout) :: BT_matrix
       type(spmat),target,       intent(inout) :: C_matrix
       type(diagmat),target,     intent(inout) :: D_matrix
       logical,           intent(in   ) :: BequalC
       real(kind=double), intent(in   ) :: f_vec(A_matrix%nrow)
       real(kind=double), intent(in   ) :: g_vec(D_matrix%nrow)
       real(kind=double), intent(inout) :: x_vec(A_matrix%nrow)
       real(kind=double), intent(inout) :: y_vec(D_matrix%nrow)
       integer,           intent(in   ) :: iter_newton
       type(tdpotsys),    target,  intent(inout) :: tdpot
       type(odedata),     intent(in   ) :: ode_inputs
       integer,           intent(in   ) :: lun_err,lun_out,lun_stat
       type(CtrlPrm),     intent(inout) :: ctrl
       integer,           intent(in   ) :: current_time_iteration
       real(kind=double), intent(in   ) :: current_time
       real(kind=double), intent(inout) :: deltat
       integer,           intent(inout) :: info
       type(codeTim),     intent(inout) :: CPU    

       ! local
       logical :: rc,schur_AD_is_symmetric
       integer :: res
       integer :: i,j,ind
       integer :: info_prec
       integer :: ntdens, npot, nfull

       ! inversion cycle 
       logical :: try2invert,schur_JD_is_symmetric, schur_JA_is_symmetric

       type(input_solver) :: ctrl_uzawa

       ! broyden updates quantities
       integer :: iter_broyden
       integer :: info_broyden

       type(file) :: fmat
       character(len=256) :: fname,directory,tail,msg3,msg4
       type(spmat),   pointer :: matrix2prec
       ! prec vars
       class(abs_linop), pointer :: prec_E
       class(abs_linop), pointer :: prec_Schur
       class(abs_linop), pointer :: prec_final 
       !
       real(kind=double) :: dnrm2,esnorm
       real(kind=double) :: max
       integer :: imax

       character(len=70) :: str
       character(len=256) :: msg,msg1,msg2

       
       type(diagmat) :: inv_D_matrix
       type(diagmat), target:: minus_D
       type(diagmat) :: diag
       type(diagmat) :: MP_matrix
       type(diagmat) :: hatS_matrix
       real(kind=double) :: gamma,rmax
       real(kind=double),allocatable :: f_gamma(:)
       type(diagmat) :: inverse_D_matrix
       type(diagmat) :: inverse_hatS_matrix
       type(inverse), target :: inverse_A_gamma 
       type(stdprec), target :: prec_A_gamma
       class(abs_linop), pointer :: prec_for_A_gamma
       type(spmat) :: workspmat
       type(spmat), target :: A_gamma
       type(spmat),target :: BT_gamma
       type(input_prec):: ctrl_prec_augmented
       type(input_solver) :: ctrl_intern
       !
       ! set no error flag
       !
       info = 0
       tdpot%nrestart_invert_jacobian = 0

       !
       ! copy dimensions
       !
       ntdens = tdpot%ntdens
       npot   = tdpot%npot
       nfull  = tdpot%nfull

       call inv_D_matrix%init(lun_err,ntdens)

       !
       ! solve linear system J_F(k) = -F(k)
       ! We use a cycle to try diffrent approaches if required.
       ! Use subroutine handle_failure_invert_jacobian
       ! switch to .True. . Otherwise is set .False.
       !
       try2invert = .True.
       !
       do while ( try2invert  .and. &
            (tdpot%nrestart_invert_jacobian .le. &
            ctrl%max_nrestart_invert_jacobian ) )
          try2invert = .False.
          !
          ! select linear solver strategy 
          ! - reduced or full
          ! - preconditioner to be used
          !
          ! Used only at first attempt, use 
          ! set controls in subroutine 
          ! handle_failure_invert_jacobian
          !
          if ( tdpot%nrestart_invert_jacobian .eq. 0) then
             !
             ! selcet method and preconditioner
             !
             call select_inversion_approach_general(lun_err,&
                  D_matrix,&
                  p1p0,&
                  ctrl,info)
             if (info .ne. 0) return
             !
             ! set flag ctrl%build_prec
             !
             call select_preconditioner_building(&
                  tdpot,&
                  iter_newton,&
                  ctrl)
          end if


          ! info state before linear system solution
          !
          if ( ctrl%info_newton .eq. 2 ) then
             !
             ! Print information on linear solver strategy
             !
             write(lun_out,*) tdpot%nrestart_invert_jacobian
             if ( tdpot%nrestart_invert_jacobian .gt. 0) write(lun_out,*) ctrl%sep('RESTARTED')
             select case (ctrl%reduced_jacobian)
             case (1)
                write(msg3,*) 'REDUCED'
             case (0)
                write(msg3,*) 'FULL '
             end select

             select case (ctrl%prec_newton)
             case (1)
                write(msg4,*) 'ICHOL A'
             case (2)
                write(msg4,*) 'ICHOL (A\D2) '
             case (3)
                write(msg4,*) 'MIXED '
             case (4)
                write(msg4,*) 'TRIANG '
             end select
                
             write(msg1,'(I2,a,a,1x,I2,1x,a,1x,a,1x,a,1x,a,1x,a,1x,I2,a,1x,I2,a,1x,e8.2)') &
                  iter_newton+1,' | CTRL  : ','build prec=',ctrl%build_prec,&
                  'LIN. SYS. :',etb(msg3),&
                  'PREC =',etb(msg4),&
                  'diag. scale =',ctrl%id_diagscale,&
                  'nbfgs =',ctrl%max_bfgs,&
                  'tol=',ctrl%ctrl_solver%tol_sol
             write(lun_out,*) etb(msg1)
             write(lun_stat,*) etb(msg1)
          end if

          !
          ! used the selected method to solve 
          !     J_F inc = - F = rhs
          !

          select case ( ctrl%reduced_jacobian ) 
          !
          ! solve reduce 
          !
       case (1)
          !
          ! solve_reduced_jacobian resolving with inverse of D
          !
          

          p1p0%scr_ntdens = one/D_matrix%diagonal        
          call inv_D_matrix%set(lun_err,p1p0%scr_ntdens)
          if ( ctrl%threshold_tdens>1e-20 ) then
             do i=1,tdpot%ntdens_off
                j=tdpot%inactive_tdens(i)
                inv_D_matrix%diagonal(j) = zero
             end do
          end if


          !
          ! Prepare linear system
          !  assembly system matrix 
          !     M = A + B^T D^{-1} C 
          schur_AD_is_symmetric = .true.
          call p1p0%jacobian_reduced%set(&
               lun_err,&
               .true.,&
               one,&
               one,&
               A_matrix,&
               B_matrix,&
               C_matrix,&
               inv_D_matrix%diagonal)  
          p1p0%jacobian_reduced%is_symmetric = .true.
          
          !
          ! assembly rhs of the reduced system 
          ! rhs=f + BT D^{-1} g
          !
          call inv_D_matrix%Mxv(g_vec, p1p0%scr_ntdens) ! p1p0%scr_ntdens =  p1p0%inv_D_matrix * vec_g
          call B_matrix%MTxv(p1p0%scr_ntdens, p1p0%scr_npot) 
          p1p0%rhs_reduced = f_vec +  p1p0%scr_npot
          
          if ( tdpot%ntdens_on .ne. tdpot%ntdens) then
             rmax=zero
             do i=1,tdpot%npot_off
                j = tdpot%inactive_pot(i)
                rmax=max(rmax,abs(p1p0%rhs_reduced(j)))
                p1p0%rhs_reduced(j)=zero
             end do
             write(*,*) 'max rhs reduced ',rmax 
          end if


          !
          ! assembly matrix A+deltat*BTDC
          ! passagge required only for building preconditioner
          !
          if ( (ctrl%build_prec  .ne. 0) .and. &
               (ctrl%prec_newton .eq. 2) ) then 
             call CPU%PREC%set('start')

             !
             ! build A + BT D^{-1} C              
             ! 
             ! 1 - compute BT D^{-1} C
             schur_JD_is_symmetric=.true.
             
             call p1p0%BTDC_matrix%mult_MDN(lun_err,&
                  BT_matrix,C_matrix,&
                  100,100*tdpot%npot,&
                  inv_D_matrix%diagonal)!,BT_matrix)
             

             ! 2 - sum A + BT D^{-1} C
             !call aMpN(one,A_matrix, p1p0%BTDC_matrix, p1p0%stiff2BTDC)

             !
             ! build A + deltat BT D1 D2^{-1} C              
             ! 
             do i=1,A_matrix%nrow
                do j=A_matrix%ia(i),A_matrix%ia(i+1)-1
                   p1p0%BTDC_matrix%coeff(p1p0%stiff2BTDC(j)) = &
                        p1p0%BTDC_matrix%coeff(p1p0%stiff2BTDC(j)) +&
                        A_matrix%coeff(j)
                end do
             end do

             ! 3 - set properties
             p1p0%BTDC_matrix%is_symmetric = schur_JD_is_symmetric

             if ( tdpot%ntdens_on .ne. tdpot%ntdens ) then
                ! freeze potential
                do i=1,tdpot%npot_off
                   call p1p0%BTDC_matrix%set_rowcol(tdpot%inactive_pot(i),zero)
                   p1p0%BTDC_matrix%coeff(p1p0%BTDC_matrix%idiag(tdpot%inactive_pot(i)))=one
                end do
             end if


             call CPU%PREC%set('stop')
             if ( ctrl%id_save_matrix > 1 ) then
                write(tail,'(I0.4,a,I0.4,a)') &
                     tdpot%time_iteration,'_',iter_newton, '.dat' 
                directory=etb('./output/linsys')
                fname=etb(etb(directory)//'/apbtdc_matrix_'//etb(tail))
                call fmat%init(lun_err,fname,10000,'out')
                call p1p0%BTDC_matrix%write(fmat%lun,'matlab')
                call fmat%kill(lun_err) 
             end if

          end if
          
!!$          !
!!$          ! scale linear system
!!$          !
!!$          if ( ctrl%id_diagscale .eq. 1 ) then
!!$             ! D=Diag(M) temporary stored in inv_sqrt_diagonal
!!$             call p1p0%jacobian_reduced%get_diagonal(p1p0%sqrt_diag)
!!$
!!$             ! D=Diag(M)^{-1/2}
!!$             p1p0%sqrt_diag=one/sqrt(p1p0%sqrt_diag)
!!$
!!$             !
!!$             ! scale by D system M x = b
!!$             !
!!$             call scale_system(lun_err, &
!!$                  p1p0%jacobian_reduced ,p1p0%rhs_reduced,x_vec,&
!!$                  p1p0%sqrt_diag)
!!$
!!$             !
!!$             ! scale matrix use to build prec
!!$             !
!!$             if ( ( ctrl%build_prec  .ge. 1 )  .and. &
!!$                  ( ctrl%prec_newton .eq. 2 ) ) then  
!!$                call CPU%PREC%set('start')
!!$                call p1p0%BTDC_matrix%diagonal_scale(lun_err,  &
!!$                     p1p0%sqrt_diag)
!!$                call CPU%PREC%set('stop')
!!$             end if
!!$          end if

          select case (ctrl%prec_newton)
          case ( 1 )
             matrix2prec => A_matrix
          case ( 2 )
             matrix2prec => p1p0%BTDC_matrix
          case default
             rc=IOerr(lun_err, err_val, 'invert_jacobian', &
                  ' control not defined for reduced jacobian'//&
                  ' ctrl%prec_newton = ', ctrl%prec_newton)
          end select
          ! lift diagonal 
          imax=1
          do i=1,npot
             j=matrix2prec%idiag(i)
             matrix2prec%coeff(j)=matrix2prec%coeff(j)+ctrl%relax2prec!*p1p0%diagonal_laplacian(j)
             if ( ode_inputs%rhs_integrated(i,2) > ode_inputs%rhs_integrated(imax,2) ) imax = i 
          end do
          

!!$          if ( ctrl%threshold_tdens>1e-20 ) then
!!$             do i=1,tdpot%npot_off
!!$                j=tdpot%inactive_pot(i)
!!$                call matrix2prec%set_rowcol(j,zero)
!!$                matrix2prec%coeff(matrix2prec%idiag(j))=p1p0%diagonal_laplacian(j)
!!$             end do
!!$          end if


          call CPU%PREC%set('start')
          call  assembly_stdprec(&
               lun_err,&
               matrix2prec,&
               ctrl%build_prec,&
               ctrl%ctrl_prec,&
               tdpot%info_prec,&
               p1p0%standard_prec_saved)
          call CPU%PREC%set('stop')

          if  ( tdpot%info_prec .ne. 0 ) then
             info=-2000
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' prec. construstion failed ', tdpot%info_prec)
          end if
          matrix2prec => null()

          prec_final => p1p0%standard_prec_saved
!!$          if ( ctrl%max_bfgs > 0) then
!!$             if ( current_time_iteration > ctrl%max_bfgs) then
!!$                
!!$                !
!!$                ! only at initial iteration compute stiff * V
!!$                !
!!$                if ( iter_newton .eq. 0 ) then
!!$                   do i=1,ctrl%max_bfgs 
!!$                      call A_matrix%Mxv(p1p0%matrix_V%coeff(:,i), p1p0%matrix_AV%coeff(:,i))
!!$                   end do
!!$                end if
!!$                call p1p0%bfgs_prec%set(&
!!$                     p1p0%matrix_V,&
!!$                     p1p0%matrix_PI,&
!!$                     p1p0%matrix_AV,&
!!$                     p1p0%standard_prec_saved)
!!$                prec_final => p1p0%bfgs_prec
!!$             else
!!$                prec_final => p1p0%standard_prec_saved
!!$             end if
!!$          end if
          

          !
          ! ortogonalized increment
          !
          call CPU%LINEAR_SOLVER%set('start')
          call linear_solver(&
               p1p0%jacobian_reduced,p1p0%rhs_reduced,x_vec,&
               tdpot%info_solver, &
               ctrl%ctrl_solver,&
               prec_left=prec_final,&
               aux=p1p0%aux_bicgstab,&
               ortogonalization_matrix=p1p0%near_kernel)
          call CPU%LINEAR_SOLVER%set('stop')


          if ( tdpot%info_solver%ierr .ne. 0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_err)
             rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_stat)
             info=-1
          end if

           

!!$          !
!!$          ! scale back 
!!$          !
!!$          if ( ctrl%id_diagscale .eq. 1  ) then
!!$             !
!!$             p1p0%sqrt_diag = one / p1p0%sqrt_diag
!!$
!!$             call scale_system(lun_err, &
!!$                  p1p0%jacobian_reduced ,p1p0%rhs_reduced,x_vec,&
!!$                  p1p0%sqrt_diag)
!!$          end if

          !
          ! 3 - get inc_tdens from inc_pot y==D^{-1} ( C * x - g) 
          !
          call C_matrix%Mxv(x_vec, p1p0%scr_ntdens)
          p1p0%scr_ntdens = p1p0%scr_ntdens - g_vec
          call inv_D_matrix%Mxv( p1p0%scr_ntdens, y_vec)

       case  (0) ! solve_full_jacobian
          if ( ctrl%debug .eq. 1 ) &
               write( lun_out, *) ' Full jacobian'

          if ( ctrl%prec_newton .eq. 1) then
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) &
                  ' Full jacobian + Block prec diag(A^-1, D2^1)'

             !
             ! Build_preconditioner
             !
             call CPU%PREC%set('start')
             if ( p1p0%nbroyden_update .eq. 0) then
                !
                ! Assembler a preconditiener for the jacobian
                !
!!$                call assembly_2x2_preconditioner(p1p0,&
!!$                 matrix_A, matrix_B, matrix_BT, matrix_C, matrix_D,&
!!$                 prec_full)

                if ( ctrl%debug .eq. 1 ) & 
                     write(lun_out,*) ' Assembly prec stiff',ctrl%build_prec
                call assembly_stdprec(lun_err,&
                     A_matrix,&
                     ctrl%build_prec,&
                     ctrl%ctrl_prec,&
                     info_prec,&
                     p1p0%standard_prec_saved)
                if ( info_prec .ne. 0) then
                   rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                        'prec. stiff construstion failed'//&
                        'info prec. = ',info_prec)
                   info = -2000
                end if

                !
                ! update component (2,2) of block prec.
                !
                if ( ctrl%debug .eq. 1 ) & 
                     write(lun_out,*) ' Assembly prec inv_D2'
                p1p0%scr_ntdens = one/D_matrix%diagonal
                call inv_D_matrix%set(lun_err,p1p0%scr_ntdens)

                !
                ! re-assign prec_full component
                !
                p1p0%prec_full%linops(1)%linop => p1p0%standard_prec_saved
                p1p0%prec_full%linops(2)%linop => p1p0%inv_D_matrix


                !
                ! redirect preconditoner used
                !
                prec_final => p1p0%prec_full
             else
                !
                ! Broyden updates techinque
                !
                !
                if ( iter_newton .eq. 0) then
                   !
                   ! assembly P0
                   !
                   !
                   ! update component (1,1) of block prec.
                   !
                   !if ( ctrl%debug .eq. 1 ) & 
                   write(lun_out,*) &
                        ' Assembly prec stiff for prec_broyden(0)' 
                   call  assembly_stdprec(lun_err,&
                        A_matrix,&
                        ctrl%build_prec,&
                        ctrl%ctrl_prec,&
                        info_prec,&
                        p1p0%standard_prec_saved)
                   if ( info .ne. 0) then
                      info=-2000
                      rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                           ' prec. stiff construstion failed'//&
                           'info prec. = ',info_prec)
                   end if


                   !
                   ! update component (2,2) of block prec.
                   !
                   if ( ctrl%debug .eq. 1 ) & 
                        write(lun_out,*) &
                        ' Assembly prec inv_D2 for prec_broyden(0)'
                    p1p0%scr_ntdens = one
                    call inv_D_matrix%set(lun_err,p1p0%scr_ntdens)!/p1p0%D2)


                   !
                   ! assign final prec.
                   !
                   p1p0%broyden_sequence(0)%linop => p1p0%prec_full
                   prec_final => p1p0%broyden_sequence(0)%linop
                end if
                !
                ! P_{iter} = P_{iter-1} + u_k w_k ^t
                !
                if ( (iter_newton .ge. 1) .and. &
                     (iter_newton .le. p1p0%nbroyden_update) ) then
                   !
                   ! store y_k &
                   ! ( can be removed we can pass y_k init update%set 
                   !
                   p1p0%scr_nfull(:) = &
                        (/p1p0%fnewton_pot,    p1p0%fnewton_tdens    /) - &
                        (/p1p0%fnewton_pot_old,p1p0%fnewton_tdens_old/)
                   !
                   !
                   !
                   if ( ctrl%debug .eq. 1 ) & 
                        write(lun_out,*) &
                        ' Assembly prec_broyden(',iter_newton,')'
                   call p1p0%broyden_updates(iter_newton)%set(lun_err,&
                        p1p0%broyden_sequence(iter_broyden)%linop,&! prec to update
                        (/x_vec,y_vec/),p1p0%scr_nfull,info_broyden)
                   if ( info .eq. 0) then
                      ! broyden update is formed properly
                      iter_broyden = iter_broyden + 1 
                      p1p0%broyden_sequence(iter_broyden)%linop => &
                           p1p0%broyden_updates(iter_newton)
                   else
                      rc = IOerr(lun_err, wrn_out, 'invert_jacobian', &
                           ' construstion broyden update failed'//&
                           ' info prec. = ',info_broyden)
                   end if
                   !
                   ! assign final prec. as the greater broyden update
                   !
                   prec_final => p1p0%broyden_sequence(iter_broyden)%linop
                end if

                if ( iter_newton .gt. p1p0%nbroyden_update) then
                   !
                   ! P_{k} = last rank-one-update
                   !
                   prec_final => p1p0%broyden_sequence(iter_broyden)%linop
                end if
             end if
             call CPU%PREC%set('stop')

             !
             ! 2-solve linear system M inc_pot = rhs_reduced
             !
             call CPU%LINEAR_SOLVER%set('start')
             p1p0%rhs_full=(/f_vec,g_vec/)
             p1p0%scr_nfull=(/x_vec,y_vec/)
             call linear_solver(&
                  p1p0%jacobian_full,p1p0%rhs_full,p1p0%scr_nfull,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,& 
                  prec_final,&
                  aux=p1p0%aux_bicgstab)
             call CPU%LINEAR_SOLVER%set('stop')
             x_vec=p1p0%scr_nfull(1:npot)
             y_vec=p1p0%scr_nfull(1+npot:nfull)

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if

          if ( ctrl%prec_newton .eq. 2) then
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' Full jacobian symmetrized'
             ! 
             ! solving (A B^T) = (I                    ) ( A            BT )
             !         (B G  )   (   (-deltat D1)^{-1} ) ( -deltatD1B   D2 )


            

             ! EFAMLB
             ! build prec_E
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_E'
             !
             ! update component (1,1) of block prec.
             !
             if ( ctrl%debug .eq. 1 ) & 
                  write(lun_out,*) ' Assembly prec stiff'
             call assembly_stdprec(lun_err,&
                  A_matrix,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
             prec_E => p1p0%standard_prec_saved

             if ( info_prec .ne. 0) then
                info = -2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     '  prec. stiff  failed '//&
                     'info prec. = ',info_prec)
             end if


             !
             ! build shur complement matrix schur_JD = - D - B diag(A)^-1 BT 
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_Schur | schur_JD = - (D + B diag(A)^-1 BT)'
             !
             ! assembly matrix  B diag(A)^{-1} BT
             !
             call A_matrix%get_diagonal(tdpot%scr_npot) 
             tdpot%scr_npot= one / tdpot%scr_npot
             call p1p0%BDBT%MULT_MDN(lun_err, B_matrix , BT_matrix, &
                  ntdens, 100*ntdens ,tdpot%scr_npot, B_matrix )
             
             do i = 1, tdpot%ntdens
                ind=p1p0%BDBT%idiag(i) 
                p1p0%BDBT%coeff(ind) = p1p0%BDBT%coeff(ind) + D_matrix%diagonal(i)
             end do
             p1p0%BDBT%coeff= -p1p0%BDBT%coeff

             !
             ! build_prec_shur
             !
             str=ctrl%ctrl_prec%prec_type
             ctrl%ctrl_prec%prec_type = 'IC'
             call tdpot%assembly_stdprec(lun_err,&
                  p1p0%BDBT,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%prec_schur)  
             ctrl%ctrl_prec%prec_type=str
             if ( info_prec .ne. 0) then
                info=-2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     ' prec. Schur  failed'//&
                     'info prec. = ',info_prec)
             end if

             prec_Schur =>  p1p0%prec_schur

             !
             ! set preconditioner
             !
             call p1p0%icprec_full%set(lun_err, p1p0%B_matrix, prec_E, prec_Schur)
             prec_final => p1p0%icprec_full

            
             !
             ! 2-solve linear system M inc_pot = rhs_reduced
             !
             call CPU%LINEAR_SOLVER%set('start')
             p1p0%rhs_full=(/f_vec,g_vec/)
             p1p0%scr_nfull=(/x_vec,y_vec/)
             call linear_solver(&
                  p1p0%jacobian_full,p1p0%rhs_full,p1p0%scr_nfull,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,&
                  prec_final,&
                  aux=p1p0%aux_bicgstab)
             call CPU%LINEAR_SOLVER%set('stop')

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if

          if ( ( ctrl%prec_newton .eq. 3) .or. &
               ( ctrl%prec_newton .eq. 4) )then 
             !
             ! solve mixed_constranied_prec or triangular
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' Full jacobian mixed constrained preconditioner'
             ! 
             ! solving (A     B^T)
             !         (C    -D  ) 
             !                     

             ! EFAMLB
             ! build prec_E
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_E'

             !
             ! update component (1,1) of block prec.
             !
             if ( ctrl%debug .eq. 1 ) & 
                  write(lun_out,*) ' Assembly prec stiff'
             call assembly_stdprec(lun_err,&
                  A_matrix,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
             prec_E => p1p0%standard_prec_saved

             if ( info_prec .ne. 0) then
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     '  prec. stiff  failed '//&
                     'info prec. = ',info_prec)
                info=-2000
             end if


             !
             ! build shur complement matrix  C diag(A)^{-1} BT - matrixD
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) & 
                  'prec_Schur | assembler D2 + deltat D1Cdiag(A)^{-1} BT'

             !
             ! assembly matrix C diag(A)^{-1} BT
             !
             call A_matrix%get_diagonal(tdpot%scr_npot) 
             tdpot%scr_npot= one / tdpot%scr_npot
             call p1p0%BDBT%MULT_MDN(lun_err, p1p0%B_matrix , p1p0%BT_matrix, &
                  ntdens, 100*ntdens ,tdpot%scr_npot, p1p0%B_matrix )


             !
             ! assembly matrix deltat D1 C diag(A)^{-1} BT + D2
             !
             tdpot%scr_ntdens=deltat*p1p0%D1
             call p1p0%BDBT%DxM( lun_err, tdpot%scr_ntdens)

             do i = 1, tdpot%ntdens
                ind=p1p0%BDBT%idiag(i) 
                p1p0%BDBT%coeff(ind) = p1p0%BDBT%coeff(ind) + p1p0%D2(i)
             end do

             if ( ctrl%id_save_matrix > 1 ) then
                write(tail,'(I0.4,a,I0.4,a)') &
                     tdpot%time_iteration,'_',iter_newton, '.dat' 
                directory=etb('./output/linsys')
                fname=etb(etb(directory)//'/bdbt_matrix_'//etb(tail))
                call fmat%init(lun_err,fname,10000,'out')
                call p1p0%BDBT%write(fmat%lun,'matlab')
                call fmat%kill(lun_err) 
             end if


             !
             ! build_prec_shur
             !
             if ( ctrl%debug .eq. 1 ) &
                  write( lun_out, *) ' prec_Schur | assembler deltaD1 * B P(A)^-1 BT+D2 '

             str=ctrl%ctrl_prec%prec_type
             ctrl%ctrl_prec%prec_type = 'ILU'
             call tdpot%assembly_stdprec(lun_err,&
                  p1p0%BDBT,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%prec_schur)  
             ctrl%ctrl_prec%prec_type=str

             if ( info_prec .ne. 0) then
                info=-2000
                rc=IOerr(lun_err, wrn_inp, 'invert_jacobian', &
                     ' prec. Schur  failed'//&
                     'info prec. = ',info_prec)
             end if

             prec_Schur =>  p1p0%prec_schur

             !
             ! set preconditioner
             !
             tdpot%scr_ntdens = deltat*p1p0%D1
             select case (ctrl%prec_newton)
             case (4)
                call p1p0%triang_prec%set(lun_err,&
                     p1p0%B_matrix, prec_E, prec_Schur, tdpot%scr_ntdens)
                prec_final => p1p0%triang_prec
             case (3)                   
                call p1p0%mix_icprec_full%set(lun_err,&
                     p1p0%B_matrix, prec_E, prec_Schur, tdpot%scr_ntdens)
                prec_final => p1p0%mix_icprec_full
             case default
                rc=IOerr(lun_err, err_val, 'invert_jacobian', &
                     ' control not defined for full jacobian'//&
                     ' ctrl%prec_newton = ', ctrl%prec_newton)
             end select


             !----------------------------------------------------------
             !
             ! 2-solve linear system J inc_full = -F = rhs
             !
             !inc_full  = zero
             !inc_pot   = zero
             !inc_tdens = zero

             !
             ! only bicgstab this configuration
             !
             ctrl%ctrl_solver%scheme = 'BICGSTAB'
             p1p0%rhs_full=(/f_vec,g_vec/)
             p1p0%scr_nfull=(/x_vec,y_vec/)
             call CPU%LINEAR_SOLVER%set('start')
             call linear_solver(&
                  p1p0%jacobian_full,p1p0%rhs_full,p1p0%scr_nfull,&
                  tdpot%info_solver,&
                  ctrl%ctrl_solver,&
                  prec_left=prec_final,&
                  aux=p1p0%aux_bicgstab,&
                  ortogonalization_matrix=p1p0%near_kernel_full)
             call CPU%LINEAR_SOLVER%set('stop')
             x_vec=p1p0%scr_nfull(1:npot)
             y_vec=p1p0%scr_nfull(1+npot:nfull)
             ctrl%ctrl_solver%scheme = ctrl%ctrl_solver_original%scheme

             if ( tdpot%info_solver%ierr .ne. 0) then
                rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' linear solver failure ',tdpot%info_solver%ierr)
                call tdpot%info_solver%info(lun_err)
                call tdpot%info_solver%info(lun_stat)
                info=-1
             end if
          end if
       case (3) ! solve_inexact_jacobian
          call assembly_stdprec(lun_err,&
                  p1p0%stiff,&
                  ctrl%build_prec,&
                  ctrl%ctrl_prec,&
                  info_prec,&
                  p1p0%standard_prec_saved)
          call CPU%LINEAR_SOLVER%set('start')
          
          p1p0%scr_npot = ode_inputs%rhs_integrated(:,2) ! copy rhs 
          x_vec = tdpot%pot     
          call linear_solver(&
               p1p0%stiff,p1p0%scr_npot,tdpot%pot,&
               tdpot%info_solver,&
               ctrl%ctrl_solver,&
               prec_left=p1p0%standard_prec_saved,&
               aux=p1p0%aux_bicgstab)          
          x_vec =  tdpot%pot - x_vec

          call tdpot%build_nrm_grad_vars(p1p0,lun_err)
          call p1p0%build_nrm_grad_dyn(tdpot,2.0d0)
          
 
          call CPU%LINEAR_SOLVER%set('stop')
          if ( ctrl%nbroyden > 0) then
             if (iter_newton .eq. 0) then
                call p1p0%scr_diagmat_ntdens%set( &
                     lun_err,one / one + deltat*( p1p0%nrm_grad_dyn + one ) )
                call p1p0%scr_diagmat_ntdens%Mxv(&
                     - p1p0%fnewton_tdens , x_vec )
             else
                p1p0%scr_ntdens = p1p0%fnewton_tdens - p1p0%fnewton_tdens_old
                esnorm=one/dnrm2(ntdens,x_vec,1)**2
                if ( iter_newton .eq. 1 ) then
                   call p1p0%rankone_update(iter_newton)%set(lun_err,&
                        p1p0%scr_diagmat_ntdens,&! prec to update
                        p1p0%fnewton_tdens, &
                        x_vec,.False., &
                        info_broyden,esnorm)
                else
                   call p1p0%rankone_update(iter_newton)%set(lun_err,&
                        p1p0%rankone_update(iter_newton-1),&! prec to update
                        p1p0%fnewton_tdens, &
                        x_vec,.False.,  info_broyden,&
                        esnorm)
                end if

                call p1p0%rankone_update(iter_newton)%Mxv(&
                     - p1p0%fnewton_tdens , x_vec )
             end if
          else
             do i=1,ntdens
                p1p0%scr_ntdens(i) =  &
                     one / &
                     (one/p1p0%grid_tdens%size_cell(i)  &
                     - deltat * ( p1p0%nrm_grad_dyn(i) + one)   &
                     + 2*deltat * tdpot%tdens(i)/p1p0%grid_tdens%size_cell(i) * p1p0%nrm_grad_dyn(i))
             end do
             do i=1,ntdens
                x_vec(i) = &
                     -p1p0%fnewton_tdens(i) * p1p0%scr_ntdens(i)
             end do
          end if

       case (4) ! solve_augemented_jacobian
          if (ode_inputs%id_ode.eq. 1)  then
             write(*,*) 'only B=C case' 
             stop
          end if
          !
          ! allocate temporary varibles
          !
          call MP_matrix%init(lun_err, ntdens)
          allocate(f_gamma(npot))
          call hatS_matrix%init(lun_err, ntdens)

          !
          ! set relaxation
          ! 
          gamma = ctrl%gamma / deltat
          p1p0%scr_ntdens=one!p1p0%grid_tdens%size_cell
          call MP_matrix%set(lun_err,p1p0%scr_ntdens)          
          call assembly_augmented_lagrangian_matrices(&
               A_matrix, B_matrix, BT_matrix, C_matrix, D_matrix,BequalC,&
               f_vec, g_vec, &
               gamma,MP_matrix, ctrl%id_hatS,&
               A_gamma, BT_gamma, hatS_matrix, &
               f_gamma,  &
               p1p0%stiff2BTDC,&
               ctrl%debug)
          
          minus_D = D_matrix
          minus_D%diagonal = -minus_D%diagonal

          !
          ! reassing jacobian matrix
          !
          p1p0%augmented_jacobian%mats(1)%mat => A_gamma
          p1p0%augmented_jacobian%mats(2)%mat => BT_gamma
          p1p0%augmented_jacobian%mats(3)%mat => C_matrix
          p1p0%augmented_jacobian%mats(4)%mat => minus_D
          p1p0%augmented_jacobian%is_symmetric = BequalC

          !A_gamma%coeff(1)=huge
          
          ! inverse block 2,2
          call inverse_hatS_matrix%init(lun_err,ntdens)
          p1p0%scr_ntdens = one/ hatS_matrix%diagonal
          call inverse_hatS_matrix%set(lun_err,p1p0%scr_ntdens)

          ! inverse at block 1,1
          workspmat = A_gamma
          ! lift diagonal 
          do i=1,npot
             j=workspmat%idiag(i)
             workspmat%coeff(j)=workspmat%coeff(j)+ctrl%relax2prec
          end do
          workspmat%coeff(1)=huge
          workspmat%is_symmetric = BequalC
          if(.not. BequalC) stop

          select case ( ctrl%ctrl_prec%prec_type) 
          case('krylov')  
             !
             ! use krylov solver to apply inverse
             !
             !

             ! prec_A_gamma ~=(matrix2prec)  ^{-1}
             ! matrix2prec ~= matrix_gamma
             !
             ctrl_prec_augmented =  ctrl%ctrl_prec 
             ctrl_prec_augmented%prec_type = 'IC'
             call prec_A_gamma%init(lun_err, info_prec,&
                  ctrl_prec_augmented, workspmat%nrow, workspmat) 
             prec_A_gamma%is_symmetric = .true.
             

             ctrl_intern=ctrl%ctrl_solver
             ctrl_intern%scheme='MINRES'
             ctrl_intern%iprt=0
             ctrl_intern%iexit=0
             ctrl_intern%iort=1
             ctrl_intern%tol_sol=&
                  1e-2*ctrl%ctrl_solver_original%tol_sol
!!$                  max(&
!!$                  1e-2*ctrl%ctrl_solver_original%tol_sol,&
!!$                  1e-4*ctrl%ctrl_solver%tol_sol)
             call inverse_A_gamma%init(lun_err,npot)
             call inverse_A_gamma%set(A_gamma, &
                  ctrl_intern,prec_left=prec_A_gamma,&
                  ortogonalization_matrix=p1p0%near_kernel)

             prec_for_A_gamma => inverse_A_gamma

            
             

          case default
             ! prec_A_gamma ~=(matrix2prec)  ^{-1}
             ! matrix2prec ~= matrix_gamma
             !
             call prec_A_gamma%init(lun_err, info_prec,&
                  ctrl%ctrl_prec, workspmat%nrow, workspmat)  
             
             prec_for_A_gamma => prec_A_gamma
             
          end select
          

          


          !
          ! set augmented lagrangian preconditoner
          !
          call p1p0%triangular_augmented_prec%set(lun_err,&
               prec_for_A_gamma , p1p0%B_matrix,inverse_hatS_matrix)
          if (info.ne.0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                     ' triangular augumented prec failure ')
              info=-1
           end if

          


          call CPU%LINEAR_SOLVER%set('start') 
          p1p0%rhs_full=(/f_gamma,g_vec/)
          p1p0%scr_nfull=(/x_vec,y_vec/)
          call linear_solver(&
               p1p0%augmented_jacobian,p1p0%rhs_full,p1p0%scr_nfull,&
               tdpot%info_solver,&
               ctrl%ctrl_solver,&
               prec_left=p1p0%triangular_augmented_prec,&
               aux=p1p0%aux_bicgstab)
          if (  tdpot%info_solver%ierr .ne. 0) then
             select case ( ctrl%ctrl_prec%prec_type ) 
             case ('kryvol') 
                call inverse_A_gamma%info_solver%info(lun_err)
             end select
          end if
          call CPU%LINEAR_SOLVER%set('stop')
          x_vec=p1p0%scr_nfull(1:npot)
          y_vec=p1p0%scr_nfull(1+npot:nfull)

          
          call MP_matrix%kill(lun_err)
          deallocate(f_gamma)
          call hatS_matrix%kill(lun_err)
          call A_gamma%kill(lun_err)
          call BT_gamma%kill(lun_err)
                    

          if ( tdpot%info_solver%ierr .ne. 0) then
             rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_err)
             rc=IOerr(lun_stat, wrn_val, 'invert_jacobian', &
                  ' linear solver failure ',tdpot%info_solver%ierr)
             call tdpot%info_solver%info(lun_stat)
             info=-1
          end if
          
       case ( 5 ) ! uzawa method
          !
          ! set A^{-1}
          !
          call assembly_stdprec(lun_err,&
               A_matrix,&
               ctrl%build_prec,&
               ctrl%ctrl_prec,&
               info_prec,&
               p1p0%standard_prec_saved)
          call p1p0%inverse_stiff%set(A_matrix,ctrl%ctrl_solver,&
               p1p0%standard_prec_saved)
          

          ctrl_uzawa = ctrl%ctrl_solver
          ctrl_uzawa%imax=10000
          ctrl_uzawa%iprt=1
          call  uzawa_arrow_hurwics(&
               A_matrix,&
               BT_matrix,&
               C_matrix,&
               D_matrix,&
               f_vec,g_vec,&
               x_vec,y_vec,&
               ctrl%alpha_uzawa, ctrl%omega_uzawa,ctrl_uzawa,&
               tdpot%info_solver,&
               p1p0%inverse_stiff,aux=p1p0%aux_bicgstab)        
          call tdpot%info_solver%info(lun_out)
          info = tdpot%info_solver%ierr
                    
       end select
       if ( ctrl%info_newton .gt. 0) then
          call tdpot%info_solver%info2str(str) 
          write(msg,'(I2,a,a)') iter_newton+1,' | LINSOL: ', etb(str)
          write(lun_out,*) etb(msg)
          write(lun_stat,*) etb(msg)
       end if

       !
       ! try to handle inversion failure with different
       ! linear solver procedure. 
       !
       if ( info .ne. 0) then
          call handle_failure_invert_jacobian(info,&
               iter_newton,&
               p1p0, &
               tdpot,&       
               ctrl, &
               try2invert)
          if ( try2invert ) then
             tdpot%nrestart_invert_jacobian = &
                  tdpot%nrestart_invert_jacobian  + 1 
             info = 0
          else
             return
          end if
       else
          !
          ! everything is OK, store linear system informations
          !
          if ( ctrl%build_prec .eq. 1) then
             tdpot%iter_last_prec = tdpot%info_solver%iter
             tdpot%iter_newton_last_prec = iter_newton
          end if
          
          tdpot%total_number_linear_system = &
               tdpot%total_number_linear_system +1
          tdpot%total_iterations_linear_system =  &
               tdpot%total_iterations_linear_system + &
               tdpot%info_solver%iter
          tdpot%res_elliptic = tdpot%info_solver%resreal
          
       end if
       
    end do

    !
    ! restore linear solver controls
    !
    ctrl%ctrl_solver = ctrl%ctrl_solver_original

    call inv_D_matrix%kill(6)

  contains
    !>------------------------------------------------
    !> Procedure to select inversion approach
    !> Reduced or full
    !<--------------------------------------------------
    subroutine select_inversion_approach_general(lun_err,&
         D_matrix,&
         p1p0,&
         ctrl,info)
      use Globals
      implicit none
      !vars
      integer,                         intent(in   )  :: lun_err
      type(diagmat),                   intent(in   )  :: D_matrix
      type(p1p0_space_discretization), intent(in   )  :: p1p0
      type(CtrlPrm),                   intent(inout)  :: ctrl
      integer,                         intent(inout)  :: info
      ! local
      logical :: rc
      integer :: res
      

      !
      ! reduced approach
      !
      if ( ctrl%newton_method/10 .eq. 1 ) then       
         if ( minval(D_matrix%diagonal) > zero)  then
            ctrl%reduced_jacobian = 1
            write (*,*) minval(D_matrix%diagonal)
         else
            if  ( ctrl%ctrl_solver%scheme .eq. 'PCG') then
               rc=IOerr(lun_err, wrn_val, 'select_inversion_strategy', &
                     ' PCG + min (D)> zero ')
               info = -1000
               return
            else
               !
               ! try to solve it with MINRES OR BIGSTAB
               !
               ctrl%reduced_jacobian = 1  
               info=-1000
               return
            end if
         end if
         ctrl%prec_newton = mod(ctrl%newton_method,10)
      end if


      !
      ! full approach
      !
      if ( ctrl%newton_method/10 .eq. 0 ) then       
         ctrl%reduced_jacobian = 0
         ctrl%prec_newton = mod(ctrl%newton_method,10) 
      end if

      !
      ! full approach
      !
      if ( ctrl%newton_method/10 .eq. 3 ) then       
         ctrl%reduced_jacobian = 3
         ctrl%prec_newton = 1
      end if

      !
      ! ibrid approach
      !
      if ( ctrl%newton_method/10 .eq. 2 ) then       
         if ( minval(D_matrix%diagonal) > zero)  then
            !
            ! D2^{-1} exist end is positive 
            !
            ctrl%reduced_jacobian = 1
            select case ( mod(ctrl%newton_method,10))
            case (1) 
               ctrl%prec_newton = 1
            case (2) 
               ctrl%prec_newton = 2
            case (3) 
               ctrl%prec_newton = 1
            case (4) 
               ctrl%prec_newton = 2
            end  select
         else
            !
            ! if PCG is used, switch to solution of full system
            !
!!$            if  ( ctrl%ctrl_solver%scheme .ne. 'PCG') then
!!$               ctrl%reduced_jacobian = 1
!!$               select case ( mod(ctrl%newton_method,10))
!!$               case (1) 
!!$                  ctrl%prec_newton = 1 ! reduced + P~=A^{-1}
!!$               case (2) 
!!$                  ctrl%prec_newton = 2 ! reduced + P~=M^{-1}
!!$               case (3) 
!!$                  ctrl%prec_newton = 1 ! reduced + P~=A^{-1}
!!$               case (4) 
!!$                  ctrl%prec_newton = 2 ! reduced + P~=M^{-1}
!!$               end  select
!!$            else
            ctrl%reduced_jacobian = 0
            select case ( mod(ctrl%newton_method,10))
            case (1) 
               ctrl%prec_newton = 3
            case (2) 
               ctrl%prec_newton = 3
            case (3) 
               ctrl%prec_newton = 4
            case (4) 
               ctrl%prec_newton = 4
            end  select
            !end if
         end if
      end if
    end subroutine select_inversion_approach_general

    !
    ! Subroutine that, according to the inversion strategy
    ! select which preconditoner must be used.
    ! It sets flag
    ! build_prec  = 0/1
    ! build_tunig = 0/1
    !
    subroutine select_preconditioner_building(&
         this,&
         iter_newton,&
         ctrl)

      type(tdpotsys), intent(in   )  :: this
      integer,        intent(in   )  :: iter_newton
      type(CtrlPrm),  intent(inout)  :: ctrl

      ! local
      logical :: rc
      integer :: res
             
      !
      ! globals approach 0,1,2,3,4 
      !
      call this%set_threshold(ctrl)

      !
      ! newton specific appoach approach
      !
      select case (ctrl%id_buffer_prec)
      case (5)
         !
         ! build only a first newton iteration
         !
         ctrl%build_prec = 0
         if (iter_newton .eq. 0) ctrl%build_prec = 1
      case (6)
         !
         ! build at first newton iteration 
         ! and set as reference the iteration required
         !
         ctrl%build_prec = 0
         if (iter_newton .eq. 0)  then
            ctrl%build_prec = 1
         else if ( iter_newton .eq. 1) then
            ctrl%build_prec = 0
         else   
            ctrl%threshold = this%sequence_info_solver(1)%iter
            if ( this%sequence_info_solver(iter_newton-1)%iter > & 
                 int(ctrl%prec_growth  * ctrl%threshold ) ) then
               ctrl%build_prec=1
               ctrl%build_tuning=1
            else
               ctrl%build_prec   = 0
               ctrl%build_tuning = 0     
            end if
         end if
      end select

    end subroutine select_preconditioner_building

    !> -----------------------------------------------------
    !> Subroutine used to reset inversion controls
    !> (strategy, preconditioner,etc,)
    !> in case of linear solver failure
    !>------------------------------------------------------
!!$    subroutine build_2x2_prec(matrix_A, matrixB,matrixBT, matrix_C, matrix_D, p1p0, final_prec)
!!$
!!$
!!$    end subroutine build_2x2_prec

    !> -----------------------------------------------------
    !> Subroutine used to reset inversion controls
    !> (strategy, preconditioner,etc,)
    !> in case of linear solver failure
    !>------------------------------------------------------
    subroutine handle_failure_invert_jacobian(info,&
         iter_newton,&
         p1p0, &
         tdpot,&       
         ctrl, &
         try2invert)
      implicit none
      integer,                         intent(in   ) :: info
      integer,                         intent(in   ) :: iter_newton
      type(p1p0_space_discretization), intent(inout) :: p1p0
      type(tdpotsys),                  intent(inout) :: tdpot
      type(CtrlPrm),                   intent(inout) :: ctrl
      logical,                         intent(inout) :: try2invert

      try2invert = .False.
      if ( ( info .eq. -1) .and. &
           ( ctrl%newton_method/10 .eq. 2 ) .and. &
           ( tdpot%nrestart_invert_jacobian .eq. 0 ) ) then
         ctrl%ctrl_solver%scheme='BICGSTAB'
         ctrl%reduced_jacobian = 0
         ctrl%prec_newton = mod(ctrl%newton_method,10)
         try2invert = .True.
      end if
      
      !
      ! if we ctrl%build_prec
      !
      if ( ctrl%build_prec .eq. 0) then
         if ( tdpot%info_solver%iter > &
              ctrl%ctrl_solver_original%imax ) then
            ctrl%build_prec = 1
            try2invert = .True.
         end if
      end if

    end subroutine handle_failure_invert_jacobian

    
    


  end subroutine invert_jacobian_general
      
  subroutine assembly_augmented_lagrangian_matrices(&
         A_matrix, B_matrix, BT_matrix, C_matrix, D_matrix,BequalC,&
         f_vec, g_vec, &
         gamma,MP_matrix,id_hatS, &
         A_gamma, BT_gamma, hatS_matrix, &
         f_gamma,  &
         coeffA2coeffAgamma,&
         debug)
      implicit none
      ! input of saddle point problem
      type(spmat), intent(in) :: A_matrix
      type(spmat), intent(in) :: B_matrix
      type(spmat), intent(inout) :: BT_matrix
      type(spmat), intent(in) :: C_matrix
      type(diagmat), intent(in) :: D_matrix
      logical,       intent(in) :: BequalC
      real(kind=double) , intent(in ) :: f_vec(A_matrix%nrow)
      real(kind=double) , intent(in ) :: g_vec(D_matrix%nrow)
      ! gamma parameter
      real(kind=double), intent(in) :: gamma
      type(diagmat), intent(in) :: Mp_matrix
      integer,     intent(in) :: id_hatS
      ! augemented lagrangrian component
      type(spmat), intent(inout) :: A_gamma
      type(spmat), intent(inout) :: BT_gamma
      type(diagmat), intent(inout) :: hatS_matrix
      real(kind=double) , intent(inout) :: f_gamma(A_matrix%nrow)
      ! extra help
      integer,     intent(in   ) :: coeffA2coeffAgamma(A_matrix%nterm)
      integer,     intent(in   ) :: debug

      !
      ! local
      ! 
      integer :: i
      integer :: na, nd,lun_err=6
      real(kind=double), allocatable :: scr_A(:)
      real(kind=double), allocatable :: scr_D(:)
      type(diagmat) :: W_matrix
      type(diagmat) :: inv_W_matrix
      
      na=A_matrix%nrow
      nd=D_matrix%nrow

      allocate(scr_A(na),scr_D(nd))

      
      !
      ! W=Mp+gamma * D
      ! 
      scr_D = MP_matrix%diagonal + gamma * D_matrix%diagonal 
      call W_matrix%init(lun_err, nd)
      call inv_W_matrix%init(lun_err, nd)
      call W_matrix%set(lun_err,scr_D)      
      call inv_W_matrix%set(lun_err,one/scr_D)

      !
      ! hatS defition 
      !
      select case ( id_hatS) 
      case (1)
         !
         ! ! hatS = -one/gamma W ( Remark 3 pag.14) 
         !
         scr_D = - ( one / gamma ) *  W_matrix%diagonal
      case (2)
         !
         ! ! hatS = -one/gamma M_p - D ( pag.15) 
         !
         scr_D = - ( one / gamma ) *  MP_matrix%diagonal - D_matrix%diagonal
      end select
      call hatS_matrix%set(6,scr_D)
      
      if (debug >0) then
         write(*,'(a,1pe9.2,a,2(1pe9.2), a, 2(1pe9.2))') &
              ' gamma ',gamma
         write(*,'(a, 2(1pe9.2))') &
              'D     min max ',minval(D_matrix%diagonal ), maxval(D_matrix%diagonal )
         write(*,'(a, 2(1pe9.2))') &
              'MP    min max ',minval(MP_matrix%diagonal ), maxval(MP_matrix%diagonal )
         write(*,'(a, 2(1pe9.2))') &
              'Hat S min max ',minval(hatS_matrix%diagonal), maxval(hatS_matrix%diagonal)
      end if

      !
      ! compute new f_gamma = f+gamma*BT W^{-1} g
      ! 
      !write(*,*) 'f_gamma'
      call inv_W_matrix%Mxv(g_vec, scr_D)
      call BT_matrix%Mxv( scr_D, scr_A)
      f_gamma = f_vec + gamma * scr_A



      ! BT_gamma = BT - gamma BT W^{-1} D = BT( 1 - gamma W^{-1} D)
      !write(*,*) 'BT_gamma'
      BT_gamma= BT_matrix
      do i=1,nd
         scr_D(i) = &
              one - & 
              gamma * &
              inv_W_matrix%diagonal(i)* &
              D_matrix%diagonal(i)
      end do
      call BT_gamma%MxD(lun_err,scr_D)


      !
      ! build A_gamma = A +  gamma BT W ^{-1} C 
      !
      !write(*,*) 'A_gamma'
      scr_D = gamma * inv_W_matrix%diagonal
      call A_gamma%mult_MDN(lun_err,&
           BT_matrix,C_matrix,&
           100,100*na,&
           scr_D)
      call A_gamma%aMpN(one, A_matrix,  coeffA2coeffAgamma)
      A_gamma%is_symmetric = BequalC

      !
      ! free memory
      !
      call W_matrix%kill(6)
      call inv_W_matrix%kill(6)
      deallocate(scr_A,scr_D)


  end subroutine assembly_augmented_lagrangian_matrices



!!$    !
!!$    ! set augmented lagrangian preconditoner
!!$    !
!!$    call p1p0%triangular_augmented_prec%set(lun_err,&
!!$         A_gamma, &
!!$         C_matrix,hatS_matrix,info)
!!$    if (info.ne.0) then
!!$       rc=IOerr(lun_err, wrn_val, 'invert_jacobian', &
!!$            ' triangular augumented prec failure ')
!!$       info=-1
!!$    end if
!!$
!!$          
!!$    
!!$    call CPU%LINEAR_SOLVER%set('start') 
!!$    call ctrl%ctrl_solver%info(6)
!!$    scr(1:na)=f_gamma
!!$    scr(na+1;na+nd)=g_vec
!!$
!!$    sol=(/x_vec, y_vec/)
!!$    call linear_solver(&
!!$         p1p0%augmented_jacobian,scr,sol,&
!!$         tdpot%info_solver,&
!!$         ctrl%ctrl_solver,&
!!$         triangular_augmented_prec,&
!!$         aux=aux_linear_solver)      
!!$    call triangular_augmented_prec%inverse_A%info(6)
!!$
!!$  end subroutine augmented_lagrangian_preconditioner

   !>--------------------------------------------------------------------
  !> Static Constructor 
  !> (public procedure for type p1p0_space_discretization)
  !> 
  !> usage: call var%init(&
  !>                lun_err,lun_out,lun_stat,& 
  !>                ctrl,&
  !>                grid_tdens, grid_pot)
  !> where:
  !> \param[in] lun_err    -> integer. I/O unit for error message
  !> \param[in] lun_out    -> integer. I/O unit for output message
  !> \param[in] lun_stat   -> integer. I/O unit for statistic
  !> \param[in] ctrl       -> type(CtrlPrm). Controls variables
  !> \param[in] grid_tdens -> tyep(mesh). Mesh for tdens
  !> \param[in] grid_pot   -> tyep(mesh). Mesh for pot
  !>---------------------------------------------------------------------
  subroutine init_p1p1(this,&
       lun_err,lun_out,lun_stat,&
       ctrl,&
       id_ode,&
       id_subgrid, grid_tdens,grid_pot)
    implicit none
    class(p1p1_space_discretization),target, intent(inout) :: this
    integer,                          intent(in   ) :: lun_err
    integer,                          intent(in   ) :: lun_out
    integer,                          intent(in   ) :: lun_stat
    type(CtrlPrm),                    intent(in   ) :: ctrl
    integer,                          intent(in   ) :: id_ode
    integer,                          intent(in   ) :: id_subgrid
    type(abs_simplex_mesh), target,   intent(in   ) :: grid_tdens
    type(abs_simplex_mesh), target,   intent(in   ) :: grid_pot
    ! local
    logical :: rc
    integer :: res
    integer :: ntdens, npot, ngrad,i,iloc,jloc
    ! jacobian 
    type(array_mat) :: jacobian_list(4)
    integer :: block_structure(3,4)
    character(len=1) :: jacobian_directions(4)
    real(kind=double), allocatable :: kernel_full(:,:) 
    ! preconditioners
    type(array_linop) :: prec_list(2)
    integer :: prec_block_structure(3,2)
    type(input_prec) :: ctrl_prec
    type(eye),   target :: identity_npot
    integer ::info
    type(input_prec) :: ctrl_inverse
    
    
    this%id_subgrid = id_subgrid
    this%grid_tdens => grid_tdens
    this%grid_pot   => grid_pot
    
    !
    ! dimension assignment + local copy
    !
    this%ntdens    = grid_tdens%ncell
    this%npot      = grid_pot%nnode
    this%ngrad     = grid_pot%ncell
    this%ambient_dimension = grid_tdens%ambient_dimension
    this%nfull     =  this%ntdens +  this%npot

    

    ntdens   = this%ntdens    
    npot     = this%npot      
    ngrad = this%ngrad  
    

    !
    ! construction of p1 element space for potential
    !
    call this%p1_pot%init(lun_err,this%grid_pot)
    write(lun_stat,'(a)') 'Pot p1 Galerkin initialized'

    !
    ! construction of p1 element space fot tdens
    !
    if ( id_subgrid .eq. 1 ) then
       call this%p1_tdens%init(lun_err,this%grid_pot)
       write(lun_stat,'(a)') 'Tdens p1 Galerkin initialized'
    else
       this%p1_tdens => this%p1_pot
    end if
    
    !
    ! Linear system varibles
    !

    ! Stiffness matrix
    call this%stiff%init(lun_err,&
         npot, npot,  this%p1_pot%nterm_csr,&
         storage_system='csr',&
         is_symmetric=.true.)
    this%stiff%ia=this%p1_pot%ia_csr
    this%stiff%ja=this%p1_pot%ja_csr
    write(lun_stat,'(a)') 'stiff initiliazed'
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Stiffness Matrix initiliazed'
    call this%near_kernel%init(lun_err,npot)
    

    !
    ! Linear system varibles
    !
    ! Stiffness matrix
    if (ctrl%debug .ge. 1) write(lun_out,'(a)') 'Stiffness Matrix Initiliazation'
    call this%mass_mat%init(lun_err,&
         npot, npot,  this%p1_tdens%nterm_csr,&
         storage_system='csr',&
         is_symmetric=.true.)
    this%mass_mat%ia  = this%p1_tdens%ia_csr
    this%mass_mat%ja  = this%p1_tdens%ja_csr
    
   
    
    

    !
    ! Variables for Newton Method in Implicit Euler Procedure
    !
    ! B_matrix and assembler_Bmatrix_subgrid
    ! (the pointer ia and ja are stored directly in B_matrix)
    allocate(&
         this%assembler_Bmatrix_subgrid(&
         this%grid_pot%nnodeincell,this%grid_pot%ncell),&
         this%assembler_Bmatrix_grid(&
         this%grid_tdens%nnodeincell,this%grid_tdens%ncell),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', &
         ' type p1p1 member assembler_Bmatrix_subgrid')
    
    if (ctrl%id_subgrid .eq. 1) then
       call assembly_B_matrix_general(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_subgrid) 
    else
       call assembly_B_matrix_grid(&
            lun_err,&
            this%grid_pot,&
            this%B_matrix, this%assembler_Bmatrix_grid)
    end if
    

    !
    ! copy and transpose matrix_B
    !
    this%BT_matrix = this%B_matrix
    allocate (this%transposer(this%B_matrix%nterm),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', &
         ' type p1p1 member transposer')
    call this%BT_matrix%transpose(lun_err,this%transposer)
    


    !
    ! copies of B matrix to copy its structure
    !
    this%C_matrix=this%B_matrix
    this%deltatD1C_matrix = this%C_matrix
    !
    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! gf matrices
    !
    this%DB_matrix = this%B_matrix
    this%BTD_matrix = this%BT_matrix

    ! copy and transpose matrix_C
    !
    this%CT_matrix=this%C_matrix
    call this%CT_matrix%transpose(lun_err)
    
    !
    ! set dimensions for compunent od block matrix
    !  
    call this%D_matrix%init(lun_err, ntdens)
    call this%inv_D_matrix%init(lun_err, ntdens)



   
    !
    ! Varaibles for Linear solver procedure
    !
    ! Auxiliary var for PCG procedure  
    call this%aux_bicgstab%init(lun_err, 0, 9*(npot+ntdens))

    ! Auxiliary var for PCG procedure  
    call this%aux_newton%init(lun_err,&
         0, 13*ntdens + 5 * npot + 4* (ntdens + npot))

    ! rhs scratch
    allocate(&
         this%rhs(npot),&
         this%rhs_ode(ntdens),&
         this%inc_ode(ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', &
         ' type p1p1 member rhs')

    ! 
    ! work array
    ! 
    allocate(&
         this%tdens_prj(ngrad),&
         this%scr_npot(npot),&
         this%scr_ntdens(ntdens),&
         this%scr_ngrad(ngrad),&
         this%scr_nfull(npot+ntdens),&
         this%rhs_full(npot+ntdens),&
         this%scr_integer(npot+ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', & 
        ' type p1p1 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

    ! 
    ! work array
    ! 
!!$    allocate(&
!!$         !this%D2(ntdens),&
!!$         !         this%D3(ntdens),&
!!$         this%invD2(ntdens),&
!!$         this%invD2_D1(ntdens),&
!!$         this%norm_rows_stiff(npot),&
!!$         this%nrm_grad_dyn(ntdens),&
!!$         stat=res)
!!$    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', &
!!$         ' type p1p1 member'//&
!!$         ' D1, D2, invD2, invD2_D1')
    
    


    ! 
    ! newton function
    ! 
    allocate(&
         this%fnewton_tdens(ntdens),&
         this%fnewton_gfvar(ntdens),&
         this%fnewton_pot(npot),&
         this%fnewton_tdens_old(ntdens),&
         this%fnewton_gfvar_old(ntdens),&
         this%fnewton_pot_old(npot),&
         this%rhs_reduced(npot),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_p1p1', &
         ' type p1p1 member'//&
         ' fnewton_pot,  fnewton_tdens'//&
         ' fnewton_pot_old,  fnewton_tdens_old'//&
         ' rhs_reduced')



  contains

      !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (itria) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_general(lun_err,subgrid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: subgrid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(subgrid%nnodeincell,subgrid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, icell_sub, inode, inode_sub,j,k,ind
    integer :: nsubnode, nsubcell, ncell_parent
    integer :: nrow, ncol, nterm, nnodeincell, nsubnode_in_cell
    integer :: start, finish
    integer , allocatable :: count_subnode_in_cell(:),ia(:),ja(:)


    nsubcell     = subgrid%ncell
    nsubnode     = subgrid%nnode
    nnodeincell  = subgrid%nnodeincell 
    ncell_parent = subgrid%ncell_parent


    ! triangle
    if ( nnodeincell .eq. 3 ) nsubnode_in_cell = 6
    ! tetrahedron
    if ( nnodeincell .eq. 4 ) nsubnode_in_cell = 10

    nrow  = ncell_parent
    ncol  = nsubnode
    nterm = nsubnode_in_cell * ncell_parent


    !
    ! Set B_matrix dimensions and work arrays
    ! we use members ia,ja in B_matrix as work array
    !
    call B_matrix%init(lun_err, &
         nrow, ncol, nterm,&
         storage_system='csr',&
         is_symmetric =.false.)

    ! allocate work array 
    allocate(&
         ! count the number of added node for each cell
         count_subnode_in_cell(nrow),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'assembly_B_matrix_general', &
         'alloc fail for temp array nsubnode_in_cell')

    B_matrix%ia=0
    B_matrix%ja=0
    count_subnode_in_cell=0



    !
    ! set ia pointer
    !
    do icell=1,ncell_parent+1
       B_matrix%ia(icell)=1+(icell-1) * nsubnode_in_cell
    end do

    do icell_sub = 1, nsubcell
       ! cycle all subcell 
       ! find parent cell
       ! find bound in the array ja
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle all nodes in subcell
       !
       if ( count_subnode_in_cell(icell) < nsubnode_in_cell ) then
          do k = 1, nnodeincell
             inode_sub=subgrid%topol(k,icell_sub)

             ! search subnode index in previously added subnodes
             found=.false.
             do j = 1, count_subnode_in_cell(icell)
                ind = start + j -1
                if ( B_matrix%ja( ind ) .eq. inode_sub ) then 
                   found=.true.
                end if
             end do
             ! add subnode index if not added yet
             if ( .not. found ) then
                ind     = start + count_subnode_in_cell(icell)
                B_matrix%ja(ind) = inode_sub
                ! add plus one to nodes  counted for each row
                count_subnode_in_cell(icell) = count_subnode_in_cell(icell) + 1
             end if
          end do
       end if
    end do

    !
    ! sort matrix column-index 
    !
    call B_matrix%sort()

    !
    ! assembly of trija
    !
    do icell_sub = 1, nsubcell
       !
       ! icell father and bounds
       !
       icell  = subgrid%cell_parent(icell_sub)
       start  = B_matrix%ia(icell)
       finish = B_matrix%ia(icell+1)-1

       !
       ! cycle subnodes
       !
       do k = 1,nnodeincell
          inode_sub = subgrid%topol(k,icell_sub)            
          found=.false.
          j=0
          do while ( .not. found )
             j=j+1
             ind   = start+j-1
             found = ( B_matrix%ja(ind) .eq. inode_sub )
          end do
          trija(k,icell_sub) = ind
       end do
    end do

    deallocate(count_subnode_in_cell,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'assembly_B_matrix_general', &
         'dealloc fail for temp array count_nsubnode_in_tria ')

  end subroutine assembly_B_matrix_general

  !>------------------------------------------------------------------
  !> Procedure for build the indexes arrays
  !> ia, ja for the rectagular matrix with 
  !> ntdens rows and npot columns 
  !> Given the index (icell) of a triangle of a coaser
  !> grid gives the indexes of the 6 nodes
  !> of a conformally refined grid
  !> Build also the trija for the assembly of the matrix,
  !> which for each triangle (icell_sub) in the refined grid
  !> gives the local position in the array
  !> ja(ia(itria):ia(itria+1)-1) [with itria the triangle 
  !> containg (icell_sub)] of the local node 
  !> (public procedure for type mesh)
  !>------------------------------------------------------------------
  subroutine assembly_B_matrix_grid(lun_err,grid,B_matrix,trija)
    use Globals
    use SparseMatrix
    implicit none
    ! inputs
    integer,     intent(in) :: lun_err
    type(abs_simplex_mesh),  intent(in) :: grid
    ! output 
    type(spmat), intent(inout) :: B_matrix
    integer,     intent(inout) :: trija(3,grid%ncell)


    !local
    logical :: rc,found
    integer :: res
    integer :: icell, inode, j,k,ind,icol
    integer :: nnode, ncell, nnodeincell
    integer :: start, finish,nodes(3)
    integer , allocatable :: nnode_in_cell(:),ia(:),ja(:)


    ncell        = grid%ncell
    nnode        = grid%nnode
    nnodeincell  = grid%nnodeincell

    ! allocate work array 
    allocate(&
         nnode_in_cell(ncell),&
         ia(ncell+1),&
         ja(ncell*3),&
         stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_alloc, 'subnode_topol', &
         'alloc fail for temp array nnode_in_cell ia ja')


    call B_matrix%init(lun_err, &
         ncell, nnode, ncell*nnodeincell,&
         storage_system='csr',&
         is_symmetric =.false.)
    
    nnode_in_cell=0
    ia=0
    ja=0

    do icell=1,ncell+1
       ia(icell)=1+(icell-1)*nnodeincell
    end do

    do icell = 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       ja(start:finish) = grid%topol(1:nnodeincell,icell)
       call isort(nnodeincell,ja(start:finish))
    end do

    !
    ! assign ia,ja
    !      
    B_matrix%ia=ia
    B_matrix%ja=ja

    !
    ! assembly of trija
    !
    do icell= 1, ncell
       start=ia(icell)
       finish=ia(icell+1)-1
       nodes=grid%topol(1:nnodeincell,icell)
       do ind=start,finish
          icol=ja(ind)
          k=1
          do while ( icol .ne. nodes(k) )
             k=k+1
          end do
          trija(k,icell) = ind
       end do
    end do



    deallocate(nnode_in_cell,ia,ja,stat=res)
    if (res.ne.0) rc=IOerr(lun_err, err_dealloc, 'subnode_topol', &
         'dealloc fail for temp array nnode_in_cell ia ja')

  end subroutine assembly_B_matrix_grid


    
  end subroutine init_p1p1
      


  end module TdensPotentialSystem

  
