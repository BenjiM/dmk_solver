

FV2DSWE             {#mainpage}
=======

Authors:
-------

Enrico Facca and Mario Putti

**Initial date:** December 2015


This document describes the source code for the FV2DSWE model.

The code solves the normally integrated shallow water equations
defined on an irregular bottom surface.

Programming style
-----------------

ogni struttura self contained va in un modulo contenuto in un file con
lo stesso nome del modulo, a cui si aggiunge all'inizio un numero in
doppia cifra crescente (per far funzionare correttamente l'mkmf) e la
stringa mod (ad esempio 00modIOdefs.f90 contiene il modulo IOdefs, che
e' il primo che deve essere caricato nel Makefile, 10modclass_terrain.f90
che contiene il modulo class_terrain)

tutte le struttre vanno commentate

- tutte le strutture (derived type) di tipo "private" o relativamente
  semplici dovranno contenere un metodo chiamato "print" che stampa il
  contenuto della variabile 

- tutte le strutture (derived type) di tipo "public" o
  sufficientemente complesse dovranno contenere un metodo
  "costruttore" chimato "init" e un metodo di stampa chiamato "info" 


Project  directory structure
-----------------------------

		Base directory
- Makefile 
- 00modGlobals.f90 
- 01modIOdefs.f90 
- 10modTerrain.f90 
- 30modVtk.f90 
- main.f90 
- NodTria 

		Makefile rules for different compilers
- Rules_make/          
    - Rules.make -> Rules.make-gfortran 
    - Rules.make-gfortran 
    - Rules.make-intel 
    - Rules.make-xlf 
    .

		vtk library source files
- vtk/
    - A0_LIB_VTK_IO.f90 
    - Makefile 
    .

		libraries for the project (only vtk library for now)
- libs/
    - libvtk.a
    .

		Fortran modules built by make during compilation
- mods/
    - globals.mod 
    - iodefs.mod 
    - lib_vtk_io.mod 
    - terrain.mod 
    - vtkloc.mod 
    .

		object files built by make during compilation
- objs/
    - 00modGlobals.o 
    - 01modIOdefs.o 
    - 10modTerrain.o 
    - 20modVTK.o 
    - A0_LIB_VTK_IO.o 
    - main.o 
    .

		doxygen documentation configuration
- doxygen/
    - DoxygenConfigFortran ! Doxygen config file
    - programming_style.txt   
    - README.tpl      ! initial page template
    .

		documentation built directory
- doc/
    - html ! html version. open doc/html/index.html
    .

		simulation base directory
- run1/
    - fv.fnames 
    - input/ 
        - grid.dat 
        - mat.dat 
        - param.dat 
        - parm.dat 
        .
    - output/ 
        - debug.dat 
        - ris.dat 
        - vtkout/     ! vtk output directory
        . 
    .

		useful sh scripts
- scripts/
    - mkdeps	 **used in "make depend" to create dependencies (modules, includes, etc)
    .

		saved old projects
- savs/
    - Makefile 
    .
.


:
