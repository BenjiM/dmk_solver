!> Defines simulation control parameters
module ControlParameters
  use Globals
  use StdSparsePrec
  use PreconditionerTuning
  use Eigenv
  use LinearSolver
  !use PCG
  implicit none
  private
  type, public :: CtrlPrm
     !>--------------------------------------------------------
     !> Simulation  Controls
     !>--------------------------------------------------------
     !> control debug output. if > 0 full debug, if =0 no debug
     integer :: debug
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_state = 1
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_inputs_update = 1
     !> control mesh output. if =0 prints mesh and stops.
     !>                      if =1 normal simulation
     integer :: info_newton
     !> Format for integer output 
     character(len=256) :: iformat
     !> Format for real output
     character(len=256) :: rformat
     !> Format for integer output 
     character(len=256) :: iformat_info
     !> Format for real output
     character(len=256) :: rformat_info
     !>--------------------------------------------------------
     !> Ode Global Controls
     !>--------------------------------------------------------
     !> Flag for use of subgrid (0= no subgrid, 1= subgrid )
     integer :: id_subgrid
     !> Flag control the use of
     !> pp ode or gradient flow ode
     !> (1= pp, 2= gradient flow )
     integer :: id_ode
     !> lower bound for tdens
     real(kind=double) :: min_tdens
     !******************************************************
     !  Time controls
     !******************************************************
     !> Initial time
     real(kind=double) :: tzero=zero
     !>---------------------------------------------------------------
     !> Time discretization 
     !> Control the time-step discretization used
     !> id_time_discr == 1 => explicit Euler 
     !> id_time_discr == 2 => implicit Euler picard
     integer :: id_time_discr
     !> Maximum number of time iterations
     !> max_time_it=0 => Only one elliptic equation is solved
     integer :: max_time_it
     !> Maximum number of time iterations
     integer :: max_iter_nonlinear
     !> Maximum number of time iterations
     integer :: nrestart_max=15
     !> Maximum number of time iterations
     integer :: max_nrestart_invert_jacobian=0
     !>---------------------------------------------------------------
     !> Convergence tolerances
     !> Tolerance of var_tdens 
     real(kind=double) :: tol_var_tdens
     !> Tolerance in fix_point scheme for implicit time stepping 
     real(kind=double) :: tol_nonlinear
     !>---------------------------------------------------------------
     !> Time step control 
     !> Control of the time step
     !> id_ctrl_time=0 => constant time step
     !> id_ctrl_time=1 => increasing time step, with factor 
     !>                   "exp_rate" and upper bound "bound_deltat" 
     integer :: id_time_ctrl
     !> Time step
     real(kind=double) :: deltat
     !> Expansion rate in id_ctrl_time == 2
     real(kind=double) :: exp_rate
     !> Upper bound for deltat in id_ctrl_time == 2,3,4
     real(kind=double) :: upper_bound_deltat
     !> Lower bound for deltat in id_ctrl_time == 3,4
     real(kind=double) :: lower_bound_deltat    
     !******************************************************
     ! Linear solver Controls
     !******************************************************
     !> Gamma relaxation 
     real(kind=double) :: gamma=1.0d2
     !> how to build hatS
     integer :: id_hatS=1
     !> Destination for error msg for linear solver
     !> Used to define ctrl_solver%lun_err
     character(len=256) :: solver_err
     !> Destination for error msg for linear solver
     !> Used to define ctrl_solver%lun_out
     character(len=256) :: solver_out
     !> Global ctrls  for solution of linear system
     !> via PCG algorithm 
     type(input_solver) :: ctrl_solver_original 
     !> Global ctrls  for solution of linear system
     !> via PCG algorithm 
     type(input_solver) :: ctrl_solver  
     !> Global ctrls  for solution of linear system
     !> via PCG algorithm 
     type(input_solver) :: ctrl_solver_augmented
     !> Global ctrls  for solution of linear system
     !> via PCG algorithm 
     type(input_prec) :: ctrl_prec_augmented
     !> Controls for the Preconditioning strategy
     !> via PCG algorithm 
     type(input_prec) :: ctrl_prec
     !> Controls for tuning of Preconditioner
     type(input_tuning) :: ctrl_tuning
     !> Id. for application of bochev correction
     !> That impose orthogonality to a given vector w
     integer :: id_singular
     !> Id. for scaling the matrix by its diagonal
     integer :: id_diagscale
     !> Controls for DACG procedure
     !> via PCG algorithm 
     type(input_dacg) :: ctrl_dacg
     !> Control of prec.  building 
     !> Not read from file
     integer :: build_prec=1
     !> Control of prec.  building 
     !> Not read from file
     integer :: build_tuning=1
     !> Reference threshold for buffering prec. calc.
     !> Not read from file
     integer :: threshold=0
     !>---------------------------------------------------------------
     !> Prec. Buffering
     !> Flag for buffering the calculation of the prec.
     !>  id_buffer_prec=0 => build at each iteration (no buffer)
     !>  id_buffer_prec=1 => build at each iteration (no buffer)
     !>  id_buffer_prec=2 => build at each iteration (no buffer)
     !>  id_buffer_prec=3 => build at each iteration (no buffer)
     integer :: id_buffer_prec
     !> Reference Number of iteration 
     !> Used for id_buffer_prec=1,...
     integer :: ref_iter 
     !> Factor of growth of iterations
     real(kind=double) :: prec_growth
     !>---------------------------------------------------------------
     !> Newton controls
     !>---------------------------------------------------------------
     !> Number of broyden update of jacobian preconditioner
     integer :: nbroyden=0
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: newton_method
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: reduced_jacobian=1
     !> Preconditioner strategy for linear system in newton scheme
     !> work in combination with reduced_jacobian
     !> ( reduced_jacobian + prec_newton ) =     
     !> 0 + 1 => full system + P=diag (stiff)^-1, D2^-1)
     !> 0 + 2 => full system + symmetization + P=mixed
     !> 0 + 3 => full system + P=mixed
     !> 0 + 4 => full system + P=triangular
     !> 1 + 1 => reduced system + P = (stiff)^-1) 
     !> 0 + 2 => reduced system + P = (stiff+ deltat BT D1 D2^{-1}C )^-1
     integer :: prec_newton=2
     !> Inecata Newton Method
     !> inexact_newton = 0 => off ( linear system solve with ctrl_sovler%tol)
     !> inexact_newton = 1 => on
     integer :: inexact_newton
     integer :: gfvar_approach=1
     !**************************************************************
     !  Saving Procedure
     !*************************************************************
     !> control saving into vtk files
     !> id_save == 0 => no save
     !> id_save == 1 => save all
     !> id_save == 2 => save with fixed frequency
     !> id_save == 3 => save when the first digit of var_tdens changes
     !> Id. saving data into .dat
     integer :: id_save_dat
     !> number for naming vtk files
     integer :: ifile_dat
     !> frequency for control id_vtksave == 2 
     integer freq_dat
     !> Id. saving into matrix
     integer :: id_save_matrix
     !> number for naming matrix files
     integer :: ifile_matrix
     !> frequency for control id_matrixsave == 2 
     integer freq_matrix
     !*************************************************************
     !> 
     integer :: factorization_job
     !> 
     integer :: max_bfgs=10
     !>  Relaxation parameter add to tdens 
     real(kind=double) :: lambda
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: alpha_uzawa
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: omega_uzawa
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax2prec=1.0d-4
     !> Selection parameter for tdens
     real(kind=double) ::  threshold_tdens = 0.0d0
     !> ONLY IN SPECTRAL P0
     integer :: id_heavy=1
     !> ONLY IN SPECTRAL P0
     integer :: var_tdens=2
   contains
     !> static constructor
     !> (public for type CtrlPrm)
     procedure, public, nopass :: init => CtrlPrm_init
     !> Procedure for reading controls from file
     !> (public for type CtrlPrm)
     procedure, public, pass :: read => CtrlPrm_read
     !> static destructor
     !> (public for type CtrlPrm)
     procedure, public, pass :: kill => CtrlPrm_kill
     !> output the content of the variable
     !> (public for type CtrlPrm)
     procedure, public, pass :: info => CtrlPrm_info
     !> Procedure to generate formats derived from iformat, rformat
     !> (public for type CtrlPrm)
     procedure, public, pass :: formatting
     !> Fucntion to build a separtor with title
     !>for statistic file
     !> (public for type CtrlPrm)
     procedure, public, pass :: sep => separator
  end type CtrlPrm

contains
  
  

  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type CtrlPrm)
  !> Instantiate (allocate if necessary)
  !> and initialize (by also reading from input file)
  !> variable of type CtrlPrm
  !>
  !> usage:
  !>     call 'var'%init(IOfiles, ctrl_global)
  !>
  !> where:
  !> \param[in] IOfiles     -> type(IOfdescr). I/O file information
  !> \param[in] ctrl_global -> type(CtrlPrm). Global controls
  !<-------------------------------------------------------------
  subroutine CtrlPrm_init(this, IOfiles)
    use Globals
    use IOdefs
    implicit none
    type(CtrlPrm),    intent(out) :: this
    type(IOfdescr),   intent(in ) :: IOfiles
    
    call this%read(IOfiles)
    
  end subroutine CtrlPrm_init


  !>-------------------------------------------------------------
  !> Procedure for reading controls from files
  !> (procedure public for type CtrlPrm, used in init)
  !> Instantiate (allocate if necessary)
  !> and initialize (by also reading from input file)
  !> variable of type CtrlPrm
  !>
  !> usage:
  !>     call 'var'%init(IOfiles, ctrl_global)
  !>
  !> where:
  !> \param[in] IOfiles     -> type(IOfdescr). I/O file information
  !<-------------------------------------------------------------
  subroutine CtrlPrm_read(this, IOfiles)
    use Globals
    use IOdefs
    use Eigenv
    implicit none
    !vars
    class(CtrlPrm), intent(out) :: this
    type(IOfdescr), intent(in ) :: IOfiles
    ! local vars
    type(file) :: file2read
    integer :: stderr   
    integer :: lun 
    integer :: n_fillin
    real(kind=double) :: tol_fillin
    character(len=256) fname,scheme_read
    character(len=256) input
    character(len=256) clean,string
    character(len=256) prec_type

    integer :: ieig=0
    integer :: nev=0
    integer :: ituning=0
    integer :: npres=0

    integer :: lun_err=6
    integer :: lun_out=6
    integer :: iexit=1
    integer :: imax=1000
    integer :: iprt=1
    integer :: isol=0
    integer :: i,res
    real(kind=double) :: tol_pcg=1.0d-12, tol_dacg
    integer :: iort
    integer :: info

    file2read=IOfiles%control

    lun   = file2read%lun    
    fname = etb(file2read%fn)

    stderr= IOfiles%stderr%lun
    
    !>-----------------------------------------------------------
    !> Simulation control
    call find_nocomment(lun,stderr,input,fname,'debug')
    read(input,*) this%debug

    call find_nocomment(lun,stderr,input,fname,'mesh_out')
    read(input,*) this%info_newton

    call find_nocomment(lun,stderr,input,fname,'iformat')
    clean = erase_comment(input)
    read(clean,*) this%iformat

    call find_nocomment(lun,stderr,input,fname,'rformat')
    clean = erase_comment(input)
    read(clean,*) this%rformat

    call find_nocomment(lun,stderr,input,fname,'iformat_info')
    clean = erase_comment(input)
    read(clean,*) this%iformat_info

    call find_nocomment(lun,stderr,input,fname,'rformat_info')
    clean = erase_comment(input)
    
    read(clean,*,iostat=res) this%lambda
    if ( res .ne. 0) then
       read(clean,*,iostat=res) this%rformat_info
    else
       this%rformat_info='1pe9.2'
    end if
    write(*,*) this%lambda



    !*********************************************************
    ! Global controls
    !*********************************************************
    call find_nocomment(lun,stderr,input,fname,'id_subgrid')
    read(input,*) this%id_subgrid

    call find_nocomment(lun,stderr,input,fname,'id_ode')
    read(input,*) this%id_ode

    call find_nocomment(lun,stderr,input,fname,'min_tdens')
    read(input,*) this%min_tdens
   
    !****************************************************
    ! Time discretization input
    !****************************************************
    call find_nocomment(lun,stderr,input,fname,'id_time_discr')
    read(input,*) this%tzero

    call find_nocomment(lun,stderr,input,fname,'id_time_discr')
    read(input,*) this%id_time_discr

    call find_nocomment(lun,stderr,input,fname,'max_time_it')
    read(input,*) this%max_time_it

    call find_nocomment(lun,stderr,input,fname,'maxr_iter_nonlinea')
    read(input,*) this%max_iter_nonlinear
    
    ! time step controls
    call find_nocomment(lun,stderr,input,fname,'id_time_ctrl')
    read(input,*) this%id_time_ctrl

    call find_nocomment(lun,stderr,input,fname,'deltat')
    read(input,*) this%deltat

    call find_nocomment(lun,stderr,input,fname,'exp_rate')
    read(input,*) this%exp_rate

    call find_nocomment(lun,stderr,input,fname,'upper_bound_deltat')
    read(input,*) this%upper_bound_deltat

    call find_nocomment(lun,stderr,input,fname,'lower_bound_deltat')
    read(input,*) this%lower_bound_deltat

    ! Convergence tolerance
    call find_nocomment(lun,stderr,input,fname,'tol_var_tdens')
    read(input,*) this%tol_var_tdens

    call find_nocomment(lun,stderr,input,fname,'tol_nonlinear')
    read(input,*) this%tol_nonlinear

    !****************************************************
    ! Time discretization input
    !****************************************************
    call find_nocomment(lun,stderr,input,fname,'id_singular')
    read(input,*) this%id_singular
    
    call find_nocomment(lun,stderr,input,fname,'id_diag_scale')
    read(input,*)this%id_diagscale

    
    ! linear solver method controls
    call find_nocomment(lun,stderr,input,fname,'scheme')
    read(input,*) clean
    scheme_read=erase_comment(clean)

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) string
    lun_err=IOfiles%find_lun(string)
    if  (lun_err<0) read(string,*) lun_err

    call find_nocomment(lun,stderr,input,fname,'file_out')
    read(input,*) string
    lun_out=IOfiles%find_lun(string)
    if  (lun_out<0) read(string,*) lun_out

    call find_nocomment(lun,stderr,input,fname,'iexit')
    read(input,*) iexit

    call find_nocomment(lun,stderr,input,fname,'imax')
    read(input,*) imax

    call find_nocomment(lun,stderr,input,fname,'iprt')
    read(input,*) iprt

    call find_nocomment(lun,stderr,input,fname,'isol')
    read(input,*) isol
    
    call find_nocomment(lun,stderr,input,fname,'tol_pcg')
    read(input,*) tol_pcg

    call find_nocomment(lun,stderr,input,fname,'iort')
    read(input,*) iort
 

    call this%ctrl_solver%init(&
         stderr,&
         scheme=scheme_read,&
         lun_err=lun_err,&
         lun_out=lun_out,&
         iexit=iexit,&
         imax=imax,&
         iprt=iprt,&
         isol=isol,&
         tol_sol=tol_pcg,&
         iort=iort)
    if ( this%debug .ge. 10) then   
       this%ctrl_solver%debug = 1
    else
       this%ctrl_solver%debug = 0
    end if

    this%ctrl_solver_original = this%ctrl_solver

    this%ctrl_solver_augmented = this%ctrl_solver
    this%ctrl_solver_augmented%scheme = 'MINRES'


    !***********************************************************
    ! Preconditioner
    !***********************************************************

    ! Standard prec

    call find_nocomment(lun,stderr,input,fname,'prec_type')
    read(input,*) prec_type

    call find_nocomment(lun,stderr,input,fname,'n_fillin')
    read(input,*) n_fillin

    call find_nocomment(lun,stderr,input,fname,'tol_fillin')
    read(input,*) tol_fillin

    call this%ctrl_prec%init(stderr,prec_type,n_fillin,tol_fillin)



    
    


     ! Tuning
    call find_nocomment(lun,stderr,input,fname,'ieig')
    read(input,*) ieig
    
    call find_nocomment(lun,stderr,input,fname,'nev')
    read(input,*) nev
    
    call find_nocomment(lun,stderr,input,fname,'ituning')
    read(input,*) ituning
    
    
    ! ctrl_tuning%ieig=1 DACG

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) string
    lun_err=IOfiles%find_lun(string)

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%lun_out')
    read(input,*) string
    lun_out=IOfiles%find_lun(string)
    
    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%iexit')
    read(input,*) iexit

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%imax')
    read(input,*) imax
    
    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%iprt')
    read(input,*) iprt

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%isol')
    read(input,*) isol

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%tol_dacg')
    read(input,*) tol_dacg

    this%threshold_tdens= tol_dacg


    ! ctrl_prec%ieig=2 LANCZOS
    call find_nocomment(lun,stderr,input,fname,'ctrl_tuning%npres')
    read(input,*) npres

    this%max_bfgs = npres

    call this%ctrl_dacg%init(lun_err,lun_out,iexit,imax,iprt,isol,tol_dacg)
    call this%ctrl_tuning%init(ieig,nev,ituning,this%ctrl_dacg,npres)



    

    ! buffering construction prec
    ! working variables controlled by id_buffer_prec
    call find_nocomment(lun,stderr,input,fname,'id_buffer_prec')
    read(input,*) this%id_buffer_prec

    call find_nocomment(lun,stderr,input,fname,'ref_iter')
    read(input,*) this%ref_iter

    call find_nocomment(lun,stderr,input,fname,'prec_growth')
    read(input,*) this%prec_growth
    
    !
    ! number of Boryden update
    !
    call find_nocomment(lun,stderr,input,fname,'nbroyden')
    read(input,*) this%nbroyden

    call find_nocomment(lun,stderr,input,fname,'reduced_jacobian')
    read(input,*) this%newton_method

    this%reduced_jacobian = this%newton_method/10
    this%prec_newton      = mod(this%newton_method,10)

    call find_nocomment(lun,stderr,input,fname,'inexact_newton')
    read(input,*) this%inexact_newton

    


    !***********************************************************
    ! Saving Data
    !***********************************************************
    ! saving dat
    call find_nocomment(lun,stderr,input,fname,'id_save_dat')
    read(input,*) this%id_save_dat
    call find_nocomment(lun,stderr,input,fname,'freq_dat')
    read(input,*) this%freq_dat
    call find_nocomment(lun,stderr,input,fname,'ifile_dat')
    read(input,*) this%ifile_dat    

    ! saving matrix
    call find_nocomment(lun,stderr,input,fname,'id_save_matrix')
    read(input,*) this%id_save_matrix
    call find_nocomment(lun,stderr,input,fname,'freq_matrix')
    read(input,*) this%freq_matrix
    call find_nocomment(lun,stderr,input,fname,'ifile_matrix')
    read(input,*) this%ifile_matrix 

    call find_nocomment(lun,stderr,input,fname,'factorization_job',info)
    if ( info .eq. 0) then
       write(*,*) etb(input)
       read(input,*,iostat=res) this%factorization_job
       if ( res.ne.0) this%factorization_job =0
    else
       this%factorization_job =0
    end if

    call find_nocomment(lun,stderr,input,fname,'relaxiation_prec',info)
    if ( info .eq. 0) then
       read(input,*) this%relax2prec
    else
        this%relax2prec =0.0d0
    end if

    call find_nocomment(lun,stderr,input,fname,'nrestart_newton',info)
    if ( info .eq. 0) then
       read(input,*) this%nrestart_max
    else
        this%nrestart_max = 10
    end if

    call find_nocomment(lun,stderr,input,fname,'gamma',info)
    if ( info .eq. 0) then
       read(input,*) this%gamma
    else
        this%gamma = zero
    end if
    
    call find_nocomment(lun,stderr,input,fname,'var_tdens',info)
    if ( info .eq. 0) then
       read(input,*) this%var_tdens
    else
        this%var_tdens = 2
    end if
  contains 
    subroutine find_nocomment(lun,stderr,input,fname,var_name,info)
      use Globals
      integer,            intent(in   ) :: lun
      integer,            intent(in   ) :: stderr
      character(len=256), intent(in   ) :: fname
      character(len=256), intent(inout) :: input
      character(len=*),   intent(in   ) :: var_name
      integer, optional,intent(inout) :: info
      !local
      logical :: rc
      integer :: res

      character(len=256) clean
      character(len=1) first
      logical :: found
      
      if ( present(info)) info=0

      found = .false.
      do while( .not. found ) 
         read(lun,'(a)',iostat = res) input
         if(res .ne. 0) then
            rc = IOerr(stderr, wrn_inp , 'Ctrl_read', &
                 fname//'input text for member'//etb(var_name),res)
            if ( present(info)) info=-1
            return
         end if
            
         clean = etb(input)
         read(clean,*) first
         if ( ( first .eq. '!') .or. &
              ( first .eq. '%') .or. &
              ( first .eq. '#') ) then
            found=.false.
         else
            found=.true.
         end if 
      end do
    end subroutine find_nocomment
        
  end subroutine CtrlPrm_read

  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type CtrlPrm)
  !> only scalars, does nothing
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine CtrlPrm_kill(this)
    implicit none
    class(CtrlPrm), intent(inout) :: this
    
    call this%ctrl_solver%kill()   
    call this%ctrl_prec%kill()
    call this%ctrl_tuning%kill()
    call this%ctrl_dacg%kill()
    
  end subroutine CtrlPrm_kill

  !>-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type CtrlPrm)
  !> Prints content of a variable of type Ctrl
  !> 
  !> usage:
  !>     call 'var'%info(lun)
  !>
  !> where:
  !> \param[in] lun: integer. output unit
  !>
  !<-------------------------------------------------------------
  subroutine CtrlPrm_info(this, lun)
    use Globals
    implicit none
    ! vars
    class(CtrlPrm), intent(in) :: this
    integer :: lun
    !local vars
    character(len=256) :: string,outf

    write(lun,*) '************************************************'
    write(lun,*) 'Info: CtrlPrm structure definition:'
    
    write(lun,*) '************************************************'
    write(lun,*) 'Simulation controls'
    write(string,'(a,I2,a,I2)') ' Debug ',this%debug
    write(lun,*) etb(string)
    write(lun,*) &
         'Integer format data=',etb(this%iformat), &
         ' Real    format data=',etb(this%rformat)
    write(lun,*) &
         'Integer format info=',etb(this%iformat_info),&
         ' Real    format info=',etb(this%rformat_info)
    write(lun,*) '************************************************'


    if (this%id_ode .eq. 1) then
       write(lun,*) 'Dynamic equation solved = PP Ode'
    else  if (this%id_ode .eq. 2) then
       write(lun,*) 'Dynamic equation solved = Gradient Flow'
    end if
    write(lun,*) '************************************************'
    write(lun,*) 'Global controls'   
    if (this%id_subgrid .eq. 1) then
       write(lun,*) 'Subgrid used'
    else
       write(lun,*) 'Subgrid not used'
    end if
    
    outf=this%formatting('ar')
    write(lun,outf) 'Lower bound tdens  =',this%min_tdens
    outf=this%formatting('a')    
    write(lun,outf) '************************************************'
    write(lun,outf) 'Time discretization info '
    outf=this%formatting('ar')
    write(lun,outf) 'Initial time = ', this%tzero
    outf=this%formatting('ai')
    write(lun,outf) 'Max time iterations = ', this%max_time_it
    outf=this%formatting('a')
    if (this%id_time_discr == 1 ) then
       write(lun,outf) 'Time stepping method = Explicit Euler'
    else if (this%id_time_discr == 2 ) then
       write(lun,outf) 'Time stepping method = Implicit Euler via Newton'
    end if
    outf=this%formatting('ai')
    write(lun,outf) 'id_time_ctrl',this%id_time_ctrl
    if (this%id_time_ctrl == 0) then 
       outf=this%formatting('aar')
       write(lun,outf) 'deltat control  =',' constant deltat =', this%deltat 
    else 
       outf=this%formatting('aa')
       write(lun,outf) 'deltat control  =',' increasing deltat'
       outf=this%formatting('ar')
       write(lun,outf) 'intial deltat   = ', this%deltat
       outf=this%formatting('ararar')       
       write(lun,outf) &
            'expansion rate  = ', this%exp_rate,&
            'upper bound     = ' ,this%upper_bound_deltat,&
            'lower bound     = ' ,this%lower_bound_deltat
    end if
    outf=this%formatting('ar')
    write(lun,outf) ' Tolerance var_tdens =', this%tol_var_tdens
    write(lun,outf) ' Tolerance non linear =', this%tol_nonlinear

    outf=this%formatting('a')
    write(lun,outf) ' ************************************************'
    write(lun,outf) ' Linear solver Controls'
    if (this%id_singular .eq. 1) then
       write(lun,outf) 'Orthogonalization w.r.t kernel' 
    else if ( this%id_singular .eq. 2 ) then
       write(lun,outf) 'Sol(1) fix to zero'
    end if
    if (this%id_diagscale .eq. 1) then
       write(lun,outf) 'Diagonal scale of the system applied'
    end if
    call this%ctrl_solver%info(lun)
    outf=this%formatting('a')
    write(lun,outf) ' ************************************************'
    write(lun,outf) ' Preconditioner Controls'
    call this%ctrl_prec%info(lun)
    call this%ctrl_tuning%info(lun)
    if (this%id_buffer_prec == 0 ) then
       outf=this%formatting('a')       
       write(lun,outf) ' Prec. build at each time iteration'
    else if ( this%id_buffer_prec == 1 ) then
       outf=this%formatting('a')       
       write(lun,outf) ' Buffered building of prec'
       outf=this%formatting('ar')       
       write(lun,outf) ' Factor of growth of iterations = ', &
            this%prec_growth
    else if ( this%id_buffer_prec == 2 ) then
       outf=this%formatting('a')       
       write(lun,outf) ' Buffered building of prec' 
       outf=this%formatting('ai')       
       write(lun,outf) ' Prec build when iter > ', this%ref_iter
    end if
    outf=this%formatting('a')       
    write(lun,outf) ' ************************************************'
    write(lun,outf) ' Controls saving'
    if (this%id_save_dat == 0 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save no data'
    else if (this%id_save_dat == 1 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save all data' 
    else if (this%id_save_dat == 2 ) then
       outf=this%formatting('ai')       
       write(lun,outf) 'save data with frequency = ', this%freq_dat 
    else if (this%id_save_dat == 3 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save data when var_tdens change first digit'
    end if
    if (this%id_save_dat == 0 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save no matrix'
    else if (this%id_save_dat == 1 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save all matrix' 
    else if (this%id_save_dat == 2 ) then
       outf=this%formatting('ai')       
       write(lun,outf) 'save matrix with frequency = ', this%freq_dat 
    else if (this%id_save_dat == 3 ) then
       outf=this%formatting('a')       
       write(lun,outf) 'save matrix when var_tdens change first digit'
    end if

  end subroutine CtrlPrm_info

 function formatting(this,compressed,style) result (out_format)
    use Globals
    implicit none
    class(CtrlPrm),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: compressed
    integer, optional, intent(in   ) :: style
    character(len=256) :: out_format
    !local
    integer :: i,length
    character(len=1) :: singleton
    
    out_format='('  
    
    

    length=len(compressed)


    if ( .not. present(style) .or. (style .eq. 1) ) then
       do i=1, length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat_info)
          case('r')
             out_format=etb(etb(out_format)//this%rformat_info)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    if ( present(style) .and. (style .eq. 2) ) then
       do i=1,length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat)
          case('r')
             out_format=etb(etb(out_format)//this%rformat)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    out_format=etb(etb(out_format)//')')

  end function formatting

  function separator(this,title) result (outstring)
    use Globals
    implicit none
    class(CtrlPrm),             intent(in   ) :: this
    character(len=*), optional, intent(in   ) :: title
    integer, parameter                        :: width=71
    integer, parameter                        :: start=10
    character(len=width) :: outstring
    !local 
    integer :: i,lentitle
    character(len=start) :: strstart
    character(len=width-start-2) :: strend

    if ( present(title)) then
       lentitle=len(etb(title))

       do i=1,start
          write(strstart(i:i),'(a)') '*'
       end do

       do i=1,width-start-2-lentitle
          write(strend(i:i),'(a)') '*'
       end do
       outstring=strstart//' '//etb(title)//' '//strend
    else
       do i=1,width
          write(outstring(i:i),'(a)') '*'
       end do
    end if
       
  end function separator

 
  
  
  
  
    
    
    

       

          
       
!!$  subroutine reset_linear_solver_controls(this,&
!!$       info_solver,&
!!$       total_iterations,iter_media,&
!!$       itemp)
!!$    use Globals    
!!$    implicit none
!!$    class(ThisPrm),      intent(inout) :: this
!!$    type(output_pcg),    intent(in   ) :: info_pcg
!!$    integer,             intent(in   ) :: total_iterations
!!$    integer,             intent(in   ) :: iter_media
!!$    integer,             intent(in   ) :: itemp    
!!$    !local 
!!$    integer :: threshold
!!$
!!$    if ( ( this%id_buffer_prec .eq. 1) .or. &
!!$         ( this%id_buffer_prec .eq. 3) )  then 
!!$       ! Reference number of iterations = average iteration number
!!$       this%ref_iter = total_iterations  / itemp
!!$    else if ( this%id_buffer_prec .eq. 2) then 
!!$       ! Fixed by user reference number of iter
!!$       this%ref_iter = this%ref_iter
!!$    end if
!!$
!!$    threshold = min (this%ref_iter, iter_media)
!!$
!!$    build_prec              = 1 
!!$
!!$    ! If the preconditioner is not built at each iteration
!!$    if ( this%id_buffer_prec .ne. 0 ) then
!!$       ! If the prec. was calculated in the last solution of
!!$       ! a linear sysmat
!!$       ! update the last number of iterations
!!$       if ( this%elliptic%flag_prec ) then
!!$          this%last_iter_prec = this%iter_media
!!$       end if
!!$       ! Do not calculate the next prec
!!$       this%this_pcg%build_prec = 0
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if ( this%id_buffer_prec .eq. 1) then
!!$          if ( this%iter_media > &
!!$               int(this%iter_growth_factor * this%ref_iter) ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration 
!!$       ! or tdens is varying too fast
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 2) then
!!$          if ( ( this%iter_media > this%ref_iter ) .or. &
!!$               ( this%loc_var_tdens > 1.0d1 )    ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that min(last_nit, ref_iter) * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 3 ) then
!!$          if ( this%iter_media > int(this%iter_growth_factor * threshold )) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$    end if
!!$
!!$    this%this_pcg%build_defl = 0
!!$
!!$
!!$    if ( this%iter_media > &
!!$         int(this%eigen_growth_factor * this%ref_iter) ) then
!!$       this%this_pcg%build_defl = 1
!!$    else
!!$       this%this_pcg%build_defl = 0
!!$    end if
!!$  end subroutine reset_linear_solver_controls
  
end module ControlParameters
