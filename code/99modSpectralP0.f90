! module SpectralP0
!   use Globals
!   use GaussQuadrature
!   use FullMatrix
!   use QuadGrid
!   use Spectral
!   use TdensPotentialSystem
!   use DmkOdeData
  
!   implicit none
!   private

!   type, public :: odedatas
!      !> number of time slot
!      integer :: ntime=0
!      !> odedata 
!      type(odedata), allocatable :: odedata_timeseries(:)
!    contains
!      !> Static constructor 
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: init => init_odedatas
!      !> Static destructor
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: kill => kill_odedatas
!   end type odedatas
     

!   type, public :: specp0
!      !> Number of degrees of freedom of ntdens
!      integer :: ntdens
!      !> Number of degrees of freedom of pot
!      integer :: npot
!      !> Number of degrees of freedom of system tdens + pot
!      integer :: nfull
!      !> Number of degrees of gradient of gradient
!      integer :: ngrad
!      !> Number of degrees of pot
!      integer :: ambient_dimension
!      !>-----------------------------------------
!      !> Number of terms used in the construction of stiffness matrix
!      integer :: nterm
!      !> Number of unknown coefficient for elliptic equation
!      !> nequ=npot-1 for Neumann BC
!      integer :: nequ
!      !> Spatial information
!      !> Mesh for tdens
!      type(quadmesh), pointer :: grid
!      !> Dimension(ninter)
!      !> Intervals quantities
!      type(inter), allocatable :: inters(:)
!      !>-----------------------------------------
!      !> Quadrature variable for integration on cell
!      type(gaussq) :: gauss_cell
!      !-------------------------------------------------
!      !> Spectral variables
!      type(spct), pointer :: spec
!      !> Scratch array 
!      !> Dimension(ncell)
!      !> $ \int_{C_r} |\nabla u|^2 } r=1,ncell$
!      real(kind=double), allocatable :: int_nrm2grad(:)
!      !> Dimension (ncell, spec%nterm ) 
!      !> Stiff matrix of cells
!      real(kind=double), allocatable :: stiff_matrices(:,:) 
!      !> Dimension (spec%nterm) 
!      !> Global stiff matrix, lower triangular part
!      real(kind=double), allocatable :: stiff(:)
!      !> Ellptic system
!      type(fullmat) :: stiffness_matrix
!      !>---------------------------------------------------
!      !> Scratch varariable
!      !> Dimension (ngrad)
!      !> Work array 
!      real(kind=double), allocatable :: scr_ngrad(:)
!      !> Dimension (ntdens)
!      !> Work array 
!      real(kind=double), allocatable :: scr_ntdens(:)
!      !> Dimension (npot)
!      !> Work array 
!      real(kind=double), allocatable :: scr_npot(:)
!      !> Dimension (npot+ntdens)
!      !> Work array 
!      integer, allocatable :: scr_integer(:)
!    contains
!      !> Static constructor 
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: init => init_specp0
!      !> Info Procedure
!      !> (procedure public for type tdpotsys)
!      !procedure, public , pass :: info => info_specp0
!      !> Static destructor
!      !> (procedure public for type tdpotsys)
!      !procedure, public , pass :: kill => kill_specp0
!      !> Saving procedure for data
!      !> (procedure public for type tdpotsys)
!      !procedure, public , pass :: writedata => writedata_specp0
!      !> Procedure for building member stiff_matrices
!      !> (procedure public for type tdpotsys)
!      procedure, private , pass :: assembly_stiff
!      !> Procedure for building member stiff_matrices
!      !> (procedure public for type tdpotsys)
!      procedure, private , pass :: build_stiffs
!      !> Procedure for upate the wholw system
!      !> to the next time step
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: syncronize_at_tzero
!      !> Procedure for building the rhs of tdens-ODE
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: assembyl_rhs_ode_tdens
!      !> Procedure for building pointwise tdens-increment 
!      !> from rhs of tdens-ODE
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: get_increment_tdens
!      !> Subroutine for computation of scalar and integer
!      !> functionals like energy, lyapunov, etc
!      !procedure, public , pass :: evaluate_functionals
!      !> Procedure for upate the whole system
!      !> to the next time step
!      !> (procedure public for type tdpotsys)
!      procedure, public , pass :: update
!   end type specp0
! contains
!   subroutine init_odedatas(lun_err,&
!        ntime,ntdens,npot)
!     implicit none
!     class(odedatas),     intent(inout) :: this
!     integer,     intent(in   ) :: lun_err
!     integer,     intent(in   ) :: ntime
!     integer,     intent(in   ) :: ntdens
!     integer,     intent(in   ) :: npot
!     !local
!     logical :: rc
!     integer :: res
    
!     this%ntime = ntime
!     allocate(this%odedata_timeseries(this%ntime),stat=res)
!     if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_odedatas', &
!          'member odedata_series  ',res)
!     do i =1, ntime
!        call this%odedata_timeseries(i)%init(lun_err,ntdens,npot)
!     end do
    
!   end subroutine init_odedatas

!   subroutine kill_odedatas(lun_err)
!     implicit none
!     class(odedatas),     intent(inout) :: this
!     integer,     intent(in   ) :: lun_err
!     !local
!     logical :: rc
!     integer :: res
    
    
    
!     do i =1,this%ntime 
!        call this%odedata_timeseries(i)%kill(lun_err)
!     end do
!     deallocate(this%odedata_timeseries,stat=res)
!     if(res .ne. 0) rc = IOerr(stderr, err_dealloc, 'init_odedatas', &
!          'member odedata_series  ',res)
!     this%ntime = 0
    
!   end subroutine init_odedatas
    


!   !>--------------------------------------------------------------------
!   !> Static Constructor, given ctrl and two meshes
!   !> (public procedure for type tdpotsys)
!   !> 
!   !> usage: call var%init(id_subgrid,&
!   !>                ntria_tdens,&
!   !>                tzero, tdens_ini,&
!   !>                IOfiles,& 
!   !>                ctrl_global,&
!   !>                grid_tdens, grid_pot)
!   !> where:
!   !> \param[in ] id_subgrid   -> integer. Id. for subgrid use
!   !> \param[in ] ntria_tdens  -> integer. Nmb. of triangles of grid_tdens
!   !> \param[in ], optional, tzero     -> real. Initial time
!   !> \param[in ], optional, tdens_ini -> real(ntria_tdens) Initial tdens
!   !> \param[in ] IOfiles      -> type(IOfdescr). I/O info
!   !> \param[in ], ctrl_global -> type(CtrlPrm). Controls variables
!   !> \param[in ] grid_tdens   -> tyep(mesh). Mesh for tdens
!   !> \param[in ] grid_pot     -> tyep(mesh). Mesh for pot
!   !>---------------------------------------------------------------------
!   subroutine init_specp0(this,&
!        spec, grid,ctrl)
!     use Globals
!     use ControlParameters
!     implicit none
!     class(specp0),     intent(inout) :: this
!     type(spct),     target, intent(inout) :: spec
!     type(quadmesh), target, intent(in   ) :: grid    
!     type(CtrlPrm),          intent(in   ) :: ctrl
!     !local
!     logical :: rc
!     integer :: res
!     integer :: i
!     integer :: stderr, stdout
!     integer :: nequ, nterm, max_time_it
!     real(kind=double) :: dnrm2
!     real(kind=double),allocatable :: test(:,:) 

    
!     ndeg        = spec%ndeg
!     nequ        = ( spec%ndeg + 1 )**2 - 1
!     nterm       = (nequ+1) * nequ /2


!     this%ntdens      = grid%grid%ncell
!     this%npot        = ( ndeg + 1 )**2 
!     this%ngrad       = ( ndeg + 1 )**2 
!     this%nterm       = nterm

!     !local grid
!     this%grid => grid
      
!     this%spec => spec
    
!     !
!     ! Build intevals
!     !
!     allocate(this%inters( grid%ninter), stat = res) 
!     if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'system_tdenspot', &
!          ' member inters',res)
!     do i=1, grid%ninter
!        call this%inters(i)%init(stderr, &
!             grid%grid%coord(2,i), grid%grid%coord(2,i+1),&
!             this%spec,this%gauss_cell)
!     end do

    
!     ! elliptic system variables
!     allocate (&
!          this%stiff( nterm ),&
!          stat=res)
!     if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_tdpotsys', &
!          'member  stiff ',res)


!     if ( ctrl%id_heavy == 1 ) then       
!        allocate (&
!             this%stiff_matrices(this%ntdens,this%nterm),&
!             stat=res)
!        if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_tdpotsys', &
!             ' member stiff matrices',res)
!        call this%build_stiffs(stderr,stdout)
!     end if 

!     ! 
!     ! work array
!     ! 
!     allocate(&
!          this%scr_npot(this%npot),&
!          this%scr_ntdens(this%ntdens),&
!          this%scr_ngrad(this%ngrad),&
!          stat=res)
!     if(res .ne. 0) rc = IOerr(stderr, err_alloc, 'init_tdpotsys', &
!          ' type tdpotsys member tdens_prj'//&
!          ' scr_ntdens scr_npot scr_ngrad sqrt_diag')


!   end subroutine init_specp0


  

!   subroutine build_stiffs(this,lun_err,lun_out)
!     use Globals
!     implicit none
!     class(specp0), intent(inout) :: this
!     integer,         intent(in   ) :: lun_err, lun_out
!     ! local
!     logical :: rc
!     integer :: res
!     integer :: icell ,ncell, nterm, ndeg
!     real(kind=double) :: tdens_loc
!     real(kind=double) :: ax,bx, ay, by
!     real(kind=double), allocatable :: stiff_loc(:)
!     real(kind=double), allocatable :: x_CTP(:,:),x_CUP(:,:)
!     real(kind=double), allocatable :: y_CTP(:,:),y_CUP(:,:)


!     ndeg  = this%spec%ndeg
!     nterm = this%spec%nterm
!     ncell = this%grid%grid%ncell
!     !write(*,*) ndeg, this%nequ, nterm, ncell

!     this%stiff=zero
!     allocate(stiff_loc( nterm ),&
!          x_CTP( ndeg+1, ndeg+1),&
!          x_CUP( ndeg+1, ndeg+1),&
!          y_CTP( ndeg+1, ndeg+1),&
!          y_CUP( ndeg+1, ndeg+1),&         
!          stat=res)
!     if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
!          ' local array stiff_loc x_CTP, x_CUP, y_CTP, y_CUP',res)

!     do icell = 1, ncell
!        if (mod( icell, 50 ) == 0 ) then
!           write(lun_out,*) 'Assembled cells = ',icell 
!        end if

!        call this%grid%get_interval_from_cell(icell,ax,bx,ay,by)
!        call this%spec%eval_CTPCUP2(ndeg,ax,bx,x_CTP,x_CUP) 
!        call this%spec%eval_CTPCUP2(ndeg,ay,by,y_CTP,y_CUP)
       

!        call build_stiff_cell( nterm, ndeg,&
!             stiff_loc,&
!             x_CTP,x_CUP,y_CTP,y_CUP, &
!             this%spec)
! !!$       call build_stiff_test( nterm, ndeg,&
! !!$            stiff_loc,&
! !!$            x_CTP,x_CUP,y_CTP,y_CUP, &
! !!$            this%spec)
!        call dcopy(nterm,&
!             stiff_loc(1),1,&
!             this%stiff_matrices(icell,1),ncell)
! !!$       this%stiff_matrices(icell,:) = stiff_loc(:)
       
!     end do    
!     deallocate(&
!          stiff_loc,&
!          x_CTP,&
!          x_CUP,&
!          y_CTP,&
!          y_CUP,&
!          stat=res)
!     if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
!          ' local array stiff_loc ',res)
!     contains
      

!   end subroutine build_stiffs

!     subroutine assembly_stiff(this,tdens)
!     use Globals
!     use omp_lib
!     implicit none
!     class(specp0), target, intent(inout) :: this
!     real(kind=double),          intent(in   ) :: tdens(this%ntdens)
!     !local
!     integer k,i,ncell,nterm
!     real(kind=double) :: ddot
    
! !!$    !openmp vars
! !!$    !$ integer :: Num_Threads,Thread_ID,Max_Threads
! !!$    !$ logical :: dynamic_NT
! !!$
! !!$    real(kind=double), pointer :: point_stiff(:)
! !!$
! !!$    real(kind=double), allocatable :: loc_stiff(:),loc_tdens(:)
! !!$    real(kind=double), allocatable :: loc_stiff_matrices(:,:)
! !!$    real(kind=double), allocatable :: loc_loc_stiff(:)
! !!$
! !!$    real(kind=double) :: scratch
! !!$    
! !!$    point_stiff => this%stiff
! !!$
!     nterm = this%nterm 
!     ncell = this%ntdens
! !!$
! !!$    allocate(loc_stiff(nterm),loc_tdens(ncell),loc_stiff_matrices(ncell,nterm))
! !!$    allocate(loc_loc_stiff(nterm))
! !!$
! !!$    loc_stiff = zero
! !!$    loc_tdens = this%tdens
! !!$    loc_stiff_matrices = this%stiff_matrices
! !!$
! !!$    !$OMP PARALLEL &
! !!$    !$OMP PRIVATE(Max_Threads,Num_Threads,Thread_ID,k,i,&
! !!$    !$OMP         loc_stiff,scratch) &
! !!$    !$OMP SHARED(nterm,ncell,loc_tdens,loc_stiff_matrices,loc_loc_stiff)
! !!$    !$ Max_Threads=OMP_get_max_threads()
! !!$    !$ Num_Threads=OMP_get_num_threads()
! !!$    !$ Thread_ID=OMP_get_thread_num()
! !!$    !$ dynamic_NT=OMP_get_dynamic()
! !!$    !$ write(200+Thread_ID,'(3(a,i2),a,l2)') &
! !!$    !$     ' proc ',Thread_ID,' of ',Num_Threads-1,&
! !!$    !$     ' (max ',Max_Threads-1,') dyn_par=',dynamic_NT
! !!$    !$ write(300+Thread_ID,'(5e12.4)') loc_tdens
! !!$    !$OMP DO
! !!$    do k = 1, nterm 
! !!$       !$ write(400+Thread_ID,'(I5)') k
! !!$       !$ write(400+Thread_ID,'(5e12.4)') loc_stiff_matrices(:,k)
! !!$       !loc_loc_stiff(k) = ddot(ncell, &
! !!$       !     loc_tdens(1), 1, &
! !!$       !     loc_stiff_matrices(1,k),1)
! !!$       loc_loc_stiff(k)=zero
! !!$
! !!$       do i=1,ncell
! !!$          scratch=loc_loc_stiff(k) + loc_tdens(i)*loc_stiff_matrices(i,k)
! !!$       !$ write(500+Thread_ID,'(2i5,4e12.5)') k,i,&
! !!$       !$          scratch,loc_stiff(k),loc_tdens(i),loc_stiff_matrices(i,k)
! !!$          loc_loc_stiff(k) = scratch
! !!$       end do
! !!$       !loc_loc_stiff(k) = loc_stiff(k)
! !!$    end do
! !!$    !$OMP END DO
! !!$    !loc_loc_stiff = loc_stiff
! !!$    !$OMP END PARALLEL
! !!$    this%stiff = loc_loc_stiff
! !!$
! !!$    write(600,'(5e12.4)') this%stiff
! !!$    write(700,'(5e12.4)') loc_loc_stiff
! !!$    call dgemv('T', ncell, nterm,&
! !!$         one,&
! !!$         this%stiff_matrices, ncell,&
! !!$         this%tdens, 1, &
! !!$         zero,&
! !!$         this%stiff,1)
            
! !!$    deallocate(loc_stiff,loc_tdens,loc_stiff_matrices)
! !!$    deallocate(loc_loc_stiff)

!     do i = 1, this%nterm
!        this%stiff(i) = ddot(ncell, tdens,1, this%stiff_matrices(1,i),1)
!     end do


!     call tria2full(this%nequ,this%stiff,this%stiffness_matrix)
!   contains

!     subroutine tria2full(n,tria,this)
!         use Globals
!         implicit none
!         integer,           intent(in   ) :: n 
!         real(kind=double), intent(in   ) :: tria(n*(n+1)/2)
!         type(fullmat),     intent(inout) :: this
!         ! local
!         integer i,size,start

!         !ENRICO meglio azzerare fuori una sola volta
!         this%coeff = zero
!         this%is_symmetric  = .True.
!         this%triangular = 'N'
!         start = 1 
!         size  = 0
!         do i = 1, n
!            start = start + size
!            size  = n + 1 - i
!            call dcopy(size,tria(start),1,this%coeff(i,i),1)
!         end do 
!         start = 1 
!         size  = 0
!         do i = 1, n-1
!            start = start + size
!            size  = n + 1 - i
!            call dcopy(size-1,tria(start+1),1,this%coeff(i,i+1),n)
!         end do
     
        
!       end subroutine tria2full
       

!   end subroutine assembly_stiff

!   subroutine build_stiff_cell(nterm,ndeg,&
!        stiff_loc,&
!        x_CTP,x_CUP,y_CTP,y_CUP,&
!        spec)
!     use Globals

!     use Spectral
!     implicit none
!     integer,           intent(in   ) :: nterm, ndeg
!     real(kind=double), intent(inout) :: stiff_loc( nterm ) 
!     real(kind=double), intent(in   ) :: x_CTP( ndeg+1, ndeg+1 )
!     real(kind=double), intent(in   ) :: x_CUP( ndeg+1, ndeg+1 )
!     real(kind=double), intent(in   ) :: y_CTP( ndeg+1, ndeg+1 ) 
!     real(kind=double), intent(in   ) :: y_CUP( ndeg+1, ndeg+1 )
!     type(spct),        intent(in   ) :: spec
!     !local
!     integer i,j,k,m

!     k=0
!     do j = 2, ( ndeg+1)**2  
!        do i = j, (ndeg+1)**2
!           k = k + 1
!           stiff_loc(k) = &
!                ddx(i,j,ndeg) *  &
!                x_CUP(&
!                spec%dx(i,ndeg)+1, &
!                spec%dx(j,ndeg)+1  ) * &
!                y_CTP( &
!                spec%dy(i,ndeg)+1, &
!                spec%dy(j,ndeg)+1  ) &
!                + &
!                ddy(i,j,ndeg) * &
!                y_CUP(&
!                spec%dy(i,ndeg)+1, &
!                spec%dy(j,ndeg)+1  ) * &
!                x_CTP(&
!                spec%dx(i,ndeg)+1, &
!                spec%dx(j,ndeg)+1  )

!        end do
!     end do

!   contains
!     function ddx(i,j,ndeg)
!       implicit none
!       integer :: i, j, ndeg
!       integer :: ddx
!       ddx = ( (i-1) / (ndeg+1) ) * ( (j-1) / (ndeg+1) )
!     end function ddx
!     function ddy(i,j,ndeg)
!       implicit none
!       integer :: i, j, ndeg
!       integer :: ddy
!       ddy = ( (mod(i-1, ndeg+1) ) * (mod(j-1,ndeg+1)) )
!     end function ddy
!   end subroutine build_stiff_cell

  
!   subroutine build_int_nrm2grad(nequ, nquad, nterm,&
!        coeff, stiff_matrices, int_nrm2grad)
!     use Globals
!     implicit none
!     integer,           intent(in ) :: nequ
!     integer,           intent(in ) :: nquad
!     integer,           intent(in ) :: nterm
!     real(kind=double), intent(in ) :: coeff(nequ+1)
!     real(kind=double), intent(in ) :: stiff_matrices(nquad, nterm)
!     real(kind=double), intent(out) :: int_nrm2grad(nquad)
!     !local
!     integer :: i,j,m
!     real(kind=double) :: ui,alpha
        
!     int_nrm2grad = zero
!     m=0
!     do i=2,nequ+1
!        m=m+1
!        ui=coeff(i)
!        !diagonal terms
!        alpha=ui**2       
!        call daxpy(nquad, alpha, stiff_matrices(1,m),1,int_nrm2grad,1)
!        ! extra diagonal
!        do j = i+1,nequ+1
!           m = m+1
!           alpha = 2.0d0 * ui * coeff(j)
!           call daxpy(nquad, alpha, stiff_matrices(1,m),1,int_nrm2grad,1)
!        end do
!     end do
!   end subroutine build_int_nrm2grad

!   subroutine syncronize_at_tzero( this,&
!        tdpot,&
!        ode_inputs,&
!        ctrl,&
!        info)
!     use DmkOdeData
!     use ControlParameters 
!     implicit none
!     class(specp0), intent(inout) :: this
!     type(tdpotsys),     intent(inout) :: tdpot
!     type(OdeData),      intent(in   ) :: ode_inputs
!     type(CtrlPrm),      intent(in   ) :: ctrl
!     integer,            intent(inout) :: info
!     ! local
!     integer :: lun_err,lun_out,lun_stat  
!     integer :: ntdens, npot
!     type(fullmat) :: copy_stiff
    
!     npot   = tdpot%npot
!     ntdens = tdpot%ntdens
    
!     ! 2.1 - Assign data 
!     tdpot%tdens   = ode_inputs%tdens0
!     if ( abs( ode_inputs%pode(1)-2.0d0 ) < small ) then
!        call tdpot%tdens2gfvar(tdpot%ntdens,2,ode_inputs%pflux(1),&
!             tdpot%tdens,tdpot%gfvar)
       
!     end if
!     tdpot%pot = zero

!     !
!     ! Assembly stiffness matrix 
!     !
!     !call this%CPUtim%STF%set('start')
!     call this%assembly_stiff(tdpot%tdens)
!     !call this%CPUtim%STF%set('stop')

!     !
!     ! - copy matrix and rhs 
!     ! - solver
!     !- get the solution
!     !
!     copy_stiff=this%stiffness_matrix
!     this%scr_npot(:) = ode_inputs%rhs_integrated(:,2)
!     call dposv('L',this%nequ, 1, copy_stiff%coeff,&
!           this%nequ, this%scr_npot(2:this%npot), this%nequ, info)
    
!     tdpot%pot(1)           = zero
!     tdpot%pot(2:this%npot) = this%scr_npot(2:this%npot)
    
!     ! zero mean 
! !!$    this%pot(1) = - ddot(nequ,&
! !!$         this%pot(2:nequ+1),1,&
! !!$         this%spec%integral_base(2:nequ+1),1)&
! !!$         /4.0d0
! !!$    write(*,*) 'mean0', ddot(nequ+1,&
! !!$         this%coeff(1:nequ+1),1,&
! !!$         this%spec%integral_base(1:nequ+1),1)




!   end subroutine syncronize_at_tzero

!   !>------------------------------------------------
!   !> Procedure for computation of next state of system
!   !> ( all variables ) given the preovius one
!   !> ( private procedure for type tdpotsys, used in update)
!   !> 
!   !> usage: call var%update(stderr,itemp)
!   !> 
!   !> where:
!   !> \param[in ] stderr -> Integer. I/O err. unit
!   !> \param[in ] itemp   -> Integer. Time iteration
!   !<---------------------------------------------------
!   subroutine update(this,&
!        lun_err,lun_out,lun_stat,&
!        tdpot,&
!        ode_inputs,&
!        ctrl,&
!        deltat,&
!        CPU,&
!        info)
!     use ControlParameters
!     use Timing
!     use MuffeTiming
!     implicit none
!     type(specp0),   intent(inout) :: this    
!     integer,        intent(in   ) :: lun_err,lun_out,lun_stat
!     integer,         intent(inout) :: info
!     type(tdpotsys),             intent(inout) :: tdpot
!     type(odedata),              intent(in   ) :: ode_inputs
!     type(CtrlPrm),              intent(inout) :: ctrl
!     type(codeTim),              intent(inout) :: CPU
    
    
!     ! local
!     real(kind=double) :: decay,pflux,pmass,pode,time,tnext
!     integer :: newton_initial
!     integer :: ntdens, npot
!     integer :: info_inter,passed_reduced_jacobian
!     type(tim) :: wasted_temp
!     character(len=256) :: str,msg,method,result_update
!     integer :: slot
    
!     ntdens = this%ntdens
!     npot   = this%npot
        
!     this%time_iteration=current_time_iteration
!     !
!     ! copy before update
!     !
!     this%tdens_old = this%tdens
!     this%gfvar_old = this%gfvar
!     this%pot_old   = this%pot
        
    
!     select case (ctrl%id_time_discr)
!     case (1)
!        write(method,'(a)') ' EXPLICIT EULER'  
!     end select
!     write(str,'(a,1ep9.2)') ' UPDATE begin | Time step = ',deltat
!     write(msg,'(a)') ctrl%sep(str) 
!     write(lun_out,*) etb(msg)
!     write(lun_stat,*) etb(msg)
!     write(str,'(a)') etb(method//' begin ')
!     write(msg,'(a)') ctrl%sep(str) 
!     write(lun_out,*) etb(msg)
!     write(lun_stat,*) etb(msg)



!     ! update all spatial variables at itemp+1
!     select case (  ctrl%id_time_discr )
!     case (1)
!        call explicit_euler(&
!             lun_err,info,&
!             tdpot,&
!             ode_inputs,&
!             ctrl,&
!             specp0,&
!             deltat,&
!             current_time,&
!             CPU)
         
!     case default



!     end select

!     if ( info .eq. 0)  then
!        write(result_update,'(a)') ' successed'  
!     else
!        write(result_update,'(a)') ' failed'
!     end if
!     write(str,'(a)') etb(method//result_update)
!     write(msg,'(a)') ctrl%sep(str) 
!     write(lun_out,*) etb(msg)
!     write(lun_stat,*) etb(msg)

!   end subroutine update

!   !>----------------------------------------------------
!   !> Procedure for update the system with Explicit Euler
!   !> IMPORTANT all the variables tdens pot 
!   !> odein have to be syncronized at time time(itemp)
!   !>
!   !> usage: call var%explicit_euler(stderr,itemp)
!   !> 
!   !> where:
!   !> \param[in ] stderr -> Integer. I/O err. unit
!   !> \param[in ] itemp   -> Integer. Time iteration
!   !<---------------------------------------------------    
!   subroutine explicit_euler(this,&
!        lun_err,info,&    
!        tdpot,&
!        ode_inputs,&
!        ctrl,&
!        deltat,&
!        current_time,&
!        CPU)
!       use Globals 
!       use ControlParameters
!       use Timing

!       implicit none
!       class(specp0), intent(inout) :: this
!       integer,           intent(in   ) :: lun_err
!       integer,           intent(inout) :: info
!       type(tdpotsys),    intent(inout) :: tdpot
!       type(odedata),     intent(in   ) :: ode_inputs
!       type(CtrlPrm),     intent(in   ) :: ctrl
!       real(kind=double), intent(in   ) :: deltat
!       real(kind=double), intent(in   ) :: current_time
!       type(codeTim),     intent(inout) :: CPU
 
!       !local
!       integer :: icell
!       integer :: ntdens,npot
!       real(kind=double) :: delta,dnrm2
      
!       ntdens = this%ntdens
!       npot   = this%npot

!       if ( .not. this%all_syncr)  then
!          write(lun_err,*) 'Not all varibles are syncronized'
!          stop
!       end if

     
!       !
!       ! Compute rhs ode and tdens increment
!       !
!       !
!       ! $ \int_{\Domain} ( rhs_ode )\chi_{k}(x) $
!       !
!       call specp0%assembly_rhs_ode(&
!          ode_inputs,&
!          this,&
!          specp0%scr_ntdens)
!       ! scale by the mass matrix con the Finite Elements used for tdens
!       call specp0%get_increment_ode(specp0%scr_ntdens,tdpot%scr_ntdens)

      
!       !
!       ! tdens update 
!       !
!       tdpot%tdens = tdpot%tdens + deltat * tdpot%scr_ntdens
!       do icell = 1, tdpot%ntdens
!          tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
!       end do
      
!       ! Store local var_tdens
!       tdpot%loc_var_tdens = specp0%eval_var_tdens(tdpot,deltat)


!       !
!       ! update $\Pot$ input varible at time $t^{\tstep+1}$
!       ! solveing -\Div( \Tdens^{k+1} \Grad \Pot ) = \Forcing
!       !
!       this%scr_ntdens = this%tdens + ode_inputs%lambda
!       call this%assembly_stiff(this%scr_ntdens)
!       ! copy because works in place
!       this%copy_stiff=this%stiffness_matrix
!       this%scr_npot(:) = ode_inputs%rhs_forcing(:)
!       call dposv('L',this%nequ, 1, copy_stiff%coeff,&
!            this%nequ, this%scr_npot(2:this%npot), this%nequ, info)
!       info = this%info_solver%ierr 
!       if ( info .ne. 0) return
      
!       tdpot%pot(1)           = zero
!       tdpot%pot(2:this%npot) = this%scr_npot(2:this%npot)

!       !
!       ! store information for linear system solution
!       !
! !!$      this%sequence_info_solver(1)=this%info_solver
! !!$      this%iter_nonlinear = 1
! !!$      this%sequence_build_prec(1)=ctrl%build_prec
! !!$      this%sequence_build_tuning(1)=ctrl%build_tuning

!     end subroutine explicit_euler


!     !>------------------------------------------------------------
!     !> Assembly the rhs of the tdens ode of gfvar ode
!     !<-------------------------------------------------------------
!     subroutine assembly_rhs_ode_tdens(this,&
!          ode_inputs,&
!          tdpot,&
!          rhs_ode)
!       implicit none
!       class(specp0), intent(in   ) :: this
!       type(OdeData),     intent(in   ) :: ode_inputs
!       type(tdpotsys),    intent(in   ) :: tdpot
!       real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 

!       !local
!       integer :: icell,j
!       integer :: ntdens
!       real(kind=double) :: min,max, pflux, pmass,decay
!       real(kind=double) :: ptrans, gf_pflux, gf_pmass
!       real(kind=double) :: penalty_factor

!       ntdens = this%ntdens


!       rhs_ode = zero

!       pflux = ode_inputs%pflux
!       decay = ode_inputs%decay
!       pmass = ode_inputs%pmass

!       call build_int_nrm2grad(&
!            this%nequ, this%grid%grid%ncell, this%nterm,&
!            tdpot%pot(2:this%npot), this%stiff_matrices, &
!            this%int_nrm2grad)

!       rhs_ode = tdpot%tdens ** pflux * this%int_nrm2grad &
!            - decay * odeinputs%kappa * tdpot%tdens ** pmass * this%grid%sizecell 

!     end subroutine assembly_rhs_ode_tdens

!     !>------------------------------------------------------------
!     !> Assembly the rhs of the tdens ode of gfvar ode
!     !<-------------------------------------------------------------
!     subroutine get_increment_ode(this, rhs_ode,inc_ode)
!       implicit none
!       class(specp0), intent(in   ) :: this
!       real(kind=double), intent(in   ) :: rhs_ode(this%ntdens)
!       real(kind=double), intent(inout) :: inc_ode(this%ntdens)

!       inc_ode = rhs_ode / this%grid%grid%size_cell

!     end subroutine get_increment_ode



!   end module SpectralP0
