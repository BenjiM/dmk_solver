!>---------------------------------------------------------------------
!> 
!> MUFFE for the solution of Optimal Transport problems
!>
!> \author{Enrico Facca and Mario Putti}
!>
!> DESCRIPTION: 
!> The main program 
!> 
!>
!> REVISION HISTORY:
!> 22 Lug 2016 - Initial Version
!>
!> TODO_dd_mmm_yyyy - TODO_describe_appropriate_changes - TODO_name
!<---------------------------------------------------------------------

PROGRAM muffa 
  use Globals
  use Timing
  use TimeInputs
  use IOdefs
  use DmkOdeData
  use DataSequence
  use ControlParameters
  use MuffeTiming
  use Geometry2d
  use AbstractGeometry
  use P1Galerkin
  use TdensPotentialSystem
  use TimeFunctionals
  use SparseMatrix
  use StdSparsePrec
  use OdeInputs
  use Matrix
  use LinearSolver
  use StdSparsePrec
    
  implicit none
  
  type(IOfdescr) :: IOfiles
  type(CtrlPrm)  :: ctrl_read,ctrl
  type(codeTim)  :: CPU
  type(abs_simplex_mesh)     :: grid,subgrid
  type(tdpotsys) :: tdpot
  type(evolfun)  :: timefun 
  type(output_solver)  :: info_solver
  type(Tim) :: wasted_temp
  type(file) :: file_out,fout
  integer  :: info_prec, passed_reduced_jacobian
  type(OdeData) :: ode_inputs
  type(p1p0_space_discretization) :: p1p0
  !> Constant varibles of the ODE
  !> For P1(grid_pot)/P0(grid_tdens) 
  type(OdeInp)   :: odein
  type(spmat) :: gradx,grady
  
  ! varaible for projection
  type(p1gal) :: p1_grid
  type(spmat) :: stiff_grid
  real(kind=double), allocatable :: pot_grid(:), rhs_grid(:)
  type(TimeData) :: rhs_integrated
  type(stdprec) :: prec_stiff

  logical :: rc
  logical :: test_var
  logical :: test_maxit
  logical :: in_cycle
  logical :: endfile
  
  integer :: lun_err,lun_out,lun,res,lun_stat
  integer :: i, inode_sub, i1,i2,itemp,k,iloc,jloc
  integer :: nrestart, nrestart_max
  integer :: int_before_dat,  int_before_matrix
  integer :: max_time_it
  integer :: nupdate

  integer :: itemp_prec_calc
  integer :: total_kryvol=0, wasted_krylov=0
  integer :: total_update=0

  
  integer :: info,info_update

  integer :: npot, ntdens

  real(kind=double)              :: deltat
  real(kind=double), allocatable :: time(:)
  real(kind=double), allocatable :: var_tdens(:)
  real(kind=double), allocatable :: var_tdens_linfty(:)


  integer, parameter :: nsequence=2

  real(kind=double) :: ddot,dnrm2

  real(kind=double) :: tol_zero,pode
  integer  :: ndir=1,flag
  integer,allocatable:: noddir(:)
  
    

  character(len=256) :: fname,folder,msg
  character(len=256) :: out_format,str
  
  !
  ! Initialization of Timing module
  !
  call CPU%init()
  call CPU%TOT%set('start')
  call CPU%OVH%set('start')

   
  !
  ! Initialization of INPUT_OUTPUT module
  !
  write(6,*) 'Init INPUT/OUTPUT files'
  call IOfiles%init()
  lun_err   = IOfiles%stderr%lun
  lun_out   = IOfiles%stdout%lun
  lun_stat = IOfiles%statistic%lun
  write(6,*) 'All files are open'

  
  !
  ! Initialization of tdpot, ctrl, ode_inputs, grid/subgrid from file
  !
  call init_p1p0_muffe_from_files(IOfiles,ctrl_read, odein, grid, subgrid)

  !
  ! Initializate spatail discretization variable 
  ! from grid subgrid
  !
  if ( ctrl_read%id_subgrid == 1 ) then       
     call p1p0%init(&
          IOfiles%stderr%lun,IOfiles%stdout%lun,IOfiles%statistic%lun,&
          ctrl_read,&
          ctrl_read%id_ode,&
          ctrl_read%id_subgrid,grid, subgrid)
  else
     call p1p0%init(&
          IOfiles%stderr%lun,IOfiles%stdout%lun,IOfiles%statistic%lun,&
          ctrl_read,&
          ctrl_read%id_ode,&
          ctrl_read%id_subgrid,grid, grid)
  end if

  if (ctrl_read%id_save_matrix > 0) then 
     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/stiff.dat')
     lun=IOfiles%linear_sys%lun
     open(lun,file=fname)
     call p1p0%stiff%write(lun,'matlab')
     close(lun)
     
     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/assembler_stiff.dat')   
     open(lun,file=fname)
     do i=1,p1p0%grid_pot%ncell
        do iloc=1,p1p0%grid_pot%nnodeincell
           do jloc=1,p1p0%grid_pot%nnodeincell
              write(lun,*) p1p0%p1%assembler_csr(jloc,iloc,i)
           end do
        end do
     end do
     close(lun)

     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/matrix_B.dat')
     open(lun,file=fname)
     call p1p0%B_matrix%write(lun,'matlab')
     close(lun)
  
     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/assembler_B.dat')
      open(lun,file=fname)
     do i=1,p1p0%grid_pot%ncell
        write(lun,*) p1p0%assembler_Bmatrix_subgrid(:,i)
     end do
     close(lun)

     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/sizesubcell.dat')
     open(lun,file=fname)
     call write_steady(6,lun,p1p0%grid_pot%ncell,p1p0%grid_pot%size_cell)
     close(lun)

     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/sizecell.dat')
     open(lun,file=fname)
     call write_steady(6,lun,p1p0%grid_tdens%ncell,p1p0%grid_tdens%size_cell)
     close(lun)

     call p1p0%p1%gradbase2spmat(lun_err,gradx,grady)
     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/gradx.dat')
     open(lun,file=fname)
     call gradx%write(lun,'matlab')
     close(lun)

     fname=etb(etb(IOfiles%output_folder%fn)//'linsys'//'/grady.dat')
     open(lun,file=fname)
     call grady%write(lun,'matlab')
     close(lun)

     call gradx%DxM(lun_err, p1p0%grid_pot%size_cell)
     call grady%DxM(lun_err, p1p0%grid_pot%size_cell)

     
     
     call gradx%kill(lun_err)
     call grady%kill(lun_err)

     
     
  end if



  !
  ! init ode_inputs containg all inputs data
  !
  call ode_inputs%init(lun_err,  p1p0%ntdens, p1p0%npot)
  ode_inputs%lambda(:)=ctrl_read%lambda
  

  !
  ! initialized tdens-potential type
  !
  call tdpot%init(&
       IOfiles%stderr%lun,IOfiles%stdout%lun,IOfiles%statistic%lun,&
       p1p0,ctrl_read)

  
  !
  ! local copy
  !
  ntdens    = tdpot%ntdens
  npot      = tdpot%npot


  !
  ! 4 - Allocate array time, var_tdens and time functional
  !
  allocate(&
       time(0:ctrl_read%max_time_it),&
       var_tdens(0:ctrl_read%max_time_it),&
       var_tdens_linfty(0:ctrl_read%max_time_it),&
       stat=res)
  if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'main', &
         ' error alloc. time var_tdens',res)
  time      = zero
  var_tdens = zero
  var_tdens(0) = huge
  var_tdens_linfty = zero
  var_tdens_linfty(0) = huge

  call timefun%init(lun_err,&
       ctrl_read%max_time_it, ctrl_read%max_iter_nonlinear)
  !
  write(lun_stat,'(a)') ctrl_read%sep('setup data ended')
  write(lun_out,'(a)') ctrl_read%sep('setup data ended')


  !
  ! create a work copy use to change orignal parameters
  !
  ctrl = ctrl_read
  
  !
  ! Time evolution starts
  !
  write(lun_stat,'(a)') ctrl_read%sep('time evolution begins')
  write(lun_out,'(a)') ctrl_read%sep('time evolution begins')
  

  !
  ! Time=tzero
  !
  itemp=0
  time(itemp)=odein%tzero
  max_time_it=ctrl%max_time_it
  in_cycle=.true.

  !
  ! Start evolution syncronazing tdens system and ode's inputs
  !
  !  
  ! 1 - Eval ODE input varible at time $tzero$
  ! Pflux, Pmass, Kappa, Decay and  Rhs_integrated kappa
  !
  msg =  ctrl%sep('Start Tdens/Pot syncronization at tzero}')
  write(lun_stat,*) etb(msg)
  write(lun_out,* ) etb(msg)
  !
  ! read data and cpoy them into ode_inputs
  !
  call odein%set(IOfiles,time(itemp),endfile)
  call odein%set_odedata(&
             1, time(itemp),&
             ctrl%id_ode,&
             ode_inputs)
  !
  ! Syncronize at tzero
  !
  ctrl%build_prec=1
  ctrl%ctrl_solver%scheme='PCG'
  ctrl%ctrl_prec%prec_type='IC'
  
  call tdpot%syncronize_at_tzero(info,&
       lun_err,lun_out, lun_stat,&
       p1p0,&
       ode_inputs,&
       ctrl, &
       CPU)
  ctrl%ctrl_solver= ctrl_read%ctrl_solver
  ctrl%ctrl_prec  = ctrl_read%ctrl_prec
  call p1p0%evaluate_functionals(tdpot,ode_inputs)
  call timefun%set_evolfun(itemp,tdpot,p1p0,ode_inputs,ctrl,CPU)
  msg =  ctrl%sep('Tdens/Pot syncronized at tzero}')
  write(lun_stat,*) etb(msg)
  write(lun_out,* ) etb(msg)
 
  !
  ! Info: Time=tzero,  time_fucntionals (basic) 
  !
  write(lun_stat,'(a)')' '
  call info_muffe(lun_stat,ctrl,itemp, time(itemp),zero, timefun,huge,huge,0,CPU)
  call info_muffe(lun_out,ctrl,itemp, time(itemp),zero, timefun,huge,huge,0,CPU)
 
  !
  ! Save data to file 
  !
  call CPU%SAVE%set('start')
  call evol2dat(tdpot,&
       ctrl,&
       .True.,&           
       itemp, &
       int_before_dat, int_before_matrix, &
       time(itemp),&
       -1.0d0,&
       IOfiles)
  call CPU%SAVE%set('stop')

  
  !
  ! Start time cycle
  !
  in_cycle    = ( max_time_it > 0) 
  deltat      = ctrl%deltat
  nupdate     = 0
  
  do while (in_cycle )
     if ( ctrl%build_prec .eq. 1 ) itemp_prec_calc = itemp
     !
     ! Update spatial variables.
     ! First save $\Tdens^\tstep$ and $\Pot^\tstep$, then 
     ! Update sytem tdens-pot, and all variable inside
     ! (gradients, ode input variable ) at next time iteration
     ! After this sobroutine ALL spatial variables need to valued
     ! at $t^{\tstepp}=t^{\tstep}+\Deltat$
     !--------------------------------------------------------
     itemp = itemp + 1
     write(lun_out,'(a)') ' '
     write(lun_stat,'(a)') ' '
     write(lun_out,'(a)') ' '
     write(lun_stat,'(a)') ' ' 
     write(str, '(a,I5)') 'UPDATE begin: itemp= ', itemp
     write(lun_stat,'(a)') ctrl%sep(etb(str))
     write(lun_out,'(a)') ctrl%sep(etb(str))
     
     info_update = -1
     nrestart = 0     
     
     if ( ctrl%debug .eq. 1) write(lun_out,*) 'Set controls before update'
     !
     ! set all controls for update system
     !
     call  set_controls_update (&
          itemp,&
          deltat,&
          tdpot, &
          p1p0,&
          ode_inputs,&
          timefun,&
          ctrl_read,&
          ctrl)

     do while ( ( info_update .ne. 0 ) .and. (nrestart < ctrl%nrestart_max) ) 
        !
        ! read new data and copy them into second slot of ode_inputs 
        !
        ! TODO : 
        ! this implementation can lead to some problems 
        ! for time varying data in case we shrink too much the time step.
        ! A possible solution can be storing a backup version of ode_inputs  
        ! 
        if ( ctrl%debug .eq. 1) write(lun_out,*) 'Read Inputs from file'
        call odein%set(IOfiles,time(itemp-1)+deltat,endfile)
        call odein%set_odedata(&
             2,  time(itemp-1)+deltat,&
             ctrl%id_ode,&
             ode_inputs)

        !
        ! update system
        !
        wasted_temp = CPU%WASTED
        call CPU%ALGORITHM%set('start')
        call CPU%WASTED%set('start')
        call tdpot%update(&
             lun_err,lun_out,lun_stat,&
             ctrl,&
             deltat,&
             itemp,&
             time(itemp-1),&
             CPU,info_update,&
             p1p0,&
             ode_inputs)
        call CPU%WASTED%set('stop') 
        call CPU%ALGORITHM%set('stop')

        if ( info_update .ne. 0 ) then
           if (ctrl%id_time_discr .eq. 1) exit
           
           !
           ! in case of failure 
           ! 1- recover data
           ! 2- reset controls
           !
           nrestart = nrestart + 1 

           !
           ! restores previous data
           !
           tdpot%tdens = tdpot%tdens_old 
           tdpot%pot   = tdpot%pot_old

           !
           ! reset controls for next attemp of update
           !
           write(*,*) 'info_update=', info_update
           call handle_failure_update ( lun_err,&
                info_update,&
                nrestart,&
                deltat,&
                p1p0,&
                tdpot, &
                ctrl_read,&
                ctrl) 
           write(*,*) 'build_prec=', ctrl%build_prec
        end if
     end do
     !
     ! reset original controls
     !
     ctrl = ctrl_read
     
     ctrl%deltat = deltat


     tdpot%nrestart_newton = nrestart
     nupdate = nupdate + 1 + nrestart
     
     !
     ! no time was wasted
     !
     if ( nrestart .eq. 0 ) then
        CPU%WASTED=wasted_temp
     end if

     !
     ! if update procedure failed break 
     !
     if ( info_update .ne. 0) then
        rc = IOerr(lun_err, wrn_inp, 'main', &
             ' UPDATE procedure failed')
        exit
     else
        !
        ! shift data from second to first slot, thus
        ! first slot and tdpot are syncronized
        !
        call ode_inputs%shift()
     end if
     !
     write(str, '(a,I5,a,I2,a)') &
          ' UPDATE end itemp= ', itemp, &
          ' nrestart=', nrestart
     write(lun_stat,'(a)') ctrl%sep(etb(str))
     write(lun_out,'(a)') ctrl%sep(etb(str))
     
     !
     ! Update time varying functionals
     ! Evaluate time, var_tdens and time funtionals
     !
     time(itemp) = time(itemp-1) + deltat  
     var_tdens(itemp) = p1p0%eval_var_tdens(tdpot,deltat)
     var_tdens_linfty(itemp) = p1p0%eval_var_tdens(tdpot,deltat,0.0d0)
     tdpot%loc_var_tdens = var_tdens(itemp)
     call p1p0%evaluate_functionals(tdpot,ode_inputs)
     call timefun%set_evolfun(itemp,tdpot,p1p0,ode_inputs,ctrl,CPU)

     !
     ! Info: Time, var_tdens,  time_fucntionals (basic) 
     !
     write(lun_stat,'(a)') ' ' 
     write(lun_out,'(a)') ' '
     call info_muffe(lun_stat,&
          ctrl,itemp, time(itemp),deltat, timefun, var_tdens(itemp),var_tdens_linfty(itemp),nupdate,CPU)
     call info_muffe(lun_out,&
          ctrl,itemp, time(itemp),deltat, timefun, var_tdens(itemp),var_tdens_linfty(itemp),nupdate,CPU)
     

     !
     ! if continuing cyclying     
     ! Test max time iterations or convergence achieved
     if ( ctrl%var_tdens .eq. 0) &
          test_var   = ( var_tdens_linfty(itemp) .gt. ctrl%tol_var_tdens )
     if ( ctrl%var_tdens .eq. 2) &
          test_var   = ( var_tdens(itemp) .gt. ctrl%tol_var_tdens )
     
     test_maxit = ( itemp            .lt. ctrl%max_time_it    )
     in_cycle   = ( test_var .and. test_maxit)
          
     

     !
     ! Save data 
     !
      call CPU%SAVE%set('start')
      call evol2dat(tdpot,&
          ctrl,&
          in_cycle,&           
          itemp, &
          int_before_dat, int_before_matrix, &
          time(itemp),&
          var_tdens(itemp),&
          IOfiles)
      call CPU%SAVE%set('stop')
  end do
  
  !
  !  kill local ctrl copy
  !
  call ctrl%kill()


  !>------------------------------------------------------
  !> Save time, var_tdens, time functional
  !>------------------------------------------------------
  call CPU%SAVE%set('start') 
  write(lun_out,*) ' '
  write(lun_stat,*) ' '
  write(lun_out,'(a)') ctrl%sep('saving time varying data to file')
  write(lun_stat,'(a)') ctrl%sep('saving time varying data to file')
    
  ! destination file
  folder=IOfiles%timefun%fn
  lun=IOfiles%timefun%lun

  ! time file
  fname=etb(etb(folder)//'/time.dat')
  call file_out%init(lun_err,fname, lun,'out')
  out_format=etb('('//etb(ctrl%rformat)//')')
  do i = 0, itemp
     write(lun,out_format) time(i)
  end  do 
  call file_out%kill(lun_err)


  ! time file
  fname=etb(etb(folder)//'/algorithm.dat')
  call file_out%init(lun_err,fname, lun,'out')
  out_format=etb('('//etb(ctrl%rformat)//')')
  do i = 0, itemp
     write(lun,out_format) timefun%CPU_time(i)
  end  do 
  call file_out%kill(lun_err)
  

  ! var_tdens
  fname=etb(etb(folder)//'/var_tdens.dat')
  call file_out%init(lun_err,fname, lun,'out')
  out_format=etb('('//etb(ctrl%rformat)//')')
  do i = 1, itemp
     !write(lun,out_format) time(i), var_tdens(i)
     write(lun,out_format) var_tdens(i)
  end  do
  call file_out%kill(lun_err)

  ! var_tdens
  fname=etb(etb(folder)//'/var_tdens_linfty.dat')
  call file_out%init(lun_err,fname, lun,'out')
  do i = 1, itemp
     write(lun,out_format) var_tdens_linfty(i)
  end  do
  call file_out%kill(lun_err)

  ! Saving other functional
  call timefun%write2dat(&
       lun_err,IOfiles%timefun%lun,IOfiles%timefun%fn,&
       itemp,time,&
       ctrl%iformat,ctrl%rformat)

  !
  ! write optimal configuration
  !

  call tdpot%write_opt(lun_err,ctrl,IOfiles%result)
  call CPU%SAVE%set('stop')
  !***********************************************************
  

  !***********************************************************
  !
  ! free memory
  !
  call CPU%OVH%set('start')
  write(lun_out,'(a)') ctrl%sep('free memory')
  write(lun_stat,'(a)') ctrl%sep('free memory')  
  deallocate(&
       time,&
       var_tdens,&
       stat=res)
  if(res .ne. 0) rc = IOerr(lun_err, err_dealloc , 'main', &
         'time var_tdens',res)
  call timefun%kill(lun_err)
  
  call kill_muffe(lun_err,&
       tdpot, grid,subgrid, ode_inputs, ctrl_read)
  
  call IOfiles%kill(lun_err)
  call CPU%OVH%set('stop')

  !***********************************************************
  

  ! Print timing statistics
  call CPU%TOT%set('stop')
  call CPU%info(lun_out, 'Global simulation times:')
  call CPU%info(lun_stat, 'Global simulation times:')

  call CPU%kill()


      
end PROGRAM muffa
  
subroutine info_muffe(lun,&
     ctrl,itemp, &
     current_time,deltat, timefun, current_var_tdens,current_var_linfty,nupdate,CPU)
  use Globals
  use ControlParameters
  use TimeFunctionals
  use MuffeTiming
  implicit none
  integer,                     intent(in) :: lun
  type(CtrlPrm),               intent(in) :: ctrl
  integer,                     intent(in) :: itemp
  real(kind=double),           intent(in) :: current_time
  real(kind=double),           intent(in) :: deltat
  type(evolfun),               intent(in) :: timefun 
  real(kind=double),           intent(in) :: current_var_tdens
  real(kind=double),           intent(in) :: current_var_linfty
  integer,                     intent(in) :: nupdate
  type(codeTim),               intent(in) :: CPU
  

  !local 
  character (len=256), parameter :: name_format='(a20)'
  character (len=256), parameter :: iformat='(I8)'
  character (len=256), parameter :: rformat='(1pe8.2)'
  character (len=256), parameter :: text_format='(a70)'
  character(len=256) :: out_format 
  character(len=3)   :: sep=' | '



  !
  ! Print iteration number, current_time and var_tdens
  ! 
  write(lun,'(a)') ctrl%sep('info evolution ')
  if ( current_var_tdens .lt. huge ) then 
     out_format='(a,I5,a,a,a,1pe9.2,a,a,a,1pe9.2,a,a,a,1pe9.2,a,a,a,1pe9.2)'
     write(lun,out_format) &
          'iter=', itemp,&
          sep,'time ',' = ', current_time,&
          sep,'deltat',' = ', deltat,& 
          sep,'var  ',' = ', current_var_tdens,&
          sep,'var infty ',' = ', current_var_linfty

  else
     out_format='(a,I5,a,a,a,1pe9.2)'!ctrl%formatting('(a,I5,a,a,a,1pe9.2)')
     write(lun,out_format) &
          'iter=', itemp,&
          sep,'time ',' = ', current_time
  end if
  write(lun,'(a)') ctrl%sep()
  
  !
  ! Print info functional at current_time
  !
  write(lun,'(a)') ctrl%sep('info system')
  call timefun%info(lun,1,ctrl,itemp)
  write(lun,'(a)') ctrl%sep(' info algoritm')
  call CPU%ALGORITHM%info(lun,'Algorithm Time : ')
  write(lun,*) 'time step =',itemp,' nupdate =', nupdate
  write(lun,'(a)') ctrl%sep()
end subroutine info_muffe


subroutine evol2dat(tdpot,&
     ctrl,&
     in_cycle,&           
     itemp, &
     int_before_dat, int_before_matrix, &
     current_time,&
     current_var,&
     IOfiles )
  use Globals
  use IOdefs
  use ControlParameters
  use TdensPotentialSystem
  implicit none
  type(tdpotsys),    intent(inout) :: tdpot
  type(CtrlPrm),     intent(inout) :: ctrl
  logical,           intent(in   ) :: in_cycle
  integer,           intent(in   ) :: itemp
  integer,           intent(inout) :: int_before_dat
  integer,           intent(inout) :: int_before_matrix
  real(kind=double), intent(in   ) :: current_time
  real(kind=double), intent(in   ) :: current_var
  type(IOfdescr),    intent(in   ) :: IOfiles

  !local 
  logical :: save_dat,save_matrix,rc
  character(len=256) :: fname
  integer :: lun,res,i
  integer :: id_save_dat, id_save_matrix
  integer :: freq_dat, freq_matrix
  integer :: ifile_dat, ifile_matrix


  id_save_dat     = ctrl%id_save_dat
  freq_dat        = ctrl%freq_dat
  ifile_dat       = ctrl%ifile_dat

  id_save_matrix  = ctrl%id_save_matrix
  freq_matrix     = ctrl%freq_matrix
  ifile_matrix    = ctrl%ifile_matrix


  call eval_save_test(save_dat,&
       in_cycle,&
       id_save_dat, freq_dat, &
       itemp, int_before_dat,&
       current_var)

  call eval_save_test(save_matrix,&
       in_cycle,&
       id_save_matrix, freq_matrix, &
       itemp, int_before_matrix, &
       current_var)

  if ( save_dat ) then
     call tdpot%write2dat(&
          'timedata',&
          itemp,&
          current_time,&
          IOfiles)
  end if

!!$  if ( save_matrix ) then
!!$     write(fname,'(a3,i0.6,a4)') 'mat',itemp,'.dat'
!!$     fname=etb(etb(IOfiles%linear_sys%fn)//fname)
!!$
!!$     lun = IOfiles%linear_sys%lun
!!$     open(lun, file=fname, iostat = res)
!!$     if(res .ne.0 ) then
!!$        write(IOfiles%lun_err%lun,*) &
!!$             ' failed to open unit ',lun,&
!!$             ' to be linked to file ',etb(fname)
!!$        stop ' simulation ended'
!!$     end if
!!$     call tdpot%copy_stiff%write(lun)
!!$     close(lun)
!!$
!!$     
!!$     !>------------------------------------------------------
!!$     !> Save rhs system
!!$     !>------------------------------------------------------
!!$     if ( ( itemp .eq. 0 ) .or. &
!!$          ( .not. tdpot%odein%rhs_integrated%steadyTD ) )then  
!!$        fname=etb(etb(IOfiles%linear_sys%fn)//'/rhs.dat')
!!$        open(IOfiles%linear_sys%lun,file=fname,iostat = res)
!!$        if(res .ne. 0) rc = IOerr(IOfiles%lun_err%lun, err_IO,&
!!$             'main', &
!!$             'err open file '//etb(fname),res)
!!$        write(lun,*) 1, tdpot%npot
!!$        do i = 1,  tdpot%npot
!!$           write(lun,*) tdpot%rhs(i)
!!$        end  do
!!$        close(lun)
!!$     end if
!!$  end if

contains
    subroutine eval_save_test(&
         save_test,&
         in_cycle,&
         id_save,&
         freq,&
         itemp,&
         int_before,&
         current_var)
    use Globals
    use IOdefs

    implicit none
    logical,           intent(out  ) :: save_test
    logical,           intent(in   ) :: in_cycle
    integer,           intent(in   ) :: id_save
    integer,           intent(in   ) :: freq
    integer,           intent(in   ) :: itemp
    integer,           intent(inout) :: int_before
    real(kind=double), intent(in   ) :: current_var
    
    !local 
    integer :: int_now
    
    save_test = .false.

    if (id_save .eq. 1) then
       save_test = .true.
    end if
    !
    if (id_save .eq. 2) then
       if ( (mod(itemp,freq) == 0) .or. (.not. in_cycle) ) then
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    end if
    !
    if (id_save .eq. 3) then
       if ( ( itemp .ne. 0) ) then 
          int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
          if ( (int_now /= int_before) .or. (.not. in_cycle ) ) then
             int_before=int_now
             save_test = .true.
          end if
       else
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    end if


    if ( id_save .eq. 4 ) then
       if ( (itemp .ne. 0) .and. (.not. in_cycle )) then
          save_test = .true.
       end if
    end if

  end subroutine eval_save_test
end subroutine evol2dat


subroutine handle_failure_update ( lun_err,&
     info,&
     nrestart,&
     deltat,&
     p1p0,&
     tdpot, &
     ctrl_original,&
     ctrl)
  use Globals 
  use ControlParameters
  use TdensPotentialSystem
  implicit none
  integer,             intent(in   ) :: lun_err
  integer,             intent(in   ) :: info
  integer,             intent(in   ) :: nrestart
  real(kind=double),   intent(inout) :: deltat
  type(p1p0_space_discretization), intent(in )  :: p1p0
  type(tdpotsys),      intent(inout) :: tdpot
  type(CtrlPrm),       intent(in   ) :: ctrl_original
  type(CtrlPrm),       intent(inout) :: ctrl
  
  ! local
  logical :: rc
  integer :: res

  if ( ctrl%id_time_discr == 1 ) then
     deltat = deltat / ctrl%exp_rate
  end if

  select case ( info ) 
  case ( -1 ) 
     !
     ! failure of linear solver
     !
     ctrl%ctrl_solver%scheme=ctrl_original%ctrl_solver%scheme
     ! in ibrid approach try to change strategy
     if ( ( ( ctrl_original%newton_method/10 ).eq. 2) .and. &
          ( tdpot%nrestart_invert_jacobian .ge. 1) ) then
        !
        ! force 01, 02, 03,04 from the begin
        !
        ctrl%newton_method = mod(ctrl_original%newton_method,10)
        
     else
        !
        ! shrink time step
        !
        deltat = deltat / ctrl%exp_rate

        !
        ! not rebuild precondtioner
        !
        !if ( ctrl%build_prec .eq. 1) ctrl%build_prec = 0 
        if ( nrestart .gt. 1 )  ctrl%build_prec=1
     end if

   case ( 1 ) 
      !
      ! failure of linear solver
      !
      ctrl%ctrl_solver%scheme=ctrl_original%ctrl_solver%scheme
      ! in ibrid approach try to change strategy
      if ( ( ( ctrl_original%newton_method/10 ).eq. 2) .and. &
           ( tdpot%nrestart_invert_jacobian .ge. 1) ) then
         !
         ! force 01, 02, 03,04 from the begin
         !
         ctrl%newton_method = mod(ctrl_original%newton_method,10)
      else
         !
         ! shrink time step
         !
         deltat = deltat / ctrl%exp_rate

         !
         ! not rebuild precondtioner
         !
         if ( ctrl%build_prec .eq. 1) ctrl%build_prec = 0 
         if ( nrestart .gt. 1 )  ctrl%build_prec=1
      end if

     
     
  case ( -2 ) 
     !
     ! Newton iterations exided maximum
     !
     deltat = deltat / ctrl%exp_rate
     
  case ( -4 ) 
     !
     ! Newton residua are not descresing
     !
     deltat = deltat / ctrl%exp_rate
     ctrl%build_prec = 1 
  case ( -1000 ) 
     !
     ! D2 not positive ( obtained only for ctrl%jac_reduced = 1 ) 
     !     
     select case ( ctrl_original%newton_method/10)
     case( 0 ) 
        !
        ! shrink time step
        !
        deltat = deltat / ctrl%exp_rate      
     case (1)
        !
        ! shrink time step
        !
        deltat = deltat / ctrl%exp_rate      
     case (2)
        !select case ( mod(ctrl_original%newton_method,10) )
        !
        ! shrink time step
        !
        deltat = deltat / ctrl%exp_rate
        !end select
     end select
        
     !
     ! 
     !     
     
     !
     ! not rebuild precondtioner
     !
     !if ( ctrl%build_prec .eq. 1) ctrl%build_prec = 0 
  case ( -2000 ) 
     !
     ! preconditioner assembly failure
     !
     deltat = deltat / ctrl%exp_rate
     ctrl%build_prec = 1 
  case default
     write(*,*) info
     rc = IOerr(lun_err, err_val , 'handle_update_failure', &
          ' unknown info flag=',info)
  end select

  if (nrestart .eq. ctrl_original%nrestart_max-1)  then
     if( deltat < 1.0d0 )  ctrl%id_time_discr = 1
  end if
  

end subroutine handle_failure_update


subroutine set_controls_update (&
     itemp,&
     deltat,&
     tdpot, &
     p1p0,&
     ode_inputs,&
     timefun,&
     ctrl_original,&
     ctrl)
  use Globals 
  use DmkOdeData
  use ControlParameters
  use TdensPotentialSystem
  use OdeInputs
  use TimeFunctionals
  implicit none
  integer ,            intent(in   ) :: itemp
  real(kind=double),   intent(inout) :: deltat
  type(tdpotsys),      intent(inout) :: tdpot
  type(p1p0_space_discretization), intent(in)  :: p1p0
  type(odedata),       intent(in   ) :: ode_inputs
  type(evolfun),       intent(in   ) :: timefun
  type(CtrlPrm),       intent(in   ) :: ctrl_original
  type(CtrlPrm),       intent(inout) :: ctrl
  !local 
  integer :: i,j
  integer :: threshold,avg_iter,lun_out=6
  real(kind=double) :: sup_rhs
  character(len=256) :: out_format

  !
  ! time step controls
  !
  select case ( ctrl%id_time_ctrl ) 
  case( 1 )
     !
     ! cosntant time step
     !
     deltat=ctrl%deltat 
     out_format=ctrl%formatting('ar')
  
  case( 2 )
     !
     ! increasing time step
     !
     deltat=min(deltat * ctrl%exp_rate,ctrl%upper_bound_deltat)
     out_format=ctrl%formatting('ar')
     !write(lun_out,out_format) 'time step = ',deltat
  
  case (3 )
     !
     ! time step one / rhs_ode
     !
     select case ( ctrl%id_time_discr ) 
     case (1)
        call p1p0%assembly_rhs_ode(&
             1,&
             ode_inputs,&
             tdpot,&
             tdpot%scr_ntdens)
        sup_rhs= maxval(tdpot%scr_ntdens)
        if ( sup_rhs < zero ) then
           deltat = min(deltat * ctrl%exp_rate,ctrl%upper_bound_deltat)
        else
           deltat=(1.0d0-ctrl%lower_bound_deltat)/sup_rhs
        end if
        deltat=min(deltat,ctrl%upper_bound_deltat)
        
        out_format=ctrl%formatting('araar')
        write(lun_out,out_format) 'time step = ',deltat,&
             ' | ', ' linfty norm increment = ', sup_rhs
     case (2)
        tdpot%scr_ntdens =  p1p0%nrm_grad_dyn - one
        sup_rhs= maxval(tdpot%scr_ntdens)
        if ( sup_rhs < zero ) then
           deltat = min(deltat * ctrl%exp_rate,ctrl%upper_bound_deltat)
        else
           deltat=(1.0d0-ctrl%lower_bound_deltat)/sup_rhs
        end if
        deltat=min(deltat,ctrl%upper_bound_deltat)
        
        out_format=ctrl%formatting('araar')
        write(lun_out,out_format) 'time step = ',deltat,&
             ' | ', ' linfty norm increment = ', sup_rhs
     end select
       
  end select
  

  !
  ! define how to build precnditiner
  ! build_prec  = 0/1
  ! build_tunig = 0/1
  !
  call tdpot%set_threshold(ctrl)
  
  !
  ! adaptive setting of linear solver tolerance 
  !
  ! ctrl%tol_nonlinear=min(5d-3,max(var_tdens(itemp),tol_zero))
  ! ctrl%ctrl_solver%tol_sol=max(min(1.0d-4,1.0d-2*var_tdens(itemp)),small)
     
  
end subroutine set_controls_update


     

!>--------------------------------------------
!> Initialization of all varibles that
!> contains information stored in file
!>--------------------------------------------
subroutine init_p1p0_muffe_from_files(IOfiles, ctrl_read, odein, grid, subgrid)
  use Globals
  use IOdefs
  use ControlParameters
  use AbstractGeometry
  use OdeInputs
  implicit none
  type(IOfdescr), intent(in   ) :: IOfiles
  type(CtrlPrm),  intent(inout) :: ctrl_read
  type(OdeInp),   intent(inout) :: odein
  type(abs_simplex_mesh),  intent(inout) :: grid
  type(abs_simplex_mesh),  intent(inout) :: subgrid
  ! local
  integer :: lun_err,lun_stat
  integer :: ntdens, npot
  
  lun_err  = IOfiles%stderr%lun
  lun_stat = IOfiles%statistic%lun
  
  !
  ! 1 - Read controls
  !
  call ctrl_read%init(ctrl_read,IOfiles)
  write(lun_stat,'(a)') ctrl_read%sep('controls simulation')
  call ctrl_read%info(IOfiles%statistic%lun)
  
  !
  ! 2 - Read geometry info
  !
  write(lun_stat,'(a)') ctrl_read%sep('setup data begin')
  ! 2.1 - Read grid
  write(lun_stat,'(a)') ctrl_read%sep('read grid')
  !call grid%init(lun_err, 0, IOfiles%grid,flag_cnc=0)
  call grid%read_mesh(lun_err,IOfiles%grid)
  call grid%build_size_cell(lun_err)
  call grid%build_normal_cell(lun_err)
  
  
  ! 2.2 - Read subgrid
  write(lun_stat,'(a)') ctrl_read%sep('read subgrid')
  !call subgrid%init(lun_err,2,IOfiles%subgrid,IOfiles%parents)
  call subgrid%read_mesh(lun_err,IOfiles%subgrid)
  call subgrid%read_parent(lun_err,IOfiles%parents)
  call subgrid%build_size_cell(lun_err)
  call subgrid%build_normal_cell(lun_err)

  
  write(lun_stat,'(a)') 'init inputs data'
  if (ctrl_read%debug .eq. 1) write(IOfiles%stdout%lun,'(a)') 'init inputs data'

  if (  ctrl_read%id_subgrid .eq. 1) then
     ntdens = grid%ncell
     npot   = subgrid%nnode
  else
     ntdens = grid%ncell
     npot   = grid%nnode
  end if

  call odein%init(&
       IOfiles,&
       ntdens,npot,ctrl_read%id_subgrid,&
       ctrl_read%tzero)

end subroutine init_p1p0_muffe_from_files






subroutine kill_muffe( lun_err,&
     tdpot, grid, subgrid,  ode_inputs, ctrl)
  use Globals
  use ControlParameters
  use AbstractGeometry
  use TdensPotentialSystem
  use DmkOdeData
  implicit none
  integer,                intent(in   ) :: lun_err
  type(tdpotsys),         intent(inout) :: tdpot
  type(abs_simplex_mesh), intent(inout) :: grid, subgrid
  type(OdeData),          intent(inout) :: ode_inputs
  type(CtrlPrm),          intent(inout) :: ctrl
  
  ! Killing procedure
  call ode_inputs%kill(lun_err)
  call tdpot%kill(lun_err)
  call grid%kill(lun_err)
  call subgrid%kill(lun_err)
  call ctrl%kill()
  

end subroutine kill_muffe

subroutine main_subroutine(&
     nnode,ncell,nnodeincell,&
     coord, topol,&
     ctrl_read,&
     ntdens, tdens,&
     pflux,pode,forcing,&
     pot,&
     nnode_pot,ncell_pot,coord_pot, topol_pot)
  use Globals
  use Timing
  use DmkOdeData
  use ControlParameters
  use AbstractGeometry
  use MuffeTiming
  use TdensPotentialSystem
  use TimeFunctionals
  implicit none
  integer, intent(in) :: nnode
  integer, intent(in) :: ncell
  integer, intent(in) :: nnodeincell
  real*8, intent(in)  :: coord(3,nnode)
  integer, intent(in) :: topol(nnodeincell,ncell)
  type(CtrlPrm),intent(in) :: ctrl_read
  integer, intent(in) :: ntdens
  real*8, intent(inout) :: tdens(ntdens)
  real*8, intent(inout) :: pflux
  real*8, intent(inout) :: pode
  real*8, intent(inout) :: forcing(ntdens)
  real*8, intent(inout) :: pot(4*nnode)
  integer, intent(inout) :: nnode_pot
  integer, intent(inout) :: ncell_pot
  real*8, intent(inout)  :: coord_pot(3,4*nnode)
  integer, intent(inout) :: topol_pot(nnodeincell,4*ncell)
  ! local
  type(codeTim) :: CPU
  type(CtrlPrm) :: ctrl
  type(abs_simplex_mesh)     :: grid,subgrid
  type(tdpotsys) :: tdpot
  type(evolfun)  :: timefun 
  type(Tim) :: wasted_temp
  type(p1p0_space_discretization) :: p1p0
  type(OdeData)  :: ode_inputs
  logical :: rc
  logical :: test_var
  logical :: test_maxit
  logical :: in_cycle
  logical :: endfile
  
  integer :: lun_err=0,lun_out=6,lun,res,lun_stat=7
  integer :: itemp,itemp_prec_calc
  integer :: nrestart, nrestart_max,max_time_it,nupdate
  integer :: info, info_update,npot
  real(kind=double)              :: deltat
  real(kind=double), allocatable :: time(:)
  real(kind=double), allocatable :: var_tdens(:)
  real(kind=double), allocatable :: var_tdens_linfty(:)
  character(len=256) :: msg,str


  !
  ! Initialization of Timing module
  !
  call CPU%init()
  call CPU%TOT%set('start')
  call CPU%OVH%set('start')


  !
  ! 2 - Read geometry info
  !
  write(lun_stat,'(a)') ctrl_read%sep('setup data begin')
  ! 2.1 - Read grid
  call grid%init_from_data(lun_err,nnode, ncell, nnodeincell,'triangle', topol,coord)
  call grid%build_size_cell(lun_err)
  call grid%build_normal_cell(lun_err)
  write(lun_stat,'(a)') ctrl_read%sep('Init. grid')

  
  ! 2.2 - Read subgrid
  call subgrid%refine(lun_err,grid)
  call subgrid%build_size_cell(lun_err)
  call subgrid%build_normal_cell(lun_err)
  write(lun_stat,'(a)') ctrl_read%sep('Subgrid Done')

  
  !
  ! BUILD FEM SPACES
  !
  if (  ctrl_read%id_subgrid .eq. 1) then
     npot   = subgrid%nnode
     call p1p0%init(&
          lun_err,lun_out,lun_stat,&
          ctrl_read,&
          ctrl_read%id_ode,&
          ctrl_read%id_subgrid,grid, subgrid)
  else
     npot   = grid%nnode
     call p1p0%init(&
          lun_err,lun_out,lun_stat,&
          ctrl_read,&
          ctrl_read%id_ode,&
          ctrl_read%id_subgrid,grid, grid)
  end if
  write(lun_stat,'(a)') 'SPATIAL DISCRETIZATION DONE'
  if (ctrl_read%debug .eq. 1) write(lun_out,'(a)') 'SPATIAL DISCRETIZATION DONE'

  !
  ! build inputs data 
  !
  call ode_inputs%init(lun_err,  p1p0%ntdens, p1p0%npot)
  !
  ! USER DEFINED
  !
  ode_inputs%pflux=pflux
  ode_inputs%pode=pode
  ode_inputs%tdens0=tdens
  if (  ctrl_read%id_subgrid .eq. 1) then
     !
     ! buils rhs integrated
     !
     call subgrid%proj_subgrid(forcing, p1p0%scr_nfull(1:subgrid%ncell))
     call p1p0%p1%build_rhs_forcing(p1p0%scr_nfull(1:subgrid%ncell),ode_inputs%rhs_integrated(:,1))
     ode_inputs%rhs_integrated(:,2)=ode_inputs%rhs_integrated(:,1)
  else
     !
     ! buils rhs integrated
     !
     call ode_inputs%init(lun_err,  p1p0%ntdens, p1p0%npot)
     call p1p0%p1%build_rhs_forcing(forcing,ode_inputs%rhs_integrated(:,1))
     ode_inputs%rhs_integrated(:,2)=ode_inputs%rhs_integrated(:,1)
  end if

  !
  ! DEFAULT OPTION
  !
  ode_inputs%kappa=one
  ode_inputs%decay=one
  ode_inputs%pmass=one

  !
  ! initialized tdens-potential type
  !
  call tdpot%init(&
       lun_err,lun_out,lun_stat,&
       p1p0,ctrl_read)


   !
  ! 4 - Allocate array time, var_tdens and time functional
  !
  allocate(&
       time(0:ctrl_read%max_time_it),&
       var_tdens(0:ctrl_read%max_time_it),&
       var_tdens_linfty(0:ctrl_read%max_time_it),&
       stat=res)
  if(res .ne. 0) rc = IOerr(lun_err, err_alloc , 'main', &
         ' error alloc. time var_tdens',res)
  time      = zero
  var_tdens = zero
  var_tdens(0) = huge
  var_tdens_linfty = zero
  var_tdens_linfty(0) = huge

  call timefun%init(lun_err,&
       ctrl_read%max_time_it, ctrl_read%max_iter_nonlinear)
  !
  write(lun_stat,'(a)') ctrl_read%sep('setup data ended')
  write(lun_out,'(a)') ctrl_read%sep('setup data ended')


  !
  ! create a work copy use to change orignal parameters
  !
  ctrl = ctrl_read
  
  !
  ! Time evolution starts
  !
  write(lun_stat,'(a)') ctrl_read%sep('time evolution begins')
  write(lun_out,'(a)') ctrl_read%sep('time evolution begins')
  

  !
  ! Time=tzero
  !
  itemp=0
  time(itemp)=ctrl%tzero
  max_time_it=ctrl%max_time_it
  in_cycle=.true.

  !
  ! Start evolution syncronazing tdens system and ode's inputs
  !
  !  
  ! 1 - Eval ODE input varible at time $tzero$
  ! Pflux, Pmass, Kappa, Decay and  Rhs_integrated kappa
  !
  msg =  ctrl%sep('Start Tdens/Pot syncronization at tzero}')
  write(lun_stat,*) etb(msg)
  write(lun_out,* ) etb(msg)
  
  !
  ! Syncronize at tzero
  !
  tdpot%pot=zero
  ctrl%build_prec=1
  ctrl%ctrl_solver%scheme='PCG'
  ctrl%ctrl_prec%prec_type='IC'
  call tdpot%syncronize_at_tzero(info,&
       lun_err,lun_out, lun_stat,&
       p1p0,&
       ode_inputs,&
       ctrl, &
       CPU)
  ctrl%ctrl_solver= ctrl_read%ctrl_solver
  ctrl%ctrl_prec  = ctrl_read%ctrl_prec
  call p1p0%evaluate_functionals(tdpot,ode_inputs)
  call timefun%set_evolfun(itemp,tdpot,p1p0,ode_inputs,ctrl,CPU)
  msg =  ctrl%sep('Tdens/Pot syncronized at tzero}')
  write(lun_stat,*) etb(msg)
  write(lun_out,* ) etb(msg)
 
  !
  ! Info: Time=tzero,  time_fucntionals (basic) 
  !
  write(lun_stat,'(a)')' '
  call info_muffe(lun_stat,ctrl,itemp, time(itemp),zero, timefun,huge,huge,0,CPU)
  call info_muffe(lun_out,ctrl,itemp, time(itemp),zero, timefun,huge,huge,0,CPU)
  
  !
  ! Start time cycle
  !
  in_cycle    = ( max_time_it > 0) 
  deltat      = ctrl%deltat
  nupdate     = 0
  
  do while (in_cycle )
     if ( ctrl%build_prec .eq. 1 ) itemp_prec_calc = itemp
     !
     ! Update spatial variables.
     ! First save $\Tdens^\tstep$ and $\Pot^\tstep$, then 
     ! Update sytem tdens-pot, and all variable inside
     ! (gradients, ode input variable ) at next time iteration
     ! After this sobroutine ALL spatial variables need to valued
     ! at $t^{\tstepp}=t^{\tstep}+\Deltat$
     !--------------------------------------------------------
     itemp = itemp + 1
     write(lun_out,'(a)') ' '
     write(lun_stat,'(a)') ' '
     write(lun_out,'(a)') ' '
     write(lun_stat,'(a)') ' ' 
     write(str, '(a,I5)') 'UPDATE begin: itemp= ', itemp
     write(lun_stat,'(a)') ctrl%sep(etb(str))
     write(lun_out,'(a)') ctrl%sep(etb(str))
     
     info_update = -1
     nrestart = 0     
     
     if ( ctrl%debug .eq. 1) write(lun_out,*) 'Set controls before update'
     !
     ! set all controls for update system
     !
     call  set_controls_update (&
          itemp,&
          deltat,&
          tdpot, &
          p1p0,&
          ode_inputs,&
          timefun,&
          ctrl_read,&
          ctrl)

     do while ( ( info_update .ne. 0 ) .and. (nrestart < ctrl%nrestart_max) ) 
        !
        ! update system
        !
        wasted_temp = CPU%WASTED
        call CPU%ALGORITHM%set('start')
        call CPU%WASTED%set('start')
        call tdpot%update(&
             lun_err,lun_out,lun_stat,&
             ctrl,&
             deltat,&
             itemp,&
             time(itemp-1),&
             CPU,info_update,&
             p1p0,&
             ode_inputs)
        call CPU%WASTED%set('stop') 
        call CPU%ALGORITHM%set('stop')

        if ( info_update .ne. 0 ) then
           if (ctrl%id_time_discr .eq. 1) exit
           
           !
           ! in case of failure 
           ! 1- recover data
           ! 2- reset controls
           !
           nrestart = nrestart + 1 

           !
           ! restores previous data
           !
           tdpot%tdens = tdpot%tdens_old 
           tdpot%gfvar = tdpot%gfvar_old
           tdpot%pot   = tdpot%pot_old

           !
           ! reset controls for next attemp of update
           !
           write(*,*) 'info_update=', info_update
           call handle_failure_update ( lun_err,&
                info_update,&
                nrestart,&
                deltat,&
                p1p0,&
                tdpot, &
                ctrl_read,&
                ctrl) 
           write(*,*) 'build_prec=', ctrl%build_prec
        end if
     end do
     !
     ! reset original controls
     !
     ctrl = ctrl_read
     
     tdpot%nrestart_newton = nrestart
     nupdate = nupdate + 1 + nrestart
     
     !
     ! no time was wasted
     !
     if ( nrestart .eq. 0 ) then
        CPU%WASTED=wasted_temp
     end if

     !
     ! if update procedure failed break 
     !
     if ( info_update .ne. 0) then
        rc = IOerr(lun_err, wrn_inp, 'main', &
             ' UPDATE procedure failed')
        exit
     else
        !
        ! shift data from second to first slot, thus
        ! first slot and tdpot are syncronized
        !
        !call ode_inputs%shift()
     end if
     !
     write(str, '(a,I5,a,I2,a)') &
          ' UPDATE end itemp= ', itemp, &
          ' nrestart=', nrestart
     write(lun_stat,'(a)') ctrl%sep(etb(str))
     write(lun_out,'(a)') ctrl%sep(etb(str))
     
     !
     ! Update time varying functionals
     ! Evaluate time, var_tdens and time funtionals
     !
     time(itemp) = time(itemp-1) + deltat  
     var_tdens(itemp) = p1p0%eval_var_tdens(tdpot,deltat)
     var_tdens_linfty(itemp) = p1p0%eval_var_tdens(tdpot,deltat,0.0d0)
     tdpot%loc_var_tdens = var_tdens(itemp)
     call p1p0%evaluate_functionals(tdpot,ode_inputs)
     call timefun%set_evolfun(itemp,tdpot,p1p0,ode_inputs,ctrl,CPU)

     !
     ! Info: Time, var_tdens,  time_fucntionals (basic) 
     !
     write(lun_stat,'(a)') ' ' 
     write(lun_out,'(a)') ' '
     call info_muffe(lun_stat,&
          ctrl,itemp, time(itemp),deltat, timefun, var_tdens(itemp),var_tdens_linfty(itemp),nupdate,CPU)
     call info_muffe(lun_out,&
          ctrl,itemp, time(itemp),deltat, timefun, var_tdens(itemp),var_tdens_linfty(itemp),nupdate,CPU)
     

     !
     ! if continuing cyclying     
     ! Test max time iterations or convergence achieved
     test_var   = ( var_tdens_linfty(itemp) .gt. ctrl%tol_var_tdens )
     test_maxit = ( itemp            .lt. ctrl%max_time_it    )
     in_cycle   = ( test_var .and. test_maxit)
          
  end do
  
  call p1p0%kill(lun_err)

  tdens=tdpot%tdens
  pot(1:npot)=tdpot%pot
  

end subroutine main_subroutine


  
  


!!$program main2
!!$   use Globals
!!$  use Timing
!!$  use TimeInputs
!!$  use IOdefs
!!$  use DataSequence
!!$  use vtkloc
!!$  use ControlParameters
!!$  use MuffeTiming
!!$  use Geometry2d
!!$  use AbstractGeometry
!!$  use P1Galerkin
!!$  use TdensPotentialSystem
!!$  use TimeFunctionals
!!$  use SparseMatrix
!!$  use StdSparsePrec
!!$  use OdeInputs
!!$  use Matrix
!!$  use Preconditioner
!!$  use LinearSolver
!!$  use StdSparsePrec
!!$    
!!$  implicit none
!!$  
!!$  type(IOfdescr) :: IOfiles
!!$  type(OdeInp)   :: odein
!!$  type(OdeData)  :: ode_inputs
!!$  type(CtrlPrm)  :: ctrl_read,ctrl
!!$  type(codeTim)  :: CPU
!!$  type(abs_simplex_mesh)     :: grid,subgrid
!!$  type(tdpotsys) :: tdpot
!!$  
!!$
!!$  
!!$  type(p1p0_space_discretization) :: p1p0
!!$  type(evolfun)  :: timefun
!!$  ! work arrray
!!$  type(Tim) :: wasted_temp
!!$  
!!$
!!$  !
!!$  ! Initialization of Timing module
!!$  !
!!$  call CPU%init()
!!$  call CPU%TOT%set('start')
!!$  call CPU%OVH%set('start')
!!$
!!$   
!!$  !
!!$  ! Initialization of INPUT_OUTPUT module
!!$  !
!!$  write(6,*) 'Init INPUT/OUTPUT files'
!!$  call IOfiles%init()
!!$  lun_err   = IOfiles%stderr%lun
!!$  lun_out   = IOfiles%stdout%lun
!!$  lun_stat = IOfiles%statistic%lun
!!$  write(6,*) 'All files are open'
!!$
!!$  
!!$  !
!!$  ! Initialization of  ctrl, odein, grid/subgrid from file
!!$  !
!!$  call init_p1p0_muffe_from_files(IOfiles, ctrl_read, odein, grid, subgrid )
!!$
!!$
!!$  !
!!$  ! init ode_inputs containg all inputs data
!!$  !
!!$  call ode_inputs%init(lun_err,  p1p0%ntdens, p1p0%npot)
!!$
!!$
!!$end program main2






!!$subroutine reverse_comunication_cycle(&
!!$     flag,ntime,info,time_iteration,current_time,&
!!$     ode_inputs,ctrl, p1p0,tdpot,)
!!$  use Globals
!!$  use ControlParameters
!!$  use AbstractGeometry
!!$  use TdensPotentialSystem
!!$  use DmkOdeData
!!$  implicit none
!!$  integer,                         intent(inout) :: flag
!!$  integer,                         intent(inout) :: ntime
!!$  integer,                         intent(inout) :: info
!!$  integer,                         intent(inout) :: time_iter
!!$  real(kind=double),               intent(inout) :: current_time
!!$  type(OdeData),                   intent(in   ) :: ode_inputs
!!$  type(CtrlPrm),                   intent(inout) :: ctrl
!!$  type(p1p0_space_discretization), intent(in   )  :: p1p0
!!$  type(tdpotsys),                  intent(inout) :: tdpot
!!$  
!!$  
!!$  
!!$  if ( flag==1) then
!!$     !
!!$     !  assign inputs
!!$     !
!!$     flag=2
!!$     ntime=1
!!$     current_time=cltr%t_zero
!!$     return
!!$  end if
!!$
!!$  !
!!$  ! ode_inputs assigned
!!$  !
!!$  if ( flag==2) then
!!$     if ( iter .eq. 0 ) then 
!!$        call tdpot%syncronize_tzero(p1p0,ode_inputs)
!!$        info=0
!!$        flag=4
!!$     else
!!$        if ( info_update == 0) then
!!$           call set_controls_before_update()
!!$        else
!!$           call ctrl%set_controls_after_failure()
!!$        end if
!!$        
!!$        call tdpot%update(&
!!$             lun_err,lun_out,lun_stat,&
!!$             ctrl,&
!!$             deltat,&
!!$             itemp,&
!!$             current_time,&
!!$             CPU,info_update,ode_inputs)   
!!$        if ( info_update .ne. 0 ) then
!!$           !
!!$           nrestart = nrestart + 1 
!!$           !
!!$           ! restores previous data
!!$           !
!!$           tdpot%tdens  = tdpot%tdens_old 
!!$           tdpot%pot    = tdpot%pot_old
!!$
!!$           call tpot%build_grad_vars 
!!$
!!$           current_time = current_time
!!$
!!$           flag = 2
!!$           info_reverse = -1
!!$
!!$           if (nrestart .ge. ctrl%nrestart_max) then
!!$              flag=-1
!!$              info=-1
!!$           end if
!!$        else
!!$           ! syncronize ode_inputs eval variation
!!$           flag=3
!!$        end if
!!$
!!$        if ( flag .eq. 3) then  
!!$           ! eval functional 
!!$           flag=4
!!$        end if
!!$
!!$        if ( flag .eq. 4) then  
!!$           ! eval functional 
!!$           flag=2
!!$           ntime=2
!!$           info_update=0
!!$        end if
!!$
!!$
!!$     end if
!!$  end if
!!$end subroutine reverse_comunication_cycle
!!$  
!!$
!!$
!!$end subroutine reserse_comunication_cycle
!!$
!!$! init iofiles
!!$! 
!!$
!!$
!!$
!!$info = 1
!!$do while( info > 0 ) !
!!$   select case ( info )
!!$   case (1)
!!$      call ode_inputs%set(&
!!$           1,  time(itemp-1)+deltat,&
!!$           IOfiles,&
!!$           tdpot%odein)
!!$      
!!$      call tdpot%syncrozined_tzero()
!!$
!!$   case (2) 
!!$      if ( info_update .eq. 0) then
!!$         itemp = itemp + 1
!!$         write(str,'(a)') ' '
!!$         write(lun_out,'(a)') ctrl%sep(etb(str))
!!$         write(lun_stat,'(a)') ctrl%sep(etb(str))
!!$         write(str, '(a,I5)') 'UPDATE begin: itemp= ', itemp
!!$         write(lun_stat,'(a)') ctrl%sep(etb(str))
!!$         write(lun_out,'(a)') ctrl%sep(etb(str))
!!$
!!$         !
!!$         ! set all controls for update system
!!$         !
!!$         call  set_controls_update (&
!!$              itemp,&
!!$              deltat,&
!!$              tdpot, &
!!$              ode_inputs,&
!!$              timefun,&
!!$              ctrl_read,&
!!$              ctrl)
!!$      end if
!!$
!!$      !
!!$      ! act only in this section and data inputs at time t+deltat
!!$      ! in the second slot
!!$      call ode_inputs%set(&
!!$           2,  time(itemp-1)+deltat,&
!!$           IOfiles,&
!!$           tdpot%odein)
!!$
!!$      !
!!$      ! update whole system
!!$      !
!!$      call tdpot%update(info_update)
!!$
!!$      if ( info_update .eq. 0 ) then
!!$         !
!!$         ! in case of succesfull update, set time to 
!!$         !
!!$         write(str, '(a,I5,a,I2,a)') &
!!$              ' UPDATE end itemp= ', itemp, ' nrestart=', nrestart
!!$         write(lun_stat,'(a)') ctrl%sep(etb(str))
!!$         write(lun_out,'(a)') ctrl%sep(etb(str))
!!$         
!!$
!!$         !
!!$         ! Update time varying functionals
!!$         ! Evaluate time, var_tdens and time funtionals
!!$         !
!!$         time(itemp) = time(itemp-1) + deltat  
!!$         var_tdens(itemp) = tdpot%eval_var_tdens(deltat)
!!$         tdpot%loc_var_tdens = var_tdens(itemp)
!!$
!!$         
!!$         !
!!$         ! if continuing cyclying     
!!$         ! Test max time iterations or convergence achieved
!!$         test_var   = ( var_tdens(itemp) .gt. ctrl%tol_var_tdens )
!!$         test_maxit = ( itemp            .lt. ctrl%max_time_it    )
!!$         in_cycle   = ( test_var .and. test_maxit)
!!$      end if
!!$   end select
!!$   !
!!$   ! this section is skipped if update failed
!!$   !
!!$   if (info_update .eq.0) then
!!$      !
!!$      ! Print iteration number, current_time and var_tdens
!!$      ! 
!!$      write(lun,'(a)') ctrl%sep('info evolution ')
!!$      if ( current_var_tdens .lt. huge ) then 
!!$         out_format=ctrl%formatting('aiaaaraaaraaar')
!!$         write(lun,out_format) &
!!$              'iter=', itemp,&
!!$              sep,'time ',' = ', current_time,&
!!$              sep,'deltat',' = ', deltat,& 
!!$              sep,'var  ',' = ', current_var_tdens
!!$      else
!!$         out_format=ctrl%formatting('aiaaar')
!!$         write(lun,out_format) &
!!$              'iter=', itemp,&
!!$              sep,'time ',' = ', current_time
!!$      end if
!!$
!!$      !
!!$      ! Print info functional at current_time
!!$      !
!!$      if ( print_info_system ) then
!!$         write(lun,'(a)') ctrl%sep('info system')
!!$         call timefun%info(lun,1,ctrl,itemp)
!!$      end if
!!$      write(lun,'(a)') ctrl%sep(' info algoritm')
!!$      call CPU%ALGORITHM%info(lun,'Algorithm Time : ')
!!$      write(lun,*) 'time step =',itemp,' nupdate =', nupdate
!!$      write(lun,'(a)') ctrl%sep()
!!$
!!$      !
!!$      ! Save data 
!!$      !
!!$      call CPU%SAVE%set('start')
!!$      call evol2dat(tdpot,&
!!$           ctrl,&
!!$           in_cycle,&           
!!$           itemp, &
!!$           int_before_dat, int_before_matrix, &
!!$           time(itemp),&
!!$           var_tdens(itemp),&
!!$           IOfiles)
!!$      call CPU%SAVE%set('stop')
!!$   end if
!!$
!!$   
!!$end do
!!$   
!!$   
!!$
!!$   
!!$
!!$
!!$
!!$


  !
  ! flag = 1 : complete system at 
  !
!!$  
!!$  flag = 1
!!$  do while ( flag .ne. 0 ) 
!!$     
!!$
!!$     call reverse_comunication_cycle(&
!!$          flag,current_time,&
!!$          tdpot,odedata,ctrl, p1p0)
!!$     !
!!$     ! mandatory action to pass inputs tothe algorithm
!!$     ! In case of ode's inputs that are all fix in time,
!!$     ! initialiazed the values before reverse communication cycle.
!!$     ! 
!!$     !
!!$     select case ( flag )
!!$     case (2) 
!!$        !
!!$        ! set ode inputs in odedata
!!$        ! 
!!$        call odein%set(IOfiles,current_time+deltat,endfile)
!!$        call ode_inputs%set(&
!!$             2,  current_time+deltat,&
!!$             IOfiles,&
!!$             odein,ctrl%id_ode)
!!$     case (3)
!!$        !
!!$        ! Syncronized data tdpot data with ode's inputs
!!$        ! in the first slot
!!$        !
!!$        call ode_inputs%shift()
!!$
!!$
!!$     case (4)
!!$        !
!!$        ! print information on state of the system 
!!$        ! or save data
!!$        !
!!$         
!!$     
!!$        !
!!$        ! store data 
!!$        !
!!$        call CPU%SAVE%set('start')
!!$        call evol2dat(tdpot,&
!!$             ctrl,&
!!$             in_cycle,&           
!!$             itemp, &
!!$             int_before_dat, int_before_matrix, &
!!$             time(itemp),&
!!$             var_tdens(itemp),&
!!$             IOfiles)
!!$        call CPU%SAVE%set('stop')
!!$
!!$
!!$     case(5)
!!$        !
!!$        ! Evalute stop criteria
!!$        !
!!$        
!!$        !
!!$        ! Evaluate time, var_tdens and time funtionals
!!$        !
!!$        time(itemp) = time(itemp-1) + deltat  
!!$        var_tdens(itemp) = tdpot%eval_var_tdens(p1p0,deltat)
!!$        
!!$        !
!!$        ! if continuing cyclying     
!!$        ! Test max time iterations or convergence achieved
!!$        test_var   = ( var_tdens(itemp) .gt. ctrl%tol_var_tdens )
!!$        test_maxit = ( itemp            .lt. ctrl%max_time_it    )
!!$        if ( test_var .and. test_maxit) then
!!$           flag=0
!!$        else
!!$           flag=2
!!$        end if
!!$     end select
!!$  end  do
