module OdeInputs
  use Globals
  use TimeInputs
  use IOdefs
  implicit none
  private
  !>----------------------------------------------------------------
  !> Structure variable containg the time varing quantities 
  !> (pflux, pmass, decay, kappa, rhs_integrated)
  !> and fix in time quanties 
  !> (tdens0) the ODE  
  !> \begin{gather}
  !>    \Div(\Tdens+lambda \Grad \Pot) = \Forcing 
  !>    \quad
  !>    -\Tdens \Grad \Pot \cdot \n_{\partial \Domain} = \Bdflux
  !> \\
  !> \Tdens'=|\Tdens \Grad \Pot(\Tdens)|^\Pflux-kappa*decay*\Tdens^{\Pmass}
  !> \\
  !> \Tdens(tzero) = \Tdens0
  !> \end{gather}
  !> It may contains the optional quantities 
  !> forcing, boundary_flux, optdens(fix initialized and used
  !> only if the correspondet input files exis
  !>------------------------------------------------------------------
  type, public :: OdeInp
     !> True/False flag for existence of optdens array
     logical :: optdens_exists= .False.
     !> True/False flag for existence of optimal potential array
     logical :: dirichlet_exists= .False.
     !> True/False flag for existence penalization
     logical :: penalty_exists = .False.
     !> Initial time
     real(kind=double) :: tzero
     !>-------------------------------------------------------------
     !> Mandatory Input data
     !>-------------------------------------------------------------
     !> Nmber of variables for Tdens-variables 
     integer :: ntdens
     !> Nmber of variables for Pot-variables
     integer :: npot
     !> Number of Dirichlet nodes 
     integer :: ndir=0
     !> Flag for subgrid
     integer :: id_subgrid
     !> Dimension = ntdens
     !> Optinal density (optional)
     !real(kind=double), allocatable :: optdens(:)
     type(TimeData) :: optdens
     !> Dimension= ndir
     !> Dirichlet node indeces ( in subgrid ) 
     integer, allocatable :: dirichlet_nodes(:)
     !> Dimension = ndir
     !> Dirichlet values at nodes ( in subgrid )
     real(kind=double), allocatable :: dirichlet_values(:)
     !> Initial data for transport dnsity
     !> Dimension (ntdens=grid%ncell)
     !> In timedata form but must be in steady state
     !> We use this format to have a unique format in input files
     type(TimeData) :: tdens0
     !> Pflux power
     !> Contiains 1 real with pflux power
     type(TimeData) :: pflux
     !> Time decay
     !> Contiains 1 real with pflux power
     type(TimeData) :: decay
     !> Pmass power
     !> Contiains 1 real with pflux power
     type(TimeData) :: pmass
     !> Positive relax parameter to ensure the coercitivy 
     !> The tdens used in the elliptic equation is tdens + lambda
     type(TimeData) :: lambda
     !> Optional weight multiplicative factor for penalty_weight 
     !> $\Wpenalty$ (rapid way to scale the penalty, can be induced 
     !> in $\Wpenalty$ directily)
     type(TimeData) :: penalty_factor
     !> Optional weight $\Wpenalty$ of penalty decay
     !> Give by adding penalty 
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     type(TimeData) :: penalty_weight
     !> Dimension = ntdens
     !> Optional penalty $\geq 0$ given by adding
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     !> in the minimization problem.
     type(TimeData) :: penalty
     !> Dimension = ntdens
     !> Spatial decay 
     !> Contiains ntria-array with spatial decay
     type(TimeData) :: kappa
     !> Dimension = npot
     !> Rhs in the linear system
     !> arising from equation 
     !> $-\div(\Tdens \Grad \Pot ) = \Forcing$
     !> Completed with proper boundary condition
     type(TimeData) :: rhs_integrated
     !> Dimension = 2, npot
     !> Indeces and values of Dirichlet points
     !> Format is :
     !>   data(1)=float(inode)
     !>   data(2)=value
     type(TimeData) :: dirichlet_bc
   contains
     !> Static constructor
     !> (public for type OdeInp)
     procedure, public, pass :: init => OdeInp_init
     !> Static destructor
     !> (public for type OdeInp)
     procedure, public, pass :: kill => OdeInp_kill
     !> Info procedure
     !> (public for type OdeInp)
     procedure, public, pass :: info => OdeInp_info 
     !> Eval TD variables in the module at given time
     !> (private procedure for type OdeInp, used in init)
     procedure, public, pass :: set    => OdeInp_set
     !> Copy ode's inuts into odedata tpye
     !> (public for type OdeData)
     procedure, public, pass :: set_odedata
  end type OdeInp
contains
  !>------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type OdeInp)
  !> Instantiate (allocate if necessary)
  !> and initialize variables of type OdeInp
  !>
  !> usage:
  !>     call 'var'%init(IOfiles,grid,subgrid,tzero)
  !>
  !> where:
  !> \param[in] IOfiles -> type(IOfdescr). I/O file information
  !> \param[in] ntdens  -> integer. Dimension of Tdens-variables
  !> \param[in] npot    -> integer. Dimension of Pot-variables
  !> \param[in] tzero   -> integer. Initial time of simulation
  !<-------------------------------------------------------------
  subroutine OdeInp_init(this,&
       IOfiles,&
       ntdens,npot,id_subgrid,&
       tzero)
    implicit none
    class(OdeInp),     intent(inout) :: this
    type(IOfdescr),    intent(in   ) :: IOfiles
    integer,           intent(in   ) :: ntdens
    integer,           intent(in   ) :: npot
    integer,           intent(in   ) :: id_subgrid
    real(kind=double),optional, intent(in   ) :: tzero

    !local
    logical :: rc,endtdens0
    integer :: nred
    integer :: stderr, stdout, lun, res 
    integer :: i
    character(len=256) :: fname
    character(len=1256) ::msg
    real(kind=double), allocatable :: ones(:), zeros(:)
    real(kind=double) :: just_one(1)
    real(kind=double) :: just_zero(1)
    
    allocate(ones(ntdens),zeros(ntdens),stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc , 'OdeInp_init', &
            'work array ones')
    ones=one
    zeros= zero
    just_one(1)=one
    just_zero(1)=zero


    ! logic units
    stderr    = IOfiles%stderr%lun
    stdout    = IOfiles%stdout%lun

    
    ! initial time
    if ( present(tzero) ) this%tzero = tzero

    ! dimensions
    this%ntdens     = ntdens
    this%npot       = npot
    this%id_subgrid = id_subgrid
    

    !>------------------------------------------------------------------
    !> Init mandatory files
    !>------------------------------------------------------------------
    ! rhs_integrated (Forcing integrated with P1 Galerkin function)
    write(*,*) id_subgrid
    if ( id_subgrid .eq. 0)  then
       call this%rhs_integrated%init(stderr,&
            IOfiles%rhs_grid_integrated, 1,npot)
    else
       ! used subgrid
       call this%rhs_integrated%init(stderr,&
            IOfiles%rhs_integrated, 1,npot)

       write(msg,*) 'Rhs integrated read from', etb(IOfiles%dirichlet_bc%fn)
       write(IOfiles%stdout%lun,*)    etb(msg)
       write(IOfiles%statistic%lun,*) etb(msg)
    end if


    !  Initial tdens
    call this%tdens0%init(stderr, IOfiles%tdens0, 1,ntdens,ones)
    if ( IOfiles%tdens0%exist ) then
       write(msg,*) 'Initial tdens0 read from: ', etb(IOfiles%tdens0%fn)
       call this%tdens0%set(stderr,IOfiles%tdens0, tzero, endtdens0)
    else
       write(msg,*) 'File', etb(IOfiles%tdens0%fn), ' does not exist. Tdens0 = 1.0 '
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    
    ! Pflux exponent
    call this%pflux%init(stderr, IOfiles%pflux, 1,1)
    if ( this%pflux%steadyTD ) then
       write(msg,*) 'Steady pflux (beta)  read from: ', etb(IOfiles%pflux%fn),' value=',this%pflux%TDActual(1,1)
    else
       write(msg,*) 'Not Steady pflux (beta)  read from: ', etb(IOfiles%pflux%fn)
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)


    ! Pmass exponent
    call this%pmass%init(stderr, IOfiles%pmass, 1,1)
    if ( this%pmass%steadyTD ) then
       write(msg,*) &
            'Steady pmass (alpha) read from: ', etb(IOfiles%pmass%fn),&
            ' value=',this%pmass%TDActual(1,1)
    else
       write(msg,*) 'Not Steady pmass (alpha) read from: ', etb(IOfiles%pmass%fn)
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)


    ! Time decay 
    call this%decay%init(stderr, IOfiles%decay, 1,1,just_one)
    if ( IOfiles%kappa%exist ) then
       write(msg,*) 'Decay read from: ', etb(IOfiles%decay%fn)
    else
       write(msg,*) 'File', etb(IOfiles%decay%fn), ' does not exist. decay = 1.0 '
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)

!!$    ! weight penalty factor
!!$    call this%lambda%init(stderr, IOfiles%lambda,1,1,just_zero)
!!$    if ( IOfiles%lambda%exist ) then
!!$       write(msg,*) 'Relaxation lambda read from : ', etb(IOfiles%lambda%fn)
!!$    else
!!$       write(msg,*) 'File', etb(IOfiles%lambda%fn), ' does not exist. Default = 0.0'
!!$    end if
!!$    write(IOfiles%stdout%lun,*)    etb(msg)
!!$    write(IOfiles%statistic%lun,*) etb(msg)

    
    ! Spatial decay kappa
    call this%kappa%init(stderr, IOfiles%kappa, 1,ntdens,ones)
    if ( IOfiles%kappa%exist ) then
       write(msg,*) 'Kappa read from: ', etb(IOfiles%kappa%fn)
    else
       write(msg,*) 'File', etb(IOfiles%kappa%fn), ' does not exist. kappa = 1.0 '
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    
        
    
    !
    ! optional files
    !
    
    !
    ! optdens = (optimal transport density)
    !
    if (IOfiles%optdens%exist ) then
       this%optdens_exists = .True.
       
       !  Initial tdens
       call this%optdens%init(stderr, IOfiles%optdens, 1,ntdens) 
    end if
    if ( IOfiles%optdens%exist ) then
       write(msg,*) 'Optimal Tdens read from: ', etb(IOfiles%optdens%fn)
    else
       write(msg,*) 'Optimal Tdens not found. File', etb(IOfiles%optdens%fn), ' does not exist'
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    

    !
    ! dirichlet = (optimal dirichlet condition)
    !
    if ( IOfiles%dirichlet_bc%exist ) then
       write(msg,*) 'Dirichlet BC read from: ', etb(IOfiles%dirichlet_bc%fn)
    else
       write(msg,*) 'File', etb(IOfiles%dirichlet_bc%fn), ' does not exist. Zero Neumann BC '
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)


    if (IOfiles%dirichlet_bc%exist ) then
       this%dirichlet_exists = .True.
       !
       ! dirichlet
       !
       call this%dirichlet_bc%init(stderr, IOfiles%dirichlet_bc, 2, npot)

       allocate(&
            this%dirichlet_nodes(npot),&
            this%dirichlet_values(npot),&
            stat=res)
       if(res .ne. 0) rc = IOerr(stderr, err_inp , 'OdeInp_init', &
            ' type odein member dirichlet_nodes, dirichlet_values')
    else
       this%ndir = 0
       allocate(&
            this%dirichlet_nodes(this%ndir),&
            this%dirichlet_values(this%ndir),&
            stat=res)
       if(res .ne. 0) rc = IOerr(stderr, err_inp , 'OdeInp_init', &
            ' type odein member dirichlet_nodes, dirichlet_values')
  
    end if

    !
    ! penalization 
    ! 
    if (IOfiles%penalty%exist ) this%penalty_exists = .True.
    
    write(msg,*) '******** Penalization ************* '
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    
    ! penalty
    call this%penalty%init(stderr, IOfiles%penalty, 1,ntdens,zeros)
    if ( IOfiles%penalty%exist ) then
       write(msg,*) 'Penalty read from: ', etb(IOfiles%penalty%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty%fn), ' does not exist. No penalization'
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    
    
    ! weight penalty
    call this%penalty_weight%init(stderr, IOfiles%penalty_weight,1,ntdens,zeros)
    if ( IOfiles%penalty_weight%exist ) then
       write(msg,*) 'Penalty weight read from: ', etb(IOfiles%penalty_weight%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty_weight%fn), ' does not exist. No penalization'
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)


    ! weight penalty factor
    call this%penalty_factor%init(stderr, IOfiles%penalty_factor,1,1,just_one)
    if ( IOfiles%penalty_weight%exist ) then
       write(msg,*) 'Penalty factor read from: ', etb(IOfiles%penalty_factor%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty_factor%fn), ' does not exist. Default = 1.0'
    end if
    write(IOfiles%stdout%lun,*)    etb(msg)
    write(IOfiles%statistic%lun,*) etb(msg)
    
    if( .not. present( tzero) ) this%tzero=max(&
         this%pflux%TDtime(1),&
         this%pmass%TDtime(1),&
         this%decay%TDtime(1),&
         this%rhs_integrated%TDtime(1),&
         this%kappa%TDtime(1))

    
    deallocate(ones,zeros,stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'OdeInp_init', &
         'work array ones')

    
  end subroutine OdeInp_init


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type OdeInp
  !> Deallocate array in type OdeInp
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine OdeInp_kill(this, lun)
    implicit none
    class(OdeInp), intent(inout) :: this
    integer,       intent(in   ) :: lun
    ! local vars
    integer :: res
    logical :: rc

    !> Mandatory varaibles
    ! spatial varible
    call this%tdens0%kill(lun)
    call this%pflux%kill(lun)
    call this%pmass%kill(lun)
    call this%decay%kill(lun)
    call this%kappa%kill(lun)
    call this%rhs_integrated%kill(lun)
    
    !
    ! optional files
    !
    if ( this%optdens_exists ) then
       call this%optdens%kill(lun)
    end if

    !
    ! optional files
    !
    if ( allocated(this%dirichlet_values) ) then
       deallocate(&
         this%dirichlet_nodes,&
         this%dirichlet_values,&
         stat=res)
       if(res .ne. 0) rc = IOerr(lun, err_dealloc, 'OdeInp_kill', &
            ' type OdeInp member dirichlet_nodes dirichlet_values',res)
    end if
    
    !
    ! optinal penalty kill
    !
    if ( this%penalty%built)        call this%penalty%kill(lun) 
    if ( this%penalty_weight%built) call this%penalty_weight%kill(lun)
    if ( this%penalty_factor%built) call this%penalty_factor%kill(lun)

        
  end subroutine OdeInp_kill


  !>-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type OdeInp)
  !> Prints content of a variable of type OdeInp
  !> 
  !> usage:
  !>     call 'var'%info(lun)
  !>
  !> where:
  !> \param[in] lun: integer. output unit
  !>
  !<-------------------------------------------------------------
  subroutine OdeInp_info(this, lun, nsample)
    use Globals
    implicit none
    ! vars
    class(OdeInp), intent(in) :: this
    integer,       intent(in) :: lun
    integer,       intent(in) :: nsample
    !local vars
    integer :: i

    write(lun,*) ' '
    write(lun,*) ' Info: Ode Input'
    write(lun,*) ' Initial time      =', this%tzero 
    write(lun,*) ' Inputs'
    call this%tdens0%info(lun, nsample)
    call this%pflux%info(lun, nsample)
    call this%pmass%info(lun, nsample)
    call this%decay%info(lun, nsample)
    call this%kappa%info(lun, nsample)
    call this%rhs_integrated%info(lun, nsample)
    
    !
    if ( this%penalty%built) then
       call this%penalty%info(lun, nsample)
       call this%penalty_weight%info(lun, nsample)
    end if
    
  end subroutine OdeInp_info
  
  !>-------------------------------------------------------------
  !> Procedure to set all data at given time .
  !> (public procedure for type OdeInp)
  !> 
  !> usage:
  !>     call 'var'%set(IOfiles,time,endfile)
  !>
  !> where:
  !> \param[in] IOfiles -> type(IOfdescr). I/O file information
  !> \param[in] time    -> real. Time required
  !> \param[in] endfile -> logical. Gives True if one file reached 
  !>                         the end of file.
  !<-------------------------------------------------------------
  subroutine OdeInp_set(this, IOfiles, time,endfile)
    implicit none
    class(OdeInp),     intent(inout) :: this
    type(IOfdescr),    intent(in   ) :: IOfiles
    real(kind=double), intent(in   ) :: time
    logical,           intent(inout) :: endfile
    ! local
    logical :: rc
    integer :: lun_err
    integer :: inode,idir
    integer :: info
    logical :: endtdens0
    logical :: endpflux, endpmass, enddecay 
    logical :: endkappa, endrhs
    logical :: end2penalty, end2penalty_weight
    logical :: end_dirichlet
    logical :: end_lambda
    real(kind=double) :: imbalance,data(2)
    
    lun_err = IOfiles%stderr%lun

    
    call this%pflux%set(lun_err,IOfiles%pflux, time, endpflux)
    call this%pmass%set(lun_err,IOfiles%pmass, time, endpmass)
    call this%decay%set(lun_err,IOfiles%decay, time, enddecay)
    call this%kappa%set(lun_err,IOfiles%kappa, time, endkappa)

    info = 0
    call this%rhs_integrated%set(lun_err,&
         IOfiles%rhs_integrated, time,endrhs,info)
    if (info.ne.0) then
       rc = IOerr(lun_err, wrn_val , 'OdeInp_set', &
                  'error in reading rhs_integrated')
    end if

    if ( this%id_subgrid .eq. 0 ) then
       call this%rhs_integrated%set(lun_err,&
            IOfiles%rhs_grid_integrated, time,endrhs)
    else
       call this%rhs_integrated%set(lun_err,&
            IOfiles%rhs_integrated, time,endrhs)
    end if

    endfile =(endpflux .or. endpmass.or. enddecay .or. endkappa .or. endrhs)
    
    !
    ! read dirichlet boundary condition if imposed
    !
    if ( this%dirichlet_exists) then
       !
       ! read data
       !
       call this%dirichlet_bc%set(lun_err,&
            IOfiles%dirichlet_bc, time,end_dirichlet)
       !
       ! get number, indeceds of the dirichlet nodes and the value
       !
       this%ndir=0
       do inode=1,this%dirichlet_bc%ndata
          data=this%dirichlet_bc%TDActual(:,inode)
          if ( data(1) > zero ) then
             this%ndir=this%ndir+1
             this%dirichlet_nodes(this%ndir)=inode
             this%dirichlet_values(this%ndir)=data(2)
          end if
       end do
       
       endfile=endfile .or. end_dirichlet
    end if

    !
    ! check consistency of the rhs_integrated and boundary condition
    !
    if ( .not. this%dirichlet_exists) then
       if ( this%ndir .eq. 0) then
          imbalance = sum(this%rhs_integrated%TDActual(1,:))
          if(abs(imbalance) .ge. 1.0d-12 ) then
             rc = IOerr(lun_err, wrn_val , 'OdeInp_set', &
                  'type odeinp rhs not balanced ')
             write(lun_err,*) 'imbalance =', imbalance
          end if
       end if
    else
       do idir=1,this%ndir
           inode = this%dirichlet_nodes(idir)
          this%dirichlet_values(idir)=this%dirichlet_bc%TDActual(2,inode)
       end do       
    end if


    !
    if ( this%penalty%built) then
       call this%penalty%set(lun_err,IOfiles%penalty, time, end2penalty)
       call this%penalty_weight%set(lun_err,IOfiles%penalty, time, end2penalty_weight)
       endfile = (endfile .or. end2penalty .or. end2penalty_weight)
    end if
    
  end subroutine OdeInp_set



  !>---------------------------------------------------------------------
  !> Procedure to set all data at given time .
  !> (public procedure for type OdeInp)
  !> 
  !> usage:
  !>     call 'var'%set(IOfiles,ntime,time, id_ode, ode_inputs)
  !>
  !> where:
  !> \param[in] ntime      -> integer. slot to be filled ( 1 or 2)
  !> \param[in] time       -> real. Corresponding time
  !> \param[in] id_ode     -> integer. Ode identifier (1,2 or 3) 
  !> \param[in] ode_inputs -> type(odedata). Ode's input container
  !<---------------------------------------------------------------------
  subroutine set_odedata(this,&
         ntime, time,&
         id_ode,&
         ode_inputs)
      use DmkOdeData
      implicit none
      class(OdeInp),     intent(in   ) :: this
      integer,           intent(in   ) :: ntime
      real(kind=double), intent(in   ) :: time
      integer,           intent(in   ) :: id_ode
      type(OdeData),    intent(inout) :: ode_inputs

      !local
      logical :: endfile
      integer :: icell
      
      ode_inputs%id_ode = id_ode
      ode_inputs%ndir   = this%ndir
      
      do icell=1,ode_inputs%ntdens
         ode_inputs%tdens0(icell) = this%tdens0%TDactual(1,icell)
      end do
      

      !
      ! copy all current data in the given slot (first or second)
      !
      ode_inputs%times(ntime) = time

      ode_inputs%pflux(ntime) = this%pflux%TDactual(1,1)
      ode_inputs%pmass(ntime) = this%pmass%TDactual(1,1)
      ode_inputs%decay(ntime) = this%decay%TDactual(1,1)
      if ( ode_inputs%id_ode .eq. 1 ) then
         ode_inputs%pode(ntime) = ode_inputs%pflux(ntime)
      else
         ode_inputs%pode(ntime) = 2.0
      end if


      ode_inputs%kappa(:,ntime) = this%kappa%TDactual(1,:)
      ode_inputs%rhs_integrated(:,ntime) = this%rhs_integrated%TDactual(1,:)

      ode_inputs%penalty(:,ntime) = this%penalty%TDactual(1,:)
      ode_inputs%penalty_weight(:,ntime) = this%penalty_weight%TDactual(1,:)
      ode_inputs%penalty_factor(ntime) = this%penalty_factor%TDactual(1,1)

      ode_inputs%opt_tdens_exists = this%optdens_exists
      if (this%optdens_exists ) then
         do icell =1,ode_inputs%ntdens
            ode_inputs%opt_tdens(icell,ntime) = this%optdens%TDactual(1,icell)
         end do
      end if
      
      ode_inputs%opt_pot_exists = .False.

    end subroutine set_odedata
    
  end module OdeInputs
