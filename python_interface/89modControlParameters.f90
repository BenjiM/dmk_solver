!> Defines simulation control parameters
module ControlParameters
  use Globals
  use DmkControls
  use StdSparsePrec
  !use PreconditionerTuning
  !use Eigenv
  use LinearSolver
  !use PCG
  implicit none
  private
  type, extends(DmkCtrl), public :: CtrlPrm
     !type(DmkCtrl) :: dmk
     !> Controls for linear systems solution
     !> mainly via Krylov method
     type(input_solver) :: ctrl_solver  
     !> Copy of controls for linear systems solution
     !> mainly via Krylov method
     type(input_solver) :: ctrl_solver_original 
     !> Controls for linear systems solution
     !> involved in the application of 
     !> Augmented Lagrangian Preconditioner
     !> Used for P1P0 discretization
     type(input_solver) :: ctrl_solver_augmented
     !> Controls for the assembly of the preconditioner
     !> used in preconditioner Krylov-based scheme
     type(input_prec) :: ctrl_prec
     !> Controls for the assembly of the preconditioner
     !> used in preconditioner Krylov-based scheme
     type(input_prec) :: ctrl_prec_augmented
     !> Controls for tuning of Preconditioner
     !type(input_tuning) :: ctrl_tuning
     !> Controls for DACG procedure
     !> via PCG algorithm 
     !type(input_dacg) :: ctrl_dacg
   contains
     !> static constructor
     !> (public for type CtrlPrm)
     procedure, public, pass :: init => CtrlPrm_init
     !> static destructor
     !> (public for type CtrlPrm)
     procedure, public, pass :: kill => CtrlPrm_kill
     !> Procedure to generate formats 
     !> based on  iformat, rformat
     !> (public for type CtrlPrm)
     procedure, public, pass :: formatting
     !> Fucntion to build a separtor with title
     !>for statistic file
     !> (public for type CtrlPrm)
     procedure, public, pass :: separator
  end type CtrlPrm

contains
  
  

  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type CtrlPrm)
  !> Instantiate (allocate if necessary)
  !> and initialize (by also reading from input file)
  !> variable of type CtrlPrm
  !>
  !> usage:
  !>     call 'var'%init(IOfiles, ctrl_global)
  !>
  !> where:
  !> \param[in] ctrl_global -> type(CtrlPrm). Global controls
  !<-------------------------------------------------------------
  subroutine CtrlPrm_init(this, lun_err, basic_ctrl)
    use Globals
    implicit none
    class(CtrlPrm),  intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    type(DmkCtrl),  intent(in   ) :: basic_ctrl
    !
    
    this%DmkCtrl = basic_ctrl

    call this%ctrl_solver%init(&
         lun_err,&
         scheme=this%krylov_scheme,&
         lun_err=this%lun_err,&
         lun_out=this%lun_out,&
         iexit=this%iexit,&
         imax=this%imax,&
         iprt=this%iprt,&
         isol=this%isol,&
         tol_sol=this%tolerance_linear_solver,&
         iort=this%iort)

    
  end subroutine CtrlPrm_init

   subroutine CtrlPrm_write(this, lun)
    use Globals
    implicit none
    type(DmkCtrl),  intent(out) :: this
    integer,        intent(in ) :: lun

    namelist /mynamelist/ this

    write(lun, nml = mynamelist)
    
  end subroutine CtrlPrm_write

  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type CtrlPrm)
  !> only scalars, does nothing
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine CtrlPrm_kill(this)
    implicit none
    class(CtrlPrm), intent(inout) :: this
    
    call this%ctrl_solver%kill()   
    call this%ctrl_prec%kill()
    !call this%ctrl_tuning%kill()
    !call this%ctrl_dacg%kill()
    
  end subroutine CtrlPrm_kill

 function formatting(this,compressed,style) result (out_format)
    use Globals
    implicit none
    class(CtrlPrm),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: compressed
    integer, optional, intent(in   ) :: style
    character(len=256) :: out_format
    !local
    integer :: i,length
    character(len=1) :: singleton
    
    out_format='('  
    
    

    length=len(compressed)


    if ( .not. present(style) .or. (style .eq. 1) ) then
       do i=1, length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat_info)
          case('r')
             out_format=etb(etb(out_format)//this%rformat_info)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    if ( present(style) .and. (style .eq. 2) ) then
       do i=1,length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat)
          case('r')
             out_format=etb(etb(out_format)//this%rformat)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    out_format=etb(etb(out_format)//')')

  end function formatting

  function separator(this,title) result (outstring)
    use Globals
    implicit none
    class(CtrlPrm),             intent(in   ) :: this
    character(len=*), optional, intent(in   ) :: title
    integer, parameter                        :: width=71
    integer, parameter                        :: start=10
    character(len=width) :: outstring
    !local 
    integer :: i,lentitle
    character(len=start) :: strstart
    character(len=width-start-2) :: strend

    if ( present(title)) then
       lentitle=len(etb(title))

       do i=1,start
          write(strstart(i:i),'(a)') '*'
       end do

       do i=1,width-start-2-lentitle
          write(strend(i:i),'(a)') '*'
       end do
       outstring=strstart//' '//etb(title)//' '//strend
    else
       do i=1,width
          write(outstring(i:i),'(a)') '*'
       end do
    end if
       
  end function separator

 
  
  
  
  
    
    
    

       

          
       
!!$  subroutine reset_linear_solver_controls(this,&
!!$       info_solver,&
!!$       total_iterations,iter_media,&
!!$       itemp)
!!$    use Globals    
!!$    implicit none
!!$    class(ThisPrm),      intent(inout) :: this
!!$    type(output_pcg),    intent(in   ) :: info_pcg
!!$    integer,             intent(in   ) :: total_iterations
!!$    integer,             intent(in   ) :: iter_media
!!$    integer,             intent(in   ) :: itemp    
!!$    !local 
!!$    integer :: threshold
!!$
!!$    if ( ( this%id_buffer_prec .eq. 1) .or. &
!!$         ( this%id_buffer_prec .eq. 3) )  then 
!!$       ! Reference number of iterations = average iteration number
!!$       this%ref_iter = total_iterations  / itemp
!!$    else if ( this%id_buffer_prec .eq. 2) then 
!!$       ! Fixed by user reference number of iter
!!$       this%ref_iter = this%ref_iter
!!$    end if
!!$
!!$    threshold = min (this%ref_iter, iter_media)
!!$
!!$    build_prec              = 1 
!!$
!!$    ! If the preconditioner is not built at each iteration
!!$    if ( this%id_buffer_prec .ne. 0 ) then
!!$       ! If the prec. was calculated in the last solution of
!!$       ! a linear sysmat
!!$       ! update the last number of iterations
!!$       if ( this%elliptic%flag_prec ) then
!!$          this%last_iter_prec = this%iter_media
!!$       end if
!!$       ! Do not calculate the next prec
!!$       this%this_pcg%build_prec = 0
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if ( this%id_buffer_prec .eq. 1) then
!!$          if ( this%iter_media > &
!!$               int(this%iter_growth_factor * this%ref_iter) ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration 
!!$       ! or tdens is varying too fast
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 2) then
!!$          if ( ( this%iter_media > this%ref_iter ) .or. &
!!$               ( this%loc_var_tdens > 1.0d1 )    ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that min(last_nit, ref_iter) * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 3 ) then
!!$          if ( this%iter_media > int(this%iter_growth_factor * threshold )) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$    end if
!!$
!!$    this%this_pcg%build_defl = 0
!!$
!!$
!!$    if ( this%iter_media > &
!!$         int(this%eigen_growth_factor * this%ref_iter) ) then
!!$       this%this_pcg%build_defl = 1
!!$    else
!!$       this%this_pcg%build_defl = 0
!!$    end if
!!$  end subroutine reset_linear_solver_controls
  
end module ControlParameters
