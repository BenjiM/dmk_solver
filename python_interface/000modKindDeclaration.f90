!>-------------------------------------------------------------
!> Define global vars
!>
!> err numbers:
!> errno = [0:1][01-10] I/O errors (file existence etc...)
!>       = [0:1][11-20] input errors (reads)
!>       = [0:1][21-30] output errors (writes)
!>       = [0:1][31-40] alloc errors 
!>       = [0:1][41-50] dealloc errors
!>       = [0:1][51-60] vtk libray errors
!<------------------------------------------------------------- 
module KindDeclaration
  implicit none
  !> single precision parameter for real vars
  integer, parameter ::  single = kind(1.0e0)
  !> double precision parameter for real vars
  integer, parameter ::  double = kind(1.0d0)
  
  !> double parameters for useful constants
  real(kind=double), parameter  :: zero=0.0d0
  real(kind=double), parameter  :: one=1.0d0
  real(kind=double), parameter  :: two=2.0d0
  real(kind=double), parameter  :: three=3.0d0
  real(kind=double), parameter  :: four=4.0d0
  real(kind=double), parameter  :: onehalf=0.5d0
  real(kind=double), parameter  :: onethird=1.0d0/3.0d0
  real(kind=double), parameter  :: onefourth=1.0d0/4.0d0
  real(kind=double), parameter  :: onesixth=1.0d0/6.0d0
  real(kind=double), parameter  :: verysmall=1.0d-40
  real(kind=double), parameter  :: small=1.0d-15  
  real(kind=double), parameter  :: large=1.0d10
  real(kind=double), parameter  :: huge=1.0d30
  real(kind=double), parameter  :: pigreco=4.0d0*atan(one)

end module KindDeclaration
