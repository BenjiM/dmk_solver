subroutine example_monge_kantorovich_spectral(ninterval,simple_ctrl)
  use KindDeclaration
  use Globals
  use QuadGrid
  use Spectral
  use TdensPotentialSystem
  use DmkInputsData 
  use SpectralP0
  use DmkControls
  use ControlParameters
  use SimpleMatrix
  implicit none
  integer, intent(in):: ninterval
  type(DmkCtrl), intent(in) :: simple_ctrl

  ! reverse communication variables
  type(DmkInputs) :: ode_inputs
  type(tdpotsys) :: tdpot
  type(specp0) :: spectralp0
  type(quadmesh) :: grid
  type(spct) :: spec
  type(CtrlPrm) :: ctrl
  type(file) :: file_grid
  type(diagmat) :: diagonal_test
  integer :: flag
  integer :: info
  integer :: lun_err
  integer :: lun_out
  real(kind=double) :: current_time
  real(kind=double) :: deltat
  integer :: ntdens
  integer :: npot
  ! example dedicated varibles 
  real(kind=double) :: origin(2), measures(2)

  lun_err = ctrl%lun_err
  lun_out = ctrl%lun_out
  write(lun_out,*) 'Nintervals=',ninterval,'Polynomial degree=', simple_ctrl%ndeg

  call  diagonal_test%init(lun_err,ninterval)
  

  call ctrl%init(lun_err,simple_ctrl)

  !
  write(lun_out,'(a)') ctrl%separator(' Setup data begin')
  if (ctrl%lun_statistics>0) &
       write(ctrl%lun_statistics,'(a)') ctrl%separator('setup data begin')
  
  !
  ! initialized spatial discretization varible
  !
  call spec%init(lun_err,ctrl%ndeg)
  write(lun_out,'(a)') ctrl%separator('Spectral Intialized')
  !
  ! build square grid
  !
  origin=(/-one,-one/)
  measures=(/two,two/)
  open(77,file='debug.dat')
  call grid%init(lun_err,&
       ninterval,&
       origin, &
       measures )
  call file_grid%init(0,'grid.dat',333,'out')
  call grid%grid%write_mesh(0,file_grid)
  call file_grid%kill(0)
  write(lun_out,'(a)') ctrl%separator('Grid Built')
  
  

  ntdens = grid%grid%ncell 
  npot   = (spec%ndeg+1)**2
  
  call spectralp0%init(spec, grid,ctrl)
  close(77)
  write(*,*) grid%ninter
  write(lun_out,'(a)') ctrl%separator('Spectral-P0 Initialized')



  !
  ! build example inputs
  !
  call ode_inputs%init(lun_err, ntdens, npot,.True.)
  ! build rhs 
  call build_forcing(lun_err, 200, spec, ode_inputs%rhs)
  


  
  !
  ! init tdpot
  !
  call tdpot%init(lun_err,lun_out,ntdens, npot)
  tdpot%tdens=one
  
  !
  write(lun_out,'(a)') ctrl%separator('setup data ended')
  if (ctrl%lun_statistics>0) &
       write(ctrl%lun_statistics,'(a)') ctrl%separator('setup data ended')
  

  !
  ! run dmk time evolution
  !
  !
  ! start time cycle
  !
  info=0
  flag=1
  current_time=ode_inputs%time
  do while ( flag.ne.0) 
     call reverse_communication_cycle(&
          flag,info,current_time,&
          ode_inputs,tdpot,ctrl,spectralp0)

     select case (flag)
     case(2)
        !
        ! fill ode_inputs with data at current time
        !
        ! nothing to be done since we are in steady state
     end select
  end do


end subroutine example_monge_kantorovich_spectral

  
  !>-------------------------------------------------------
  !> Compute rhs for the sorcing term $f$ and the Neuman boundary contibution $q$
  !> identifed in the functions 'f_source' and 'q_neum' . 
  !> $(Rhssource)_i=\int_{\Omega}f\Psi_i 
  !>    - \int_{\partial \Omega} q \cdot n_{\partial \Omega} \Psi$ 
  !> $source_{j}=\frac{\int_{T_j}f}{|T_j|}$
  !> (procedure pubblic for type OdeCnst, used in init)
  !>
  !> usage :
  !>    call source_compute(id_source,id_neum,nnode,ntria,
  !>                        rhs,source,grid,p1)
  !>
  !> where 
  !> \param [in ] id_source     -> integer. Number of nodes in grid
  !> \param [in ] id_neum       -> integer. Number of triagnles in grid
  !> \param [in ] nnode         -> integer. Number of nodes in grid
  !> \param [in ] ntria         -> integer. Number of triangles in grid
  !> \param [out] rhs_source    -> real(nnode). Rhs of Poisson equ
  !> \param [out] source        -> real(ntria). Puntual value of sourcing term
  !> \param [in ] grid          -> type(mesh). Geometrical info
  !> \param [in ] p1            -> type(P1Galerkin). P1 Galerkin basis function info
  !<----------------------------------------------------------------------
  subroutine build_forcing(&
       lun_err,&
       ngauss,&
       spec,&
       rhs_forcing)
    use Globals
    use GaussQuadrature
    use Spectral
    implicit none
    integer,           intent(in   ) :: lun_err
    integer,           intent(in   ) :: ngauss
    type(spct),        intent(in   ) :: spec
    real(kind=double), intent(inout) :: rhs_forcing((spec%ndeg+1)**2)
    !local
    logical rc
    integer res
    integer i,j,k,k1,k2,m, id_source
    type(gaussq) :: gauss_source
    real(kind=double) :: int,xq, yq, wx, wy, wq, fq,dnrm2,ddot,test
    real(kind=double) :: pi=4.0d0*atan(1.0d0)
    real(kind=double) :: f_source
        
    call gauss_source%init(lun_err, ngauss)
    call gauss_source%on_cell(ax = -one, bx = one, ay= -one, by= one)

    m = 0
    test=0.0d0
    id_source = -1
    
    do m = 1, (spec%ndeg + 1) **2
       i = spec%saved_dx(m)
       j = spec%saved_dy(m)
       int= zero
       do k1 = 1, ngauss 
          xq = gauss_source%coord_cell(k1,1)
          wx = gauss_source%weight_cell(k1,1)
          
          do k2 = 1, ngauss
             yq = gauss_source%coord_cell(k2,2)
             wy = gauss_source%weight_cell(k2,2)
             wq = wx * wy
             
             !write(*,*) xq,yq,wq
             
             fq =  f_source( id_source, xq, yq ) *      &
                  spec%eval_vander1d( i, xq) * &
                  spec%eval_vander1d( j, yq)

             int = int +  wq * fq 
          end do

       end do
       rhs_forcing(m)=int
       !write(0,*) this%rhs_source(m)
       test=test+int**2

    end do
    !end do

    call gauss_source%kill(lun_err)
  end subroutine build_forcing

  function f_source(id_source,x,y)
    use KindDeclaration
    implicit none
    integer,           intent(in) :: id_source
    real(kind=double), intent(in) :: x,y
    real(kind=double) :: f_source

    !local variables
    logical :: test
    real(kind=double) :: pi=4.0d0*atan(1.0d0)
    real(kind=double) :: lambda_x,lambda_y,px,py,r,r1,r2,temp1(2),temp2(2)
    real(kind=double) :: dnrm2


    if (id_source .eq. -100) then
       f_source=(2.0d0*pi**2)*cos(pi*x)*cos(pi*y)
    end if

    if (id_source .eq. -99) then
       f_source=one
    end if



    if (id_source == -1) then
       f_source=zero
       ! positive part : piecewise constant with rectangular support
       test=(x>= -3*one/4 .and. x<= -1*one/4 .and. &
            y>= -1*one/2 .and. y<= 1*one/2 )
       if (test) then
          f_source= one
       end if
       ! negative part : piecewise constant with rectangular support
       test=(x>= 1*one/4 .and. x<= 3*one/4 .and. &
            y>= -1*one/2 .and. y<= 1*one/2 )
       if (test) then
          f_source= - one
       end if
    end if
    !
    !
    if (id_source == -2) then
       f_source = zero
       ! positive part : continous with rectangular support
       test=(x>= -3*one/4 .and. x<= -1*one/4 .and. &
            y>= -1*one/2 .and. y<=  1*one/2 )
       if (test) then
          lambda_x = onehalf * ( 4*x + 3 )
          lambda_y = onehalf * ( 2*y + 1 )
          f_source = 2 * sin(pi*lambda_x) * sin(pi*lambda_y) 
       end if
       ! negative part : continous with rectangular support
       test=(x>=  1*one/4 .and. x<= 3*one/4 .and. &
            y>= -1*one/2 .and. y<= 1*one/2 )
       if (test) then
          lambda_x = onehalf * ( 4*x - 1 )
          lambda_y = onehalf * ( 2*y + 1 )
          f_source = - 2 * sin(pi*lambda_x) * sin(pi*lambda_y) 
       end if
    end if
    !
    if (id_source == 1 ) then
       temp2=(/x-0.25d0,y-0.7d0/)
       r=dnrm2(2,temp2,1)
       if (r<0.15d0) then
          f_source=2*one
       end if
       px= x - 0.85d0
       py= y - 0.4d0
       test=( (px**2)/(0.1d0**2)+(py**2)/(0.35d0**2)<=1 )
       if (test) then
          f_source= - 2 * one
       end if
    end if

    if (id_source == 2 ) then
       temp1=(/x-0.25d0,y-0.25d0/)
       r1=sqrt(temp1(1)**2+temp1(2)**2)
       temp2=(/x-0.75d0,y-0.75d0/)
       r2=sqrt(temp2(1)**2+temp2(2)**2)
       if (r1<0.15d0) then
          f_source=1.0d0-(r1/0.15d0)**2
       end if
       if (r2<0.15d0) then
          f_source=1.0d0-(r2/0.15d0)**2
       end if

    end if

    if ( id_source == -3 ) then
       f_source = zero
       temp1=(/x-0.5d0,y-0.5d0/)
       r=sqrt(temp1(1)**2+temp1(2)**2)
       if (r <= 0.2d0) then
          f_source= 3.0d0*sin( 5.0d0*pi*r )**2
       end if
       if (( r> 0.2d0) .and. (r<=0.4d0)) then
          f_source= -sin (5.0d0*pi*(r-0.2d0))**2
       end if
    end if

    if ( id_source == 10 ) then
       f_source=2*pi**2*cos(pi*x)*cos(pi*y)
    end if

  end function f_source
