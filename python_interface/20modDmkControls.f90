!> Defines simulation control parameters
module DmkControls
  use KindDeclaration
  implicit none
  private
  type, public :: DmkCtrl
     !>--------------------------------------------------------
     !> Information Controls
     !>--------------------------------------------------------
     !> Control debug output. if > 0 full debug
     !>  0   :: no debug
     !>  1   :: print main steps (initialization)
     !>  2   :: print inputs and controls
     !>  12  :: 1 + 2 
     integer :: debug=0
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_state = 1
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_inputs_update = 1
     !> control mesh output. if =0 prints mesh and stops.
     !>                      if =1 normal simulation
     integer :: info_newton
     !> Format for integer output 
     character(len=20) :: iformat='I12'
     !> Format for real output
     character(len=20) :: rformat='1e18.10'
     !> Format for integer output 
     character(len=256) :: iformat_info='I9'
     !> Format for real output
     character(len=256) :: rformat_info='1pe9.2'
     !>--------------------------------------------------------
     !> Ode Global Controls
     !>--------------------------------------------------------
     !> lower bound for tdens
     real(kind=double) :: min_tdens=1.0d-10
     !> Selection parameter for tdens
     real(kind=double) ::  threshold_tdens = 0.0d0
     !>--------------------------------------------------------
     !> Time stepping approach
     !>--------------------------------------------------------
     !> Time discretization 
     !> Control the time-step discretization used
     !> time_stepping_scheme == 1 => explicit Euler 
     !> time_stepping_scheme == 2 => implicit Euler Newton
     !> time_stepping_scheme == 3 => accelerate explicit Euler
     integer :: time_discretization_scheme=1
     !> Tolerance in fix_point scheme for implicit time stepping 
     real(kind=double) :: tol_nonlinear=1e-8
     !> Maximum number of time iterations
     integer :: max_nonlinear_iterations=10
     !> Maximum number of time iterations
     integer :: max_restart_update=15
     !> Maximum number of time iterations
     integer :: max_restart_invert_jacobian=1
     !>--------------------------------------------------------
     !>  Time controls
     !>--------------------------------------------------------
     !> Initial time
     real(kind=double) :: tzero=zero
     !> Maximum number of time iterations
     !> max_time_it=0 => Only one elliptic equation is solved
     integer :: max_time_iterations=100
     !> Convergence tolerances
     !> Tolerance of var_tdens 
     real(kind=double) :: tolerance_system_variation=1.0d-4
     !> Time step control 
     !> Control of the time step
     !> flag_deltat_control = 0 => constant time step
     !> flag_deltat_control = 1 => increasing time step, with factor 
     !>                   "exp_rate" and upper bound "bound_deltat" 
     integer :: deltat_control
     !> Time step
     real(kind=double) :: deltat=0.1d0
     !> Expansion rate in id_ctrl_time == 2
     real(kind=double) :: deltat_expansion_rate=1.05d0
     !> Upper bound for deltat in id_ctrl_time == 2,3,4
     real(kind=double) :: deltat_upper_bound=1.0d0
     !> Lower bound for deltat in id_ctrl_time == 3,4
     real(kind=double) :: deltat_lower_bound=1.0d-3
     !>------------------------------------------------------
     ! Linear solver Controls
     !>------------------------------------------------------
     !> Solver scheme
     !> PCG
     !> BICGSTAB
     !> GMRES
     !> MINRES
     character(len=20) :: krylov_scheme='BICGSTAB'
     !> I/O err msg. unit
     integer :: linear_solver_lun_err=0
     !> I/O out msg. unit
     integer :: linear_solver_lun_out=6
     !> Integer identifying the Preconditinoer
     !>     iexit=0 => exit on absolute residual               
     !>     iexit=1 => exit on |r_k|/|b|     
     integer :: iexit=1
     !> Number of maximal iterations for iterative solver
     integer :: imax=1000
     !> Flag for printing option for itereative solver
     integer :: iprt=0
     !> Flag for starting solution
     !>  isol=0 => SOL      is the initial solution for pcg
     !>  isol=1 => PREC*RHS is the initial solution for pcg
     integer :: isol=0
     !> Tolerance for the scheme
     real(kind=double) :: tol_sol=1.0d-13
     !> Ortogonalized w.r.t. to given vectors 
     !> (usally the kernel of the matrix)
     integer :: iort=0
     !> Number of krylov directions saved in GMRES algorithm
     !> Used only for GMRES
     integer :: nrestart=20
     !> Flag to active to print steps for debugging
     !> debug=0 => no print 
     !> debug=1 => print steps
     integer :: debug_solver=0
     !> Linear Solver Tolerance
     real(kind=double) :: tolerance_linear_solver
     !> Diagonal scaling of linear system
     integer :: id_diagonal_scaling
     !> Diagonal scaling of linear system
     integer :: id_singular=0
     !>  Relaxation parameter add to tdens 
     real(kind=double) :: lambda
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: alpha_uzawa
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: omega_uzawa
     !>------------------------------------------------------
     !> Preconditioner controls 
     !>------------------------------------------------------
     !> Integer identifying the Preconditinoer
     !>     'identity'  P=Id (nothing is done)
     !>     'diag'      P=diag(A)^{-1}
     !>     'IC'        P=(U^{T}U)^{-1} with A~(U^T)U
     !>     'ILU'       P=(LU)^{-1} with A~LU
     !>     'C'         P=(M^{T}M)^{-1} with A=(U^T)U
     !>     'LU'        P=(LU)^{-1} with A=LU
     !>     'AGMG'      P=(A^-1) with agmg 
     character(len=20) :: prec_type='identity'
     !> Number of maximal extra-non-zero tem for
     !> row in the construction of IC_{fillin}
     integer :: n_fillin=0
     !> Tolerance for the dual drop procedure
     !> in the construction of IC_{fillin} (iprec=4)
     real(kind=double) :: tol_fillin=0.0d0
     !> Control for factorization breakdown
     !> 0 = stop factorization 
     !> 1 = try to recover factorization 
     !> 2 = try recovering in regime of "normal" number
     integer :: factorization_job=2
     !> Maximal number of BFGS update for triangular preconditioner
     integer :: max_bfgs=10
     !>-------------------------------------
     !> Prec. Buffering
     !>-------------------------------------
     !> Flag for buffering the calculation of the prec.
     !>  id_buffer_prec=0 => build at each iteration (no buffer)
     !>  id_buffer_prec=1 => build at each iteration (no buffer)
     !>  id_buffer_prec=2 => build at each iteration (no buffer)
     !>  id_buffer_prec=3 => build at each iteration (no buffer)
     integer :: id_buffer_prec
     !> Reference Number of iteration 
     !> Used for id_buffer_prec=1,...
     integer :: ref_iter 
     !> Factor of growth of iterations
     real(kind=double) :: prec_growth
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax2prec=1.0d-7
     !>-------------------------------------
     !> Augemented lagrangian controls
     !>-------------------------------------
     !> Gamma relaxation 
     real(kind=double) :: gamma=1.0d0
     !> how to build hatS
     integer :: id_hatS=2
     !>---------------------------------------------------------------
     !> Newton controls
     !>---------------------------------------------------------------
     !> Number of broyden update of jacobian preconditioner
     integer :: nbroyden=0
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: newton_method
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: reduced_jacobian=1
     !> Preconditioner strategy for linear system in newton scheme
     !> work in combination with reduced_jacobian
     !> ( reduced_jacobian + prec_newton ) =     
     !> 0 + 1 => full system + P=diag (stiff)^-1, D2^-1)
     !> 0 + 2 => full system + symmetization + P=mixed
     !> 0 + 3 => full system + P=mixed
     !> 0 + 4 => full system + P=triangular
     !> 1 + 1 => reduced system + P = (stiff)^-1) 
     !> 0 + 2 => reduced system + P = (stiff+ deltat BT D1 D2^{-1}C )^-1
     integer :: prec_newton=2
     !> Inecata Newton Method
     !> inexact_newton = 0 => off ( linear system solve with ctrl_sovler%tol)
     !> inexact_newton = 1 => on
     integer :: inexact_newton
     !>---------------------------------------------------------------
     !  Saving Procedure
     !>---------------------------------------------------------------
     !> control saving into vtk files
     !> id_save == 0 => no save
     !> id_save == 1 => save all
     !> id_save == 2 => save with fixed frequency
     !> id_save == 3 => save when the first digit of var_tdens changes
     !> Id. saving data into .dat
     integer :: id_save_dat=0
     !> Logical unit for Tdens
     !> Defualt (-1) will not to save to file
     integer :: lun_tdens=-1
     !> File name for Tdens
     character(1024) :: fn_tdens='tdens.dat'
     !> Logical unit for Potential
     !> Defualt (-1) will not to save to file
     integer :: lun_pot=-1
     !> File name for Tdens
     character(1024) :: fn_pot='pot.dat'
     !> frequency for control id_vtksave == 2 
     integer freq_dat
     !> Id. saving into matrix
     integer :: id_save_matrix
     !> frequency for control id_matrixsave == 2 
     integer freq_matrix
     !>--------------------------------------------------------------
     !> INFO CONTROLS
     !>-------------------------------------------------------------
     !> Flag to control print of time evolution 
     integer :: info_time_evolution=1
     !> Flag to control print of time evolution 
     integer :: info_functional=1
     !> I/O err msg. unit
     integer :: lun_err=0
     !> I/O out msg. unit
     integer :: lun_out=6
     !> Defualt (-1) will not to save to file
     integer :: lun_statistics=-1
     !> File name for Tdens
     character(1024) :: fn_statistics='statistic.dat'
     !>---------------------------------------------------------------
     !> Controls dedicated to P1-P0 and P1-P1 approach for DMK
     !>---------------------------------------------------------------
     !> Flag for use of subgrid (0= no subgrid, 1= subgrid )
     integer :: id_subgrid
     !>---------------------------------------------------------------
     !> Controls dedicated to SPECTRAL-P0 approach for DMK
     !>---------------------------------------------------------------
     integer :: preassembly_matrices
     !> Degree of 1d-polynomial
     integer :: ndeg=10
   contains
     !> Procedure to print line
     procedure, public, pass :: should_I_save
  end type DmkCtrl

contains

  subroutine should_I_save(&
       this,&
       save_test,&
       in_cycle,&
       itemp,&
       int_before,&
       current_var)
    use KindDeclaration
    implicit none
    class(DmkCtrl),    intent(in   ) :: this
    logical,           intent(inout) :: save_test
    logical,           intent(in   ) :: in_cycle
    integer,           intent(in   ) :: itemp
    integer,           intent(inout) :: int_before
    real(kind=double), intent(in   ) :: current_var

    !local 
    integer :: int_now

    save_test = .false.

    select case ( this%id_save_dat ) 
    case (1)
       !
       ! save each time step
       !
       save_test = .true.
    case (2)
       !
       ! save with give frequency and the last one
       !
       if ( (mod(itemp,this%freq_dat) == 0) .or. (.not. in_cycle) ) then
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    case (3)
       !
       ! save when variation change digit the last one
       !
       if ( ( itemp .ne. 0) ) then 
          int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
          if ( (int_now /= int_before) .or. (.not. in_cycle ) ) then
             int_before=int_now
             save_test = .true.
          end if
       else
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    case (4)
       !
       ! save only last
       !
       if ( (itemp .ne. 0) .and. (.not. in_cycle )) then
          save_test = .true.
       end if
    end select

  end subroutine should_I_save


end module DmkControls
