module LinearOperator
  use Globals
  implicit none
  private
  !> This module contains the structure varibles for 
  !> abstract definition of linear operator (matrix)
  !> and some simple extension, like linear combination
  !> or composition.
  !> Abstract type contains minimal informations to describe
  !> a linear operator namely, domain and codomain dimension, 
  !> symmetry or other properties. It also define the
  !> abstact procedure for linear operator application the
  !> computes vec_out = linear_operato(vec_in)
  public :: abs_linop,multiplication,adj_linop, set_adj_properties,array_linop

  !> Abstract type definition for linop-type, which
  !> represents any linear operator for REAL^{NCOL} to
  !> REAL^{NROW} Additional label are included to described
  !> the linerar operator structure that will limit or
  !> active matrix operations (only symmetric matrices can
  !> be passed to PCG procedure or can possibly have a
  !> Cholesky decomposition). While triangular matric can be
  !> easily solved.  The Abstract inferface requires that
  !> any type extdending the class abs_matrix must have a
  !> procedure defining the operation matrix-times-vector
  !>
  !> ISSUES 1: "replace intent(in) or intent(inout)?"
  !>         Now is intent(inout)

  !> Note that we set intent(inout) for abs_matrix in the
  !> muliptlication procedure while, in theory, application
  !> of linear operator should not affects its components.
  !> This is done due to the fact that scratch arrays
  !> included in the concrete type extendining abstract type
  !> linop may be used. For example matrix-vector operation
  !> $M \times v$ with $M =A^T A$ a scratch array is
  !> required. This can be removed passing an optional
  !> scratch type with the !  proper size.
  !> PRO   : intent(in) used, avoid modification 
  !> CONTRA: not self-contained, each time you need to pass
  !> or create scratch array with auxliary( type scrt in
  !> module Scratch may help) 
  !>
  !> ISSUES 2: "Use vec_out = vec_out +M *vec_in?"
  !>          Now vec_out = M * Vec_in
  !>
  !> The current impletation store in vec_out just M *vec_in
  !> In essentially all concrete type we first initialize vec_out to 
  !> zero and that make all computations. If insteand we add 
  !> an new arguments, possibly optional, that set vec_out to zero
  !> (letting the user to define it) we could compute
  !> vec_out = vec_out +M *vec_in with no additional effort.
  !> It will be a sort of daxpy procedure.
  !> PRO   : save computation time (maybe limited), 
  !>         save storage requirement, for example for linear combination
  !> CONTRA: more arguments, less clean procedure
  type, abstract :: abs_linop
     !> Number of rows
     integer :: nrow=0
     !> Number of columns
     integer :: ncol=0
     !> Number of non-zeros term
     integer :: nterm=0
     !> Flag for squared matrix
     !> i.e. if nrow .eq. ncol
     logical :: is_squared=.false.
     !> Logical flag for sysmmetric matrix
     logical :: is_symmetric=.false.
     !> Character indicating if the matrix is 
     !> lower ('L')  or upper ('U') or not ('N')
     !> which is the default case
     character(len=1) :: triangular='N'
     !> Dimension of the Kernel space
     !integer :: dim_kernel=0
     ! Dimension (ncol,dim_kernel)
     ! Null space of the matrix
     !real(kind=double), allocatable :: kernel(:,:)
     !> Logical flag matrix with one on the diagonal
     ! TODO: Used only in SparseMatrix Module
     ! remove?
     logical :: unitary_diag=.false.
   contains
     !>-----------------------------------------------------------
     !> Procedure for computation of (matrix) times (vector)
     procedure(multiplication), deferred :: Mxv
     !>-----------------------------------------------------------
     !> Procedure for computation of (matrix) times (vector)
     procedure, public, pass :: info
  end type abs_linop
  
  !> Abstract type definition for adjount linop-type,
  !> i.e. given a matrix A with domian dimension want shoul be $A^T$
  !> represents any linear operator for REAL^{NCOL} to
  !> REAL^{NROW} Additional label are included to described
  !> the linerar operator structure that will limit or
  type, extends(abs_linop), abstract :: adj_linop
   contains
     !>-----------------------------------------------------------
     !> Procedure implcit assignement of adjoint_operator
     !>-----------------------------------------------------------
     !procedure(init_adj), deferred :: inita
     !>-----------------------------------------------------------
     !> Publlic procedure to set the properties
     !> adjoint operator AT (nrow ncol etc) 
     !> for origianl operator A
     !>-----------------------------------------------------------
     procedure, public, pass:: set_adj_properties
  end type adj_linop

  !> Derived type used to create list of pointers to abstract 
  !> Linera Operators, that can be allocatable.
  !> (Fortan does not allow to create arrays of pointers)
  !> E.g. in a Block-Matrix
  !> M = (A D) with A and D of different type, we can create
  !> an array type that points toward A and D.
  !> Declarations: 
  !>  type(spmat)     :: A
  !>  type(diagmat  ) :: D
  !>  type(array_mat) :: list(2)
  !> Assignment
  !>  list(1)%linop => A
  !>  list(2)%linop => D
  !> See BlockMatrix module for a concrete example
  type, public :: array_linop
     !> Dimension (nmats)
     !> Array that contains the non-zero blocks
     class(abs_linop), pointer :: linop
  end type array_linop
  




  abstract interface
     !>-------------------------------------------------------------
     !> Abstract procedure defining the interface for a general
     !> matrix-vector multiplication
     !>         vec_out = (M) times (vec_in)
     !> (public procedure for class abs_matrix)
     !> 
     !> usage:
     !>     call 'var'%Mxv(vec_in,vec_out,[info])
     !>
     !> where 
     !> \param[in   ] vec_in            -> real, dimension('var'%ncol)
     !>                                   vector to be multiplied
     !> \param[inout] vec_out           -> real, dimension('var'%nrow)
     !>                                    vector (M) times (vec_in) 
     !> \param[in   ] (optional) info   -> integer. Info number
     !>                                    in case of error 
     !> \param[in   ] (optional) lun_er -> integer. Info number
     !>                                    in case of error 
     !<-------------------------------------------------------------
     subroutine multiplication(this,vec_in,vec_out,info,lun_err)
       use Globals
       import abs_linop
       implicit none
       class(abs_linop), intent(inout) :: this
       real(kind=double), intent(in   ) :: vec_in(this%ncol)
       real(kind=double), intent(inout) :: vec_out(this%nrow)
       integer, optional, intent(inout) :: info
       integer, optional, intent(in   ) :: lun_err
     end subroutine multiplication

  end interface

  type, extends(abs_linop), public :: eye
     !> Eye preconditioner
     !>  vec_out = Id vec_in
   contains
     !> Static constructor 
     !> (procedure public for type eye)
     procedure, public, pass :: init => init_eye
     !> Static destructor
     !> (procedure public for type eye)
     procedure, public, pass :: kill => kill_eye
     !> Info procedure  eye matrix
     !> (public procedure for type eye)
     procedure, public, pass :: info => info_eye
     !> Compute matrix times vector operatoration
     !> (public procedure for type eye)
     procedure, public,  pass :: Mxv => apply_eye
     !> Compute matrix transpose time vector operation
     !> (public procedure for type eye)
     procedure, public,  pass :: MTxv => apply_eye
  end type eye

contains
  !>------------------------------------------------------
  !> Subruotine to define all proprieties of
  !> adjoint linear operator. Useful in the 
  !> initialiazionion of concrete type extending the
  !> adj_linop class.
  !> (public procedure for class adj_linop)
  !> 
  !> usage:
  !>     call 'var'%set_adj_properties(matrix)
  !<-------------------------------------------------------------
  subroutine set_adj_properties(this,matrix)
    implicit none
    class(adj_linop),  intent(inout) :: this
    class(abs_linop),  intent(in   ) :: matrix

    this%nrow         = matrix%ncol
    this%ncol         = matrix%nrow
    this%nterm        = matrix%nterm
    this%is_symmetric = matrix%is_symmetric
    this%unitary_diag = matrix%unitary_diag


    ! set properties of transpose matrix
    select case (matrix%triangular)
    case ('N')
       this%triangular = 'N'
    case ('U')
       this%triangular = 'L'
    case ('L')
       this%triangular = 'U'
    end select


  end subroutine set_adj_properties

  !>------------------------------------------------------
  !> Subruotine to print linear operator properties
  !> (public procedure for class adj_linop)
  !> 
  !> usage:
  !>     call 'var'%info(lun)
  !> where :
  !>
  !> \param[in   ] lun -> integer. I/O logical unit 
  !<-------------------------------------------------------------
  subroutine info(this,lun)
    implicit none
    class(abs_linop),  intent(in   ) :: this
    integer,           intent(in   ) :: lun

    write(lun,*) 'NROW= ', this%nrow, ' NCOL= ', this%ncol, 'NNZ = ' ,this%nterm 
    if ( this%is_symmetric) then
       write(lun,*) 'SYMMETRIC' 
    else
       if ( this%triangular .ne. 'N') then
          if ( this%triangular .eq. 'U')  then
             write(lun,*) 'UPPER TRIANGULAR' 
          else
             write(lun,*) 'LOWER TRIANGULAR' 
          end if
       else
          write(lun,*) 'NON SYMMETRIC' 
       end if
    end if
  end subroutine info

  subroutine  init_eye(this,nrow_ncol)
    implicit none
    class(eye), intent(inout) :: this
    integer,        intent(in ) :: nrow_ncol
    this%ncol = nrow_ncol
    this%nrow = nrow_ncol
    this%is_symmetric = .true.
    this%is_squared = .true.
  end subroutine init_eye

  subroutine  kill_eye(this)
    implicit none
    class(eye), intent(inout) :: this
    this%ncol = 0
    this%nrow = 0
  end subroutine kill_eye

  subroutine  info_eye(this,lun)
    implicit none
    class(eye), intent(in) :: this 
    integer,        intent(in   ) :: lun

    write(lun,*) 'Nequ= ', this%nrow
  end subroutine info_eye

  recursive subroutine apply_eye(this,vec_in,vec_out,info,lun_err)
    use Globals
    class(eye),   intent(inout) :: this
    real(kind=double), intent(in   ) :: vec_in(this%ncol)
    real(kind=double), intent(inout) :: vec_out(this%nrow)
    integer, optional, intent(inout) :: info
    integer, optional, intent(in   ) :: lun_err

    vec_out = vec_in
  end subroutine apply_eye 
  

end module LinearOperator




