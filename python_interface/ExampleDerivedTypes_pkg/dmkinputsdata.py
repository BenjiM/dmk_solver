"""
Module dmkinputsdata


Defined at 09modDmkInputsData.fpp lines 5-294

"""
from __future__ import print_function, absolute_import, division
import _ExampleDerivedTypes_pkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("ExampleDerivedTypes_pkg.DmkInputs")
class DmkInputs(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=dmkinputs)
    
    
    Defined at 09modDmkInputsData.fpp lines 28-118
    
    """
    def __init__(self, handle=None):
        """
        self = Dmkinputs()
        
        
        Defined at 09modDmkInputsData.fpp lines 28-118
        
        
        Returns
        -------
        this : Dmkinputs
        	Object to be constructed
        
        
        Automatically generated constructor for dmkinputs
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _ExampleDerivedTypes_pkg.f90wrap_dmkinputs_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Dmkinputs
        
        
        Defined at 09modDmkInputsData.fpp lines 28-118
        
        Parameters
        ----------
        this : Dmkinputs
        	Object to be destructed
        
        
        Automatically generated destructor for dmkinputs
        """
        if self._alloc:
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs_finalise(this=self._handle)
    
    @property
    def id_ode(self):
        """
        Element id_ode ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 33
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__id_ode(self._handle)
    
    @id_ode.setter
    def id_ode(self, id_ode):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__id_ode(self._handle, id_ode)
    
    @property
    def ntimes(self):
        """
        Element ntimes ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 35
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__ntimes(self._handle)
    
    @ntimes.setter
    def ntimes(self, ntimes):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__ntimes(self._handle, ntimes)
    
    @property
    def time(self):
        """
        Element time ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 37
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__time(self._handle)
    
    @time.setter
    def time(self, time):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__time(self._handle, time)
    
    @property
    def ntdens(self):
        """
        Element ntdens ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 42
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__ntdens(self._handle)
    
    @ntdens.setter
    def ntdens(self, ntdens):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__ntdens(self._handle, ntdens)
    
    @property
    def npot(self):
        """
        Element npot ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 44
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__npot(self._handle)
    
    @npot.setter
    def npot(self, npot):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__npot(self._handle, npot)
    
    @property
    def dirichlet_exists(self):
        """
        Element dirichlet_exists ftype=logical pytype=bool
        
        
        Defined at 09modDmkInputsData.fpp line 47
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__dirichlet_exists(self._handle)
    
    @dirichlet_exists.setter
    def dirichlet_exists(self, dirichlet_exists):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__dirichlet_exists(self._handle, \
            dirichlet_exists)
    
    @property
    def ndir(self):
        """
        Element ndir ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 50
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__ndir(self._handle)
    
    @ndir.setter
    def ndir(self, ndir):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__ndir(self._handle, ndir)
    
    @property
    def dirichlet_nodes(self):
        """
        Element dirichlet_nodes ftype=integer pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 53
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__dirichlet_nodes(self._handle)
        if array_handle in self._arrays:
            dirichlet_nodes = self._arrays[array_handle]
        else:
            dirichlet_nodes = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__dirichlet_nodes)
            self._arrays[array_handle] = dirichlet_nodes
        return dirichlet_nodes
    
    @dirichlet_nodes.setter
    def dirichlet_nodes(self, dirichlet_nodes):
        self.dirichlet_nodes[...] = dirichlet_nodes
    
    @property
    def dirichlet_values(self):
        """
        Element dirichlet_values ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 56
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__dirichlet_values(self._handle)
        if array_handle in self._arrays:
            dirichlet_values = self._arrays[array_handle]
        else:
            dirichlet_values = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__dirichlet_values)
            self._arrays[array_handle] = dirichlet_values
        return dirichlet_values
    
    @dirichlet_values.setter
    def dirichlet_values(self, dirichlet_values):
        self.dirichlet_values[...] = dirichlet_values
    
    @property
    def lambda_(self):
        """
        Element lambda_ ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 61
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__lambda_(self._handle)
    
    @lambda_.setter
    def lambda_(self, lambda_):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__lambda_(self._handle, lambda_)
    
    @property
    def lasso(self):
        """
        Element lasso ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 65
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__lasso(self._handle)
    
    @lasso.setter
    def lasso(self, lasso):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__lasso(self._handle, lasso)
    
    @property
    def pflux(self):
        """
        Element pflux ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 69
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__pflux(self._handle)
    
    @pflux.setter
    def pflux(self, pflux):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__pflux(self._handle, pflux)
    
    @property
    def decay(self):
        """
        Element decay ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 72
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__decay(self._handle)
    
    @decay.setter
    def decay(self, decay):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__decay(self._handle, decay)
    
    @property
    def pmass(self):
        """
        Element pmass ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 75
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__pmass(self._handle)
    
    @pmass.setter
    def pmass(self, pmass):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__pmass(self._handle, pmass)
    
    @property
    def pode(self):
        """
        Element pode ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 79
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__pode(self._handle)
    
    @pode.setter
    def pode(self, pode):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__pode(self._handle, pode)
    
    @property
    def penalty_exists(self):
        """
        Element penalty_exists ftype=logical pytype=bool
        
        
        Defined at 09modDmkInputsData.fpp line 84
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__penalty_exists(self._handle)
    
    @penalty_exists.setter
    def penalty_exists(self, penalty_exists):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__penalty_exists(self._handle, \
            penalty_exists)
    
    @property
    def penalty_factor(self):
        """
        Element penalty_factor ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 87
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__get__penalty_factor(self._handle)
    
    @penalty_factor.setter
    def penalty_factor(self, penalty_factor):
        _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__set__penalty_factor(self._handle, \
            penalty_factor)
    
    @property
    def penalty_weight(self):
        """
        Element penalty_weight ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 91
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__penalty_weight(self._handle)
        if array_handle in self._arrays:
            penalty_weight = self._arrays[array_handle]
        else:
            penalty_weight = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__penalty_weight)
            self._arrays[array_handle] = penalty_weight
        return penalty_weight
    
    @penalty_weight.setter
    def penalty_weight(self, penalty_weight):
        self.penalty_weight[...] = penalty_weight
    
    @property
    def penalty(self):
        """
        Element penalty ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 96
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__penalty(self._handle)
        if array_handle in self._arrays:
            penalty = self._arrays[array_handle]
        else:
            penalty = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__penalty)
            self._arrays[array_handle] = penalty
        return penalty
    
    @penalty.setter
    def penalty(self, penalty):
        self.penalty[...] = penalty
    
    @property
    def kappa(self):
        """
        Element kappa ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 101
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__kappa(self._handle)
        if array_handle in self._arrays:
            kappa = self._arrays[array_handle]
        else:
            kappa = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__kappa)
            self._arrays[array_handle] = kappa
        return kappa
    
    @kappa.setter
    def kappa(self, kappa):
        self.kappa[...] = kappa
    
    @property
    def rhs(self):
        """
        Element rhs ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 107
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__rhs(self._handle)
        if array_handle in self._arrays:
            rhs = self._arrays[array_handle]
        else:
            rhs = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs__array__rhs)
            self._arrays[array_handle] = rhs
        return rhs
    
    @rhs.setter
    def rhs(self, rhs):
        self.rhs[...] = rhs
    
    def __str__(self):
        ret = ['<dmkinputs>{\n']
        ret.append('    id_ode : ')
        ret.append(repr(self.id_ode))
        ret.append(',\n    ntimes : ')
        ret.append(repr(self.ntimes))
        ret.append(',\n    time : ')
        ret.append(repr(self.time))
        ret.append(',\n    ntdens : ')
        ret.append(repr(self.ntdens))
        ret.append(',\n    npot : ')
        ret.append(repr(self.npot))
        ret.append(',\n    dirichlet_exists : ')
        ret.append(repr(self.dirichlet_exists))
        ret.append(',\n    ndir : ')
        ret.append(repr(self.ndir))
        ret.append(',\n    dirichlet_nodes : ')
        ret.append(repr(self.dirichlet_nodes))
        ret.append(',\n    dirichlet_values : ')
        ret.append(repr(self.dirichlet_values))
        ret.append(',\n    lambda_ : ')
        ret.append(repr(self.lambda_))
        ret.append(',\n    lasso : ')
        ret.append(repr(self.lasso))
        ret.append(',\n    pflux : ')
        ret.append(repr(self.pflux))
        ret.append(',\n    decay : ')
        ret.append(repr(self.decay))
        ret.append(',\n    pmass : ')
        ret.append(repr(self.pmass))
        ret.append(',\n    pode : ')
        ret.append(repr(self.pode))
        ret.append(',\n    penalty_exists : ')
        ret.append(repr(self.penalty_exists))
        ret.append(',\n    penalty_factor : ')
        ret.append(repr(self.penalty_factor))
        ret.append(',\n    penalty_weight : ')
        ret.append(repr(self.penalty_weight))
        ret.append(',\n    penalty : ')
        ret.append(repr(self.penalty))
        ret.append(',\n    kappa : ')
        ret.append(repr(self.kappa))
        ret.append(',\n    rhs : ')
        ret.append(repr(self.rhs))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

@f90wrap.runtime.register_class("ExampleDerivedTypes_pkg.ProblemData")
class ProblemData(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=problemdata)
    
    
    Defined at 09modDmkInputsData.fpp lines 120-148
    
    """
    def __init__(self, handle=None):
        """
        self = Problemdata()
        
        
        Defined at 09modDmkInputsData.fpp lines 120-148
        
        
        Returns
        -------
        this : Problemdata
        	Object to be constructed
        
        
        Automatically generated constructor for problemdata
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _ExampleDerivedTypes_pkg.f90wrap_problemdata_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Problemdata
        
        
        Defined at 09modDmkInputsData.fpp lines 120-148
        
        Parameters
        ----------
        this : Problemdata
        	Object to be destructed
        
        
        Automatically generated destructor for problemdata
        """
        if self._alloc:
            _ExampleDerivedTypes_pkg.f90wrap_problemdata_finalise(this=self._handle)
    
    @property
    def ntdens(self):
        """
        Element ntdens ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 125
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_problemdata__get__ntdens(self._handle)
    
    @ntdens.setter
    def ntdens(self, ntdens):
        _ExampleDerivedTypes_pkg.f90wrap_problemdata__set__ntdens(self._handle, ntdens)
    
    @property
    def npot(self):
        """
        Element npot ftype=integer  pytype=int
        
        
        Defined at 09modDmkInputsData.fpp line 127
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_problemdata__get__npot(self._handle)
    
    @npot.setter
    def npot(self, npot):
        _ExampleDerivedTypes_pkg.f90wrap_problemdata__set__npot(self._handle, npot)
    
    @property
    def opt_tdens_exists(self):
        """
        Element opt_tdens_exists ftype=logical pytype=bool
        
        
        Defined at 09modDmkInputsData.fpp line 132
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__get__opt_tdens_exists(self._handle)
    
    @opt_tdens_exists.setter
    def opt_tdens_exists(self, opt_tdens_exists):
        \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__set__opt_tdens_exists(self._handle, \
            opt_tdens_exists)
    
    @property
    def opt_tdens(self):
        """
        Element opt_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 135
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__opt_tdens(self._handle)
        if array_handle in self._arrays:
            opt_tdens = self._arrays[array_handle]
        else:
            opt_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__opt_tdens)
            self._arrays[array_handle] = opt_tdens
        return opt_tdens
    
    @opt_tdens.setter
    def opt_tdens(self, opt_tdens):
        self.opt_tdens[...] = opt_tdens
    
    @property
    def opt_pot_exists(self):
        """
        Element opt_pot_exists ftype=logical pytype=bool
        
        
        Defined at 09modDmkInputsData.fpp line 137
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__get__opt_pot_exists(self._handle)
    
    @opt_pot_exists.setter
    def opt_pot_exists(self, opt_pot_exists):
        _ExampleDerivedTypes_pkg.f90wrap_problemdata__set__opt_pot_exists(self._handle, \
            opt_pot_exists)
    
    @property
    def opt_pot(self):
        """
        Element opt_pot ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 140
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__opt_pot(self._handle)
        if array_handle in self._arrays:
            opt_pot = self._arrays[array_handle]
        else:
            opt_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__opt_pot)
            self._arrays[array_handle] = opt_pot
        return opt_pot
    
    @opt_pot.setter
    def opt_pot(self, opt_pot):
        self.opt_pot[...] = opt_pot
    
    @property
    def tdens0(self):
        """
        Element tdens0 ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 143
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__tdens0(self._handle)
        if array_handle in self._arrays:
            tdens0 = self._arrays[array_handle]
        else:
            tdens0 = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__tdens0)
            self._arrays[array_handle] = tdens0
        return tdens0
    
    @tdens0.setter
    def tdens0(self, tdens0):
        self.tdens0[...] = tdens0
    
    @property
    def pot0(self):
        """
        Element pot0 ftype=real(kind=double) pytype=float
        
        
        Defined at 09modDmkInputsData.fpp line 146
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__pot0(self._handle)
        if array_handle in self._arrays:
            pot0 = self._arrays[array_handle]
        else:
            pot0 = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _ExampleDerivedTypes_pkg.f90wrap_problemdata__array__pot0)
            self._arrays[array_handle] = pot0
        return pot0
    
    @pot0.setter
    def pot0(self, pot0):
        self.pot0[...] = pot0
    
    def __str__(self):
        ret = ['<problemdata>{\n']
        ret.append('    ntdens : ')
        ret.append(repr(self.ntdens))
        ret.append(',\n    npot : ')
        ret.append(repr(self.npot))
        ret.append(',\n    opt_tdens_exists : ')
        ret.append(repr(self.opt_tdens_exists))
        ret.append(',\n    opt_tdens : ')
        ret.append(repr(self.opt_tdens))
        ret.append(',\n    opt_pot_exists : ')
        ret.append(repr(self.opt_pot_exists))
        ret.append(',\n    opt_pot : ')
        ret.append(repr(self.opt_pot))
        ret.append(',\n    tdens0 : ')
        ret.append(repr(self.tdens0))
        ret.append(',\n    pot0 : ')
        ret.append(repr(self.pot0))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

def dmkinputs_constructor(self, lun_err, ntdens, npot, set2default=None):
    """
    dmkinputs_constructor(self, lun_err, ntdens, npot[, set2default])
    
    
    Defined at 09modDmkInputsData.fpp lines 181-228
    
    Parameters
    ----------
    this : Dmkinputs
    lun_err : int
    ntdens : int
    npot : int
    set2default : bool
    
    """
    _ExampleDerivedTypes_pkg.f90wrap_dmkinputs_constructor(this=self._handle, \
        lun_err=lun_err, ntdens=ntdens, npot=npot, set2default=set2default)


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "dmkinputsdata".')

for func in _dt_array_initialisers:
    func()
