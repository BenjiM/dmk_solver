"""
Module globals


Defined at 00modGlobals.fpp lines 16-1318

"""
from __future__ import print_function, absolute_import, division
import _ExampleDerivedTypes_pkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("ExampleDerivedTypes_pkg.file")
class file(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=file)
    
    
    Defined at 00modGlobals.fpp lines 48-61
    
    """
    def __init__(self, handle=None):
        """
        self = File()
        
        
        Defined at 00modGlobals.fpp lines 48-61
        
        
        Returns
        -------
        this : File
        	Object to be constructed
        
        
        Automatically generated constructor for file
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _ExampleDerivedTypes_pkg.f90wrap_file_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class File
        
        
        Defined at 00modGlobals.fpp lines 48-61
        
        Parameters
        ----------
        this : File
        	Object to be destructed
        
        
        Automatically generated destructor for file
        """
        if self._alloc:
            _ExampleDerivedTypes_pkg.f90wrap_file_finalise(this=self._handle)
    
    @property
    def exist(self):
        """
        Element exist ftype=logical pytype=bool
        
        
        Defined at 00modGlobals.fpp line 49
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_file__get__exist(self._handle)
    
    @exist.setter
    def exist(self, exist):
        _ExampleDerivedTypes_pkg.f90wrap_file__set__exist(self._handle, exist)
    
    @property
    def lun(self):
        """
        Element lun ftype=integer  pytype=int
        
        
        Defined at 00modGlobals.fpp line 50
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_file__get__lun(self._handle)
    
    @lun.setter
    def lun(self, lun):
        _ExampleDerivedTypes_pkg.f90wrap_file__set__lun(self._handle, lun)
    
    @property
    def fn(self):
        """
        Element fn ftype=character (len=256) pytype=str
        
        
        Defined at 00modGlobals.fpp line 51
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_file__get__fn(self._handle)
    
    @fn.setter
    def fn(self, fn):
        _ExampleDerivedTypes_pkg.f90wrap_file__set__fn(self._handle, fn)
    
    @property
    def fnloc(self):
        """
        Element fnloc ftype=character (len=256) pytype=str
        
        
        Defined at 00modGlobals.fpp line 52
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_file__get__fnloc(self._handle)
    
    @fnloc.setter
    def fnloc(self, fnloc):
        _ExampleDerivedTypes_pkg.f90wrap_file__set__fnloc(self._handle, fnloc)
    
    def __str__(self):
        ret = ['<file>{\n']
        ret.append('    exist : ')
        ret.append(repr(self.exist))
        ret.append(',\n    lun : ')
        ret.append(repr(self.lun))
        ret.append(',\n    fn : ')
        ret.append(repr(self.fn))
        ret.append(',\n    fnloc : ')
        ret.append(repr(self.fnloc))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module "globals".')

for func in _dt_array_initialisers:
    func()
