"""
Module kinddeclaration


Defined at 000modKindDeclaration.fpp lines 16-37

"""
from __future__ import print_function, absolute_import, division
import _ExampleDerivedTypes_pkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

def get_single():
    """
    Element single ftype=integer pytype=int
    
    
    Defined at 000modKindDeclaration.fpp line 19
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__single()

single = get_single()

def get_double_bn():
    """
    Element double_bn ftype=integer pytype=int
    
    
    Defined at 000modKindDeclaration.fpp line 21
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__double_bn()

double = get_double_bn()

def get_zero():
    """
    Element zero ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 24
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__zero()

zero = get_zero()

def get_one():
    """
    Element one ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 25
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__one()

one = get_one()

def get_two():
    """
    Element two ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 26
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__two()

two = get_two()

def get_three():
    """
    Element three ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 27
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__three()

three = get_three()

def get_four():
    """
    Element four ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 28
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__four()

four = get_four()

def get_onehalf():
    """
    Element onehalf ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 29
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__onehalf()

onehalf = get_onehalf()

def get_onethird():
    """
    Element onethird ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 30
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__onethird()

onethird = get_onethird()

def get_onefourth():
    """
    Element onefourth ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 31
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__onefourth()

onefourth = get_onefourth()

def get_onesixth():
    """
    Element onesixth ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 32
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__onesixth()

onesixth = get_onesixth()

def get_verysmall():
    """
    Element verysmall ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 33
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__verysmall()

verysmall = get_verysmall()

def get_small():
    """
    Element small ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 34
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__small()

small = get_small()

def get_large():
    """
    Element large ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 35
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__large()

large = get_large()

def get_huge():
    """
    Element huge ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 36
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__huge()

huge = get_huge()

def get_pigreco():
    """
    Element pigreco ftype=real(kind=double) pytype=float
    
    
    Defined at 000modKindDeclaration.fpp line 37
    
    """
    return _ExampleDerivedTypes_pkg.f90wrap_kinddeclaration__get__pigreco()

pigreco = get_pigreco()


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "kinddeclaration".')

for func in _dt_array_initialisers:
    func()
