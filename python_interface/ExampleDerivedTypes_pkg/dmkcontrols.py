"""
Module dmkcontrols


Defined at 20modDmkControls.fpp lines 6-333

"""
from __future__ import print_function, absolute_import, division
import _ExampleDerivedTypes_pkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("ExampleDerivedTypes_pkg.DmkCtrl")
class DmkCtrl(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=dmkctrl)
    
    
    Defined at 20modDmkControls.fpp lines 10-266
    
    """
    def __init__(self, handle=None):
        """
        self = Dmkctrl()
        
        
        Defined at 20modDmkControls.fpp lines 10-266
        
        
        Returns
        -------
        this : Dmkctrl
        	Object to be constructed
        
        
        Automatically generated constructor for dmkctrl
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _ExampleDerivedTypes_pkg.f90wrap_dmkctrl_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Dmkctrl
        
        
        Defined at 20modDmkControls.fpp lines 10-266
        
        Parameters
        ----------
        this : Dmkctrl
        	Object to be destructed
        
        
        Automatically generated destructor for dmkctrl
        """
        if self._alloc:
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl_finalise(this=self._handle)
    
    @property
    def debug(self):
        """
        Element debug ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 19
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__debug(self._handle)
    
    @debug.setter
    def debug(self, debug):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__debug(self._handle, debug)
    
    @property
    def info_state(self):
        """
        Element info_state ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 22
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__info_state(self._handle)
    
    @info_state.setter
    def info_state(self, info_state):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__info_state(self._handle, \
            info_state)
    
    @property
    def info_inputs_update(self):
        """
        Element info_inputs_update ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 25
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__info_inputs_update(self._handle)
    
    @info_inputs_update.setter
    def info_inputs_update(self, info_inputs_update):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__info_inputs_update(self._handle, \
            info_inputs_update)
    
    @property
    def info_newton(self):
        """
        Element info_newton ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 28
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__info_newton(self._handle)
    
    @info_newton.setter
    def info_newton(self, info_newton):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__info_newton(self._handle, \
            info_newton)
    
    @property
    def iformat(self):
        """
        Element iformat ftype=character(len=20) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 30
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__iformat(self._handle)
    
    @iformat.setter
    def iformat(self, iformat):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__iformat(self._handle, iformat)
    
    @property
    def rformat(self):
        """
        Element rformat ftype=character(len=20) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 32
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__rformat(self._handle)
    
    @rformat.setter
    def rformat(self, rformat):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__rformat(self._handle, rformat)
    
    @property
    def iformat_info(self):
        """
        Element iformat_info ftype=character(len=256) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 34
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__iformat_info(self._handle)
    
    @iformat_info.setter
    def iformat_info(self, iformat_info):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__iformat_info(self._handle, \
            iformat_info)
    
    @property
    def rformat_info(self):
        """
        Element rformat_info ftype=character(len=256) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 36
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__rformat_info(self._handle)
    
    @rformat_info.setter
    def rformat_info(self, rformat_info):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__rformat_info(self._handle, \
            rformat_info)
    
    @property
    def min_tdens(self):
        """
        Element min_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 41
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__min_tdens(self._handle)
    
    @min_tdens.setter
    def min_tdens(self, min_tdens):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__min_tdens(self._handle, \
            min_tdens)
    
    @property
    def threshold_tdens(self):
        """
        Element threshold_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 43
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__threshold_tdens(self._handle)
    
    @threshold_tdens.setter
    def threshold_tdens(self, threshold_tdens):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__threshold_tdens(self._handle, \
            threshold_tdens)
    
    @property
    def time_discretization_scheme(self):
        """
        Element time_discretization_scheme ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 52
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__time_discretization_scheme(self._handle)
    
    @time_discretization_scheme.setter
    def time_discretization_scheme(self, time_discretization_scheme):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__time_discretization_scheme(self._handle, \
            time_discretization_scheme)
    
    @property
    def tol_nonlinear(self):
        """
        Element tol_nonlinear ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 54
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tol_nonlinear(self._handle)
    
    @tol_nonlinear.setter
    def tol_nonlinear(self, tol_nonlinear):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tol_nonlinear(self._handle, \
            tol_nonlinear)
    
    @property
    def max_nonlinear_iterations(self):
        """
        Element max_nonlinear_iterations ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 56
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__max_nonlinear_iterations(self._handle)
    
    @max_nonlinear_iterations.setter
    def max_nonlinear_iterations(self, max_nonlinear_iterations):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__max_nonlinear_iterations(self._handle, \
            max_nonlinear_iterations)
    
    @property
    def max_restart_update(self):
        """
        Element max_restart_update ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 58
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__max_restart_update(self._handle)
    
    @max_restart_update.setter
    def max_restart_update(self, max_restart_update):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__max_restart_update(self._handle, \
            max_restart_update)
    
    @property
    def max_restart_invert_jacobian(self):
        """
        Element max_restart_invert_jacobian ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 60
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__max_restart_invert_jacobian(self._handle)
    
    @max_restart_invert_jacobian.setter
    def max_restart_invert_jacobian(self, max_restart_invert_jacobian):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__max_restart_invert_jacobian(self._handle, \
            max_restart_invert_jacobian)
    
    @property
    def tzero(self):
        """
        Element tzero ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 65
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tzero(self._handle)
    
    @tzero.setter
    def tzero(self, tzero):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tzero(self._handle, tzero)
    
    @property
    def max_time_iterations(self):
        """
        Element max_time_iterations ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 68
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__max_time_iterations(self._handle)
    
    @max_time_iterations.setter
    def max_time_iterations(self, max_time_iterations):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__max_time_iterations(self._handle, \
            max_time_iterations)
    
    @property
    def tolerance_system_variation(self):
        """
        Element tolerance_system_variation ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 71
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tolerance_system_variation(self._handle)
    
    @tolerance_system_variation.setter
    def tolerance_system_variation(self, tolerance_system_variation):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tolerance_system_variation(self._handle, \
            tolerance_system_variation)
    
    @property
    def deltat_control(self):
        """
        Element deltat_control ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 77
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__deltat_control(self._handle)
    
    @deltat_control.setter
    def deltat_control(self, deltat_control):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__deltat_control(self._handle, \
            deltat_control)
    
    @property
    def deltat(self):
        """
        Element deltat ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 79
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__deltat(self._handle)
    
    @deltat.setter
    def deltat(self, deltat):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__deltat(self._handle, deltat)
    
    @property
    def deltat_expansion_rate(self):
        """
        Element deltat_expansion_rate ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 81
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__deltat_expansion_rate(self._handle)
    
    @deltat_expansion_rate.setter
    def deltat_expansion_rate(self, deltat_expansion_rate):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__deltat_expansion_rate(self._handle, \
            deltat_expansion_rate)
    
    @property
    def deltat_upper_bound(self):
        """
        Element deltat_upper_bound ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 83
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__deltat_upper_bound(self._handle)
    
    @deltat_upper_bound.setter
    def deltat_upper_bound(self, deltat_upper_bound):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__deltat_upper_bound(self._handle, \
            deltat_upper_bound)
    
    @property
    def deltat_lower_bound(self):
        """
        Element deltat_lower_bound ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 85
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__deltat_lower_bound(self._handle)
    
    @deltat_lower_bound.setter
    def deltat_lower_bound(self, deltat_lower_bound):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__deltat_lower_bound(self._handle, \
            deltat_lower_bound)
    
    @property
    def krylov_scheme(self):
        """
        Element krylov_scheme ftype=character(len=20) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 94
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__krylov_scheme(self._handle)
    
    @krylov_scheme.setter
    def krylov_scheme(self, krylov_scheme):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__krylov_scheme(self._handle, \
            krylov_scheme)
    
    @property
    def linear_solver_lun_err(self):
        """
        Element linear_solver_lun_err ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 96
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__linear_solver_lun_err(self._handle)
    
    @linear_solver_lun_err.setter
    def linear_solver_lun_err(self, linear_solver_lun_err):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__linear_solver_lun_err(self._handle, \
            linear_solver_lun_err)
    
    @property
    def linear_solver_lun_out(self):
        """
        Element linear_solver_lun_out ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 98
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__linear_solver_lun_out(self._handle)
    
    @linear_solver_lun_out.setter
    def linear_solver_lun_out(self, linear_solver_lun_out):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__linear_solver_lun_out(self._handle, \
            linear_solver_lun_out)
    
    @property
    def iexit(self):
        """
        Element iexit ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 102
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__iexit(self._handle)
    
    @iexit.setter
    def iexit(self, iexit):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__iexit(self._handle, iexit)
    
    @property
    def imax(self):
        """
        Element imax ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 104
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__imax(self._handle)
    
    @imax.setter
    def imax(self, imax):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__imax(self._handle, imax)
    
    @property
    def iprt(self):
        """
        Element iprt ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 106
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__iprt(self._handle)
    
    @iprt.setter
    def iprt(self, iprt):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__iprt(self._handle, iprt)
    
    @property
    def isol(self):
        """
        Element isol ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 110
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__isol(self._handle)
    
    @isol.setter
    def isol(self, isol):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__isol(self._handle, isol)
    
    @property
    def tol_sol(self):
        """
        Element tol_sol ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 112
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tol_sol(self._handle)
    
    @tol_sol.setter
    def tol_sol(self, tol_sol):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tol_sol(self._handle, tol_sol)
    
    @property
    def iort(self):
        """
        Element iort ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 115
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__iort(self._handle)
    
    @iort.setter
    def iort(self, iort):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__iort(self._handle, iort)
    
    @property
    def nrestart(self):
        """
        Element nrestart ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 118
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__nrestart(self._handle)
    
    @nrestart.setter
    def nrestart(self, nrestart):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__nrestart(self._handle, nrestart)
    
    @property
    def debug_solver(self):
        """
        Element debug_solver ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 122
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__debug_solver(self._handle)
    
    @debug_solver.setter
    def debug_solver(self, debug_solver):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__debug_solver(self._handle, \
            debug_solver)
    
    @property
    def tolerance_linear_solver(self):
        """
        Element tolerance_linear_solver ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 124
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tolerance_linear_solver(self._handle)
    
    @tolerance_linear_solver.setter
    def tolerance_linear_solver(self, tolerance_linear_solver):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tolerance_linear_solver(self._handle, \
            tolerance_linear_solver)
    
    @property
    def id_diagonal_scaling(self):
        """
        Element id_diagonal_scaling ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 126
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_diagonal_scaling(self._handle)
    
    @id_diagonal_scaling.setter
    def id_diagonal_scaling(self, id_diagonal_scaling):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_diagonal_scaling(self._handle, \
            id_diagonal_scaling)
    
    @property
    def id_singular(self):
        """
        Element id_singular ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 128
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_singular(self._handle)
    
    @id_singular.setter
    def id_singular(self, id_singular):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_singular(self._handle, \
            id_singular)
    
    @property
    def lambda_(self):
        """
        Element lambda_ ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 130
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lambda_(self._handle)
    
    @lambda_.setter
    def lambda_(self, lambda_):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lambda_(self._handle, lambda_)
    
    @property
    def alpha_uzawa(self):
        """
        Element alpha_uzawa ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 132
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__alpha_uzawa(self._handle)
    
    @alpha_uzawa.setter
    def alpha_uzawa(self, alpha_uzawa):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__alpha_uzawa(self._handle, \
            alpha_uzawa)
    
    @property
    def omega_uzawa(self):
        """
        Element omega_uzawa ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 134
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__omega_uzawa(self._handle)
    
    @omega_uzawa.setter
    def omega_uzawa(self, omega_uzawa):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__omega_uzawa(self._handle, \
            omega_uzawa)
    
    @property
    def prec_type(self):
        """
        Element prec_type ftype=character(len=20) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 146
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__prec_type(self._handle)
    
    @prec_type.setter
    def prec_type(self, prec_type):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__prec_type(self._handle, \
            prec_type)
    
    @property
    def n_fillin(self):
        """
        Element n_fillin ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 149
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__n_fillin(self._handle)
    
    @n_fillin.setter
    def n_fillin(self, n_fillin):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__n_fillin(self._handle, n_fillin)
    
    @property
    def tol_fillin(self):
        """
        Element tol_fillin ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 152
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__tol_fillin(self._handle)
    
    @tol_fillin.setter
    def tol_fillin(self, tol_fillin):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__tol_fillin(self._handle, \
            tol_fillin)
    
    @property
    def factorization_job(self):
        """
        Element factorization_job ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 157
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__factorization_job(self._handle)
    
    @factorization_job.setter
    def factorization_job(self, factorization_job):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__factorization_job(self._handle, \
            factorization_job)
    
    @property
    def max_bfgs(self):
        """
        Element max_bfgs ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 159
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__max_bfgs(self._handle)
    
    @max_bfgs.setter
    def max_bfgs(self, max_bfgs):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__max_bfgs(self._handle, max_bfgs)
    
    @property
    def id_buffer_prec(self):
        """
        Element id_buffer_prec ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 168
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_buffer_prec(self._handle)
    
    @id_buffer_prec.setter
    def id_buffer_prec(self, id_buffer_prec):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_buffer_prec(self._handle, \
            id_buffer_prec)
    
    @property
    def ref_iter(self):
        """
        Element ref_iter ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 171
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__ref_iter(self._handle)
    
    @ref_iter.setter
    def ref_iter(self, ref_iter):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__ref_iter(self._handle, ref_iter)
    
    @property
    def prec_growth(self):
        """
        Element prec_growth ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 173
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__prec_growth(self._handle)
    
    @prec_growth.setter
    def prec_growth(self, prec_growth):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__prec_growth(self._handle, \
            prec_growth)
    
    @property
    def relax2prec(self):
        """
        Element relax2prec ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 176
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__relax2prec(self._handle)
    
    @relax2prec.setter
    def relax2prec(self, relax2prec):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__relax2prec(self._handle, \
            relax2prec)
    
    @property
    def gamma(self):
        """
        Element gamma ftype=real(kind=double) pytype=float
        
        
        Defined at 20modDmkControls.fpp line 181
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__gamma(self._handle)
    
    @gamma.setter
    def gamma(self, gamma):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__gamma(self._handle, gamma)
    
    @property
    def id_hats(self):
        """
        Element id_hats ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 183
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_hats(self._handle)
    
    @id_hats.setter
    def id_hats(self, id_hats):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_hats(self._handle, id_hats)
    
    @property
    def nbroyden(self):
        """
        Element nbroyden ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 188
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__nbroyden(self._handle)
    
    @nbroyden.setter
    def nbroyden(self, nbroyden):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__nbroyden(self._handle, nbroyden)
    
    @property
    def newton_method(self):
        """
        Element newton_method ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 192
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__newton_method(self._handle)
    
    @newton_method.setter
    def newton_method(self, newton_method):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__newton_method(self._handle, \
            newton_method)
    
    @property
    def reduced_jacobian(self):
        """
        Element reduced_jacobian ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 196
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__reduced_jacobian(self._handle)
    
    @reduced_jacobian.setter
    def reduced_jacobian(self, reduced_jacobian):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__reduced_jacobian(self._handle, \
            reduced_jacobian)
    
    @property
    def prec_newton(self):
        """
        Element prec_newton ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 206
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__prec_newton(self._handle)
    
    @prec_newton.setter
    def prec_newton(self, prec_newton):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__prec_newton(self._handle, \
            prec_newton)
    
    @property
    def inexact_newton(self):
        """
        Element inexact_newton ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 210
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__inexact_newton(self._handle)
    
    @inexact_newton.setter
    def inexact_newton(self, inexact_newton):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__inexact_newton(self._handle, \
            inexact_newton)
    
    @property
    def id_save_dat(self):
        """
        Element id_save_dat ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 220
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_save_dat(self._handle)
    
    @id_save_dat.setter
    def id_save_dat(self, id_save_dat):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_save_dat(self._handle, \
            id_save_dat)
    
    @property
    def lun_tdens(self):
        """
        Element lun_tdens ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 223
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lun_tdens(self._handle)
    
    @lun_tdens.setter
    def lun_tdens(self, lun_tdens):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lun_tdens(self._handle, \
            lun_tdens)
    
    @property
    def fn_tdens(self):
        """
        Element fn_tdens ftype=character(1024) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 225
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__fn_tdens(self._handle)
    
    @fn_tdens.setter
    def fn_tdens(self, fn_tdens):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__fn_tdens(self._handle, fn_tdens)
    
    @property
    def lun_pot(self):
        """
        Element lun_pot ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 228
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lun_pot(self._handle)
    
    @lun_pot.setter
    def lun_pot(self, lun_pot):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lun_pot(self._handle, lun_pot)
    
    @property
    def fn_pot(self):
        """
        Element fn_pot ftype=character(1024) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 230
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__fn_pot(self._handle)
    
    @fn_pot.setter
    def fn_pot(self, fn_pot):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__fn_pot(self._handle, fn_pot)
    
    @property
    def freq_dat(self):
        """
        Element freq_dat ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 232
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__freq_dat(self._handle)
    
    @freq_dat.setter
    def freq_dat(self, freq_dat):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__freq_dat(self._handle, freq_dat)
    
    @property
    def id_save_matrix(self):
        """
        Element id_save_matrix ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 234
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_save_matrix(self._handle)
    
    @id_save_matrix.setter
    def id_save_matrix(self, id_save_matrix):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_save_matrix(self._handle, \
            id_save_matrix)
    
    @property
    def freq_matrix(self):
        """
        Element freq_matrix ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 236
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__freq_matrix(self._handle)
    
    @freq_matrix.setter
    def freq_matrix(self, freq_matrix):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__freq_matrix(self._handle, \
            freq_matrix)
    
    @property
    def info_time_evolution(self):
        """
        Element info_time_evolution ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 241
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__info_time_evolution(self._handle)
    
    @info_time_evolution.setter
    def info_time_evolution(self, info_time_evolution):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__info_time_evolution(self._handle, \
            info_time_evolution)
    
    @property
    def info_functional(self):
        """
        Element info_functional ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 243
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__info_functional(self._handle)
    
    @info_functional.setter
    def info_functional(self, info_functional):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__info_functional(self._handle, \
            info_functional)
    
    @property
    def lun_err(self):
        """
        Element lun_err ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 245
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lun_err(self._handle)
    
    @lun_err.setter
    def lun_err(self, lun_err):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lun_err(self._handle, lun_err)
    
    @property
    def lun_out(self):
        """
        Element lun_out ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 247
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lun_out(self._handle)
    
    @lun_out.setter
    def lun_out(self, lun_out):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lun_out(self._handle, lun_out)
    
    @property
    def lun_statistics(self):
        """
        Element lun_statistics ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 249
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__lun_statistics(self._handle)
    
    @lun_statistics.setter
    def lun_statistics(self, lun_statistics):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__lun_statistics(self._handle, \
            lun_statistics)
    
    @property
    def fn_statistics(self):
        """
        Element fn_statistics ftype=character(1024) pytype=str
        
        
        Defined at 20modDmkControls.fpp line 251
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__fn_statistics(self._handle)
    
    @fn_statistics.setter
    def fn_statistics(self, fn_statistics):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__fn_statistics(self._handle, \
            fn_statistics)
    
    @property
    def id_subgrid(self):
        """
        Element id_subgrid ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 256
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__id_subgrid(self._handle)
    
    @id_subgrid.setter
    def id_subgrid(self, id_subgrid):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__id_subgrid(self._handle, \
            id_subgrid)
    
    @property
    def preassembly_matrices(self):
        """
        Element preassembly_matrices ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 260
        
        """
        return \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__preassembly_matrices(self._handle)
    
    @preassembly_matrices.setter
    def preassembly_matrices(self, preassembly_matrices):
        \
            _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__preassembly_matrices(self._handle, \
            preassembly_matrices)
    
    @property
    def ndeg(self):
        """
        Element ndeg ftype=integer  pytype=int
        
        
        Defined at 20modDmkControls.fpp line 262
        
        """
        return _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__get__ndeg(self._handle)
    
    @ndeg.setter
    def ndeg(self, ndeg):
        _ExampleDerivedTypes_pkg.f90wrap_dmkctrl__set__ndeg(self._handle, ndeg)
    
    def __str__(self):
        ret = ['<dmkctrl>{\n']
        ret.append('    debug : ')
        ret.append(repr(self.debug))
        ret.append(',\n    info_state : ')
        ret.append(repr(self.info_state))
        ret.append(',\n    info_inputs_update : ')
        ret.append(repr(self.info_inputs_update))
        ret.append(',\n    info_newton : ')
        ret.append(repr(self.info_newton))
        ret.append(',\n    iformat : ')
        ret.append(repr(self.iformat))
        ret.append(',\n    rformat : ')
        ret.append(repr(self.rformat))
        ret.append(',\n    iformat_info : ')
        ret.append(repr(self.iformat_info))
        ret.append(',\n    rformat_info : ')
        ret.append(repr(self.rformat_info))
        ret.append(',\n    min_tdens : ')
        ret.append(repr(self.min_tdens))
        ret.append(',\n    threshold_tdens : ')
        ret.append(repr(self.threshold_tdens))
        ret.append(',\n    time_discretization_scheme : ')
        ret.append(repr(self.time_discretization_scheme))
        ret.append(',\n    tol_nonlinear : ')
        ret.append(repr(self.tol_nonlinear))
        ret.append(',\n    max_nonlinear_iterations : ')
        ret.append(repr(self.max_nonlinear_iterations))
        ret.append(',\n    max_restart_update : ')
        ret.append(repr(self.max_restart_update))
        ret.append(',\n    max_restart_invert_jacobian : ')
        ret.append(repr(self.max_restart_invert_jacobian))
        ret.append(',\n    tzero : ')
        ret.append(repr(self.tzero))
        ret.append(',\n    max_time_iterations : ')
        ret.append(repr(self.max_time_iterations))
        ret.append(',\n    tolerance_system_variation : ')
        ret.append(repr(self.tolerance_system_variation))
        ret.append(',\n    deltat_control : ')
        ret.append(repr(self.deltat_control))
        ret.append(',\n    deltat : ')
        ret.append(repr(self.deltat))
        ret.append(',\n    deltat_expansion_rate : ')
        ret.append(repr(self.deltat_expansion_rate))
        ret.append(',\n    deltat_upper_bound : ')
        ret.append(repr(self.deltat_upper_bound))
        ret.append(',\n    deltat_lower_bound : ')
        ret.append(repr(self.deltat_lower_bound))
        ret.append(',\n    krylov_scheme : ')
        ret.append(repr(self.krylov_scheme))
        ret.append(',\n    linear_solver_lun_err : ')
        ret.append(repr(self.linear_solver_lun_err))
        ret.append(',\n    linear_solver_lun_out : ')
        ret.append(repr(self.linear_solver_lun_out))
        ret.append(',\n    iexit : ')
        ret.append(repr(self.iexit))
        ret.append(',\n    imax : ')
        ret.append(repr(self.imax))
        ret.append(',\n    iprt : ')
        ret.append(repr(self.iprt))
        ret.append(',\n    isol : ')
        ret.append(repr(self.isol))
        ret.append(',\n    tol_sol : ')
        ret.append(repr(self.tol_sol))
        ret.append(',\n    iort : ')
        ret.append(repr(self.iort))
        ret.append(',\n    nrestart : ')
        ret.append(repr(self.nrestart))
        ret.append(',\n    debug_solver : ')
        ret.append(repr(self.debug_solver))
        ret.append(',\n    tolerance_linear_solver : ')
        ret.append(repr(self.tolerance_linear_solver))
        ret.append(',\n    id_diagonal_scaling : ')
        ret.append(repr(self.id_diagonal_scaling))
        ret.append(',\n    id_singular : ')
        ret.append(repr(self.id_singular))
        ret.append(',\n    lambda_ : ')
        ret.append(repr(self.lambda_))
        ret.append(',\n    alpha_uzawa : ')
        ret.append(repr(self.alpha_uzawa))
        ret.append(',\n    omega_uzawa : ')
        ret.append(repr(self.omega_uzawa))
        ret.append(',\n    prec_type : ')
        ret.append(repr(self.prec_type))
        ret.append(',\n    n_fillin : ')
        ret.append(repr(self.n_fillin))
        ret.append(',\n    tol_fillin : ')
        ret.append(repr(self.tol_fillin))
        ret.append(',\n    factorization_job : ')
        ret.append(repr(self.factorization_job))
        ret.append(',\n    max_bfgs : ')
        ret.append(repr(self.max_bfgs))
        ret.append(',\n    id_buffer_prec : ')
        ret.append(repr(self.id_buffer_prec))
        ret.append(',\n    ref_iter : ')
        ret.append(repr(self.ref_iter))
        ret.append(',\n    prec_growth : ')
        ret.append(repr(self.prec_growth))
        ret.append(',\n    relax2prec : ')
        ret.append(repr(self.relax2prec))
        ret.append(',\n    gamma : ')
        ret.append(repr(self.gamma))
        ret.append(',\n    id_hats : ')
        ret.append(repr(self.id_hats))
        ret.append(',\n    nbroyden : ')
        ret.append(repr(self.nbroyden))
        ret.append(',\n    newton_method : ')
        ret.append(repr(self.newton_method))
        ret.append(',\n    reduced_jacobian : ')
        ret.append(repr(self.reduced_jacobian))
        ret.append(',\n    prec_newton : ')
        ret.append(repr(self.prec_newton))
        ret.append(',\n    inexact_newton : ')
        ret.append(repr(self.inexact_newton))
        ret.append(',\n    id_save_dat : ')
        ret.append(repr(self.id_save_dat))
        ret.append(',\n    lun_tdens : ')
        ret.append(repr(self.lun_tdens))
        ret.append(',\n    fn_tdens : ')
        ret.append(repr(self.fn_tdens))
        ret.append(',\n    lun_pot : ')
        ret.append(repr(self.lun_pot))
        ret.append(',\n    fn_pot : ')
        ret.append(repr(self.fn_pot))
        ret.append(',\n    freq_dat : ')
        ret.append(repr(self.freq_dat))
        ret.append(',\n    id_save_matrix : ')
        ret.append(repr(self.id_save_matrix))
        ret.append(',\n    freq_matrix : ')
        ret.append(repr(self.freq_matrix))
        ret.append(',\n    info_time_evolution : ')
        ret.append(repr(self.info_time_evolution))
        ret.append(',\n    info_functional : ')
        ret.append(repr(self.info_functional))
        ret.append(',\n    lun_err : ')
        ret.append(repr(self.lun_err))
        ret.append(',\n    lun_out : ')
        ret.append(repr(self.lun_out))
        ret.append(',\n    lun_statistics : ')
        ret.append(repr(self.lun_statistics))
        ret.append(',\n    fn_statistics : ')
        ret.append(repr(self.fn_statistics))
        ret.append(',\n    id_subgrid : ')
        ret.append(repr(self.id_subgrid))
        ret.append(',\n    preassembly_matrices : ')
        ret.append(repr(self.preassembly_matrices))
        ret.append(',\n    ndeg : ')
        ret.append(repr(self.ndeg))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "dmkcontrols".')

for func in _dt_array_initialisers:
    func()
