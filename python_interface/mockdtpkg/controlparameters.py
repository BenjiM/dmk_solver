"""
Module controlparameters


Defined at 89modControlParameters.fpp lines 6-231

"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("mockdtpkg.CtrlPrm")
class CtrlPrm(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=ctrlprm)
    
    
    Defined at 89modControlParameters.fpp lines 10-231
    
    """
    def __init__(self, handle=None):
        """
        self = Ctrlprm()
        
        
        Defined at 89modControlParameters.fpp lines 10-231
        
        
        Returns
        -------
        this : Ctrlprm
        	Object to be constructed
        
        
        Automatically generated constructor for ctrlprm
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_ctrlprm_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Ctrlprm
        
        
        Defined at 89modControlParameters.fpp lines 10-231
        
        Parameters
        ----------
        this : Ctrlprm
        	Object to be destructed
        
        
        Automatically generated destructor for ctrlprm
        """
        if self._alloc:
            _mockdtpkg.f90wrap_ctrlprm_finalise(this=self._handle)
    
    @property
    def debug(self):
        """
        Element debug ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 15
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__debug(self._handle)
    
    @debug.setter
    def debug(self, debug):
        _mockdtpkg.f90wrap_ctrlprm__set__debug(self._handle, debug)
    
    @property
    def info_state(self):
        """
        Element info_state ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 18
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__info_state(self._handle)
    
    @info_state.setter
    def info_state(self, info_state):
        _mockdtpkg.f90wrap_ctrlprm__set__info_state(self._handle, info_state)
    
    @property
    def info_inputs_update(self):
        """
        Element info_inputs_update ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 21
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__info_inputs_update(self._handle)
    
    @info_inputs_update.setter
    def info_inputs_update(self, info_inputs_update):
        _mockdtpkg.f90wrap_ctrlprm__set__info_inputs_update(self._handle, \
            info_inputs_update)
    
    @property
    def info_newton(self):
        """
        Element info_newton ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 24
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__info_newton(self._handle)
    
    @info_newton.setter
    def info_newton(self, info_newton):
        _mockdtpkg.f90wrap_ctrlprm__set__info_newton(self._handle, info_newton)
    
    @property
    def iformat(self):
        """
        Element iformat ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 26
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__iformat(self._handle)
    
    @iformat.setter
    def iformat(self, iformat):
        _mockdtpkg.f90wrap_ctrlprm__set__iformat(self._handle, iformat)
    
    @property
    def rformat(self):
        """
        Element rformat ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 28
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__rformat(self._handle)
    
    @rformat.setter
    def rformat(self, rformat):
        _mockdtpkg.f90wrap_ctrlprm__set__rformat(self._handle, rformat)
    
    @property
    def iformat_info(self):
        """
        Element iformat_info ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 30
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__iformat_info(self._handle)
    
    @iformat_info.setter
    def iformat_info(self, iformat_info):
        _mockdtpkg.f90wrap_ctrlprm__set__iformat_info(self._handle, iformat_info)
    
    @property
    def rformat_info(self):
        """
        Element rformat_info ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 32
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__rformat_info(self._handle)
    
    @rformat_info.setter
    def rformat_info(self, rformat_info):
        _mockdtpkg.f90wrap_ctrlprm__set__rformat_info(self._handle, rformat_info)
    
    @property
    def id_subgrid(self):
        """
        Element id_subgrid ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 37
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_subgrid(self._handle)
    
    @id_subgrid.setter
    def id_subgrid(self, id_subgrid):
        _mockdtpkg.f90wrap_ctrlprm__set__id_subgrid(self._handle, id_subgrid)
    
    @property
    def id_ode(self):
        """
        Element id_ode ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 41
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_ode(self._handle)
    
    @id_ode.setter
    def id_ode(self, id_ode):
        _mockdtpkg.f90wrap_ctrlprm__set__id_ode(self._handle, id_ode)
    
    @property
    def min_tdens(self):
        """
        Element min_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 43
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__min_tdens(self._handle)
    
    @min_tdens.setter
    def min_tdens(self, min_tdens):
        _mockdtpkg.f90wrap_ctrlprm__set__min_tdens(self._handle, min_tdens)
    
    @property
    def threshold_tdens(self):
        """
        Element threshold_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 45
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__threshold_tdens(self._handle)
    
    @threshold_tdens.setter
    def threshold_tdens(self, threshold_tdens):
        _mockdtpkg.f90wrap_ctrlprm__set__threshold_tdens(self._handle, threshold_tdens)
    
    @property
    def id_time_discr(self):
        """
        Element id_time_discr ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 54
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_time_discr(self._handle)
    
    @id_time_discr.setter
    def id_time_discr(self, id_time_discr):
        _mockdtpkg.f90wrap_ctrlprm__set__id_time_discr(self._handle, id_time_discr)
    
    @property
    def tol_nonlinear(self):
        """
        Element tol_nonlinear ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 56
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__tol_nonlinear(self._handle)
    
    @tol_nonlinear.setter
    def tol_nonlinear(self, tol_nonlinear):
        _mockdtpkg.f90wrap_ctrlprm__set__tol_nonlinear(self._handle, tol_nonlinear)
    
    @property
    def max_iteration_nonlinear(self):
        """
        Element max_iteration_nonlinear ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 58
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__max_iteration_nonlinear(self._handle)
    
    @max_iteration_nonlinear.setter
    def max_iteration_nonlinear(self, max_iteration_nonlinear):
        _mockdtpkg.f90wrap_ctrlprm__set__max_iteration_nonlinear(self._handle, \
            max_iteration_nonlinear)
    
    @property
    def nrestart_max(self):
        """
        Element nrestart_max ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 60
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__nrestart_max(self._handle)
    
    @nrestart_max.setter
    def nrestart_max(self, nrestart_max):
        _mockdtpkg.f90wrap_ctrlprm__set__nrestart_max(self._handle, nrestart_max)
    
    @property
    def max_nrestart_invert_jacobian(self):
        """
        Element max_nrestart_invert_jacobian ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 62
        
        """
        return \
            _mockdtpkg.f90wrap_ctrlprm__get__max_nrestart_invert_jacobian(self._handle)
    
    @max_nrestart_invert_jacobian.setter
    def max_nrestart_invert_jacobian(self, max_nrestart_invert_jacobian):
        _mockdtpkg.f90wrap_ctrlprm__set__max_nrestart_invert_jacobian(self._handle, \
            max_nrestart_invert_jacobian)
    
    @property
    def tzero(self):
        """
        Element tzero ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 67
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__tzero(self._handle)
    
    @tzero.setter
    def tzero(self, tzero):
        _mockdtpkg.f90wrap_ctrlprm__set__tzero(self._handle, tzero)
    
    @property
    def max_time_it(self):
        """
        Element max_time_it ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 70
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__max_time_it(self._handle)
    
    @max_time_it.setter
    def max_time_it(self, max_time_it):
        _mockdtpkg.f90wrap_ctrlprm__set__max_time_it(self._handle, max_time_it)
    
    @property
    def tol_var_tdens(self):
        """
        Element tol_var_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 73
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__tol_var_tdens(self._handle)
    
    @tol_var_tdens.setter
    def tol_var_tdens(self, tol_var_tdens):
        _mockdtpkg.f90wrap_ctrlprm__set__tol_var_tdens(self._handle, tol_var_tdens)
    
    @property
    def id_time_ctrl(self):
        """
        Element id_time_ctrl ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 79
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_time_ctrl(self._handle)
    
    @id_time_ctrl.setter
    def id_time_ctrl(self, id_time_ctrl):
        _mockdtpkg.f90wrap_ctrlprm__set__id_time_ctrl(self._handle, id_time_ctrl)
    
    @property
    def deltat(self):
        """
        Element deltat ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 81
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__deltat(self._handle)
    
    @deltat.setter
    def deltat(self, deltat):
        _mockdtpkg.f90wrap_ctrlprm__set__deltat(self._handle, deltat)
    
    @property
    def exp_rate(self):
        """
        Element exp_rate ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 83
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__exp_rate(self._handle)
    
    @exp_rate.setter
    def exp_rate(self, exp_rate):
        _mockdtpkg.f90wrap_ctrlprm__set__exp_rate(self._handle, exp_rate)
    
    @property
    def upper_bound_deltat(self):
        """
        Element upper_bound_deltat ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 85
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__upper_bound_deltat(self._handle)
    
    @upper_bound_deltat.setter
    def upper_bound_deltat(self, upper_bound_deltat):
        _mockdtpkg.f90wrap_ctrlprm__set__upper_bound_deltat(self._handle, \
            upper_bound_deltat)
    
    @property
    def lower_bound_deltat(self):
        """
        Element lower_bound_deltat ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 87
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__lower_bound_deltat(self._handle)
    
    @lower_bound_deltat.setter
    def lower_bound_deltat(self, lower_bound_deltat):
        _mockdtpkg.f90wrap_ctrlprm__set__lower_bound_deltat(self._handle, \
            lower_bound_deltat)
    
    @property
    def krylov_scheme(self):
        """
        Element krylov_scheme ftype=character(len=20) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 96
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__krylov_scheme(self._handle)
    
    @krylov_scheme.setter
    def krylov_scheme(self, krylov_scheme):
        _mockdtpkg.f90wrap_ctrlprm__set__krylov_scheme(self._handle, krylov_scheme)
    
    @property
    def lun_err(self):
        """
        Element lun_err ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 98
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__lun_err(self._handle)
    
    @lun_err.setter
    def lun_err(self, lun_err):
        _mockdtpkg.f90wrap_ctrlprm__set__lun_err(self._handle, lun_err)
    
    @property
    def lun_out(self):
        """
        Element lun_out ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 100
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__lun_out(self._handle)
    
    @lun_out.setter
    def lun_out(self, lun_out):
        _mockdtpkg.f90wrap_ctrlprm__set__lun_out(self._handle, lun_out)
    
    @property
    def iexit(self):
        """
        Element iexit ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 104
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__iexit(self._handle)
    
    @iexit.setter
    def iexit(self, iexit):
        _mockdtpkg.f90wrap_ctrlprm__set__iexit(self._handle, iexit)
    
    @property
    def imax(self):
        """
        Element imax ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 106
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__imax(self._handle)
    
    @imax.setter
    def imax(self, imax):
        _mockdtpkg.f90wrap_ctrlprm__set__imax(self._handle, imax)
    
    @property
    def iprt(self):
        """
        Element iprt ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 108
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__iprt(self._handle)
    
    @iprt.setter
    def iprt(self, iprt):
        _mockdtpkg.f90wrap_ctrlprm__set__iprt(self._handle, iprt)
    
    @property
    def isol(self):
        """
        Element isol ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 112
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__isol(self._handle)
    
    @isol.setter
    def isol(self, isol):
        _mockdtpkg.f90wrap_ctrlprm__set__isol(self._handle, isol)
    
    @property
    def tol_sol(self):
        """
        Element tol_sol ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 114
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__tol_sol(self._handle)
    
    @tol_sol.setter
    def tol_sol(self, tol_sol):
        _mockdtpkg.f90wrap_ctrlprm__set__tol_sol(self._handle, tol_sol)
    
    @property
    def iort(self):
        """
        Element iort ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 117
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__iort(self._handle)
    
    @iort.setter
    def iort(self, iort):
        _mockdtpkg.f90wrap_ctrlprm__set__iort(self._handle, iort)
    
    @property
    def nrestart(self):
        """
        Element nrestart ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 120
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__nrestart(self._handle)
    
    @nrestart.setter
    def nrestart(self, nrestart):
        _mockdtpkg.f90wrap_ctrlprm__set__nrestart(self._handle, nrestart)
    
    @property
    def debug_solver(self):
        """
        Element debug_solver ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 124
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__debug_solver(self._handle)
    
    @debug_solver.setter
    def debug_solver(self, debug_solver):
        _mockdtpkg.f90wrap_ctrlprm__set__debug_solver(self._handle, debug_solver)
    
    @property
    def solver_err(self):
        """
        Element solver_err ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 127
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__solver_err(self._handle)
    
    @solver_err.setter
    def solver_err(self, solver_err):
        _mockdtpkg.f90wrap_ctrlprm__set__solver_err(self._handle, solver_err)
    
    @property
    def solver_out(self):
        """
        Element solver_out ftype=character(len=256) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 130
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__solver_out(self._handle)
    
    @solver_out.setter
    def solver_out(self, solver_out):
        _mockdtpkg.f90wrap_ctrlprm__set__solver_out(self._handle, solver_out)
    
    @property
    def id_diagscale(self):
        """
        Element id_diagscale ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 132
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_diagscale(self._handle)
    
    @id_diagscale.setter
    def id_diagscale(self, id_diagscale):
        _mockdtpkg.f90wrap_ctrlprm__set__id_diagscale(self._handle, id_diagscale)
    
    @property
    def lambda_(self):
        """
        Element lambda_ ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 134
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__lambda_(self._handle)
    
    @lambda_.setter
    def lambda_(self, lambda_):
        _mockdtpkg.f90wrap_ctrlprm__set__lambda_(self._handle, lambda_)
    
    @property
    def alpha_uzawa(self):
        """
        Element alpha_uzawa ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 136
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__alpha_uzawa(self._handle)
    
    @alpha_uzawa.setter
    def alpha_uzawa(self, alpha_uzawa):
        _mockdtpkg.f90wrap_ctrlprm__set__alpha_uzawa(self._handle, alpha_uzawa)
    
    @property
    def omega_uzawa(self):
        """
        Element omega_uzawa ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 138
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__omega_uzawa(self._handle)
    
    @omega_uzawa.setter
    def omega_uzawa(self, omega_uzawa):
        _mockdtpkg.f90wrap_ctrlprm__set__omega_uzawa(self._handle, omega_uzawa)
    
    @property
    def prec_type(self):
        """
        Element prec_type ftype=character(len=20) pytype=str
        
        
        Defined at 89modControlParameters.fpp line 150
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__prec_type(self._handle)
    
    @prec_type.setter
    def prec_type(self, prec_type):
        _mockdtpkg.f90wrap_ctrlprm__set__prec_type(self._handle, prec_type)
    
    @property
    def n_fillin(self):
        """
        Element n_fillin ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 153
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__n_fillin(self._handle)
    
    @n_fillin.setter
    def n_fillin(self, n_fillin):
        _mockdtpkg.f90wrap_ctrlprm__set__n_fillin(self._handle, n_fillin)
    
    @property
    def tol_fillin(self):
        """
        Element tol_fillin ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 156
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__tol_fillin(self._handle)
    
    @tol_fillin.setter
    def tol_fillin(self, tol_fillin):
        _mockdtpkg.f90wrap_ctrlprm__set__tol_fillin(self._handle, tol_fillin)
    
    @property
    def factorization_job(self):
        """
        Element factorization_job ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 161
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__factorization_job(self._handle)
    
    @factorization_job.setter
    def factorization_job(self, factorization_job):
        _mockdtpkg.f90wrap_ctrlprm__set__factorization_job(self._handle, \
            factorization_job)
    
    @property
    def max_bfgs(self):
        """
        Element max_bfgs ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 163
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__max_bfgs(self._handle)
    
    @max_bfgs.setter
    def max_bfgs(self, max_bfgs):
        _mockdtpkg.f90wrap_ctrlprm__set__max_bfgs(self._handle, max_bfgs)
    
    @property
    def id_buffer_prec(self):
        """
        Element id_buffer_prec ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 172
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_buffer_prec(self._handle)
    
    @id_buffer_prec.setter
    def id_buffer_prec(self, id_buffer_prec):
        _mockdtpkg.f90wrap_ctrlprm__set__id_buffer_prec(self._handle, id_buffer_prec)
    
    @property
    def ref_iter(self):
        """
        Element ref_iter ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 175
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__ref_iter(self._handle)
    
    @ref_iter.setter
    def ref_iter(self, ref_iter):
        _mockdtpkg.f90wrap_ctrlprm__set__ref_iter(self._handle, ref_iter)
    
    @property
    def prec_growth(self):
        """
        Element prec_growth ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 177
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__prec_growth(self._handle)
    
    @prec_growth.setter
    def prec_growth(self, prec_growth):
        _mockdtpkg.f90wrap_ctrlprm__set__prec_growth(self._handle, prec_growth)
    
    @property
    def relax2prec(self):
        """
        Element relax2prec ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 180
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__relax2prec(self._handle)
    
    @relax2prec.setter
    def relax2prec(self, relax2prec):
        _mockdtpkg.f90wrap_ctrlprm__set__relax2prec(self._handle, relax2prec)
    
    @property
    def gamma(self):
        """
        Element gamma ftype=real(kind=double) pytype=float
        
        
        Defined at 89modControlParameters.fpp line 185
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__gamma(self._handle)
    
    @gamma.setter
    def gamma(self, gamma):
        _mockdtpkg.f90wrap_ctrlprm__set__gamma(self._handle, gamma)
    
    @property
    def id_hats(self):
        """
        Element id_hats ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 187
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_hats(self._handle)
    
    @id_hats.setter
    def id_hats(self, id_hats):
        _mockdtpkg.f90wrap_ctrlprm__set__id_hats(self._handle, id_hats)
    
    @property
    def nbroyden(self):
        """
        Element nbroyden ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 192
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__nbroyden(self._handle)
    
    @nbroyden.setter
    def nbroyden(self, nbroyden):
        _mockdtpkg.f90wrap_ctrlprm__set__nbroyden(self._handle, nbroyden)
    
    @property
    def newton_method(self):
        """
        Element newton_method ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 196
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__newton_method(self._handle)
    
    @newton_method.setter
    def newton_method(self, newton_method):
        _mockdtpkg.f90wrap_ctrlprm__set__newton_method(self._handle, newton_method)
    
    @property
    def reduced_jacobian(self):
        """
        Element reduced_jacobian ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 200
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__reduced_jacobian(self._handle)
    
    @reduced_jacobian.setter
    def reduced_jacobian(self, reduced_jacobian):
        _mockdtpkg.f90wrap_ctrlprm__set__reduced_jacobian(self._handle, \
            reduced_jacobian)
    
    @property
    def prec_newton(self):
        """
        Element prec_newton ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 210
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__prec_newton(self._handle)
    
    @prec_newton.setter
    def prec_newton(self, prec_newton):
        _mockdtpkg.f90wrap_ctrlprm__set__prec_newton(self._handle, prec_newton)
    
    @property
    def inexact_newton(self):
        """
        Element inexact_newton ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 214
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__inexact_newton(self._handle)
    
    @inexact_newton.setter
    def inexact_newton(self, inexact_newton):
        _mockdtpkg.f90wrap_ctrlprm__set__inexact_newton(self._handle, inexact_newton)
    
    @property
    def id_save_dat(self):
        """
        Element id_save_dat ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 224
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_save_dat(self._handle)
    
    @id_save_dat.setter
    def id_save_dat(self, id_save_dat):
        _mockdtpkg.f90wrap_ctrlprm__set__id_save_dat(self._handle, id_save_dat)
    
    @property
    def freq_dat(self):
        """
        Element freq_dat ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 226
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__freq_dat(self._handle)
    
    @freq_dat.setter
    def freq_dat(self, freq_dat):
        _mockdtpkg.f90wrap_ctrlprm__set__freq_dat(self._handle, freq_dat)
    
    @property
    def id_save_matrix(self):
        """
        Element id_save_matrix ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 228
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__id_save_matrix(self._handle)
    
    @id_save_matrix.setter
    def id_save_matrix(self, id_save_matrix):
        _mockdtpkg.f90wrap_ctrlprm__set__id_save_matrix(self._handle, id_save_matrix)
    
    @property
    def freq_matrix(self):
        """
        Element freq_matrix ftype=integer  pytype=int
        
        
        Defined at 89modControlParameters.fpp line 230
        
        """
        return _mockdtpkg.f90wrap_ctrlprm__get__freq_matrix(self._handle)
    
    @freq_matrix.setter
    def freq_matrix(self, freq_matrix):
        _mockdtpkg.f90wrap_ctrlprm__set__freq_matrix(self._handle, freq_matrix)
    
    def __str__(self):
        ret = ['<ctrlprm>{\n']
        ret.append('    debug : ')
        ret.append(repr(self.debug))
        ret.append(',\n    info_state : ')
        ret.append(repr(self.info_state))
        ret.append(',\n    info_inputs_update : ')
        ret.append(repr(self.info_inputs_update))
        ret.append(',\n    info_newton : ')
        ret.append(repr(self.info_newton))
        ret.append(',\n    iformat : ')
        ret.append(repr(self.iformat))
        ret.append(',\n    rformat : ')
        ret.append(repr(self.rformat))
        ret.append(',\n    iformat_info : ')
        ret.append(repr(self.iformat_info))
        ret.append(',\n    rformat_info : ')
        ret.append(repr(self.rformat_info))
        ret.append(',\n    id_subgrid : ')
        ret.append(repr(self.id_subgrid))
        ret.append(',\n    id_ode : ')
        ret.append(repr(self.id_ode))
        ret.append(',\n    min_tdens : ')
        ret.append(repr(self.min_tdens))
        ret.append(',\n    threshold_tdens : ')
        ret.append(repr(self.threshold_tdens))
        ret.append(',\n    id_time_discr : ')
        ret.append(repr(self.id_time_discr))
        ret.append(',\n    tol_nonlinear : ')
        ret.append(repr(self.tol_nonlinear))
        ret.append(',\n    max_iteration_nonlinear : ')
        ret.append(repr(self.max_iteration_nonlinear))
        ret.append(',\n    nrestart_max : ')
        ret.append(repr(self.nrestart_max))
        ret.append(',\n    max_nrestart_invert_jacobian : ')
        ret.append(repr(self.max_nrestart_invert_jacobian))
        ret.append(',\n    tzero : ')
        ret.append(repr(self.tzero))
        ret.append(',\n    max_time_it : ')
        ret.append(repr(self.max_time_it))
        ret.append(',\n    tol_var_tdens : ')
        ret.append(repr(self.tol_var_tdens))
        ret.append(',\n    id_time_ctrl : ')
        ret.append(repr(self.id_time_ctrl))
        ret.append(',\n    deltat : ')
        ret.append(repr(self.deltat))
        ret.append(',\n    exp_rate : ')
        ret.append(repr(self.exp_rate))
        ret.append(',\n    upper_bound_deltat : ')
        ret.append(repr(self.upper_bound_deltat))
        ret.append(',\n    lower_bound_deltat : ')
        ret.append(repr(self.lower_bound_deltat))
        ret.append(',\n    krylov_scheme : ')
        ret.append(repr(self.krylov_scheme))
        ret.append(',\n    lun_err : ')
        ret.append(repr(self.lun_err))
        ret.append(',\n    lun_out : ')
        ret.append(repr(self.lun_out))
        ret.append(',\n    iexit : ')
        ret.append(repr(self.iexit))
        ret.append(',\n    imax : ')
        ret.append(repr(self.imax))
        ret.append(',\n    iprt : ')
        ret.append(repr(self.iprt))
        ret.append(',\n    isol : ')
        ret.append(repr(self.isol))
        ret.append(',\n    tol_sol : ')
        ret.append(repr(self.tol_sol))
        ret.append(',\n    iort : ')
        ret.append(repr(self.iort))
        ret.append(',\n    nrestart : ')
        ret.append(repr(self.nrestart))
        ret.append(',\n    debug_solver : ')
        ret.append(repr(self.debug_solver))
        ret.append(',\n    solver_err : ')
        ret.append(repr(self.solver_err))
        ret.append(',\n    solver_out : ')
        ret.append(repr(self.solver_out))
        ret.append(',\n    id_diagscale : ')
        ret.append(repr(self.id_diagscale))
        ret.append(',\n    lambda_ : ')
        ret.append(repr(self.lambda_))
        ret.append(',\n    alpha_uzawa : ')
        ret.append(repr(self.alpha_uzawa))
        ret.append(',\n    omega_uzawa : ')
        ret.append(repr(self.omega_uzawa))
        ret.append(',\n    prec_type : ')
        ret.append(repr(self.prec_type))
        ret.append(',\n    n_fillin : ')
        ret.append(repr(self.n_fillin))
        ret.append(',\n    tol_fillin : ')
        ret.append(repr(self.tol_fillin))
        ret.append(',\n    factorization_job : ')
        ret.append(repr(self.factorization_job))
        ret.append(',\n    max_bfgs : ')
        ret.append(repr(self.max_bfgs))
        ret.append(',\n    id_buffer_prec : ')
        ret.append(repr(self.id_buffer_prec))
        ret.append(',\n    ref_iter : ')
        ret.append(repr(self.ref_iter))
        ret.append(',\n    prec_growth : ')
        ret.append(repr(self.prec_growth))
        ret.append(',\n    relax2prec : ')
        ret.append(repr(self.relax2prec))
        ret.append(',\n    gamma : ')
        ret.append(repr(self.gamma))
        ret.append(',\n    id_hats : ')
        ret.append(repr(self.id_hats))
        ret.append(',\n    nbroyden : ')
        ret.append(repr(self.nbroyden))
        ret.append(',\n    newton_method : ')
        ret.append(repr(self.newton_method))
        ret.append(',\n    reduced_jacobian : ')
        ret.append(repr(self.reduced_jacobian))
        ret.append(',\n    prec_newton : ')
        ret.append(repr(self.prec_newton))
        ret.append(',\n    inexact_newton : ')
        ret.append(repr(self.inexact_newton))
        ret.append(',\n    id_save_dat : ')
        ret.append(repr(self.id_save_dat))
        ret.append(',\n    freq_dat : ')
        ret.append(repr(self.freq_dat))
        ret.append(',\n    id_save_matrix : ')
        ret.append(repr(self.id_save_matrix))
        ret.append(',\n    freq_matrix : ')
        ret.append(repr(self.freq_matrix))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "controlparameters".')

for func in _dt_array_initialisers:
    func()
