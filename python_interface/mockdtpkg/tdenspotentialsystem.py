"""
Module tdenspotentialsystem


Defined at 90modTdensPotentialSystem.fpp lines 5-544

"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

@f90wrap.runtime.register_class("mockdtpkg.tdpotsys")
class tdpotsys(f90wrap.runtime.FortranDerivedType):
    """
    Type(name=tdpotsys)
    
    
    Defined at 90modTdensPotentialSystem.fpp lines 28-218
    
    """
    def __init__(self, handle=None):
        """
        self = Tdpotsys()
        
        
        Defined at 90modTdensPotentialSystem.fpp lines 28-218
        
        
        Returns
        -------
        this : Tdpotsys
        	Object to be constructed
        
        
        Automatically generated constructor for tdpotsys
        """
        f90wrap.runtime.FortranDerivedType.__init__(self)
        result = _mockdtpkg.f90wrap_tdpotsys_initialise()
        self._handle = result[0] if isinstance(result, tuple) else result
    
    def __del__(self):
        """
        Destructor for class Tdpotsys
        
        
        Defined at 90modTdensPotentialSystem.fpp lines 28-218
        
        Parameters
        ----------
        this : Tdpotsys
        	Object to be destructed
        
        
        Automatically generated destructor for tdpotsys
        """
        if self._alloc:
            _mockdtpkg.f90wrap_tdpotsys_finalise(this=self._handle)
    
    @property
    def ntdens(self):
        """
        Element ntdens ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 31
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__ntdens(self._handle)
    
    @ntdens.setter
    def ntdens(self, ntdens):
        _mockdtpkg.f90wrap_tdpotsys__set__ntdens(self._handle, ntdens)
    
    @property
    def npot(self):
        """
        Element npot ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 33
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__npot(self._handle)
    
    @npot.setter
    def npot(self, npot):
        _mockdtpkg.f90wrap_tdpotsys__set__npot(self._handle, npot)
    
    @property
    def nfull(self):
        """
        Element nfull ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 35
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__nfull(self._handle)
    
    @nfull.setter
    def nfull(self, nfull):
        _mockdtpkg.f90wrap_tdpotsys__set__nfull(self._handle, nfull)
    
    @property
    def tdens(self):
        """
        Element tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 38
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__tdens(self._handle)
        if array_handle in self._arrays:
            tdens = self._arrays[array_handle]
        else:
            tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__tdens)
            self._arrays[array_handle] = tdens
        return tdens
    
    @tdens.setter
    def tdens(self, tdens):
        self.tdens[...] = tdens
    
    @property
    def tdens_old(self):
        """
        Element tdens_old ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 41
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__tdens_old(self._handle)
        if array_handle in self._arrays:
            tdens_old = self._arrays[array_handle]
        else:
            tdens_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__tdens_old)
            self._arrays[array_handle] = tdens_old
        return tdens_old
    
    @tdens_old.setter
    def tdens_old(self, tdens_old):
        self.tdens_old[...] = tdens_old
    
    @property
    def pot(self):
        """
        Element pot ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 44
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__pot(self._handle)
        if array_handle in self._arrays:
            pot = self._arrays[array_handle]
        else:
            pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__pot)
            self._arrays[array_handle] = pot
        return pot
    
    @pot.setter
    def pot(self, pot):
        self.pot[...] = pot
    
    @property
    def pot_old(self):
        """
        Element pot_old ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 47
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__pot_old(self._handle)
        if array_handle in self._arrays:
            pot_old = self._arrays[array_handle]
        else:
            pot_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__pot_old)
            self._arrays[array_handle] = pot_old
        return pot_old
    
    @pot_old.setter
    def pot_old(self, pot_old):
        self.pot_old[...] = pot_old
    
    @property
    def gfvar(self):
        """
        Element gfvar ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 50
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__gfvar(self._handle)
        if array_handle in self._arrays:
            gfvar = self._arrays[array_handle]
        else:
            gfvar = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__gfvar)
            self._arrays[array_handle] = gfvar
        return gfvar
    
    @gfvar.setter
    def gfvar(self, gfvar):
        self.gfvar[...] = gfvar
    
    @property
    def gfvar_old(self):
        """
        Element gfvar_old ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 53
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__gfvar_old(self._handle)
        if array_handle in self._arrays:
            gfvar_old = self._arrays[array_handle]
        else:
            gfvar_old = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__gfvar_old)
            self._arrays[array_handle] = gfvar_old
        return gfvar_old
    
    @gfvar_old.setter
    def gfvar_old(self, gfvar_old):
        self.gfvar_old[...] = gfvar_old
    
    @property
    def tdpot_syncr(self):
        """
        Element tdpot_syncr ftype=logical pytype=bool
        
        
        Defined at 90modTdensPotentialSystem.fpp line 58
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__tdpot_syncr(self._handle)
    
    @tdpot_syncr.setter
    def tdpot_syncr(self, tdpot_syncr):
        _mockdtpkg.f90wrap_tdpotsys__set__tdpot_syncr(self._handle, tdpot_syncr)
    
    @property
    def all_syncr(self):
        """
        Element all_syncr ftype=logical pytype=bool
        
        
        Defined at 90modTdensPotentialSystem.fpp line 62
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__all_syncr(self._handle)
    
    @all_syncr.setter
    def all_syncr(self, all_syncr):
        _mockdtpkg.f90wrap_tdpotsys__set__all_syncr(self._handle, all_syncr)
    
    @property
    def time_iteration(self):
        """
        Element time_iteration ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 67
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__time_iteration(self._handle)
    
    @time_iteration.setter
    def time_iteration(self, time_iteration):
        _mockdtpkg.f90wrap_tdpotsys__set__time_iteration(self._handle, time_iteration)
    
    @property
    def current_time(self):
        """
        Element current_time ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 69
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__current_time(self._handle)
    
    @current_time.setter
    def current_time(self, current_time):
        _mockdtpkg.f90wrap_tdpotsys__set__current_time(self._handle, current_time)
    
    @property
    def info_prec(self):
        """
        Element info_prec ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 73
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__info_prec(self._handle)
    
    @info_prec.setter
    def info_prec(self, info_prec):
        _mockdtpkg.f90wrap_tdpotsys__set__info_prec(self._handle, info_prec)
    
    @property
    def nlinear_system(self):
        """
        Element nlinear_system ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 75
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__nlinear_system(self._handle)
    
    @nlinear_system.setter
    def nlinear_system(self, nlinear_system):
        _mockdtpkg.f90wrap_tdpotsys__set__nlinear_system(self._handle, nlinear_system)
    
    @property
    def iter_media(self):
        """
        Element iter_media ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 79
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__iter_media(self._handle)
    
    @iter_media.setter
    def iter_media(self, iter_media):
        _mockdtpkg.f90wrap_tdpotsys__set__iter_media(self._handle, iter_media)
    
    @property
    def total_iteration(self):
        """
        Element total_iteration ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 81
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__total_iteration(self._handle)
    
    @total_iteration.setter
    def total_iteration(self, total_iteration):
        _mockdtpkg.f90wrap_tdpotsys__set__total_iteration(self._handle, total_iteration)
    
    @property
    def iter_last_prec(self):
        """
        Element iter_last_prec ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 83
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__iter_last_prec(self._handle)
    
    @iter_last_prec.setter
    def iter_last_prec(self, iter_last_prec):
        _mockdtpkg.f90wrap_tdpotsys__set__iter_last_prec(self._handle, iter_last_prec)
    
    @property
    def iter_first_system(self):
        """
        Element iter_first_system ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 85
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__iter_first_system(self._handle)
    
    @iter_first_system.setter
    def iter_first_system(self, iter_first_system):
        _mockdtpkg.f90wrap_tdpotsys__set__iter_first_system(self._handle, \
            iter_first_system)
    
    @property
    def total_iterations_linear_system(self):
        """
        Element total_iterations_linear_system ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 87
        
        """
        return \
            _mockdtpkg.f90wrap_tdpotsys__get__total_iterations_linear_system(self._handle)
    
    @total_iterations_linear_system.setter
    def total_iterations_linear_system(self, total_iterations_linear_system):
        _mockdtpkg.f90wrap_tdpotsys__set__total_iterations_linear_system(self._handle, \
            total_iterations_linear_system)
    
    @property
    def total_number_linear_system(self):
        """
        Element total_number_linear_system ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 89
        
        """
        return \
            _mockdtpkg.f90wrap_tdpotsys__get__total_number_linear_system(self._handle)
    
    @total_number_linear_system.setter
    def total_number_linear_system(self, total_number_linear_system):
        _mockdtpkg.f90wrap_tdpotsys__set__total_number_linear_system(self._handle, \
            total_number_linear_system)
    
    @property
    def itemp_last_prec(self):
        """
        Element itemp_last_prec ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 91
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__itemp_last_prec(self._handle)
    
    @itemp_last_prec.setter
    def itemp_last_prec(self, itemp_last_prec):
        _mockdtpkg.f90wrap_tdpotsys__set__itemp_last_prec(self._handle, itemp_last_prec)
    
    @property
    def ref_iter(self):
        """
        Element ref_iter ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 93
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__ref_iter(self._handle)
    
    @ref_iter.setter
    def ref_iter(self, ref_iter):
        _mockdtpkg.f90wrap_tdpotsys__set__ref_iter(self._handle, ref_iter)
    
    @property
    def iter_newton_last_prec(self):
        """
        Element iter_newton_last_prec ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 96
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__iter_newton_last_prec(self._handle)
    
    @iter_newton_last_prec.setter
    def iter_newton_last_prec(self, iter_newton_last_prec):
        _mockdtpkg.f90wrap_tdpotsys__set__iter_newton_last_prec(self._handle, \
            iter_newton_last_prec)
    
    @property
    def nrestart_newton(self):
        """
        Element nrestart_newton ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 98
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__nrestart_newton(self._handle)
    
    @nrestart_newton.setter
    def nrestart_newton(self, nrestart_newton):
        _mockdtpkg.f90wrap_tdpotsys__set__nrestart_newton(self._handle, nrestart_newton)
    
    @property
    def nrestart_invert_jacobian(self):
        """
        Element nrestart_invert_jacobian ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 100
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__nrestart_invert_jacobian(self._handle)
    
    @nrestart_invert_jacobian.setter
    def nrestart_invert_jacobian(self, nrestart_invert_jacobian):
        _mockdtpkg.f90wrap_tdpotsys__set__nrestart_invert_jacobian(self._handle, \
            nrestart_invert_jacobian)
    
    @property
    def res_elliptic(self):
        """
        Element res_elliptic ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 103
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__res_elliptic(self._handle)
    
    @res_elliptic.setter
    def res_elliptic(self, res_elliptic):
        _mockdtpkg.f90wrap_tdpotsys__set__res_elliptic(self._handle, res_elliptic)
    
    @property
    def mass_tdens(self):
        """
        Element mass_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 109
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__mass_tdens(self._handle)
    
    @mass_tdens.setter
    def mass_tdens(self, mass_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__mass_tdens(self._handle, mass_tdens)
    
    @property
    def weighted_mass_tdens(self):
        """
        Element weighted_mass_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 112
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__weighted_mass_tdens(self._handle)
    
    @weighted_mass_tdens.setter
    def weighted_mass_tdens(self, weighted_mass_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__weighted_mass_tdens(self._handle, \
            weighted_mass_tdens)
    
    @property
    def energy(self):
        """
        Element energy ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 115
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__energy(self._handle)
    
    @energy.setter
    def energy(self, energy):
        _mockdtpkg.f90wrap_tdpotsys__set__energy(self._handle, energy)
    
    @property
    def lyapunov(self):
        """
        Element lyapunov ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 118
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__lyapunov(self._handle)
    
    @lyapunov.setter
    def lyapunov(self, lyapunov):
        _mockdtpkg.f90wrap_tdpotsys__set__lyapunov(self._handle, lyapunov)
    
    @property
    def min_tdens(self):
        """
        Element min_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 121
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__min_tdens(self._handle)
    
    @min_tdens.setter
    def min_tdens(self, min_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__min_tdens(self._handle, min_tdens)
    
    @property
    def max_tdens(self):
        """
        Element max_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 124
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__max_tdens(self._handle)
    
    @max_tdens.setter
    def max_tdens(self, max_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__max_tdens(self._handle, max_tdens)
    
    @property
    def max_velocity(self):
        """
        Element max_velocity ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 127
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__max_velocity(self._handle)
    
    @max_velocity.setter
    def max_velocity(self, max_velocity):
        _mockdtpkg.f90wrap_tdpotsys__set__max_velocity(self._handle, max_velocity)
    
    @property
    def max_nrm_grad(self):
        """
        Element max_nrm_grad ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 130
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__max_nrm_grad(self._handle)
    
    @max_nrm_grad.setter
    def max_nrm_grad(self, max_nrm_grad):
        _mockdtpkg.f90wrap_tdpotsys__set__max_nrm_grad(self._handle, max_nrm_grad)
    
    @property
    def max_nrm_grad_avg(self):
        """
        Element max_nrm_grad_avg ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 133
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__max_nrm_grad_avg(self._handle)
    
    @max_nrm_grad_avg.setter
    def max_nrm_grad_avg(self, max_nrm_grad_avg):
        _mockdtpkg.f90wrap_tdpotsys__set__max_nrm_grad_avg(self._handle, \
            max_nrm_grad_avg)
    
    @property
    def max_d3(self):
        """
        Element max_d3 ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 136
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__max_d3(self._handle)
    
    @max_d3.setter
    def max_d3(self, max_d3):
        _mockdtpkg.f90wrap_tdpotsys__set__max_d3(self._handle, max_d3)
    
    @property
    def integral_flux_pvel(self):
        """
        Element integral_flux_pvel ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 140
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__integral_flux_pvel(self._handle)
    
    @integral_flux_pvel.setter
    def integral_flux_pvel(self, integral_flux_pvel):
        _mockdtpkg.f90wrap_tdpotsys__set__integral_flux_pvel(self._handle, \
            integral_flux_pvel)
    
    @property
    def duality_gap(self):
        """
        Element duality_gap ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 143
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__duality_gap(self._handle)
    
    @duality_gap.setter
    def duality_gap(self, duality_gap):
        _mockdtpkg.f90wrap_tdpotsys__set__duality_gap(self._handle, duality_gap)
    
    @property
    def err_tdens(self):
        """
        Element err_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 146
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__err_tdens(self._handle)
    
    @err_tdens.setter
    def err_tdens(self, err_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__err_tdens(self._handle, err_tdens)
    
    @property
    def err_pot(self):
        """
        Element err_pot ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 149
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__err_pot(self._handle)
    
    @err_pot.setter
    def err_pot(self, err_pot):
        _mockdtpkg.f90wrap_tdpotsys__set__err_pot(self._handle, err_pot)
    
    @property
    def err_wasserstein(self):
        """
        Element err_wasserstein ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 152
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__err_wasserstein(self._handle)
    
    @err_wasserstein.setter
    def err_wasserstein(self, err_wasserstein):
        _mockdtpkg.f90wrap_tdpotsys__set__err_wasserstein(self._handle, err_wasserstein)
    
    @property
    def wasserstein_distance(self):
        """
        Element wasserstein_distance ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 155
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__wasserstein_distance(self._handle)
    
    @wasserstein_distance.setter
    def wasserstein_distance(self, wasserstein_distance):
        _mockdtpkg.f90wrap_tdpotsys__set__wasserstein_distance(self._handle, \
            wasserstein_distance)
    
    @property
    def iter_nonlinear(self):
        """
        Element iter_nonlinear ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 159
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__iter_nonlinear(self._handle)
    
    @iter_nonlinear.setter
    def iter_nonlinear(self, iter_nonlinear):
        _mockdtpkg.f90wrap_tdpotsys__set__iter_nonlinear(self._handle, iter_nonlinear)
    
    @property
    def loc_var_tdens(self):
        """
        Element loc_var_tdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 164
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__loc_var_tdens(self._handle)
    
    @loc_var_tdens.setter
    def loc_var_tdens(self, loc_var_tdens):
        _mockdtpkg.f90wrap_tdpotsys__set__loc_var_tdens(self._handle, loc_var_tdens)
    
    @property
    def scr_ntdens(self):
        """
        Element scr_ntdens ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 170
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__scr_ntdens(self._handle)
        if array_handle in self._arrays:
            scr_ntdens = self._arrays[array_handle]
        else:
            scr_ntdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__scr_ntdens)
            self._arrays[array_handle] = scr_ntdens
        return scr_ntdens
    
    @scr_ntdens.setter
    def scr_ntdens(self, scr_ntdens):
        self.scr_ntdens[...] = scr_ntdens
    
    @property
    def scr_npot(self):
        """
        Element scr_npot ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 173
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__scr_npot(self._handle)
        if array_handle in self._arrays:
            scr_npot = self._arrays[array_handle]
        else:
            scr_npot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__scr_npot)
            self._arrays[array_handle] = scr_npot
        return scr_npot
    
    @scr_npot.setter
    def scr_npot(self, scr_npot):
        self.scr_npot[...] = scr_npot
    
    @property
    def scr_nfull(self):
        """
        Element scr_nfull ftype=real(kind=double) pytype=float
        
        
        Defined at 90modTdensPotentialSystem.fpp line 176
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__scr_nfull(self._handle)
        if array_handle in self._arrays:
            scr_nfull = self._arrays[array_handle]
        else:
            scr_nfull = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__scr_nfull)
            self._arrays[array_handle] = scr_nfull
        return scr_nfull
    
    @scr_nfull.setter
    def scr_nfull(self, scr_nfull):
        self.scr_nfull[...] = scr_nfull
    
    @property
    def ntdens_on(self):
        """
        Element ntdens_on ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 181
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__ntdens_on(self._handle)
    
    @ntdens_on.setter
    def ntdens_on(self, ntdens_on):
        _mockdtpkg.f90wrap_tdpotsys__set__ntdens_on(self._handle, ntdens_on)
    
    @property
    def npot_on(self):
        """
        Element npot_on ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 183
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__npot_on(self._handle)
    
    @npot_on.setter
    def npot_on(self, npot_on):
        _mockdtpkg.f90wrap_tdpotsys__set__npot_on(self._handle, npot_on)
    
    @property
    def ntdens_off(self):
        """
        Element ntdens_off ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 185
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__ntdens_off(self._handle)
    
    @ntdens_off.setter
    def ntdens_off(self, ntdens_off):
        _mockdtpkg.f90wrap_tdpotsys__set__ntdens_off(self._handle, ntdens_off)
    
    @property
    def npot_off(self):
        """
        Element npot_off ftype=integer  pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 187
        
        """
        return _mockdtpkg.f90wrap_tdpotsys__get__npot_off(self._handle)
    
    @npot_off.setter
    def npot_off(self, npot_off):
        _mockdtpkg.f90wrap_tdpotsys__set__npot_off(self._handle, npot_off)
    
    @property
    def active_tdens(self):
        """
        Element active_tdens ftype=integer pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 190
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__active_tdens(self._handle)
        if array_handle in self._arrays:
            active_tdens = self._arrays[array_handle]
        else:
            active_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__active_tdens)
            self._arrays[array_handle] = active_tdens
        return active_tdens
    
    @active_tdens.setter
    def active_tdens(self, active_tdens):
        self.active_tdens[...] = active_tdens
    
    @property
    def active_pot(self):
        """
        Element active_pot ftype=integer pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 193
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__active_pot(self._handle)
        if array_handle in self._arrays:
            active_pot = self._arrays[array_handle]
        else:
            active_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__active_pot)
            self._arrays[array_handle] = active_pot
        return active_pot
    
    @active_pot.setter
    def active_pot(self, active_pot):
        self.active_pot[...] = active_pot
    
    @property
    def inactive_tdens(self):
        """
        Element inactive_tdens ftype=integer pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 196
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__inactive_tdens(self._handle)
        if array_handle in self._arrays:
            inactive_tdens = self._arrays[array_handle]
        else:
            inactive_tdens = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__inactive_tdens)
            self._arrays[array_handle] = inactive_tdens
        return inactive_tdens
    
    @inactive_tdens.setter
    def inactive_tdens(self, inactive_tdens):
        self.inactive_tdens[...] = inactive_tdens
    
    @property
    def inactive_pot(self):
        """
        Element inactive_pot ftype=integer pytype=int
        
        
        Defined at 90modTdensPotentialSystem.fpp line 199
        
        """
        array_ndim, array_type, array_shape, array_handle = \
            _mockdtpkg.f90wrap_tdpotsys__array__inactive_pot(self._handle)
        if array_handle in self._arrays:
            inactive_pot = self._arrays[array_handle]
        else:
            inactive_pot = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                    self._handle,
                                    _mockdtpkg.f90wrap_tdpotsys__array__inactive_pot)
            self._arrays[array_handle] = inactive_pot
        return inactive_pot
    
    @inactive_pot.setter
    def inactive_pot(self, inactive_pot):
        self.inactive_pot[...] = inactive_pot
    
    def __str__(self):
        ret = ['<tdpotsys>{\n']
        ret.append('    ntdens : ')
        ret.append(repr(self.ntdens))
        ret.append(',\n    npot : ')
        ret.append(repr(self.npot))
        ret.append(',\n    nfull : ')
        ret.append(repr(self.nfull))
        ret.append(',\n    tdens : ')
        ret.append(repr(self.tdens))
        ret.append(',\n    tdens_old : ')
        ret.append(repr(self.tdens_old))
        ret.append(',\n    pot : ')
        ret.append(repr(self.pot))
        ret.append(',\n    pot_old : ')
        ret.append(repr(self.pot_old))
        ret.append(',\n    gfvar : ')
        ret.append(repr(self.gfvar))
        ret.append(',\n    gfvar_old : ')
        ret.append(repr(self.gfvar_old))
        ret.append(',\n    tdpot_syncr : ')
        ret.append(repr(self.tdpot_syncr))
        ret.append(',\n    all_syncr : ')
        ret.append(repr(self.all_syncr))
        ret.append(',\n    time_iteration : ')
        ret.append(repr(self.time_iteration))
        ret.append(',\n    current_time : ')
        ret.append(repr(self.current_time))
        ret.append(',\n    info_prec : ')
        ret.append(repr(self.info_prec))
        ret.append(',\n    nlinear_system : ')
        ret.append(repr(self.nlinear_system))
        ret.append(',\n    iter_media : ')
        ret.append(repr(self.iter_media))
        ret.append(',\n    total_iteration : ')
        ret.append(repr(self.total_iteration))
        ret.append(',\n    iter_last_prec : ')
        ret.append(repr(self.iter_last_prec))
        ret.append(',\n    iter_first_system : ')
        ret.append(repr(self.iter_first_system))
        ret.append(',\n    total_iterations_linear_system : ')
        ret.append(repr(self.total_iterations_linear_system))
        ret.append(',\n    total_number_linear_system : ')
        ret.append(repr(self.total_number_linear_system))
        ret.append(',\n    itemp_last_prec : ')
        ret.append(repr(self.itemp_last_prec))
        ret.append(',\n    ref_iter : ')
        ret.append(repr(self.ref_iter))
        ret.append(',\n    iter_newton_last_prec : ')
        ret.append(repr(self.iter_newton_last_prec))
        ret.append(',\n    nrestart_newton : ')
        ret.append(repr(self.nrestart_newton))
        ret.append(',\n    nrestart_invert_jacobian : ')
        ret.append(repr(self.nrestart_invert_jacobian))
        ret.append(',\n    res_elliptic : ')
        ret.append(repr(self.res_elliptic))
        ret.append(',\n    mass_tdens : ')
        ret.append(repr(self.mass_tdens))
        ret.append(',\n    weighted_mass_tdens : ')
        ret.append(repr(self.weighted_mass_tdens))
        ret.append(',\n    energy : ')
        ret.append(repr(self.energy))
        ret.append(',\n    lyapunov : ')
        ret.append(repr(self.lyapunov))
        ret.append(',\n    min_tdens : ')
        ret.append(repr(self.min_tdens))
        ret.append(',\n    max_tdens : ')
        ret.append(repr(self.max_tdens))
        ret.append(',\n    max_velocity : ')
        ret.append(repr(self.max_velocity))
        ret.append(',\n    max_nrm_grad : ')
        ret.append(repr(self.max_nrm_grad))
        ret.append(',\n    max_nrm_grad_avg : ')
        ret.append(repr(self.max_nrm_grad_avg))
        ret.append(',\n    max_d3 : ')
        ret.append(repr(self.max_d3))
        ret.append(',\n    integral_flux_pvel : ')
        ret.append(repr(self.integral_flux_pvel))
        ret.append(',\n    duality_gap : ')
        ret.append(repr(self.duality_gap))
        ret.append(',\n    err_tdens : ')
        ret.append(repr(self.err_tdens))
        ret.append(',\n    err_pot : ')
        ret.append(repr(self.err_pot))
        ret.append(',\n    err_wasserstein : ')
        ret.append(repr(self.err_wasserstein))
        ret.append(',\n    wasserstein_distance : ')
        ret.append(repr(self.wasserstein_distance))
        ret.append(',\n    iter_nonlinear : ')
        ret.append(repr(self.iter_nonlinear))
        ret.append(',\n    loc_var_tdens : ')
        ret.append(repr(self.loc_var_tdens))
        ret.append(',\n    scr_ntdens : ')
        ret.append(repr(self.scr_ntdens))
        ret.append(',\n    scr_npot : ')
        ret.append(repr(self.scr_npot))
        ret.append(',\n    scr_nfull : ')
        ret.append(repr(self.scr_nfull))
        ret.append(',\n    ntdens_on : ')
        ret.append(repr(self.ntdens_on))
        ret.append(',\n    npot_on : ')
        ret.append(repr(self.npot_on))
        ret.append(',\n    ntdens_off : ')
        ret.append(repr(self.ntdens_off))
        ret.append(',\n    npot_off : ')
        ret.append(repr(self.npot_off))
        ret.append(',\n    active_tdens : ')
        ret.append(repr(self.active_tdens))
        ret.append(',\n    active_pot : ')
        ret.append(repr(self.active_pot))
        ret.append(',\n    inactive_tdens : ')
        ret.append(repr(self.inactive_tdens))
        ret.append(',\n    inactive_pot : ')
        ret.append(repr(self.inactive_pot))
        ret.append('}')
        return ''.join(ret)
    
    _dt_array_initialisers = []
    

def init_tdpotsys(self, lun_err, lun_out, lun_stat, ntdens, npot):
    """
    init_tdpotsys(self, lun_err, lun_out, lun_stat, ntdens, npot)
    
    
    Defined at 90modTdensPotentialSystem.fpp lines 254-333
    
    Parameters
    ----------
    this : Tdpotsys
    lun_err : int
    lun_out : int
    lun_stat : int
    ntdens : int
    npot : int
    
    """
    _mockdtpkg.f90wrap_init_tdpotsys(this=self._handle, lun_err=lun_err, \
        lun_out=lun_out, lun_stat=lun_stat, ntdens=ntdens, npot=npot)


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "tdenspotentialsystem".')

for func in _dt_array_initialisers:
    func()
