"""
Module kinddeclaration


Defined at 00modKindDeclaration.fpp lines 16-42

"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging

_arrays = {}
_objs = {}

def get_single():
    """
    Element single ftype=integer pytype=int
    
    
    Defined at 00modKindDeclaration.fpp line 21
    
    """
    return _mockdtpkg.f90wrap_kinddeclaration__get__single()

single = get_single()

def get_double_bn():
    """
    Element double_bn ftype=integer pytype=int
    
    
    Defined at 00modKindDeclaration.fpp line 23
    
    """
    return _mockdtpkg.f90wrap_kinddeclaration__get__double_bn()

double = get_double_bn()

def get_zero():
    """
    Element zero ftype=real(kind=double) pytype=float
    
    
    Defined at 00modKindDeclaration.fpp line 27
    
    """
    return _mockdtpkg.f90wrap_kinddeclaration__get__zero()

zero = get_zero()

def get_one():
    """
    Element one ftype=real(kind=double) pytype=float
    
    
    Defined at 00modKindDeclaration.fpp line 28
    
    """
    return _mockdtpkg.f90wrap_kinddeclaration__get__one()

one = get_one()


_array_initialisers = []
_dt_array_initialisers = []

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module \
        "kinddeclaration".')

for func in _dt_array_initialisers:
    func()
