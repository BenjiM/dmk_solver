"""
Module use_a_type


Defined at fwrap.fpp lines 10-73

This file contains a single module with several module-level vars
with one of them a derived type, which is defined in the 'base library'.
"""
from __future__ import print_function, absolute_import, division
import _mockdtpkg
import f90wrap.runtime
import logging
from mockdtpkg.define_a_type import atype

_arrays = {}
_objs = {}

def do_stuff(factor):
    """
    out = do_stuff(factor)
    
    
    Defined at fwrap.fpp lines 23-64
    
    Parameters
    ----------
    factor : float
    
    Returns
    -------
    out : float
    
    This is the module which defines the type 'atype'
    Here's a routine that does something
    """
    out = _mockdtpkg.f90wrap_do_stuff(factor=factor)
    return out

def not_used(x):
    """
    y = not_used(x)
    
    
    Defined at fwrap.fpp lines 66-73
    
    Parameters
    ----------
    x : float
    
    Returns
    -------
    y : float
    
    """
    y = _mockdtpkg.f90wrap_not_used(x=x)
    return y

def get_p():
    """
    Element p ftype=type(atype) pytype=Atype
    
    
    Defined at fwrap.fpp line 12
    
    A variable with this type. NB: target attribute needed to allow access from \
        Python
    """
    global p
    p_handle = _mockdtpkg.f90wrap_use_a_type__get__p()
    if tuple(p_handle) in _objs:
        p = _objs[tuple(p_handle)]
    else:
        p = atype.from_handle(p_handle)
        _objs[tuple(p_handle)] = p
    return p

def set_p(p):
    p = p._handle
    _mockdtpkg.f90wrap_use_a_type__set__p(p)

def init_array_p_array():
    global p_array
    p_array = f90wrap.runtime.FortranDerivedTypeArray(f90wrap.runtime.empty_type,
                                    _mockdtpkg.f90wrap_use_a_type__array_getitem__p_array,
                                    _mockdtpkg.f90wrap_use_a_type__array_setitem__p_array,
                                    _mockdtpkg.f90wrap_use_a_type__array_len__p_array,
                                    """
    Element p_array ftype=type(atype) pytype=Atype
    
    
    Defined at fwrap.fpp line 13
    
    Array of derived types, also wrapped
    """, atype)
    return p_array

def get_array_vector():
    """
    Element vector ftype=real(8) pytype=float
    
    
    Defined at fwrap.fpp line 14
    
    It also contains allocatable arrays
    For simplicity, P has a variable of each of several base types.
    That is, a logical, real, integer, real array, and a
    derived type from a different module.
    """
    global vector
    array_ndim, array_type, array_shape, array_handle = \
        _mockdtpkg.f90wrap_use_a_type__array__vector(f90wrap.runtime.empty_handle)
    if array_handle in _arrays:
        vector = _arrays[array_handle]
    else:
        vector = f90wrap.runtime.get_array(f90wrap.runtime.sizeof_fortran_t,
                                f90wrap.runtime.empty_handle,
                                _mockdtpkg.f90wrap_use_a_type__array__vector)
        _arrays[array_handle] = vector
    return vector

def set_array_vector(vector):
    vector[...] = vector


_array_initialisers = [get_array_vector]
_dt_array_initialisers = [init_array_p_array]

try:
    for func in _array_initialisers:
        func()
except ValueError:
    logging.debug('unallocated array(s) detected on import of module "use_a_type".')

for func in _dt_array_initialisers:
    func()
