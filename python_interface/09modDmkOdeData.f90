module DmkOdeData
  use KindDeclaration
  implicit none
  private  
  public OdeData_constructor, OdeData_destructor
  !>----------------------------------------------------------------
  !> Structure variable containg the time varing quantities 
  !> (pflux, pmass, decay, kappa, rhs_integrated)
  !> and fix in time quanties 
  !> (tdens0) the ODE  
  !> \begin{gather}
  !>    \Div(\Tdens \Grad \Pot) = \Forcing 
  !>    \quad
  !>    -\Tdens \Grad \Pot \cdot \n_{\partial \Domain} = \Bdflux
  !> \\
  !> \Tdens'=|\Tdens \Grad \Pot(\Tdens)|^\Pflux-kappa*decay*\Tdens^{\Pmass}
  !> \\
  !> \Tdens(tzero) = \Tdens0
  !> \end{gather}
  !> It may contains the optional quantities 
  !> forcing, boundary_flux, optdens(fix initialized and used
  !> only if the correspondet input files exis
  !>------------------------------------------------------------------
  type, public :: OdeData
     !> Identifier of ODE
     !> 1 : PP  (Physarum Policephalum dynamic)
     !> 2 : GF  (Gradient Flow dynamic in Tdens variable)
     !> 3 : GF2 (Gradient Flow dynamic in Gfvar variable)
     integer :: id_ode
     !> Number of time frames
     integer :: ntimes
     !> time frames
     real(kind=double) ::  time
     !>-------------------------------------------------------------
     !> Mandatory Input data
     !>-------------------------------------------------------------
     !> Nmber of variables for Tdens-variables 
     integer :: ntdens
     !> Nmber of variables for Pot-variables
     integer :: npot
     !>--------------------------------------------------------------
     !> True/False flag for existence of optimal potential array
     logical :: dirichlet_exists= .False.
     !> Dirichelet quantities
     !> Number of Dirichlet nodes 
     integer :: ndir=0
     !> Dimension= ndir
     !> Dirichlet node indeces ( in subgrid ) 
     integer, allocatable :: dirichlet_nodes(:)
     !> Dimension = ndir
     !> Dirichlet values at nodes ( in subgrid )
     real(kind=double), allocatable :: dirichlet_values(:)
     !>------------------------------------------------------------
     !> Lift of tdens in elliptic equation
     !> -div(\tdens + lasso \grad \pot) = rhs         ( continouos )
     !> ( A ( diag(\tdens) + lambda  Id )A pot  ) \pot = rhs ( discrete )
     real(kind=double) :: lambda
     !> Lasso relaxation 
     !> (stiff(tdens) + lasso mass_matrix)  pot = rhs         ( continouos )
     !> (stiff(tdens) + lasso Id         )  pot = rhs         ( discrete )
     real(kind=double) :: lasso
     !>--------------------------------------------------------------
     !> Pflux power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pflux
     !> Time decay
     !> Contiains 1 real with pflux power
     real(kind=double) :: decay
     !> Pmass power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pmass
     !> ODE expoent for norm of grad
     !> pflux : PP  (Physarum Policephalum dynamic)
     !> 2.0   : GF  (Gradient Flow dynamic in Tdens variable)
     real(kind=double) :: pode
     !>-----------------------------------------------------
     !> Penalization
     !>-----------------------------------------------------
     !> True/False flag for existence penalization
     logical :: penalty_exists = .False.
     !> Scaling factor for penalty
     !> Contiains 1 real with pflux power
     real(kind=double), allocatable :: penalty_factor
     !> Optional weight $\Wpenalty$ of penalty decay
     !> Give by adding penalty 
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     real(kind=double), allocatable :: penalty_weight(:)
     !> Dimension = ntdens
     !> Optional penalty $\geq 0$ given by adding
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     !> in the minimization problem.
     real(kind=double), allocatable :: penalty(:)
     !>-----------------------------------------------------
     !> Dimension = ntdens
     !> Spatial decay 
     !> Contiains ntria-array with spatial decay
     real(kind=double), allocatable :: kappa(:)
     !> Dimension = npot
     !> Rhs in the linear system
     !> arising from equation 
     !> $-\div(\Tdens \Grad \Pot ) = \Forcing$
     !> Completed with proper boundary condition
     real(kind=double), allocatable :: rhs_forcing(:)
   contains
     !> Static constructor
     !> (public for type OdeData)
     procedure, public, nopass :: OdeData_constructor
     !> Static destructor
     !> (public for type OdeData)
     procedure, public, nopass :: OdeData_destructor
     !> Info procedure
     !> (public for type OdeData)
     procedure, public, nopass :: OdeData_info
  end type OdeData
  
   type, public :: ProblemData
      !>-------------------------------------------------------------
      !> Mandatory Input data
      !>-------------------------------------------------------------
      !> Nmber of variables for Tdens-variables 
      integer :: ntdens
      !> Nmber of variables for Pot-variables
      integer :: npot
      !>-----------------------------------------------------
      !> Reference solution to compute errors
      !>-----------------------------------------------------
      !> True/False flag for existence of optdens array
      logical :: opt_tdens_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_tdens(:)
      !> True/False flag for existence of optdens array
      logical :: opt_pot_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_pot(:)
      !> Initial Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: tdens0(:)
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: pot0(:,:)
   end type ProblemData


contains
    !>------------------------------------------------------------
    !> Static constructor.
    !> (procedure public for type OdeInp)
    !> Instantiate (allocate if necessary)
    !> and initialize variables of type OdeInp
    !>
    !> usage:
    !>     call 'var'%init(lun_err,ntdens,npot)
    !>
    !> where:
    !> \param[in] lun_err -> integer. Logical unit for error msg.
    !> \param[in] ntdens  -> integer. Dimension of Tdens-variables
    !> \param[in] npot    -> integer. Dimension of Pot-variables
    !<-------------------------------------------------------------
    subroutine OdeData_constructor(this,&
         lun_err,&
         ntdens,npot)
      implicit none
      class(OdeData),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      integer,     intent(in   ) :: ntdens
      integer,     intent(in   ) :: npot
      !local
      logical :: rc
      integer :: res


      this%ntdens = ntdens
      this%npot   = npot

      allocate( &
            this%dirichlet_nodes(this%npot),&
            this%dirichlet_values(this%npot),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in OdeData_desctructor'

!!$      allocate( &
!!$           this%tdens0(ntdens),&
!!$           stat=res)
!!$      if(res .ne. 0) write(lun_err,*) 'allocation error in OdeData_desctructor'

      allocate( &
           this%penalty_weight(ntdens),&
           this%kappa(ntdens),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in OdeData_desctructor'

      allocate( &
           this%rhs_forcing(npot),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in OdeData_desctructor'

    end subroutine OdeData_constructor


    !>------------------------------------------------------------
    !> Static desconstructor.
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine OdeData_destructor(this,lun_err )
      implicit none
      class(OdeData),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      !local
      integer ::res

      this%ntdens = 0
      this%npot   = 0
      this%ndir   = 0


      deallocate( &
            this%dirichlet_nodes,&
            this%dirichlet_values,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in OdeData_desctructor'

      deallocate( &
           this%penalty,&
           this%penalty_weight,&
           this%kappa,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in OdeData_desctructor'

      deallocate( &
           this%rhs_forcing,&
           stat=res)
      if(res .ne. 0)  write(lun_err,*) 'deallocation error in OdeData_desctructor'
    end subroutine OdeData_destructor

    !>------------------------------------------------------------
    !> Information procedure
    !> (procedure public for type OdeData)
    !>
    !> usage:
    !>     call 'var'%info(lun_out)
    !>
    !> where:
    !> \param[in] lun_out -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine OdeData_info(this,lun_out )
      implicit none
      class(OdeData),     intent(inout) :: this
      integer,     intent(in   ) :: lun_out

      write(lun_out,*) &
           'Tdens     dimensions =', this%ntdens , &
           'Potential dimensions =', this%npot
      write(lun_out,*) 'ODE type             =', this%id_ode

    end subroutine OdeData_info

   
    
  end module DmkOdeData
