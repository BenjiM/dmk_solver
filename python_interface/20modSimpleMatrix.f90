module SimpleMatrix
  use Globals
  use LinearOperator
  implicit none
  private
  type, extends(abs_linop), public :: diagmat
     !> Flag if matrix has been initialized
     logical :: is_initialized =.false.
     !> Vector y = prec_zero^{-1} uvec 
     !> Dimension(prec_zero%nequ)
     real(kind=double), allocatable :: diagonal(:)
   contains
     !> static constructor
     !> (procedure public for type diagmat)
     procedure, public, pass :: init => init_diagmat
     !> static constructor
     !> (procedure public for type diagmat)
     procedure, public, pass :: set => set_diagmat
     !> static destructor
     !> (procedure public for type diagmat)
     procedure, public, pass :: kill => kill_diagmat
     !> Procedure to compute 
     !>         y = M * x 
     !> (public procedure for type blockmat)
     procedure, public, pass :: Mxv => Mxv_diagmat
  end type diagmat
  
  contains
    subroutine init_diagmat(this,lun_err,nequ)
     use Globals
     implicit none
     class(diagmat),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     integer,            intent(in   ) :: nequ
     !local
     logical :: rc
     integer :: res,i
     integer, allocatable :: iwork(:)
     real(kind=double) :: dnrm2

     ! set properties
     this%is_squared   = .true.
     this%is_symmetric = .true.
     this%triangular   = 'N'
     this%is_initialized = .True.
     
     


     ! copy coefficients
     this%nrow  = nequ
     this%ncol  = nequ
     allocate(this%diagonal(this%nrow),stat=res)

     write(*,*) 'internal info',  dnrm2(this%nrow,this%diagonal,1)
   contains
     subroutine test_contains(i,j)
       integer :: i
       integer :: j
       i=j
     end subroutine test_contains

     subroutine test_contains2(i,j)
       integer :: i
       integer :: j
       i=j
     end subroutine test_contains2
    
   end subroutine init_diagmat

    subroutine set_diagmat(this,lun_err,diagonal)
     use Globals
     implicit none
     class(diagmat),     intent(inout) :: this
     integer,            intent(in   ) :: lun_err
     real(kind=double),  intent(in   ) :: diagonal(this%nrow)
     !local
     logical :: rc
     integer :: res,i
     
     !
     ! copy coefficients
     !
     this%diagonal   = diagonal
     
   end subroutine set_diagmat


   subroutine kill_diagmat(this,lun_err)
     use Globals
     implicit none
     class(diagmat),  intent(inout) :: this
     integer,         intent(in   ) :: lun_err
     !local
     logical :: rc
     integer :: res


     deallocate(this%diagonal,stat=res)

     this%is_initialized = .False.
     this%nrow           = 0
     this%is_symmetric   = .false.
     this%is_squared     = .false.
     this%triangular     = 'N' 
     
   end subroutine kill_diagmat

   recursive  subroutine Mxv_diagmat(this,vec_in,vec_out,info,lun_err)
     use Globals
     implicit none
     class(diagmat), intent(inout) :: this
     real(kind=double), intent(in   ) :: vec_in(this%ncol)
     real(kind=double), intent(inout) :: vec_out(this%nrow)
     integer, optional, intent(inout) :: info
     integer, optional, intent(in   ) :: lun_err
     
     vec_out = this%diagonal * vec_in
   end subroutine Mxv_diagmat



   

 end module SimpleMatrix
