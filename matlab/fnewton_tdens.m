function [ F ] = tdens_newton( tdens, tdens_old, deltat, norm_grad2 )
%UNTITLED4 Summary of this function goes here
%   Detailed explanation goes here

F = -(tdens-tdens_old)/deltat + tdens.*(norm_grad2-1);
end

