function [ matrix_B ] = build_matrixB( Bassembler,ia, ja, gradpot, gradx_coeff, grady_coeff, size_subcell,coeff_B)
%UNTITLED7 Summary of this function goes here
%   Detailed explanation goes here
ntdens = size(size_subcell,1)/4;

nsubcell=size(size_subcell,1);
values = zeros(nsubcell,1);
coeff  = zeros(ntdens*6,1);
format long
for isubcell = 1: nsubcell
    for iloc=1:3
        iterm = Bassembler(iloc,isubcell);
        %inode = topol(iloc,isubcell);
        gx=gradx_coeff(iloc,isubcell);
        gy=grady_coeff(iloc,isubcell);
        %disp(( gradpot(1,isubcell) * gx +  gradpot(2,isubcell) * gy)*size_subcell(isubcell))
        %disp(values(isubcell))
        %fprintf('%d %d %f %f %f %f \n ',isubcell, iloc, gx,gy, gradpot(1,isubcell), gradpot(2,isubcell) )
        coeff(iterm) = coeff(iterm) + ( gradpot(1,isubcell) * gx +  gradpot(2,isubcell) * gy)*size_subcell(isubcell);
        %disp(values(isubcell))
        %fprintf('%f\n',values(isubcell))
    end
end



% 
% for icol = 1: nsubcell
%     icell = idivide(int32(icol-1),4)+1;
%     %fprintf('%d %d\n', icol, icell)
%     for j=1:6
%         iterm = (icell-1)*6 + j;
%         %fprintf('%d %d\n', j,iterm)
%         
%         coeff(iterm) = coeff(iterm) + values (icol);
%     end
% end


%norm(coeff-coeff_B)
%coeff(ntdens*6-5:ntdens*6)


matrix_B = sparse(ia,ja,coeff);



end

