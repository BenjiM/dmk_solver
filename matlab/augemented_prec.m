function [ y ] = augemented_prec( x, Agamma, B, S)
% applicaiton of prec 
% (Agamma  0 )
% (B       S )

y=zeros(size(x));
na=size(Agamma,1);
nb=size(B,1);
y(1:na) = (Agamma \ x(1:na));
z= x(na+1:na+nb);
z= z - B*y(1:na);
y(na+1:na+nb) = S \ z;  
end

