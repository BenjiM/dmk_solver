function [] = write2td( fname, data )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

fileID = fopen(fname,'w');
fprintf(fileID,'1 %d \n',size(data,1));
fprintf(fileID,'time 0.0\n');
fprintf(fileID,'%d\n',size(data,1));
for i=1:size(data,1)
    fprintf(fileID,'%d %12.8e \n',i,data(i));
end
fprintf(fileID,'time 1.0e30');
fclose(fileID);

end

