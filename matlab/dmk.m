close all
clear all

%%% set controls
%% time step 
max_timestep = 1;
tol_stop = 1.0e-5;
deltat = 4e-1;
min_tdens = 1e-10;
% time stepping
id_scheme = 2; % 1 = Explicit Euler 2=Implicit Euler 3=gfvar
method='augmented';
study_schur =0;

% Newton
max_nnlin = 15;
tol_nnlin = 1.0e-10;

% linear system 
tol_linsol=1.0d-13;
imax=1000;

beta=1.0;

folder='../benzi/runs/medium_copy/';
folder_input='input/';




%%%  define minimal file requirement
file_sizecell=strcat(folder,'sizecell.dat');
file_sizesubcell=strcat(folder,'sizesubcell.dat');
file_stiff=strcat(folder,'stiff.dat');
file_assembler_stiff=strcat(folder,'assembler_stiff.dat');
file_gradx=strcat(folder,'gradx.dat');
file_grady=strcat(folder,'grady.dat');
file_gradpot=strcat(folder,'grad.dat');
file_Bmatrix=strcat(folder,'Bmatrix.dat');
file_assembler_Bmatrix=strcat(folder,'assemblerB.dat');

file_rhs=strcat(folder,'rhs.dat');
file_optdens=strcat(folder,'optdens.dat');

%% load cellsize  and subcell size
sizecell=load(file_sizecell,'ascii');
sizesubcell=load(file_sizesubcell,'ascii');
ncellpot=size(sizesubcell,1);
rhs=load(file_rhs,'ascii');
optdens=load(file_optdens,'ascii')';


% read matrices
mystiff = load(file_stiff,'ascii');
ia_stiff = mystiff(:,1);
ja_stiff = mystiff(:,2);
coeff_stiff = mystiff(:,3);
mystiff = spconvert(mystiff);
assembler_stiff = int16(load(file_assembler_stiff,'ascii'))';
assembler_stiff=reshape(assembler_stiff,3,3,ncellpot);


B_matrix = load(file_Bmatrix,'ascii');
iaB = B_matrix(:,1);
jaB = B_matrix(:,2);
coeffB = B_matrix(:,3);
B_matrix = spconvert(B_matrix);
BT_matrix = B_matrix';
assemblerB = int16(load(file_assembler_Bmatrix,'ascii'))';



gradx= load(file_gradx,'ascii');
topol = reshape(gradx(:,2),3,ncellpot);
gradx_coeff =reshape(gradx(:,3),3,ncellpot);
gradx= spconvert(gradx);


grady = load(file_grady,'ascii');
grady_coeff =reshape(grady(:,3),3,ncellpot);
grady = spconvert(grady);

%gradpot_fortran = load(file_gradpot,'ascii')';
%gradpot_fortran = gradpot_fortran(1:2,:);


%% define diemensions
ntdens=size(sizecell,1);
npotcell=size(sizesubcell,1);
npot = size(gradx,2);
tdens=ones(ntdens,1);
gfvar=ones(ntdens,1);
pot=ones(npot,1);
inc=zeros(npot+ntdens,1);
norm_grad2=zeros(ntdens,1);
gradpot=zeros(ntdens,2);
broyden_y=zeros(ntdens,max_nnlin);
broyden_s=zeros(ntdens,max_nnlin+1);
broyden_z=zeros(ntdens,max_nnlin);

x=1:ntdens;
y=1:ntdens;
[X,Y]=meshgrid(x,y);
    


[r,idir]=max(rhs);
rhs(idir) = 0.0;


stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);%,coeff_stiff);


stiff(:,idir) = zeros(size(stiff,1),1);
stiff(idir,:) = zeros(1,size(stiff,1));
stiff(idir,idir) = 1.0; 

OPTS.type='ict';
OPTS.droptol=1e-4;
delta=1.e-3;
matrix_for_prec = stiff+delta*speye(npot,npot);

L_stiff= ichol(matrix_for_prec);
[pot,flag,res,iterA,vresA] =  pcg(stiff,rhs,tol_linsol,imax,L_stiff,L_stiff');
fprintf('%3d -  ITER %d RESINI  %12.2e res %12.2e trueres  %12.2e \n',...
    flag, iterA, vresA(1), vresA(end),res)


%pot=stiff\rhs;


gradpot=[(gradx*pot)';(grady*pot)'];
norm_grad2=build_normgrad2(gradpot);
        

%B = build_matrixB( assemblerB,iaB, jaB, gradpot, gradx_coeff, grady_coeff, sizesubcell,coeffB);


time =0;
timestep=0;
steady = 0;
while ( ( not(steady ) && (timestep < max_timestep) ))
    % store before update
    tdens_old = tdens;
    pot_old   = pot;
    gfvar_old = gfvar;
    
    %% UPDATE tdens gfvar and pot
    fprintf('BEGIN UPDATE=%d \n', timestep)
    if ( id_scheme == 1) 
        %% Explicit Euler
        norm_grad2=build_normgrad2(gradpot);
                
        increment=tdens.^beta.*(norm_grad2-1);
        
        tdens = tdens + deltat * increment;
        tdens=max(min_tdens,tdens);
                        
                
        stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);
        stiff(:,idir) = zeros(size(stiff,1),1);
        stiff(idir,:) = zeros(1,size(stiff,1));
        stiff(idir,idir) = 1.0;
        %pot = stiff\rhs;
        
        OPTS.type='ict';
        OPTS.droptol=1e-4;
        L_stiff= ichol(stiff);
        [pot,flag,res,iterA,vresA] =  pcg(stiff,rhs,tol_linsol,imax,L_stiff,L_stiff');
        fprintf('%3d -  ITER %d RESINI  %12.2e res %12.2e trueres  %12.2e \n',...
            flag, iterA, vresA(1), vresA(end),res)      
        fprintf('   res=%16.8e \n', res)
    end
    
    
    if ( id_scheme == 3 )
        %% Explicit Euler
        norm_grad2=build_normgrad2(gradpot);
        gfvar = sqrt(tdens);
        
        increment=gfvar.*(norm_grad2-1);
        
        gfvar = gfvar + deltat * increment;
                
        tdens = gfvar.^2;
        
        tdens=max(min_tdens,tdens);
        stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);
        stiff(:,idir) = zeros(size(stiff,1),1);
        stiff(idir,:) = zeros(1,size(stiff,1));
        stiff(idir,idir) = 1.0;
        
        OPTS.type='ict';
        OPTS.droptol=1e-4;
        L_stiff= ichol(stiff);
        [pot,flag,res,iterA,vresA] =  pcg(stiff,rhs,tol_linsol,imax,L_stiff,L_stiff');
        fprintf('%3d -  ITER %d RESINI  %12.2e res %12.2e trueres  %12.2e \n',...
            flag, iterA, vresA(1), vresA(end),res)
        %pot = stiff \ rhs;
        gradpot=[(gradx*pot)';(grady*pot)'];
        fprintf('   res=%16.8e \n', res)
    end
    
    if ( id_scheme ==2)
        %% Implicit scheme
        nnlin = 0;
        converged = 0;
        while ( not(converged) && nnlin <=max_nnlin )
            %% compute grad quanties
            gradpot=[(gradx*pot)';(grady*pot)'];
            norm_grad2 = build_normgrad2(gradpot);
                        
            %% assembly jacobian component
            % stiffness matrix
            stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);
                        
            % B matrix
            matrix_B =  build_matrixB( assemblerB,iaB, jaB, gradpot, gradx_coeff, grady_coeff, sizesubcell,coeffB);
            
            % matrix D1 ( in block 2,1)
            coeff_matrix_D1 = 2.*tdens./sizecell;
            matrix_D1=diag(coeff_matrix_D1);
            
            
            % matrix D ( in block 2,2)
            coeff_matrix_D = -1.0/deltat+(norm_grad2 -1);
            matrix_D=diag(coeff_matrix_D);
            
            %% assembly rhs newton
            fpot   = stiff*pot-rhs;
            ftdens = -(tdens-tdens_old)/deltat + tdens.*(norm_grad2-1);
            rhs_newton=-[fpot;ftdens];
            
            
            %% solver newton system
            inc = invert_jacobian(method,tol_linsol, stiff,matrix_B, coeff_matrix_D1, coeff_matrix_D,  rhs_newton ,idir,deltat);
            
            % evaluate
            fprintf('%i : inc=%8.3e rhs=%8.3e| incpot=%8.3e inctd=%8.3e \n', nnlin, norm(inc),norm(rhs_newton) ,norm(inc(1:npot)),norm(inc(npot+1:npot+ntdens)))
            
            nnlin = nnlin +1;
            if ( min ( norm(inc), norm(rhs_newton) ) < tol_nnlin )
                    converged = 1;
            end
            pot   = pot   + inc(1:npot);
            tdens = tdens + inc(npot+1:npot+ntdens);
            tdens=max(min_tdens,tdens);
                
        end
    end
    
    if ( id_scheme == 4)
        %% Implicit scheme
        nnlin = 0;
        converged = 0;
        deltat=1.05*deltat;
        

        norm_grad2 = build_normgrad2(gradpot);

        
        
        % set newton function
        fnewton_tdens = tdens_newton(tdens,tdens_old,deltat,norm_grad2);
        
        
        % define B0
        coeff_matrix_D = -1.0/deltat+(norm_grad2 -1);
        

        matrix_D=diag(coeff_matrix_D);
        % J = D - diag(tdens)B'A^{-1} B ~= D - diag( tdens.*(norm_grad2)=B0
        approx_newton=coeff_matrix_D - tdens.*(norm_grad2);
        size(approx_newton)
        fprintf('%i : min=%8.3e max=%8.3e\n', nnlin, min(norm_grad2(:)),max(norm_grad2(:)))
        fprintf('%i : min=%16.6e max=%16.6e\n', nnlin, min(approx_newton(:)),max(approx_newton(:)))

        
        % H0= B0^{-1}
        inv_approx_newton = diag(1.0./approx_newton);
             
        % compute s_{1} = H0^{-1} F(newton)
        nnlin = 1; 
        broyden_s(:,nnlin)=-inv_approx_newton*(fnewton_tdens);
                      
        while ( not(converged) && nnlin <=max_nnlin )
            tdens_prev=tdens;
            % update tdens wiht s
            tdens=tdens+broyden_s(:,nnlin);
            tdens=max(min_tdens,tdens);
            if (norm(tdens-tdens_prev)< tol_nnlin)
                converged=1;
            end
            fprintf('%i : inc=%8.3e rhs=%8.3e\n', nnlin, norm(broyden_s(:,nnlin)),norm(fnewton_tdens))
            
            %% assembly jacobian component
            % stiffness matrix
            stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);
            
            
            %OPTS.type='ict';
            %OPTS.droptol=1e-4;
            %L_stiff= ichol(stiff);
            %[pot,flag,res,iterA,vresA] =  pcg(stiff,rhs,tol_linsol,imax,L_stiff,L_stiff');
            %fprintf('%3d -  ITER %d RESINI  %12.2e res %12.2e trueres  %12.2e \n',...
            %    flag, iterA, vresA(1), vresA(end),res)
            %fprintf('   res=%16.8e \n', res)
            
            % compute potential gradient and norm_grad2
            pot=stiff\rhs;
            gradpot=[(gradx*pot)';(grady*pot)'];
            norm_grad2 = build_normgrad2(gradpot);
              
            if (converged)
                break
            end
           
            %% compute new update
            %
            fnewton_old   = fnewton_tdens;
            fnewton_tdens = tdens_newton(tdens,tdens_old,deltat,norm_grad2);
            
            % defined y_{j} = F(x_{j}) - F(x_{j-1})
            broyden_y(:,nnlin)  = fnewton_tdens-fnewton_old;
            % compute z_{j} = H_{j-1} y_{j}
            
            broyden_z(:,nnlin ) = broyden(inv_approx_newton, nnlin-1, broyden_s, broyden_z, broyden_y(:,nnlin));
            nnlin = nnlin + 1;
            
            % compute s_{j+1}=(s_{j} - z_{j}) | s_{j} |^2/(s_{j}'z_{j)
            alpha = (broyden_s(:,nnlin-1)'*broyden_s(:,nnlin-1))/(broyden_z(:,nnlin-1)'*broyden_s(:,nnlin-1));
            broyden_s(:,nnlin)  = (broyden_s(:,nnlin-1)-broyden_z(:,nnlin-1)) * alpha;
            
        end
        
    end
    
    
    gradpot=[(gradx*pot)';(grady*pot)'];
    fprintf('END UPDATE=%d \n', timestep)
    
    
    
    
    
        
    
    
        
    

        
    var_tdens = norm ( ( tdens-tdens_old ) .* sizecell) / (deltat * norm ( tdens_old  .* sizecell) );
    err_tdens = norm ( ( tdens-optdens ) .* sizecell) / (norm ( optdens  .* sizecell) );
    
    timestep=timestep+1;
    time=time+deltat;
    steady = (var_tdens < tol_stop);
    
    disp('INFO')
    fprintf('   iter = %d  time=%6.2e time=%6.2e \n' , timestep, time,deltat)
    fprintf('   var=%6.2e err=%6.2e \n', var_tdens,err_tdens)
    fprintf('   tdens min=%6.2e max=%6.2e \n', min(tdens),max(tdens))


    disp(' ')
    
    if ( steady && study_schur) 
        % assembly stiff
        stiff = build_stiff( ia_stiff, ja_stiff, assembler_stiff,tdens, sizesubcell,gradx_coeff, grady_coeff,idir);
        stiff(:,idir) = zeros(size(stiff,1),1);
        stiff(idir,:) = zeros(1,size(stiff,1));
        stiff(idir,idir) = 1.0;
        
        
        % B matrix
        matrix_B =  build_matrixB( assemblerB,iaB, jaB, gradpot, gradx_coeff, grady_coeff, sizesubcell,coeffB);
        %[schurDA,BW]=schur_d_a(stiff, matrix_B, matrix_D, matrix_D1 );
        if ( id_scheme == 1 || id_scheme == 2)
            % matrix D1 ( in block 2,1)
            coeff_matrix_D1 = 2.*tdens./sizecell;
            matrix_D1=diag(coeff_matrix_D1);
                        
            % matrix D ( in block 2,2)
            coeff_matrix_D = -1.0/deltat+(norm_grad2 -1);
            matrix_D=diag(coeff_matrix_D);
            
            
            W=stiff\(matrix_B');
            CW=matrix_D1*matrix_B * W;
            schur_complement = matrix_D-CW;           
        elseif (id_scheme ==3)
            % matrix D1 ( in block 2,1)
            coeff_matrix_D1 = 2.*tdens./sizecell;
            matrix_D1=diag(coeff_matrix_D1);
                        
            % matrix D ( in block 2,2)
            coeff_matrix_D = -1.0/deltat+gfvar.*(norm_grad2 -1);
            matrix_D=diag(coeff_matrix_D);
            Btilde=diag(gfvar) * matrix_B;
            
            W=stiff\(Btilde');
            CW=Btilde * W;
            schur_complement = matrix_D-CW; 
        end
        h=imagesc(schur_complement);
        colorbar
        saveas(h,sprintf('FIG%d.png',timestep))
        
        
        diag_CW=diag(CW);
        fname='diagonal.dat';
        path=[folder,'output/',fname];
        write2td(path,diag_CW)
        
        fname='normgrad.dat';
        path=[folder,'output/',fname];
        write2td(path,norm_grad2)
        
        fname='tdens.dat';
        path=[folder,'output/',fname];
        write2td(path,tdens)
        
        fname='pot.dat';
        path=[folder,'output/',fname];
        write2td(path,pot)
        
        
        for i=1:ntdens;
            str_timestep = sprintf('%04d',timestep);
            str_cell=sprintf('%04d',i);
            fname=['w',str_timestep,'_',str_cell,'.dat']
            path=[folder,'output/',fname];
            write2td(path,W(:,i))
        end
        
        
        for i=1:ntdens;
            str_timestep = sprintf('%04d',timestep);
            str_cell=sprintf('%04d',i);
            fname=['ori_bw',str_timestep,'_',str_cell,'.dat']
            path=[folder,'output/',fname];
            write2td(path,CW(:,i)/sizecell(i))
        end
        
        
    end  
end

