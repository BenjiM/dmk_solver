function [ inc] = invert_jacobian( method, tol_linsol, stiff,matrix_B, matrix_D1_coeff, matrix_D_coeff,  rhs_newton, idir , deltat)
%
% solve saddle point problem
% [ S    B'] x = f 
% [ D1B  D ] y = g
imax=1000;
npot=size(stiff,1);
ntdens=size(matrix_B,1);
matrix_D = diag( matrix_D_coeff );
matrix_D1= diag(matrix_D1_coeff);
inc=zeros(npot+ntdens,1);

if ( strcmp(method,'reduced'));
    inv_matrix_D_coeff = 1.0./matrix_D_coeff;
    inv_matrix_D = diag( inv_matrix_D_coeff );

    %% rhs=f-B'D^{-1} g
    rhs_reduced =  rhs_newton(1:npot)  - matrix_B'*inv_matrix_D*rhs_newton(npot+1:npot+ntdens) ;
    %% jac_r = A - B' D1 D^{-1} B
    Schur_A_D = sparse(stiff - matrix_B'*inv_matrix_D*matrix_D1*matrix_B);
    Schur_A_D  (:,idir) = zeros(npot,1);
    Schur_A_D  (idir,:) = zeros(1,npot);
    Schur_A_D  (idir,idir) = 1.0;

    
    OPTS.type='ict';
    OPTS.droptol=1e-4; 
    LSchur= ichol(Schur_A_D );
    [inc(1:npot),flag,res,iterA,vresA] =  pcg(Schur_A_D ,rhs_reduced,tol_linsol,imax,LSchur,LSchur');
    fprintf('   %1d %d %12.2e %12.2e %12.2e \n',...
        flag, iterA, vresA(1), vresA(end),res)
    
    %inc(1:npot) = Schur_A_D  \ rhs_reduced;

    %% y = D^{-1} g - D1 * B * x
    inc(npot+1:npot+ntdens) = inv_matrix_D*(rhs_newton(npot+1:npot+ntdens) - matrix_D1*matrix_B*inc(1:npot));

end
if ( strcmp(method,'augmented'));
    % define matrix W_mat MP_mat, S_mat
    gamma=1.0;
    % define MP
    diag_MP = ones(npot,1);
    MP_mat = diag(diag_MP); 
    %define W
    diag_W = 1.0+gamma.*matrix_D_coeff;
    W_mat = diag(diag_W);
    inv_W_mat = diag(1./diag_W);
    %define S
    S_mat = - 1/gamma * W_mat;
    
    
    
    % define augmented jacobian
    Agamma=sparse(stiff+gamma*matrix_B'*inv_W_mat*matrix_D1*matrix_B);
    BTgamma=sparse(matrix_B'-gamma*matrix_B'*inv_W_mat*matrix_D1*matrix_B);
    aug_jac = [ Agamma, Bgamma'; matrix_D1*matrix_B, matrix_D ];
    Agamma(:,idir) = zeros(npot,1);
    Agamma(idir,:) = zeros(1,npot);
    Agamma(idir,idir) = 1.0;
            
    
    % define rhs systems
    rhs_aug=rhs_newton;
    rhs_aug(1:npot) = rhs_aug(1:npot) + ...
        gamma*matrix_D1*matrix_B*inv_W_mat*rhs_aug(1+npot:npot+ntdens);
    
    
    [inc,flag,res,iterA,vresA] =  bicgstab(aug_jac ,rhs_newton_gamma,tol_linsol,imax,@(x) augemented_prec(x,A_gamma,B_matrix,S_mat));
    fprintf('   %1d %d %12.2e %12.2e %12.2e \n',...
        flag, iterA, vresA(1), vresA(end),res)
    
end


if ( strcmp(method,'backslash')    )
    method
    
    %% solver newton system
    stiff(:,idir) = zeros(size(stiff,1),1);
    stiff(idir,:) = zeros(1,size(stiff,1));
    stiff(idir,idir) = 1.0;
    Jacobian = [ stiff, matrix_B'; matrix_D1*matrix_B, matrix_D ];

    inc = Jacobian\rhs_newton;
end

end

