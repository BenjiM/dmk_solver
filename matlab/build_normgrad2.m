function [ nrm_grad2 ] = build_normgrad2( gradpot)
%UNTITLED5 Summary of this function goes here
%   Detailed explanation goes here
ncell=size(gradpot,2);
nrm_grad2 = zeros(ncell/4,1);
for i=1:ncell
    j= idivide(int32(i-1),4)+1;
    %fprintf('%d %d\n', i,j)
    nrm_grad2(j)=nrm_grad2(j) + gradpot(1,i)^2 + gradpot(2,i)^2;
end
nrm_grad2=nrm_grad2/4;

end

