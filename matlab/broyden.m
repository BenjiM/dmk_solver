function [ vec_out] = broyden( invB0, newton_iteration,s, z, vec_in )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
vec_out=invB0*vec_in;
for k=1:newton_iteration;
    alpha=s(:,k)'*vec_out / ((s(:,k))'*z(:,k))  ;
    %fprintf('k=%d alpha=%8.3e\n ',k,alpha) 
    vec_out=vec_out + alpha *( s(:,k) -z(:,k) ) ;
end

end

