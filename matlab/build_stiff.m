function [ stiff] = build_stiff( ia, ja, assembler,tdens, sizesubcell,gradx_coeff, grady_coeff,idir)%,coeff_stiff)
%UNTITLED6 Summary of this function goes here
%   Detailed explanation goes here
nsubcell = size(sizesubcell,1);
nterm    = size(ia,1);
tdens_prj=zeros(nsubcell,1);
coeff=zeros(nterm,1);
for isubcell=1:nsubcell;
    icell = idivide(int32(isubcell-1),4)+1;
    tdens_prj(isubcell) = tdens(icell)*sizesubcell(isubcell);
end

for isubcell=1:nsubcell;
    for iloc = 1:3;
        gx_iloc=gradx_coeff(iloc,isubcell);
        gy_iloc=grady_coeff(iloc,isubcell);
        for jloc = 1:3;
            iterm = assembler(jloc,iloc,isubcell);
            gx_jloc=gradx_coeff(jloc,isubcell);
            gy_jloc=grady_coeff(jloc,isubcell);
            coeff(iterm) = coeff(iterm) + tdens_prj(isubcell) * ( gx_iloc * gx_jloc + gy_iloc * gy_jloc);
        end
    end
end
% size(assembler)
% size(ia)
% size(coeff)
% size(coeff_stiff)
% norm(coeff-coeff_stiff)
stiff = sparse(ia,ja,coeff);

if ( idir ~= 0)
    stiff(:,idir) = zeros(size(stiff,1),1);
    stiff(idir,:) = zeros(1,size(stiff,1));
    stiff(idir,idir) = 1.0;
end

