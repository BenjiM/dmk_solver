module DmkInputsData
  use KindDeclaration
  implicit none
  private  
  public :: DmkInputs, DmkInputs_constructor, DmkInputs_destructor
  !>----------------------------------------------------------------
  !> Structure variable containg the time varing quantities 
  !> (pflux, pmass, decay, kappa, rhs_integrated)
  !> and fix in time quanties 
  !> (tdens0) the ODE  
  !> \begin{gather}
  !>    \Div(\Tdens \Grad \Pot) = \Forcing 
  !>    \quad
  !>    -\Tdens \Grad \Pot \cdot \n_{\partial \Domain} = \Bdflux
  !> \\
  !> \Tdens'=|\Tdens \Grad \Pot(\Tdens)|^\Pflux-kappa*decay*\Tdens^{\Pmass}
  !> \\
  !> \Tdens(tzero) = \Tdens0
  !> \end{gather}
  !> It may contains the optional quantities 
  !> forcing, boundary_flux, optdens(fix initialized and used
  !> only if the correspondet input files exis
  !>------------------------------------------------------------------
  type :: DmkInputs
     !> Identifier of ODE
     !> 1 : PP  (Physarum Policephalum dynamic)
     !> 2 : GF  (Gradient Flow dynamic in Tdens variable)
     !> 3 : GF2 (Gradient Flow dynamic in Gfvar variable)
     integer :: id_ode
     !> Number of time frames
     integer :: ntimes
     !> time frames
     real(kind=double) ::  time
     !>-------------------------------------------------------------
     !> Mandatory Input data
     !>-------------------------------------------------------------
     !> Number of variables for Tdens-variables 
     integer :: ntdens
     !> Number of variables for Pot-variables
     integer :: npot
     !> Number of commodities to be transported
     !> Default=1
     integer :: ncommodity=1
     !>--------------------------------------------------------------
     !> True/False flag for existence of optimal potential array
     logical :: dirichlet_exists= .False.
     !> Dirichelet quantities
     !> Number of Dirichlet nodes 
     integer :: ndir=0
     !> Dimension= ndir
     !> Dirichlet node indeces ( in subgrid ) 
     integer, allocatable :: dirichlet_nodes(:)
     !> Dimension = ndir
     !> Dirichlet values at nodes ( in subgrid )
     real(kind=double), allocatable :: dirichlet_values(:)
     !>------------------------------------------------------------
     !> Lift of tdens in elliptic equation
     !> -div(\tdens + lambda_tdens \grad \pot) = rhs ( continouos )
     !> ( A ( diag(\tdens+lambda)  )A    \pot  = rhs ( discrete )
     real(kind=double) :: lambda_tdens=zero
     !> Lasso relaxation 
     !> (stiff(tdens) +lasso*Id)          \pot = rhs ( continouos )
     !> ( A ( diag(\Tdends) A  +lasso Id )\pot = rhs ( discrete )
     real(kind=double) :: lasso=zero
     !>--------------------------------------------------------------
     !> Pflux power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pflux=one
     !> Time decay
     !> Contiains 1 real with pflux power
     real(kind=double) :: decay=one
     !> Pmass power
     !> Contiains 1 real with pflux power
     real(kind=double) :: pmass=one
     !> ODE expoent for norm of grad
     !> pflux : PP  (Physarum Policephalum dynamic)
     !> 2.0   : GF  (Gradient Flow dynamic in Tdens variable)
     real(kind=double) :: pode=two
     !> Laplacian Smoothing parameter
     !> \sigma in the paper
     !> "Laplacian Smoothing Gradient Descent" by
     !> Osher, Stanley et al.
     real(kind=double) :: sigma_laplacian_smoothing=0.0d0
     !>-----------------------------------------------------
     !> Penalization
     !>-----------------------------------------------------
     !> True/False flag for existence penalization
     logical :: penalty_exists = .False.
     !> Scaling factor for penalty
     !> Contiains 1 real with pflux power
     real(kind=double)  :: penalty_factor=zero
     !> Optional weight $\Wpenalty$ of penalty decay
     !> Give by adding penalty 
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     real(kind=double), allocatable :: penalty_weight(:)
     !> Dimension = ntdens
     !> Optional penalty $\geq 0$ given by adding
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     !> in the minimization problem.
     real(kind=double), allocatable :: penalty(:)
     !>-----------------------------------------------------
     !> Dimension = ntdens
     !> Spatial decay 
     !> Contiains ntria-array with spatial decay
     real(kind=double), allocatable :: kappa(:)
     !> Dimension = npot
     !> Rhs in the linear system
     !> arising from equation 
     !> $-\div(\Tdens \Grad \Pot ) = \Forcing$
     !> Completed with proper boundary condition
     real(kind=double), allocatable :: rhs(:)
   contains
     !> Static constructor
     !> (public for type DmkInputs)
     procedure, public, pass :: init => DmkInputs_init
     !> Static destructor
     !> (public for type DmkInputs)
     procedure, public, pass :: kill => DmkInputs_kill
     !> Info procedure
     !> (public for type DmkInputs)
     procedure, public, nopass :: DmkInputs_info
  end type DmkInputs
  
   type, public :: ProblemData
      !>-------------------------------------------------------------
      !> Mandatory Input data
      !>-------------------------------------------------------------
      !> Nmber of variables for Tdens-variables 
      integer :: ntdens
      !> Nmber of variables for Pot-variables
      integer :: npot
      !>-----------------------------------------------------
      !> Reference solution to compute errors
      !>-----------------------------------------------------
      !> True/False flag for existence of optdens array
      logical :: opt_tdens_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_tdens(:)
      !> True/False flag for existence of optdens array
      logical :: opt_pot_exists= .False.
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: opt_pot(:)
      !> Initial Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: tdens0(:)
      !> Reference solution for Tdens
      !> Dimension (ntdens=grid%ncell)
      real(kind=double), allocatable :: pot0(:,:)
   end type ProblemData


contains
  !>------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type OdeInp)
  !> Instantiate (allocate if necessary)
  !> and initialize variables of type OdeInp
  !>
  !> usage:
  !>     call 'var'%init(lun_err,ntdens,npot,set2default,ncommodity)
  !>
  !> where:
  !> \param[in] lun_err              -> integer. Logical unit for error msg.
  !> \param[in] ntdens               -> integer. Dimension of Tdens-variables
  !> \param[in] npot                 -> integer. Dimension of Pot-variables
  !> \param[in] optional,set2defualt -> logical. Flag to set
  !>                                    innput values to defualt = l1-minimizaiton
  !> \param[in] optional, ncommodity -> intger. Number of commodity
  !>                                    Default=1
  !<-------------------------------------------------------------
  subroutine DmkInputs_init(this,&
         lun_err, ntdens,npot,set2default,ncommodity)
      implicit none
      class(DmkInputs),  intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      integer,           intent(in   ) :: ntdens
      integer,           intent(in   ) :: npot
      logical, optional, intent(in   ) :: set2default
      integer, optional, intent(in   ) :: ncommodity

      !local
      logical :: rc
      integer :: res


      this%ntdens = ntdens
      this%npot   = npot
      if (present(ncommodity)) this%ncommodity = ncommodity
      
      

      allocate( &
            this%dirichlet_nodes(this%npot*this%ncommodity),&
            this%dirichlet_values(this%npotthis%ncommodity),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'

      allocate( &
           this%penalty(ntdens),&
           this%penalty_weight(ntdens),&
           this%kappa(ntdens),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'

      allocate( &
           this%rhs(npot*this%ncommodity),&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'allocation error in DmkInputs_init'

      if ( present( set2default) .and.  set2default ) then
         this%time  = zero
         this%ndir  = 0
         this%pflux = one
         this%pmass = one
         this%pode  = two
         this%decay = one
         this%kappa = one
         this%penalty_factor = zero
         this%penalty = zero
         this%penalty_weight = zero
      end if
         
    end subroutine DmkInputs_init

    !>------------------------------------------------------------
    !> Static desconstructor for Pyhton Wrap
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_constructor(this,&
         lun_err, ntdens,npot,set2default)
      implicit none
      type(DmkInputs),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      integer,     intent(in   ) :: ntdens
      integer,     intent(in   ) :: npot
      logical, optional, intent(in ) :: set2default

      call this%init(lun_err,ntdens,npot,set2default)

    end subroutine DmkInputs_constructor

    !>------------------------------------------------------------
    !> Static desconstructor.
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_kill(this,lun_err )
      implicit none
      class(DmkInputs),     intent(inout) :: this
      integer,     intent(in   ) :: lun_err
      !local
      integer ::res

      this%ntdens = 0
      this%npot   = 0
      this%ndir   = 0


      deallocate( &
            this%dirichlet_nodes,&
            this%dirichlet_values,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in DmkInputs_desctructor'

      deallocate( &
           this%penalty,&
           this%penalty_weight,&
           this%kappa,&
           stat=res)
      if(res .ne. 0) write(lun_err,*) 'deallocation error in DmkInputs_desctructor'

      deallocate( &
           this%rhs,&
           stat=res)
      if(res .ne. 0)  write(lun_err,*) 'deallocation error in DmkInputs_desctructor'
    end subroutine DmkInputs_kill

    !>------------------------------------------------------------
    !> Static desconstructor for Pyhton Wrap
    !> (procedure public for type OdeInp)
    !> Free memory
    !>
    !> usage:
    !>     call 'var'%kill(lun_err)
    !>
    !> where:
    !> \param[in] lun_err -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_destructor(this,lun_err )
      implicit none
      type(DmkInputs),  intent(inout) :: this
      integer,          intent(in   ) :: lun_err

      call this%kill(lun_err)
    end subroutine DmkInputs_destructor

    !>------------------------------------------------------------
    !> Information procedure
    !> (procedure public for type DmkInputs)
    !>
    !> usage:
    !>     call 'var'%info(lun_out)
    !>
    !> where:
    !> \param[in] lun_out -> integer. I/O logical unit
    !<-------------------------------------------------------------
    subroutine DmkInputs_info(this,lun_out )
      implicit none
      class(DmkInputs),     intent(inout) :: this
      integer,     intent(in   ) :: lun_out

      write(lun_out,*) &
           'Tdens     dimensions =', this%ntdens , &
           'Potential dimensions =', this%npot
      write(lun_out,*) 'ODE type             =', this%id_ode

    end subroutine DmkInputs_info

   
    
  end module DmkInputsData
