program simple
use KindDeclaration
use Globals
use AbstractGeometry
use TimeInputs  
use DmkControls

type(abs_simplex_mesh) :: grid
type(file) :: fgrid,fforcing
type(TimeData) :: forcing
type(DmkCtrl) :: ctrl
integer :: lun_err=6
integer, allocatable :: topol(:,:)

real(kind=double), allocatable :: forcing_cell(:),tdens(:),pot(:)
logical :: endfile
real(kind=double) :: pflux

call fgrid%init(lun_err,'grid.dat',10,'in')
call fforcing%init(lun_err,'forcing.dat',11,'in')
call grid%read_mesh(lun_err,fgrid)
call forcing%init(lun_err, fforcing, 1,grid%ncell)
call forcing%set(lun_err, fforcing, 0.0d0,endfile)
allocate(forcing_cell(grid%ncell),tdens(grid%ncell),pot(grid%nnode),topol(3,grid%ncell))
topol=grid%topol(1:3,1:grid%ncell)
do i=1,grid%ncell
   forcing_cell(i) =forcing%TDactual(1,i)
end do

pflux=one

ctrl%time_discretization_scheme=1
ctrl%max_time_iterations=10
ctrl%info_update=3
ctrl%info_state=2

ctrl%lun_tdens=10
ctrl%fn_tdens='tdens.dat'
ctrl%id_save_dat=3

tdens=one
pot=zero

call otpdmk(grid%nnodeincell,grid%nnode,grid%ncell,&
     topol,grid%coord,&
     pflux,forcing_cell,&
     tdens,pot,&
     ctrl,info)

end program simple
