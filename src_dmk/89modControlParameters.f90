!> Defines simulation control parameters
module ControlParameters
  use Globals
  use DmkControls
  use StdSparsePrec
  use PreconditionerTuning
  use Eigenv
  use LinearSolver
  implicit none
  private
  type, extends(DmkCtrl), public :: CtrlPrm
     !type(DmkCtrl) :: dmk
     !> Controls for linear systems solution
     !> mainly via Krylov method
     type(input_solver) :: ctrl_solver  
     !> Copy of controls for linear systems solution
     !> mainly via Krylov method
     type(input_solver) :: ctrl_solver_original 
     !> Controls for linear systems solution
     !> involved in the application of 
     !> Augmented Lagrangian Preconditioner
     !> Used for P1P0 discretization
     type(input_solver) :: ctrl_solver_augmented
     !> Controls for the assembly of the preconditioner
     !> used in preconditioner Krylov-based scheme
     type(input_prec) :: ctrl_prec
     !> Controls for the assembly of the preconditioner
     !> used in preconditioner Krylov-based scheme
     type(input_prec) :: ctrl_prec_augmented
     !> Controls for tuning of Preconditioner
     type(input_tuning) :: ctrl_tuning
     !> Controls for DACG procedure
     !> via PCG algorithm 
     type(input_dacg) :: ctrl_dacg
     !> File where to write tdens data
     type(file) :: file_tdens
     !> File where to write potential
     type(file) :: file_pot
     !> File where to write statistics
     type(file) :: file_statistics
     !>---------------------------------------------------------
     !> Flags to controls DMK discretization
     !>---------------------------------------------------------
     !> Flag for construction of preconditoner
     integer :: build_prec=1
     !> 
     integer :: threshold
   contains
     !> static constructor
     !> (public for type CtrlPrm)
     procedure, public, pass :: init => CtrlPrm_init
     !> static destructor
     !> (public for type CtrlPrm)
     procedure, public, pass :: kill => CtrlPrm_kill
     !> Procedure to generate formats 
     !> based on  iformat, rformat
     !> (public for type CtrlPrm)
     procedure, public, pass :: formatting
     !> Fucntion to build a separtor with title
     !>for statistic file
     !> (public for type CtrlPrm)
     procedure, public, pass :: separator
     !> Standrd info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_info_update 
     !> Standrd info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_info_tdpot
     !> 
     procedure, public, pass :: write_data2file
  end type CtrlPrm

contains
  
  

  !>-------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type CtrlPrm)
  !> Instantiate (allocate if necessary)
  !> and initialize (by also reading from input file)
  !> variable of type CtrlPrm
  !>
  !> usage:
  !>     call 'var'%init(IOfiles, ctrl_global)
  !>
  !> where:
  !> \param[in] ctrl_global -> type(CtrlPrm). Global controls
  !<-------------------------------------------------------------
  subroutine CtrlPrm_init(this, lun_err, basic_ctrl)
    use Globals
    implicit none
    class(CtrlPrm),  intent(inout) :: this
    integer,        intent(in   ) :: lun_err
    type(DmkCtrl),  intent(in   ) :: basic_ctrl
    !
    
    this%DmkCtrl = basic_ctrl

    call this%ctrl_solver%init(&
         lun_err,&
         scheme=this%krylov_scheme,&
         lun_err=this%lun_err,&
         lun_out=this%lun_out,&
         iexit=this%iexit,&
         imax=this%imax,&
         iprt=this%iprt,&
         isol=this%isol,&
         tol_sol=this%tolerance_linear_solver,&
         iort=this%iort)

    
  end subroutine CtrlPrm_init

  



  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type CtrlPrm)
  !> only scalars, does nothing
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine CtrlPrm_kill(this)
    implicit none
    class(CtrlPrm), intent(inout) :: this
    
    call this%ctrl_solver%kill()   
    call this%ctrl_prec%kill()
    call this%ctrl_tuning%kill()
    call this%ctrl_dacg%kill()
    
  end subroutine CtrlPrm_kill

 function formatting(this,compressed,style) result (out_format)
    use Globals
    implicit none
    class(CtrlPrm),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: compressed
    integer, optional, intent(in   ) :: style
    character(len=256) :: out_format
    !local
    integer :: i,length
    character(len=1) :: singleton
    
    out_format='('  
    
    

    length=len(compressed)


    if ( .not. present(style) .or. (style .eq. 1) ) then
       do i=1, length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat_info)
          case('r')
             out_format=etb(etb(out_format)//this%rformat_info)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    if ( present(style) .and. (style .eq. 2) ) then
       do i=1,length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat)
          case('r')
             out_format=etb(etb(out_format)//this%rformat)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    out_format=etb(etb(out_format)//')')

  end function formatting

  function separator(this,title) result (outstring)
    use Globals
    implicit none
    class(CtrlPrm),             intent(in   ) :: this
    character(len=*), optional, intent(in   ) :: title
    integer, parameter                        :: width=71
    integer, parameter                        :: start=10
    character(len=width) :: outstring
    !local 
    integer :: i,lentitle
    character(len=start) :: strstart
    character(len=width-start-2) :: strend
    character(len=1) :: symbol
    
     symbol=this%separator_char

    
    if ( present(title)) then
       lentitle=len(etb(title))
       do i=1,start
          write(strstart(i:i),'(a)') symbol
       end do

       do i=1,width-start-2-lentitle
          write(strend(i:i),'(a)') symbol
       end do
       outstring=strstart//' '//etb(title)//' '//strend
    else
       do i=1,width
          write(outstring(i:i),'(a)') symbol
       end do
    end if    
       
  end function separator
  

  subroutine print_info_update(this,&
       flag_info,&
       info,&
       time_iteration,&
       nrestart)
    use Globals    
    implicit none
    class(CtrlPrm),    intent(in) :: this
    character(len=*),  intent(in) :: flag_info
    integer,           intent(in) :: info
    integer,           intent(in) :: time_iteration
    integer,           intent(in) :: nrestart
    
    
    !local
    character(len=77) :: result_update
    character(len=30) :: flag
    character(len=77) :: msg,msg_out
    character(len=256) :: out_format,str
    character(len=77) :: method
    

    flag=etb(flag_info) 

    select case (flag)
    case ( 'before_inside') 
       if (  nrestart .eq. 0) then
          if ((this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) ' '
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) ' ' 
          out_format=this%formatting('aiar')
          write(str,out_format) 'UPDATE',time_iteration,' | TIME STEP = ', this%deltat
          write(msg,'(a)') this%separator(str) 
       else
          out_format=this%formatting('aiarai')
          write(str,out_format) 'UPDATE',time_iteration,'| TIME STEP = ', this%deltat,&
               ' RESTART #', nrestart
          write(msg,'(a)') this%separator(str)              
       end if
       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,'(a)') etb(msg)
       if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,'(a)') etb(msg)

    case ( 'after_inside') 
       if ( info .eq. 0) then 
          write(msg,*) this%separator('UPDATE SUCCEED ')
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
          if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,*) etb(msg)
       else
          if ( nrestart + 1 .eq. this%max_restart_update) then
             write(msg,*) this%separator('UPDATE FAILED ')
             if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
             if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) etb(msg)
             write(this%lun_err,*) etb(msg)
          end if
       end if

!!$    case ('before_ouside') 
!!$       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) ' '
!!$       if (this%lun_statistics .gt. 1 ) write(this%lun_out,*) ' '
!!$       out_format=this%formatting('ai')
!!$       write(msg,out_format) &
!!$            'UPDATE # ',time_iteration
!!$       write(msg_out,*) etb(this%separator(msg))
!!$       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$       if (this%lun_statistics .gt. 1 ) write(this%lun_out,*) etb(msg)
!!$    case ('after_ouside')
!!$       if ( info .eq. 0) then 
!!$          write(msg,*) this%separator('UPDATE SUCCEED ')
!!$          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$          if (this%lun_out .gt. 0 ) write(this%lun_out,*) etb(msg)
!!$       else
!!$          write(msg,*) this%separator('UPDATE FAILED ')
!!$          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$          if (this%lun_out .gt. 0 ) write(this%lun_out,*) etb(msg)
!!$          write(this%lun_err,*) etb(msg)
!!$       end if
       
    end select


  end subroutine print_info_update


  subroutine print_info_tdpot(this,&
       tdpot)
    use Globals 
    use TdensPotentialSystem
    implicit none
     class(CtrlPrm),    intent(in) :: this
     type(tdpotsys),    intent(in) :: tdpot
     !local
     character(len=256) :: msg

     
     if (this%info_state .gt. 0) then
        if ( this%lun_out > 0 ) then 
           call tdpot%info_time_evolution(this%lun_out)
        end if
        if ( this%lun_statistics > 0 ) then 
           call tdpot%info_time_evolution(this%lun_statistics)
        end if
     end if
     

    
!!$     if ((this%info_state .gt. 0) .and. (this%lun_out >0) ) then
!!$        if ( this%info_state .ge. 1 ) then
!!$           write(this%lun_out,*) ' '
!!$           msg=this%separator('INFO TIME EVOLUTION')
!!$           write(this%lun_out,'(a)') etb(msg)
!!$        end if
!!$        call tdpot%info_time_evolution(this%lun_out)
!!$        if ( this%info_state .ge. 2 ) then
!!$           msg=this%separator('INFO STATE')
!!$           write(this%lun_out,'(a)') etb(msg)
!!$           call tdpot%info_functional(this%lun_out)
!!$           msg=this%separator()
!!$           write(this%lun_out,'(a)') etb(msg)
!!$        end if
!!$     end if
!!$
!!$    if ((this%info_state .gt. 0) .and. (this%lun_statistics >0) ) then
!!$       write(this%lun_statistics,*) ' '
!!$       write(this%lun_statistics,*) this%separator('INFO TIME EVOLUTION')
!!$       call tdpot%info_time_evolution(this%lun_statistics)
!!$       write(this%lun_statistics,*) this%separator('INFO STATE')
!!$       call tdpot%info_functional(this%lun_statistics)
!!$    end if
  end subroutine print_info_tdpot
   

  subroutine write_data2file(ctrl,flag,tdpot)
    use TdensPotentialSystem
    use TimeInputs
    implicit none
    class(CtrlPrm), intent(inout) :: ctrl
    integer,        intent(in   ) :: flag
    type(tdpotsys), intent(inout) :: tdpot
    ! local
    logical :: save_data
    
    if (flag .eq. 1) then
       ! 
       ! write head of timedata file
       ! 
       if ( ctrl%id_save_dat >0 ) then
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'head',&
                  tdpot%time,tdpot%ntdens, tdpot%tdens, &
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             call writearray2file(ctrl%lun_err, 'head',&
                  tdpot%time, tdpot%npot, tdpot%pot, &
                  ctrl%lun_pot, ctrl%fn_pot)
          end if
          
       end if
    else
       !
       ! save at itermediate time 
       !
       call should_I_save(ctrl,tdpot%steady_state,&
            tdpot%time_iteration,&
            tdpot%int_before,&
            tdpot%system_variation,&
            save_data )

       if ( save_data  ) then
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'body',&
                  tdpot%time,&
                  tdpot%ntdens, tdpot%tdens,&
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             call writearray2file(ctrl%lun_err, 'body',&
                  tdpot%time,&
                  tdpot%npot, tdpot%pot, &
                  ctrl%lun_pot, ctrl%fn_pot)
          end if
       end if

       if ( flag .eq. 0) then
          !
          ! optionally close logical unit for tdens and potential
          !
          if (ctrl%lun_statistics>0) then
             call ctrl%file_statistics%kill(ctrl%lun_err)
          end if
          if  ( ctrl%id_save_dat >0 ) then
             if (ctrl%lun_tdens>0) then
                call writearray2file(ctrl%lun_err, 'tail',&
                     tdpot%time, tdpot%ntdens, tdpot%tdens, &
                     ctrl%lun_tdens, ctrl%fn_tdens)
                call ctrl%file_tdens%kill(ctrl%lun_err)
             end if
             if (ctrl%lun_pot>0) then
                call writearray2file(ctrl%lun_err, 'tail',&
                     tdpot%time, tdpot%npot, tdpot%pot, &
                     ctrl%lun_pot, ctrl%fn_pot)
                call ctrl%file_pot%kill(ctrl%lun_err)
             end if
          end if
       end if
    end if

  contains
    subroutine should_I_save(this,&
         steady_state,&
         itemp,&
         int_before,&
         current_var,&
         save_test)
      use KindDeclaration
      implicit none
      type(CtrlPrm),    intent(in   ) :: this    
      logical,           intent(in   ) :: steady_state
      integer,           intent(in   ) :: itemp
      integer,           intent(inout) :: int_before
      real(kind=double), intent(in   ) :: current_var
      logical,intent(inout) :: save_test

      !local 
      integer :: int_now

      save_test = .false.

      select case ( this%id_save_dat ) 
      case (1)
         !
         ! save each time step
         !
         save_test = .true.
      case (2)
         !
         ! save with give frequency and the last one
         !
         if ( (mod(itemp,this%freq_dat) == 0) .or. ( steady_state) ) then
            save_test = .true.
         end if
      case (3)
         !
         ! save when variation change digit the last one
         !
         if ( ( itemp .eq. 0) ) then 
            save_test = .true.
         else
            int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
            if ( (int_now /= int_before) .or. ( steady_state ) ) then
               int_before=int_now
               save_test = .true.
            end if
         end if
      case (4)
         !
         ! save only last
         !
         if ( steady_state ) then
            save_test = .true.
         end if
      end select
      
    end subroutine should_I_save
  end subroutine write_data2file
    

       

          
       
!!$  subroutine reset_linear_solver_controls(this,&
!!$       info_solver,&
!!$       total_iterations,iter_media,&
!!$       itemp)
!!$    use Globals    
!!$    implicit none
!!$    class(ThisPrm),      intent(inout) :: this
!!$    type(output_pcg),    intent(in   ) :: info_pcg
!!$    integer,             intent(in   ) :: total_iterations
!!$    integer,             intent(in   ) :: iter_media
!!$    integer,             intent(in   ) :: itemp    
!!$    !local 
!!$    integer :: threshold
!!$
!!$    if ( ( this%id_buffer_prec .eq. 1) .or. &
!!$         ( this%id_buffer_prec .eq. 3) )  then 
!!$       ! Reference number of iterations = average iteration number
!!$       this%ref_iter = total_iterations  / itemp
!!$    else if ( this%id_buffer_prec .eq. 2) then 
!!$       ! Fixed by user reference number of iter
!!$       this%ref_iter = this%ref_iter
!!$    end if
!!$
!!$    threshold = min (this%ref_iter, iter_media)
!!$
!!$    build_prec              = 1 
!!$
!!$    ! If the preconditioner is not built at each iteration
!!$    if ( this%id_buffer_prec .ne. 0 ) then
!!$       ! If the prec. was calculated in the last solution of
!!$       ! a linear sysmat
!!$       ! update the last number of iterations
!!$       if ( this%elliptic%flag_prec ) then
!!$          this%last_iter_prec = this%iter_media
!!$       end if
!!$       ! Do not calculate the next prec
!!$       this%this_pcg%build_prec = 0
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if ( this%id_buffer_prec .eq. 1) then
!!$          if ( this%iter_media > &
!!$               int(this%iter_growth_factor * this%ref_iter) ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       ! If the number of iterations is bigger
!!$       ! that reference_iteration 
!!$       ! or tdens is varying too fast
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 2) then
!!$          if ( ( this%iter_media > this%ref_iter ) .or. &
!!$               ( this%loc_var_tdens > 1.0d1 )    ) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$
!!$       ! If the number of iterations is bigger
!!$       ! that min(last_nit, ref_iter) * growth_factor
!!$       ! then calculate the prec at next time step
!!$       if (this%id_buffer_prec .eq. 3 ) then
!!$          if ( this%iter_media > int(this%iter_growth_factor * threshold )) then
!!$             this%this_pcg%build_prec = 1
!!$          end if
!!$       end if
!!$       
!!$    end if
!!$
!!$    this%this_pcg%build_defl = 0
!!$
!!$
!!$    if ( this%iter_media > &
!!$         int(this%eigen_growth_factor * this%ref_iter) ) then
!!$       this%this_pcg%build_defl = 1
!!$    else
!!$       this%this_pcg%build_defl = 0
!!$    end if
!!$  end subroutine reset_linear_solver_controls
  
end module ControlParameters
