module DmkDiscretization
  use Globals
  use LinearSolver
  use TdensPotentialSystem
  use DmkInputsData  
  implicit none
  private
  public :: dmkpair, destructor, syncronization, update, variation ,dmk_cycle_reverse_communication
  !>---------------------------------------------------
  !> Abstract type contains the minimal procedure to 
  !> compute DMK time evolution, either in the continuous
  !> and the discrte case.
  !> The 3 fundamental procedures, for which the abstract interface,
  !> is defined are :
  !> 1 - syncronized_tdpot
  !> 2 - udpate_tdpot
  !> 3 - compute_variation_tdpot
  !> There are 2 other procedures with  
  !> the actual implementation. Procedure that may be 
  !> overrriden if requires by a specific procedure.
  !<-------------------------------------------------
  type, abstract, public :: dmkpair
   contains
     !> Abstract procedure to override
     !> Static destructor
     !> (procedure public for type dmkpair)
     procedure(destructor), deferred :: kill
     !> Abstract procedure to be overrided
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type dmkpair)
     procedure(syncronization), deferred :: syncronize_tdpot
     !> Abstract procedure to override
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type dmkpair)
     procedure(update), deferred :: update_tdpot
     !> Abstract procedure to override
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type dmkpair_space_discretization)
     procedure(variation), deferred :: compute_tdpot_variation
     !> Procedure to reset controls 
     !> (time-step, preconditioner construction etc.) 
     !> in case of failure in update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: reset_controls_after_update_failure
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: set_controls_next_update
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type dmkpair_space_discretization)
     procedure, public, pass :: dmk_cycle_reverse_communication
  end type dmkpair

  abstract interface
     !>--------------------------------------------------------------------
     !> Static destructor
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%kill(lun_err)
     !> 
     !> where:
     !> \param[in ] lun_err -> integer. Logical unit for errors message
     !>---------------------------------------------------------------------
     subroutine destructor(this,lun_err)
       use Globals
       import dmkpair
       implicit none
       class(dmkpair), intent(inout) :: this
       integer,       intent(in   ) :: lun_err

     end subroutine destructor

     !>--------------------------------------------------------------------
     !> Procedure for the syncronization od tdens potential variable
     !> 
     !> (public procedure for type dmkpair)
     !> 
     !> usage: call var%syncronization(tdpot,ode_inputs,ctrl,info)
     !> 
     !> where:
     !> \param[inout] tdpot      -> tye(tdpotsys). Tdens-Potentail System
     !> \param[in   ] ode_inputs -> tye(DmkInputs). Input for Ode
     !> \param[in   ] ctrl       -> tye(CtrlPrm). Dmk controls
     !> \param[inout] info       -> integer. Flag for info 
     !>                              0 = Everything is fine
     !>                              1** = Error in linear solver
     !>                              11* = Convergence not achivied
     !>                              12* = Internal error in linear solver
     !>                              2**  = Error in non-linear solver
     !>                              21* = Non-linear convergence not achievied
     !>                              22* = Non-linear solver stoped because was divergening
     !>                              23* = Condition for non-linear solver to
     !>                                     continue failed
     !>                              3**  = Other errors
     !>---------------------------------------------------------------------
     subroutine syncronization( this,&
          tdpot,&
          ode_inputs,&
          ctrl,&
          info)
       use TdensPotentialSystem
       use DmkInputsData
       use ControlParameters
      
       import dmkpair
       implicit none
       class(dmkpair), target,    intent(inout) :: this
       type(tdpotsys),     intent(inout) :: tdpot
       type(DmkInputs),    intent(in   ) :: ode_inputs
       type(CtrlPrm),      intent(in   ) :: ctrl
       integer,            intent(inout) :: info


     end subroutine syncronization

     !>------------------------------------------------
     !> Procedure for computation of next state of system
     !> ( all variables ) given the preovius one
     !> ( private procedure for type dmkpair, used in update)
     !> 
     !> usage: call var%update(lun_err,itemp)
     !> 
     !> where:
     !> \param[inout] tdpot -> type(tdpotsys). Tdens-pot system
     !>                          syncronized at time $t^{k}$
     !>                          to be updated at time $t^{k+1}$
     !> \param[in   ] odein -> type(DmkInputs). Input data
     !>                          at time t^k or t^{k+1}, according
     !>                          time discretization scheme
     !> \param[in   ] ctrl  -> type(CtrlPrm). Controls of DMK
     !>                        evolution (time-step, linear system approach)
     !> \param[in   ] info  -> integer. Flag with 3 digits for errors.
     !>                        If info==0 no erros occurred.
     !>                        First digit from the left describes 
     !>                        the main errors. The remaining digits can be used to
     !>                        mark specific errors. Current dictionary:
     !>                        1**  = Error in linear solver
     !>                         11* = Convergence not achivied
     !>                         12* = Internal error in linear solver
     !>                        2**  = Error in non-linear solver
     !>                         21* = Non-linear convergence not achievied
     !>                         22* = Non-linear solver stoped because was divergening
     !>                         23* = Condition for non-linear solver to
     !>                               continue failed
     !>                        3**  = Other errors
     !<-----------------------------------------------------------------------------
     subroutine update(this,&
          tdpot,&
          ode_inputs,&
          ctrl,&
          info)
       use TdensPotentialSystem
       use ControlParameters
       use DmkInputsData  
       import dmkpair

       implicit none
       class(dmkpair),   target, intent(inout) :: this    
       type(tdpotsys),    intent(inout) :: tdpot
       type(DmkInputs),   intent(in   ) :: ode_inputs
       type(CtrlPrm),     intent(in   ) :: ctrl
       integer,           intent(inout) :: info

     end subroutine update


     !>----------------------------------------------------------------
     !> Function to eval.
     !> $ var(\TdensH^k):=\frac{
     !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
     !>                   }{ 
     !>                     \| \tdens^{k+1} \|_{L2}}  
     !>                   } $
     !> (procedure public for type dmkpair) 
     !> 
     !> usage: call var%err_tdesn()
     !>    
     !> where:
     !> \param  [in ] var       -> type(dmkpair) 
     !> \result [out] var_tdens -> real. Weighted var. of tdens
     !<----------------------------------------------------------------
     function variation(this,tdpot,ctrl) result(var)
       use Globals
       use TdensPotentialSystem
       use ControlParameters
       import dmkpair

       implicit none
       class(dmkpair),    intent(in   ) :: this
       type(tdpotsys),    intent(in   ) :: tdpot
       type(CtrlPrm),     intent(in   ) :: ctrl
       real(kind=double) :: var

     end function variation


  end interface

contains
  
  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine reset_controls_after_update_failure(this,&
       info,&
       ode_inputs,&
       tdpot,&
       ctrl,&
       inputs_time,&
       ask_for_inputs)
    
    use DmkInputsData 
    use TdensPotentialSystem
    use ControlParameters


    implicit none
    class(dmkpair),    intent(in   ) :: this
    integer,           intent(in   ) :: info
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(CtrlPrm),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: inputs_time
    logical,           intent(inout) :: ask_for_inputs
    ! local
    integer :: digit1,digit2,digit3

    digit1=info/100
    digit2=mod(info,100)/10
    digit1=mod(info,10)


    select case ( digit1 )
    case (1)
       ! failure of update due failure in linear system
       ! solution 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    case (2)
       ! failure of non linear solver for update 
       ! (convergence not achivied, diverging residua, etc) 
       ! 
       ctrl%deltat=max(ctrl%deltat_lower_bound,&
            min(ctrl%deltat_upper_bound,ctrl%deltat/ctrl%deltat_expansion_rate))
    end select
       
       


  end subroutine reset_controls_after_update_failure

     !>----------------------------------------------------
     !>
     !<---------------------------------------------------    
     subroutine set_controls_next_update(this,&   
          ode_inputs,&
          tdpot,&
          ctrl)
       use DmkInputsData 
       use TdensPotentialSystem
       use ControlParameters
       implicit none
       class(dmkpair),    intent(in   ) :: this
       type(DmkInputs),   intent(in   ) :: ode_inputs
       type(tdpotsys),    intent(in   ) :: tdpot
       type(CtrlPrm),     intent(inout) :: ctrl

     end subroutine set_controls_next_update


     subroutine dmk_cycle_reverse_communication(&
          this,&
          flag,info,inputs_time,&
          ode_inputs,tdpot,ctrl)
       use Globals
       use DmkInputsData
       use TdensPotentialSystem
       use ControlParameters
       use TimeInputs
       implicit none
       class(dmkpair),    intent(inout) :: this
       integer,           intent(inout) :: flag
       integer,           intent(inout) :: info
       real(kind=double), intent(inout) :: inputs_time
       type(DmkInputs),   intent(in   ) :: ode_inputs
       type(tdpotsys),    intent(inout) :: tdpot
       type(CtrlPrm),     intent(inout) :: ctrl
       !
       logical :: rc
       logical :: save_test
       logical :: ask_for_inputs
       integer :: res
       integer :: lun_err
       integer :: lun_out
       integer :: lun_stat
       character(len=256) :: msg,msg_out

       lun_err  = ctrl%lun_err
       lun_out  = ctrl%lun_out
       lun_stat = ctrl%lun_statistics

       !
       ! ode_inputs assigned
       !
       select case (flag)
       case (1) ! initialization case
          !
          ! optionally open files where to write data
          !
          call ctrl%write_data2file(flag,tdpot)
          !
          ! return and read inputs at time zero
          !
          flag=2
       case( 2 ) 

          if ( tdpot%time_iteration .eq. 0 ) then
             if (ctrl%info_update.ge.1) then
                msg=ctrl%separator('SYNCRONIZATION TDPOT')
                if (ctrl%lun_out>0) write(ctrl%lun_out,'(a)') etb(msg)
                if (ctrl%lun_statistics>0) write(ctrl%lun_statistics,'(a)') etb(msg)
             end if
             call this%syncronize_tdpot(tdpot,ode_inputs,ctrl,info)
             info=0
          else if (tdpot%skip_update .eq. 1) then
             tdpot%skip_update=0
             flag=3
             return
          else
             info=0
             !
             ! Update cycle. 
             ! If it succees then print info, save data and set to next time step
             ! In case of failure, reset controls and ask for inputs 
             ! returning flag=2
             !
             do while ( tdpot%nrestart_update < ctrl%max_restart_update)
                !
                ! update member pot, tdens, gfvar of type tdpotsys
                !
                ! copy before update
                tdpot%tdens_old = tdpot%tdens
                tdpot%gfvar_old = tdpot%gfvar
                tdpot%pot_old   = tdpot%pot
                tdpot%time_old  = tdpot%time
                ! print info 
                info=0
                call ctrl%print_info_update(&
                     'before_inside',&
                     info,&
                     tdpot%time_iteration,&
                     tdpot%nrestart_update)
                !
                ! actual update
                !
                call this%update_tdpot(&
                     tdpot,&
                     ode_inputs,&
                     ctrl,&
                     info)

                call ctrl%print_info_update(&
                     'after_inside',&
                     info,&
                     tdpot%time_iteration,&
                     tdpot%nrestart_update)
                if ( info .eq. 0 ) then
                   !
                   ! exit from update cycle
                   ! print info, save data, set new update
                   !
                   exit
                else
                   !
                   ! update failed
                   !

                   ! restores previous data
                   tdpot%tdens = tdpot%tdens_old 
                   tdpot%gfvar = tdpot%gfvar_old 
                   tdpot%pot   = tdpot%pot_old
                   tdpot%time  = tdpot%time_old

                   ! stop if number max restart update is passed 
                   tdpot%nrestart_update = tdpot%nrestart_update + 1 
                   if (tdpot%nrestart_update .ge. ctrl%max_restart_update) then
                      flag=-1
                      info=-1
                      return 
                   end if

                   ! reset controls. If deltat changes return  
                   ! flag==2 to get the data at the right time 
                   call this%reset_controls_after_update_failure(&
                        info,&
                        ode_inputs,&
                        tdpot,&
                        ctrl,&
                        inputs_time,&
                        ask_for_inputs)

                   if ( ask_for_inputs) then 
                      flag=2
                      return 
                   end if
                end if
             end do

             !
             ! syncronized tdpot and ode_inputs
             ! For example explicit Euler use the Inputs
             ! at t^{k} while at this point tdens-pot are 
             ! at t^{k+1}
             !
             if ( abs(tdpot%time-ode_inputs%time) > small ) then
                flag=2
                tdpot%skip_update = 1
                return
             end if
          end if
          !
          ! return flag out when ode_inputs and tdpot are syncronized 
          !
          flag = 3
       case (3) 
          !
          ! If number of iterations exceeds maximum break 
          ! time cycle setting flag=0 with info=-1
          !
          if ( tdpot%time_iteration .ge. ctrl%max_time_iterations) then
             flag = 0
             info = -1
          end if

          !
          ! Evalutate variation of tdens-potential system
          ! Break time cycle, setting flag=0, if convergence is achieved
          !
          if (  tdpot%time_iteration > 0) then
             tdpot%system_variation = &
                  this%compute_tdpot_variation(tdpot,ctrl)
             if ( tdpot%system_variation <ctrl%tolerance_system_variation) then
                flag=0
             end if
          end if
          !
          ! INFO ON FUNCTIONAL 
          ! info=1 (time variation of system) 
          call ctrl%print_info_tdpot(tdpot)

          !
          ! optional saving procedure
          !
          call ctrl%write_data2file(flag,tdpot)


          !
          ! if flag.ne.0 continue time cycle
          ! setting next update controls
          !
          if ( flag .ne. 0) then
             call this%set_controls_next_update(&
                  ode_inputs,&
                  tdpot,&
                  ctrl)
             tdpot%time_iteration = tdpot%time_iteration + 1
             tdpot%nrestart_update = 0
             flag=2
          end if
       end select


     end subroutine dmk_cycle_reverse_communication
     
     
     

end module DmkDiscretization

