module SpectralP0
  use Globals
  use GaussQuadrature
  use LinearSolver
  use FullMatrix
  use QuadGrid
  use Spectral
  use TdensPotentialSystem
  use DmkInputsData 
  use DmkDiscretization
  implicit none
  private
  type, extends(dmkpair), public :: specp0
     !> Number of degrees of freedom of ntdens
     integer :: ntdens
     !> Number of degrees of freedom of pot
     integer :: npot
     !> Number of degrees of freedom of system tdens + pot
     integer :: nfull
     !> Number of degrees of pot
     integer :: ambient_dimension
     !>-----------------------------------------
     !> Number of terms used in the construction of stiffness matrix
     integer :: nterm
     !> Number of unknown coefficient for elliptic equation
     !> nequ=npot-1 for Neumann BC
     integer :: nequ
     !> Spatial information
     !> Mesh for tdens
     type(quadmesh), pointer :: grid
     !> Dimension(ninter)
     !> Intervals quantities
     !> ----------------------------------------------------------
     !> TODO : replace wiht a
     !> integer :: ninter
     !> integer :: nextremal
     !> dimensio(nextremal)
     !> real(kind=double), allocatable(:) :: interval_extreme
     !> dimensio(ninter)
     !> integer, allocatable(:) :: interval_topol
     !> dimension (dim,ncell)  
     !> integer, allocatable(:) :: cell2interval(:,:)
     !>----------------------------------------------------------
     type(inter), allocatable :: inters(:)
     !>-----------------------------------------
     !> Quadrature variable for integration on cell
     type(gaussq) :: gauss_cell
     !-------------------------------------------------
     !> Spectral variables
     type(spct), pointer :: spec
     !> Scratch array 
     !> Dimension(ncell)
     !> $ \int_{C_r} |\nabla u|^2 } r=1,ncell$
     real(kind=double), allocatable :: int_nrm2grad(:)
     !> Dimension (ncell, spec%nterm ) 
     !> Stiff matrix of cells
     real(kind=double), allocatable :: stiff_matrices(:,:) 
     !> Dimension (spec%nterm) 
     !> Global stiff matrix, lower triangular part
     real(kind=double), allocatable :: stiff(:)
     !> Ellptic system
     type(fullmat) :: stiffness_matrix
     !> Ellptic system
     type(fullmat) :: copy_stiff
     !> Informtion on linear system solution
     type(output_solver) :: info_solver
     !>---------------------------------------------------
     !> Scratch varariable
     !> Dimension (ngrad)
     !> Work array 
     real(kind=double), allocatable :: scr_ngrad(:)
     !> Dimension (ntdens)
     !> Work array 
     real(kind=double), allocatable :: scr_ntdens(:)
     !> Dimension (npot)
     !> Work array 
     real(kind=double), allocatable :: scr_npot(:)
     !> Dimension (npot+ntdens)
     !> Work array 
     integer, allocatable :: scr_integer(:)
   contains
     !> Static constructor 
     !> (procedure public for type specp0)
     procedure, public , pass :: init => init_specp0
     !> Static destructor
     !> (procedure public for type specp0)
     procedure, public , pass :: kill => kill_specp0
     !> Procedure for building member stiff_matrices
     !> (procedure public for type specp0)
     procedure, private , pass :: build_stiffs
     !> Procedure for building stiffness matrix give tdens
     !> (procedure public for type specp0)
     procedure, private , pass :: assembly_stiff
     !> Procedure for building stiffness matrix give tdens
     !> (procedure public for type specp0)
     procedure, private , pass :: build_integrals_bis
     !> Procedure for upate the wholw system
     !> to the next time step
     !> (procedure public for type specp0)
     procedure, public , pass :: syncronize_tdpot
     !> Procedure for building the rhs of tdens-ODE
     !> (procedure public for type specp0)
     procedure, public , pass :: assembly_rhs_ode_tdens
     !> Procedure for building pointwise tdens-increment 
     !> from rhs of tdens-ODE
     !> (procedure public for type specp0)
     procedure, public , pass :: get_increment_tdens
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type specp0)
     procedure, public , pass :: update_tdpot
     !> Procedure for upate the whole system
     !> to the next time step
     !> (procedure public for type specp0)
     procedure, public , pass :: explicit_euler
     !>$\frac{\|\tdens-a(f)\|_{L2}}{\|a(f)\|_{L2}}$
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: compute_tdpot_variation
     !> Subroutine for computation of scalar and integer
     !> functionals like energy, lyapunov, etc
     procedure, public , pass :: compute_functionals
     !> Procedure to reset controls 
     !> (time-step, preconditioner construction etc.) 
     !> in case of failure in update
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: reset_controls_after_update_failure
     !> Procedure to set controls (time-step, preconditioner construction
     !> etc.) for next update
     !> (procedure public for type specp0_space_discretization)
     procedure, public , pass :: set_controls_next_update
  end type specp0

  

  type, public :: odedatas
     !> number of time slot
     integer :: ntime=0
     !> odedata 
     type(dmkinputs), allocatable :: odedata_timeseries(:)
   contains
     !> Static constructor 
     !> (procedure public for type specp0)
     procedure, public , pass :: init => init_odedatas
     !> Static destructor
     !> (procedure public for type specp0)
     procedure, public , pass :: kill => kill_odedatas
  end type odedatas
  
contains
  subroutine init_odedatas(this,&
       lun_err,&
       ntime,ntdens,npot)
    implicit none
    class(odedatas), intent(inout) :: this
    integer,         intent(in   ) :: lun_err
    integer,         intent(in   ) :: ntime
    integer,         intent(in   ) :: ntdens
    integer,         intent(in   ) :: npot
    !local
    logical :: rc
    integer :: res,i
    
    this%ntime = ntime
    allocate(this%odedata_timeseries(this%ntime),stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_odedatas', &
         'member odedata_series  ',res)
    do i =1, ntime
       call this%odedata_timeseries(i)%init(lun_err,ntdens,npot)
    end do
    
  end subroutine init_odedatas

  subroutine kill_odedatas(this,lun_err)
    implicit none
    class(odedatas),     intent(inout) :: this
    integer,     intent(in   ) :: lun_err
    !local
    logical :: rc
    integer :: res,i
    
    do i =1,this%ntime 
       call this%odedata_timeseries(i)%kill(lun_err)
    end do
    deallocate(this%odedata_timeseries,stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'init_odedatas', &
         'member odedata_series  ',res)
    this%ntime = 0
    
  end subroutine kill_odedatas
    


  !>--------------------------------------------------------------------
  !> Static Constructor, given spectral FEM space, mesh made by
  !> squares and 
  !> 
  !>
  !> (public procedure for type specp0)
  !> 
  !> usage: call var%init(spec,grid,ctrl)
  !> 
  !> where:
  !> \param[in ] spec         -> type(spct): Polynomial FEM
  !> \param[in ] grid         -> type(quadmesh): Mesh of squares
  !> \param[in ] ctrl         -> type(CtrlPrm): Controls 
  !>---------------------------------------------------------------------
  subroutine init_specp0(this,&
       spec, grid,ctrl)
    use Globals
    use ControlParameters
    implicit none
    class(specp0),     intent(inout) :: this
    type(spct),     target, intent(in   ) :: spec
    type(quadmesh), target, intent(in   ) :: grid    
    type(CtrlPrm),          intent(in   ) :: ctrl
    !local
    logical :: rc
    integer :: res
    integer :: i
    integer :: lun_err, lun_out
    integer :: nequ, ndeg,nterm, max_time_it
    real(kind=double) :: dnrm2
    real(kind=double),allocatable :: test(:,:) 

    lun_err = ctrl%lun_err
    lun_out = ctrl%lun_out


    ndeg        = spec%ndeg
    nequ        = ( spec%ndeg + 1 )**2 - 1
    nterm       = (nequ+1) * nequ /2


    this%ntdens      = grid%grid%ncell
    this%npot        = ( ndeg + 1 )**2 
    this%nterm       = nterm
    this%nequ        = nequ


    !local grid
    this%grid => grid
      
    this%spec => spec
    !call this%spec(lun_err,ctrl%ndeg)
    
    !
    ! Build intevals
    !
    allocate(this%inters( grid%ninter), stat = res) 
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'system_tdenspot', &
         ' member inters',res)
    write(*,*) 'grid interval', grid%ninter
    write(*,*) 'nterm', this%nterm
    write(*,*) 'ntdens', this%ntdens
    write(*,*) 'npot', this%npot
    write(*,*) 'nequ', this%nequ
    write(*,*) 'ndeg', spec%ndeg
    write(*,*) 'preassembly ', ctrl%preassembly_matrices
        

    
    ! elliptic system variables
    allocate(&
         this%stiff( nterm ),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_specp0', &
         'member  stiff ',res)

    ! 
    allocate(&
         this%int_nrm2grad( this%ntdens ),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_specp0', &
         'member  stiff ',res)


    if ( ctrl%preassembly_matrices == 1 ) then       
       allocate (&
            this%stiff_matrices(this%ntdens,this%nterm),&
            stat=res)
       if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_specp0', &
            ' member stiff matrices',res)
       call this%build_stiffs(lun_err,lun_out)
    end if 

    call this%stiffness_matrix%init(lun_err, nequ, nequ,.True.)

    ! 
    ! work array
    ! 
    allocate(&
         this%scr_npot(this%npot),&
         this%scr_ntdens(this%ntdens),&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'init_specp0', &
         ' type specp0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

  end subroutine init_specp0

  !>--------------------------------------------------------------------
  !> Static destructor
  !> 
  !> (public procedure for type specp0)
  !> 
  !> usage: call var%init(lun_err)
  !> 
  !> where:
  !> \param[in ] lun_err -> integer. Logical unit for errors message
  !>---------------------------------------------------------------------
  subroutine kill_specp0(this,lun_err)
    use Globals
    use ControlParameters
    implicit none
    class(specp0), intent(inout) :: this
    integer,       intent(in) :: lun_err
    !local
    logical :: rc
    integer :: res


    this%ntdens      = 0
    this%npot        = 0
    this%nterm       = 0
    this%nequ        = 0

    this%grid => null()
    this%spec => null()

    deallocate(this%inters, stat = res) 
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_specp0', &
         ' member inters',res)
    
    
    ! elliptic system variables
    deallocate(&
         this%stiff,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_specp0', &
         'member  stiff ',res)

    ! 
    deallocate(&
         this%int_nrm2grad,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_dealloc, 'kill_specp0', &
         'member  stiff ',res)


    if ( allocated(this%stiff_matrices)) then       
       deallocate (&
            this%stiff_matrices,&
            stat=res)
       if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'kill_specp0', &
            ' member stiff matrices',res)
    end if 

    call this%stiffness_matrix%kill(lun_err)

    ! 
    ! work array
    ! 
    deallocate(&
         this%scr_npot,&
         this%scr_ntdens,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'kill_specp0', &
         ' type specp0 member tdens_prj'//&
         ' scr_ntdens scr_npot scr_ngrad sqrt_diag')

  end subroutine kill_specp0


  

  subroutine build_stiffs(this,lun_err,lun_out)
    use Globals
    implicit none
    class(specp0), intent(inout) :: this
    integer,         intent(in   ) :: lun_err, lun_out
    ! local
    logical :: rc
    integer :: res
    integer :: icell ,ncell, nterm, ndeg
    real(kind=double) :: tdens_loc
    real(kind=double) :: ax,bx, ay, by
    real(kind=double), allocatable :: stiff_loc(:)
    real(kind=double), allocatable :: x_CTP(:,:),x_CUP(:,:)
    real(kind=double), allocatable :: y_CTP(:,:),y_CUP(:,:)


    ndeg  = this%spec%ndeg
    nterm = this%spec%nterm
    ncell = this%grid%grid%ncell
    !write(*,*) ndeg, this%nequ, nterm, ncell

    this%stiff=zero
    allocate(stiff_loc( nterm ),&
         x_CTP( ndeg+1, ndeg+1),&
         x_CUP( ndeg+1, ndeg+1),&
         y_CTP( ndeg+1, ndeg+1),&
         y_CUP( ndeg+1, ndeg+1),&         
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
         ' local array stiff_loc x_CTP, x_CUP, y_CTP, y_CUP',res)

    do icell = 1, ncell
       if (mod( icell, 500 ) == 0 ) then
          write(lun_out,*) 'Assembled cells = ',icell 
       end if

       call this%grid%get_interval_from_cell(icell,ax,bx,ay,by)
       call this%spec%eval_CTPCUP2(ndeg,ax,bx,x_CTP,x_CUP) 
       call this%spec%eval_CTPCUP2(ndeg,ay,by,y_CTP,y_CUP)
       

       call build_stiff_cell( nterm, ndeg,&
            stiff_loc,&
            x_CTP,x_CUP,y_CTP,y_CUP, &
            this%spec)
!!$       call build_stiff_test( nterm, ndeg,&
!!$            stiff_loc,&
!!$            x_CTP,x_CUP,y_CTP,y_CUP, &
!!$            this%spec)
       call dcopy(nterm,&
            stiff_loc(1),1,&
            this%stiff_matrices(icell,1),ncell)
!!$       this%stiff_matrices(icell,:) = stiff_loc(:)
       
    end do    
    deallocate(&
         stiff_loc,&
         x_CTP,&
         x_CUP,&
         y_CTP,&
         y_CUP,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
         ' local array stiff_loc ',res)
    contains
      

  end subroutine build_stiffs

  subroutine build_integrals_bis(this,lun_err,lun_out,pot,integrals)
    use Globals
    implicit none
    class(specp0), intent(inout) :: this
    integer,         intent(in   ) :: lun_err, lun_out
    real(kind=double), intent(in   ) :: pot(this%npot) 
    real(kind=double), intent(inout) :: integrals(this%ntdens) 
    ! local
    logical :: rc
    integer :: res
    integer :: icell ,ncell, nterm, ndeg
    real(kind=double) :: tdens_loc
    real(kind=double) :: ax,bx, ay, by
    real(kind=double), allocatable :: stiff_loc(:)
    real(kind=double), allocatable :: x_CTP(:,:),x_CUP(:,:)
    real(kind=double), allocatable :: y_CTP(:,:),y_CUP(:,:)
    type(fullmat) :: cell_stiffness_matrix
    real(kind=double) :: ddot


    call cell_stiffness_matrix%init(lun_err, this%nequ, this%nequ,.True.)

    ndeg  = this%spec%ndeg
    nterm = this%spec%nterm
    ncell = this%grid%grid%ncell
    !write(*,*) ndeg, this%nequ, nterm, ncell

    this%stiff=zero
    allocate(stiff_loc( nterm ),&
         x_CTP( ndeg+1, ndeg+1),&
         x_CUP( ndeg+1, ndeg+1),&
         y_CTP( ndeg+1, ndeg+1),&
         y_CUP( ndeg+1, ndeg+1),&         
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
         ' local array stiff_loc x_CTP, x_CUP, y_CTP, y_CUP',res)

    do icell = 1, ncell
       if (mod( icell, 500 ) == 0 ) then
          write(lun_out,*) 'Assembled cells = ',icell 
       end if

       call this%grid%get_interval_from_cell(icell,ax,bx,ay,by)
       call this%spec%eval_CTPCUP2(ndeg,ax,bx,x_CTP,x_CUP) 
       call this%spec%eval_CTPCUP2(ndeg,ay,by,y_CTP,y_CUP)
       

       call build_stiff_cell( nterm, ndeg,&
            stiff_loc,&
            x_CTP,x_CUP,y_CTP,y_CUP, &
            this%spec)
!!$       call build_stiff_test( nterm, ndeg,&
!!$            stiff_loc,&
!!$            x_CTP,x_CUP,y_CTP,y_CUP, &
!!$            this%spec)
       call tria2full(this%nequ,stiff_loc,cell_stiffness_matrix)
       call cell_stiffness_matrix%Mxv(pot(2:this%npot), this%scr_npot(2:this%npot))
       integrals(icell) = ddot(this%npot-1, pot(2:this%npot),1, this%scr_npot(2:this%npot),1)

!!$       this%stiff_matrices(icell,:) = stiff_loc(:)
       
    end do    
    call cell_stiffness_matrix%kill(lun_err)
    deallocate(&
         stiff_loc,&
         x_CTP,&
         x_CUP,&
         y_CTP,&
         y_CUP,&
         stat=res)
    if(res .ne. 0) rc = IOerr(lun_err, err_alloc, 'build_stiffs', &
         ' local array stiff_loc ',res)
    contains
       subroutine tria2full(n,tria,this)
        use Globals
        implicit none
        integer,           intent(in   ) :: n 
        real(kind=double), intent(in   ) :: tria(n*(n+1)/2)
        type(fullmat),     intent(inout) :: this
        ! local
        integer i,size,start

        !ENRICO meglio azzerare fuori una sola volta
        this%coeff = zero
        this%is_symmetric  = .True.
        this%triangular = 'N'
        start = 1 
        size  = 0
        do i = 1, n
           start = start + size
           size  = n + 1 - i
           call dcopy(size,tria(start),1,this%coeff(i,i),1)
        end do 
        start = 1 
        size  = 0
        do i = 1, n-1
           start = start + size
           size  = n + 1 - i
           call dcopy(size-1,tria(start+1),1,this%coeff(i,i+1),n)
        end do
     
        
      end subroutine tria2full
      

    end subroutine build_integrals_bis



  !!$
    subroutine assembly_stiff(this,tdens)
    use Globals
    use omp_lib
    implicit none
    class(specp0), target, intent(inout) :: this
    real(kind=double),          intent(in   ) :: tdens(this%ntdens)
    !local
    integer k,i,ncell,nterm
    real(kind=double) :: ddot
    
!!$    !openmp vars
!!$    !$ integer :: Num_Threads,Thread_ID,Max_Threads
!!$    !$ logical :: dynamic_NT
!!$
!!$    real(kind=double), pointer :: point_stiff(:)
!!$
!!$    real(kind=double), allocatable :: loc_stiff(:),loc_tdens(:)
!!$    real(kind=double), allocatable :: loc_stiff_matrices(:,:)
!!$    real(kind=double), allocatable :: loc_loc_stiff(:)
!!$
!!$    real(kind=double) :: scratch
!!$    
!!$    point_stiff => this%stiff
!!$
    nterm = this%nterm 
    ncell = this%ntdens
!!$
!!$    allocate(loc_stiff(nterm),loc_tdens(ncell),loc_stiff_matrices(ncell,nterm))
!!$    allocate(loc_loc_stiff(nterm))
!!$
!!$    loc_stiff = zero
!!$    loc_tdens = this%tdens
!!$    loc_stiff_matrices = this%stiff_matrices
!!$
!!$    !$OMP PARALLEL &
!!$    !$OMP PRIVATE(Max_Threads,Num_Threads,Thread_ID,k,i,&
!!$    !$OMP         loc_stiff,scratch) &
!!$    !$OMP SHARED(nterm,ncell,loc_tdens,loc_stiff_matrices,loc_loc_stiff)
!!$    !$ Max_Threads=OMP_get_max_threads()
!!$    !$ Num_Threads=OMP_get_num_threads()
!!$    !$ Thread_ID=OMP_get_thread_num()
!!$    !$ dynamic_NT=OMP_get_dynamic()
!!$    !$ write(200+Thread_ID,'(3(a,i2),a,l2)') &
!!$    !$     ' proc ',Thread_ID,' of ',Num_Threads-1,&
!!$    !$     ' (max ',Max_Threads-1,') dyn_par=',dynamic_NT
!!$    !$ write(300+Thread_ID,'(5e12.4)') loc_tdens
!!$    !$OMP DO
!!$    do k = 1, nterm 
!!$       !$ write(400+Thread_ID,'(I5)') k
!!$       !$ write(400+Thread_ID,'(5e12.4)') loc_stiff_matrices(:,k)
!!$       !loc_loc_stiff(k) = ddot(ncell, &
!!$       !     loc_tdens(1), 1, &
!!$       !     loc_stiff_matrices(1,k),1)
!!$       loc_loc_stiff(k)=zero
!!$
!!$       do i=1,ncell
!!$          scratch=loc_loc_stiff(k) + loc_tdens(i)*loc_stiff_matrices(i,k)
!!$       !$ write(500+Thread_ID,'(2i5,4e12.5)') k,i,&
!!$       !$          scratch,loc_stiff(k),loc_tdens(i),loc_stiff_matrices(i,k)
!!$          loc_loc_stiff(k) = scratch
!!$       end do
!!$       !loc_loc_stiff(k) = loc_stiff(k)
!!$    end do
!!$    !$OMP END DO
!!$    !loc_loc_stiff = loc_stiff
!!$    !$OMP END PARALLEL
!!$    this%stiff = loc_loc_stiff
!!$
!!$    write(600,'(5e12.4)') this%stiff
!!$    write(700,'(5e12.4)') loc_loc_stiff
!!$    call dgemv('T', ncell, nterm,&
!!$         one,&
!!$         this%stiff_matrices, ncell,&
!!$         this%tdens, 1, &
!!$         zero,&
!!$         this%stiff,1)
            
!!$    deallocate(loc_stiff,loc_tdens,loc_stiff_matrices)
!!$    deallocate(loc_loc_stiff)

    do i = 1, this%nterm
       this%stiff(i) = ddot(ncell, tdens,1, this%stiff_matrices(1,i),1)
    end do


    call tria2full(this%nequ,this%stiff,this%stiffness_matrix)
  contains

    subroutine tria2full(n,tria,this)
        use Globals
        implicit none
        integer,           intent(in   ) :: n 
        real(kind=double), intent(in   ) :: tria(n*(n+1)/2)
        type(fullmat),     intent(inout) :: this
        ! local
        integer i,size,start

        !ENRICO meglio azzerare fuori una sola volta
        this%coeff = zero
        this%is_symmetric  = .True.
        this%triangular = 'N'
        start = 1 
        size  = 0
        do i = 1, n
           start = start + size
           size  = n + 1 - i
           call dcopy(size,tria(start),1,this%coeff(i,i),1)
        end do 
        start = 1 
        size  = 0
        do i = 1, n-1
           start = start + size
           size  = n + 1 - i
           call dcopy(size-1,tria(start+1),1,this%coeff(i,i+1),n)
        end do
     
        
      end subroutine tria2full
       

  end subroutine assembly_stiff

  subroutine syncronize_tdpot( this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use DmkInputsData
    use ControlParameters 
    implicit none
    class(specp0), target, intent(inout) :: this
    type(tdpotsys),     intent(inout) :: tdpot
    type(DmkInputs),    intent(in   ) :: ode_inputs
    type(CtrlPrm),      intent(in   ) :: ctrl
    integer,            intent(inout) :: info
    ! local
    integer :: lun_err,lun_out,lun_stat  
    integer :: ntdens, npot
    type(fullmat) :: copy_stiff
    real(kind=double) :: dnrm2
    
    npot   = tdpot%npot
    ntdens = tdpot%ntdens
    
    ! 2.1 - Assign data 
    if ( abs( ode_inputs%pode-2.0d0 ) < small ) then
       call tdpot%tdens2gfvar(tdpot%ntdens,ode_inputs%pode,ode_inputs%pflux,&
            tdpot%tdens,tdpot%gfvar)
       
    end if

    !
    ! Assembly stiffness matrix 
    !
    !call this%CPUtim%STF%set('start')
    call this%assembly_stiff(tdpot%tdens)
    !call this%CPUtim%STF%set('stop')

    !
    ! - copy matrix and rhs 
    ! - solver
    ! - get the solution
    !
    copy_stiff=this%stiffness_matrix
    !write(*,*) dnrm2(this%npot,ode_inputs%rhs,1) 
    this%scr_npot(:) = ode_inputs%rhs(:)
    call dposv('L',this%nequ, 1, copy_stiff%coeff,&
          this%nequ, this%scr_npot(2:this%npot), this%nequ, this%info_solver%ierr)
    
    tdpot%pot(1)           = zero
    tdpot%pot(2:this%npot) = this%scr_npot(2:this%npot)

    !call this%stiffness_matrix%Mxv(tdpot%pot(2:tdpot%npot),this%scr_npot(2:tdpot%npot))
    !this%scr_npot(2:tdpot%npot) = this%scr_npot(2:tdpot%npot) - ode_inputs%rhs(2:tdpot%npot)
    !write(*,*) dnrm2(this%npot-1, this%scr_npot(2:tdpot%npot),1) 

    if ( this%info_solver%ierr .ne. 0 ) info=-1
    
    ! zero mean 
!!$    this%pot(1) = - ddot(nequ,&
!!$         this%pot(2:nequ+1),1,&
!!$         this%spec%integral_base(2:nequ+1),1)&
!!$         /4.0d0
!!$    write(*,*) 'mean0', ddot(nequ+1,&
!!$         this%coeff(1:nequ+1),1,&
!!$         this%spec%integral_base(1:nequ+1),1)
    
    call tdpot%tdens2gfvar(ntdens,ode_inputs%pode,ode_inputs%pflux,&
         tdpot%tdens,tdpot%gfvar)

    tdpot%all_syncr = .true.


  end subroutine syncronize_tdpot

    !>------------------------------------------------------------
    !> Assembly the rhs of the tdens ode of gfvar ode
    !<-------------------------------------------------------------
    subroutine assembly_rhs_ode_tdens(this,&
         tdpot,&
         ode_inputs,&
         rhs_ode)
      implicit none
      class(specp0),     intent(inout) :: this
      type(DmkInputs),   intent(in   ) :: ode_inputs
      type(tdpotsys),    intent(in   ) :: tdpot
      real(kind=double), intent(inout) :: rhs_ode(this%ntdens) 

      !local
      integer :: icell,j
      integer :: ntdens
      real(kind=double) :: min,max, pflux, pmass,decay
      real(kind=double) :: ptrans, gf_pflux, gf_pmass
      real(kind=double) :: penalty_factor,dnrm2

      ntdens = this%ntdens


      rhs_ode = zero

      pflux = ode_inputs%pflux
      decay = ode_inputs%decay
      pmass = ode_inputs%pmass

      call build_int_nrm2grad(&
           this%npot, this%grid%grid%ncell, this%nterm,&
           tdpot%pot, this%stiff_matrices, &
           this%int_nrm2grad)

!!$      call this%build_integrals_bis(0,6,tdpot%pot,this%scr_ntdens)
!!$      
!!$      write(*,*) 'difference=', dnrm2(this%ntdens,this%int_nrm2grad-this%scr_ntdens,1)



      rhs_ode = tdpot%tdens ** pflux * this%int_nrm2grad &
           - decay * ode_inputs%kappa * tdpot%tdens ** pmass &
           * this%grid%grid%size_cell

    end subroutine assembly_rhs_ode_tdens

    !>------------------------------------------------------------
    !> Assembly pointwise increment of tdens 
    !> Invert rhs_ode w.r.t mass matrix for tdens-FEM 
    !<-------------------------------------------------------------
    subroutine get_increment_tdens(this, rhs_ode,inc_ode)
      implicit none
      class(specp0),     intent(in   ) :: this
      real(kind=double), intent(in   ) :: rhs_ode(this%ntdens)
      real(kind=double), intent(inout) :: inc_ode(this%ntdens)

      inc_ode = rhs_ode / this%grid%grid%size_cell

    end subroutine get_increment_tdens

  !>------------------------------------------------
  !> Procedure for computation of next state of system
  !> ( all variables ) given the preovius one
  !> ( private procedure for type specp0, used in update)
  !> 
  !> usage: call var%update(lun_err,itemp)
  !> 
  !> where:
  !> \param[in ] lun_err -> Integer. I/O err. unit
  !> \param[in ] itemp   -> Integer. Time iteration
  !<---------------------------------------------------
  subroutine update_tdpot(this,&
       tdpot,&
       ode_inputs,&
       ctrl,&
       info)
    use ControlParameters
    use Timing
    use MuffeTiming
    implicit none
    class(specp0),  target,   intent(inout) :: this        
    type(tdpotsys),    intent(inout) :: tdpot
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(CtrlPrm),     intent(in   ) :: ctrl
    integer,           intent(inout) :: info
    
    
    ! local
    real(kind=double) :: decay,pflux,pmass,pode,time,tnext
    integer :: newton_initial
    integer :: ntdens, npot
    integer :: info_inter,passed_reduced_jacobian
    type(tim) :: wasted_temp
    character(len=256) :: str,msg,method,result_update,out_format
    integer :: slot
    integer :: lun_err,lun_out,lun_stat


    ntdens = tdpot%ntdens
    npot   = tdpot%npot

    lun_err  = ctrl%lun_err
    lun_out  = ctrl%lun_out
    lun_stat = ctrl%lun_statistics

    ! update all spatial variables at itemp+1
    select case ( ctrl%time_discretization_scheme )
    case (1)
       call this%explicit_euler(&
            lun_err,info,&
            tdpot,&
            ode_inputs,&
            ctrl)
         
    end select


  end subroutine update_tdpot

  !>----------------------------------------------------
  !> Procedure for update the system with Explicit Euler
  !> IMPORTANT all the variables tdens pot 
  !> odein have to be syncronized at time time(itemp)
  !>
  !> usage: call var%explicit_euler(lun_err,itemp)
  !> 
  !> where:
  !> \param[in ] lun_err -> Integer. I/O err. unit
  !> \param[in ] itemp   -> Integer. Time iteration
  !<---------------------------------------------------    
  subroutine explicit_euler(this,&
       lun_err,info,&    
       tdpot,&
       ode_inputs,&
       ctrl)
       !CPU)
      use Globals 
      use ControlParameters
      use MuffeTiming
      implicit none
      class(specp0),     intent(inout) :: this
      integer,           intent(in   ) :: lun_err
      integer,           intent(inout) :: info
      type(tdpotsys),    intent(inout) :: tdpot
      type(DmkInputs),   intent(in   ) :: ode_inputs
      type(CtrlPrm),     intent(in   ) :: ctrl
 
      !local
      integer :: icell
      integer :: ntdens,npot
      real(kind=double) :: deltat,dnrm2
      character(len=256) :: msg
      
      ntdens = this%ntdens
      npot   = this%npot

      if ( .not. tdpot%all_syncr)  then
         write(lun_err,*) 'Tdens Gfvar Pot are not syncronized'
         stop
      end if

      deltat=ctrl%deltat
     
      !
      ! Compute rhs ode and tdens increment
      !
      !
      ! $ \int_{\Domain} ( rhs_ode )\chi_{k}(x) $
      !
      call this%assembly_rhs_ode_tdens(&
         tdpot,&  
         ode_inputs,&
         tdpot%scr_nfull(1:ntdens))
      ! scale by the mass matrix con the Finite Elements used for tdens
      call this%get_increment_tdens(tdpot%scr_nfull(1:ntdens),tdpot%scr_ntdens)

      
      !
      ! tdens update 
      !
      tdpot%tdens = tdpot%tdens + deltat * tdpot%scr_ntdens
      do icell = 1, tdpot%ntdens
         tdpot%tdens(icell) = max(tdpot%tdens(icell),ctrl%min_tdens)
      end do
      
      ! Store local var_tdens
      tdpot%system_variation = this%compute_tdpot_variation(tdpot,ctrl)


      !
      ! update $\Pot$ input varible at time $t^{\tstep+1}$
      ! solveing -\Div( \Tdens^{k+1} \Grad \Pot ) = \Forcing
      !
      this%scr_ntdens = tdpot%tdens + ode_inputs%lambda
      call this%assembly_stiff(this%scr_ntdens)
      ! copy because works in place
      this%copy_stiff=this%stiffness_matrix
      this%scr_npot(:) = ode_inputs%rhs(:)
      call dposv('L',this%nequ, 1, this%copy_stiff%coeff,&
           this%nequ, this%scr_npot(2:this%npot), this%nequ, this%info_solver%ierr)
      info = this%info_solver%ierr
                 
      if ( info .ne. 0) then
         return
      else
         tdpot%pot(1)           = zero
         tdpot%pot(2:npot) = this%scr_npot(2:npot)


         !
         ! optinal information
         !
         if ( (ctrl%info_time_evolution .gt. 1 ) .or. (ctrl%lun_statistics >0 ) ) then
            write(msg,*)'MAX \int_{Cell} |\ Grad \Pot|^2  / |CELL| =', &
                 maxval(this%int_nrm2grad/this%grid%grid%size_cell)
            if (ctrl%info_time_evolution .gt. 1 )  write(ctrl%lun_out,*) etb(msg)
            if (ctrl%lun_statistics >0 ) write(ctrl%lun_statistics,*) etb(msg) 

            call this%stiffness_matrix%Mxv(tdpot%pot(2:npot),&
                 tdpot%scr_npot(2:npot))
            tdpot%scr_npot(1)=zero
            tdpot%scr_npot=tdpot%scr_npot-ode_inputs%rhs
            write(msg,*) 'DIRECT LINEAR SOLVER | RES = ',&
                 dnrm2(npot,tdpot%scr_npot,1)/dnrm2(npot,ode_inputs%rhs,1)      
            write(ctrl%lun_out,*) etb(msg)
            if (ctrl%lun_statistics >0 ) write(ctrl%lun_statistics,*) etb(msg) 
         end if


         !
         ! store information for linear system solution
         !
!!$      this%sequence_info_solver(1)=this%info_solver
!!$      this%iter_nonlinear = 1
!!$      this%sequence_build_prec(1)=ctrl%build_prec
!!$      this%sequence_build_tuning(1)=ctrl%build_tuning

         call tdpot%tdens2gfvar(ntdens,ode_inputs%pode,ode_inputs%pflux,&
              tdpot%tdens,tdpot%gfvar)

         tdpot%deltat    = deltat
         tdpot%time      = tdpot%time + deltat 
         tdpot%all_syncr = .true.
      end if


    end subroutine explicit_euler

    !>----------------------------------------------------------------
  !> Function to eval.
  !> $ var(\TdensH^k):=\frac{
  !>                     \| \tdens^{k+1} - \tdens^{k} \|_{L2} 
  !>                   }{ 
  !>                     \| \tdens^{k+1} \|_{L2}}  
  !>                   } $
  !> (procedure public for type specp0) 
  !> 
  !> usage: call var%err_tdesn()
  !>    
  !> where:
  !> \param  [in ] var       -> type(specp0) 
  !> \result [out] var_tdens -> real. Weighted var. of tdens
  !<----------------------------------------------------------------
  function compute_tdpot_variation(this,tdpot,ctrl) result(var)
    use Globals
    use ControlParameters
    implicit none
    class(specp0),     intent(in   ) :: this
    type(tdpotsys),    intent(in   ) :: tdpot
    type(CtrlPrm),     intent(in   ) :: ctrl
    real(kind=double) :: var
    !local
    real(kind=double) :: norm_old, norm_var,exponent

    ! $ var_tdens = 
    !              frac{
    !                   \|\tdens^{n+1}-\tdens^n\|_{L^2}
    !                  }{
    !                   \deltat \|\tdens^{n}\|_{L^2}
    !                  }$
    exponent = ctrl%norm_tdens
    norm_old = this%grid%grid%normp_cell(exponent,tdpot%tdens_old)
    norm_var = this%grid%grid%normp_cell(exponent,tdpot%tdens-tdpot%tdens_old)

    var =  norm_var / ( ctrl%deltat * norm_old )
  end function compute_tdpot_variation

  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine compute_functionals(this,&
       ode_inputs,&
       tdpot,&
       deltat)
    use DmkInputsData 
    use TdensPotentialSystem
    use ControlParameters
    implicit none
    class(specp0),     intent(inout) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(inout) :: tdpot
    real(kind=double), intent(in   ) :: deltat
    ! local 
    real(kind=double) :: power_wmass
    

    call build_int_nrm2grad(&
         this%npot, this%ntdens, this%nterm,&
         tdpot%pot, this%stiff_matrices, &
         this%int_nrm2grad)
    
    tdpot%energy = onehalf * sum(tdpot%tdens * this%int_nrm2grad)
    power_wmass = tdpot%wmass_exponent(2.0d0,ode_inputs%pflux,ode_inputs%pmass)
    
    tdpot%weighted_mass_tdens  = onehalf / power_wmass *  &
         sum((tdpot%tdens **   power_wmass) * this%grid%grid%size_cell )
    tdpot%lyapunov = tdpot%energy +  tdpot%weighted_mass_tdens

    tdpot%min_tdens  = minval(tdpot%tdens)
    tdpot%max_tdens  = maxval(tdpot%tdens)

  end subroutine compute_functionals


  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine reset_controls_after_update_failure(this,&
       info,&
       ode_inputs,&
       tdpot,&
       ctrl,&
       inputs_time,&
       ask_for_inputs)
    use DmkInputsData 
    use TdensPotentialSystem
    use ControlParameters
    implicit none
    class(specp0),     intent(in   ) :: this
    integer,           intent(in   ) :: info
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(CtrlPrm),     intent(inout) :: ctrl
    real(kind=double), intent(inout) :: inputs_time
    logical,           intent(inout) :: ask_for_inputs

    ! local
    
    select case ( ctrl%time_discretization_scheme )
    case(1)
       ctrl%deltat =  ctrl%deltat/ctrl%deltat_expansion_rate
       inputs_time = tdpot%time
       ask_for_inputs = .False.
    end select
    
  end subroutine reset_controls_after_update_failure

  !>----------------------------------------------------
  !>
  !<---------------------------------------------------    
  subroutine set_controls_next_update(this,&   
       ode_inputs,&
       tdpot,&
       ctrl)
    use DmkInputsData 
    use TdensPotentialSystem
    use ControlParameters
    implicit none
    class(specp0),     intent(in   ) :: this
    type(DmkInputs),   intent(in   ) :: ode_inputs
    type(tdpotsys),    intent(in   ) :: tdpot
    type(CtrlPrm),     intent(inout) :: ctrl
    ! local
    real(kind=double) :: deltat
    


    !
    ! time step controls
    !
    select case ( ctrl%deltat_control ) 
    case( 1 )
       ! constant time step
       deltat=ctrl%deltat        
    case( 2 )
       ! increasing time step
       deltat=deltat * ctrl%deltat_expansion_rate
       deltat=min(deltat,ctrl%deltat_upper_bound)
       deltat=max(deltat,ctrl%deltat_lower_bound)
    case (3 )
    end select
       

    
  end subroutine set_controls_next_update


  

  subroutine build_stiff_cell(nterm,ndeg,&
       stiff_loc,&
       x_CTP,x_CUP,y_CTP,y_CUP,&
       spec)
    use Globals

    use Spectral
    implicit none
    integer,           intent(in   ) :: nterm, ndeg
    real(kind=double), intent(inout) :: stiff_loc( nterm ) 
    real(kind=double), intent(in   ) :: x_CTP( ndeg+1, ndeg+1 )
    real(kind=double), intent(in   ) :: x_CUP( ndeg+1, ndeg+1 )
    real(kind=double), intent(in   ) :: y_CTP( ndeg+1, ndeg+1 ) 
    real(kind=double), intent(in   ) :: y_CUP( ndeg+1, ndeg+1 )
    type(spct),        intent(in   ) :: spec
    !local
    integer i,j,k,m

    k=0
    do j = 2, ( ndeg+1)**2  
       do i = j, (ndeg+1)**2
          k = k + 1
          stiff_loc(k) = &
               ddx(i,j,ndeg) *  &
               x_CUP(&
               spec%dx(i,ndeg)+1, &
               spec%dx(j,ndeg)+1  ) * &
               y_CTP( &
               spec%dy(i,ndeg)+1, &
               spec%dy(j,ndeg)+1  ) &
               + &
               ddy(i,j,ndeg) * &
               y_CUP(&
               spec%dy(i,ndeg)+1, &
               spec%dy(j,ndeg)+1  ) * &
               x_CTP(&
               spec%dx(i,ndeg)+1, &
               spec%dx(j,ndeg)+1  )

       end do
    end do

  contains
    function ddx(i,j,ndeg)
      implicit none
      integer :: i, j, ndeg
      integer :: ddx
      ddx = ( (i-1) / (ndeg+1) ) * ( (j-1) / (ndeg+1) )
    end function ddx
    function ddy(i,j,ndeg)
      implicit none
      integer :: i, j, ndeg
      integer :: ddy
      ddy = ( (mod(i-1, ndeg+1) ) * (mod(j-1,ndeg+1)) )
    end function ddy
  end subroutine build_stiff_cell

  subroutine build_int_nrm2grad(npot, ntdens, nterm,&
       pot, stiff_matrices, int_nrm2grad)
    use Globals
    implicit none
    integer,           intent(in ) :: npot
    integer,           intent(in ) :: ntdens
    integer,           intent(in ) :: nterm
    real(kind=double), intent(in ) :: pot(npot)
    real(kind=double), intent(in ) :: stiff_matrices(ntdens, nterm)
    real(kind=double), intent(out) :: int_nrm2grad(ntdens)
    !local
    integer :: i,j,m
    real(kind=double) :: ui,alpha
        
    int_nrm2grad = zero
    m=0
    do i=2,npot
       m=m+1
       ui=pot(i)
       !diagonal terms
       alpha=ui**2       
       call daxpy(ntdens, alpha, stiff_matrices(1,m),1,int_nrm2grad,1)
       ! extra diagonal
       do j = i+1,npot
          m = m+1
          alpha = 2.0d0 * ui * pot(j)
          call daxpy(ntdens, alpha, stiff_matrices(1,m),1,int_nrm2grad,1)
       end do
    end do
  end subroutine build_int_nrm2grad

end module SpectralP0

