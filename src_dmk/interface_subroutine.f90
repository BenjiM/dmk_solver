subroutine otpdmk(&
     nnodeincell,nnode,ncell, &
     topol,coord,&
     pflux,forcing,&
     tdens,pot,&
     simple_ctrl,info)
  use KindDeclaration
  use Globals
  use TdensPotentialSystem
  use DmkInputsData
  use DmkP1P0
  use DmkControls
  use ControlParameters
  use AbstractGeometry
  implicit none
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  real(kind=double), intent(in   ) :: pflux
  real(kind=double), intent(in   ) :: forcing(ncell)
  real(kind=double), intent(inout) :: tdens(ncell)
  real(kind=double), intent(inout) :: pot(nnode)
  type(DmkCtrl),     intent(inout) :: simple_ctrl
  integer,           intent(inout) :: info
  ! local variables
  type(file):: fgrid,fsubgrid
  type(DmkInputs) :: ode_inputs
  type(tdpotsys) :: tdpot
  type(p1p0_space_discretization) :: p1p0
  type(DmkInputs) :: inputs_data
  type(abs_simplex_mesh) :: grid,subgrid
  ! reverse communication varaibles
  integer :: flag
  real(kind=double) :: current_time
  ! shorthand copies
  integer :: lun_err
  integer :: lun_out
  integer :: lun_stat
  integer :: ntdens
  integer :: npot
  ! working variables
  logical :: rc
  integer :: res
  integer :: i,j,k,icell, inode
  real(kind=double), allocatable :: forcing_subgrid(:)
  real(kind=double), allocatable :: dirac_subgrid(:)
  type(CtrlPrm)  :: ctrl
  character(len=256) :: msg

  write(*,*) 'pflux=',pflux,nnodeincell,ncell,nnode

  !
  ! copy controls
  !
  write(*,*) 'copy ctrl', etb(simple_ctrl%krylov_scheme)
  call ctrl%init(lun_err,simple_ctrl)
  write(*,*) 'ctrl definition'



  lun_err  = ctrl%lun_err
  lun_out  = ctrl%lun_out
  lun_stat = ctrl%lun_statistics

  do i=1,ncell
     write(*,*) topol(1:nnodeincell,i)
  end do

  !
  ! buils grid and subgrid
  !
  call data2grids(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,&
     grid,subgrid)
  write(*,*) 'ctrl definition'



  call fgrid%init(lun_err,'gridin.dat',14,'out')
  call fsubgrid%init(lun_err,'subgridin.dat',15,'out')


  call grid%write_mesh(lun_err,fgrid)
  call subgrid%write_mesh(lun_err,fsubgrid)


  call fgrid%kill(lun_err)
  call fsubgrid%kill(lun_err)


  ntdens=grid%ncell
  npot=subgrid%nnode
  write(*,*) 'grid done',ntdens,npot


  !
  ! inputs data
  !
  call inputs_data%init(lun_err, ntdens,npot)
  inputs_data%pflux=pflux

  !
  ! build p1p0 FEM space
  !
  call p1p0%init(&
       ctrl,&
       1,grid, subgrid)


  !
  ! build rhs integrated and check imbalance
  !
  call subgrid%info(6)
    write(*,*) 'pflux=',pflux,nnodeincell,ncell,nnode


  allocate(&
       forcing_subgrid(subgrid%ncell),&
       dirac_subgrid(subgrid%nnode),&
       stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_alloc, 'otpdmk', &
       ' work arrays forcing_subgrid dirac_subgrid ', res)

  call subgrid%proj_subgrid(forcing,forcing_subgrid)
  call p1p0%p1%int_test(forcing_subgrid,inputs_data%rhs)
  inputs_data%rhs=inputs_data%rhs+dirac_subgrid
  deallocate(&
       forcing_subgrid,&
       dirac_subgrid,&
       stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'otpdmk', &
       ' work arrays forcing_subgrid dirac_subgrid ', res)
  write(*,*) 'inputs data done'

  !
  ! 2.3 -Initialized tdens-potential type
  !
  call tdpot%init( lun_err,lun_out,ntdens, npot)
  tdpot%tdens=tdens
  tdpot%pot=zero

  !------------------------------------------------------------------------------------
  !
  ! 3 Time evolution starts
  !
  msg=ctrl%separator('Evolution begins')
  if ( ( ctrl%info_state .gt. 1).and. (lun_out>0) )  write(lun_out,'(a)') etb(msg)
  if ( (lun_stat>0) ) write(lun_stat,'(a)') etb(msg)

  !
  ! run dmk time evolution
  !
  !
  ! start time cycle
  !
  info=0
  flag=1
  current_time=inputs_data%time
  do while ( flag.ne.0)
     call p1p0%dmk_cycle_reverse_communication(&
          flag,info,current_time,&
          inputs_data,tdpot,ctrl)

     select case (flag)
     case(2)
        !
        ! fill ode_inputs with data at current time
        ! reading data from files and copy them into inputs_data
        inputs_data%time=current_time
     case(3)
        !
        ! flag==3 R
        ! Right before new update
        ! Here tdpot and inputs_data are syncronized
        ! The user can compute print any information regarding
        ! the state of the system
        !
     end select
     !
     ! In case of negative info, break cycle, free memory
     !
     if ( info.ne.0 )  flag=0
  end do

  !
  !  kill local ctrl copy
  !
  call ctrl%kill()


  !>------------------------------------------------------
  !> Save time, var_tdens, time functional
  !>------------------------------------------------------
  !
  ! free memory
  !
  call tdpot%kill(lun_err)
  call inputs_data%kill(lun_err)

  call p1p0%kill(lun_err)
  call grid%kill(lun_err)
  call subgrid%kill(lun_err)

end subroutine otpdmk


subroutine data2grids(&
     lun_err,&
     nnodeincell,nnode,ncell, &
     coord,topol,&
     grid,subgrid)
  use KindDeclaration
  use Globals
  use AbstractGeometry
  use SparseMatrix
  implicit none
  integer,           intent(in   ) :: lun_err
  integer,           intent(in   ) :: nnodeincell,nnode,ncell
  integer,           intent(in   ) :: topol(nnodeincell,ncell)
  real(kind=double), intent(in   ) :: coord(3,nnode)
  type(abs_simplex_mesh),    intent(inout) :: grid,subgrid
  ! local
  type(file) :: fgrid
  logical :: rc
  integer :: res
  type(spmat) :: connection_matrix
  character(len=256) :: cell_type
  integer, allocatable :: perm(:),iperm(:)

  if (nnodeincell==3) then
     cell_type='triangle'
  else
     cell_type='tetrahedron'
  end if

  !
  ! init grid
  !
  call grid%init_from_data(lun_err,&
       nnode,ncell,nnodeincell,cell_type, &
       topol,coord)
  write(*,*)'grid from data'
  call grid%build_size_cell(lun_err)
  write(*,*)'grid sizecell'
  call grid%build_normal_cell(lun_err)
  write(*,*)'grid done'

  write(*,*) grid%nnode
  call grid%test(lun_err)
  call fgrid%init(lun_err,'gridin.dat',14,'out')
  call grid%write_mesh(lun_err,fgrid)

  !
  ! build subgrid
  !
  call subgrid%refine(lun_err,input_mesh=grid)
  write(*,*)'subgrid refined'
  call subgrid%build_nodenode_matrix(lun_err, .False.,connection_matrix)
  write(*,*)'subgrid nodenode'
  allocate(perm(subgrid%nnode),iperm(subgrid%nnode),stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_inp, 'data2grids', &
       ' work arrays perm iperm', res)
  call connection_matrix%genrcm(6,perm,iperm)
  write(*,*)'subgrid genrcm'
  call connection_matrix%kill(lun_err)
  call subgrid%renumber(lun_err, subgrid%nnode,perm,iperm)
  write(*,*)'subgrid renumber'
  call subgrid%build_size_cell(lun_err)
  call subgrid%build_normal_cell(lun_err)

  deallocate(perm,iperm,stat=res)
  if (res.ne.0) rc = IOerr(lun_err, err_dealloc, 'data2grids', &
       ' work arrays perm iperm', res)

end subroutine data2grids
