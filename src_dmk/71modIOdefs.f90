!> Defines I/O units, directories, filenames
module IOdefs
  !use hdf5, only: hid_t
  use Globals
  use TimeInputs
  implicit none  
  private
  type, public :: IOfdescr
     !> max unit number used in the code.
     !> Dynamic var used to avoid conflicts in opening new units.
     integer :: maxUsedLun
     !> terminal (generally stdout)
     type(file) :: term
     !> obvious defs
     type(file) :: stdin,stdout,stderr
     !>-------------------------------------------------------
     !> Input Files and Directories (mandatory)
     !>-------------------------------------------------------
     !> Genereal input folder
     type(file) :: input_folder
     !>-------------------------------------------------------
     !> Simulations controls     
     !> input file for control parameters
     type(file) :: control
     !>-------------------------------------------------------
     !> Topology files 
     !> input file for mesh data
     type(file) :: grid
     !> input file with subgrid for p1 discretization
     type(file) :: subgrid  
     !> output directory for linear system data
     type(file) :: parents
     !>-------------------------------------------------------
     !> Input file of ODE
     !> input file for real left power
     type(file) :: pflux
      !> Input file of ODE
     !> input file for real left power
     type(file) :: pode
     !> input file for real right power
     type(file) :: pmass
     !> input file for time- varing decay
     type(file) :: decay
     !> input file for initial tdens
     type(file) :: tdens0
     !> input file for spatial decay
     type(file) :: kappa
     !> input file for integrated rhs
     !> given by 
     !> $ \int_{\Domain} \Focring\Psi 
     !>   + \int_{\partial \Domain}\OutFlux \cdot n $
     type(file) :: rhs_integrated
     !>-------------------------------------------------------
     !> Output Files and Directories
     !> general output directory 
     type(file) :: output_folder
     !> output file summarizing all data of simulations
     type(file) :: statistic
     !> output directory where to put spatial varying datas 
     !> tdens, pot etc
     type(file) :: result
     !> Tdens time evolution
     type(file) :: tdens_out
     !> Pot time evolution
     type(file) :: pot_out
     !> Pot time evolution
     type(file) :: nrmgraddyn_out
     !> output directory where to put time functional data
     type(file) :: timefun
     !> output directory where to put vtk files
     type(file) :: linear_sys
     !> generic debug file
     type(file) :: debug
     !>-------------------------------------------------------
     !> input file for forcing term ode
     type(file) :: rhs_grid_integrated
     !> input file to compare with tdens (optional)
     type(file) :: optdens
     !> Input file for penalty 
     type(file) :: penalty
     !> Input file for penalty weight
     type(file) :: penalty_weight
     !> Input file for multiplicative factor for penalty weight
     type(file) :: penalty_factor
     !> input file to compare with tdens (optional)
     type(file) :: dirichlet_bc
     !> Input file paremeter lambda to ensure coercivity
     type(file) :: lambda
   contains
     !> static constructor
     !> (public for type IOfdescr)
     procedure, public, pass :: init => IO_initialize
     !> output the content of the variable
     !> (public for type IOfdescr)
     procedure, public, pass :: info => IO_print_info 
     !> static destructor
     !> (public for type IOfdescr)
     procedure, public, pass :: kill => IO_close_all_units
     !> Find the logical unit for 
     !> stderr, stdout, statistic and debug
     !> (public for type IOfdescr)
     procedure, public, pass :: find_lun
  end type IOfdescr

  !>----------------------------------------------------------------
  !> Structure variable containg the time varing quantities 
  !> (pflux, pmass, decay, kappa, rhs_integrated)
  !> and fix in time quanties 
  !> (tdens0) the ODE  
  !> \begin{gather}
  !>    \Div(\Tdens+lambda \Grad \Pot) = \Forcing 
  !>    \quad
  !>    -\Tdens \Grad \Pot \cdot \n_{\partial \Domain} = \Bdflux
  !> \\
  !> \Tdens'=|\Tdens \Grad \Pot(\Tdens)|^\Pflux-kappa*decay*\Tdens^{\Pmass}
  !> \\
  !> \Tdens(tzero) = \Tdens0
  !> \end{gather}
  !> It may contains the optional quantities 
  !> forcing, boundary_flux, optdens(fix initialized and used
  !> only if the correspondet input files exis
  !>------------------------------------------------------------------
  type, public :: OdeInp
     !> True/False flag for existence of optdens array
     logical :: optdens_exists= .False.
     !> True/False flag for existence of optimal potential array
     logical :: dirichlet_exists= .False.
     !> True/False flag for existence penalization
     logical :: penalty_exists = .False.
     !> Initial time
     real(kind=double) :: tzero
     !>-------------------------------------------------------------
     !> Mandatory Input data
     !>-------------------------------------------------------------
     !> Nmber of variables for Tdens-variables 
     integer :: ntdens
     !> Nmber of variables for Pot-variables
     integer :: npot
     !> Number of Dirichlet nodes 
     integer :: ndir=0
     !> Flag for subgrid
     integer :: id_subgrid
     !> Dimension = ntdens
     !> Optinal density (optional)
     !real(kind=double), allocatable :: optdens(:)
     type(TimeData) :: optdens
     !> Dimension= ndir
     !> Dirichlet node indeces ( in subgrid ) 
     integer, allocatable :: dirichlet_nodes(:)
     !> Dimension = ndir
     !> Dirichlet values at nodes ( in subgrid )
     real(kind=double), allocatable :: dirichlet_values(:)
     !> Initial data for transport dnsity
     !> Dimension (ntdens=grid%ncell)
     !> In timedata form but must be in steady state
     !> We use this format to have a unique format in input files
     type(TimeData) :: tdens0
     !> Pflux power=beta
     !> Contiains 1 real with pflux power
     type(TimeData) :: pflux
     !> Pode power
     !> Power which norm_grad is risen
     type(TimeData) :: pode
     !> Time decay
     !> Contiains 1 real with pflux power
     type(TimeData) :: decay
     !> Pmass power
     !> Contiains 1 real with pflux power
     type(TimeData) :: pmass
     !> Positive relax parameter to ensure the coercitivy 
     !> The tdens used in the elliptic equation is tdens + lambda
     type(TimeData) :: lambda
     !> Optional weight multiplicative factor for penalty_weight 
     !> $\Wpenalty$ (rapid way to scale the penalty, can be induced 
     !> in $\Wpenalty$ directily)
     type(TimeData) :: penalty_factor
     !> Optional weight $\Wpenalty$ of penalty decay
     !> Give by adding penalty 
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     type(TimeData) :: penalty_weight
     !> Dimension = ntdens
     !> Optional penalty $\geq 0$ given by adding
     !> $\Wpenalty\|\Tdens -\Penalty\|^2
     !> in the minimization problem.
     type(TimeData) :: penalty
     !> Dimension = ntdens
     !> Spatial decay 
     !> Contiains ntria-array with spatial decay
     type(TimeData) :: kappa
     !> Dimension = npot
     !> Rhs in the linear system
     !> arising from equation 
     !> $-\div(\Tdens \Grad \Pot ) = \Forcing$
     !> Completed with proper boundary condition
     type(TimeData) :: rhs_integrated
     !> Dimension = 2, npot
     !> Indeces and values of Dirichlet points
     !> Format is :
     !>   data(1)=float(inode)
     !>   data(2)=value
     type(TimeData) :: dirichlet_bc
   contains
     !> Static constructor
     !> (public for type OdeInp)
     procedure, public, pass :: init => OdeInp_init
     !> Static destructor
     !> (public for type OdeInp)
     procedure, public, pass :: kill => OdeInp_kill
     !> Info procedure
     !> (public for type OdeInp)
     procedure, public, pass :: info => OdeInp_info 
     !> Eval TD variables in the module at given time
     !> (private procedure for type OdeInp, used in init)
     procedure, public, pass :: set    => OdeInp_set
     !> Copy ode's inuts into odedata tpye
     !> (public for type OdeData)
     procedure, public, pass :: set_DmkInputs
  end type OdeInp

contains
  !>-------------------------------------------------------------
  !> Static constructor. 
  !> (public procedure for type IOfdescr)
  !> Instantiate and initialize a variable of type IOfdescr
  !>
  !> usage:
  !>     call 'var'\%init()
  !>
  !> - defines stdin,stderr,stdout
  !> - reads from "muffa.fnames" working directory and filenames
  !> - open all file units checking for consistency
  !<-------------------------------------------------------------
  subroutine IO_initialize(this)
    use Globals
    use, intrinsic :: iso_fortran_env, only : stdin=>input_unit, &
         stdout=>output_unit, &
         stderr=>error_unit
    class(IOfdescr) :: this

    ! local vars
    integer :: opstat,res
    logical :: file_exist=.false.,dir_exist
    character (len=256) :: fname='muffa.fnames', dir,input,str

    integer :: nlun
    integer :: lun !  da tirare via ********
    lun = stderr   !  da tirare via ********
    
    this%stderr%exist = .true.
    this%stderr%lun=stderr
    this%stderr%fn='stderr'
    
    this%stdin%exist = .true.
    this%stdin%lun=stdin
    this%stdin%fn='stdin'
    
    this%stdout%exist = .true.
    this%stdout%lun=stdout
    this%stdout%fn='stdout'
    
    this%term%exist = .true.
    this%term%lun=this%stdout%lun
    this%term%fn='terminal'


    ! read in from standard file (or input) the IO control file
    ! that overrides above default values
    do while (.not. file_exist)
       inquire(file=fname,exist=file_exist)
       if (file_exist) then
          !
          ! read folder position (usaully .)
          !
          open(1,file=fname)
          read(1,'(a)') dir
          dir=etb(erase_comment(dir))

          ! Want to avoid default lun 0, 5 and 6
          ! as they usually are stderr,stdin,stdout

          nlun=6
          !
          ! 1-read files/fodlers listed in muffa.fnames
          ! 2-clean str from comments
          ! 3-append dir if no absolute path
          ! 4-open/check file
          !
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%input_folder%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.True.)
          
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%control%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%grid%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%subgrid%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%parents%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pflux%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pode%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pmass%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%decay%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%tdens0%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))

          call dir2str(dir,str)
          call this%kappa%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%rhs_integrated%init(stderr,str,nlun,'in',&
               mandatory_input=.True.,&
               folder=.False.)

          !
          ! output files
          !
          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%output_folder%init(stderr,str,nlun,'out',&
               folder=.True.)
          


          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%statistic%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%result%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%tdens_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%pot_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%nrmgraddyn_out%init(stderr,str,nlun,'out',&
               folder=.False.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%timefun%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%linear_sys%init(stderr,str,nlun,'out',&
               folder=.True.)

          nlun=nlun+1
          read(1,'(a)') str
          str=etb(erase_comment(str))
          call dir2str(dir,str)
          call this%debug%init(stderr,str,nlun,'out',&
               mandatory_input=.False.,&
               folder=.False.,verbose=.False.)


          !
          ! optional files
          !
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%optdens%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%rhs_grid_integrated%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty_weight%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          
         
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%dirichlet_bc%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if
          
          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%penalty_factor%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          nlun=nlun+1
          read(1,'(a)',iostat=res) str
          if ( res .eq. 0 ) then
             str=etb(erase_comment(str))
             call dir2str(dir,str)
             call this%lambda%init(stderr,str,nlun,'in',&
                  mandatory_input=.False.,&
                  folder=.False.,verbose=.False.)
          end if

          this%maxUsedLun=nlun

          close(1)

          !inquire(file=dir,exist=dir_exist)
          !if(.not.dir_exist) then
          !   file_exist = IOerr(stderr, wrn_IO,'IOinitialize',etb(dir))
          !   cycle
          !end if
       else
          write(lun,*) ' Control file ',etb(fname),&
               ' does not exist in the current directory!!'
          if (lun.eq.this%stdin%lun) then
             write(lun,*) ' input another file name or END/QUIT to stop'
             read(*,'(a)') fname
             fname=to_lower(fname)
             if ( &
                  fname.eq.'end' .or. &
                  fname.eq.'quit' .or. &
                  fname.eq.'q' .or. &
                  fname.eq.'e' &
                  ) then
                stop ' simulation ended'
             end if
             ! everything has failed, restart loop
          else 
             stop ' simulation ended'
          end if
       end if
    end do
  contains
    subroutine prepend_dir(file_name,dir)
      use Globals
      implicit none
      type(file),       intent(inout) :: file_name
      character(len=*), intent(in   ) :: dir
      
      if( file_name%fn(1:1) .ne. '/') then
         file_name%fn=etb(dir)//'/'//etb(erase_comment(file_name%fn))
      else
         file_name%fn=etb(erase_comment(file_name%fn))
      end if
    end subroutine prepend_dir

    subroutine dir2str(dir,str)
      use Globals
      implicit none
      character(len=*), intent(in   ) :: dir
      character(len=*), intent(inout) :: str
      
      if( str(1:1) .ne. '/') then
         str=etb(dir)//'/'//etb(str)
      end if
    end subroutine dir2str

    
    subroutine check_open(file2read,lun,&
         file_folder_flag,&
         mandatory_optional_flag,&
         input_output_flag)
      use Globals
      implicit none
      type(file), intent(inout) :: file2read
      integer,    intent(in   ) :: lun
      integer,    intent(in   ) :: file_folder_flag
      integer,    intent(in   ) :: mandatory_optional_flag
      integer,    intent(in   ) :: input_output_flag

      ! rhs_integrated
      ! local
      logical :: file_exist
      
      if   ( file_folder_flag .eq. 1 ) then
         if ( input_output_flag .eq. 1 ) then
            inquire(file=file2read%fn,exist=file_exist)
            if ( file_exist ) then
               file2read%exist = .true.
               open(file2read%lun,file=file2read%fn,iostat=opstat)
               if(opstat.ne.0) then
                  write(lun,*) ' failed to open unit ',file2read%lun,&
                       ' to be linked to file ',etb(file2read%fn)
                  stop ' simulation ended'
               end if
            else 
               if ( mandatory_optional_flag .eq. 2 ) then
                  write(lun,*) 'File ', etb(file2read%fn), ' not found'
                  write(lun,*) 'Optional,  simulation continues'
               else
                  write(lun,*) 'Mandatory File ', &
                       etb(file2read%fn), 'not found'
                  stop ' simulation ended'
               end if
            end if
         else if ( input_output_flag .eq. 2 )  then
            if ( mandatory_optional_flag .eq. 1 ) then 
               open(file2read%lun,file=file2read%fn,iostat=opstat)
               if(opstat.ne.0) then
                  write(lun,*) ' failed to open unit ',file2read%lun,&
                       ' to be linked to file ',etb(file2read%fn)
                  stop ' simulation ended'
               else
                  file2read%exist = .true.
               end if
            end if
         end if
      else if ( file_folder_flag .eq. 2 ) then        
         inquire(file=etb(file2read%fn),exist=file_exist)
         if(.not.file_exist) then
            call execute_command_line ('mkdir -p ' // etb(file2read%fn))
         end if
         file2read%exist= .true.
         open(file2read%lun,file=etb(file2read%fn)//'deleteme',&
              iostat=opstat)
         if (opstat.ne.0) then
            write(lun,*) ' problems creating or accessing folder directory ', &
                 etb(file2read%fn)
            stop ' simulation ended'
         end if
         close(file2read%lun)
         call execute_command_line ('rm ' // etb(file2read%fn)//'deleteme')  
      end if

    end subroutine check_open
    
  end subroutine IO_initialize

  !-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type IOfdescr)
  !> Prints content of a variable of type IOfdescr
  !>
  !> usage:
  !>    call 'var'%info(lun)
  !>
  !> where:
  !> \param[in] lun: output unit
  !-------------------------------------------------------------
  subroutine IO_print_info(this, lun)
    class(IOfdescr) :: this
    integer :: lun

    write(lun,*) ' IO Units used in the code'
    
    write(lun,*) ' '
    write(lun,*) ' Standard units'
    call this%term%info(lun)
    call this%stderr%info(lun)
    call this%stdin%info(lun)
    call this%stdout%info(lun)

    write(lun,*) ' '
    write(lun,*) ' Simulation units'
    write(lun,*) ' Input units'
    call this%input_folder%info(lun)
    call this%control%info(lun)
    write(lun,*) ' Spatial discretiazion  units '
    call this%grid%info(lun)
    call this%subgrid%info(lun)
    call this%parents%info(lun)
    call this%pflux%info(lun)
    call this%pode%info(lun)
    call this%pmass%info(lun)
    call this%decay%info(lun)
    call this%tdens0%info(lun)
    call this%kappa%info(lun)
    call this%rhs_integrated%info(lun)
    write(lun,*) ' Output units'
    call this%output_folder%info(lun)
    call this%statistic%info(lun)
    call this%result%info(lun)
    call this%tdens_out%info(lun)
    call this%pot_out%info(lun)
    call this%nrmgraddyn_out%info(lun)
    call this%timefun%info(lun)    
    call this%linear_sys%info(lun)    
    call this%debug%info(lun)
    write(lun,*) ' Optional files units' 
    call this%optdens%info(lun)
    call this%rhs_grid_integrated%info(lun)
    call this%penalty%info(lun)
    call this%penalty_weight%info(lun)
    call this%dirichlet_bc%info(lun)
    call this%penalty_factor%info(lun)
    call this%lambda%info(lun)


    
  end subroutine IO_print_info
  
  !-------------------------------------------------------------
  !> Static destructor.
  !> (public procedure for type IOfdescr)
  !> Deallocate a variable of type IOfdescr and close I/O units
  !>
  !> usage:
  !>    call 'var'%kill(lun_err)
  !>
  !> where:
  !> \param[in] lun_err -> integer. I/O unit for error message
  !-------------------------------------------------------------
  subroutine IO_close_all_units(this,lun_err)
    class(IOfdescr),intent(inout) :: this
    integer, intent(in)           :: lun_err
    ! input units
    close(1)
    call this%input_folder%kill(lun_err)
    call this%control%kill(lun_err)
    call this%grid%kill(lun_err)
    call this%subgrid%kill(lun_err)
    call this%pflux%kill(lun_err)
    call this%pode%kill(lun_err)
    call this%pmass%kill(lun_err)
    call this%decay%kill(lun_err)
    call this%tdens0%kill(lun_err)
    call this%rhs_integrated%kill(lun_err)
    call this%kappa%kill(lun_err)
    ! output units 
    call this%output_folder%kill(lun_err)
    call this%statistic%kill(lun_err)
    call this%result%kill(lun_err)
    call this%tdens_out%kill(lun_err)
    call this%pot_out%kill(lun_err)
    call this%nrmgraddyn_out%kill(lun_err)        
    call this%timefun%kill(lun_err)
    call this%linear_sys%kill(lun_err)
    call this%debug%kill(lun_err)
    ! optional 
    call this%optdens%kill(lun_err)
    call this%rhs_grid_integrated%kill(lun_err)
    call this%penalty%kill(lun_err)
    call this%penalty_weight%kill(lun_err)
    call this%dirichlet_bc%kill(lun_err)
    call this%penalty_factor%kill(lun_err)
    call this%lambda%kill(lun_err)


  end subroutine IO_close_all_units  
  
  !>-------------------------------------------
  !> Short-cut fucntion to find the logical unit
  !> associate to a certain file
  !>-------------------------------------------
  function find_lun(this,string) result (lun)
    use Globals
    implicit none
    class(IOfDescr),    intent(in   ) :: this
    character(len=256), intent(inout) :: string
    integer :: lun

    select case ( etb(erase_comment(string)) )
    case ('stderr')
       lun=this%stderr%lun
    case ('stdout')
       lun=this%stdout%lun
    case ('debug')
       lun=this%debug%lun
    case ('statistic')
       lun=this%statistic%lun
    case default
       lun=-1000
    end select
  end function find_lun


  !>------------------------------------------------------------
  !> Static constructor.
  !> (procedure public for type OdeInp)
  !> Instantiate (allocate if necessary)
  !> and initialize variables of type OdeInp
  !>
  !> usage:
  !>     call 'var'%init(IOfiles,grid,subgrid,tzero)
  !>
  !> where:
  !> \param[in] IOfiles -> type(IOfdescr). I/O file information
  !> \param[in] ntdens  -> integer. Dimension of Tdens-variables
  !> \param[in] npot    -> integer. Dimension of Pot-variables
  !> \param[in] tzero   -> integer. Initial time of simulation
  !<-------------------------------------------------------------
  subroutine OdeInp_init(this,&
       IOfiles,lun_info,&
       ntdens,npot,id_subgrid,&
       tzero)
    implicit none
    class(OdeInp),     intent(inout) :: this
    type(IOfdescr),    intent(in   ) :: IOfiles
    integer,           intent(in   ) :: lun_info
    integer,           intent(in   ) :: ntdens
    integer,           intent(in   ) :: npot
    integer,           intent(in   ) :: id_subgrid
    real(kind=double),optional, intent(in   ) :: tzero

    !local
    logical :: rc,endtdens0
    integer :: nred
    integer :: stderr, stdout, lun, res 
    integer :: i
    character(len=256) :: fname
    character(len=1256) ::msg
    real(kind=double), allocatable :: ones(:), zeros(:)
    real(kind=double) :: just_one(1)
    real(kind=double) :: just_zero(1)
    
    allocate(ones(ntdens),zeros(ntdens),stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_alloc , 'OdeInp_init', &
            'work array ones')
    ones=one
    zeros= zero
    just_one(1)=one
    just_zero(1)=zero


    ! logic units
    stderr    = IOfiles%stderr%lun
    stdout    = IOfiles%stdout%lun

    
    ! initial time
    if ( present(tzero) ) this%tzero = tzero

    ! dimensions
    this%ntdens     = ntdens
    this%npot       = npot
    this%id_subgrid = id_subgrid
    
    write(msg,*) '****** BASIC INPUTS ********************'
    write(lun_info,*) etb(msg)

    !>------------------------------------------------------------------
    !> Init mandatory files
    !>------------------------------------------------------------------
    ! rhs_integrated (Forcing integrated with P1 Galerkin function)
    if ( id_subgrid .eq. 0)  then
       call this%rhs_integrated%init(stderr,&
            IOfiles%rhs_grid_integrated, 1,npot)
    else
       ! used subgrid
       call this%rhs_integrated%init(stderr,&
            IOfiles%rhs_integrated, 1,npot)

       write(msg,*) 'Rhs integrated read from', etb(IOfiles%rhs_integrated%fn)
       write(lun_info,*)    etb(msg)
    end if


    !  Initial tdens
    call this%tdens0%init(stderr, IOfiles%tdens0, 1,ntdens,ones)
    if ( IOfiles%tdens0%exist ) then
       call this%tdens0%set(stderr,IOfiles%tdens0, tzero, endtdens0)
       write(msg,'(a,a,a,1pe9.2,a,1pe9.2)') 'Initial tdens0 read from: ', etb(IOfiles%tdens0%fn), &
            ' min=',minval(this%tdens0%TDactual),&
            ' max=',maxval(this%tdens0%TDactual)
    else
       write(msg,*) 'File', etb(IOfiles%tdens0%fn), ' does not exist. Tdens0 = 1.0 '
    end if
    write(lun_info,*)    etb(msg)
    
    ! Pflux exponent
    call this%pflux%init(stderr, IOfiles%pflux, 1,1)
    if ( this%pflux%steadyTD ) then
       write(msg,'(a,a,a,1pe9.2)' ) 'Steady pflux (beta)  read from: ', etb(IOfiles%pflux%fn),' value=',this%pflux%TDActual(1,1)    
    else
       write(msg,*) 'Not Steady pflux (beta)  read from: ', etb(IOfiles%pflux%fn)
    end if
    write(lun_info,*)    etb(msg)

    ! PODE exponent
    call this%pode%init(stderr, IOfiles%pode, 1,1)
    if ( this%pode%steadyTD ) then
       write(msg,'(a,a,a,1pe9.2)' ) 'Steady pode (beta)  read from: ', etb(IOfiles%pode%fn),' value=',this%pode%TDActual(1,1)
    else
       write(msg,*) 'Not Steady pode           read from: ', etb(IOfiles%pode%fn)
    end if
    write(lun_info,*)    etb(msg)


    ! Pmass exponent
    call this%pmass%init(stderr, IOfiles%pmass, 1,1)
    if ( this%pmass%steadyTD ) then
       write(msg,'(a,a,a,1pe9.2)' ) &
            'Steady pmass (alpha) read from: ', etb(IOfiles%pmass%fn),&
            ' value=',this%pmass%TDActual(1,1)
    else
       write(msg,*) 'Not Steady pmass (alpha) read from: ', etb(IOfiles%pmass%fn)
    end if
    write(lun_info,*)    etb(msg)


    ! Time decay 
    call this%decay%init(stderr, IOfiles%decay, 1,1,just_one)
    if ( IOfiles%kappa%exist ) then
        write(msg,'(a,a,a,1pe9.2)' ) &
            'Steady decay         read from: ', etb(IOfiles%decay%fn),&
            ' value=',this%decay%TDActual(1,1)
    else
       write(msg,*) 'File', etb(IOfiles%decay%fn), ' does not exist. decay = 1.0 '
    end if
    write(lun_info,*)    etb(msg)

!!$    ! weight penalty factor
!!$    call this%lambda%init(stderr, IOfiles%lambda,1,1,just_zero)
!!$    if ( IOfiles%lambda%exist ) then
!!$       write(msg,*) 'Relaxation lambda read from : ', etb(IOfiles%lambda%fn)
!!$    else
!!$       write(msg,*) 'File', etb(IOfiles%lambda%fn), ' does not exist. Default = 0.0'
!!$    end if
!!$    write(IOfiles%stdout%lun,*)    etb(msg)
!!$    write(IOfiles%statistic%lun,*) etb(msg)

    
    ! Spatial decay kappa
    call this%kappa%init(stderr, IOfiles%kappa, 1,ntdens,ones)
    if ( IOfiles%kappa%exist ) then
       if (  this%kappa%steadyTD  ) then
          write(msg,'(a,a,a,1pe9.2,a,1pe9.2)') 'Steady kappa read from: ', etb(IOfiles%kappa%fn), &
               ' min=',minval(this%kappa%TDactual),&
               ' max=',maxval(this%kappa%TDactual)
       else
          write(msg,'(a,a)') 'Steady kappa read from: ', etb(IOfiles%kappa%fn)
       end if
    else
       write(msg,*) 'File', etb(IOfiles%kappa%fn), ' not found. kappa = 1.0 '
    end if
    write(lun_info,*)    etb(msg)
    
        
    
    !
    ! optional files
    !
    
    !
    ! optdens = (optimal transport density)
    !
    if (IOfiles%optdens%exist ) then
       this%optdens_exists = .True.
       
       !  Initial tdens
       call this%optdens%init(stderr, IOfiles%optdens, 1,ntdens) 
    end if
    if ( IOfiles%optdens%exist ) then
       write(msg,*) 'Optimal Tdens read from: ', etb(IOfiles%optdens%fn)
    else
       write(msg,*) 'Optimal Tdens not found. File', etb(IOfiles%optdens%fn), ' does not exist'
    end if
    write(lun_info,*)    etb(msg)
    

    !
    ! dirichlet = (optimal dirichlet condition)
    !
    if ( IOfiles%dirichlet_bc%exist ) then
       write(msg,*) 'Dirichlet BC read from: ', etb(IOfiles%dirichlet_bc%fn)
    else
       write(msg,*) 'File', etb(IOfiles%dirichlet_bc%fn), ' does not exist. Zero Neumann BC '
    end if
    write(lun_info,*)    etb(msg)


    if (IOfiles%dirichlet_bc%exist ) then
       this%dirichlet_exists = .True.
       !
       ! dirichlet
       !
       call this%dirichlet_bc%init(stderr, IOfiles%dirichlet_bc, 2, npot)

       allocate(&
            this%dirichlet_nodes(npot),&
            this%dirichlet_values(npot),&
            stat=res)
       if(res .ne. 0) rc = IOerr(stderr, err_inp , 'OdeInp_init', &
            ' type odein member dirichlet_nodes, dirichlet_values')
    else
       this%ndir = 0
       allocate(&
            this%dirichlet_nodes(this%ndir),&
            this%dirichlet_values(this%ndir),&
            stat=res)
       if(res .ne. 0) rc = IOerr(stderr, err_inp , 'OdeInp_init', &
            ' type odein member dirichlet_nodes, dirichlet_values')
  
    end if

    !
    ! penalization 
    ! 
    if (IOfiles%penalty%exist ) this%penalty_exists = .True.
    
    write(msg,*) '******** Penalization ************* '
    write(lun_info,*)    etb(msg)
    
    ! penalty
    call this%penalty%init(stderr, IOfiles%penalty, 1,ntdens,zeros)
    if ( IOfiles%penalty%exist ) then
       write(msg,*) 'Penalty read from: ', etb(IOfiles%penalty%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty%fn), ' does not exist. No penalization'
    end if
    write(lun_info,*)    etb(msg)
    
    
    ! weight penalty
    call this%penalty_weight%init(stderr, IOfiles%penalty_weight,1,ntdens,zeros)
    if ( IOfiles%penalty_weight%exist ) then
       write(msg,*) 'Penalty weight read from: ', etb(IOfiles%penalty_weight%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty_weight%fn), ' does not exist. No penalization'
    end if
    write(lun_info,*)    etb(msg)


    ! weight penalty factor
    call this%penalty_factor%init(stderr, IOfiles%penalty_factor,1,1,just_one)
    if ( IOfiles%penalty_weight%exist ) then
       write(msg,*) 'Penalty factor read from: ', etb(IOfiles%penalty_factor%fn)
    else
       write(msg,*) 'File', etb(IOfiles%penalty_factor%fn), ' does not exist. Default = 1.0'
    end if
    write(lun_info,*)    etb(msg)
    
    if( .not. present( tzero) ) this%tzero=max(&
         this%pflux%TDtime(1),&
         this%pmass%TDtime(1),&
         this%decay%TDtime(1),&
         this%rhs_integrated%TDtime(1),&
         this%kappa%TDtime(1))

    
    deallocate(ones,zeros,stat=res)
    if(res .ne. 0) rc = IOerr(stderr, err_dealloc , 'OdeInp_init', &
         'work array ones')

    
  end subroutine OdeInp_init


  !>-------------------------------------------------------------
  !> Static destructor.
  !> (procedure public for type OdeInp
  !> Deallocate array in type OdeInp
  !>
  !> usage:
  !>     call 'var'%kill(lun)
  !>
  !> where:
  !> \param[in] lun -> integer. I/O unit for error message output
  !<-----------------------------------------------------------
  subroutine OdeInp_kill(this, lun)
    implicit none
    class(OdeInp), intent(inout) :: this
    integer,       intent(in   ) :: lun
    ! local vars
    integer :: res
    logical :: rc

    !> Mandatory varaibles
    ! spatial varible
    call this%tdens0%kill(lun)
    call this%pflux%kill(lun)
    call this%pode%kill(lun)
    call this%pmass%kill(lun)
    call this%decay%kill(lun)
    call this%kappa%kill(lun)
    call this%rhs_integrated%kill(lun)
    
    !
    ! optional files
    !
    if ( this%optdens_exists ) then
       call this%optdens%kill(lun)
    end if

    !
    ! optional files
    !
    if ( allocated(this%dirichlet_values) ) then
       deallocate(&
         this%dirichlet_nodes,&
         this%dirichlet_values,&
         stat=res)
       if(res .ne. 0) rc = IOerr(lun, err_dealloc, 'OdeInp_kill', &
            ' type OdeInp member dirichlet_nodes dirichlet_values',res)
    end if
    
    !
    ! optinal penalty kill
    !
    if ( this%penalty%built)        call this%penalty%kill(lun) 
    if ( this%penalty_weight%built) call this%penalty_weight%kill(lun)
    if ( this%penalty_factor%built) call this%penalty_factor%kill(lun)

        
  end subroutine OdeInp_kill


  !>-------------------------------------------------------------
  !> Info procedure.
  !> (public procedure for type OdeInp)
  !> Prints content of a variable of type OdeInp
  !> 
  !> usage:
  !>     call 'var'%info(lun)
  !>
  !> where:
  !> \param[in] lun: integer. output unit
  !>
  !<-------------------------------------------------------------
  subroutine OdeInp_info(this, lun, nsample)
    use Globals
    implicit none
    ! vars
    class(OdeInp), intent(in) :: this
    integer,       intent(in) :: lun
    integer,       intent(in) :: nsample
    !local vars
    integer :: i

    write(lun,*) ' '
    write(lun,*) ' Info: Ode Input'
    write(lun,*) ' Initial time      =', this%tzero 
    write(lun,*) ' Inputs'
    call this%tdens0%info(lun, nsample)
    call this%pflux%info(lun, nsample)
    call this%pmass%info(lun, nsample)
    call this%decay%info(lun, nsample)
    call this%kappa%info(lun, nsample)
    call this%rhs_integrated%info(lun, nsample)
    
    !
    if ( this%penalty%built) then
       call this%penalty%info(lun, nsample)
       call this%penalty_weight%info(lun, nsample)
    end if
    
  end subroutine OdeInp_info
  
  !>-------------------------------------------------------------
  !> Procedure to set all data at given time .
  !> (public procedure for type OdeInp)
  !> 
  !> usage:
  !>     call 'var'%set(IOfiles,time,endfile)
  !>
  !> where:
  !> \param[in] IOfiles -> type(IOfdescr). I/O file information
  !> \param[in] time    -> real. Time required
  !> \param[in] endfile -> logical. Gives True if one file reached 
  !>                         the end of file.
  !<-------------------------------------------------------------
  subroutine OdeInp_set(this, IOfiles, time,endfile)
    implicit none
    class(OdeInp),     intent(inout) :: this
    type(IOfdescr),    intent(in   ) :: IOfiles
    real(kind=double), intent(in   ) :: time
    logical,           intent(inout) :: endfile
    ! local
    logical :: rc
    integer :: lun_err
    integer :: inode,idir
    integer :: info
    logical :: endtdens0
    logical :: endpflux, endpode,endpmass, enddecay 
    logical :: endkappa, endrhs
    logical :: end2penalty, end2penalty_weight
    logical :: end_dirichlet
    logical :: end_lambda
    real(kind=double) :: imbalance,data(2)
    
    lun_err = IOfiles%stderr%lun

    
    call this%pflux%set(lun_err,IOfiles%pflux, time, endpflux)
    call this%pode%set(lun_err,IOfiles%pode , time, endpode)
    call this%pmass%set(lun_err,IOfiles%pmass, time, endpmass)
    call this%decay%set(lun_err,IOfiles%decay, time, enddecay)
    call this%kappa%set(lun_err,IOfiles%kappa, time, endkappa)

    info = 0
    call this%rhs_integrated%set(lun_err,&
         IOfiles%rhs_integrated, time,endrhs,info)
    if (info.ne.0) then
       rc = IOerr(lun_err, wrn_val , 'OdeInp_set', &
                  'error in reading rhs_integrated')
    end if

    if ( this%id_subgrid .eq. 0 ) then
       call this%rhs_integrated%set(lun_err,&
            IOfiles%rhs_grid_integrated, time,endrhs)
    else
       call this%rhs_integrated%set(lun_err,&
            IOfiles%rhs_integrated, time,endrhs)
    end if

    endfile =(endpflux .or. endpode .or. endpmass.or. enddecay .or. endkappa .or. endrhs)
    
    !
    ! read dirichlet boundary condition if imposed
    !
    if ( this%dirichlet_exists) then
       !
       ! read data
       !
       call this%dirichlet_bc%set(lun_err,&
            IOfiles%dirichlet_bc, time,end_dirichlet)
       !
       ! get number, indeceds of the dirichlet nodes and the value
       !
       this%ndir=0
       do inode=1,this%dirichlet_bc%ndata
          data=this%dirichlet_bc%TDActual(:,inode)
          if ( data(1) > zero ) then
             this%ndir=this%ndir+1
             this%dirichlet_nodes(this%ndir)=inode
             this%dirichlet_values(this%ndir)=data(2)
          end if
       end do
       
       endfile=endfile .or. end_dirichlet
    end if

    !
    ! check consistency of the rhs_integrated and boundary condition
    !
    if ( .not. this%dirichlet_exists) then
       if ( this%ndir .eq. 0) then
          imbalance = sum(this%rhs_integrated%TDActual(1,:))
          if(abs(imbalance) .ge. 1.0d-12 ) then
             rc = IOerr(lun_err, wrn_val , 'OdeInp_set', &
                  'type odeinp rhs not balanced ')
             write(lun_err,*) 'imbalance =', imbalance
          end if
       end if
    else
       do idir=1,this%ndir
           inode = this%dirichlet_nodes(idir)
           this%dirichlet_values(idir)=this%dirichlet_bc%TDActual(2,inode)
       end do       
    end if


    !
    if ( this%penalty%built) then
       call this%penalty%set(lun_err,IOfiles%penalty, time, end2penalty)
       call this%penalty_weight%set(lun_err,IOfiles%penalty, time, end2penalty_weight)
       endfile = (endfile .or. end2penalty .or. end2penalty_weight)
    end if
    
  end subroutine OdeInp_set


  !>---------------------------------------------------------------------
  !> Procedure to transfer all input data to 
  !> DmkINputs data type
  !> (public procedure for type OdeInp)
  !> 
  !> usage:
  !>     call 'var'%set(input_data)
  !>
  !> where:
  !> \param[inout] ode_inputs -> type(DmkInputs). Ode's input container
  !<---------------------------------------------------------------------
  subroutine set_DmkInputs(this,input_data)
    use DmkInputsData
    implicit none
    class(OdeInp),     intent(in   ) :: this
    type(DmkInputs),   intent(inout) :: input_data

    !local
    integer :: i

    !
    ! copy all current data in the given slot (first or second)
    !
    input_data%time = this%pflux%time

    ! copy all mandatory data 
    input_data%pflux = this%pflux%TDactual(1,1)
    input_data%pmass = this%pmass%TDactual(1,1)
    input_data%pode  = this%pode%TDactual(1,1)

    do i=1,input_data%npot
       input_data%rhs(i) = this%rhs_integrated%TDactual(1,i)
    end do

    !
    ! copy optional data
    !

    ! spatial end temporal decay
    if (this%decay%built) then
       input_data%decay = this%decay%TDactual(1,1)
    end if
    if (this%kappa%built) then
       do i=1,input_data%ntdens
          input_data%kappa(i) = this%kappa%TDactual(1,i)
       end do
    end if

    ! tdens penalization
    if (this%penalty%built) then
       do i=1,input_data%ntdens
          input_data%penalty(i) = this%penalty%TDactual(1,i)
       end do
    end if
    if (this%penalty_weight%built) then
       do i=1,input_data%ntdens
          input_data%penalty_weight(i) = this%penalty_weight%TDactual(1,i)
       end do
    end if
    if (this%penalty_factor%built) input_data%penalty_factor = this%penalty_factor%TDactual(1,1)

    ! 
    if ( this%dirichlet_exists) then
       input_data%ndir   = this%ndir
       do i=1,this%ndir
          input_data%dirichlet_nodes(i)=this%dirichlet_nodes(i)
          input_data%dirichlet_values(i)=this%dirichlet_values(i)
       end do
    end if


  end subroutine set_DmkInputs

    
end module IOdefs
