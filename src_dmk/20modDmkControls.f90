!> Defines simulation control parameters
module DmkControls
  use KindDeclaration
  use Globals
  implicit none
  private
  public :: DmkCtrl
  type :: DmkCtrl
     !>--------------------------------------------------------
     !> Information Controls
     !>--------------------------------------------------------
     !> Control debug output. if > 0 full debug
     !>  0   :: no debug
     !>  1   :: print main steps (initialization)
     !>  2   :: print inputs and controls
     !>  12  :: 1 + 2 
     integer :: debug=0
     !> Flag to control print information on state of system
     !> energy lyapunov etc.
     integer :: info_state = 1
     !> Flag to print info in update procedure
     !> 0   :: no print
     !> 1   :: print basic output (linear solver check)
     !> 2   :: inputs used
     integer :: info_update = 1
     !> control mesh output. if =0 prints mesh and stops.
     !>                      if =1 normal simulation
     integer :: info_newton
     !> Format for integer output 
     character(len=20) :: iformat='I12'
     !> Format for real output
     character(len=20) :: rformat='1e18.10'
     !> Format for integer output 
     character(len=256) :: iformat_info='I9'
     !> Format for real output
     character(len=256) :: rformat_info='1pe9.2'
     !>--------------------------------------------------------
     !> Ode Global Controls
     !>--------------------------------------------------------
     !> Flag to define transformation of Algebraic DMK
     !> Used only in new_implicit_euler_newton_gfvar (time_discretization_scheme=1)
     !> trans=0 :: tdens=gfvar
     !> trans=1 :: tdens=gfvar^2/4
     !> trans=2 :: tdens=exp(gfvar)
     integer :: trans=1
     !> lower bound for tdens
     real(kind=double) :: min_tdens=1.0d-10
     !> Flag for selection of tdpot variable
     !> 0 :: selection off
     !> 1 :: secection on tdens varaible
     !>      pot selection is done :
     !>      ADMK: remove zero coloumns after elimination of   
     integer :: selection=0
     !> Selection parameter for tdens
     real(kind=double) ::  threshold_tdens = 0.0d0
     !>--------------------------------------------------------
     !> Time stepping approach
     !>--------------------------------------------------------
     !> Time discretization 
     !> Control the time-step discretization used
     !> time_stepping_scheme == 1 => explicit Euler 
     !> time_stepping_scheme == 2 => implicit Euler Newton
     !> time_stepping_scheme == 3 => accelerate explicit Euler
     integer :: time_discretization_scheme=1
     !> Tolerance in fix_point scheme for implicit time stepping 
     real(kind=double) :: tolerance_nonlinear=1e-8
     !> Tolerance in fix_point scheme for implicit time stepping 
     real(kind=double) :: tolerance_linear_inexact_newton=1.0d-4
     !> Maximum number of time iterations
     integer :: max_nonlinear_iterations=10
     real(kind=double) :: relative_growth_factor=1.0d3
     real(kind=double) :: absolute_growth_factor=1.0d8
     integer :: max_nrestart_invert_jacobian=1
     !> Maximum number of time iterations
     integer :: max_restart_update=15
     !> Maximum number of time iterations
     integer :: max_restart_invert_jacobian=1
     !> Limit for dump parameter for Newton increment
     real(kind=double) :: relax_limit=5.0d-2
     !>--------------------------------------------------------
     !>  Time controls
     !>--------------------------------------------------------
     !> Initial time
     real(kind=double) :: tzero=zero
     !> Maximum number of time iterations
     !> max_time_it=0 => Only one elliptic equation is solved
     integer :: max_time_iterations=100
     !> Convergence tolerances
     !> Tolerance of var_tdens 
     real(kind=double) :: tolerance_system_variation=1.0d-4
     !> Norm for tdens
     real(kind=double) :: norm_tdens=2.0d0
     !> Time step control 
     !> Control of the time step
     !> flag_deltat_control = 0 => constant time step
     !> flag_deltat_control = 1 => increasing time step, with factor 
     !>                   "exp_rate" and upper bound "bound_deltat"
     !> flag_deltat_control = 3 => adaptive deltat
     integer :: deltat_control=1
     !> Time step
     real(kind=double) :: deltat=0.1d0
     !> Expansion rate in id_ctrl_time == 2
     real(kind=double) :: deltat_expansion_rate=1.05d0
     !> Upper bound for deltat in id_ctrl_time == 2,3,4
     real(kind=double) :: deltat_upper_bound=1.0d0
     !> Lower bound for deltat in id_ctrl_time == 3,4
     real(kind=double) :: deltat_lower_bound=1.0d-3
     !> Controls for Algebraic Multigrid
     !> Lower bound for W22= block 2-2 jacobian
     !> Used deltat_control=3
     real(kind=double) :: epsilon_W22=1.0d-4
     !>------------------------------------------------------
     ! Linear solver Controls
     !>------------------------------------------------------
     !> Solver scheme
     !> AGMG   :: AGgregation based Multigrid by Notay, Napov
     !> GAGMG  :: AGMG for Laplaician graph 
     !> KRYLOV :: Iterative Krylov methods (see krylov_scheme)  
     character(len=20) :: linear_solver='KRYLOV'
     !> Solver scheme
     !> PCG
     !> BICGSTAB
     !> GMRES
     !> MINRES
     character(len=20) :: krylov_scheme='BICGSTAB'
     !> I/O err msg. unit
     integer :: linear_solver_lun_err=0
     !> I/O out msg. unit
     integer :: linear_solver_lun_out=6
     !> Integer identifying the Preconditinoer
     !>     iexit=0 => exit on absolute residual               
     !>     iexit=1 => exit on |r_k|/|b|     
     integer :: iexit=1
     !> Number of maximal iterations for iterative solver
     integer :: imax=1000
     !> Flag for printing option for itereative solver
     integer :: iprt=0
     !> Flag for starting solution
     !>  isol=0 => SOL      is the initial solution for pcg
     !>  isol=1 => PREC*RHS is the initial solution for pcg
     integer :: isol=0
    !> Linear Solver Tolerance
     real(kind=double) :: tolerance_linear_solver=1e-13
     !> Ortogonalized w.r.t. to given vectors 
     !> (usally the kernel of the matrix)
     integer :: iort=0
     !> Number of krylov directions saved in GMRES algorithm
     !> Used only for GMRES
     integer :: nrestart=20
     !> Flag to active to print steps for debugging
     !> debug=0 => no print 
     !> debug=1 => print steps
     integer :: debug_solver=0
     
     !> Diagonal scaling of linear system
     integer :: id_diagonal_scaling
     !> Diagonal scaling of linear system
     integer :: id_singular=0
     !>  Relaxation parameter add to tdens 
     real(kind=double) :: lambda
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: alpha_uzawa
     !>  Alpha relaxation parameter in uzawa interations 
     real(kind=double) :: omega_uzawa
     !>------------------------------------------------------
     !> Preconditioner controls 
     !>------------------------------------------------------
     !> Integer identifying the Preconditinoer
     !>     'identity'  P=Id (nothing is done)
     !>     'diag'      P=diag(A)^{-1}
     !>     'IC'        P=(U^{T}U)^{-1} with A~(U^T)U
     !>     'ILU'       P=(LU)^{-1} with A~LU
     !>     'C'         P=(M^{T}M)^{-1} with A=(U^T)U
     !>     'LU'        P=(LU)^{-1} with A=LU
     !>     'AGMG'      P=(A^-1) with agmg 
     character(len=20) :: prec_type='ILU'
     !> Number of maximal extra-non-zero tem for
     !> row in the construction of IC_{fillin}
     integer :: n_fillin=30
     !> Tolerance for the dual drop procedure
     !> in the construction of IC_{fillin} (iprec=4)
     real(kind=double) :: tol_fillin=1.0d-5
     !> Control for factorization breakdown
     !> 0 = stop factorization 
     !> 1 = try to recover factorization 
     !> 2 = try recovering in regime of "normal" number
     integer :: factorization_job=2
     !> Max number of internal iterations for iterative preconditioner
     integer :: imax_internal=1
     !> Internal tolerance for iterative preconditioner
     real(kind=double) :: tol_internal=1.0d-5     
     !> Maximal number of BFGS update for triangular preconditioner
     integer :: max_bfgs=10
     !>-------------------------------------
     !> Prec. Buffering
     !>-------------------------------------
     !> Flag for buffering the calculation of the prec.
     !>  id_buffer_prec=0 => build at each iteration (no buffer)
     !>  id_buffer_prec=1 => build at each iteration (no buffer)
     !>  id_buffer_prec=2 => build at each iteration (no buffer)
     !>  id_buffer_prec=3 => build at each iteration (no buffer)
     integer :: id_buffer_prec
     !> Reference Number of iteration 
     !> Used for id_buffer_prec=1,...
     integer :: ref_iter 
     !> Factor of growth of iterations
     real(kind=double) :: prec_growth
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax4prec=1.0d-7
     !> relaxation parameter add to the diagonal of the 
     !> stiffness matrix before building the preconditioner
     real(kind=double) ::  relax_direct=1.0d-7
     !>-------------------------------------
     !> Augemented lagrangian controls
     !>-------------------------------------
     !> Gamma relaxation 
     real(kind=double) :: gamma=1.0d0
     !> how to build hatS
     integer :: id_hatS=2
     !>---------------------------------------------------------------
     !> Newton controls
     !>---------------------------------------------------------------
     !> Number of broyden update of jacobian preconditioner
     integer :: nbroyden=0
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: newton_method=12
     integer :: use_newton_method
     !> Method to solve the linear system of the Newton method
     !> reduced_jacobian = 0 => solve the full system
     !> reduced_jacobian = 1 => solve the reduced system
     integer :: reduced_jacobian=1
     !> Preconditioner strategy for linear system in newton scheme
     !> work in combination with reduced_jacobian
     !> ( reduced_jacobian + prec_newton ) =     
     !> 0 + 1 => full system + P=diag (stiff)^-1, D2^-1)
     !> 0 + 2 => full system + symmetization + P=mixed
     !> 0 + 3 => full system + P=mixed
     !> 0 + 4 => full system + P=triangular
     !> 1 + 1 => reduced system + P = (stiff)^-1) 
     !> 0 + 2 => reduced system + P = (stiff+ deltat BT D1 D2^{-1}C )^-1
     integer :: prec_newton=2
     !> Inecata Newton Method
     !> inexact_newton = 0 => off ( linear system solve with ctrl_sovler%tol)
     !> inexact_newton = 1 => on
     integer :: inexact_newton
     !>---------------------------------------------------------------
     !  Saving Procedure
     !>---------------------------------------------------------------
     !> control saving into vtk files
     !> id_save == 0 => no save
     !> id_save == 1 => save all
     !> id_save == 2 => save with fixed frequency
     !> id_save == 3 => save when the first digit of var_tdens changes
     !> Id. saving data into .dat
     integer :: id_save_dat=0
     !> Logical unit for Tdens
     !> Defualt (-1) will not to save to file
     integer :: lun_tdens=-1
     !> File name for Tdens
     character(1024) :: fn_tdens='tdens.dat'
     !> Logical unit for Potential
     !> Defualt (-1) will not to save to file
     integer :: lun_pot=-1
     !> File name for Tdens
     character(1024) :: fn_pot='pot.dat'
     !> frequency for control id_vtksave == 2
     !> File where to write tdens data
     type(file) :: file_tdens
     !> File where to write potential
     type(file) :: file_pot
     !> File where to write statistics
     type(file) :: file_statistics

     
     integer :: freq_dat=0
     !> Id. saving into matrix
     !> 0 - do not save
     !> 1 - save all
     !> 2 - save with frequency freq matrix
     integer :: id_save_matrix=0
     !> frequency for control id_matrixsave == 2 
     integer :: freq_matrix=10
     !>--------------------------------------------------------------
     !> INFO CONTROLS
     !>-------------------------------------------------------------
     !> Flag to control print of time evolution 
     integer :: info_time_evolution=1
     !> Flag to control print of time evolution 
     integer :: info_functional=1
     !> I/O err msg. unit
     integer :: lun_err=0
     !> I/O out msg. unit
     integer :: lun_out=6
     !> Defualt (-1) will not to save to file
     integer :: lun_statistics=-1
     !> File name for Tdens
     character(1024) :: fn_statistics='statistic.dat'
     !>---------------------------------------------------------------
     !> Controls dedicated to P1-P0 and P1-P1 approach for DMK
     !>---------------------------------------------------------------
     !> Flag for use of subgrid (0= no subgrid, 1= subgrid )
     integer :: id_subgrid
     !>---------------------------------------------------------------
     !> Controls dedicated to SPECTRAL-P0 approach for DMK
     !>---------------------------------------------------------------
     integer :: preassembly_matrices=1
     !> Degree of 1d-polynomial
     integer :: ndeg=15
     integer :: gfvar_approach
     character(len=1) :: separator_char='*'
     !> Flag for construction of preconditoner
     integer :: build_prec=1
     !> 
     integer :: threshold
   contains
     !> Procedure to print line
     procedure, public, pass :: should_I_save
     !> Procedure to print line
     procedure, public, pass :: readfromfile
     !> Fucntion to build a separtor with title
     !>for statistic file
     !> (public for type CtrlPrm)
     procedure, public, pass :: separator
     !> Standrd info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_info_update 
     !> Standrd info formatting after update
     !> (public for type CtrlPrm)
     procedure, public, pass :: print_info_tdpot
     !> 
     procedure, public, pass :: write_data2file
     !> Procedure to generate formats 
     !> based on  iformat, rformat
     !> (public for type CtrlPrm)
     procedure, public, pass :: formatting
  end type DmkCtrl

contains

   function separator(this,title) result (outstring)
    use Globals
    implicit none
    class(DmkCtrl),             intent(in   ) :: this
    character(len=*), optional, intent(in   ) :: title
    integer, parameter                        :: width=71
    integer, parameter                        :: start=10
    character(len=width) :: outstring
    !local 
    integer :: i,lentitle
    character(len=start) :: strstart
    character(len=width-start-2) :: strend
    character(len=1) :: symbol
    
     symbol=this%separator_char

    
    if ( present(title)) then
       lentitle=len(etb(title))
       do i=1,start
          write(strstart(i:i),'(a)') symbol
       end do

       do i=1,width-start-2-lentitle
          write(strend(i:i),'(a)') symbol
       end do
       outstring=strstart//' '//etb(title)//' '//strend
    else
       do i=1,width
          write(outstring(i:i),'(a)') symbol
       end do
    end if    
       
  end function separator
  


  subroutine should_I_save(&
       this,&
       save_test,&
       in_cycle,&
       itemp,&
       int_before,&
       current_var)
    use KindDeclaration
    implicit none
    class(DmkCtrl),    intent(in   ) :: this
    logical,           intent(inout) :: save_test
    logical,           intent(in   ) :: in_cycle
    integer,           intent(in   ) :: itemp
    integer,           intent(inout) :: int_before
    real(kind=double), intent(in   ) :: current_var

    !local 
    integer :: int_now

    save_test = .false.

    select case ( this%id_save_dat ) 
    case (1)
       !
       ! save each time step
       !
       save_test = .true.
    case (2)
       !
       ! save with give frequency and the last one
       !
       if ( (mod(itemp,this%freq_dat) == 0) .or. (.not. in_cycle) ) then
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    case (3)
       !
       ! save when variation change digit the last one
       !
       if ( ( itemp .ne. 0) ) then 
          int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
          if ( (int_now /= int_before) .or. (.not. in_cycle ) ) then
             int_before=int_now
             save_test = .true.
          end if
       else
          save_test = .true.
       end if
       if ( .not. in_cycle ) then
          save_test = .true.
       end if
    case (4)
       !
       ! save only last
       !
       if ( (itemp .ne. 0) .and. (.not. in_cycle )) then
          save_test = .true.
       end if
    end select

  end subroutine should_I_save

  !>-------------------------------------------------------------
  !> Procedure for reading controls from files
  !> (procedure public for type CtrlPrm, used in init)
  !> Instantiate (allocate if necessary)
  !> and initialize (by also reading from input file)
  !> variable of type CtrlPrm
  !>
  !> usage:
  !>     call 'var'%init(IOfiles, ctrl_global)
  !>
  !> where:
  !> \param[in] IOfiles     -> type(IOfdescr). I/O file information
  !<-------------------------------------------------------------
  subroutine readfromfile(this, lun)
    use Globals
    implicit none
    !vars
    class(DmkCtrl), intent(inout) :: this
    integer,        intent(in   ) :: lun
    ! local vars
    type(file) :: file2read
    integer :: stderr   
    integer :: n_fillin
    real(kind=double) :: tol_fillin
    character(len=256) fname,scheme_read
    character(len=256) input
    character(len=256) clean,string
    character(len=256) prec_type

    integer :: ieig=0
    integer :: nev=0
    integer :: ituning=0
    integer :: npres=0

    integer :: lun_err=6
    integer :: lun_out=6
    integer :: iexit=1
    integer :: imax=1000
    integer :: iprt=1
    integer :: isol=0
    integer :: i,res
    real(kind=double) :: tol_pcg=1.0d-12, tol_dacg
    integer :: iort
    integer :: info
    
    !>-----------------------------------------------------------
    !> Simulation control
    call find_nocomment(lun,stderr,input,fname,'debug')
    read(input,*) this%debug

    call find_nocomment(lun,stderr,input,fname,'info state')
    read(input,*) this%info_state

    call find_nocomment(lun,stderr,input,fname,'info state')
    read(input,*) this%info_update
  


    call find_nocomment(lun,stderr,input,fname,'iformat')
    clean = erase_comment(input)
    read(clean,*) this%iformat

    call find_nocomment(lun,stderr,input,fname,'rformat')
    clean = erase_comment(input)
    read(clean,*) this%rformat

    call find_nocomment(lun,stderr,input,fname,'iformat_info')
    clean = erase_comment(input)
    read(clean,*) this%iformat_info

    call find_nocomment(lun,stderr,input,fname,'rformat_info')
    clean = erase_comment(input)

    



    !*********************************************************
    ! Global controls
    !*********************************************************
    call find_nocomment(lun,stderr,input,fname,'id_subgrid')
    read(input,*) this%id_subgrid
    
    call find_nocomment(lun,stderr,input,fname,'min_tdens')
    read(input,*) this%min_tdens

    call find_nocomment(lun,stderr,input,fname,'lift_tdens')
    read(input,*,iostat=res) this%lambda
   
    !****************************************************
    ! Time discretization input
    !****************************************************
    call find_nocomment(lun,stderr,input,fname,'tzero')
    read(input,*) this%tzero

    call find_nocomment(lun,stderr,input,fname,'time_discretization_scheme')
    read(input,*) this%time_discretization_scheme


    call find_nocomment(lun,stderr,input,fname,'max_time_iterations')
    read(input,*) this%max_time_iterations

    call find_nocomment(lun,stderr,input,fname,'max_nonlinear_iterations')
    read(input,*) this%max_nonlinear_iterations
    
    ! time step controls
    call find_nocomment(lun,stderr,input,fname,'deltat_control')
    read(input,*) this%deltat_control

    call find_nocomment(lun,stderr,input,fname,'deltat')
    read(input,*) this%deltat

    call find_nocomment(lun,stderr,input,fname,'deltat_expansion_rate')
    read(input,*) this%deltat_expansion_rate

    call find_nocomment(lun,stderr,input,fname,'upper_bound_deltat')
    read(input,*) this%deltat_upper_bound

    call find_nocomment(lun,stderr,input,fname,'lower_bound_deltat')
    read(input,*) this%deltat_lower_bound

    ! Convergence tolerance
    call find_nocomment(lun,stderr,input,fname,'tolerance_system_variation')
    read(input,*) this%tolerance_system_variation

    call find_nocomment(lun,stderr,input,fname,'tol_nonlinear')
    read(input,*) this%tolerance_nonlinear

    !****************************************************
    ! Time discretization input
    !****************************************************
    call find_nocomment(lun,stderr,input,fname,'id_singular')
    read(input,*) this%id_singular
    
    call find_nocomment(lun,stderr,input,fname,'id_diagonal_scaling')
    read(input,*)this%id_diagonal_scaling

    
    ! linear solver method controls
    call find_nocomment(lun,stderr,input,fname,'scheme')
    read(input,*) clean
    this%krylov_scheme=erase_comment(clean)

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) this%linear_solver_lun_err

    call find_nocomment(lun,stderr,input,fname,'file_out')
    read(input,*) this%linear_solver_lun_out

    call find_nocomment(lun,stderr,input,fname,'iexit')
    read(input,*) this%iexit

    call find_nocomment(lun,stderr,input,fname,'imax')
    read(input,*) this%imax

    call find_nocomment(lun,stderr,input,fname,'iprt')
    read(input,*) this%iprt

    call find_nocomment(lun,stderr,input,fname,'isol')
    read(input,*) this%isol
    
    call find_nocomment(lun,stderr,input,fname,'tol_pcg')
    read(input,*) this%tolerance_linear_solver

    call find_nocomment(lun,stderr,input,fname,'iort')
    read(input,*) this%iort
    !***********************************************************
    ! Preconditioner
    !***********************************************************

    ! Standard prec

    call find_nocomment(lun,stderr,input,fname,'prec_type')
    read(input,*) this%prec_type

    call find_nocomment(lun,stderr,input,fname,'n_fillin')
    read(input,*) this%n_fillin

    call find_nocomment(lun,stderr,input,fname,'tol_fillin')
    read(input,*) this%tol_fillin


    
    ! Tuning
    call find_nocomment(lun,stderr,input,fname,'ieig')
    read(input,*) ieig
    
    call find_nocomment(lun,stderr,input,fname,'nev')
    read(input,*) nev
    
    call find_nocomment(lun,stderr,input,fname,'ituning')
    read(input,*) ituning
    
    
    ! ctrl_tuning%ieig=1 DACG

    call find_nocomment(lun,stderr,input,fname,'file_err')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%lun_out')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%iexit')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%imax')
    read(input,*) i
    
    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%iprt')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%isol')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'this%ctrl_dacg%tol_dacg')
    read(input,*) this%threshold_tdens

    this%threshold_tdens= tol_dacg


    ! ctrl_prec%ieig=2 LANCZOS
    call find_nocomment(lun,stderr,input,fname,'ctrl_tuning%npres')
    read(input,*) npres
    this%max_bfgs = npres

    

    ! buffering construction prec
    ! working variables controlled by id_buffer_prec
    call find_nocomment(lun,stderr,input,fname,'id_buffer_prec')
    read(input,*) this%id_buffer_prec

    call find_nocomment(lun,stderr,input,fname,'ref_iter')
    read(input,*) this%ref_iter

    call find_nocomment(lun,stderr,input,fname,'prec_growth')
    read(input,*) this%prec_growth
    
    !
    ! number of Boryden update
    !
    call find_nocomment(lun,stderr,input,fname,'nbroyden')
    read(input,*) this%nbroyden

    call find_nocomment(lun,stderr,input,fname,'reduced_jacobian')
    read(input,*) this%newton_method

    this%reduced_jacobian = this%newton_method/10
    this%prec_newton      = mod(this%newton_method,10)


    call find_nocomment(lun,stderr,input,fname,'inexact_newton')
    read(input,*) this%inexact_newton

    


    !***********************************************************
    ! Saving Data
    !***********************************************************
    ! saving dat
    call find_nocomment(lun,stderr,input,fname,'id_save_dat')
    read(input,*) this%id_save_dat
    call find_nocomment(lun,stderr,input,fname,'freq_dat')
    read(input,*) this%freq_dat
    call find_nocomment(lun,stderr,input,fname,'ifile_dat')
    read(input,*) i   

    ! saving matrix
    call find_nocomment(lun,stderr,input,fname,'id_save_matrix')
    read(input,*) this%id_save_matrix
    call find_nocomment(lun,stderr,input,fname,'freq_matrix')
    read(input,*) this%freq_matrix
    call find_nocomment(lun,stderr,input,fname,'ifile_matrix')
    read(input,*) i

    call find_nocomment(lun,stderr,input,fname,'factorization_job',info)
    if ( info .eq. 0) then
       read(input,*,iostat=res) this%factorization_job
       if ( res.ne.0) this%factorization_job =0
    else
       this%factorization_job =0
    end if

    call find_nocomment(lun,stderr,input,fname,'relaxiation_prec',info)
    if ( info .eq. 0) then
       read(input,*) this%relax4prec
    else
        this%relax4prec =0.0d0
    end if

    call find_nocomment(lun,stderr,input,fname,'nrestart_newton',info)
    if ( info .eq. 0) then
       read(input,*) this%max_restart_update
    else
        this%max_restart_update= 10
    end if

    call find_nocomment(lun,stderr,input,fname,'gamma',info)
    if ( info .eq. 0) then
       read(input,*) this%gamma
    else
        this%gamma = zero
    end if
    
    call find_nocomment(lun,stderr,input,fname,'var_tdens',info)
    if ( info .eq. 0) then
       read(input,*) this%norm_tdens
    else
        this%norm_tdens = 2.0d0
    end if
  contains 
    subroutine find_nocomment(lun,stderr,input,fname,var_name,info)
      use Globals
      integer,            intent(in   ) :: lun
      integer,            intent(in   ) :: stderr
      character(len=256), intent(in   ) :: fname
      character(len=256), intent(inout) :: input
      character(len=*),   intent(in   ) :: var_name
      integer, optional,intent(inout) :: info
      !local
      logical :: rc
      integer :: res

      character(len=256) clean
      character(len=1) first
      logical :: found
      
      if ( present(info)) info=0

      found = .false.
      do while( .not. found ) 
         read(lun,'(a)',iostat = res) input
         if(res .ne. 0) then
            !rc = IOerr(stderr, wrn_inp , 'Ctrl_read', &
            !     fname//'input text for member'//etb(var_name),res)
            if ( present(info)) info=-1
            return
         end if
            
         clean = etb(input)
         read(clean,*) first
         if ( ( first .eq. '!') .or. &
              ( first .eq. '%') .or. &
              ( first .eq. '#') ) then
            found=.false.
         else
            found=.true.
         end if 
      end do
    end subroutine find_nocomment
        
  end subroutine readfromfile


   subroutine write_data2file(ctrl,flag,tdpot)
    use TdensPotentialSystem
    use TimeInputs
    implicit none
    class(DmkCtrl), intent(inout) :: ctrl
    integer,        intent(in   ) :: flag
    type(tdpotsys), intent(inout) :: tdpot
    ! local
    logical :: save_data
    
    if (flag .eq. 1) then
       ! 
       ! write head of timedata file
       ! 
       if ( ctrl%id_save_dat >0 ) then
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'head',&
                  tdpot%time,tdpot%ntdens, tdpot%tdens, &
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             call writearray2file(ctrl%lun_err, 'head',&
                  tdpot%time, tdpot%npot, tdpot%pot, &
                  ctrl%lun_pot, ctrl%fn_pot)
          end if
          
       end if
    else
       !
       ! save at itermediate time 
       !
       call should_I_save(ctrl,tdpot%steady_state,&
            tdpot%time_iteration,&
            tdpot%int_before,&
            tdpot%system_variation,&
            save_data )

       if ( save_data  ) then
          if (ctrl%lun_tdens>0) then
             call writearray2file(ctrl%lun_err, 'body',&
                  tdpot%time,&
                  tdpot%ntdens, tdpot%tdens,&
                  ctrl%lun_tdens, ctrl%fn_tdens)
          end if
          if (ctrl%lun_pot>0) then
             call writearray2file(ctrl%lun_err, 'body',&
                  tdpot%time,&
                  tdpot%npot, tdpot%pot, &
                  ctrl%lun_pot, ctrl%fn_pot)
          end if
       end if

       if ( flag .eq. 0) then
          !
          ! optionally close logical unit for tdens and potential
          !
          if (ctrl%lun_statistics>0) then
             call ctrl%file_statistics%kill(ctrl%lun_err)
          end if
          if  ( ctrl%id_save_dat >0 ) then
             if (ctrl%lun_tdens>0) then
                call writearray2file(ctrl%lun_err, 'tail',&
                     tdpot%time, tdpot%ntdens, tdpot%tdens, &
                     ctrl%lun_tdens, ctrl%fn_tdens)
                call ctrl%file_tdens%kill(ctrl%lun_err)
             end if
             if (ctrl%lun_pot>0) then
                call writearray2file(ctrl%lun_err, 'tail',&
                     tdpot%time, tdpot%npot, tdpot%pot, &
                     ctrl%lun_pot, ctrl%fn_pot)
                call ctrl%file_pot%kill(ctrl%lun_err)
             end if
          end if
       end if
    end if

  contains
    subroutine should_I_save(this,&
         steady_state,&
         itemp,&
         int_before,&
         current_var,&
         save_test)
      use KindDeclaration
      implicit none
      type(DmkCtrl),    intent(in   ) :: this    
      logical,           intent(in   ) :: steady_state
      integer,           intent(in   ) :: itemp
      integer,           intent(inout) :: int_before
      real(kind=double), intent(in   ) :: current_var
      logical,intent(inout) :: save_test

      !local 
      integer :: int_now

      save_test = .false.

      select case ( this%id_save_dat ) 
      case (1)
         !
         ! save each time step
         !
         save_test = .true.
      case (2)
         !
         ! save with give frequency and the last one
         !
         if ( (mod(itemp,this%freq_dat) == 0) .or. ( steady_state) ) then
            save_test = .true.
         end if
      case (3)
         !
         ! save when variation change digit the last one
         !
         if ( ( itemp .eq. 0) ) then 
            save_test = .true.
         else
            int_now=int(current_var*10.0d0**(-int(log10(current_var))+1))
            if ( (int_now /= int_before) .or. ( steady_state ) ) then
               int_before=int_now
               save_test = .true.
            end if
         end if
      case (4)
         !
         ! save only last
         !
         if ( steady_state ) then
            save_test = .true.
         end if
      end select
      
    end subroutine should_I_save
  end subroutine write_data2file

  subroutine print_info_update(this,&
       flag_info,&
       info,&
       time_iteration,&
       nrestart)
    use Globals    
    implicit none
    class(DmkCtrl),    intent(in) :: this
    character(len=*),  intent(in) :: flag_info
    integer,           intent(in) :: info
    integer,           intent(in) :: time_iteration
    integer,           intent(in) :: nrestart
    
    
    !local
    character(len=77) :: result_update
    character(len=30) :: flag
    character(len=77) :: msg,msg_out
    character(len=256) :: out_format,str
    character(len=77) :: method
    

    flag=etb(flag_info) 

    select case (flag)
    case ( 'before_inside') 
       if (  nrestart .eq. 0) then
          if ((this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) ' '
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) ' ' 
          out_format='(a,I3,a,1pe8.2,a,I2)'
          write(str,out_format) 'UPDATE',time_iteration,' | TIME STEP = ', this%deltat
          write(msg,'(a)') this%separator(str) 
       else
          out_format='(a,I3,a,1pe8.2,a,I2)'
          write(str,out_format) 'UPDATE',time_iteration,'| TIME STEP = ', this%deltat,&
               ' RESTART ', nrestart
          write(msg,'(a)') this%separator(str)              
       end if
       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,'(a)') etb(msg)
       if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,'(a)') etb(msg)

    case ( 'after_inside') 
       if ( info .eq. 0) then 
          write(msg,*) this%separator('UPDATE SUCCEED ')
          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
          if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 )) write(this%lun_out,*) etb(msg)
       else
          if ( nrestart + 1 .eq. this%max_restart_update) then
             write(msg,*) this%separator('UPDATE FAILED ')
             if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
             if ( (this%info_update .ge. 1) .and. (this%lun_out .gt. 0 ) ) write(this%lun_out,*) etb(msg)
             write(this%lun_err,*) etb(msg)
          end if
       end if

!!$    case ('before_ouside') 
!!$       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) ' '
!!$       if (this%lun_statistics .gt. 1 ) write(this%lun_out,*) ' '
!!$       out_format=this%formatting('ai')
!!$       write(msg,out_format) &
!!$            'UPDATE # ',time_iteration
!!$       write(msg_out,*) etb(this%separator(msg))
!!$       if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$       if (this%lun_statistics .gt. 1 ) write(this%lun_out,*) etb(msg)
!!$    case ('after_ouside')
!!$       if ( info .eq. 0) then 
!!$          write(msg,*) this%separator('UPDATE SUCCEED ')
!!$          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$          if (this%lun_out .gt. 0 ) write(this%lun_out,*) etb(msg)
!!$       else
!!$          write(msg,*) this%separator('UPDATE FAILED ')
!!$          if (this%lun_statistics .gt. 0 ) write(this%lun_statistics,*) etb(msg)
!!$          if (this%lun_out .gt. 0 ) write(this%lun_out,*) etb(msg)
!!$          write(this%lun_err,*) etb(msg)
!!$       end if
       
    end select


  end subroutine print_info_update


  subroutine print_info_tdpot(this,&
       tdpot)
    use Globals 
    use TdensPotentialSystem
    implicit none
     class(DmkCtrl),    intent(in) :: this
     type(tdpotsys),    intent(in) :: tdpot
     !local
     character(len=256) :: msg

     
     if (this%info_state .gt. 0) then
        if ( this%lun_out > 0 ) then 
           call tdpot%info_time_evolution(this%lun_out)
        end if
        if ( this%lun_statistics > 0 ) then 
           call tdpot%info_time_evolution(this%lun_statistics)
        end if
     end if
     

    
!!$     if ((this%info_state .gt. 0) .and. (this%lun_out >0) ) then
!!$        if ( this%info_state .ge. 1 ) then
!!$           write(this%lun_out,*) ' '
!!$           msg=this%separator('INFO TIME EVOLUTION')
!!$           write(this%lun_out,'(a)') etb(msg)
!!$        end if
!!$        call tdpot%info_time_evolution(this%lun_out)
!!$        if ( this%info_state .ge. 2 ) then
!!$           msg=this%separator('INFO STATE')
!!$           write(this%lun_out,'(a)') etb(msg)
!!$           call tdpot%info_functional(this%lun_out)
!!$           msg=this%separator()
!!$           write(this%lun_out,'(a)') etb(msg)
!!$        end if
!!$     end if
!!$
!!$    if ((this%info_state .gt. 0) .and. (this%lun_statistics >0) ) then
!!$       write(this%lun_statistics,*) ' '
!!$       write(this%lun_statistics,*) this%separator('INFO TIME EVOLUTION')
!!$       call tdpot%info_time_evolution(this%lun_statistics)
!!$       write(this%lun_statistics,*) this%separator('INFO STATE')
!!$       call tdpot%info_functional(this%lun_statistics)
!!$    end if
  end subroutine print_info_tdpot

   function formatting(this,compressed,style) result (out_format)
    use Globals
    implicit none
    class(DmkCtrl),    intent(in   ) :: this
    character(len=*),  intent(in   ) :: compressed
    integer, optional, intent(in   ) :: style
    character(len=256) :: out_format
    !local
    integer :: i,length
    character(len=1) :: singleton
    
    out_format='('  
    
    

    length=len(compressed)


    if ( .not. present(style) .or. (style .eq. 1) ) then
       do i=1, length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat_info)
          case('r')
             out_format=etb(etb(out_format)//this%rformat_info)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    if ( present(style) .and. (style .eq. 2) ) then
       do i=1,length
          singleton=compressed(i:i)
          select case (singleton)
          case('a')
             out_format=etb(etb(out_format)//'a')
          case('i')
             out_format=etb(etb(out_format)//this%iformat)
          case('r')
             out_format=etb(etb(out_format)//this%rformat)
          end select
          if (i .lt. length) out_format=etb(etb(out_format)//',')
       end do
    end if

    out_format=etb(etb(out_format)//')')

  end function formatting




end module DmkControls
