!>-------------------------------------------------------------
!> procedures for writing vtk files. 
!>
!> only mesh and scalars up to now
!<------------------------------------------------------------- 
module vtkloc
  use Globals
  implicit none
  
  private
  !> outputs mesh info in vtk format
  public :: vtkmesh
contains
  !>-------------------------------------------------------------
  !> outputs mesh info in vtk format
  !>
  !> usage:
  !>     call vtkmesh(lun,fname,fileformat,time,geo)
  !>
  !> where:
  !> \param[in] lun: (integer) unit for error message output
  !> \param[in] fname: (character) filename for main output
  !> \param[in] fileformat: (character) 'ASCII' or 'BINARY'
  !> \param[in] time: (double) simulation time of output
  !> \param[in] geo: (mesh) mesh structure
  !>
  !> uses private subroutines alloc_mesh_tmp and
  !>      dealloc_mesh_tmp
  !<-----------------------------------------------------------
  subroutine vtkmesh(lun,fname,fileformat,time,nnode,coord,ntetra,tetra)
    use Globals
    use LIB_VTK_IO
    implicit none
    ! vars
    integer, intent(in) :: lun
    character(len=*),  intent(in) :: fname,fileformat
    real(kind=single), intent(in) :: time
    integer,           intent(in) :: nnode,ntetra
    real(kind=double), intent(in) :: coord(3,nnode)
    integer,           intent(in) :: tetra(5,ntetra)
    ! local vars
    real(kind=double), allocatable :: xcoord(:),ycoord(:),zcoord(:)
    integer, allocatable :: cell(:),ctype(:),material(:)
    integer :: nnode_in_cell
    integer :: res,E_IO
    integer :: i
    logical :: rc
    
    nnode_in_cell = 4
    call alloc_mesh_tmp(lun,ntetra,nnode, nnode_in_cell,&
         tetra,coord,cell,ctype,xcoord,ycoord,zcoord)

    allocate(material(ntetra),stat=res)
    if(res .ne. 0) rc = IOerr(lun, err_alloc, 'vtkmesh', &
         '  material (local array)',res)

    do i=1,ntetra
       material(i) = tetra(nnode_in_cell+1,i)
    end do

    E_IO = VTK_INI(output_format = fileformat, &
         filename = trim(fname), &
         title = 'Unstructured Grid', &
         time = time, &
         mesh_topology = 'UNSTRUCTURED_GRID')
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in initializing vtk file, E_IO=',E_IO)
    E_IO = VTK_GEO(Nn = nnode, X = xcoord, Y = ycoord, Z = zcoord)
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in writing coordinates in vtk file, E_IO=',E_IO)
    E_IO = VTK_CON(NC = ntetra, connect = cell, cell_type = ctype)
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in writing mesh topology in vtk file, E_IO=',E_IO)
    
    ! Materiali
    E_IO = VTK_DAT(NC_NN = ntetra, var_location = 'cell')
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in writing material headers in vtk file, E_IO=',E_IO)
    E_IO = VTK_VAR(NC_NN = ntetra, varname = 'Material', var = material)
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in writing mesh materials in vtk file, E_IO=',E_IO)

    E_IO = VTK_END()
    if (E_IO .ne. 0) rc = IOerr(lun, err_vtk, 'vtkmesh', &
         ' error in finalizing vtk file, E_IO=',E_IO)

    call dealloc_mesh_tmp(lun,cell,ctype,xcoord,ycoord,zcoord) 

    deallocate(material,stat=res)
    if (res.ne.0) rc=IOerr(lun, err_dealloc, 'vtkmesh', &
         'material (local array)',res)
  end subroutine vtkmesh
 
  !>-------------------------------------------------------------
  !> allocate temp arrays to be use by vtk subroutines
  !> (private procedure)
  !>
  !> usage:
  !>     call alloc_mesh_tmp(lun,ntria,nnod,nodes_in_tria,geo,
  !>          triangles,ctype,xcoord,ycoord,zcoord)
  !>
  !> where:
  !> \param[in] lun: (integer) unit for error message output
  !> \param[in] ntria: (integer) number of cells
  !> \param[in] nnod: (integer) number of nodes
  !> \param[in] nodes_in_tria: (integer) number of nodes per cell
  !> \param[in] geo: (mesh) mesh structure
  !> \param[in] triangles: (integer array of dim (ntria*nodes_in_tria)
  !>                   cell topology
  !> \param[in] ctype: (integer array, dim ntria) cell type
  !>               (so far only 5 -> triangles)
  !> \param[in] xcoord: (double array, dim nnod) nodal x-coord
  !> \param[in] ycoord: (double array, dim nnod) nodal z-coord
  !> \param[in] zcoord: (double array, dim nnod) nodal z-coord
  !>
  !<-----------------------------------------------------------
  subroutine alloc_mesh_tmp(lun,ncell,nnode,nnode_in_cell,&
       node_in_cell,coord,&
       cell,ctype,xcoord,ycoord,zcoord)
    use Globals
    use LIB_VTK_IO
    implicit none
    ! vars
    integer,                        intent(in ) :: lun
    integer,                        intent(in ) :: ncell,nnode,nnode_in_cell
    integer,                        intent(in ) :: node_in_cell(nnode_in_cell+1,ncell)
    real(kind=double)  ,            intent(in ) :: coord(3,nnode)
    integer,           allocatable, intent(out) :: cell(:),ctype(:)
    real(kind=double), allocatable, intent(out) :: xcoord(:),ycoord(:),zcoord(:)
    ! local vars
    integer :: res
    integer :: i,j,k
    logical :: rc

    allocate(cell(ncell*(nnode_in_cell+1)),stat=res)
    if(res .ne. 0) rc = IOerr(lun, err_alloc, 'alloc_mesh_tmp', &
         ' triangles (local array)',res)
    allocate(ctype(ncell),stat=res)
    if(res .ne. 0) rc = IOerr(lun, err_alloc, 'alloc_mesh_tmp', &
         ' ctype (local array)',res)
    allocate(xcoord(nnode),ycoord(nnode),zcoord(nnode),stat=res)
    if(res .ne. 0) rc = IOerr(lun, err_alloc, 'alloc_mesh_tmp', &
         ' xcoord,ycoord,zcoord (local arrays)',res)

    k=0
    do i=1,ncell
       k=k+1
       cell(k) = nnode_in_cell
       do j=1,nnode_in_cell
          k=k+1
          cell(k) = node_in_cell(j,i) - 1
       end do
       ctype(i) = 10
    end do

    do i=1,nnode
       xcoord(i) = coord(1,i)
       ycoord(i) = coord(2,i)
       zcoord(i) = coord(3,i)
    end do
  end subroutine alloc_mesh_tmp

  !>-------------------------------------------------------------
  !> deallocate temp arrays to be use by vtk subroutines
  !> (private procedure)
  !>
  !> usage:
  !>     call dealloc_mesh_tmp(lun,
  !>          triangles,ctype,xcoord,ycoord,zcoord)
  !>
  !> where:
  !> \param[in] lun: (integer) unit for error message output
  !> \param[in] triangles: (integer array of dim (ntria*nodes_in_tria)
  !>                   cell topology
  !> \param[in] ctype: (integer array, dim ntria) cell type
  !>               (so far only 5 -> triangles)
  !> \param[in] xcoord: (double array, dim nnod) nodal x-coord
  !> \param[in] ycoord: (double array, dim nnod) nodal z-coord
  !> \param[in] zcoord: (double array, dim nnod) nodal z-coord
  !>
  !<-----------------------------------------------------------
  subroutine dealloc_mesh_tmp(lun,triangles,ctype,xcoord,ycoord,zcoord) 
    use Globals
    use Geometry
    use LIB_VTK_IO
    implicit none
    ! vars
    integer :: lun
    integer, allocatable :: triangles(:),ctype(:)
    real(kind=double),allocatable :: xcoord(:),ycoord(:),zcoord(:)
    ! local vars
    integer :: res
    logical :: rc
    
    deallocate(triangles,stat=res)
    if (res.ne.0) rc=IOerr(lun, err_dealloc, 'dealloc_mesh_tmp', &
         'triangles (local)',res)
    deallocate(ctype,stat=res)
    if (res.ne.0) rc=IOerr(lun, err_dealloc, 'dealloc_mesh_tmp', &
         'ctype (local)',res)
    deallocate(xcoord,ycoord,zcoord,stat=res)
    if (res.ne.0) rc=IOerr(lun, err_dealloc, 'dealloc_mesh_tmp', &
         'xcoord,ycoord,zcoord (local)',res)
  end subroutine dealloc_mesh_tmp


  
  
end module vtkloc
