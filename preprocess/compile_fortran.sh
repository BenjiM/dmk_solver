#!/bin/bash

code_folders=$(find . -name "code")

for folder in $code_folders
do
    echo $folder
    cd $folder
    make clean
    #make dirs
    #make libs
    #make
    cd -
done
