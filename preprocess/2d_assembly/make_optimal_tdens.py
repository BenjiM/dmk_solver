# -*- coding: utf-8 -*-
#!/usr/bin/env python
import numpy as np
import sys
import os

#
# geometry
#
import meshtools as mt
import example_grid as ex_grid

#
# inputs
#
import example_forcing as ex_forcing
import common as common
import timecell as timecell

def define_optimal_tdens(flag_grid, flag_source, flag_sink,
                         extra_grid, extra_source, extra_sink,):
    #####################################################################
    # optimal transport density definition
    #####################################################################
    if ( # ( (flag_gridd='rect_cnst') or  (flag_source='rect_cnst_aligned') ) and 
            (flag_source=='rect_cnst') and 
            (flag_sink=='rect_cnst') ):
        def optimal_tdens(time,coord,flag):
            x=coord[0]
            y=coord[1]
            z=coord[2]
            value=0.0
            steady=True
            
            # supportrs info
            base=np.array([0.125,0.25])
            width=0.25
            height=0.5
            shift=0.25
            forcing_value=2.0
            if ( (x >  base[0]        ) and 
                 (x <= base[0]+width  ) and 
                 (y >  base[1]        ) and 
                 (y <= base[1]+height ) )  :
                value=(x-base[0])*forcing_value
            if ( (x >  base[0]+width        ) and 
                 (x <= base[0]+width+shift  ) and 
                 (y >  base[1]              ) and 
                 (y <= base[1]+height       ) )  :
                value=(width)*forcing_value
            if ( (x >  base[0]+  width+shift ) and 
                 (x <= base[0]+2*width+shift ) and 
                 (y >  base[1]               ) and 
                 (y <= base[1]+height        ) )  :
                value=(base[0]+2*width+shift-x)*forcing_value
            return value,steady;

        return optimal_tdens;        

if __name__ == "__main__":
    if len(sys.argv) > 1:
        fin_ctrl=sys.argv[1]  
        fin_grid=sys.argv[2]
        fout_optdens=sys.argv[3]
        
        ############################################
        # reads flags and other values
        ############################################
        ctrl = open(fin_ctrl, "r")
        ctrl_lines = ctrl.readlines()
        ctrl.close()
        
        # flags grid
        flag_grid=str(common.read_column('flag_grid',0,ctrl_lines))
        extra_grid=str(common.read_column('flag_grid',1,ctrl_lines))
        
        # flag_source
        flag_source=str(common.read_column('flag_source',0,ctrl_lines))
        extra_source=str(common.read_column('flag_source',1,ctrl_lines))
        
        #flag sink
        flag_sink=str(common.read_column('flag_sink',0,ctrl_lines))
        extra_sink=str(common.read_column('flag_sink',1,ctrl_lines))

        opt_tdens_functions=[]
        opt_tdens_functions.append(define_optimal_tdens(
            flag_grid, flag_source, flag_sink, 
            extra_grid, extra_source, extra_sink))
        
        if( len(opt_tdens_functions) >= 1):
            # reads coord topol and flags 
            coord, topol, flags = mt.read_grid(fin_grid)
            if ( coord.shape[1] == 2 ):
                zcoord = np.zeros([coord.shape[0],1])
                coord=np.append(coord, zcoord, axis=1)
            ncell=len(topol)
            bar_cell=mt.make_bar(coord,topol)


            # definitions 
            optdens_cell = np.zeros([ncell,1])
            steady=False
            time=0.0
            
            # open file and write dimensions
            file_out=open(fout_optdens, 'w')
            timecell.write2file(file_out,time,True,optdens_cell)
            
            optdens_cell, steady = timecell.build(
                optdens_cell, steady, 
                opt_tdens_functions,time,bar_cell,flags)
            # write 2 file
            timecell.write2file(file_out,time,False,optdens_cell,steady)
            
            file_out.close()
                
        else:
            print('Optimal transport density not defined')
        
    else:
        raise SystemExit("usage:  python make_optimal_tdens.py 'input ctrl file' 'grid file' 'output file'"  )
