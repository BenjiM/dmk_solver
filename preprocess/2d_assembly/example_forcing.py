#!/usr/bin/env python
import numpy as np
import common
import meshtools as mt


# assembly procedure 
def make_source(source_cell,steady,sources,time,flags,bar_cell):
    f_cell=0.0
    steady=True
    source_cell[:]=0.0
    for icell in range(len(bar_cell)):
        for ifun in range(len(sources)):
            f_cell,steady_cell=sources[ifun](
                time,
                bar_cell[icell][0],
                bar_cell[icell][1],
                bar_cell[icell][2],
                flags[icell])
            source_cell[icell][:]+=float(f_cell)
            steady=steady and steady_cell
    return source_cell,steady;

def make_sink(sink_cell,steady,sinks,time,flags,bar_cell):
    f_cell=0.0
    steady=True
    sink_cell[:]=0
    for icell in range(len(bar_cell)):
        for ifun in range(len(sinks)):
            f_cell,steady_cell=sinks[ifun](
                time,
                bar_cell[icell,0],
                bar_cell[icell][1],
                bar_cell[icell][2],
                flags[icell])
            sink_cell[icell][:]+=float(f_cell)
            steady=steady and steady_cell
    return sink_cell,steady;


# assembly procedure 
def make_dirac_source(source_nodes,steady,dirac_sources,time,path_file,coord):
    steady_global=True
    source_nodes[:]=0
    for fdirac in dirac_sources:
        source_coord,values, steady=fdirac(time,path_file)
        steady_global = steady_global and steady
        for idirac in range(len(source_coord)):
            inode,dist=mt.FindClosestNode(
                range(len(coord)),coord,source_coord[idirac])
            source_nodes[inode[0]][:] += values[idirac]
            
    return source_nodes, steady_global;

# assembly procedure 
def make_dirac_sink(sink_nodes,steady,dirac_sinks,time,path_file,coord):
    steady_global=True
    sink_nodes[:]=0
    for fdirac in dirac_sinks:
        sink_coord=[]
        sink_values=[]
        sink_coord,values, steady=fdirac(time,path_file)
        steady_global = steady_global and steady
        for idirac in range(len(sink_coord)):
            inode,dist=mt.FindClosestNode(
                range(len(coord)),coord,sink_coord[idirac])
            sink_nodes[inode[0]][:] += values[idirac]
    return sink_nodes, steady_global;



def correction(topol,
               source_cell,sink_cell, 
               dirac_source_values, dirac_sink_values,
               size_cell, tol=1.0e-10):

    for icell in range(len(sink_cell)):
        if (source_cell[icell,0] * sink_cell[icell,0] != 0.0):
            sink_cell[icell,0]=0.0
            #print icell,source_cell[icell,0], sink_cell[icell,0]
            #sys.exit('source and sink have no disjoint support')

    rhs = assembly_rhs(topol,
                       source_cell-sink_cell,
                       dirac_source_values-dirac_sink_values,size_cell)
    rhs_source = assembly_rhs(topol,
                       source_cell,
                       dirac_source_values,size_cell)
    rhs_sink  = assembly_rhs(topol,
                       sink_cell,
                       dirac_sink_values,size_cell)

    plus =np.sum(rhs_source)
    minus=np.sum(rhs_sink)
    imbalance =np.sum(rhs_source-rhs_sink)
        
    if ( abs(imbalance) >= tol):
        print ('imbalance=',imbalance, sum(rhs,all>0.0),sum(rhs,all<0.0))
        correct_factor = plus/minus 
        print ('correction factor =',correct_factor)
        sink_cell=sink_cell*correct_factor
        #dirac_sink_values=dirac_sink_values*correct_factor
        rhs_sink  = assembly_rhs(topol,
                       sink_cell,
                       dirac_sink_values,size_cell)
        print ('imbalance=',np.sum(rhs_source-rhs_sink))


    return source_cell,sink_cell, dirac_source_values, dirac_sink_values;

def assembly_rhs(topol,lebesgue, dirac,cell_size):
    rhs=np.zeros(dirac.shape[0])
    nnodeincell=topol.shape[1]
    for icell in range(len(lebesgue)):
        for node in topol[icell,:]:
            rhs[node]=rhs[node] + lebesgue[icell][0] * cell_size[icell] / nnodeincell

    
    for node in range(len(dirac)):
        if (dirac[node][0] != 0.0):
            rhs[node] = dirac[node][0]
    return (rhs)
        



def normalize(source_cell,sink_cell, 
              dirac_source_values, dirac_sink_values,
              size_cell):
    fplus=np.dot(source_cell,size_cell)+sum(dirac_source_values)
    fminus=np.dot(sink_cell,size_cell)+sum(dirac_sink_values)
    source_cell=source_cell/fplus
    dirac_source_values=dirac_source_values/fplus
    sink_cell=sink_cell/fminus
    dirac_sink_values=dirac_sink_values/fminus
    return source_cell,sink_cell, dirac_source_values, dirac_sink_values;

# build 
def example_source(sources,dirac_sources,flag_source,extra_info):
    #  personal
    print(flag_source)
    if (flag_source == 'cnst'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if (flag_mesh > 0 ):
                fvalue=2.0
            return fvalue, steady;
        sources.append(source)

    #  personal
    if (flag_source == 'uniform'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            fvalue=1.0
            return fvalue, steady;
        sources.append(source)

     #  personal
    if (flag_source == 'zero'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            fvalue=0.0
            return fvalue, steady;
        sources.append(source)

    #  A parallel fast sweeping method for the Eikonal equation
    #  Miles Detrixhea, Frederic Giboua,b, Chohong Min
    if (flag_source == 'example1fmt'):
        def dirac_source(t,info_path):
            coord_points=[[0.0,0.0,0.0]]
            values=[1.0]
            steady=True
            return coord_points, values,steady;
        dirac_sources.append(dirac_source)

    if (flag_source == 'one2two'):
        def dirac_source(t,info_path):
            coord_points=[[0.5,0.1,0.0]]
            values=[1.0]
            steady=True
            return coord_points, values,steady;
        dirac_sources.append(dirac_source)


    # An accurate discontinuous Galerkin method for solving point-source
    # Eikonal equation in 2-D heterogeneous anisotropic media
    # Bouteiller,1 M. Benjemaa,2 L. Metivier1,3 and J. Virieux1
    if (flag_source == 'bouteiller18_tc1'):
        def dirac_source(t,info_path):
            coord_points=[[2.0,2.0,0.0]]
            values=[1.0]
            steady=True
            return coord_points, values,steady;
        dirac_sources.append(dirac_source)

    


    #  prigozhin
    if (flag_source == 'prigozhin'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if (flag_mesh == 1 ):
                fvalue=2.0
            return fvalue, steady;
        sources.append(source)


    #  sin
    if (flag_source == 'sin'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            if (flag_mesh > 0 ):
                fvalue=np.sin(2.0*y)
            steady=True
            return fvalue,steady;
        sources.append(source)
            
    #  rectangles constat
    if (flag_source == 'rect_cnst'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x >= 1.0/8.0) and (x<=3.0/8.0) and 
                 (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
                fvalue=2.0
            return fvalue,steady;
        sources.append(source)

    #  rectangles constat
    if (flag_source == 'cnst_center'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x >= 0.4) and (x<=0.6) and 
                 (y >= 0.4) and (y<=0.6) ) :
                fvalue=1.0
            return fvalue,steady;
        sources.append(source)
    
    #  rectangles cont
    if (flag_source == 'rect_continuous'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x >= 1.0/8.0) and (x<=3.0/8.0) and 
                 (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
                fvalue=(np.sin(4*np.pi*(x-1.0/8.0)) * 
                        np.sin(2*np.pi*(y-1.0/4.0)) ) 
            return fvalue,steady;
        sources.append(source)

    x17 ,  x18 , y17, y18, y19, y20, y21, y22, x19, x20= 0.1, 0.2, 0.1, 0.250, 0.475, 0.625, 0.750, 0.9, 0.45, 0.55
    if (flag_source == '5rcm'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ((x > x17 and x < x18 and y > y17 and y < y18) 
                or (x > x17 and x < x18 and y > y19 and y < y20) 
                or (x > x17 and x < x18 and y > y21 and y < y22) 
                or (x > x19 and x < x20 and y > y19 and y < y20)):
                fvalue=2.0
            return fvalue, steady;
        sources.append(source)

   
    #  y branch problem
    if (flag_source == 'ybranch'):
        # read points
        info_path=common.remove_comments(extra_info,'!')
        def dirac_source(t,info_path):
            coord_points=[]
            values=[]
            steady=True
            input_file=open(info_path,'r')
            input_lines = input_file.readlines()
            for line in input_lines[0:3]:
                coord_dirac = [float(w) for w in line.split()[0:2]]
                branch_value = float(line.split()[2])
                if ( branch_value > 0.0 ):
                    values.append(branch_value)
                    coord_points.append(coord_dirac)                    
            input_file.close()
            return coord_points, values,steady;
        dirac_sources.append(dirac_source)
        
    #  y branch problem
    if (flag_source == 'dirac'):
        # read points
        print(extra_info)

        info_path=common.remove_comments(extra_info,'!')
        def dirac_source(t,info_path):
            coord_points=[]
            values=[]
            steady=True
            input_file=open(info_path,'r')
            input_lines = input_file.readlines()
            print(len(input_lines),'source')
            for line in input_lines[0:]:
                coord = [float(w) for w in line.split()[0:3]]
                value = float(line.split()[3])
                print(coord,value)
                # check for positive values only
                if ( value > 0.0 ):
                    values.append(value)
                    coord_points.append(coord)                    
            input_file.close()
            return coord_points, values,steady;
        dirac_sources.append(dirac_source)


    return sources, dirac_sources;

# build standard tdens_0  given id_tdens0
def example_sink(sinks,dirac_sinks,flag_sink,extra_info): 
    print( 'Sink flag ='+flag_sink)
    #  constant examples
    if (flag_sink == 'cnst'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if (flag_mesh < 0 ):
                fvalue=2.0
            return fvalue,steady;
        sinks.append(sink)

    #  personal
    if (flag_sink == 'uniform'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            fvalue=1.0
            return fvalue, steady;
        sinks.append(sink)

    if (flag_sink == 'one2two'):
        def dirac_sink(t,info_path):
            coord_points=[[0.4,0.9,0.0],[0.6,0.9,0.0]]
            values=[0.5,0.5]
            steady=True
            return coord_points, values,steady;
        dirac_sinks.append(dirac_sink)

    x11 , x12 , y11, y12 = 0.750, 0.9, 0.475, 0.625
    if (flag_sink == '5rcm'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if (x > x11 and x < x12 and y > y11 and y < y12):
                fvalue=2.0
            return fvalue, steady;
        sinks.append(sink)


    #  A parallel fast sweeping method for the Eikonal equation
    #  Miles Detrixhea, Frederic Giboua,b, Chohong Min
    if (flag_sink == 'example1fmt'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=1.0
            steady=True
            return fvalue, steady;
        sinks.append(sink)

    # An accurate discontinuous Galerkin method for solving point-source
    # Eikonal equation in 2-D heterogeneous anisotropic media
    # Bouteiller,1 M. Benjemaa,2 L. Metivier1,3 and J. Virieux1
    if (flag_sink == 'bouteiller18_tc1'):
        def sink(t,x,y,z,info_path):
            fvalue=1.0
            steady=True
            return fvalue, steady;
        sinks.append(sink)


    #  personal
    if (flag_sink == 'corner'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( x**2+(y-1.0)**2>0.005 ):
                fvalue=1.0
            return fvalue, steady;
        sinks.append(sink)

    #  rectangles constat
    if (flag_sink == 'cnst_center'):
        def source(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x < 0.4) and (x>0.6) and 
                 (y < 0.4) and (y>0.6) ) :
                fvalue=1.0
            return fvalue,steady;
        sources.append(source)



    #  prigozhin
    if (flag_sink == 'prigozhin'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if (flag_mesh == -1 ):
                fvalue=2.0
            return fvalue, steady;
        sinks.append(sink)


    #  sin
    if (flag_sink == 'sin'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            if (flag_mesh < 0 ):
                fvalue=np.sin(2.0*x)
            steady=True
            return fvalue,steady;
        sinks.append(sink)


    # classical example
    if (flag_sink == 'rect_cnst'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x >= 5.0/8.0) and 
                 (x <= 7.0/8.0) and 
                 (y >= 1.0/4.0) and 
                 (y<=3.0/4.0)     ) :
                fvalue=2.0
            return fvalue, steady;
        sinks.append(sink)
    
    #  rectangles continouos
    if (flag_sink == 'rect_continuous'):
        def sink(time,x,y,z,flag_mesh):
            fvalue=0.0
            steady=True
            if ( (x >= 5.0/8.0) and (x<=7.0/8.0) and 
                 (y >= 1.0/4.0) and (y<=3.0/4.0) ) :
                fvalue=(np.sin(4*np.pi*(x-5.0/8.0)) * 
                        np.sin(2*np.pi*(y-1.0/4.0)) ) 
            return fvalue,steady;
        sinks.append(sink)

     
    #  y branch problem
    if (flag_sink == 'ybranch'):
        # read points
        file_path=common.remove_comments(extra_info,'!')
        def dirac_sink(time, file_path):
            coord_points=[]
            values=[]
            input_file  = open(file_path,'r')
            input_lines = input_file.readlines()
            for line in input_lines[0:3]:
                coord_dirac = [float(w) for w in line.split()[0:3]]
                branch_value = float(line.split()[3])
                if ( branch_value < 0.0 ):
                    values.append(branch_value)
                    coord_points.append(coord_dirac)
            input_file.close()
            steady=True

            return coord_points, values, steady;
        dirac_sinks.append(dirac_sink)
        
    #  y branch problem
    if (flag_sink == 'dirac'):
        # read points
        print(extra_info)
        info_path=common.remove_comments(extra_info,'!')
        def dirac_sink(t,info_path):
            coord_points=[]
            values=[]
            steady=True
            input_file=open(info_path,'r')
            input_lines = input_file.readlines()
            for line in input_lines[0:]:
                coord = [float(w) for w in line.split()[0:3]]
                value = float(line.split()[3])
                print(coord,value)
                # check for positive values only
                if ( value > 0.0 ):
                    values.append(value)
                    coord_points.append(coord)                    
            input_file.close()
            return coord_points, values,steady;
        dirac_sinks.append(dirac_sink)


    return sinks, dirac_sinks;              


def known_optpot(flag_grid,flag_source,flag_sink):
    if ( flag_grid=='example1fmt' and 
         flag_source=='example1fmt' and 
         flag_sink=='example1fmt'):
        def optpot(coord,flag):
            x=coord[0]
            y=coord[1]
            z=coord[2]
            if (x>0 and y>0):
                f=x**2+y**2+3*x*y
            else:
                f=x**2+y**2
            return f;
    
    if ( flag_grid=='bouteiller18_tc1' and 
         flag_source=='bouteiller18_tc1' and 
         flag_sink=='bouteiller18_tc1'):
        def optpot(coord,flag):
            x=coord[0]
            y=coord[1]
            z=coord[2]
            #
            Szero=2
            gzero=(0.0,0.5)
            gzeronorm=np.sqrt(gzero[0]**2+gzero[1]**2)
            xzero=(2.0,2.0)
            S=1/(1/Szero + np.scal(gzero,coord[0:1]-xzero))
            
            f=1/Gzeronorm * np.arccosh(
                1+0.5*S*Szero*Gzeronrm**2*((x-xzero[0])**2+(y-xzer0[1])**2))
            return f;
    
    return optpot;
